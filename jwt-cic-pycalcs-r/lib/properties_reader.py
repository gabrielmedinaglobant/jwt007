import ConfigParser

def read_properties(env, file):
    """ Reads properties from a files and converts then to a dictionary """

    config = ConfigParser.ConfigParser()
    config.read(file)
    dict1 = {}

    section = "PROPERTIES"
    props = config.options(section)
    for prop in props:
        try:
            dict1[prop] = config.get(section, prop)
            if dict1[prop] == -1:
                print("skip: %s" % prop)
        except Exception as e:
            print("exception on %s! " % prop)
            print(e)
            dict1[prop] = None

    section = env + "_PROPERTIES"
    envprops = config.options(section)
    for prop in envprops:
        try:
            dict1[prop] = config.get(section, prop)
            if dict1[prop] == -1:
                print("skip: %s" % prop)
        except Exception as e:
            print("exception on %s!" % prop)
            print(e)
            dict1[prop] = None

    return dict1