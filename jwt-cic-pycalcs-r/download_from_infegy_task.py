import getopt
import logging
from datetime import date, datetime, timedelta
import mysql.connector
import time
import sys
import os

import request_infegy.request_jwt as jwt

from lib import properties_reader


logger = logging.getLogger('download_from_infegy_task')

def download(properties):
    try:
        """it set mysql connect"""
        cnx = mysql.connector.connect(user=properties['db_user'], \
        password=properties['db_password'], database=properties['db_schema'], \
        host=properties['db_host'], port=properties['db_port'])
        """it build a mysql cursor"""
        cursor = cnx.cursor(named_tuple=True, buffered=True)

        """ it trasform datetime to string in %Y-%m-%d format because the next query need this"""
        today = datetime.today().strftime('%Y-%m-%d')
        """it declare query on parameters table"""
        query = ("SELECT project_uuid,query_type,infegy_api_key,query_string FROM vw_query_types_per_project")

        """ it shoot query"""
        cursor.execute(query)

        """it get all result of query"""
        result = cursor.fetchall()
        brandic = []
        """this loop if for get URL Query and  shoot querys on infegy. after this it save in the properties['files_directory']"""
        """properties['directory'] will be a json directory the where others scripting will take from this """
        cursorbrand = cnx.cursor(named_tuple=True, buffered=True)
        for k in result:
          dir_ = properties['files_directory'] + k[0]
          """ at this moment we are ready for build directory tree"""
          if not os.path.isdir(dir_):
              os.makedirs(dir_)
              subdir=dir_+'/output/' + k[1]
              os.makedirs(subdir)
          else:


             try:
                 subdir = dir_ + '/output/' + k[1]
                 if not os.path.isdir(subdir):
                  print subdir
                  os.mkdir(subdir)
                 if not os.path.isdir(subdir):
                     try:
                         os.mkdir(subdir)
                     except Exception as e:
                         logger.error(e.message)
             except Exception as e:
                logger.error("Error in MK Dir " +e.message)
                exit(1)
        """Now we are shooting the download process"""
        #print brandic
        for k in result:
            try:
                filepath=""
                jwt.atlas_jwt_request.ATLAS_API_KEY = k[2]
                q = jwt.atlas_jwt_request(k[3])
                query_type_files = ("SELECT * FROM vw_query_files_of_query_types WHERE query_type =%s")
                cursorbrand.execute(query_type_files, (k[1],))
                brandResult = cursorbrand.fetchall()
                for i in brandResult:
                    method = i[1].split('.')[0]
                    json_return_method = q.method(method)
                    filepath = properties['files_directory'] + k[0] +'/output/'+ k[1]+'/'+i[1]
                    jsonfile = open(filepath, "w")
                    jsonfile.write(json_return_method)
                    time.sleep(2)
            except Exception as e:
                logger.error("DOWNLOAD  error :" +e.message)
                exit(1)
        """close cursor"""
        cursor.close()
        """close myslq connect"""
        cnx.close()

    except Exception as e:
       logger.error("Download failed. Error in get json file  :" + e.message)
       print e.message
       exit(1)

def main(argv):
    """ Argument parsing """
    opts, args = getopt.getopt(argv, "e:", ["env="])
    env = ""

    for opt, arg in opts:
        if opt in ("-e","--env"):
            env = arg

    if (env == ""):
	    print("No arguments were provided, using --env=LOCAL")
	    env = "LOCAL"

    """ Read properties according to environment """
    properties = properties_reader.read_properties(env, "properties.ini")

    """ Set logger properties """
    logging.basicConfig(format='%(asctime)-15s %(message)s', filename=properties['logs_directory']+'download_from_infegy_task.log', level=logging.INFO)

    print("Executing download script!!")
    download(properties)


if __name__ == '__main__':
    main(sys.argv[1:])

