JWT CIC
====================

Quick project description
-------------------

Run "mvn clean package" on "root" directory to deploy all modules.
You can also run "mvn clean package" on each project separately.

Services module - jwt-cic-services
-------------------
(TBD)

Frontend module - jwt-cic-frontend
-------------------
(TBD)

Python scripts module - (TBD)
-------------------
(TBD)