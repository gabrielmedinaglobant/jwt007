# jwt-cic-frontend

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Suggestions

Some of the folder organization was borrowed from this guideline:
https://github.com/toddmotto/angular-styleguide
Components module
Common module
*.module.js file convention
*.component.js file convention
*.service.js file convention
*.directive.js file convention
*.filter.js file convention
*.spec.js file convention
*.html file convention
*.css file convention, (use of BEM would be great)
use "jwt-" prefix for each component
d3.js v4 will be used

Feel free to override jwtApp component or delete current pie-chart component, it was just a small demo example...

We are basically using Angular 1.6 (following components from 1.5), Bower, Grunt, having each .js on the index.html,
wich perhaps can be automated using something like .js scanning and inject, but atm its not priority...
