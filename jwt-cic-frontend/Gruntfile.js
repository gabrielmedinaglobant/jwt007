// Generated on 2017-03-02 using generator-angular 0.16.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
      includeSource:'grunt-include-source',
      bowerConcat:'grunt-bower-concat',
      'gruntless':'grunt-contrib-less',
      'ngconstant':'grunt-ng-constant',
      'gruntAddComment':'grunt-add-comment'
  });

    
// Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'src',
    dist: 'dist'
  };
    
var includeObj = {
    options: {
        basePath: 'src/',
        baseUrl: '',
        ordering: 'top-down'
    },
    app: {
        files: {
            'src/index.html': 'src/index.html'
            // you can add karma config as well here if want inject to karma as well
        }
    }
};    
    
  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,
//     app : {
//        // Application variables
//        scripts: [
//               'scripts/**/*.js',
//               'app/app.module.js',
//               'app/app.component.js',
//               'app/**/*.module.js',
//               'app/**/*.constant.js',
//               'app/services/**/*.service.js',
//               'app/**/*.service.js',
//               'app/**/*.controller.js',    
//               'app/**/*.component.js',
//               'app/**/*.js',
//             ],
//        css: [
//               'styles/**/*.css',
//               'app/assets/css/**/*.css'
//             ]
//    },
      app : {
          scripts: [
                   'scripts/vendor.js',
                   'scripts/jwt-app-built.js'
                 ],
            css: [
                    'styles/vendor.css',
                    'app/assets/css/jwt-app-built.css',
                    'app/**/*.css',
                    'styles/**/*.css'
                 ]
      },
      
      includeSource: includeObj,      
      
      
bowerConcat: {
  all: {
    dest: {
      'js': 'dist/scripts/jwt-app-bower.js'
    }
  }
},      
    less: {
      build: {
        src: ['src/app/assets/less/app.main.less'],
        dest: 'src/app/assets/css/jwt-app-built.css'
      }
    },
    ngconstant: {
        options: {
          name: 'config',
          dest: 'src/app/common/constants/config.js',
          constants: {
            package: grunt.file.readJSON('src/jwt.config.json')
          }
        },
        build: {
        }
    },
  // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['src/app/**/*.js'],
        tasks: ['concat:serveDistJS',/*'newer:jshint:all', 'newer:jscs:all'*/],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css','<%= yeoman.app %>/app/**/*.less'],
        tasks: ['newer:copy:styles', 'postcss','less']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0',
        livereload: 35729
      },
      livereload: {
        options: {
          open: false,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/src/styles',
                connect.static('./src/styles')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: false,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Make sure code styles are up to par
    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    postcss: {
      options: {
        processors: [
          require('autoprefixer-core')({browsers: ['last 1 version']})
        ]
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath:  /\.\.\//
      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath:  /\.\.\//,
        fileTypes:{
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
              detect: {
                js: /'(.*\.js)'/gi
              },
              replace: {
                js: '\'{{filePath}}\','
              }
            }
          }
      }
    }, 

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/{,*/}*.js',
          '<%= yeoman.dist %>/styles/{,*/}*.css',
          '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= yeoman.dist %>/styles/fonts/*'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/styles'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
//     uglify: {
//       dist: {
//         files: {
//           '<%= yeoman.dist %>/scripts/bower.js': [
//             '<%= yeoman.dist %>/scripts/bower.js'
//           ]
//         }
//       }
//     },
     concat: {
       distJS: {
                 src: [
                       'src/app/app.module.js',
                       'src/app/app.component.js',
                       'src/app/**/*.module.js',
                       'src/app/**/*.component.js',
                       'src/app/**/*.constant.js',
                       'src/app/**/*.service.js',
                       'src/app/**/*.js'
                    ],
                    dest: 'dist/scripts/jwt-app-built.js'
       },
       serveDistJS: {
                 src: [
                       'src/app/app.module.js',
                       'src/app/app.component.js',
                       'src/app/**/*.module.js',
                       'src/app/**/*.component.js',
                       'src/app/**/*.constant.js',
                       'src/app/**/*.service.js',
                       'src/app/**/*.js'
                    ],
                    dest: 'src/scripts/jwt-app-built.js'
       },
       distCSS:{
                 src: [
                       'src/app/assets/css/*.css'
                    ],
                    dest: 'src/app/assets/css/jwt-app-built.css',
                }           
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'jwtDemoFrontendApp',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/scripts.js'
        },
        cwd: '<%= yeoman.app %>',
        src: 'views/{,*/}*.html',
        dest: '.tmp/templateCache.js'
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
     build: {
        cwd: 'src',
        src: [ 'app/**/*.html','app/**/*.css','app/assets/img/**/*.{ico,png,jpg,svg,gif}','app/assets/fonts/**/*.{eot,svg,ttf,woff,woff2}' ],
        dest: 'dist',
        expand: true
      },    
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '*.html',
            'images/{,*/}*.{webp}',
            'styles/fonts/{,*/}*.*'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: 'bower_components/bootstrap/dist',
          src: 'fonts/*',
          dest: '<%= yeoman.dist %>'
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'copy:styles',
        'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    }
  });

    
    function setEnviroment(){
        var jwtConfigJson = grunt.file.readJSON('src/jwt.config.json');
        var env = grunt.option('env');
        grunt.log.writeln("env : ",env);
        var jwtConfigConstant = {AUTH_APP:"",BASE_API:"",APP_VERSION:jwtConfigJson.APP_VERSION,APP_URL_PREFIX:''};
        if(env){
            jwtConfigConstant.BASE_API = jwtConfigJson.API[env];
            jwtConfigConstant.AUTH_APP = jwtConfigJson.AUTH_APP[env];
            jwtConfigConstant.APP_URL_PREFIX = jwtConfigJson.APP_URL_PREFIX[env];
        }
        var c = {
            options: {
              name: 'app.common',
              dest: 'src/app/common/constants/jwt.config.constant.js',
              constants: {
                jwtConfigConstant: jwtConfigConstant
              }
            },
            build: {
            }
        }
        grunt.config('ngconstant' ,c);
        
      var cc =  {
          com : {
            options: {
               comments: ['JWT App Version : '+jwtConfigConstant.APP_VERSION],
                carriageReturn: "\n",
                prepend: true,
                syntaxes: {
                  '.html': ['<!--', '-->']
                }
            },
            files: [{
            src: ['src/index.html'],
            dest: 'src/index.html'

            }]
      }
  }
        grunt.config('add_comment' ,cc);
        
    }    

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }
    setEnviroment();
      
    grunt.task.run([
      'clean:server',
      'wiredep',
       'ngconstant',
      'concat',
      'concat:serveDistJS',        
      'includeSource',
      'add_comment',
      'concurrent:server',
      'postcss:server',
      'connect:livereload',
      'less',
      'watch'
    ]);
  });

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'wiredep',
    'concurrent:test',
    'postcss',
    'connect:test',
    'karma'
  ]);

//  grunt.registerTask('build', [
//    'clean:dist',
//    'wiredep',
//    'useminPrepare',
//    'concurrent:dist',
//    'postcss',
//    'ngtemplates',
//    'concat',
//    'ngAnnotate',
//    'copy:dist',
//    'cdnify',
//    'cssmin',
//    'uglify',
//    'filerev',
//    'usemin',
//    'htmlmin'
//  ]);
    
    
  grunt.registerTask('buildNew', [
    'clean:dist',
    'wiredep',
    'less',
    'ngconstant',  
    'copy:build',
    'useminPrepare',
    'concurrent:dist',
      'add_comment',
//    'postcss',
//    'ngtemplates',
      'concat',
      //'concat:distCSS', temporary comment
      'concat:distJS',
      //'bowerConcat',
//    'ngAnnotate',
    'copy:dist',
      
//    'cdnify',
    'cssmin',
    'uglify',
//    'filerev',
//    'usemin',
//    'htmlmin',
      
      'includeSource',

  ]);
    
  grunt.registerTask('build', '', function () {
      includeObj = {
        options: {
            basePath: 'dist/',
            baseUrl: '',
            ordering: 'top-down'
        },
        app: {
            files: {
                'dist/index.html': 'dist/index.html'
                // you can add karma config as well here if want inject to karma as well
            }
        }
    };
//    var distApp = {
//        // Application variables
//        scripts: [
//               'scripts/jwt-app-bower.js',
//               'scripts/vendor.js',
//               'scripts/jwt-app-built.js'
//             ],
//        css: [
//                'styles/vendor.css',
//                'app/assets/css/jwt-app-built.css',
//                'app/**/*.css',
//                'styles/**/*.css'
//             ]
//    };
    setEnviroment();  
    grunt.config('includeSource' ,includeObj);
    //grunt.config('app' ,distApp);
    grunt.task.run(['buildNew']);
  });    

  grunt.registerTask('default', [
    'newer:jshint',
    'newer:jscs',
    // 'test',
    //'build'
  ]);
};
