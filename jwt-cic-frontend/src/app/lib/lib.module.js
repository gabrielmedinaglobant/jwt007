'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Common module of the application.
 */
angular
  .module('app.lib', [
  ]);
