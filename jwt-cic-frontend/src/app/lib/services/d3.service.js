(function() {
'use strict';

  angular
    .module('app.lib')
    .factory('D3Service', D3Service)

    D3Service.$inject = ['$window'];

    function D3Service($window) {
      var service = $window.d3;
      
      if(!service){
        console.log("D3 library not found");
      }

      return service;
    }


})();