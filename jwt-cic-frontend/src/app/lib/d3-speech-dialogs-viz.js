(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (factory((global.d3 = global.d3 || {})));
}(this, function (exports) {
    'use strict';

    // ===========================
    // ancillary geometric classes
    // ===========================
    var Point = function (x, y) {
        this.x = x;
        this.y = y;
    };

    Point.prototype = {
        dist: function (p) { return this.vect(p).norm(); },
        vect: function (p) { return new Point(p.x - this.x, p.y - this.y); },
        norm: function (p) { return Math.sqrt(this.x * this.x + this.y * this.y); },
        add: function (v) { return new Point(this.x + v.x, this.y + v.y); },
        mult: function (a) { return new Point(this.x * a, this.y * a); }
    };


    var Circle = function (radius, center) {
        this.r = radius;
        this.c = center;
    };

    Circle.prototype = {
        surface: function () { return Math.PI * this.r * this.r; },
        distance: function (circle) { return this.c.dist(circle.c) - this.r - circle.r; }
    };


    // =========================
    // circle packer lives here!
    // =========================
    var Packer = function (circles, ratio) {
        this.circles = circles;
        this.ratio = ratio || 1;
        this.list = this.solve();
    };

    Packer.prototype = {
        // try to fit all circles into a rectangle of a given surface
        compute: function (surface) {
            // check if a circle is inside our rectangle
            function in_rect(radius, center) {
                if (center.x - radius < - w / 2) return false;
                if (center.x + radius > w / 2) return false;
                if (center.y - radius < - h / 2) return false;
                if (center.y + radius > h / 2) return false;
                return true;
            }

            // approximate a segment with an "infinite" radius circle
            function bounding_circle(x0, y0, x1, y1) {
                var xm = Math.abs((x1 - x0) * w);
                var ym = Math.abs((y1 - y0) * h);
                var m = xm > ym ? xm : ym;
                var theta = Math.asin(m / 4 / bounding_r);
                var r = bounding_r * Math.cos(theta);
                return new Circle(bounding_r,
                    new Point(r * (y0 - y1) / 2 + (x0 + x1) * w / 4,
                        r * (x1 - x0) / 2 + (y0 + y1) * h / 4));
            }

            // return the corner placements for two circles
            function corner(radius, c1, c2) {
                var u = c1.c.vect(c2.c); // c1 to c2 vector
                var A = u.norm();
                if (A == 0) return [] // same centers
                u = u.mult(1 / A); // c1 to c2 unary vector
                // compute c1 and c2 intersection coordinates in (u,v) base
                var B = c1.r + radius;
                var C = c2.r + radius;
                if (A > (B + C)) return []; // too far apart
                var x = (A + (B * B - C * C) / A) / 2;
                var y = Math.sqrt(B * B - x * x);
                var base = c1.c.add(u.mult(x));

                var res = [];
                var p1 = new Point(base.x - u.y * y, base.y + u.x * y);
                var p2 = new Point(base.x + u.y * y, base.y - u.x * y);
                if (in_rect(radius, p1)) res.push(new Circle(radius, p1));
                if (in_rect(radius, p2)) res.push(new Circle(radius, p2));
                return res;
            }

            /////////////////////////////////////////////////////////////////

            // deduce starting dimensions from surface
            var bounding_r = Math.sqrt(surface) * 100; // "infinite" radius
            var w = this.w = Math.sqrt(surface * this.ratio);
            var h = this.h = this.w / this.ratio;

            // place our bounding circles
            var placed = [
                bounding_circle(1, 1, 1, -1),
                bounding_circle(1, -1, -1, -1),
                bounding_circle(-1, -1, -1, 1),
                bounding_circle(-1, 1, 1, 1)];

            // Initialize our rectangles list
            var unplaced = this.circles.slice(0); // clones the array
            while (unplaced.length > 0) {
                // compute all possible placements of the unplaced circles
                var lambda = {};
                var circle = {};
                for (var i = 0; i != unplaced.length; i++) {
                    var lambda_min = 1e10;
                    lambda[i] = -1e10;
                    // match current circle against all possible pairs of placed circles
                    for (var j = 0; j < placed.length; j++)
                        for (var k = j + 1; k < placed.length; k++) {
                            // find corner placement
                            if (k > 3) {
                                var zog = 1;
                            }
                            var corners = corner(unplaced[i], placed[j], placed[k]);

                            // check each placement
                            for (var c = 0; c != corners.length; c++) {
                                // check for overlap and compute min distance
                                var d_min = 1e10;
                                for (var l = 0; l != placed.length; l++) {
                                    // skip the two circles used for the placement
                                    if (l == j || l == k) continue;

                                    // compute distance from current circle
                                    var d = placed[l].distance(corners[c]);
                                    if (d < 0) break; // circles overlap

                                    if (d < d_min) d_min = d;
                                }
                                if (l == placed.length) // no overlap
                                {
                                    if (d_min < lambda_min) {
                                        lambda_min = d_min;
                                        lambda[i] = 1 - d_min / unplaced[i];
                                        circle[i] = corners[c];
                                    }
                                }
                            }
                        }
                }

                // select the circle with maximal gain
                var lambda_max = -1e10;
                var i_max = -1;
                for (var i = 0; i != unplaced.length; i++) {
                    if (lambda[i] > lambda_max) {
                        lambda_max = lambda[i];
                        i_max = i;
                    }
                }

                // failure if no circle fits
                if (i_max == -1) break;

                // place the selected circle
                unplaced.splice(i_max, 1);
                placed.push(circle[i_max]);
            }

            // return all placed circles except the four bounding circles
            this.tmp_bounds = placed.splice(0, 4);
            return placed;
        },

        // find the smallest rectangle to fit all circles
        solve: function () {
            // compute total surface of the circles
            var surface = 0;
            for (var i = 0; i != this.circles.length; i++) {
                surface += Math.PI * Math.pow(this.circles[i], 2);
            }

            // set a suitable precision
            var limit = surface / 1000;

            var step = surface / 2;
            var res = [];
            while (step > limit) {
                var placement = this.compute.call(this, surface);
                //console.log ("placed",placement.length,"out of",this.circles.length,"for surface", surface);
                if (placement.length != this.circles.length) {
                    surface += step;
                }
                else {
                    res = placement;
                    this.bounds = this.tmp_bounds;
                    surface -= step;
                }
                step /= 2;
            }
            return res;
        }
    };

    var tooltip = d3.select("body")
        .append("div")
        .attr("class", "post-interests-tooltip");

    function commentsViz(expanded, staticInfoObj) {
        var width = 300,
            height = 300,
            data = [],
            r = function (d) { return d; },
            scale = function (d) { return r(d) / 50; },
            fontToRadiusRatio = 1.8,
            radiusToTitleFont = 0.15,
            radiusToSubTitleFont = 0.13;

        function commentsViz(context) {
            if (isNaN(width) || width == 'undefined' || isNaN(height)) {
                width = $('.chart-cont-q12-post').innerWidth();
            }
            height = $('.chart-cont-q12-post').innerHeight();
            if (!expanded && height > 355) {
                height = 355;
            } else if (expanded && height < 538) {
                height = 638;
            }

            var selection = context.selection ? context.selection() : context,
                svg = selection.append("svg")
                    .attr("width", width)
                    .attr("height", height),
                //.attr("viewBox", "0 0 350 250"),
                g = svg.append("g")
                    .attr("transform", "translate(0,0)")
                    .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
                circleRadius = (width > height ? height : width);


            data = data.sort(function (a, b) { return b.value - a.value });
            //console.log(data);
            var packer = new Packer(data.map(function (d) { return d.value; }), width / height);

            var list = packer.list.sort(function (a, b) { return b.r - a.r; });

            svg.attr("viewBox", "0 0 " + packer.w + " " + packer.h + "");
            g.attr("transform", "translate(" + packer.w / 2 + "," + packer.h / 2 + ")");

            var circleG = g.selectAll('.packer-list')
                .data(list)
                .enter()
                .append('g')
                .attr('transform', function (d) {
                    return 'translate(' + d.c.x + ',' + d.c.y + ')'
                });

            //circleG.append('circle').attr('r', function(d){ return d.r; }).attr('fill', 'red');

            circleG.append('text')
                //.attr("x", function(d){ return -((d.r * fontToRadiusRatio)/2); })
                .attr("y", function (d) { return (d.r * fontToRadiusRatio) / 4; })
                .attr("font-family", "FontAwesome")
                .style('font-size', function (d) { return (d.r * fontToRadiusRatio) + 'px'; })
                .style('stroke', 'transparent')
                .style('fill', staticInfoObj.colors.cloudColor)
                .style('text-anchor', 'middle')
                .text(function (d) { return '\uf075'; });

            var chkBrowser = jwtUtil.whichBrowser();


            var fo = circleG.append('foreignObject')
                .attr('x', function (d) { return -(d.r * 1.5) / 2; })
                .attr('y', function (d) {
                    var yCoord = -(d.r / 2);
                    if (chkBrowser.is_safari && d.r >= 15) {
                        yCoord = -(d.r * 1.5) / 2;
                    }
                    return yCoord;
                })
                .attr('width', function (d) { return d.r * 1.5; })
                .attr('height', function (d) {
                    var heightVal = d.r * 0.6;
                    if (chkBrowser.is_safari && d.r >= 15) {
                        heightVal = d.r * 1.5;
                    }
                    return heightVal;
                })
                .each(function (d, i) {
                    var div = d3.select(this).append('xhtml:div')
                        .attr('class', function () {
                            if (i > 5 && !expanded) {
                                return 'text-cont small-clouds';
                            } else {
                                return 'text-cont';
                            }
                        })
                        .style('text-align', 'center')
                        .style('display', function () {
                            if (i > 5 && !expanded) {
                                return 'block';
                            } else {
                                return 'block';
                            }
                        });
                    div
                        .on("mouseover", function (d) {
                            if ((d.r * radiusToTitleFont) < 2) {
                                var htmlTxt = '<span class="tooltip-topic" style="color:'+staticInfoObj.tooltipColors.textColor[0]+'">' + data[i].topic +'</span>'
                                    + '<span class="tooltip-subtopic" style="color:'+staticInfoObj.tooltipColors.textColor[1]+'">' + data[i].subtopic + '<br/>'
                                    + data[i].value + data[i].unit + '</span>';

                                tooltip.html(htmlTxt);
                                tooltip.style("visibility", "visible");
                            }
                        })
                        .on("mousemove", function () {
                            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
                        })
                        .on("mouseout", function () { return tooltip.style("visibility", "hidden"); });

                    div
                        .append('div')
                        .attr('class', 'text-topic')
                        .text(function () {
                            if ((d.r * radiusToTitleFont) < 2) {
                                return data[i].topic.substring(0, 4)+"...";
                            } else {
                                return data[i].topic;
                            }
                        })
                        .style('font-size', function () {
                            if ((d.r * radiusToTitleFont) < 2 && !expanded) {
                                return "2px";
                            } else {
                                return (d.r * radiusToTitleFont) + 'px';
                            }
                        })
                        .style('color', staticInfoObj.colors.textColor[0]);

                    div
                        .append('div')
                        .attr('class', 'text-subtopic')                        
                        .text(function () {
                            if ((d.r * radiusToTitleFont) < 2) {
                                return "...";
                            } else {
                                return data[i].subtopic;
                            }
                        })
                        .style('font-size', function (d, i) {
                            return (d.r * radiusToSubTitleFont) + 'px';
                        })
                        .style('color', staticInfoObj.colors.textColor[1])
                        ;
                    div
                        .append('div')
                        .attr('class', 'text-unit')                        
                        .text(function () {
                            if ((d.r * radiusToTitleFont) < 2) {
                                return "...";
                            } else {
                                return data[i].value + data[i].unit;
                            }
                        })
                        .style('font-size', function () { return (d.r * radiusToSubTitleFont) + 'px'; })
                        .style('color', staticInfoObj.colors.textColor[1])
                        ;

                    //update the layout position
                    d3.select(this)
                        .attr('y', function (d) {
                            var yCoord = -d.r / 2 + div.node().clientHeight / 4;
                            if (chkBrowser.is_safari && d.r >= 15) {
                                yCoord = -d.r / 2;
                            }
                            return yCoord;
                        })
                });



            /*circleG.append('text')
                //.attr("x", function(d){ return -((d.r * fontToRadiusRatio)/2); })
                //.attr("y", function(d){ return (d.r * fontToRadiusRatio)/4; })
                .attr("y", 0)
                //.attr("font-family","FontAwesome")
                .style('font-size', function(d) { return (d.r * radiusToFont) + 'px';} )
                //.style('stroke', 'transparent')
                .style('fill', '#487D7F')
                .style('text-anchor', 'middle')
                .text(function(d, i) { return data[i].topic; });
    
            circleG.append('text')
                //.attr("x", function(d){ return -((d.r * fontToRadiusRatio)/2); })
                .attr("y", 20 )
                //.attr("font-family","FontAwesome")
                .style('font-size', function(d) { return (d.r * radiusToFont) + 'px';} )
                //.style('stroke', 'transparent')
                .style('fill', 'white')
                .style('text-anchor', 'middle')
                .text(function(d, i) { return data[i].subtopic; });*/

        }

        commentsViz.width = function (_) {
            return arguments.length ? (width = _, commentsViz) : width;
        };

        commentsViz.height = function (_) {
            return arguments.length ? (height = _, commentsViz) : height;
        };

        commentsViz.data = function (_) {
            return arguments.length ? (data = _, commentsViz) : data;
        };

        commentsViz.r = function (_) {
            return arguments.length ? (r = _, commentsViz) : r;
        };

        commentsViz.scale = function (_) {
            return arguments.length ? (scale = _, commentsViz) : scale;
        };

        return commentsViz;
    };

    exports.speechDialogsViz = commentsViz;

    Object.defineProperty(exports, '__esModule', { value: true });

}));