'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
    .module('app', [
        'app.common',
        'app.components',
        'app.common.constants',
        'app.lib',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'toaster',
        'ui.bootstrap',
        'ngIdle'
    ])
    .config(function($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider,KeepaliveProvider, IdleProvider) {
        window.JWTConfig = { hashPrefix: "!" };
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix(window.JWTConfig.hashPrefix);
        $urlRouterProvider.otherwise('/');
        $httpProvider.interceptors.push('httpAuthInterceptor');
        IdleProvider.idle(5);
        IdleProvider.timeout(2700);
        KeepaliveProvider.interval(30);    
    }).run(['Idle', function(Idle) {
        Idle.watch();
    }]);