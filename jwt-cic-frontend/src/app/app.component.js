(function() {
'use strict';

  angular
    .module('app')
    .component('jwtApp', {
      template: '',
      abstract:true,
      controller: AppController,
      bindings: {

      }
    });

  AppController.$inject = [];

  function AppController() {
    this.$onInit = function(){
      this.message = 'Hello world with D3.js components';
    }
    
  }

})();