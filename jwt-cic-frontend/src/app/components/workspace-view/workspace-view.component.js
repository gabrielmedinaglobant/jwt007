(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtWorkspaceView', {
      templateUrl: 'app/components/workspace-view/workspace-view.html',
      controller: workspaceViewController,
      bindings: {
      }
    });
})();
