 angular
    .module('app.common')
    .factory('workspaceViewService', ['commonService','jwtWorkspaceViewConstant','sessionService','userService',function(commonService,jwtWorkspaceViewConstant,sessionService,userService) {
        
        return angular.extend({
            cnst:jwtWorkspaceViewConstant
        },commonService,sessionService,userService);
        
    }]);

