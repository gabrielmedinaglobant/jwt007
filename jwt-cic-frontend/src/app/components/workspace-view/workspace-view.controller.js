'use strict';

workspaceViewController.$inject = ['$log', '$rootScope' ,'$scope', '$state', 'workspaceViewService'];

function workspaceViewController($log, $rootScope ,$scope, $state, workspaceViewService) {
    var $ctrl = this;
    $log.debug('components:workspace-view');

    if(!workspaceViewService.isLoggedIn()){
        $state.go('login');
    }
    
    $ctrl.model = {workspaceParams:$state.params.id,workspaceId:0};
    $ctrl.dataModel = {workspace:{}};
    $ctrl.viewModel = {showError:true};

    $scope.$on('IdleTimeout', function() {
        workspaceViewService.clearSessionData();
        window.location.assign(workspaceViewService.getConstant('AUTH_APP'));
        window.close();
    });      
    
    
    $ctrl.$onInit = function() {
        var c = atob($ctrl.model.workspaceParams).split('$$');
        if(c.length == 2){
            if(_.first(c) == 'workspace-name'){
                if(parseInt(c[1]) >0){
                    $ctrl.model.workspaceId = c[1];
                    $ctrl.viewModel.showError = false;
                    loadWorkspace();
                }
            }
        }
    };   
    
    function loadWorkspace(){
        var d = workspaceViewService.getSessionData('WORSPACES');
        if(_.isNull(d)){
        }else{
            if(d.length > 0){
                $ctrl.dataModel.list = angular.fromJson(d);
                _.each($ctrl.dataModel.list,function(o,i){
                    if(o.id == $ctrl.model.workspaceId){
                        if(o.charts.length > 0){
                            $ctrl.dataModel.workspace = o;
                        }
                    }
                });
            }           
        }
    };
    
    $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
        if(toState.name != 'login'){
            if(!workspaceViewService.isLoggedIn()){
                $state.go('login');
            }
        }
        if(fromState.name == 'workspace' && toState.name != 'login'){
            event.preventDefault();   
        }
        
    });    
};

angular.module('app.components').directive('jwtcompilehtml', function($compile) {
      return { 
        restrict: 'EA', 
        scope: {
            jwtHtmlTemplate: "="
        },
        replace: true,
        link: function(scope, element, attrs) {
            var template = $compile(scope.jwtHtmlTemplate)(scope);
            element.replaceWith(template);               
        }
      }
});

