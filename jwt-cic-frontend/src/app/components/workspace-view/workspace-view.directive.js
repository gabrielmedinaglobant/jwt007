(function() {
'use strict';

  angular
    .module('app.components')
    
    .directive("customWorkspace", function ($compile) {
      return {
        restrict: "A",
        link: function (scope, element, attrs) {
          var customWorkspace = attrs.customWorkspace;
          if (!customWorkspace) return;
          var dynamicElem = angular.element("<" + customWorkspace + "></" + customWorkspace + ">");
          element.replaceWith($compile(dynamicElem)(scope));
        }
      }
  })
})();
