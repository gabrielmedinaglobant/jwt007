projectSearchController.$inject = ['$log', '$scope', 'projectSearchService'];

function projectSearchController($log, $scope, projectSearchService) {
    var $ctrl = this;
    $scope.model = { project: '' };

    $log.debug('components:project-search');

    $ctrl.getLocation = function(val) {
        var params = { name: val };
        if (angular.isDefined($ctrl.client)) {
            if (!_.isEmpty($ctrl.client)) {
                params["client-name"] = $ctrl.client.name;
            }
        }
        if (angular.isDefined($ctrl.brand)) {
            if (!_.isEmpty($ctrl.brand)) {
                params["brand-name"] = $ctrl.brand.name;
            }
        }
        return projectSearchService.getData(projectSearchService.getConstant('PROJECT_LIST'), params).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:project-search:error:getlist');
        })
    };
    $scope.$watch("$ctrl.clearsearch", function(n, o) {
        if (n == true) {
            $scope.model.project = "";
        }
    })
    $scope.$watch("model.project", function(n, o) {
        if (n == '') {
            $ctrl.onProjectSelect({ project: { "name": "$%$%$" } });
        }
    })

    $ctrl.onClientSearch = function(keyEvent) {
        if (keyEvent.which === 13) { $ctrl.onProjectSelect({ project: $scope.model.project ? $scope.model.project : {} }); }

    }


    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        $ctrl.onProjectSelect({ project: $item });
    }
}