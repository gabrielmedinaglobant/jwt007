(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtProjectSearchConstant', {
            PROJECT_LIST: {
                API: '/projects',
                PREFIX: true
            }

        });
})();