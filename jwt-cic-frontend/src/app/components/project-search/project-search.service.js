 angular
    .module('app.common')
    .factory('projectSearchService', ['commonService','jwtProjectSearchConstant',function(commonService,jwtProjectSearchConstant) {
        
        return angular.extend({
            cnst:jwtProjectSearchConstant
        },commonService);
        
    }]);

