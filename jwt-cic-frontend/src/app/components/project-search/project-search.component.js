(function() {
    'use strict';

    angular
        .module('app.components')
        .component('jwtProjectSearch', {
            templateUrl: 'app/components/project-search/project-search.html',
            controller: projectSearchController,
            bindings: {
                client: '<',
                clearsearch: '<',
                brand: '<',
                project: '<',
                onProjectSelect: '&'
            }
        });
})();