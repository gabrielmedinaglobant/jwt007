(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtLogout', {
      templateUrl: 'app/components/logout/logout.html',
      controller: logoutController
    });
})();

angular
    .module('app.common')
    .factory('jwtLogoutService', ['commonService','userService',function(commonService,userService) {
        return angular.extend({
            cnst:{},
        },commonService,userService);
}]);

logoutController.$inject = ['jwtLogoutService'];
function logoutController(jwtLogoutService) {
    var $ctrl = this;
        jwtLogoutService.clearSessionData();
        window.location.assign(jwtLogoutService.getConstant('AUTH_APP'));
}



