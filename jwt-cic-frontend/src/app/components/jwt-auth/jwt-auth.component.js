(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtAuth', {
      templateUrl: 'app/components/jwt-auth/jwt-auth.html',
      controller: jwtAuthController,
    });

jwtAuthController.$inject = ['$state','jwtAuthService'];
function jwtAuthController($state,jwtAuthService){
    if(angular.isDefined($state.params.token)){
        jwtAuthService.setUserData($state.params.token);
        $state.go("app.discover");
    }else{
        alert('Invalid token');
        window.location.assign(jwtAuthService.getConstant('AUTH_APP'));
    }
}
    
})();
