workspaceSelectController.$inject = ['$log', '$rootScope','$scope', 'workspaceSelectService','$state'];

function workspaceSelectController($log, $rootScope ,$scope, workspaceSelectService, $state) {
    var $ctrl = this;
    $log.debug('components:workspace-select');
    $ctrl.dataModel = {list:[]};
    $ctrl.model = {screenId:0,screen:{}};
    $ctrl.viewModel = {canSelect:true,showUl:false};

    if($state.current.name == "infegy"){
        $ctrl.selectScreen = "Assign to Screen";
    }else{
      $ctrl.selectScreen = "Select Screen";
    }
    $rootScope.$on(workspaceSelectService.getConstant('EVENT_NAME').WORKSPACE.ADDED,function(e,d){
        loadWorkspaces();
    });

    $rootScope.$on(workspaceSelectService.getConstant('EVENT_NAME').WORKSPACE.DELETED,function(e,d){
        $ctrl.model = {screenId:0,screen:{}};
        $ctrl.viewModel = {canSelect:true};
    });

    $ctrl.defaultScreenSelect = function($event){
        $event.stopPropagation();
    }
    
    $ctrl.onScreenSelect = function(option){
        if(!option.isUsed){
            $ctrl.model.screenId = option.id;
            $ctrl.viewModel.canSelect = false;
            _.each($ctrl.dataModel.list,function(o,i){
                if(o.id == $ctrl.model.screenId ){
                    o.isUsed = true;
                    $ctrl.model.screen = o;
                    o.charts.push(workspaceSelectService.getConstant('CHART_COMPONENTS')[$ctrl.jwtChartId]);
                }
            });
            workspaceSelectService.setSessionData('WORSPACES',angular.toJson($ctrl.dataModel.list));
            raiseEvent();
        }
    };

    $ctrl.$onInit = function() {
        loadWorkspaces();
        $ctrl.viewModel.pinningMode = $ctrl.pinningMode;
    };

    $ctrl.toggleSelect = function() {
        $ctrl.viewModel.showUl = !$ctrl.viewModel.showUl;
        $ctrl.onSelectExpanded({ state: $ctrl.viewModel.showUl });
        if($ctrl.viewModel.showUl){
            $rootScope.$broadcast(workspaceSelectService.getConstant('EVENT_NAME').WORKSPACE.EXPANDED,{id:$ctrl.jwtChartId});
        }
    };

    $(document).click(function(e){
     if($(e.target).is("#chartselect"+$ctrl.jwtChartId) || $(e.target).closest("#chartselect"+$ctrl.jwtChartId).length){

     }
    else {
        $ctrl.viewModel.showUl = false;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

    })

    $ctrl.onRemoveSelection = function(){
        _.each($ctrl.dataModel.list,function(o,i){
            if(o.id == $ctrl.model.screenId ){
                o.isUsed = false;
                o.charts = [];
            }
        });
        $ctrl.model.screenId = 0;
        $ctrl.model.screen = {};
        $ctrl.viewModel.canSelect = true;
        workspaceSelectService.setSessionData('WORSPACES',angular.toJson($ctrl.dataModel.list));
        raiseEvent();
    };

    function raiseEvent(){
        $rootScope.$broadcast(workspaceSelectService.getConstant('EVENT_NAME').WORKSPACE.ADDED,{});
    };

    function loadWorkspaces(){
        var d = workspaceSelectService.getSessionData('WORSPACES');
        if(_.isNull(d)){
        }else{
            if(d.length > 0){
                $ctrl.dataModel.list = angular.fromJson(d);
                _.each($ctrl.dataModel.list,function(o,i){
                    if(o.charts.length > 0){
                        if(_.first(o.charts).id == $ctrl.jwtChartId){
                            $ctrl.model.screen = o;
                            $ctrl.model.screenId = o.id;
                            $ctrl.viewModel.canSelect = false;
                        }
                    }
                });
            }
        }
    };

};
