(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtWorkspaceSelect', {
      templateUrl: 'app/components/workspace-select/workspace-select.html',
      controller: workspaceSelectController,
      bindings: {
        jwtChartId: '<',
        pinningMode: '<',
        onSelectExpanded: '&'  
      }
    });
})();
