 angular
    .module('app.common')
    .factory('workspaceSelectService', ['commonService','jwtWorkspaceSelectConstant','sessionService',function(commonService,jwtWorkspaceSelectConstant,sessionService) {
        
        return angular.extend({
            cnst:jwtWorkspaceSelectConstant
        },commonService,sessionService);
        
    }]);

