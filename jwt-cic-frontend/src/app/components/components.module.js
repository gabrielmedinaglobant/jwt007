'use strict';

/**
 * @ngdoc overview
 * @name app.components
 * @description
 * # app.components
 *
 * Components module of the application.
 */
angular
  .module('app.components', []);
