(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtMainView', {
      templateUrl: 'app/components/main-view/main-view.html',
      controller: appMainViewController
    });
})();

appMainViewController.$inject = ['$scope', '$state', '$rootScope'];

function appMainViewController($scope, $state, $rootScope){
  var $ctrl = this;
  $ctrl.viewModel = {showHeader:true,showWorkspace:true};

  function updateView(){
      if(_.contains(['app.discover','app.evaluate'],$state.current.name)){
            $ctrl.viewModel.showHeader = true;
      }else{
            $ctrl.viewModel.showHeader = false;
      }

      if(_.contains(['app.discover','app.evaluate','app.infegy','app.googlehottrend'],$state.current.name)){
            $ctrl.viewModel.showWorkspace = true;
      }else{
            $ctrl.viewModel.showWorkspace = false;
      }
  }

  updateView();
  
  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      updateView();
  });

}
