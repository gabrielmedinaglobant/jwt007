(function() {
'use strict';

  angular
    .module('app.components')
    .constant('jwtWorkspaceListConstant', {
      BRAND_LIST :{
          API:'/brands',
          PREFIX:true
      }
      
    });
})();
