 angular
    .module('app.common')
    .factory('workspaceListService', ['commonService','jwtWorkspaceListConstant','sessionService',function(commonService,jwtWorkspaceListConstant,sessionService) {
        
        return angular.extend({
            cnst:jwtWorkspaceListConstant
        },commonService,sessionService);
        
    }]);

