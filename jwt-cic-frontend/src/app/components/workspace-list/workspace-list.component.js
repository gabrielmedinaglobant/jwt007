(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtWorkspaceList', {
      templateUrl: 'app/components/workspace-list/workspace-list.html',
      controller: workspaceListController,
      bindings: {
      }
    });
})();
