workspaceListController.$inject = ['$log', '$rootScope' ,'$scope', '$location' ,'workspaceListService'];

function workspaceListController($log, $rootScope ,$scope, $location ,workspaceListService) {
    var $ctrl = this;
    console.log('in:application:workspace-list');

    $ctrl.viewObj = jwtUtil.getViewport();
    $ctrl.margin = {'left': 50, 'top': 50};
    $ctrl.model = {newWorkspace:{id:0,name:"Screen ",charts:[],isUsed:false},showWorkspace:"hide",
                    toggleArrowClass:"icon-jwt-uparrow-bdl",atleastOneScreenAssigned :false};
    $ctrl.dataModel = {list:[]};
    $ctrl.indexValue = 0;

    $ctrl.toggleWorkspace = function(){
      if($ctrl.model.showWorkspace == "hide"){
        $ctrl.model.showWorkspace = "show";
        $ctrl.model.toggleArrowClass = "icon-jwt-downarrow-dbl";
      }else{
        $ctrl.model.showWorkspace = "hide";
        $ctrl.model.toggleArrowClass = "icon-jwt-uparrow-bdl";
      }
    };


    $ctrl.addNewWorkspace = function(){
        var tempWorkspace = angular.copy($ctrl.model.newWorkspace);
        tempWorkspace.id = 1;
        if(_.last($ctrl.dataModel.list)){
         tempWorkspace.id = (_.last($ctrl.dataModel.list)).id + 1;
        }
        tempWorkspace.name = tempWorkspace.name + tempWorkspace.id;
        $ctrl.dataModel.list.push(angular.copy(tempWorkspace));
        workspaceListService.setSessionData('WORSPACES',angular.toJson($ctrl.dataModel.list));
        $ctrl.model.showWorkspace = "show";
        $ctrl.model.toggleArrowClass = "icon-jwt-downarrow-dbl";
        raiseEvent();
    }

    $ctrl.deleteWorkspace = function(item){
        $ctrl.dataModel.list = _.without($ctrl.dataModel.list,item);
        workspaceListService.setSessionData('WORSPACES',angular.toJson($ctrl.dataModel.list));
        $rootScope.$broadcast(workspaceListService.getConstant('EVENT_NAME').WORKSPACE.DELETED,{});
        raiseEvent();
    }

    $ctrl.launchWorkspace = function(item){
        var width = ($ctrl.viewObj.width-$ctrl.margin.left);
        var height = ($ctrl.viewObj.height-$ctrl.margin.top);
        var url = '';
        if(item.charts.length >0){
            if(item.charts[0].type == 'EXTERNAL'){
                url = item.charts[0].html;
                window.open(url, '_blank','width='+width+', height='+height);
            }else{
                //url = '/#'+window.JWTConfig.hashPrefix+'/workspace/'+btoa("workspace-name$$"+item.id);
                url = workspaceListService.getConstant('APP_URL_PREFIX')+window.JWTConfig.hashPrefix+'/workspace/'+btoa("workspace-name$$"+item.id);
                window.open(url, '_blank','width='+width+', height='+height);
            }
        }
    }

    $ctrl.launcAllhWorkspaces = function(){
        _.each($ctrl.dataModel.list,function(o,i){
            setTimeout(function(){
                $ctrl.launchWorkspace(o);    
            },i*2000);
            
        })
    }

    $rootScope.$on(workspaceListService.getConstant('EVENT_NAME').WORKSPACE.ADDED,function(e,d){
        loadWorkspaces();
    });

    $rootScope.$on(workspaceListService.getConstant('EVENT_NAME').WORKSPACE.EXPANDED,function(e,d){
        if(_.contains(['googleTrend','infegy'],d.id)){
            $ctrl.model.showWorkspace = "hide";
            $ctrl.model.toggleArrowClass = "icon-jwt-uparrow-bdl";
        }
    });

    function loadWorkspaces(){
        var d = workspaceListService.getSessionData('WORSPACES');
        if(_.isNull(d)){
        }else{
            if(d.length > 0){
                $ctrl.dataModel.list = angular.fromJson(d);
            }
        }
        $ctrl.model.atleastOneScreenAssigned = _.chain($ctrl.dataModel.list)
                .pluck('isUsed')
                .uniq()
                .contains(true)
                .value();
    }

    function raiseEvent(){
        $rootScope.$broadcast(workspaceListService.getConstant('EVENT_NAME').WORKSPACE.ADDED,{});
    }

    function init(){
        loadWorkspaces();
    }

    $scope.onEditScreenName = function(evt , index) {
        var screen = evt.target;
        var screenLabel = screen.innerText;
          screen.setAttribute("contenteditable", true);
		      screen.setAttribute("target", 0);
          screen.focus();
          $ctrl.indexValue = index;
          if(screenLabel.trim()=='Invalid Name'){
            screen.innerText = '';
            $(screen).removeClass('invalid');
          }
      };

      $scope.updateScreenName = function(evt){
        var screen = evt.target;
        var screenLabel = screen.innerText;
        if(screenLabel.trim().length > 0 && screenLabel.trim()!='Invalid Name'){
           $(screen).removeClass('invalid');        
            //console.log("value after blur" + $ctrl.dataModel.list[$ctrl.indexValue]);
            $ctrl.dataModel.list[$ctrl.indexValue].name = screenLabel;
            workspaceListService.setSessionData('WORSPACES',angular.toJson($ctrl.dataModel.list));
            raiseEvent();
        }else{
           screen.innerText = 'Invalid Name';
           $(screen).addClass('invalid');
           setTimeout(function(){
                $(screen).removeClass('invalid');
                $(screen).fadeOut(500, function(){
                    screen.innerText =  $ctrl.dataModel.list[$ctrl.indexValue].name;
                }).fadeIn(200);
           }, 1000);
        }
      }
    init();
}
