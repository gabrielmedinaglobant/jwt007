(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtMlineChart', {
      template: '<h1>Multi Line</h1>{{::$ctrl.message}}<div class="visualisation"></div>',
      controller: MlineChartController,
      bindings: {
        data: '<'
      }
    });


  MlineChartController.$inject = ['$log','$element','D3Service', '$discoverQ1Service'];

  function MlineChartController($log,$element,d3, disq1service) {
    var $ctrl = this;

    this.$onInit = function(){
      this.message = 'Random example of multi-line-chart component';
    }

    this.$postLink = function(){
      $log.info(d3);
      $log.info($element);

      // TODO: set watchers for bindings,
      // TODO: every time data is updated, D3 would be rendered


      $log.info($ctrl.data);

      // Set the dimensions of the canvas / graph
      var margin = {top: 30, right: 20, bottom: 30, left: 50},
        width = 600 - margin.left - margin.right,
        height = 270 - margin.top - margin.bottom;

// Parse the date / time
      var parseDate = d3.timeParse("%b %Y");

// Set the ranges
      var x = d3.scaleTime().range([0, width]);
      var y = d3.scaleLinear().range([height, 0]);

// Define the line
      var priceline = d3.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.price); });

// Adds the svg canvas
      var svg = d3.select(".visualisation")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

      // Get the data
      var data = disq1service.getLineData();


        data.forEach(function(d) {
          d.date = parseDate(d.date);
          d.price = +d.price;
        });

        // Scale the range of the data
        x.domain(d3.extent(data, function(d) { return d.date; }));
        y.domain([0, d3.max(data, function(d) { return d.price; })]);

        // Nest the entries by symbol
        var dataNest = d3.nest()
          .key(function(d) {return d.symbol;})
          .entries(data);

        // set the colour scale
        var color = d3.scaleOrdinal(d3.schemeCategory10);

        // Loop through each symbol / key
        dataNest.forEach(function(d) {

          svg.append("path")
            .attr("class", "line")
            .style("stroke", function() { // Add the colours dynamically
              return d.color = color(d.key); })
            .attr("d", priceline(d.values));

        });

        // Add the X Axis
        svg.append("g")
          .attr("class", "axis")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x));

        // Add the Y Axis
        svg.append("g")
          .attr("class", "axis")
          .call(d3.axisLeft(y));

      //});


    }

  }

})();
