(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtSidebar', {
      templateUrl: 'app/components/sidebar/sidebar.html',
      controller: sidebar,
      bindings: {
        data: '<'
      }
    });

  sidebar.$inject = ['$scope','jwtAppConstant', '$log','$rootScope','$state', '$stateParams', 'sidebarService'];

  function sidebar($scope,jwtAppConstant,$log,$rootScope,$state,$stateParams,sidebarService) {
    var $ctrl = this;
    var rtime, timeout = false, delta = 200;
    var sidebartimeOut1,sidebartimeOut2;
      
    $ctrl.$onDestroy = function(){
      clearTimeout(sidebartimeOut1);
      clearTimeout(sidebartimeOut2);
    }
    
    $scope.$on('IdleTimeout', function() {
        $ctrl.logout();
    });      
      
    // Assigning a state properties to rootscope
    $rootScope.state = $state;
    $rootScope.stateParams = $stateParams;

    // Binding window resize
    // $(window).resize(function () {
    //     rtime = new Date();
    //     console.log('vop2');
    //     if (timeout === false) {
    //         timeout = true;
    //         sidebartimeOut1 = setTimeout($ctrl.resizeend, delta);
    //     }
    // });

    //  /* ** Re rendering the chart on window resize ** */
    // $ctrl.resizeend = function() {
    //     if (new Date() - rtime < delta) {
    //         sidebartimeOut2 = setTimeout($ctrl.raiseEvent, delta);
    //     } else {
    //         timeout = false;
    //         $ctrl.raiseEvent();
    //         console.log('vop');
    //     }
    // }

    $ctrl.raiseEvent = function(){
        $rootScope.$broadcast(jwtAppConstant.EVENT_NAME.CHART.RESIZED,{'resized':true});
        timeout = false;
    }
    var lazyLayout = _.debounce($ctrl.raiseEvent, 400);
    $(window).resize(lazyLayout);


    $ctrl.userCurrentState = $state.current;
    $ctrl.dataModel = {menuOptions:[]};
    $ctrl.dataModel.allMenuOptions = [{name:'Home',class:"icon-jwt-home",state:['app.discover','app.evaluate']},{name:'Notification',class:"icon-jwt-user"},
                                      {name:'Workspaces',class:"icon-jwt-star"},{name:'Infegy',class:"icon-jwt-bulb",state:['app.infegy']},
                                      {name:'Orbit',class:"icon-jwt-timer"},{name:'Twitter',class:"icon-jwt-value"},
                                      {name:'GoogleHotTrend',class:"icon-jwt-google-hot-trend"},
                                      {name:'Crowdtangle',class:"icon-jwt-message"},{name:'Google',class:"icon-jwt-google",state:['app.googlehottrend']},
                                      {name:'ClientMapping',class:"icon-jwt-flag",state:['app.clients']},{name:'Help',class:"icon-jwt-info logout"},
                                      {name:'Logout',class:"icon-jwt-logout logout",state:['logout']}

                                     ];

    $ctrl.dataModel["ROLE_ANALYST"] = ['Home','Workspaces','Infegy','GoogleHotTrend','ClientMapping','Logout'];
    $ctrl.dataModel["ROLE_STRATEGIC"] = $ctrl.dataModel["ROLE_DUMMY"] = $ctrl.dataModel["ROLE_RANDOM1"]
        = $ctrl.dataModel["ROLE_RANDOM2"]= $ctrl.dataModel["ROLE_RANDOM2"]
        = ['Home','Workspaces','Logout']

      $log.info('in:application:sidebar');
      $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
        if(toState.name != 'login'){
            if(!sidebarService.isLoggedIn()){
               window.location.assign(sidebarService.getConstant('AUTH_APP'));
                //$state.go('login');
            }
        }
        $ctrl.userCurrentState = toParams;
    });

    if(!sidebarService.isLoggedIn()){
        //$state.go('login');
        window.location.assign(sidebarService.getConstant('AUTH_APP'));
    }

    $ctrl.setActive = function(cstate){
        _.each($ctrl.dataModel.currentUserMenu,function(o,i){
            if(_.contains(o.state,cstate)){
                $ctrl.dataModel.currentUserMenu[i].isSelected = true;
            }else{
                $ctrl.dataModel.currentUserMenu[i].isSelected = false;
            }
        });
    }

    $ctrl.getCurrentUserMenu = function(){
        $ctrl.dataModel.currentUserMenu =[];
        var currentRole = sidebarService.getUserRole();
        if(currentRole){
            $ctrl.dataModel.currentUserMenu = _.filter($ctrl.dataModel.allMenuOptions,function(o,i){
                return _.contains($ctrl.dataModel[currentRole],o.name);
            });
            $ctrl.setActive($state.current.name);
        }
    }
    $ctrl.getCurrentUserMenu();


    $ctrl.logout = function(){
        sidebarService.clearSessionData();
        window.location.assign(sidebarService.getConstant('AUTH_APP'));
    }

    $ctrl.changeState = function(state, obj){
        if(state.name == 'GoogleHotTrend'){
            var hotTrendUrl = jwtAppConstant.googleHotTrend.apiUrl;
            window.open(hotTrendUrl, '_blank','width='+jwtUtil.getViewport().width+', height='+jwtUtil.getViewport().height);
        }else{
            if(state.state){
                if(state.name == 'Logout'){
                    $ctrl.logout();
                }else{
                    $ctrl.setActive(state.state[0]);
                    $state.go(state.state[0]);
                }
            }
        }                
    };

    $scope.currentState = function(name){
      return $state.is(name);
    }

  }


})();
