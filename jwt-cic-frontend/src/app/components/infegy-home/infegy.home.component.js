(function() {
'use strict';
  angular
    .module('app.components')
    .config(function($sceProvider) {
        $sceProvider.enabled(false);
     })
    .component('jwtInfegyHome', {
      templateUrl: 'app/components/infegy-home/infegy.home.html',
      controller: infegyHomeController,
      bindings: {
        data: '<'
      }
    });
})();
