/* --------- Controller for Infegy Home Screen  --------------*|
 |--------------------------------------------------------------*/

  infegyHomeController.$inject = ['$scope','jwtAppConstant','$log','$element','$sce'];

function infegyHomeController($scope,jwtAppConstant,$log,$element,$sce) {
  $scope.showWorkspace = false;

  var $ctrl = this;
  $ctrl.showExpanded = false;
  $ctrl.showWorkspaceSelect = true;
  $ctrl.isActive = false;
  $ctrl.selectScreen = "Assign Screen";

    $scope.frameName = 'infegyFrame';
    $scope.frameUrl = $sce.trustAsResourceUrl('http://atlas.infegy.com/login');

    var viewportHgt = jwtUtil.getViewport().height;
    $("#infegyFrame").height(viewportHgt-51);
    //console.log("height is : " + viewportHgt );
    $(window).resize(function(){
       var viewportHgt = jwtUtil.getViewport().height;
        $("#infegyFrame").height(viewportHgt-51);
    });

    //console.log($(".icon-jwt-bulb").offset());
    var topValue = $(".icon-jwt-bulb").offset().top + 34 ;
    var leftValue = $(".icon-jwt-bulb").width() - 13;


    //$(".masonryelementclass").css({ top: topValue });
    $( "#infegyHomeSelect" ).css({"top":topValue,"position":"fixed","left" : leftValue});

   // $('#iframealternative').load('http://www.google.co.in');

}
