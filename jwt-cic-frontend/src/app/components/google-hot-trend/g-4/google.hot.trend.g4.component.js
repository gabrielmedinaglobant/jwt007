(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtGoogleHotTrendG4', {
      controller:GoogleHotTrendG4Controller,
      templateUrl: 'app/components/google-hot-trend/g-4/google.hot.trend-g4.html',
          bindings: {
              data: '<',
              isWorkspace:'<',
              showWorkspaceSelect:'<'
          }      
    });

})();

GoogleHotTrendG4Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$timeout', '$log', '$element'];

function GoogleHotTrendG4Controller($scope, jwtAppConstant, $rootScope, $timeout, $log, $element) {
    var $ctrl = this;
    
    var frameHtml = '<iframe id="trends-widget-4" src="https://trends.google.com:443/trends/embed/explore/TIMESERIES?req=%7B%22comparisonItem%22%3A%5B%7B%22keyword%22%3A%22%2Fm%2F0gwzvs1%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22today%205-y%22%7D%2C%7B%22keyword%22%3A%22%2Fm%2F02y1vz%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22today%205-y%22%7D%2C%7B%22keyword%22%3A%22google%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22today%205-y%22%7D%5D%2C%22category%22%3A0%2C%22property%22%3A%22%22%7D&amp;tz=-330&amp;eq=q%3D%252Fm%252F0gwzvs1%2C%252Fm%252F02y1vz%2Cgoogle" width="99%" frameborder="0" scrolling="0" style="border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.117647) 0px 0px 2px 0px, rgba(0, 0, 0, 0.239216) 0px 2px 2px 0px; height: 384px;"></iframe> ';
    
    $ctrl.refreshView = function(){
        $element.find('#trends-widget-g4-collapse').empty();        
        $element.find('#trends-widget-g4-collapse').html(frameHtml);        
    };

    $scope.masonryelementclass = "masonry-brick";

    $ctrl.$onInit = function () {
        $ctrl.showLoader();
        var drawChart = function () {
            isWorkspaceElement();
        }
        $timeout(drawChart, 100);
    };

    function isWorkspaceElement() {
        if ($ctrl.isWorkspace == true) {
            $ctrl.showExpanded = true;
            $scope.masonryelementclass = "masonry-brick big-tile popped-window";
            var newWindWid = jwtUtil.getViewport().width;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.85);
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            $ctrl.redrawChart();
        } else {
            if ($ctrl.showExpanded)
                $scope.masonryelementclass = "masonry-brick big-tile";
            $ctrl.showExpanded = false;
            $ctrl.redrawChart();
        }
    }

    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
        $ctrl.initRedrawChart(args.resized);
    });

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1]) {
            var elem = $('#trends-widget-g4-collapse');
            $ctrl.showLoader();
            $ctrl.showExpanded = !$ctrl.showExpanded;
            $timeout(function () {
                $scope.expandTile(data.event, elem);
            }, 300);
        }
    });

    /* ** Show loader ** */
    $ctrl.showLoader = function () {
        var elem = $('#trends-widget-g4-collapse');
        elem.empty();
        elem.html('<div class="loader"></div>');
    }

    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function (resize) {
        if (resize) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.showExpanded);
            $ctrl.redrawChart();
        }
    }

    /* ** Function for expanding the card ** */
    $scope.expandTile = function (e, elem) {
        $ctrl.showLoader();
        var targetElement = $(elem).parents('.masonry-brick');
        targetElement.siblings().removeClass('big-tile');
        if (targetElement.hasClass('big-tile')) {
            $ctrl.showExpanded = false;
        } else {
            $ctrl.showExpanded = true;
        }
        targetElement.toggleClass('big-tile');
        $ctrl.redrawChart();
    };

    $ctrl.redrawChart = function () {
        $ctrl.refreshView();
    };
}