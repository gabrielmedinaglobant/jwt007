(function () {
    'use strict';

    angular
        .module('app.components')
        .component('jwtGoogleHotTrendG1', {
            controller: GoogleHotTrendG1Controller,
            templateUrl: 'app/components/google-hot-trend/g-1/google.hot.trend-g1.html',
            bindings: {
                data: '<',
                isWorkspace: '<',
                showWorkspaceSelect: '<',
                jwtChartId: '<'
            }
        });

})();

GoogleHotTrendG1Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$timeout', '$log', '$element'];

function GoogleHotTrendG1Controller($scope, jwtAppConstant, $rootScope, $timeout, $log, $element) {
    var $ctrl = this;

    var frameHtml = '<iframe id="trends-widget-1" src="https://trends.google.co.uk:443/trends/embed/explore/TIMESERIES?req=%7B%22comparisonItem%22%3A%5B%7B%22keyword%22%3A%22debbie%20schlussel%22%2C%22geo%22%3A%22US%22%2C%22time%22%3A%22now%207-d%22%7D%5D%2C%22category%22%3A0%2C%22property%22%3A%22%22%7D&amp;tz=-330&amp;eq=date%3Dnow%25207-d%26geo%3DUS%26q%3Ddebbie%2520schlussel"   width="99%" frameborder="0" scrolling="0" style="border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.117647) 0px 0px 2px 0px, rgba(0, 0, 0, 0.239216) 0px 2px 2px 0px; height: 384px;"></iframe>';

    $ctrl.refreshView = function () {
        $element.find('#trends-widget-g1-collapse').empty();
        $element.find('#trends-widget-g1-collapse').html(frameHtml);
    };

    $scope.masonryelementclass = "masonry-brick";

    $ctrl.$onInit = function () {
        $ctrl.showLoader();
        var drawChart = function () {
            isWorkspaceElement();
        }
        $timeout(drawChart, 100);
    };

    function isWorkspaceElement() {
        if ($ctrl.isWorkspace == true) {
            $ctrl.showExpanded = true;
            $scope.masonryelementclass = "masonry-brick big-tile popped-window";
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.85);
            newWindWid = Math.round(parseInt(newWindWid) * 0.70);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
            if (poppedContWidth < 650) {
              poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            $ctrl.redrawChart(newWindWid, newWindHgt);
        } else {
            if($ctrl.showExpanded)
                $scope.masonryelementclass = "masonry-brick big-tile";

            var width = $('#trends-widget-g1-collapse').width();
            $ctrl.showExpanded = false;
            $ctrl.redrawChart(width, 360);
        }
    }

    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
        $ctrl.initRedrawChart(args.resized);
    });

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1]) {
            var elem = $('#trends-widget-g1-collapse');
            $ctrl.showLoader();
            $ctrl.showExpanded = !$ctrl.showExpanded;
            $timeout(function () {
                $scope.expandTile(data.event, elem);
            }, 300);
        }
    });

    /* ** Show loader ** */
    $ctrl.showLoader = function () {
        var elem = $('#trends-widget-g1-collapse');
        elem.empty();
        elem.html('<div class="loader"></div>');
    }

    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function (resize) {
        if (resize) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.showExpanded);
            $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight);
        }
    }

    /* ** Function for expanding the card ** */
    $scope.expandTile = function (e, elem) {
        e.stopPropagation();
        e.preventDefault();

        var targetElement = $(elem).parents('.masonry-brick');

        targetElement.siblings().removeClass('big-tile');
        if (targetElement.hasClass('big-tile')) {
            $ctrl.showExpanded = false;
        } else {
            $ctrl.showExpanded = true;
        }
        targetElement.toggleClass('big-tile');
        var nWidth = Math.round(targetElement.innerWidth());
        var nHeight = Math.round(targetElement.innerHeight());
        if (!$scope.expanded) {
            nHeight = (nHeight > 360) ? 360 : nHeight;
        }

        $ctrl.redrawChart(nWidth, nHeight);

    };

    $ctrl.redrawChart = function (w, h) {
        $ctrl.refreshView();
    };

}
