(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtGoogleHotTrendHome', {
      controller: googleHotTrendHome,
      templateUrl: 'app/components/google-hot-trend/home/google.hot.trend.html',
      bindings: {
        data: '<',
        isWorkspace: '<'
      }
    });

  googleHotTrendHome.$inject = ['$scope', '$rootScope', '$timeout', 'jwtAppConstant'];

  function googleHotTrendHome($scope, $rootScope, $timeout, jwtAppConstant) {
    var $ctrl = this;
    $ctrl.viewModel = { showWorkspaceSelect: true };

    $ctrl.showWorkspace = false;

    $ctrl.showExpanded = false;
    $ctrl.showWorkspaceSelect = true;
    $ctrl.isActive = true;

    $ctrl.$onInit = function () {
      if ($ctrl.isWorkspace) {
        $ctrl.viewModel.showWorkspaceSelect = false;
      }
    };

    var cc = $(".icon-jwt-google").offset();
    if (cc) {
      var topValue = $(".icon-jwt-google").offset().top + 34;
      var leftValue = $(".icon-jwt-google").width() - 13;
      $("#googleHotTrendSelect").css({ "top": topValue, "position": "fixed", "left": leftValue, "z-index": 10 });
    }

    if ($('#grid').length > 0) {
      var $grid = $('#grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
      });

      $grid.on('click', '.grid-item-content', function (e) {
        var itemContent = this;
        if ($(e.target).hasClass('expand') && ($(e.target).hasClass('icon-jwt-resize') || $(e.target).hasClass('icon-jwt-contract'))) {
          setItemContentPixelSize(itemContent);

          var itemElem = itemContent.parentNode;
          //var itemElem = $(itemContent).parents('.grid-item');
          $(itemElem).toggleClass('is-expanded');
          $(e.target).toggleClass('icon-jwt-contract');
          $(e.target).toggleClass('icon-jwt-resize');

          // force redraw
          var redraw = itemContent.offsetWidth;
          // renable default transition
          itemContent.style[transitionProp] = '';

          addTransitionListener(itemContent);
          setItemContentTransitionSize(itemContent, itemElem, e);
          setScrollPosition(itemContent, itemElem, e);
          //$(e.target).toggleClass('collapse');

          $grid.masonry();
        }
      });

      var docElem = document.documentElement;
      var transitionProp = typeof docElem.style.transition == 'string' ?
        'transition' : 'WebkitTransition';
      var transitionEndEvent = {
        WebkitTransition: 'webkitTransitionEnd',
        transition: 'transitionend'
      }[transitionProp];

      function setItemContentPixelSize(itemContent) {
        var previousContentSize = getSize(itemContent);
        // disable transition
        itemContent.style[transitionProp] = 'none';
        // set current size in pixels
        itemContent.style.width = previousContentSize.width + 'px';
        itemContent.style.height = previousContentSize.height + 'px';
      }

      function addTransitionListener(itemContent) {
        // reset 100%/100% sizing after transition end
        var onTransitionEnd = function () {
          itemContent.style.width = '';
          itemContent.style.height = '';
          itemContent.removeEventListener(transitionEndEvent, onTransitionEnd);
        };
        itemContent.addEventListener(transitionEndEvent, onTransitionEnd);
      }

      function setItemContentTransitionSize(itemContent, itemElem, ev) {
        // set new size
        var size = getSize(itemElem);
        itemContent.style.width = size.width + 'px';
        itemContent.style.height = size.height + 'px';
        $scope.parentData = {
          elem: $(ev.target),
          event: ev,
          message: $(ev.target).hasClass('contract') ? 'collapsed called' : 'expanded called'
        };

        $scope.$broadcast(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, $scope.parentData);
      }

      function setScrollPosition(itemContent, itemElem, ev) {
        var parentObj = $(ev.target).parents('.grid-item-content');
        var parentOffset = parentObj.offset();
        var relX = ev.pageX - parentOffset.left;
        var relY = ev.pageY - parentOffset.top;
        var viewPortObj = jwtUtil.getViewport();
        var size = getSize(itemElem);
        var parentHeight = size.height;

        //if (parseInt(parentHeight + ev.pageY) > viewPortObj.height) {
          $('html, body').delay(400).animate({
            scrollTop: parentObj.offset().top
          }, 700);
        //}
        //$(window).scrollTop(parentOffset.top - 5);
      }

    }




  }
})();
