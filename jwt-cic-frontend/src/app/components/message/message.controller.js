angular
    .module('app.common')
    .controller('messageController', messageController);

messageController.$inject = ['$log', '$uibModalInstance'];

function messageController($log,$uibModalInstance) {
    var $ctrl = this;
    $ctrl.close = function(){
        $uibModalInstance.close('dismiss');
    }
                                        
}