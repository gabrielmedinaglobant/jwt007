(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtInlineMessage', {
      templateUrl: 'app/components/message/inline.message.html',
      controller: inlineMessageController,
      bindings: {
        content: '<',
      }
    });
})();

inlineMessageController.$inject = ['$log'];

function inlineMessageController(){
    var $ctrl = this;
}
