(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtBrandSearch', {
      templateUrl: 'app/components/brand-search/brand-search.html',
      controller: brandSearchController,
      bindings: {
        client: '<',
        onBrandSelect: '&'
      }
    });
})();
