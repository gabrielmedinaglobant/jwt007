brandSearchController.$inject = ['$log', '$scope', 'brandSearchService'];

function brandSearchController($log, $scope, brandSearchService) {
    var $ctrl = this;
    $scope.model = { brand: '' };
    $scope.disableBrand = true;
    $log.debug('components:brand-search');

    $ctrl.getLocation = function(val) {
        var params = { name: val };
        if (angular.isDefined($ctrl.client)) {
            if (!_.isEmpty($ctrl.client)) {
                params["client-name"] = $ctrl.client.name;
            }
        }
        return brandSearchService.getData(brandSearchService.getConstant('BRAND_LIST'), params).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:brand-search:error:getlist');
        })
    };

    // $scope.$watch("model.brand", function(n, o) {
    //     if (n == '') {
    //         $ctrl.onBrandSelect({ client: {} });
    //     }
    // });

    $scope.$watch("$ctrl.client", function(n, o) {
        if (Object.keys($ctrl.client ? $ctrl.client : {}).length == 0) {
            $scope.model.brand = "";
            $scope.disableBrand = true;
            $ctrl.onBrandSelect({ brand: {} });
        } else if (Object.keys($ctrl.client ? $ctrl.client : {}).length > 0) {
            if (n != o)
                $scope.model.brand = "";
            $scope.disableBrand = false;
        }
    });


    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        $ctrl.onBrandSelect({ brand: $item });
    }
}