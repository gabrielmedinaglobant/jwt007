(function() {
'use strict';

  angular
    .module('app.components')
    .constant('jwtBrandSearchConstant', {
      BRAND_LIST :{
          API:'/brands',
          PREFIX:true
      }
      
    });
})();
