 angular
    .module('app.common')
    .factory('brandSearchService', ['commonService','jwtBrandSearchConstant',function(commonService,jwtBrandSearchConstant) {
        
        return angular.extend({
            cnst:jwtBrandSearchConstant
        },commonService);
        
    }]);

