(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtPieChart', {
      template: '<h1>{{::$ctrl.message}}</h1>',
      controller: PieChartController,
      bindings: {
        data: '<'
      }
    });

  PieChartController.$inject = ['$log','$element','D3Service'];

  function PieChartController($log,$element,d3) {
    var $ctrl = this;

    this.$onInit = function(){
      this.message = 'Random example of pie-chart component';
    }

    this.$postLink = function(){
      $log.info(d3);
      $log.info($element);

      // TODO: set watchers for bindings, 
      // TODO: every time data is updated, D3 would be rendered


      $log.info($ctrl.data);

      var width = 960,
          height = 500,
          radius = Math.min(width, height) / 2;

      var colors = ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"];

      var arc = d3.arc()
          .outerRadius(radius - 10)
          .innerRadius(0);

      var labelArc = d3.arc()
          .outerRadius(radius - 40)
          .innerRadius(radius - 40);

      var pie = d3.pie()
          .sort(null)
          .value(function(d) { return d.population; });

      var svg = d3.select("body").append("svg")
          .attr("width", width)
          .attr("height", height)
        .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

      d3.csv("data/example-data.csv", type, function(error, data) {
        if (error) throw error;

        var g = svg.selectAll(".arc")
            .data(pie(data))
          .enter().append("g")
            .attr("class", "arc");

        g.append("path")
            .attr("d", arc)
            .style("fill", function(d,i) { return colors[i]; });

        g.append("text")
            .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
            .attr("dy", ".35em")
            .text(function(d) { return d.data.age; });
      });

      function type(d) {
        d.population = +d.population;
        return d;
      }

    }
    
  }

  
  
})();