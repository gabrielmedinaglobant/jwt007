(function() {
'use strict';

  angular
    .module('app.components')
    .constant('jwtClientSearchConstant', {
      CLIENT_LIST :{
          API:'/clients',
          PREFIX:true
      }
      
    });
})();
