 angular
    .module('app.common')
    .factory('clientSearchService', ['commonService','jwtClientSearchConstant',function(commonService,jwtClientSearchConstant) {
        
        return angular.extend({
            cnst:jwtClientSearchConstant
        },commonService);
        
    }]);

