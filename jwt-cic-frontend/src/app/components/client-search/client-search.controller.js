clientSearchController.$inject = ['$log', '$scope', 'clientSearchService'];

function clientSearchController($log, $scope, clientSearchService) {
    var $ctrl = this;
    $scope.model = { client: '' };
    $log.debug('components:client-search');

    $ctrl.getLocation = function(val) {
        return clientSearchService.getData(clientSearchService.getConstant('CLIENT_LIST'), { name: val }).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:client-search:error:getlist');
        })
    };

    $scope.$watch("model.client", function(n, o) {
        if (n == '') {
            $ctrl.onClientSelect({ client: { "name": "$%$%$%" } });
        }
    })
    $scope.$watch("$ctrl.clearsearch", function(n, o) {
        if (n == true) {
            $scope.model.client = "";
        }
    })

    $ctrl.onClientSearch = function(keyEvent) {
        if (keyEvent.which === 13) { $ctrl.onClientSelect({ client: $scope.model.client ? $scope.model.client : {} }); }

    }

    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        $ctrl.onClientSelect({ client: $item });
    }
}