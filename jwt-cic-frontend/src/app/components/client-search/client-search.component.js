(function() {
    'use strict';

    angular
        .module('app.components')
        .component('jwtClientSearch', {
            templateUrl: 'app/components/client-search/client-search.html',
            controller: clientSearchController,
            bindings: {
                client: '<',
                clearsearch: '<',
                onClientSelect: '&'
            }
        });
})();