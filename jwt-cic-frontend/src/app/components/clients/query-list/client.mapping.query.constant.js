(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('clientMappingQueryConstant', {
            QUERY_TYPE_LIST: {
                API: '/query-types',
                PREFIX: true
            },

            QUERY_LIST: {
                API: function(data) {
                    return '/queries';
                },
                PREFIX: true
            },
            QUERY_TYPES: {
                API: function(data) {
                    return '/query-types/';
                },
                PREFIX: true
            },
            DELETE_QUERY: {
                API: function(data) {
                    return '/queries/' + data.queryId;
                },
                PREFIX: true
            },
            ADD_QUERY: {
                API: function(data) {
                    return '/queries/';
                },
                PREFIX: true
            },
            UPDATE_QUERY: {
                API: function(data) {
                    return '/queries/' + data.queryId;
                },
                PREFIX: true
            }


        });
})();