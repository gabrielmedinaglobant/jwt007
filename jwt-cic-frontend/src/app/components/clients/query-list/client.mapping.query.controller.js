clientMappingQueryController.$inject = ['$scope', '$log', '$location', '$element', 'filterFilter', '$timeout', '$anchorScroll', 'clientMappingQueryService'];

function clientMappingQueryController($scope, $log, $location, $element, filterFilter, $timeout, $anchorScroll, clientMappingQueryService) {

    var $ctrl = this;
    $ctrl.addId = 999;
    $ctrl.dataModel = { queryTypes: [] };
    $ctrl.viewModel = { showTypeUl: false };
    $ctrl.model = { queries: [], currentQuery: {}, queryString: "", id: 0 };
    $ctrl.validation = { assignQuery: false };
    $ctrl.selectedType = "Define Type";


    $ctrl.$onInit = function $onInit() {
        $ctrl.getQueryTypes();
        $ctrl.paginationSettings = {
            currentPage: 1,
            recordsPerPage: 5,
            totalItems: 0
        };

    };

    $scope.$watch("$ctrl.project", function(n, o) {
        if (n == '' || n.item.id != o.item.id) {
            $ctrl.model.queryString = "";
            $ctrl.selectedType = "Define Type";
        }
    });



    $(document).click(function(e) {
        if ($(e.target).is("#typeselect") || $(e.target).closest("#typeselect").length) {
            // $timeout(function() {
            //     $location.hash('bottom-scroll-query');
            //     $anchorScroll();
            // });

        } else {
            $ctrl.viewModel.showTypeUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })

    function validateQueryInputs() {
        $ctrl.validation.assignQuery = false;
        if (_.isEmpty($ctrl.model.queryString)) {
            $ctrl.validation.assignQuery = false;
            return;
        }

        if ($ctrl.selectedType == "Define Type") {
            $ctrl.validation.assignQuery = false;
            return;
        }
        $ctrl.validation.assignQuery = true;
    }

    $ctrl.validateQuery = function() {
        validateQueryInputs();
    }

    $ctrl.onQueryTypeSelect = function(option) {
        $ctrl.model.currentQuery.queryTypeId = option.id;
        $ctrl.model.currentQuery.description = option.description;
        $ctrl.selectedType = option.description;
        validateQueryInputs();

    };
    $ctrl.onAssignQuery = function onAssignQuery() {

        $ctrl.model.currentQuery.queryString = angular.copy($ctrl.model.queryString);
        $ctrl.model.currentQuery.id = $ctrl.addId++;
        $ctrl.model.queries.push(angular.copy($ctrl.model.currentQuery));
        $ctrl.addQuery();
        $ctrl.model.currentQuery = {};
        $ctrl.model.queryString = "";
        $ctrl.model.queries = [];
        $ctrl.validation.assignQuery = false;
        $ctrl.selectedType = "Define Type";
    };

    $ctrl.addQuery = function addQuery(event) {
        $ctrl.onAdd({
            $event: {
                index: $ctrl.model.queries
            }
        });
    };

    $ctrl.deleteQuery = function deleteQuery(event) {
        $ctrl.onDeleteQuery({
            $event: {
                index: event
            }
        });
    };
    $ctrl.updateQuery = function updateQuery(event) {
        $ctrl.onUpdateQuery({
            $event: {
                index: event
            }
        });
    };

    $ctrl.getQueryTypes = function() {
        clientMappingQueryService.getData(clientMappingQueryService.getConstant('QUERY_TYPE_LIST'), {})
            .then(function(response) {
                $ctrl.dataModel.queryTypes = response.data;
            }, function() {});
    };

    function init() {
        $ctrl.addId = 999;

    };


    init();
}