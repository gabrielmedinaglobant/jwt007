 angular
     .module('app.common')
     .factory('clientMappingQueryService', ['commonService', 'clientMappingQueryConstant', function(commonService, clientMappingQueryConstant) {

         return angular.extend({
             cnst: clientMappingQueryConstant
         }, commonService);

     }]);