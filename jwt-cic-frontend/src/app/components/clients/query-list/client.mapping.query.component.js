(function() {
    'use strict';
    angular
        .module('app.components')
        .component('queryList', {
            templateUrl: 'app/components/clients/query-list/client.mapping.query.search.html',
            controller: clientMappingQueryController,
            bindings: {
                index: '<',
                querylist: '<',
                queryTypes: '<',
                project: '<',
                onAddQuery: '&',
                onUpdateQuery: '&',
                onDeleteQuery: '&',
                onAdd: '&'
            }
        });
})();