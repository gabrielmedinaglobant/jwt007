 (function() {
     'use strict';

     angular
         .module('app.components')
         .constant('jwtUserMappingConstant', {
             USER_PROJECT_LIST: {
                 API: function(data) {
                     return '/users/' + data.userId;
                 },
                 PREFIX: true
             },
             USER_SEARCH_LIST: {
                 API: function(data) {
                     return '/users/search';
                 },
                 PREFIX: true
             }




         });
 })();