userMappingController.$inject = ['$scope', '$log', '$element', 'UserMappingService'];

function userMappingController($scope, $log, $elementr, UserMappingService) {
    var $ctrl = this;
    $ctrl.userSearchTxt = "";
    $ctrl.userProjectList = [];
    $ctrl.showError = false;
    $ctrl.fName = "";
    $ctrl.lName = "";
    // $ctrl.tab = '';
    $ctrl.$onInit = function $onInit() {
        $ctrl.paginationSettings = {
            currentPage: 1,
            recordsPerPage: 5,
            totalItems: 0
        };
        $ctrl.paginationSettings.totalItems = $ctrl.userProjectList.projects ? $ctrl.userProjectList.projects.length : 0;
    };

    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        if ($item) {
            return UserMappingService.getData(UserMappingService.getConstant('USER_PROJECT_LIST'), {}, { userId: $item.nameId }).then(function(response) {
                $ctrl.userProjectList = response.data;
                $ctrl.fName = jwtUtil.toTitleCase($ctrl.userProjectList.name);;
                $ctrl.lName = jwtUtil.toTitleCase($ctrl.userProjectList.lastName);;
                if ($ctrl.userProjectList.projects.length == 0) { $ctrl.showError = true; } else { $ctrl.showError = false; }

            }, function(error) {
                $ctrl.userProjectList = [];
                $ctrl.fName = "";
                $ctrl.lName = "";
                $ctrl.showError = true;
            })
        }
    }

    $scope.$watch("$ctrl.userSearchTxt", function(n, o) {
        if (n == '') {
            $ctrl.userProjectList = [];
            $ctrl.fName = "";
            $ctrl.lName = "";
            $ctrl.showError = false;
        }
    })

    $scope.$watch("$ctrl.tab", function(n, o) {
        if (n == 'user') {
            $ctrl.userProjectList = [];
            $ctrl.fName = "";
            $ctrl.lName = "";
            $ctrl.userSearchTxt = "";
            $ctrl.showError = false;
        }

    })

    $ctrl.getProjectUserList = function(val) {
        return UserMappingService.getData(UserMappingService.getConstant('USER_SEARCH_LIST'), { "query": val }, {}).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:user-search:error:getlist');
        })

    }

}