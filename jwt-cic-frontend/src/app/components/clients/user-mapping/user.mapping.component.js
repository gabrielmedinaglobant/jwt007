(function() {
    'use strict';
    angular
        .module('app.components')
        .component('jwtUserMapping', {
            templateUrl: 'app/components/clients/user-mapping/user.mapping.html',
            controller: userMappingController,
            bindings: {
                data: '<',
                tab: '<'
            }
        });
})();