 angular
     .module('app.common')
     .factory('UserMappingService', ['commonService', 'jwtUserMappingConstant', function(commonService, jwtUserMappingConstant) {

         return angular.extend({
             cnst: jwtUserMappingConstant
         }, commonService);

     }]);