clientMappingUsersController.$inject = ['$scope', '$log', '$element', 'jwtClientMappingService', 'filterFilter'];

function clientMappingUsersController($scope, $log, $element, jwtClientMappingService, filterFilter) {

    var $ctrl = this;
    $ctrl.$onInit = function $onInit() {
        $ctrl.paginationSettings = {
            currentPage: 1,
            recordsPerPage: 5,
            totalItems: 0
        };
        $ctrl.paginationSettings.totalItems = $ctrl.assignlist.length;
    };
    $ctrl.unselectUser = function unselectUser(event) {
        $ctrl.onUnSelect({
            $event: {
                index: event
            }
        });
    };

    $ctrl.unassignUser = function unassignUser(event) {
        $ctrl.onunAssign({
            $event: {
                index: event
            }
        });
    };
    $ctrl.onChange = function onChange(event) {
        $ctrl.onRoleChange({
            $event: {
                index: event.index
            }
        });
    };
    $ctrl.unassignAllUser = function unassignAllUser(event) {
        $ctrl.onunAssignAll({
            $event: {
                index: event
            }
        });
    };

    $scope.$watch('search', function(newVal, oldVal) {
        $ctrl.fillterdlist = filterFilter($ctrl.assignlist, newVal);
        $ctrl.paginationSettings.totalItems = $ctrl.fillterdlist.length;
        $ctrl.paginationSettings.currentPage = 1;
    }, true);

}