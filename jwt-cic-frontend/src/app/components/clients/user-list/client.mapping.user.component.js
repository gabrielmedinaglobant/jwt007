(function() {
    'use strict';
    angular
        .module('app.components')
        .component('userList', {
            templateUrl: 'app/components/clients/user-list/client.mapping.user.html',
            controller: clientMappingUsersController,
            bindings: {
                assignlist: '<',
                userroles: '<',
                onunAssign: '&',
                onRoleChange: '&',
                onunAssignAll: '&',
                onUnSelect: '&'
            }
        });
})();