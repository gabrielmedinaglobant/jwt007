angular
    .module('app.common')
    .controller('clientMappingModalController', clientMappingModalController);

clientMappingModalController.$inject = ['$log', '$scope', '$uibModalInstance', 'options'];

function clientMappingModalController($log, $scope, $uibModalInstance, options) {
    var $ctrl = this;
    $ctrl.save = function() {
        $uibModalInstance.close('');
    }

    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
}