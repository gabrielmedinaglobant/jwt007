 (function() {
     'use strict';

     angular
         .module('app.components')
         .constant('jwtClientMappingConstant', {
             PROJECT_LIST: {
                 API: '/projects',
                 PREFIX: true
             },

             ASSIGN_USER_LIST: {
                 API: function(data) {
                     return '/projects/' + data.projectId + '/users';
                 },
                 PREFIX: true
             },

             UNASSIGNED_USER_LIST: {
                 API: function(data) {
                     return '/projects/' + data.projectId + '/users/unassigned';
                 },
                 PREFIX: true
             },

             PROJECT_DELETE: {

                 API: function(data) {
                     return '/projects/' + data.projectId;
                 },
                 PREFIX: true
             },
             USER_ROLE: {
                 API: '/roles',
                 PREFIX: true
             },
             UNASSIGNED_USER_SELECTED: {
                 API: function(data) {
                     return '/projects/' + data.projectId + '/users/' + data.userId;
                 },
                 PREFIX: true
             },
             UNASSIGNED_USER_ALL: {
                 API: function(data) {
                     return '/projects/' + data.projectId + '/users/unassign-many';
                 },
                 PREFIX: true
             }



         });
 })();