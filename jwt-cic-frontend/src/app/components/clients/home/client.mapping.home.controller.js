/* --------- Controller for client mapping project home --------------------------------------------------------------------------*|
 |--- common controller for client mapping project home, this is parent componet for all client mapping components ----------------|
 |--------------------------------------------------------------------------*/
clientMappingHomeController.$inject = ['$scope', '$log', '$element', '$uibModal', 'clientCreateProjectService', 'ClientMappingService', 'projectSearchService', 'toaster', 'jwtAppConstant', 'UserMappingService', 'clientMappingQueryService'];

function clientMappingHomeController($scope, $log, $element, $uibModal, clientCreateProjectService, ClientMappingService, projectSearchService, toaster, jwtAppConstant, UserMappingService, clientMappingQueryService) {
    var $ctrl = this;
    $ctrl.clientMsg = jwtAppConstant.clientMapping.client.messages;
    $ctrl.projectList = [], $ctrl.userList = [], $ctrl.userSearchList = [],
        $ctrl.userRole = [], $ctrl.projectId, $ctrl.client,
        $ctrl.brand, $ctrl.project, $ctrl.selectedProject = [],
        $ctrl.searchUsers,
        $ctrl.unassignSaveList = [],
        $ctrl.assignSaveList = [],
        $ctrl.roleSaveList = [],
        $ctrl.projectSaveList = [],
        $ctrl.unassignDataArray = [],
        $ctrl.queryList = [],
        $ctrl.addQueryList = [],
        $ctrl.updateQueryList = [],
        $ctrl.deleteQueryList = [],
        $ctrl.validate = false,
        $ctrl.clearSearch = false,
        $ctrl.dummyprojectId = -1,
        $ctrl.tab = 'client';
    $ctrl.paginationSettings = {
        currentPage: 1,
        recordsPerPage: 5
    };
    $ctrl.$onInit = function() {
        $('html, body').animate({
            scrollTop: '0'
        }, 200);
    };
    //$ctrl.$onDestroy = function $onDestroy() {};

    //slecte project from project list from chile componete project list
    $ctrl.selectProject = function selectProject(project) {
        $ctrl.selectedProject = project;
        $ctrl.projectId = project.item.id;
        // $ctrl.userSearchList = [];
        getAssignedUsers($ctrl.projectId);
        getQueryList($ctrl.projectId);

    };

    //on cleint select from serch client
    $ctrl.onClientSelect = function(client) {
        $ctrl.projectList = [];
        if (client.name != "$%$%$%" || $ctrl.project.name != "") {
            if (client.name == "$%$%$%") {
                $ctrl.client = {};
                if ($ctrl.project.name == "")
                    return;
            } else {
                $ctrl.client = client;
            }
            getAllProjects();
        } else {
            $ctrl.client = {};
        }
    };
    //on brand select from brand search
    $ctrl.onBrandSelect = function(brand) {
        $ctrl.projectList = [];
        $ctrl.brand = brand;
        if ($ctrl.client.name) {
            getAllProjects();
        }
    };
    //on prject select from project search
    $ctrl.onProjectSelect = function(project) {
        $ctrl.projectList = [];
        if (project.name != "$%$%$" || $ctrl.client.name != "") {
            if (project.name == "$%$%$") {
                $ctrl.project = {};
            } else {
                $ctrl.project = project;
            }
            getAllProjects();
        } else {
            $ctrl.project = {};
        }
    };
    //on user select from unassigend user list
    $ctrl.onUserSelect = function(user) {
        $ctrl.searchUsers = user;
        // getUnAssignedUsers(user);
    };
    //update infegy queries for project
    $ctrl.onAddQuery = function onAddQuery(event) {
        if (event) {

            $ctrl.addQueryList.push({ id: event.index[0].id, queryString: event.index[0].queryString, queryTypeId: event.index[0].queryTypeId, projectId: $ctrl.selectedProject.item.id });
            $ctrl.queryList.push({ id: event.index[0].id, queryString: event.index[0].queryString, queryTypeId: event.index[0].queryTypeId, projectId: $ctrl.selectedProject.item.id });
            $ctrl.validate = true;
        }
    };
    $ctrl.onUpdateQuery = function(query) {
        if (query) {
            if ($ctrl.updateQueryList.length > 0) {
                for (var i = $ctrl.updateQueryList.length - 1; i >= 0; i--) {
                    if ($ctrl.updateQueryList[i].id == query.index.id) {
                        $ctrl.updateQueryList.splice(i, 1);
                    }
                }
            }
            $ctrl.updateQueryList.push(query.index);

            $ctrl.validate = true;

        }
    };
    $ctrl.onDeleteQuery = function(query) {

        if (query) {
            for (var i = $ctrl.queryList.length - 1; i >= 0; i--) {
                if (query.index.id != 0) {
                    if ($ctrl.queryList[i].id == query.index.id) {
                        $ctrl.queryList.splice(i, 1);

                    }
                } else if (query.index.$$hashKey) {
                    if ($ctrl.queryList[i].$$hashKey == query.index.$$hashKey) {
                        $ctrl.queryList.splice(i, 1);

                    }
                }
            }
            if ($ctrl.addQueryList.length > 0) {
                for (var i = $ctrl.addQueryList.length - 1; i >= 0; i--) {
                    if ($ctrl.addQueryList[i].id == query.index.id) {
                        $ctrl.addQueryList.splice(i, 1);

                    }
                }
            }
            if ($ctrl.updateQueryList.length > 0) {
                for (var i = $ctrl.updateQueryList.length - 1; i >= 0; i--) {
                    if ($ctrl.updateQueryList[i].id == query.index.id) {
                        $ctrl.updateQueryList.splice(i, 1);

                    }
                }
            }
            if (query.index.id < 999) {
                $ctrl.deleteQueryList.push(query.index);
            }
            $ctrl.validate = true;
            checkValidate();
        }
    };


    //unassign all user event
    $ctrl.unassignAllUser = function unassignAllUser(event) {
        if (event) {
            $ctrl.validate = true;
            $.map($ctrl.userList, function(data, i) {
                $ctrl.unassignDataArray.push(data.userNameId);
            });
            $ctrl.userList = [];
            $ctrl.roleSaveList = [];
        }
    };

    //assign user event
    $ctrl.assignUser = function assignUser(event) {
        if (event) {
            if ($ctrl.assignSaveList.length > 0) {
                for (var i = $ctrl.assignSaveList.length - 1; i >= 0; i--) {
                    if ($ctrl.assignSaveList[i].assignList.userNameId == event.index.userNameId) {
                        return;
                    }
                }
            }
            if ($ctrl.userList.length > 0) {
                for (var i = $ctrl.userList.length - 1; i >= 0; i--) {
                    if ($ctrl.userList[i].userNameId == event.index.userNameId) {
                        popToaster("error", "User already assigned to project, Please select another one.");
                        return;
                    }
                }
            }
            $ctrl.assignSaveList.push({ projectId: $ctrl.projectId, assignList: event.index });
            $ctrl.validate = true;
        }
    };
    //delte project evetn
    $ctrl.deleteProject = function deleteProject(event) {
        return ClientMappingService.deleteData(ClientMappingService.getConstant('PROJECT_DELETE'), {}, { projectId: event.index }).then(function(response) {
            getAllProjects();
            getAssignedUsers($ctrl.projectId);
            getQueryList($ctrl.dummyprojectId);
        }, function(error) {
            $log.debug('components:unassign-Users');
        })
    };

    function checkValidate() {
        if ($ctrl.projectSaveList.length == 0 && $ctrl.unassignSaveList.length == 0 && $ctrl.assignSaveList.length == 0 && $ctrl.roleSaveList.length == 0 && $ctrl.unassignDataArray.length == 0 && $ctrl.updateQueryList.length == 0 && $ctrl.addQueryList.length == 0 && $ctrl.deleteQueryList.length == 0) {
            $ctrl.validate = false;
        }
    }

    //update project event
    $ctrl.updateProject = function updateProject(event) {
        if (event && Object.keys(event.project).length > 0) {
            $ctrl.validate = true;
            $ctrl.projectSaveList = event.project;
        } else {
            $ctrl.projectSaveList = [];
            checkValidate();
        }
    };

    $ctrl.unselectUser = function unselectUser(event) {
        if (event) {
            for (var i = $ctrl.roleSaveList.length - 1; i >= 0; i--) {
                if ($ctrl.roleSaveList[i].roleList.userNameId == event.index.userNameId) {
                    $ctrl.roleSaveList.splice(i, 1);
                }
            }
            for (var i = $ctrl.unassignSaveList.length - 1; i >= 0; i--) {
                if ($ctrl.unassignSaveList[i].unassignList.userNameId == event.index.userNameId) {
                    $ctrl.unassignSaveList.splice(i, 1);
                }
            }
            checkValidate();
        }

    }

    //unassign user from list event
    $ctrl.unassignUser = function unassignUser(event) {
        if (event) {
            $ctrl.unassignSaveList.push({ projectId: $ctrl.projectId, unassignList: event.index });
            for (var i = $ctrl.userList.length - 1; i >= 0; i--) {
                if ($ctrl.userList[i].userNameId == event.index.userNameId) {
                    $ctrl.userList.splice(i, 1);
                }
            }

            for (var i = $ctrl.roleSaveList.length - 1; i >= 0; i--) {
                if ($ctrl.roleSaveList[i].roleList.userNameId == event.index.userNameId) {
                    $ctrl.roleSaveList.splice(i, 1);
                }
            }

            $ctrl.validate = true;
        }
    };
    //cahnge rol of selected user event
    $ctrl.changeUserRole = function changeUserRole(event) {

        if (event) {
            // if (event.index.role == "ROLE_ANALYST") {
            //     var userRoleCnt = 0;
            //     UserMappingService.getData(UserMappingService.getConstant('USER_PROJECT_LIST'), {}, { userId: event.index.userNameId }).then(function(response) {
            //         if (response.data.projects.length > 0) {
            //             for (var i = response.data.projects.length - 1; i >= 0; i--) {
            //                 if (response.data.projects[i].role == "ROLE_ANALYST") {
            //                     userRoleCnt++;

            //                     popToaster("error", "User can not have role as 'Analyst' in more than one project");
            //                     return;
            //                 }
            //             }
            //             if (userRoleCnt == 0) {
            //                 $ctrl.roleSaveList.push({ projectId: $ctrl.projectId, roleList: event.index });
            //                 $ctrl.validate = true;
            //             }

            //         }
            //     }, function(error) {})
            // } else {
            $ctrl.roleSaveList.push({ projectId: $ctrl.projectId, roleList: event.index });
            $ctrl.validate = true;
            // }
        }
    };

    $ctrl.initData = function() {
            $ctrl.validate = false;

        }
        //this function update all project related event on save button click
    $ctrl.savetData = function() {
        toaster.clear();
        if (Object.keys($ctrl.projectSaveList).length > 0) {
            var serverModel = { name: "", brandId: 0 };
            if (angular.isDefined($ctrl.projectSaveList.projectName)) {
                serverModel.name = $ctrl.projectSaveList.projectName;
            }
            serverModel.brandId = $ctrl.projectSaveList.brandId;

            clientCreateProjectService.postData(clientCreateProjectService.getConstant('PROJECT_UPDATE'), serverModel, { projectId: $ctrl.projectSaveList.projectId })
                .then(function(response) {
                        $ctrl.projectSaveList = [];
                        getAllProjects();
                        $ctrl.userList = [];
                        //$ctrl.userSearchList = [];
                        $ctrl.projectId = -1;
                        popToaster("success", $ctrl.clientMsg.projectSuccess);
                    },
                    function(error) {
                        $ctrl.projectSaveList = [];

                    });
        }



        if ($ctrl.unassignSaveList.length > 0) {
            var cnt = 0;
            angular.forEach($ctrl.unassignSaveList, function(value, key) {
                ClientMappingService.deleteData(ClientMappingService.getConstant('UNASSIGNED_USER_SELECTED'), {}, { projectId: value.projectId, userId: value.unassignList.userNameId }).then(function(response) {
                    cnt++;
                    if (cnt == $ctrl.unassignSaveList.length) {
                        var cntMsg = cnt > 1 ? "users" : "user";
                        popToaster("success", "Unassign Users : " + cnt + " " + cntMsg + " unassigned successfully from project " + $ctrl.selectedProject.item.name);
                        $ctrl.unassignSaveList = [];
                        $ctrl.validate = false;
                        if ($ctrl.projectId != -1);
                        getAssignedUsers($ctrl.projectId);
                    }
                }, function(error) {
                    $ctrl.unassignSaveList = [];
                })
            });
        }

        if ($ctrl.roleSaveList.length > 0) {
            var roleCnt = 0;
            angular.forEach($ctrl.roleSaveList, function(value, key) {
                ClientMappingService.postData(ClientMappingService.getConstant('ASSIGN_USER_LIST'), value.roleList, { projectId: value.projectId })
                    .then(function(response) {
                        roleCnt++;
                        if (roleCnt == $ctrl.roleSaveList.length) {

                            popToaster("success", "Change User Roles : " + roleCnt + " users role changed successfully for project " + $ctrl.selectedProject.item.name);
                            $ctrl.validate = false;
                            $ctrl.roleSaveList = [];
                            if ($ctrl.projectId != -1)
                                getAssignedUsers($ctrl.projectId);

                        }
                    }, function(error) {

                        popToaster("error", "Change User Roles : Error encountered  while changing user roles for project " + $ctrl.selectedProject.item.name);
                        $ctrl.validate = false;
                        $ctrl.roleSaveList = [];
                        getAssignedUsers($ctrl.projectId);

                    });
            });
        }

        if ($ctrl.assignSaveList.length > 0) {
            var assignCnt = 0;
            angular.forEach($ctrl.assignSaveList, function(value, key) {
                ClientMappingService.postData(ClientMappingService.getConstant('ASSIGN_USER_LIST'), value.assignList, { projectId: value.projectId })
                    .then(function(response) {
                        assignCnt++;
                        if (assignCnt == $ctrl.assignSaveList.length) {
                            var assigncntMsg = assignCnt > 1 ? "users" : "user";
                            $ctrl.assignSaveList = [];
                            $ctrl.validate = false;
                            popToaster("success", "Assign Users : " + assignCnt + " " + assigncntMsg + " assigned successfully to project " + $ctrl.selectedProject.item.name);
                            if ($ctrl.projectId != -1) {
                                getAssignedUsers($ctrl.projectId);
                                // getUnAssignedUsers($ctrl.searchUsers);
                            }
                        }
                    }, function() {

                    });
            });
        };

        if ($ctrl.deleteQueryList.length > 0) {
            var deleteQueryCnt = 0;
            angular.forEach($ctrl.deleteQueryList, function(value, key) {
                clientMappingQueryService.deleteData(clientMappingQueryService.getConstant('DELETE_QUERY'), {}, { queryId: value.id })
                    .then(function(response) {
                        deleteQueryCnt++;
                        if (deleteQueryCnt == $ctrl.deleteQueryList.length) {
                            var deleteQueryCntMsg = deleteQueryCnt > 1 ? "queries" : "query";
                            $ctrl.deleteQueryList = [];
                            $ctrl.validate = false;
                            popToaster("success", "Delete Query : " + deleteQueryCnt + " " + deleteQueryCntMsg + " deleted successfully for project " + $ctrl.selectedProject.item.name);
                            if ($ctrl.projectId != -1) {
                                getQueryList($ctrl.projectId);
                                // getUnAssignedUsers($ctrl.searchUsers);
                            }
                        }
                    }, function() {

                    });
            });
        };

        if ($ctrl.updateQueryList.length > 0) {
            var updateCnt = 0;
            angular.forEach($ctrl.updateQueryList, function(value, key) {
                clientMappingQueryService.postData(clientMappingQueryService.getConstant('UPDATE_QUERY'), value, { queryId: value.id })
                    .then(function(response) {
                        updateCnt++;
                        if (updateCnt == $ctrl.updateQueryList.length) {
                            var updateCntMsg = updateCnt > 1 ? "queries" : "query";
                            popToaster("success", "Update query : " + updateCnt + " " + updateCntMsg + " updated successfully for project " + $ctrl.selectedProject.item.name);
                            $ctrl.validate = false;
                            $ctrl.updateQueryList = [];
                            if ($ctrl.projectId != -1) {
                                getQueryList($ctrl.projectId);
                                // getUnAssignedUsers($ctrl.searchUsers);
                            }

                        }
                    }, function() {

                        popToaster("error", "Update query : Error encountered  while updating query for project " + $ctrl.selectedProject.item.name);
                        $ctrl.validate = false;
                        $ctrl.updateQueryList = [];


                    });
            });
        }

        if ($ctrl.addQueryList.length > 0) {
            var addCnt = 0;
            angular.forEach($ctrl.addQueryList, function(value, key) {
                clientMappingQueryService.postData(clientMappingQueryService.getConstant('ADD_QUERY'), value, {})
                    .then(function(response) {
                        addCnt++;
                        if (addCnt == $ctrl.addQueryList.length) {
                            var addCntMsg = addCnt > 1 ? "queries" : "query";
                            popToaster("success", "Add Query : " + addCnt + " " + addCntMsg + " added successfully for project " + $ctrl.selectedProject.item.name);
                            $ctrl.validate = false;
                            $ctrl.addQueryList = [];
                            if ($ctrl.projectId != -1) {
                                getQueryList($ctrl.projectId);
                                // getUnAssignedUsers($ctrl.searchUsers);
                            }

                        }
                    }, function() {

                        popToaster("error", "Add query : Error encountered  while addding query for project " + $ctrl.selectedProject.item.name);
                        $ctrl.validate = false;
                        $ctrl.addQueryList = [];


                    });
            });
        }

        if ($ctrl.unassignDataArray.length > 0) {

            ClientMappingService.deleteData(ClientMappingService.getConstant('UNASSIGNED_USER_ALL'), $ctrl.unassignDataArray, { projectId: $ctrl.projectId }).then(function(response) {
                $ctrl.validate = false;
                popToaster("success", "Unassign all users : All users unassigned successfully from project " + $ctrl.selectedProject.item.name);
                if ($ctrl.projectId != -1) {
                    getAssignedUsers($ctrl.projectId);
                }
                $ctrl.unassignDataArray = [];
            }, function(error) {
                $log.debug('components:unassign-Users');
            });

        };
    }

    //show pop up on click of cancel button click
    $ctrl.cancelData = function() {
        $log.debug('in:cancel data save');
        var modalInstance = $uibModal.open({
            templateUrl: 'app/components/clients/home/client.mapping.project.modal.html',
            controller: 'clientMappingModalController',
            controllerAs: '$ctrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false,
            resolve: { options: { id: $ctrl.projectId } }
        });

        modalInstance.result.then(function(selectedItem) {
            $ctrl.unassignSaveList = [];
            $ctrl.assignSaveList = [];
            $ctrl.roleSaveList = [];
            // $ctrl.userSearchList = [];
            $ctrl.unassignDataArray = [];
            $ctrl.userList = [];
            $ctrl.addQueryList = [];
            $ctrl.deleteQueryList = [];
            $ctrl.updateQueryList = [];
            $ctrl.queryList = [];
            $ctrl.validate = false;
            getAllProjects();
        }, function() {

        });

    };
    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

    //get all project list
    function getAllProjects() {
        $ctrl.initData();
        if (_.isEmpty($ctrl.project) && _.isEmpty($ctrl.brand) && _.isEmpty($ctrl.client)) {
            $ctrl.userList = [];
            $ctrl.queryList = [];
            $ctrl.selectedProject = [];
            $ctrl.projectList = [];

        }
        var params = { name: $ctrl.project ? $ctrl.project.name : "" };
        if (angular.isDefined($ctrl.client)) {
            if (!_.isEmpty($ctrl.client)) {
                params["client-name"] = $ctrl.client.name ? $ctrl.client.name : "";
            }
        }
        if (angular.isDefined($ctrl.client)) {
            if (!_.isEmpty($ctrl.client)) {

                if (angular.isDefined($ctrl.brand)) {
                    if (!_.isEmpty($ctrl.brand)) {
                        params["brand-name"] = $ctrl.brand.name ? $ctrl.brand.name : "";
                    }
                }
            }
        }

        return projectSearchService.getData(projectSearchService.getConstant('PROJECT_LIST'), params).then(function(response) {
            $ctrl.projectList = response.data;
            $ctrl.selectedProject = [];
            if (_.isEmpty($ctrl.projectList)) {
                $ctrl.clearSearch = true;
            } else {
                $ctrl.clearSearch = false;
            }
            if (_.isEmpty($ctrl.project) && _.isEmpty($ctrl.brand) && _.isEmpty($ctrl.client)) {
                $ctrl.userList = [];
                $ctrl.selectedProject = [];
                // $ctrl.projectList = [];
                $ctrl.queryList = [];
            }
        }, function(error) {
            $log.debug('components:project-search:error:getlist');
        })
    }

    //get all assigned users from selcted project
    function getAssignedUsers(item) {
        return ClientMappingService.getData(ClientMappingService.getConstant('ASSIGN_USER_LIST'), {}, { projectId: item }).then(function(response) {
            $ctrl.userList = response.data;
            getUserRoles();
        }, function(error) {
            $log.debug('components:client-search:error:getlist');
        })
    }

    function getQueryList(item) {

        return clientMappingQueryService.getData(clientMappingQueryService.getConstant('QUERY_LIST'), { "project-id": item }, {}).then(function(response) {
            $ctrl.queryList = response.data;
            getUserRoles();
        }, function(error) {
            $log.debug('components:client-search:error:querylist');
        })
    }




    function getUserRoles() {
        return ClientMappingService.getData(ClientMappingService.getConstant('USER_ROLE'), {}).then(function(response) {
            $ctrl.userRole = response.data;
        }, function(error) {
            $log.debug('components:client-project-search:error:getlist');
        })
    };
    //pop toaster messages
    function popToaster(msgType, title, msg) {
        toaster.pop(msgType, title, msg, 5000, 'trustedHtml');
    };
    //User control events
    $ctrl.createProject = function() {
        $log.debug('in:create new project');
        var modalInstance = $uibModal.open({
            templateUrl: 'app/components/clients/create-project/create-project.html',
            controller: 'clientCreateProjectController',
            controllerAs: '$ctrl',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: {}
        });

        modalInstance.result.then(function(selectedItem) {
            // getAllProjects();
            $ctrl.userList = [];
            $ctrl.selectedProject = [];
            $ctrl.projectList = [];
            $ctrl.queryList = [];
            $ctrl.clearSearch = true;
        }, function() {

        });

    };
    //handle tab change functions
    $ctrl.setTab = function(newTab) {
        $ctrl.tab = newTab;
    };
    $ctrl.isSet = function(tabName) {
        return $ctrl.tab === tabName;
    };


}