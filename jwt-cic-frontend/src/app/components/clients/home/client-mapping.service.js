 angular
    .module('app.common')
    .factory('ClientMappingService', ['commonService','jwtClientMappingConstant',function(commonService,jwtClientMappingConstant) {
        
        return angular.extend({
            cnst:jwtClientMappingConstant
        },commonService);
        
    }]);


 
