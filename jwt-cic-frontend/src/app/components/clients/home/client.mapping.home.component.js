(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtClientHome', {
      templateUrl: 'app/components/clients/home/client.mapping.home.html',
      controller: clientMappingHomeController,
      bindings: {
       filter: '<'
      }
    });

 

})();
