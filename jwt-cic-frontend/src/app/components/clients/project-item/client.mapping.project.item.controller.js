/* --------- Controller for client mapping project list item  --------------*|
 |--- common controller for client mapping project list item ----------------|
 |--------------------------------------------------------------------------*/
clientMappingProjectItemController.$inject = ['$scope', '$log', '$element', '$uibModal', 'jwtClientMappingService', 'clientSearchService', 'brandSearchService', 'projectSearchService', 'toaster', 'jwtAppConstant'];


function clientMappingProjectItemController($scope, $log, $element, $uibModal, jwtClientMappingService, clientSearchService, brandSearchService, projectSearchService, toaster, jwtAppConstant) {
    var $ctrl = this;
    $ctrl.projectMsg = jwtAppConstant.clientMapping.client.messages;
    $ctrl.selected = { value: null };
    $ctrl.select = false;
    $ctrl.model = { client: '' };
    $ctrl.model = { brand: '' };
    $ctrl.model.project = "";
    $ctrl.disableBrand = true;
    $ctrl.validation = { save: true };

    $ctrl.$onInit = function $onInit() {
        $ctrl.model.client = $ctrl.client.clientName;
        $ctrl.model.brand = $ctrl.client.brandName;
        $ctrl.model.project = $ctrl.client.name;
        $ctrl.selected = false;
        $ctrl.editing = false;

        if ($ctrl.selctedproject.item) {
            if ($ctrl.selctedproject.item.id == $ctrl.client.id) {
                var liCnt = $ctrl.index + 1;
                $ctrl.select = true;

                $(".project-table li").removeClass("select-row");
                $(".project-table li:eq(" + liCnt + ")").addClass("select-row");
                $(".project-table .noBrand").addClass("ng-hide");
                $(".project-table .noClient").addClass("ng-hide");
            }

        }
    };

    //  get list of clients while project update
    $ctrl.getLocation = function(val) {
        return clientSearchService.getData(clientSearchService.getConstant('CLIENT_LIST'), { name: val }).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:client-search:error:getlist');
        })
    };
    $scope.$watch("$ctrl.model.project", function(n, o) {
        if (n == '') {
            validateInputs();

        } else if (n != o) {
            //  validateInputs();
            if ($ctrl.validation.save == true) {
                $ctrl.updateProject();
            }
        }
    });
    $scope.$watch("$ctrl.model.client", function(n, o) {
        if (n == '') {
            $ctrl.model.brand = "";
            validateInputs();
        }
    })

    $scope.$watch("noResults", function(n, o) {
        if (n == true) {
            $ctrl.updateEmptyProject();
        }
    })

    $scope.$watch("noBrandResults", function(n, o) {
        if (n == true) {
            $ctrl.updateEmptyProject();
        }
    })



    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
            $ctrl.model.client = $item;
            if ($ctrl.model.client.name != $ctrl.client.clientName) {
                $ctrl.model.brand = "";
            } else {
                $ctrl.model.brand = $ctrl.client.brandName;
                $ctrl.updateEmptyProject();
                toaster.clear();
                return;
            }

            //$ctrl.isProjectExist();
            validateInputs();
            if ($ctrl.validation.save == true) {
                $ctrl.updateProject();
            }
        }
        // get list of brands while project update
    $ctrl.getbrandLocation = function(val) {
        var params = { name: val };
        if (angular.isDefined($ctrl.model.client)) {
            if (!_.isEmpty($ctrl.model.client)) {
                params["client-name"] = $ctrl.model.client.name ? $ctrl.model.client.name : $ctrl.model.client;;
            }
        }
        return brandSearchService.getData(brandSearchService.getConstant('BRAND_LIST'), params).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:brand-search:error:getlist');
        })
    };

    $scope.$watch("$ctrl.model.brand", function(n, o) {
        if (n == '') {
            validateInputs();
        }
    });

    $ctrl.onBrandSelectionChange = function($item, $model, $label, $event) {
        $ctrl.model.brand = $item;
        // $ctrl.isProjectExist();
        validateInputs();
        if ($ctrl.validation.save == true) {
            $ctrl.updateProject();
        }
    }





    // check if project with cleint brand comb alredy exists and validate inputs while project update
    $ctrl.isProjectExist = function() {
        var params = {};
        if (angular.isDefined($ctrl.model.project)) {
            if (!_.isEmpty($ctrl.model.project)) {
                params["name"] = $ctrl.model.project;
                projectSearchService.getData(projectSearchService.getConstant('PROJECT_LIST'), params).then(function(response) {
                    if (response.data.length > 0) {
                        var cnt = 0;
                        for (var i = response.data.length - 1; i >= 0; i--) {
                            if (response.data[i].name.toLowerCase() == $ctrl.model.project.toLowerCase()) {
                                cnt++;
                                break;
                            }

                        }
                        if (cnt > 0) {
                            toaster.clear();
                            // popToaster("error", $ctrl.projectMsg.projectExists);
                            toaster.pop("warning", $ctrl.projectMsg.projectExists, "", 3000, 'trustedHtml');
                            $ctrl.model.client = $ctrl.client.clientName;
                            $ctrl.model.brand = $ctrl.client.brandName;
                            $ctrl.model.project = $ctrl.client.name;
                            $ctrl.validation.save = false;
                            $ctrl.updateEmptyProject();

                        }


                    } else {
                        validateInputs();
                        if ($ctrl.validation.save == true) {
                            $ctrl.updateProject();
                        }
                    }
                }, function(error) {
                    $log.debug('components:project-search:error:getlist');
                })
            }
        }


    };

    function validateInputs() {
        toaster.clear();
        if (angular.isUndefined($ctrl.model.client)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.clientEmpty);
            $ctrl.updateEmptyProject();
            return;
        }
        if (_.isEmpty($ctrl.model.client)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.clientEmpty);
            $ctrl.updateEmptyProject();
            return;
        }
        if (angular.isUndefined($ctrl.model.brand)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.brandEmpty);
            $ctrl.updateEmptyProject();
            return;
        }
        if (_.isEmpty($ctrl.model.brand)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.brandEmpty);
            $ctrl.updateEmptyProject();
            return;
        }

        if (angular.isUndefined($ctrl.model.project)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.projectEmpty);
            $ctrl.updateEmptyProject();
            return;
        }
        if (_.isEmpty($ctrl.model.project)) {
            $ctrl.validation.save = false;
            popToaster("error", $ctrl.projectMsg.projectEmpty);
            $ctrl.updateEmptyProject();
            return;
        }
        $ctrl.validation.save = true;


    };

    function popToaster(msgType, title, msg) {
        toaster.pop(msgType, title, msg);
    };

    $ctrl.enableEditing = function enableEditing() {

    };


    $ctrl.deleteProject = function() {
        $log.debug('in:delete  project');
        var modalInstance = $uibModal.open({
            templateUrl: 'app/components/clients/project-item/client.mapping.project.item.modal.html',
            controller: 'clientMappingProjectModalController',
            controllerAs: '$ctrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false,
            resolve: { options: { id: $ctrl.client.id } }
        });

        modalInstance.result.then(function(selectedItem) {
            $ctrl.onDelete({
                $event: {
                    index: $ctrl.client.id
                }
            });

        }, function() {

        });

    };

    // if input invalid empty project object whihc is sent for updation
    $ctrl.updateEmptyProject = function updateEmptyProject() {
        $ctrl.onUpdate({
            $event: {
                project: {}
            }
        });
    };
    $ctrl.focusOut = function focusOut() {
        if (_.isEmpty($ctrl.model.client)) {
            $ctrl.model.client = $ctrl.client.clientName;
            $ctrl.model.brand = $ctrl.client.brandName;
            validateInputs();
        }
    };
    $ctrl.focusOutBrand = function focusOutBrand() {
        if (_.isEmpty($ctrl.model.brand)) {
            $ctrl.model.client = $ctrl.client.clientName;
            $ctrl.model.brand = $ctrl.client.brandName;
            validateInputs();
        }
    };
    $ctrl.focusOutProject = function focusOutProject() {
        if (!_.isEmpty($ctrl.model.project)) {
            validateInputs();
            $ctrl.isProjectExist();
        }

    };

    //update project event sent project object for update/modify
    $ctrl.updateProject = function updateProject() {
        toaster.clear();
        //Call parent
        if ($ctrl.model.client == $ctrl.client.clientName &&
            $ctrl.model.brand == $ctrl.client.brandName &&
            $ctrl.model.project == $ctrl.client.name) {
            return;
        }
        $ctrl.client.name = $ctrl.model.project ? $ctrl.model.project : $ctrl.client.name;
        $ctrl.client.clientName = $ctrl.model.client ? $ctrl.model.client : $ctrl.client.clientName;
        $ctrl.client.brandName = $ctrl.model.brand ? $ctrl.model.brand : $ctrl.client.brandName;
        $ctrl.onUpdate({
            $event: {
                project: {
                    // description: self.description
                    brandId: $ctrl.model.brand.id ? $ctrl.model.brand.id : $ctrl.client.brandId,
                    brandName: $ctrl.model.brand.name,
                    clientId: $ctrl.model.client.id,
                    clientName: $ctrl.model.client.name,
                    projectId: $ctrl.client.id,
                    projectName: $ctrl.model.project ? $ctrl.model.project : $ctrl.client.name

                }
            }
        });
    };

    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
    $ctrl.selectProject = function selectProject(event) {
        toaster.clear();

        $scope.noBrandResults = false;
        $ctrl.model.client = $ctrl.client.clientName;
        $ctrl.model.brand = $ctrl.client.brandName;
        $ctrl.model.project = $ctrl.client.name;
        var liCnt = $ctrl.index + 1;
        $(".project-table li").removeClass("select-row");
        $(".project-table li:eq(" + liCnt + ")").addClass("select-row");
        $(".project-table .noBrand").addClass("ng-hide");
        $(".project-table .noClient").addClass("ng-hide");
        $ctrl.select = true;


        $ctrl.onSelect({
            $event: {
                index: $ctrl.client
            }
        });
        //}
    };




}