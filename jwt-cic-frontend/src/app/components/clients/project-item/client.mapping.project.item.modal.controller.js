angular
    .module('app.common')
    .controller('clientMappingProjectModalController', clientMappingProjectModalController);

clientMappingProjectModalController.$inject = ['$log', '$scope', '$uibModalInstance', 'options'];

function clientMappingProjectModalController($log, $scope, $uibModalInstance, options) {
    var $ctrl = this;
    $ctrl.save = function() {
        $uibModalInstance.close('');
    }

    $ctrl.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
}