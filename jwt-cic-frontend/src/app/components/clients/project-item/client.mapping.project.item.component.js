(function() {
    'use strict';

    angular
        .module('app.components')
        .component('projectItem', {
            templateUrl: 'app/components/clients/project-item/client.mapping.project.item.html',
            controller: clientMappingProjectItemController,
            bindings: {
                index: '<',
                client: '<',
                onSelect: '&',
                selected: '<',
                onDelete: '&',
                onUpdate: '&',
                savepending: '<',
                selctedproject: '<'
            }
        });



})();