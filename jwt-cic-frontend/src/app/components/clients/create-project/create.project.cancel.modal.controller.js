angular
    .module('app.common')
    .controller('clientMappingProjectCancelController', clientMappingProjectCancelController);

clientMappingProjectCancelController.$inject = ['$log', '$scope', '$uibModalInstance'];

function clientMappingProjectCancelController($log, $scope, $uibModalInstance) {
    var $ctrl = this;
    $ctrl.cancelProject = function() {
        $uibModalInstance.dismiss('cancel');
    }

    $ctrl.saveProject = function() {
        $uibModalInstance.close('');
    }
}