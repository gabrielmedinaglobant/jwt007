(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('clientCreateProjectConstant', {
            SAVE_NEW_PROJECT: {
                API: '/projects',
                PREFIX: true
            },
            USER_ROLE_LIST: {
                API: '/roles',
                PREFIX: true
            },
            PROJECT_UPDATE: {
                API: function(data) {
                    return '/projects/' + data.projectId;
                },
                PREFIX: true
            },
          

        });
})();