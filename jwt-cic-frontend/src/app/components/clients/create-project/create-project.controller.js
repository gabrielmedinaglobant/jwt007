angular
    .module('app.common')
    .controller('clientCreateProjectController', clientCreateProjectController);

clientCreateProjectController.$inject = ['$log', '$scope', '$timeout', '$uibModal', '$uibModalInstance', 'clientCreateProjectService', 'clientMappingUsersSearchService', 'projectSearchService', 'toaster', 'UserMappingService', 'clientMappingQueryService'];

function clientCreateProjectController($log, $scope, $timeout, $uibModal, $uibModalInstance, clientCreateProjectService, clientMappingUsersSearchService, projectSearchService, toaster, UserMappingService, clientMappingQueryService) {
    var $ctrl = this;
    $ctrl.model = { projectName: "", brand: {}, client: {}, users: [], currentUser: {}, userSearchTxt: "", queries: [], currentQuery: {}, queryString: "" };
    $ctrl.dataModel = { userRoles: [], userSearchList: [], queryTypes: [] };
    $ctrl.serverModel = { projectId: 0, projectName: "", brandId: 0, users: [], queries: [] };
    $ctrl.validation = { save: false, isCombinationExist: false, assignUser: false, assignQuery: false };
    $ctrl.selectedRole = "Define Role";
    $ctrl.selectedType = "Define Type";
    $ctrl.showTypeUl = false;
    $ctrl.showRowId = false;
    $ctrl.rowId;
    $ctrl.viewModel = { showUl: false, showTypeUl: false };
    $ctrl.isProjectDisabled = true;
    var rowId = 0;
    $ctrl.$onInit = function $onInit() {
        $ctrl.paginationSettings = {
            currentPage: 1,
            recordsPerPage: 5,
            totalItems: 0
        };

    };

    $log.debug('components:brand-search');

    $ctrl.onBrandSelect = function(brand) {
        $ctrl.model.brand = brand;
        $ctrl.isProjectExist();
        validateInputs();
    };

    $(document).click(function(e) {
        if ($(e.target).is("#roleselect") || $(e.target).closest("#roleselect").length) {

        } else {
            $ctrl.viewModel.showUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })

    $(document).click(function(e) {
        if ($(e.target).is("#typeselect") || $(e.target).closest("#typeselect").length) {

        } else {
            $ctrl.viewModel.showTypeUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })

    $(document).click(function(e) {

        if ($(e.target).is("#typeselect_" + $ctrl.rowId) || $(e.target).closest("#typeselect_" + $ctrl.rowId).length) {

        } else {
            $ctrl.showTypeUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })

    $ctrl.getRowId = function(index) {
        $ctrl.rowId = index;
    }


    $ctrl.onUserRoleSelect = function(option) {

        if ($scope.nofnameResults == false) {
            $ctrl.model.currentUser.userRole = option;
            $ctrl.selectedRole = option.displayName;
            validateInputs();
            if ($ctrl.model.users.length == 0) {
                $ctrl.validation.save = false;
            }
        } else {
            $ctrl.validation.assignUser = false;
            $ctrl.selectedRole = "Define Role";
        }

    };






    $scope.$watch("$ctrl.model.userSearchTxt", function(n, o) {
        if (n != o) {
            validateInputs();
        }
    });

    $ctrl.onClientSelect = function(client) {
        $ctrl.model.client = client;
        $ctrl.isProjectExist();
        validateInputs();
    };

    $ctrl.projectNameChange = function() {
        $ctrl.isProjectExist();
        validateInputs();
    }

    $scope.$watch("nofnameResults", function(n, o) {
        if (n == true) {
            $ctrl.validation.assignUser = false;
            $ctrl.validation.save = false;
            $ctrl.selectedRole = "Define Role";
        }
    });

    $ctrl.onAssignUser = function() {
        for (var i = $ctrl.model.users.length - 1; i >= 0; i--) {
            if ($ctrl.model.users[i].id == $ctrl.model.currentUser.id) {
                $ctrl.validation.assignUser = false;
                popToaster("error", "User already assigned to project. Please select a new one.");
                $ctrl.model.userSearchTxt = "";
                $ctrl.selectedRole = "Define Role";

                return;
            }
        }

        // if ($ctrl.model.currentUser.userRole.value == "ROLE_ANALYST") {
        //     var userRoleCnt = 0;
        //     UserMappingService.getData(UserMappingService.getConstant('USER_PROJECT_LIST'), {}, { userId: $ctrl.model.currentUser.nameId }).then(function(response) {
        //         if (response.data.projects.length > 0) {
        //             for (var i = response.data.projects.length - 1; i >= 0; i--) {
        //                 if (response.data.projects[i].role == "ROLE_ANALYST") {
        //                     userRoleCnt++;
        //                     popToaster("error", "User can not have role as 'Analyst' in more than one project");
        //                     $ctrl.selectedRole = "Define Role";
        //                     $ctrl.validation.assignUser = false;
        //                     return;
        //                 }
        //             }
        //             if (userRoleCnt == 0) {
        //                 $ctrl.model.users.push(angular.copy($ctrl.model.currentUser));
        //                 $ctrl.validation.assignUser = false;
        //                 $ctrl.validation.save = true;
        //                 $ctrl.model.currentUser = {};
        //                 $ctrl.model.userSearchTxt = "";
        //                 $ctrl.selectedRole = "Define Role";
        //             }

        //         } else {
        //             $ctrl.model.users.push(angular.copy($ctrl.model.currentUser));
        //             $ctrl.validation.assignUser = false;
        //             $ctrl.validation.save = true;
        //             $ctrl.model.currentUser = {};
        //             $ctrl.model.userSearchTxt = "";
        //             $ctrl.selectedRole = "Define Role";
        //         }
        //     }, function(error) {})
        // } else {
        $ctrl.model.users.push(angular.copy($ctrl.model.currentUser));
        $ctrl.validation.assignUser = false;
        $ctrl.validation.save = true;
        $ctrl.model.currentUser = {};
        $ctrl.model.userSearchTxt = "";
        $ctrl.selectedRole = "Define Role";
        //  }
        // $ctrl.model.userEmailTxt = "";
    };

    function validateQueryInputs() {
        $ctrl.validation.assignQuery = false;
        if (_.isEmpty($ctrl.model.queryString)) {
            $ctrl.validation.assignQuery = false;
            return;
        }

        if ($ctrl.selectedType == "Define Type") {
            $ctrl.validation.assignQuery = false;
            return;
        }
        $ctrl.validation.assignQuery = true;
    }


    $ctrl.validateQuery = function() {
        validateQueryInputs();
        validateInputs();
    }

    $ctrl.onQueryTypeSelect = function(option) {
        $ctrl.model.currentQuery.queryTypeId = option.id;
        $ctrl.model.currentQuery.description = option.description;
        $ctrl.selectedType = option.description;
        validateQueryInputs();
        validateInputs();
    };
    $ctrl.onAssignQuery = function() {

        $ctrl.model.currentQuery.queryString = angular.copy($ctrl.model.queryString);
        $ctrl.model.currentQuery.rowId = rowId++;
        $ctrl.model.queries.push(angular.copy($ctrl.model.currentQuery));
        $ctrl.model.currentQuery = {};
        $ctrl.model.queryString = "";
        $ctrl.validation.assignQuery = false;
        $ctrl.selectedType = "Define Type";
    };

    $ctrl.onQueryLisTypeSelect = function(option, index) { // $ctrl.model.queries
        for (var i = $ctrl.model.queries.length - 1; i >= 0; i--) {
            if ($ctrl.model.queries[i].rowId == index) {
                $ctrl.model.queries[i].queryTypeId = option.id;
                $ctrl.model.queries[i].description = option.description;
            }
        }



    };



    $ctrl.deleteQuery = function(index) {
        for (var i = $ctrl.model.queries.length - 1; i >= 0; i--) {
            if ($ctrl.model.queries[i].rowId == index) {
                $ctrl.model.queries.splice(i, 1);
            }
        }


    };


    function popToaster(msgType, title, msg) {
        toaster.pop(msgType, title, msg, 3000);
    };

    $ctrl.onUnAssignUser = function(item) {
        $ctrl.model.users = _.without($ctrl.model.users, item);
        if ($ctrl.model.users.length == 0) {
            $ctrl.validation.save = false;
        }
    };

    $ctrl.getQueryTypes = function() {
        clientMappingQueryService.getData(clientMappingQueryService.getConstant('QUERY_TYPE_LIST'), {})
            .then(function(response) {
                $ctrl.dataModel.queryTypes = response.data;
            }, function() {});
    };

    $ctrl.getUserRoles = function() {
        clientCreateProjectService.getData(clientCreateProjectService.getConstant('USER_ROLE_LIST'), {})
            .then(function(response) {
                $ctrl.dataModel.userRoles = response.data;
            }, function() {});
    };

    $ctrl.getUserList = function(val, type) {
        return clientMappingUsersSearchService.getData(clientMappingUsersSearchService.getConstant('USER_SEARCH_LIST'), { "query": val }, {}).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:client-search:error:getlist');
        })

    }

    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        $ctrl.model.currentUser = angular.copy($item);
        $ctrl.model.currentUser.userRole = _.first($ctrl.dataModel.userRoles);
        validateInputs();
    }



    $ctrl.save = function() {
        $ctrl.serverModel.projectName = $ctrl.model.projectName;
        $ctrl.serverModel.brandId = $ctrl.model.brand.id;
        _.each($ctrl.model.users, function(o, i) {
            var newObj = {
                "brandId": $ctrl.model.brand.id,
                "brandName": $ctrl.model.brand.name,
                "clientId": $ctrl.model.client.id,
                "clientName": $ctrl.model.client.name,
                "projectId": 0,
                "projectName": $ctrl.model.projectName,
                "role": o.userRole.value,
                "roleId": o.userRole.id,
                "userId": o.id,
                "userNameId": o.nameId
            };
            $ctrl.serverModel.users.push(newObj);
        });
        _.each($ctrl.model.queries, function(o, i) {
            var newObj = {
                "queryString": o.queryString,
                "queryTypeId": o.queryTypeId,
                "projectId": 0
            };
            $ctrl.serverModel.queries.push(newObj);
        });

        clientCreateProjectService.postData(clientCreateProjectService.getConstant('SAVE_NEW_PROJECT'), $ctrl.serverModel)
            .then(function(response) {
                $uibModalInstance.close('');
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/components/message/message.html',
                    controller: 'messageController',
                    controllerAs: '$ctrl',
                    size: 'sm'
                });

            }, function() {
                $uibModalInstance.close('cancel');
            });
    };

    $ctrl.cancel = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'app/components/clients/create-project/create.project.cancel.modal.html',
            controller: 'clientMappingProjectCancelController',
            controllerAs: '$ctrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false

        });

        modalInstance.result.then(function(selectedItem) {
            $uibModalInstance.close('');

        }, function() {

        });
    };

    $ctrl.cancelProject = function() {
        $uibModalInstance.dismiss('cancel');
    }

    $ctrl.saveProject = function() {
        $uibModalInstance.close('');
    }


    $ctrl.isProjectExist = function() {
        var params = {};
        if (angular.isDefined($ctrl.model.projectName)) {
            if (!_.isEmpty($ctrl.model.projectName)) {
                $ctrl.showError = false;
                params["name"] = $ctrl.model.projectName;
                projectSearchService.getData(projectSearchService.getConstant('PROJECT_LIST'), params).then(function(response) {
                    if (response.data.length > 0) {
                        for (var i = response.data.length - 1; i >= 0; i--) {
                            if (response.data[i].name.toLowerCase() == $ctrl.model.projectName.toLowerCase()) {
                                $ctrl.showError = true;
                                $ctrl.validation.isCombinationExist = true;
                                $ctrl.isProjectDisabled = false;
                                break;
                            }
                        }

                        // $timeout(function() {
                        //     $ctrl.showError = false;
                        // }, 10000);
                    } else {
                        $ctrl.showError = false;
                        $ctrl.validation.isCombinationExist = false;
                        $ctrl.isProjectDisabled = false;
                    }
                    validateInputs();
                }, function(error) {
                    $log.debug('components:project-search:error:getlist');
                })


            } else {
                $ctrl.isProjectDisabled = false;
                $ctrl.showError = false;
            }
        }
    };

    function validateInputs() {
        $ctrl.validation.save = false;
        $ctrl.validation.assignQuery = false;
        if (angular.isUndefined($ctrl.model.client)) {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            $ctrl.showError = false;
            return;
        }
        if (_.isEmpty($ctrl.model.client)) {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            $ctrl.showError = false;
            return;
        }
        if (angular.isUndefined($ctrl.model.brand)) {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            $ctrl.showError = false;
            return;
        }
        if (_.isEmpty($ctrl.model.brand)) {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            $ctrl.showError = false;
            return;
        }
        if ($ctrl.validation.isCombinationExist) {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            return;
        }

        if ($ctrl.model.projectName == '') {
            $ctrl.validation.save = false;
            $ctrl.validation.assignUser = false;
            $ctrl.validation.assignQuery = false;
            return;
        }
        validateQueryInputs();
        if ($ctrl.selectedRole == "Define Role") {
            if ($ctrl.model.users.length > 0) { $ctrl.validation.save = true; } else {
                $ctrl.validation.save = false;
            }
            $ctrl.validation.assignUser = false;
            return;
        }
        if (_.isEmpty($ctrl.model.userSearchTxt)) {
            if ($ctrl.model.users.length > 0) {
                $ctrl.validation.save = true;
            } else { $ctrl.validation.save = false; }
            $ctrl.validation.assignUser = false;
            return;
        }
        if (_.isEmpty($ctrl.model.currentUser)) {
            $ctrl.validation.assignUser = false;
            $ctrl.validation.save = true;
            return;
        }



        $ctrl.validation.save = true;
        $ctrl.validation.assignUser = true;





    };

    function init() {
        $ctrl.getUserRoles();
        $ctrl.getQueryTypes();
    };


    init();

}