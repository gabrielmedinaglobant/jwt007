 angular
    .module('app.common')
    .factory('clientCreateProjectService', ['commonService','clientCreateProjectConstant',function(commonService,clientCreateProjectConstant) {

        return angular.extend({
            cnst:clientCreateProjectConstant
        },commonService);

    }]);
