(function() {
    'use strict';

    angular
        .module('app.components')
        .component('userItem', {
            templateUrl: 'app/components/clients/user-item/client.mapping.user.item.html',
            controller: clientMappingUserItemController,
            bindings: {
                index: '<',
                users: '<',
                userroles: '<',
                selected: '<',
                onunAssign: '&',
                onRoleChange: '&',
                onUnSelect: '&'
            }
        });



})();