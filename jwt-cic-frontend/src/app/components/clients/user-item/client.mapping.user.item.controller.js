/* --------- Controller for client mapping assigned user  list item  --------------*|
 |--- common controller for client mapping assigned user  list item ----------------|
 |---------------------------------------------------------------------------------*/
clientMappingUserItemController.$inject = ['$scope', '$log', '$element', '$location', 'jwtClientMappingService', '$timeout', '$anchorScroll', 'toaster'];


function clientMappingUserItemController($scope, $log, $element, $location, jwtClientMappingService, $timeout, $anchorScroll, toaster) {

    var $ctrl = this;
    $ctrl.currentUser = {};
    $ctrl.role = { id: "", value: "" }
    $ctrl.selectedRole = "Define Role";
    $ctrl.viewModel = { showUl: false };
    $ctrl.assignObject = { userNameId: "", role: "" };

    $ctrl.$onInit = function $onInit() {

        $ctrl.selectedRole = $ctrl.users.roleDisplayName;
        $ctrl.selected = false;
    };

    $(document).click(function(e) {
        if ($(e.target).is("#userselects" + $ctrl.users.userId) || $(e.target).closest("#userselects" + $ctrl.users.userId).length) {
            // $timeout(function() {
            //     $location.hash('bottom');
            //     $anchorScroll();
            // });
        } else {
            $ctrl.viewModel.showUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })
    $ctrl.unassignUser = function unassignUser() {
        $ctrl.onunAssign({
            $event: {
                index: $ctrl.index
            }
        });
    };

    $ctrl.onChange = function onChange(option) {
        toaster.clear();
        $ctrl.selectedRole = option.displayName;
        if ($ctrl.users.roleDisplayName != option.displayName) {
            $ctrl.assignObject.userNameId = $ctrl.users.userNameId;
            $ctrl.assignObject.role = option.value;
            $ctrl.onRoleChange({
                $event: {
                    index: $ctrl.assignObject
                }
            });
        }
    };



    $ctrl.selectUser = function selectUser(event) {
        $ctrl.selected = $ctrl.selected ? true : false;
        if (!$ctrl.selected) {
            $ctrl.selectedRole = $ctrl.users.roleDisplayName;
            $ctrl.onUnSelect({
                $event: {
                    index: $ctrl.index
                }
            });
        }
    };

}