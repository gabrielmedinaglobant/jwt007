clientMappingUsersSearchController.$inject = ['$scope', '$log', '$location', '$element', 'filterFilter', 'clientMappingUsersSearchService', '$timeout', '$anchorScroll'];

function clientMappingUsersSearchController($scope, $log, $location, $element, filterFilter, clientMappingUsersSearchService, $timeout, $anchorScroll) {

    var $ctrl = this;
    $ctrl.isAPiCall = true;
    $ctrl.userSearchTxt = "";
    $ctrl.serverModel = { firstName: "", lastName: "", email: "", userNameId: "", role: "" };

    //007
    $ctrl.selectedRole = "Define Role";
    $ctrl.viewModel = { showUl: false };
    $ctrl.validation = { assignUser: false };
    //007
    $(document).click(function(e) {
            if ($(e.target).is("#roleselect") || $(e.target).closest("#roleselect").length || $(e.target).is("#searchUser")) {
                $location.hash('bottom');
                $anchorScroll();

            } else {
                $ctrl.viewModel.showUl = false;
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }
        })
        //007    
    $ctrl.onUserRoleSelect = function(option) {

        if ($scope.nofnameResults == false) {

            $ctrl.serverModel.role = option.value;
            $ctrl.selectedRole = option.displayName;
            if ($ctrl.userSearchTxt != "") {
                $ctrl.validation.assignUser = true;
            }
        } else {
            $ctrl.validation.assignUser = false;
            $ctrl.selectedRole = "Define Role";
        }
    };
    $ctrl.$onInit = function $onInit() {};
    $scope.$watch("$ctrl.userSearchTxt", function(n, o) {
        if (n != o) {
            $ctrl.validation.assignUser = false;
            $ctrl.selectedRole = "Define Role";

        }
    });

    function initData() {
        $ctrl.serverModel = { firstName: "", lastName: "", email: "", userNameId: "", role: "" };
        $ctrl.selectedRole = "Define Role";
        $ctrl.viewModel.showUl = false;
        $ctrl.validation.assignUser = false;
        $ctrl.userSearchTxt = "";
    }

    $scope.$watch("nofnameResults", function(n, o) {
        if (n == true) {
            $ctrl.validation.assignUser = false;
            $ctrl.selectedRole = "Define Role";


        }
    });

    $scope.$watch("$ctrl.project", function(n, o) {
        if (n == '' || n.item.id != o.item.id) {
            initData();
        }
    });





    $ctrl.assignUser = function assignUser() {
        $ctrl.isAPiCall = false;
        $ctrl.onAssign({
            $event: {
                index: $ctrl.serverModel
            }
        });
        initData();
    };
    $ctrl.getUserList = function(val, type) {

        return clientMappingUsersSearchService.getData(clientMappingUsersSearchService.getConstant('USER_SEARCH_LIST'), { "query": val }, {}).then(function(response) {
            return response.data;
        }, function(error) {
            $log.debug('components:client-search:error:getlist');
        });

    }
    $ctrl.onSelectionChange = function($item, $model, $label, $event) {
        $ctrl.isAPiCall = true;
        $ctrl.serverModel.firstName = $item.name;
        $ctrl.serverModel.lastName = $item.lastName;
        $ctrl.serverModel.email = $item.email;
        $ctrl.serverModel.userNameId = $item.nameId;
    }
}