 angular
    .module('app.common')
    .factory('clientMappingUsersSearchService', ['commonService','clientMappingUsersSearchConstant',function(commonService,clientMappingUsersSearchConstant) {
        
        return angular.extend({
            cnst:clientMappingUsersSearchConstant
        },commonService);
        
    }]);

