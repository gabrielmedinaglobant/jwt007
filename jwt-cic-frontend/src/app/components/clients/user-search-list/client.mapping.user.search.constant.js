(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('clientMappingUsersSearchConstant', {
            USER_LIST: {
                API: '/projects/1/users',
                PREFIX: true
            },
            ASSIGNED_USER_LIST: {
                API: function(data) {
                    return '/projects/' + data.projectId + '/users';
                },
                PREFIX: true
            },
            UNASSIGNED_USER_LIST: {
                API: function(data) {
                    return '/projects/' + data.projectId + '/users/unassigned';
                },
                PREFIX: true
            },
            USER_SEARCH_LIST: {
                API: function(data) {
                    return '/users/search';
                },
                PREFIX: true
            }

        });
})();