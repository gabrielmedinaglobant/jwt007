(function() {
    'use strict';
    angular
        .module('app.components')
        .component('userSearchList', {
            templateUrl: 'app/components/clients/user-search-list/client.mapping.user.search.html',
            controller: clientMappingUsersSearchController,
            bindings: {
                unassignlist: '<',
                userroles: '<',
                project: '<',
                onAssign: '&',
                onUserSelect: '&'

            }
        });
})();