(function() {
    'use strict';

    angular
        .module('app.components')
        .component('projectList', {
            templateUrl: 'app/components/clients/project-list/client.mapping.project.html',
            controller: clientMappingProjectsController,
            bindings: {
                list: '<',
                currentpage: '<',
                recordsperpage: '<',
                onDelete: '&',
                onUpdate: '&',
                onSelect: '&',
                savepending: '<',
                selctedproject: '<'

            }
        });



})();