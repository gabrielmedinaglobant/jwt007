clientMappingProjectsController.$inject = ['$scope', '$log', '$element', 'jwtClientMappingService'];

function clientMappingProjectsController($scope, $log, $element, jwtClientMappingService) {

    var $ctrl = this;
    $ctrl.deleteProject = function deleteProject(event) {
        $ctrl.onDelete({
            $event: {
                index: event.index
            }
        });
    };

    $ctrl.updateProject = function updateProject(event) {
        $ctrl.onUpdate({
            $event: {
                project: event.project
            }
        });
    };

    $ctrl.selectProject = function selectProject(event) {
        $ctrl.onSelect({
            $event: {
                item: event
            }
        });
    };

    $scope.$watch('$ctrl.currentpage', function(newPage) {
        //
    });





}