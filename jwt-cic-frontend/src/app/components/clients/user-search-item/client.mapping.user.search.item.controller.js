/* --------- Controller for client mapping unassigned user  list item  --------------*|
 |--- common controller for client mapping unassigned user  list item ----------------|
 |---------------------------------------------------------------------------------*/
clientMappingUserSearchItemController.$inject = ['$scope', '$rootScope', '$location', '$log', '$element', '$timeout', '$anchorScroll'];

function clientMappingUserSearchItemController($scope, $rootScope, $location, $log, $element, $timeout, $anchorScroll) {
    var $ctrl = this;
    $ctrl.role = {};
    $ctrl.viewModel = { showUl: false, canAssign: false };
    $ctrl.selectedRole = "Define Role";
    $ctrl.serverModel = { userNameId: "", role: "", firstName: "", lastName: "", email: "" }
    $ctrl.$onInit = function $onInit() {
        $ctrl.selected = false;
    };

    $(document).click(function(e) {
        if ($(e.target).is("#userselect" + $ctrl.assignusers.id) || $(e.target).closest("#userselect" + $ctrl.assignusers.id).length) {
            $timeout(function() {
                $location.hash('bottom');
                $anchorScroll();
            });
        } else {
            $ctrl.viewModel.showUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })

    $ctrl.onUserRoleSelect = function(option) {

        $ctrl.viewModel.canAssign = true;
        $ctrl.role = option;
        $ctrl.selectedRole = option.displayName;
    };


    $ctrl.assignUser = function assignUser() {
        $ctrl.serverModel.userNameId = $ctrl.assignusers.nameId;
        $ctrl.serverModel.role = $ctrl.role.value;
        $ctrl.serverModel.firstName = $ctrl.assignusers.name;
        $ctrl.serverModel.lastName = $ctrl.assignusers.lastName;
        $ctrl.serverModel.email = $ctrl.assignusers.email;
        $ctrl.onAssign({
            $event: {
                index: $ctrl.serverModel
            }
        });
    };

    $ctrl.selectUser = function selectUser() {
        $ctrl.selected = $ctrl.selected ? true : false;
    };


}