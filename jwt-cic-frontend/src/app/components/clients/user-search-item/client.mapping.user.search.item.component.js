(function() {
    'use strict';

    angular
        .module('app.components')
        .component('userSearchItem', {
            templateUrl: 'app/components/clients/user-search-item/client.mapping.user.search.item.html',
            controller: clientMappingUserSearchItemController,
            bindings: {
                index: '<',
                assignusers: '<',
                userroles: '<',
                selected: '<',
                onAssign: '&',
            }
        });



})();