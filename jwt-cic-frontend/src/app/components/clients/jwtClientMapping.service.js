 angular
     .module('app.common')
     .service('jwtClientMappingService', ['$http', '$q', 'httpService', function($http, $q, httpService) {

         this.getProjectData = function() {
             return httpService.getData('http://jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com:8081/api/v1/projects')
                 .then(function(response) {
                     if (typeof response.data === 'object') {
                         return response.data;
                     } else {

                         $q.reject(response.data);
                     }
                 }, function errorCallback(response) {

                     $q.reject(response.data);
                 });
         }




     }]);