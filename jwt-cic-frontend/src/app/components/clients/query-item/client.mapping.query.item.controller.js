/* --------- Controller for client mapping assigned user  list item  --------------*|
 |--- common controller for client mapping assigned user  list item ----------------|
 |---------------------------------------------------------------------------------*/
clientMappingQueryItemController.$inject = ['$scope', '$log', '$element', '$location', '$timeout', '$anchorScroll', 'toaster'];


function clientMappingQueryItemController($scope, $log, $element, $location, $timeout, $anchorScroll, toaster) {

    var $ctrl = this;
    $ctrl.showTypeUl = false;
    $ctrl.selectedType = "Define Type";
    $ctrl.$onInit = function $onInit() {
        for (var i = $ctrl.querytypes.length - 1; i >= 0; i--) {
            if ($ctrl.querytypes[i].id == $ctrl.query.queryTypeId) {
                $ctrl.selectedType = $ctrl.querytypes[i].description;
                break;
            }
        }

    };

    $(document).click(function(e) {
        if ($(e.target).is("#typeselect_" + $ctrl.query.id) || $(e.target).closest("#typeselect_" + $ctrl.query.id).length) {

        } else {
            $ctrl.showTypeUl = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }

    })


    $ctrl.onQueryLisTypeSelect = function(option) {
        $ctrl.query.queryTypeId = option.id;
        for (var i = $ctrl.querytypes.length - 1; i >= 0; i--) {
            if ($ctrl.querytypes[i].id == option.id) {
                $ctrl.selectedType = $ctrl.querytypes[i].description;
                break;
            }
        }
        $ctrl.updateQuery();

    }
    $ctrl.updateQueryText = function() {
        $ctrl.updateQuery();
    }


    $ctrl.updateQuery = function updateQuery() {
        $ctrl.onUpdateQuery({
            $event: {
                index: $ctrl.index
            }
        });
    };

    $ctrl.deleteQuery = function deleteQuery() {
        $ctrl.onDeleteQuery({
            $event: {
                index: $ctrl.index
            }
        });
    };



}