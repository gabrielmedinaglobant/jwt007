(function() {
    'use strict';

    angular
        .module('app.components')
        .component('queryItem', {
            templateUrl: 'app/components/clients/query-item/client.mapping.query.item.html',
            controller: clientMappingQueryItemController,
            bindings: {
                index: '<',
                query: '<',
                querytypes: '<',
                onUpdateQuery: '&',
                onDeleteQuery: '&'
            }
        });



})();