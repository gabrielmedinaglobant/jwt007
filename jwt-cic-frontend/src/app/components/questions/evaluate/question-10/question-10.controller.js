question10EvaluateController.$inject = ['jwtAppConstant', '$rootScope', '$timeout', '$scope', '$log', '$element', 'D3Service', 'jwtQuestionEvaluate10Service'];

function question10EvaluateController(jwtAppConstant, $rootScope, $timeout, $scope, $log, $element, d3, jwtQuestionEvaluate10Service) {
    var $ctrl = this;
    var CONST_OBJ_Q10 = jwtQuestionEvaluate10Service.getConstant('QAPI');
    $scope.question = CONST_OBJ_Q10.STATIC_INFO;

    var q10timeOut;
    $ctrl.viewModel = { expanded: false };
    $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: null };

    $ctrl.$onInit = function() {
        callApi();
    }
    $ctrl.$onDestroy = function() {
        clearTimeout(q10timeOut);
    }

    function callApi() {
        jwtQuestionEvaluate10Service.getData(jwtQuestionEvaluate10Service.getConstant('QAPI'))
            .then(function(response) {
                if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
                    $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                    $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                }
                $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                $ctrl.data = response.data.value;
                isWorkspaceElement($ctrl.data);
            }, function(error) {
                $ctrl.showLoader();
            });
    }

    function isWorkspaceElement(data) {
        if ($ctrl.isWorkspace) {
            //$scope.showQuestion = true;
            $('.masonry-brick').addClass('big-tile popped-window');
            $ctrl.viewModel.expanded = true;
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
            if (poppedContWidth < 650) {
              poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            newWindWid = Math.round(parseInt(newWindWid) * 0.75);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.65);
            $ctrl.drawChart(newWindWid, newWindHgt, data);
        } else {
            var width = $(".chart-cont-q10").width();
            //$scope.showQuestion = false;
            $ctrl.drawChart(width, 360, data);
        }
    }

    $scope.masonryelementclass = "masonry-brick";

    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function(e, args) {
        $ctrl.initRedrawChart(args.resized);
    });

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function(ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
            $ctrl.showLoader();
            //$scope.showQuestion = !$scope.showQuestion;
            $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
            clearTimeout(q10timeOut);
            q10timeOut = setTimeout(function() {
                $scope.expandTile(data.event, data.elem);
            }, 300);
        }
    });

    /* ** Show loader ** */
    $ctrl.showLoader = function() {
        var elem = $('.chart-cont-q10');
        elem.empty();

        elem.html('<div class="loader"></div>');
    }

    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function(resize, elem) {
        var targetElement, chartCont;
        if (resize && $ctrl.data != undefined) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
            $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
        } else {
            $ctrl.showLoader();
        }
    }

    $scope.expandTile = function(e, elem) {

        setTimeout(function() {
            e.stopPropagation();
            e.preventDefault();

            var targetElement = $(e.target).parents('.masonry-brick');

            targetElement.siblings().removeClass('big-tile');


            targetElement.toggleClass('big-tile');


            var nWidth = Math.round(targetElement.innerWidth());
            var nHeight = Math.round(targetElement.innerHeight());


            $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);
        }, 100)
    };

    /* ** Re rendering the chart on resize of the card ** */
    this.redrawChart = function(w, h, qservice) {
        var elem = $('.chart-cont-q10');
        elem.empty();
        //        if ($ctrl.isWorkspace)
        //            $scope.showQuestion = !$scope.showQuestion;
        this.drawChart(w, h, qservice);
    };

    function min(a, b) { return a < b ? a : b; }

    function max(a, b) { return a > b ? a : b; }

    this.drawChart = function(w, h, data) {
        data = data.candleStickChart.line;

        var margin = { top: 40, right: 20, bottom: 30, left: 50 },
            width, height;


        if (!$ctrl.viewModel.expanded) {
            width = 360;
            height = 359;
        } else {
            width = w;
            height = h;
        }


        String.prototype.format = function() {
            var formatted = this;
            for (var i = 0; i < arguments.length; i++) {
                var regexp = new RegExp('\\{' + i + '\\}', 'gi');
                formatted = formatted.replace(regexp, arguments[i]);
            }
            return formatted;
        };


        var end = new Date();
        var start = new Date(end.getTime() - 1000 * 60 * 60 * 24 * 60);

        var margin = 50;
        var marginbar = {
            top: 430,
            right: 0,
            bottom: 30,
            left: 50,
            float: 'left',
            axisHeight: 3
        };

        var height2 = 500 - marginbar.top - marginbar.bottom;

        var chart = d3.select(".chart-cont-q10")
            .append("svg")
            .attr("class", "chart")
            .attr("width", width - marginbar.bottom)
            .attr("height", height)

        chart = chart.append("g")
            .attr("transform", "translate(15,10)")

        var tickCount = 0;
        if ($ctrl.viewModel.expanded) {
            tickCount = 4;
        } else {
            tickCount = 7;
        }

        var mVal = Math.max.apply(Math, data.map(function(x) {
            return x["high"];
        }));

        var minVal = Math.min.apply(Math, data.map(function(x) {
            return x["low"];
        }));

        var y = d3.scaleLinear()
            .domain([minVal, mVal])
            .nice()
            .range([height - margin, 0]);

        var x = d3.scaleTime()
            .domain([d3.min(data.map(function(d) {
                    var dt = new Date(d.dateValue * 1000);
                    return dt.getTime() + (dt.getTimezoneOffset() * 60 * 1000);
                })),
                d3.max(data.map(function(d) {
                    var dt = new Date(d.dateValue * 1000);
                    return dt.getTime() + (dt.getTimezoneOffset() * 60 * 1000);
                }))
            ])
            .range([margin, width - margin - 30]);


        // Add the X Axis
        chart.append("g")
            .attr("class", "axis x-axis timeSeriesTics")
            .attr("transform", "translate(0," + (height - margin + marginbar.axisHeight) + ")")
            .call(
                d3.axisBottom(x)
                .ticks(tickCount)
                .tickSizeOuter(0)
            )
            .selectAll("text")
            .text(function(d) {
                var date = new Date(d);
                var ts = date.getTime() - (date.getHours() + date.getMinutes() * 60 * 1000);
                var newDate = new Date(ts)
                return ((newDate.getMonth() + 1) + "/" + newDate.getDate()) + "/" + (newDate.getFullYear().toString().substr(2));
            });



        // Add the Y Axis
        chart.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(" + marginbar.left + "," + marginbar.right + marginbar.axisHeight + ")")
            .call(d3.axisLeft(y)
                .tickSizeOuter(0))
            .append("text")
            .attr("class", "axis-info-labels")
            .attr("text-anchor", "middle")
            .attr("transform", "translate(-55," + (height / 2) + ")rotate(-90)")
            .text("Posts");

        var wt = (0.5 * (width - 2 * margin) / data.length);

        chart.selectAll("rect")
            .data(data)
            .enter().append("svg:rect")
            .attr("x", function(d) {
                var dt = new Date(d.dateValue * 1000);
                var t = dt.getTime() + (dt.getTimezoneOffset() * 60 * 1000);
                return (x(t) - wt / 2);
            })
            .attr("y", function(d) { return y(d.high); })
            .attr("height", function(d) { return y(d.low) - y(d.high); })
            .attr("width", function(d) { return wt; })
            .attr("fill", function(d) { return (d.trend == "up") ? $scope.question.colors.positive : $scope.question.colors.negative; });


        if ($ctrl.viewModel.expanded) {
            var secondSVGMargin = {
                width: 810,
                height: 50,
                top: 0,
                right: 20,
                bottom: 30,
                left: 0
            };
            secondSVGMargin.width = width - marginbar.left - 70;

            var x2 = d3.scaleTime()
                .domain([d3.min(data.map(function(d) {
                        var dt = new Date(d.dateValue * 1000);
                        var date = new Date(dt.getTime() * 1000);
                        return dt.getTime();
                    })),
                    d3.max(data.map(function(d) {
                        var dt = new Date(d.dateValue * 1000);
                        var date = new Date(dt.getTime() * 1000);
                        return dt.getTime();
                    }))
                ])
                .range([0, width - margin - 30]);

            var y2 = d3.scaleLinear()
                .domain([d3.min(data.map(function(x) { return x["value"]; })), d3.max(data.map(function(x) { return x["value"]; }))])
                .range([height2, 0]);

            var area2 = d3.area()
                .curve(d3.curveMonotoneX)
                .x(function(d) {
                    var dt = new Date(d.dateValue * 1000);
                    var t = dt.getTime();
                    return x2(t);
                })
                .y0(height2)
                .y1(function(d) { return y2(d.value); });

            var svg = d3.select(".chart-cont-q10").append("svg")
                .attr("class", "timeseries-svg")
                .attr("width", secondSVGMargin.width + 60)
                .attr("height", secondSVGMargin.height + 30)
                .style("fill", "none")
                .style("border-bottom", "1px solid " + $scope.question.timeColors.border)
                .style("stroke-width", "1")


            var timeSeries = svg.append("g")
                .attr("class", "time-series")
                .attr("width", secondSVGMargin.width - 100)
                .attr("transform", "translate(0, 20)");


            timeSeries.append("path")
                .datum(data)
                .style("stroke", "#49bfab")
                .attr("fill", "#1b353c")
                .style("fill", "yes")
                .attr("d", area2);


            timeSeries.append("g")
                .attr("class", "axis axis--x q10-axis")
                .call(d3.axisTop(x2)
                    .ticks(d3.timeMonth)
                    .tickSizeOuter(0))
                .selectAll("text")
                .text(function(d) {
                    var locale = "en-us";
                    var month = d.toLocaleString(locale, { month: "short" });
                    return ((month) + "/" + (d.getFullYear().toString().substr(2)));
                });



            timeSeries.append("g")
                .attr("width", secondSVGMargin.width - 100)
                .attr("class", "brush")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [(width - 70), secondSVGMargin.height - 1]
                    ])
                    .on("end", brushended));


            data.forEach(function(value, index) {

                var date = new Date(value.dateValue * 1000);
                value.actualDate = date;
            });


            function brushended() {
                if (!d3.event.sourceEvent) return; // Only transition after input.
                if (!d3.event.selection) return; // Ignore empty selections.
                var noOfTicks = 0;
                var d0 = d3.event.selection.map(x2.invert),
                    d1 = d0.map(d3.timeDay.round);
                // debugger;
                // If empty when rounded, use floor & ceil instead.
                if (d1[0] >= d1[1]) {
                    d1[0] = d3.timeDay.floor(d0[0]);
                    d1[1] = d3.timeDay.offset(d1[0]);
                }

                d3.select(this).transition().call(d3.event.target.move, d1.map(x2));


                var timeDiff = Math.abs(d1[1].getTime() - d0[0].getTime());

                noOfTicks = Math.ceil(timeDiff / (1000 * 3600 * 24));
                noOfTicks = Math.max(1, noOfTicks - 1);
                noOfTicks = (noOfTicks > 10) ? 10 : noOfTicks;

                x.domain([]);
                x.domain([d1[0], d1[1]]);
                y2.domain(y.domain());

                x.range([100, width - margin - 30]);

                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                if (diffDays >= 5) {


                    // If brush is empty then reset the Xscale domain to default, if not then make it the brush extent

                    // draw the x axis
                    chart.selectAll("g.x-axis") // replot xAxis with transition when brush used
                        .attr("transform", "translate(0," + (height - margin + marginbar.axisHeight) + ")")
                        .transition()
                        .call(d3.axisBottom(x)
                            .ticks(noOfTicks)
                            .tickSizeOuter(0))
                        .selectAll("text")
                        .text(function(d) {
                            var date = new Date(d);
                            var ts = date.getTime() - (date.getHours() + date.getMinutes() * 60 * 1000);
                            var newDate = new Date(ts)
                            return ((newDate.getMonth() + 1) + "/" + newDate.getDate()) + "/" + (newDate.getFullYear().toString().substr(2));

                        });


                    var newArray = _.filter(data, function(d) {
                        if (d.actualDate >= d0[0] && d.actualDate <= d1[1]) {
                            return d;
                        }
                    });

                    var rectWidth = noOfTicks / 2;

                    // draw the rect too
                    chart.selectAll("rect").remove();

                    var wt = (0.5 * (width - 2 * margin) / newArray.length);

                    chart.selectAll("rect")
                        .data(newArray)
                        .enter().append("svg:rect")
                        .attr("x", function(d) {
                            //  var dt = new Date(d.actualDate);
                            //  var t = dt.getTime() + (dt.getTimezoneOffset() * 60 * 1000);
                            //  var temp = (0.5 * (width - 2 * margin) / newArray.length);
                            //  var k = t - (temp / 2);
                            //  return x(k);

                            var dt = new Date(d.actualDate);
                            var t = dt.getTime() + (dt.getTimezoneOffset() * 60 * 1000);
                            return (x(t) - wt / 2);
                        })
                        .attr("y", function(d) { return y(d.high); })
                        .attr("height", function(d) { return y(d.low) - y(d.high); })
                        .attr("width", function(d) {
                            return wt;
                        })
                        .attr("fill", function(d) { return (d.trend == "up") ? $scope.question.colors.positive : $scope.question.colors.negative; });
                }
            }

        }
    }
}
