 /* --------- Service for Question 9  --------------*|
 |---  Q10: Has sentiment changed day over day?--------|
 |------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate10Service', ['commonService', 'jwtQuestionEvaluate10Constant', function(commonService, jwtQuestionEvaluate10Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate10Constant
        }, commonService);

    }]);


// angular
//     .module('app.common')
//     .service('jwtQuestionEvaluate10Service', ['$http', '$q', 'httpService', function($http, $q, httpService) {
//
//         this.getChartData = function() {
//             return httpService.getData('/questions/volume-changed-day-over-day')
//                 .then(function(response) {
//                     if (typeof response.data === 'object') {
//                         return response.data;
//                     } else {
//                         console.log("error");
//                         $q.reject(response.data);
//                     }
//                 }, function errorCallback(response) {
//                     console.log("error");
//                     $q.reject(response.data);
//                 });
//         }
//
//
//     }]);