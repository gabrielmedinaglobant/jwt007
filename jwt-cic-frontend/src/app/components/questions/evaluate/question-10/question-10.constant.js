(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate10Constant', {
            QAPI: {
                API: '/questions/volume-changed-day-over-day',
                PREFIX: true,
                STATIC_INFO: {
                    'title': 'Volume',
                    'qText': 'Has Volume changed day over day?',
                    'colors': { 'positive': '#59a95b', 'negative': '#bd514c'},
                    'timeColors': {'area': '#21353c', 'border': '#2f4351'},
                    'description': 'A count of overall activity for your brand during a set time period.',
                    'lblRefreshDate': 'Last Refresh Date',
                    'lblDateRange': 'Data Date Range',
                    'lblFrom': 'From',
                    'lblTo': 'To'
                }
            }

        });
})();
