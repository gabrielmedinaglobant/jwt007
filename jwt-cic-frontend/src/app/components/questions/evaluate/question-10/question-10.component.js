(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate10', {
      templateUrl: 'app/components/questions/evaluate/question-10/evaluate.question-10.html',
      controller: question10EvaluateController,
      bindings: {
        data: '<',
        isWorkspace: '<',
        showWorkspaceSelect: '<',
        jwtChartId: '<'
      }
    });




})();