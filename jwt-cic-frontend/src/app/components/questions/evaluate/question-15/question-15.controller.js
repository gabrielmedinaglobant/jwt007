/* --------- Controller for Question 15 --------------*|
 |---  Q15: Where is the conversation happening? --------|
 |----------------------------------------------------*/
questionEvaluate15Controller.$inject = ['$scope', '$rootScope', 'jwtAppConstant', '$element', '$log', 'jwtQuestionEvaluate15Service'];

function questionEvaluate15Controller($scope, $rootScope, jwtAppConstant, $element, $log, jwtQuestionEvaluate15Service) {
    var $ctrl = this;

    $ctrl.viewModel = { expanded: false };
    $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: null };

    $scope.lineAdjust = 15;
    var q15timeOut, q15timeOut2;
    $scope.question = jwtQuestionEvaluate15Service.getConstant('INFO');

    $ctrl.$onInit = function() {
        callApi();
    }

    $ctrl.$onDestroy = function() {
            clearTimeout(q15timeOut);
            clearTimeout(q15timeOut2);
        }
        /* ** Getting data from the service ** */
    function callApi() {
        jwtQuestionEvaluate15Service.getData(jwtQuestionEvaluate15Service.getConstant('QAPI'))
            .then(function(response) {
                var data = response.data.value;
                if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
                    $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                    $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                }
                $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });

                var dataArray = data.pieChart.values;
                $ctrl.data = dataArray;
                $ctrl.data.sort(function(a, b) {
                    var textA = a.displayName;
                    var textB = b.displayName;
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });

                $ctrl.data.forEach(function(element) {
                    element.indicatorValue = jwtUtil.formatWithCommas(element.indicatorValue, "f");
                }, this);

                isWorkspaceElement($ctrl.data);

            }, function(error) {
                $ctrl.showLoader();
            });
    }
    /* ** Class for Grid layout ** */
    $scope.masonryelementclass = "masonry-brick";


    function isWorkspaceElement(data) {
        if ($ctrl.isWorkspace == true) {
            //$scope.showQuestion = true;
            $('.masonry-brick').addClass('big-tile popped-window');
            $ctrl.viewModel.expanded = true;
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
            if (poppedContWidth < 650) {
                poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            newWindWid = Math.round(parseInt(newWindWid) * 0.70);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
            $ctrl.drawChart(newWindWid, newWindHgt, data);
        } else {
            var width = $('.chart-cont-q15').width();
            $ctrl.drawChart(400, 360, data);
            //$scope.showQuestion = false;
        }
    }


    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function(e, args) {
        $ctrl.initRedrawChart(args.resized);
    });

    /* ** Show loader ** */
    $ctrl.showLoader = function() {
        var elem = $('.chart-cont-q15');
        elem.empty();
        elem.html('<div class="loader"></div>');

    }

    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function(resize, elem) {
        var targetElement, chartCont;
        if (resize && $ctrl.data != undefined) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
            $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
        } else {
            $ctrl.showLoader();
        }
    }

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function(ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
            $ctrl.showLoader();
            //$scope.showQuestion = !$scope.showQuestion;
            $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
            clearTimeout(q15timeOut);
            q15timeOut2 = setTimeout(function() {
                $scope.expandTile(data.event, data.elem);
            }, 300);
        }
    });

    /* ** Function for expanding the card ** */
    $scope.expandTile = function(e, elem) {

        setTimeout(function() {
            e.stopPropagation();
            e.preventDefault();

            var targetElement = $(e.target).parents('.masonry-brick');

            targetElement.siblings().removeClass('big-tile');

            targetElement.toggleClass('big-tile');
            var nWidth = Math.round(targetElement.innerWidth());
            var nHeight = Math.round(targetElement.innerHeight());
            if (!$ctrl.viewModel.expanded) {
                nHeight = (nHeight > 360) ? 360 : nHeight;
                nWidth = (nWidth < 200) ? 200 : nWidth;

            }
            $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

        }, 100)

    };

    /* ** Re rendering the chart on resize of the card ** */
    this.redrawChart = function(w, h, qservice) {
            var elem = $('.chart-cont-q15');
            elem.empty();
            //        if ($ctrl.isWorkspace)
            //            $scope.showQuestion = !$scope.showQuestion;
            this.drawChart(w - 30, h, qservice);
        },

        /* --------- Rendering the chart  -------------------*|
         |- Params : Width, Height and data from the service --|
         |-----------------------------------------------------*/
        this.drawChart = function(w, h, data) {

            //declare the variables
            var margin, width, height, circleRadius, fontSize;
            var nameSize, numberSize;
            numberSize = "20px";
            nameSize = "16px";


            // Set the dimensions of the canvas / graph
            margin = { top: 15, right: 30, bottom: 10, left: 15 };

            if (!$ctrl.viewModel.expanded) {
                width = w - margin.left - margin.right;
                height = h - margin.top - margin.bottom;
                circleRadius = 60;
            } else {
                width = w - margin.left - 30;
                height = h - margin.top - margin.bottom;
                circleRadius = 40;
            }

            var radius;
            radius = Math.min(width, height) / 2;

            if (jwtUtil.getViewport().width <= '1280' && jwtUtil.getViewport().width > '800') {
                radius = Math.min(width, height) / 2.5;
            }

            $ctrl.textColor = $scope.question.textColor;

            var color = d3.scaleOrdinal()
                .range($scope.question.textColor);


            var pie = d3.pie()
                .value(function(d) { return d.value; })(data);

            var arc = d3.arc()
                .outerRadius(radius - circleRadius)
                .innerRadius(0);

            var labelArc = d3.arc()
                .outerRadius(radius - 40)
                //.innerRadius(radius - 40);

            var svg = d3.select(".chart-cont-q15")
                .append("svg")
                .attr("width", w)
                .attr("height", h)
                .append("g")
                .attr("width", w)
                .attr("height", h)

            if (!$ctrl.viewModel.expanded) {
                svg.attr("transform", "translate(" + (w / 3.5) + "," + h / 2 + ")"); // Moving the center point. 1/2 the width and 1/2 the height

            } else {
                svg.attr("transform", "translate(" + (w / 3) + "," + h / 2 + ")"); // Moving the center point. 1/2 the width and 1/2 the height
            }

            var g = svg.selectAll("arc")
                .data(pie)
                .enter().append("g")
                .attr("class", "arc");

            g.append("path")
                .attr("d", arc)
                .style("fill", function(d) { return color(d.data.displayName); })
                .style("stroke", $scope.question.colors.border)
                .style("stroke-width", "3px");



        };

}