(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate15Constant', {
            QAPI: {
                API: '/questions/conversation-happening',
                PREFIX: true
            },
            INFO: {
                'title': 'Conversation Channels',
                'qText': 'Where is the conversation happening?',
                'description' : 'Channels determine where brand content and conversations are originating.',
                'textColor': ["#e1c942", "#96c1b0", "#c17f42", "#9983f5"],
                'colors': {'category': '#53829f', 'border': '#172530'}
            }

        });
})();
