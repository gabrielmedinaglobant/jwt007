(function () {
    'use strict';

    angular
        .module('app.components')
        .component('jwtQuestionEvaluate15', {
            controller: questionEvaluate15Controller,
            templateUrl: 'app/components/questions/evaluate/question-15/evaluate.question-15.html',
            bindings: {
                data: '<',
                isWorkspace: '<',
                showWorkspaceSelect: '<',
                jwtChartId: '<'
            }
        });

})();