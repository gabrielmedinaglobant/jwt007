/* --------- Service for Question 15 --------------*|
|---  Q15: Where is the conversation happening?--|
|------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate15Service', ['commonService', 'jwtQuestionEvaluate15Constant', function(commonService, jwtQuestionEvaluate15Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate15Constant
        }, commonService);

    }]);
//angular
//    .module('app.common')
//    .service('jwtQuestionEvaluate15Service', ['$http', '$q', 'httpService', function($http, $q, httpService) {
//
//        /* --------- get chart data  --------------*/
//        this.getData = function() {
//            return httpService.getData('/questions/conversation-happening')
//                .then(function(response) {
//                    if (typeof response.data === 'object') {
//                        return response.data;
//                    } else {
//                        console.log("error");
//                        $q.reject(response.data);
//                    }
//                }, function errorCallback(response) {
//                    console.log("error");
//                    $q.reject(response.data);
//                });
//        }
//    }]);