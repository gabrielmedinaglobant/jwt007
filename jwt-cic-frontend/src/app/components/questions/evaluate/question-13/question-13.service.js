/* --------- Service for Question 13 --------------*|
|---  Q13: How do people feel about my brand?--|
|------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate13Service', ['commonService', 'jwtQuestionEvaluate13Constant', function(commonService, jwtQuestionEvaluate13Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate13Constant
        }, commonService);

    }]);

//angular
//   .module('app.common')
//   .service('jwtQuestionEvaluate13Service',['$http','$q','httpService','jwtAppConstant',function($http,$q,httpService,jwtAppConstant){
//
//   // jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//   /* --------- get chart data  --------------*/
//       this.getData = function(){
//           return httpService.getData(jwtAppConstant.evaluate.q13.questionAPI)
//       .then(function(response){
//         if(typeof response.data === 'object' ) {
//           return response.data;
//         } else
//         {
//           $q.reject(response.data);
//         }
//       },function errorCallback(response) {
//           $q.reject(response.data);
//       });
//       }
//}]);
