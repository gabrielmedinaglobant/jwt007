(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate13', {
      controller: questionEvaluate13Controller,
      templateUrl: 'app/components/questions/evaluate/question-13/evaluate.question-13.html',
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'
      }
    });

})();
