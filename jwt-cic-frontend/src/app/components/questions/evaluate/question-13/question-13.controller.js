/* --------- Controller for Question 13  --------------*|
 |---  Q13: How do people feel about my brand?----|
 |----------------------------------------------------*/

questionEvaluate13Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$log', '$element', 'D3Service', 'jwtQuestionEvaluate13Service'];

function questionEvaluate13Controller($scope, jwtAppConstant, $rootScope, $log, $element, d3, jwtQuestionEvaluate13Service) {
  var $ctrl = this;

  //$ctrl.viewModel.expanded = false;
    $ctrl.viewModel = {expanded:false};
    $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  $scope.question = jwtQuestionEvaluate13Service.getConstant('INFO');
  //$ctrl.expanded = $ctrl.viewModel.expanded;
  $ctrl.cloudWidth = '';
  $ctrl.cloudHeight = '';
  $ctrl.type = null;
  $ctrl.dataLoad = false;
  $ctrl.typeSelected = null;
    var q13timeOut,q13timeOut2;
  /* ** Getting data from the service ** */

  $ctrl.$onInit = function () {
    callApi();
  }

    $ctrl.$onDestroy = function(){
      clearTimeout(q13timeOut);
      clearTimeout(q13timeOut2);
    }

function callApi(){
  jwtQuestionEvaluate13Service.getData(jwtQuestionEvaluate13Service.getConstant('QAPI'))
    .then(function (response) {
      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      }
	     $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      var data = response.data.value;
      if (data != undefined) {
        $ctrl.dataLoad = true;
      }
      $ctrl.data = data;
      $scope.showQuestion = false;
      var dataArray = [];
      $.map(data, function (key, index) {
        $.map(key.words, function (value, j) {
          var wordSize = value.size;
          var wordSizeSM = value.size;
          if (wordSize < 2) {
            wordSize = 2;
          }
          if (wordSize < 3) {
            wordSizeSM = 3;
          }
          var fSmSize = wordSizeSM * 3;
          var fExSize = wordSize * 5;
          dataArray.push({ "option": key.option, "value": value.value, "feelingRank": value.feelingRank, "sizeSm": fSmSize, "sizeEx": fExSize });
        });
      });
      $ctrl.data = dataArray;

      //var drawChart = function () {
        isWorkspaceElement($ctrl.data);
      //}
      //clearTimeout(q13timeOut);
      //q13timeOut = setTimeout(drawChart, 100);
    }, function (error) {
      $ctrl.showLoader();
    });
}
  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace) {
      $scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      //$ctrl.expanded = $ctrl.viewModel.expanded;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.75);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      var width = $('.chart-cont-q13').width();
      $ctrl.drawChart(width, 360, data);
      $scope.showQuestion = false;
    }
  }

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
        $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
        clearTimeout(q13timeOut2);
      q13timeOut2 = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.loaderPlaceHolder');
    elem.empty();

    $('div[class^="legends-"]').hide();
    $('.wordCloud-options').parent().hide();

    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data != undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();
    $ctrl.type = null;

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
//    if (targetElement.hasClass('big-tile')) {
//      $ctrl.viewModel.expanded = false;
//      $ctrl.expanded = $ctrl.viewModel.expanded;
//    } else {
//      $ctrl.viewModel.expanded = true;
//      $ctrl.expanded = $ctrl.viewModel.expanded;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.width());
    var nHeight = Math.round(targetElement.height());

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q13 .wordCloud-chart');
    elem.empty();
    $('.loaderPlaceHolder').empty();
    if ($ctrl.isWorkspace)
      $scope.showQuestion = !$scope.showQuestion;

    $('div[class^="legends-"]').show();
    $('.wordCloud-options').parent().show();

    this.drawChart(w, h, qservice);
  };



  /* --------- Rendering the chart  -------------------*|
   |- Params : Width, Height and data from the service --|
   |-----------------------------------------------------*/
  this.drawChart = function (w, h, data, type) {
    if (type == $ctrl.typeSelected) {
      type = undefined;
      $ctrl.type = undefined;
    }
    if ($ctrl.cloudWidth == undefined || $ctrl.cloudWidth == "") {
      $ctrl.cloudWidth = w;
      $ctrl.cloudHeight = h;
    }
    var parseData = [];
    if (type != undefined) {
      $ctrl.typeSelected = type;
      $.map(data, function (value, j) {
        if (value.option == type) {
          parseData.push(value);
        }
      });
    } else {
      $ctrl.typeSelected = undefined;
      parseData = data;
    }


    var margin = { top: 20, right: 40, bottom: 20, left: 40 }, width, height;
    width = $('.chart-cont-q13 .wordCloud-chart').width();
    height = $('.chart-cont-q13 .wordCloud-chart ').height();
    var padAdjust = 2;
    var transWidth = width;
    var transHeight = height;
    if ($ctrl.viewModel.expanded) {
      //padAdjust = 5;
      width = width - 20;
      transWidth = width / 1.95;
      transHeight = height / 2;
    }
    else {
      width = width ;
      height = height;
      transWidth = width / 2;
      transHeight = height / 2;
    }

    var color = $scope.question.color;
    d3.layout.cloud().size([width, height - margin.top])
      .words(parseData)
      .padding(padAdjust)
      .rotate(0)
      .fontSize(function (d) {
        if (!$ctrl.viewModel.expanded) {
          return d.sizeSm;
        } else {
          return d.sizeEx;
        }
      })
      .on("end", draw)
      .start();

    function draw(words) {
      d3.select(".wordCloud-chart").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "wordcloud")
        .append("g")
        // without the transform, words words would get cutoff to the left and top, they would
        // appear outside of the SVG area
        .attr("transform", "translate(" + transWidth + "," + transHeight + ")")
        .selectAll("text")
        .data(words)
        .enter().append("text")
        .style("font-size", function (d) { return d.size + "px"; })
        .style("fill", function (d) { return color[d.feelingRank - 1]; })
        .attr("text-anchor", "middle")
        .attr("transform", function (d) {
          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function (d) { return d.value; });
    }

  };

  $ctrl.changeType = function (type) {
    $ctrl.type = type;
    var elem = $('.chart-cont-q13 .wordCloud-chart');
    elem.empty();
    $ctrl.drawChart($ctrl.cloudWidth, $ctrl.cloudHeight, $ctrl.data, type);
  }

}
