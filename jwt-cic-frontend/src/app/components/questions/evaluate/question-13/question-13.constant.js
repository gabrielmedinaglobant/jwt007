(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate13Constant', {
            QAPI: {
                API: '/questions/people-feel-about-brand',
                PREFIX: true
            },
            INFO: {
                'title': 'Like Feel Do',
                'qText': 'How do people feel about my brand?',
                'questionAPI': '/questions/people-feel-about-brand',
                'color' : ['#bd514c', '#d7803c', '#eccb30', '#88a959', '#59a95b'],
                'description' : 'Key topics that the brand is mentioned when combined to synonym keywords of  "Like" "Feel" "Do".'
              }

        });
})();
