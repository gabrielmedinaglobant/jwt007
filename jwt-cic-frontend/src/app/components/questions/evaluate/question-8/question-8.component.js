

(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate8', {
      controller: questionEvaluate8Controller,
      templateUrl: 'app/components/questions/evaluate/question-8/evaluate.question-8.html',
      bindings: {
        data: '<',
        isWorkspace: '<',
        showWorkspaceSelect: '<',
        jwtChartId: '<'
      }
    });

})();
