 /* --------- Service for Question 8 --------------*|
|---  Q8: What are the key themes they talk about?--|
|------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate8Service', ['commonService', 'jwtQuestionEvaluate8Constant', function(commonService, jwtQuestionEvaluate8Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate8Constant
        }, commonService);

    }]);

// angular
//    .module('app.common')
//    .service('jwtQuestionEvaluate8Service',['$http','$q','httpService',function($http,$q,httpService){
//    //  http://jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com:8080/api/v1/questions/key-themes-talk-about
//		// jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//		/* --------- get chart data  --------------*/
//        this.getDonutChartData = function(){
//            return httpService.getData('/questions/key-themes-talk-about')
//				.then(function(response){
//					if(typeof response.data === 'object' ) {
//						return response.data;
//					} else
//					{
//                        console.log("error");
//						$q.reject(response.data);
//					}
//				},function errorCallback(response) {
//                    console.log("error");
//					$q.reject(response.data);
//				});
//        }
//}]);

