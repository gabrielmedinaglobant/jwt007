/* --------- Controller for Question 8  --------------*|
 |---  Q8: What are the key themes they talk about?----|
 |----------------------------------------------------*/

questionEvaluate8Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$log', '$element', 'D3Service', 'jwtQuestionEvaluate8Service'];

function questionEvaluate8Controller($scope, jwtAppConstant, $rootScope, $log, $element, d3, jwtQuestionEvaluate8Service) {
  var $ctrl = this;
  $ctrl.evaluateQuestion8Data = {};
  $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  $ctrl.viewModel = {expanded:false};
  $scope.question = jwtQuestionEvaluate8Service.getConstant('INFO');
    var q8timeOut;

  $ctrl.$onInit = function () {
    callApi();
  }
    $ctrl.$onDestroy = function(){
      clearTimeout(q8timeOut);
    }
  /* ** Getting data from the service ** */
function callApi(){
  jwtQuestionEvaluate8Service.getData(jwtQuestionEvaluate8Service.getConstant('QAPI'))
    .then(function (response) {
      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' } );
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' } );
      }
     $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' } );

      $ctrl.data = response.data.value;
      isWorkspaceElement($ctrl.data);
    }, function (error) {
      $ctrl.showLoader();
    });
}
  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace) {
      //$scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.75);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      $ctrl.drawChart(180, 360, data);
      //$scope.showQuestion = false;
    }
  }

  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
      $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
        clearTimeout(q8timeOut);
      q8timeOut = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q8');
    elem.empty();

    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data != undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }


  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
//    if (targetElement.hasClass('big-tile')) {
//      $ctrl.viewModel.expanded = false;
//    } else {
//      $ctrl.viewModel.expanded = true;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.width());
    var nHeight = Math.round(targetElement.height());

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q8');
    elem.empty();
//    if ($ctrl.isWorkspace)
//      $scope.showQuestion = !$scope.showQuestion;
    this.drawChart(w, h, qservice);
  };



  /* --------- Rendering the chart  -------------------*|
   |- Params : Width, Height and data from the service --|
   |-----------------------------------------------------*/
  this.drawChart = function (w, h, data) {

    var width, height;
    if (!$ctrl.viewModel.expanded) {
      width = 95;
      height = 100;
    } else {
      width = 105;
      height = 100;
    }

    var radius = Math.min(width, height) / 2;
    var positivityText, negativitytext;
    var conutColor1 = ['#49bfab', "#1e3c41"];
    var conutColor2 = ['#ffffff', "#39454e"];
    var colorchange = true;
    var themesCount = 0;

    for (var key in data) {
      // skip loop if the property is from prototype
      if (!data.hasOwnProperty(key)) continue;
      var obj = data[key];
      for (var prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        if (prop == "name")
          var textTitle = obj[prop];
        if (prop == "pieChart")
          var percentageTextVal = obj[prop].indicator;
        if (prop == "positivityIndicatorValue")
          positivityText = Math.round(obj[prop]);
        if (prop == "negativityIndicatorValue")
          negativitytext = Math.round(obj[prop]);

      }

      var dataset = {
        percentData: [percentageTextVal, 100 - percentageTextVal],
      };
      var color;
      if (colorchange) {
        color = $scope.question.donutColor.colorSet1;
        colorchange = false;
      }
      else {
        color = $scope.question.donutColor.colorSet2;
        colorchange = true;
      }

      var pie = d3.pie()
        .sort(null);
      var arc;

      if ($ctrl.viewModel.expanded) {
        arc = d3.arc()
          .innerRadius(radius - 10)
          .outerRadius(radius - 24);
      } else {
        arc = d3.arc()
          .innerRadius(radius - 10)
          .outerRadius(radius - 24);

      }

      if ($ctrl.viewModel.expanded) {
        var parentDiv = d3.select(".chart-cont-q8").append("div").attr("class", "themes-graph-expand");
        var donutChartDiv = parentDiv.append("div").attr("class", "themes-graph-expand-donut");
        var moodDiv = parentDiv.append("div").attr("class", "themes-graph-expand-mood");

        var svg = donutChartDiv.append("svg")
          .attr("width", width)
          .attr("height", height);

        var nameTextTitle = svg.append("g")
          .attr("transform", "translate(" + width / 2 + "," + (height - (height - 13)) + ")")
          .append("text").attr("class", "themes-graph-expand-title")
          .style("fill", color[2])
          .text(textTitle);


        var percentageText = svg.append("g")
          .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 10) + ")")
          .append("text").attr("class", "themes-graph-expand-percentVal")
          .attr("dy", ".3em")
          .style("fill", color[2])
          .text(percentageTextVal + "%");

        var path = svg.append("g")
          .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 10) + ")")
          .selectAll("path")
          .data(pie(dataset.percentData))
          .enter().append("path")
          .style("fill", function (d, i) { return color[i]; })
          .attr("d", arc)
          .style('stroke', "none");

        //-------- rendering moods ----------------//

        var smilyDiv = moodDiv.append("div")
          .attr("class", "smiley-positive");
        var smily = smilyDiv.append("i")
          .attr("class", "icon-jwt-Smiley");
        var smilyText = smilyDiv.append("span").attr("class", "spacer10lft")
          .text(positivityText + "%");
        var smilySadDiv = moodDiv.append("div")
          .attr("class", "smiley-negative");
        var smilySad = smilySadDiv.append("i")
          .attr("class", "icon-jwt-Smiley-sad");
        var smilySadText = smilySadDiv.append("span").attr("class", "spacer10lft")
          .text(negativitytext + "%");

      }
      else {
        if (themesCount < 7) {
          var parentDiv = d3.select(".chart-cont-q8").append("div").attr("class", "themes-graph-colaps");
          var svg = parentDiv.append("svg")
            .attr("width", width)
            .attr("height", height);

          var nameTextTitle = svg.append("g")
            .attr("transform", "translate(" + width / 2 + "," + (height - (height - 13)) + ")")
            .append("text").attr("class", "themes-graph-colaps-title")
            .style("fill", color[0])
            .text(textTitle);

          var percentageText = svg.append("g")
            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 10) + ")")
            .append("text").attr("class", "themes-graph-colaps-percentVal")
            .attr("dy", ".3em")
            .style("fill", color[0])
            .text(percentageTextVal + "%");

          var path = svg.append("g")
            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 10) + ")")
            .selectAll("path")
            .data(pie(dataset.percentData))
            .enter().append("path")
            .style("fill", function (d, i) { return color[i]; })
            .attr("d", arc)
            .style('stroke', "none");

        }

      }
      themesCount = themesCount + 1;
    }

    //----------------- Rendring mood References ---------------//
    if ($ctrl.viewModel.expanded) {
      var references = d3.select(".chart-cont-q8").append("div").attr("class", "themes-graph-expand")
        .append("div").attr("class", "themes-graph-refer");
      references.append("span").text("References").attr("class", "themes-graph-referText")
      var referencesPos = references.append("div").attr("class", "themes-graph-referPos");
      var smilyRefDiv = referencesPos.append("div")
        .attr("class", "smiley-positive");
      smilyRefDiv.append("i")
        .attr("class", "icon-jwt-Smiley");
      smilyRefDiv.append("span").text("Positive").attr("class", "themes-graph-referText spacer10lft")
      var smilySadRefDiv = referencesPos.append("div")
        .attr("class", "smiley-negative");
      smilySadRefDiv.append("i")
        .attr("class", "icon-jwt-Smiley-sad");
      smilySadRefDiv.append("span").text("Negative").attr("class", "themes-graph-referText spacer10lft")
    }


  };

}
