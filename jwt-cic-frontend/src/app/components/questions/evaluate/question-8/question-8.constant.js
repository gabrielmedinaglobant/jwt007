(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate8Constant', {
            QAPI: {
                API: '/questions/key-themes-talk-about',
                PREFIX: true
            },
            INFO: {
                'title': 'Themes',
                'qText': 'What are the key themes people talk about my brand?',
                'donutColor': {'colorSet1': ['#82c2b2', '#314c50', '#82c2b2'], 'colorSet2': ['#7f6dc8', '#3b3e64', '#9983f5']},
                'description' :'Themes are categorizations of popular topics and give context of what other topics are associated to the query.'
              }

        });
})();
