 /* --------------- Service for Question 12 ratio chart  ---------------------*|
 |---  Q12: What other topics do they care about? : ratio per category --------|
 |----------------------------------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate12postService', ['commonService', 'jwtQ12PostConstant', function(commonService, jwtQ12PostConstant) {

        return angular.extend({
            cnst: jwtQ12PostConstant
        }, commonService);

    }]);