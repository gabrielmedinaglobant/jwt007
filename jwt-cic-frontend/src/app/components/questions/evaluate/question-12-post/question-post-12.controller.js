/* ------------- Controller for Question 12 ratio chart  --------------------*|
|---  Q12: What other topics do they care about? : ratio per category --------|
|----------------------------------------------------------------------------*/
questionEvaluate12postController.$inject = ['$scope', '$rootScope', 'jwtAppConstant', '$element', '$log', 'jwtQuestionEvaluate12postService'];

function questionEvaluate12postController($scope, $rootScope, jwtAppConstant, $element, $log, jwtQuestionEvaluate12postService) {
  var $ctrl = this;
  var q12PosttimeOut,q12PosttimeOut2;

  $ctrl.viewModel = {expanded:false};
  $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};

  var CONST_OBJ = jwtQuestionEvaluate12postService.getConstant('Q12Post');
  $scope.question = CONST_OBJ.STATIC_INFO;

  $ctrl.$onInit = function () {
    $ctrl.chartDrawn = false;
    $ctrl.data = {};
      callApi();
  }
    $ctrl.$onDestroy = function(){
      clearTimeout(q12PosttimeOut);
      clearTimeout(q12PosttimeOut2);
    }

  /* ** Getting data from the service ** */
function callApi(){
  jwtQuestionEvaluate12postService.getData(jwtQuestionEvaluate12postService.getConstant('Q12Post'))
    .then(function (response) {
      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      }
	     $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      var data = response.data.value;
      //$scope.showQuestion = false;
      var dataArray = [];

      // -- Parsing data and creating an array
      $.map(data, function (value, index) {
        var temp = {
          "topic": "FOOD & DRINK",
          "subtopic": "Mexican Cuisine",
          "value": 10,
          "unit": "%"
        };
        temp = data[index];
        temp.value = Math.round(parseFloat(data[index].value * 100));
        dataArray.push(temp);
      });

      $ctrl.data = dataArray;

      //var drawChart = function () {
        isWorkspaceElement($ctrl.data);
      //}
      //clearTimeout(q12PosttimeOut);
      //q12PosttimeOut = setTimeout(drawChart, 100);

    }, function (error) {
      $ctrl.showLoader();
    });

  }

  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace == true) {
      //$scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.70);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      var width = $('.chart-cont-q12-post').width();
      //$scope.showQuestion = false;
      $ctrl.drawChart(width, 360, data);
    }
  }


  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && Object.keys($ctrl.data).length != 0) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
    $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
        clearTimeout(q12PosttimeOut2);
      q12PosttimeOut2 = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');

//    if (targetElement.hasClass('big-tile')) {
//      $ctrl.viewModel.expanded = false;
//      $ctrl.viewModel.expanded = $ctrl.viewModel.expanded;
//    } else {
//      $ctrl.viewModel.expanded = true;
//      $ctrl.viewModel.expanded = $ctrl.viewModel.expanded;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.width());
    var nHeight = Math.round(targetElement.height());

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);
  };

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q12-post');
    elem.empty();
    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && Object.keys($ctrl.data).length != 0) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(parseInt(newContAttrObj.nwidth), parseInt(newContAttrObj.nheight), $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q12-post');
    elem.empty();
//    if ($ctrl.isWorkspace)
//      $scope.showQuestion = !$scope.showQuestion;
    this.drawChart(w, h, qservice);
  },

    /* --------- Rendering the chart  -------------------*|
    |- Params : Width, Height and data from the service --|
    |-----------------------------------------------------*/
    this.drawChart = function (w, h, data) {

      // Set the dimensions of the canvas / graph
      var margin = { top: 15, right: 15, bottom: 15, left: 15 },
        width = w - margin.left - margin.right,
        height = h - margin.top - margin.bottom,
        color = $scope.question.colors;

      if (!$ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width - margin.left - margin.right;
      } else if ($ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width - 2 * (margin.left + margin.right);
        height = height - 2 * (margin.top + margin.bottom);
      }


      // Calling chart for showing post interests
      var viz = d3.speechDialogsViz($ctrl.viewModel.expanded, $scope.question)
        .data(data)
        .r(function (d) { return d.value; })
        .height(height)
        .width(width)
        .scale(function (d) { return d.value / 90; });
      d3.select(".chart-cont-q12-post")
        .call(viz);
      $ctrl.chartDrawn = true;
    };
}
