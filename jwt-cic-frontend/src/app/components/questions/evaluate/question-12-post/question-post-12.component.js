(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate12Post', {
      templateUrl: 'app/components/questions/evaluate/question-12-post/evaluate.question-post-12.html',
      controller: questionEvaluate12postController,
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'
      }
    });

})();
