(function() {
    'use strict';

    angular
        .module('app.components')
                .constant('jwtQ12PostConstant', {
            'Q12Post': {
                API: '/questions/other-topics-care-about/top-post-interests',
                PREFIX: true,
                STATIC_INFO: {
                    'title': 'Post Interest',
                    'qText': 'What else is mentioned with the posts?',
                    'colors': {
                        'textColor': ['#16252f', '#1e433a'],
                        'cloudColor': '#82c2b2'
                    },
                    'tooltipColors':{
                        'textColor': ['#49bfab', '#749ebf']
                    },
                    'description1': '1) Posts mentioning the brand also contain information about other topics.',
                    'description2': '2) The context of the post when the brand is mentioned.'
                }
            }
        });
})();
