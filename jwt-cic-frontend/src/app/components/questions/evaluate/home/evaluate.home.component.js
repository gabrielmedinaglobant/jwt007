(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluateHome', {
      templateUrl: 'app/components/questions/evaluate/home/evaluate.home.html',
      controller: questionEvaluateHomeController,
      bindings: {
        data: '<',
        isWorkspace:'<'
      }
    });

})();
