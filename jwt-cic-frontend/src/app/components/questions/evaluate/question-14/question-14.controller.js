/* --------- Controller for Question 14  --------------*|
 |---  Q14: What emotions do people have when talking about the category?----|
 |----------------------------------------------------*/

questionEvaluate14Controller.$inject = ['$scope', '$rootScope', '$timeout', 'jwtAppConstant', '$log', '$element', 'D3Service', 'jwtQuestionEvaluate14Service'];

function questionEvaluate14Controller($scope, $rootScope, $timeout, jwtAppConstant, $log, $element, d3, jwtQuestionEvaluate14Service) {
  var $ctrl = this;
  $ctrl.evaluateQuestion8Data = {};
  $ctrl.viewModel = {expanded:false};
  $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  //$ctrl.viewModel.expanded = false;
  $scope.question = jwtQuestionEvaluate14Service.getConstant('INFO');
var q14timeOut;

  $ctrl.$onInit = function () {
    callApi();
  }

    $ctrl.$onDestroy = function(){
      clearTimeout(q14timeOut);
    }

  /* ** Getting data from the service ** */
  function callApi(){
      jwtQuestionEvaluate14Service.getData(jwtQuestionEvaluate14Service.getConstant('QAPI'))
        .then(function (response) {
      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      }
     $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });

          //data = response.data.values;
          $ctrl.data = response.data.value;
          //$scope.showQuestion = false;
          isWorkspaceElement($ctrl.data);
        }, function (error) {
          $ctrl.showLoader();
        });
  }
  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace) {
      //$scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.75);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      $ctrl.drawChart(180, 360, data);
      //$scope.showQuestion = false;
    }
  }


  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q14');
      elem.empty();
      elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data != undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
        $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
        clearTimeout(q14timeOut);
      q14timeOut = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
//    if (targetElement.hasClass('big-tile')) {
//      $ctrl.viewModel.expanded = false;
//    } else {
//      $ctrl.viewModel.expanded = true;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.width());
    var nHeight = Math.round(targetElement.height());

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q14');
    elem.empty();
//    if ($ctrl.isWorkspace)
//      $scope.showQuestion = !$scope.showQuestion;
    this.drawChart(w, h, qservice);
  };



  /* --------- Rendering the chart  -------------------*|
   |- Params : Width, Height and data from the service --|
   |-----------------------------------------------------*/
  this.drawChart = function (w, h, data) {

    var width, height;

    if (!$ctrl.viewModel.expanded) {
      width = 90;
      height = 90;
    } else {
      width = 160;
      height = 120;
      d3.select(".chart-cont-q14").append("div").attr("class", "expand-margin");
    }

    var radius = Math.min(width, height) / 2;
    var conutColor1 = ['#82c2b2', "#39454e"];


    $.map(data, function (data, index) {
      var emotionType = jwtUtil.toTitleCase(data.emotionType);
      var value = data.value;
      var unit = data.unit;

      var dataset = {
        percentData: [value, 100 - value],
      };

      var pie = d3.pie()
        .sort(null);
      var arc;


      if ($ctrl.viewModel.expanded) {
        arc = d3.arc()
          .innerRadius(radius - 10)
          .outerRadius(radius - 30);

        var parentDiv = d3.select(".expand-margin").append("div").attr("class", "emotions-graph-expand");

        var svg = parentDiv.append("div").append("svg")
          .attr("width", "100%")
          .attr("height", height);

        var path = svg.append("g")
          .attr("transform", function () {
            if (jwtUtil.getViewport().width > 1600 && jwtUtil.getViewport().width < 1921) {
              return "translate(" + width / 1.4 + "," + ((height / 2)) + ")";
            }
            else if (jwtUtil.getViewport().width > 800 && jwtUtil.getViewport().width < 1401) {
              return "translate(" + width / 2 + "," + ((height / 2)) + ")";
            }
            else if (jwtUtil.getViewport().width > 1400 && jwtUtil.getViewport().width < 1601) {
              return "translate(" + width / 1.75 + "," + ((height / 2)) + ")";
            }
            else if (jwtUtil.getViewport().width < 801) {
              return "translate(" + width / 2.6 + "," + ((height / 2)) + ")";
            }
          })
          .selectAll("path")
          .data(pie(dataset.percentData))
          .enter().append("path")
          .style("fill", function (d, i) { return conutColor1[i]; })
          .attr("d", arc)
          .style('stroke', "none");


        svg.append("foreignObject")
          .attr("class", "emotions-smily-expand icon-jwt-emo-" + data.emotionType)
          .attr("transform", function () {
            if (jwtUtil.getViewport().width > 1600 && jwtUtil.getViewport().width < 1921) {
              return "translate(" + (width / 1.85) + "," + (height / 2.8) + ")";
            }
            else if (jwtUtil.getViewport().width > 800 && jwtUtil.getViewport().width < 1401) {
              return "translate(" + (width / 3) + "," + (height / 2.8) + ")";
            }
            else if (jwtUtil.getViewport().width > 1400 && jwtUtil.getViewport().width < 1601) {
              return "translate(" + (width / 2.5) + "," + (height / 2.8) + ")";
            }
            else if (jwtUtil.getViewport().width < 801) {
              return "translate(" + (width / 4.7) + "," + (height / 2.8) + ")";
            }

          })

        svg.select("foreignObject").attr("x", 0).attr("y", 0).attr("width", (width / 3)).attr("height", (height / 2.8));

        var nameTextTitle = parentDiv.append("div").attr("class", "emotion-graph-expand-info");
        nameTextTitle.append("span").attr("class", "emotions-graph-expand-title")
          .text(emotionType);
        nameTextTitle.append("span").attr("class", "emotions-graph-expand-value")
          .text(jwtUtil.formatWithCommas(value, "fc", "%"));

      }
      else {
        arc = d3.arc()
          .innerRadius(radius - 4)
          .outerRadius(radius - 20);

        var parentDiv = d3.select(".chart-cont-q14").append("div").attr("class", "emotions-graph-colaps");
        var svg = parentDiv.append("div").append("svg")
          .attr("width", width)
          .attr("height", height);

        var path = svg.append("g")
          .attr("transform", "translate(" + width / 2 + "," + ((height / 2)) + ")")
          .selectAll("path")
          .data(pie(dataset.percentData))
          .enter().append("path")
          .style("fill", function (d, i) { return conutColor1[i]; })
          .attr("d", arc)
          .style('stroke', "none");

        svg.append("foreignObject")
          .attr("class", "emotions-smily-colaps icon-jwt-emo-" + data.emotionType)
          .attr("transform", "translate(" + (width / 3) + "," + (height / 2.7) + ")");

        svg.select("foreignObject").attr("x", 0).attr("y", 0).attr("width", (width / 3)).attr("height", (height / 2.7));

        var nameTextTitle = parentDiv.append("div").attr("class", "emotion-graph-colaps-info");
        nameTextTitle.append("span").attr("class", "emotions-graph-colaps-title")
          .text(emotionType);
        nameTextTitle.append("span").attr("class", "emotions-graph-colaps-value")
          .text(jwtUtil.formatWithCommas(value, "fc", "%"));

      }
    });

  };

}
