(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate14Constant', {
            QAPI: {
                API: '/questions/emotions-about-category',
                PREFIX: true
            },
            INFO: {
                'title': 'Brand Emotions',
                'qText': 'What emotions do people have when talking about my brand?',
                'description' :'A deep dive to sentiment exploring the 8 basic types of human emotions.'
              }

        });
})();
