/* --------- Service for Question 14 --------------*|
|---  Q14: What emotions do people have when talking about the category?--|
|------------------------------------------------*/


angular
    .module('app.components')
    .factory('jwtQuestionEvaluate14Service', ['commonService', 'jwtQuestionEvaluate14Constant', function(commonService, jwtQuestionEvaluate14Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate14Constant
        }, commonService);

    }]);

//angular
//   .module('app.common')
//   .service('jwtQuestionEvaluate14Service',['$http','$q','httpService','jwtAppConstant',function($http,$q,httpService,jwtAppConstant){
//
//   // jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//   /* --------- get chart data  --------------*/
//       this.getData = function(){
//           return httpService.getData(jwtAppConstant.evaluate.q14.questionAPI)
//       .then(function(response){
//         if(typeof response.data === 'object' ) {
//           return response.data;
//         } else
//         {
//           $q.reject(response.data);
//         }
//       },function errorCallback(response) {
//           $q.reject(response.data);
//       });
//       }
//}]);
