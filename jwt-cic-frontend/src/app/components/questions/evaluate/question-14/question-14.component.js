(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate14', {
      controller: questionEvaluate14Controller,
      templateUrl: 'app/components/questions/evaluate/question-14/evaluate.question-14.html',
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'
      }
    });

})();
