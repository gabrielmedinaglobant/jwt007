/* ------------- Controller for Question 12 ratio chart  --------------------*|
|---  Q12: What other topics do they care about? : ratio per category --------|
|----------------------------------------------------------------------------*/
questionEvaluate12ratioController.$inject = ['$scope', '$rootScope', '$timeout', 'jwtAppConstant', '$element', '$log', 'jwtQuestionEvaluate12ratioService'];

function questionEvaluate12ratioController($scope, $rootScope, $timeout, jwtAppConstant, $element, $log, jwtQuestionEvaluate12ratioService) {
  var $ctrl = this;

  $ctrl.viewModel = { expanded: false };
  $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: null };
  var q12ratiotimeOut, q12ratiotimeOut2;
  $scope.question = jwtQuestionEvaluate12ratioService.getConstant('INFO');

  $ctrl.$onInit = function () {
    $ctrl.chartDrawn = false;
    $ctrl.data = {};
    callApi();
  }
  $ctrl.$onDestroy = function () {
    clearTimeout(q12ratiotimeOut);
    clearTimeout(q12ratiotimeOut2);
  }

  /* ** Getting data from the service ** */
  function callApi() {
    jwtQuestionEvaluate12ratioService.getData(jwtQuestionEvaluate12ratioService.getConstant('QAPI'))
      .then(function (response) {
        if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
          $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
          $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        }
        $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });

        var data = response.data.value;
        //$scope.showQuestion = false;
        var dataArray = data;
        $ctrl.data = dataArray;

        //var drawChart = function () {
        isWorkspaceElement($ctrl.data);
        //}
        //clearTimeout(q12ratiotimeOut);
        //q12ratiotimeOut = setTimeout(drawChart, 200);

      }, function (error) {
        $ctrl.showLoader();
      });
  }


  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace == true) {
      //$scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.70);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      var width = $('.chart-cont-q12-ratio').width();
      $ctrl.drawChart(width, 360, data);
    }
  }

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q12-ratio');
    elem.empty();
    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && Object.keys($ctrl.data).length != 0) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    } else {
      $ctrl.showLoader();
    }
  }

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && Object.keys($ctrl.data).length != 0) {
      $ctrl.showLoader();
      $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
      clearTimeout(q12ratiotimeOut2);
      q12ratiotimeOut2 = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');
    //$scope.showQuestion = !$scope.showQuestion;

    targetElement.siblings().removeClass('big-tile');
    //    if (targetElement.hasClass('big-tile')) {
    //      $ctrl.viewModel.expanded = false;
    //    } else {
    //      $ctrl.viewModel.expanded = true;
    //    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.innerWidth());
    var nHeight = Math.round(targetElement.innerHeight());
    if (!$ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 360 : nHeight;
    }

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);
  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q12-ratio');
    elem.empty();
    this.drawChart(w, h, qservice);
  },

    /* --------- Rendering the chart  -------------------*|
    |- Params : Width, Height and data from the service --|
    |-----------------------------------------------------*/
    this.drawChart = function (w, h, data) {

      // Set the dimensions of the canvas / graph
      var margin = { top: 15, right: 15, bottom: 15, left: 15 },
        width = w - margin.left - margin.right,
        height = h - margin.top - margin.bottom,
        color = $scope.question.colors;

      if (!$ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width - margin.left - margin.right;
      } else if ($ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width - 2 * (margin.left + margin.right);
        height = height - 2 * (margin.top + margin.bottom);
      }

      // Init ratio chart
      var ratioMap = d3.treemap().size([width, height]);

      // Create chart container block
      var chartContDiv = d3.select(".chart-cont-q12-ratio")
        .append("div")
        .attr("class", "ratio-chart-cont")
        .style("width", width + "px")
        .style("height", height + "px")
        .style("top", function () {
          var tMargin = 0;
          if (!$ctrl.chartDrawn) {
            if (!$ctrl.viewModel.expanded) {
              tMargin = margin.top;
            }
          } else {
            if (!$ctrl.viewModel.expanded) {
              tMargin = margin.top;
            }
          }
          return tMargin + "px";
        });

      // Create an object with all the hirarchical data and process over required value
      var rootData = d3.hierarchy(data, function (d) {
        return d;
      })
        .sum(function (d) {
          var ratioV = parseFloat(d.ratioValue);
          return parseFloat(ratioV.toFixed(2));
        });

      // Initializing tooltip
      var tooltip = d3.select("body")
        .append("div")
        .attr("class", "ratio-tooltip");

      var creteTooltip = false;

      // Assigning a main node with rootData
      var rootNode = ratioMap(rootData);

      // Create child nodes for each object block
      var node = chartContDiv.datum(rootData).selectAll(".node")
        .data(rootNode.leaves())
        .enter().append("div")
        .attr("class", "node")
        .style("left", (d) => d.x0 + "px")
        .style("top", (d) => d.y0 + "px")
        .style("width", (d) => Math.max(0, d.x1 - d.x0 - 1) + "px")
        .style("height", (d) => Math.max(0, d.y1 - d.y0 - 1) + "px")
        .style("background", function (d, i) {
          return color.barColor[i];
        })
        .style("color", function (d, i) {
          var iCount = Math.floor(i / 3);
          return color.fontColor[iCount];
        })

        // Adding the html element with text
        .append("span")
        .attr("class", "text-ratio")
        .html(function (d, i) {
          creteTooltip = false;
          var sClass = "text-topic-top";
          if (!$ctrl.viewModel.expanded) {
            sClass = "text-topic-top hide";
          }

          var sText = d.data.topic;
          sText += '<span class="' + sClass + '">' + d.data.subtopic + '</span>';
          return sText;
        })
        .on("mouseover", function (d, i) {
          var iCount = Math.floor(i / 3);
          var fontColor = color.fontColor[iCount];
          var unitText = parseFloat(d.data.ratioValue);
          unitText = parseFloat(unitText.toFixed(2));


          var tooltipFunc = function () {
            var htmlTxt = '<span class="ratio-tooltip-topic">' + d.data.topic + '</span>'
              + '<span class="ratio-tooltip-subtopic">' + d.data.subtopic + '<br/>'
              + unitText + d.data.ratioUnit + '</span>';

            tooltip.html(htmlTxt);
            tooltip.style("visibility", "visible");
          };

          var $element = $('<span/>');
            $element.attr('class', 'text-ratio');
            $element.html('<span>' + d.data.topic + '<br/>'+ d.data.subtopic + '</span>');

          var $c = $element
            .clone()
            .css({ display: 'inline', width: 'auto', visibility: 'hidden' })
            .appendTo('body');
            tooltipFunc();
          // if (!$ctrl.viewModel.expanded){
          //   tooltipFunc();
          // }else if($(this).parents('.node').width() < $c.width()){
          //   tooltipFunc();
          // }
          $c.remove();
        })
        .on("mousemove", function () {
          if (tooltip.html.length > 0 && tooltip.html != undefined) {
            var viewportObj = jwtUtil.getViewport();
            var xCoord;
            if (viewportObj.width - $('.ratio-tooltip').width() > d3.event.pageX) {
              xCoord = d3.event.pageX;
            } else {
              xCoord = d3.event.pageX - $('.ratio-tooltip').width() - 40;
            }
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (xCoord + 10) + "px");
          }
        })
        .on("mouseout", function () {
          if (tooltip.html.length > 0 && tooltip.html != undefined) {
            return tooltip.style("visibility", "hidden");
          }
        })
        .append("span")
        .attr("class", function () {
          var sClass = "text-topic";
          if (!$ctrl.viewModel.expanded) {
            sClass = "text-topic hide";
          }
          return sClass;
        })
        .html(function (d, i) {
          var sText = parseFloat(d.data.ratioValue);
          sText = parseFloat(sText.toFixed(2));
          sText += '<span class="text-ratio-unit">' + d.data.ratioUnit + '</span>';
          return sText;
        })
        .style("width", (d) => Math.max(0, d.x1 - d.x0 - margin.right) + "px");

      $ctrl.chartDrawn = true;
    };
}
