(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate12Ratio', {
      templateUrl: 'app/components/questions/evaluate/question-12-ratio/evaluate.question-ratio-12.html',
      controller: questionEvaluate12ratioController,
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'
      }
    });

})();
