 /* --------------- Service for Question 12 ratio chart  ---------------------*|
 |---  Q12: What other topics do they care about? : ratio per category --------|
 |----------------------------------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate12ratioService', ['commonService', 'jwtQuestionEvaluate12ratioConstant', function(commonService, jwtQuestionEvaluate12ratioConstant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate12ratioConstant
        }, commonService);

    }]);


// angular
//    .module('app.common')
//    .service('jwtQuestionEvaluate12ratioService',['$http','$q', 'jwtAppConstant', 'httpService',function($http,$q, jwtAppConstant, httpService){
//
//        // https://demo7088558.mockable.io/linedata
//		// jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//
//		/* --------- get chart data  --------------*/
//        this.geRatioData = function(){
//            return httpService.getData(jwtAppConstant.evaluate.q12.ratio.apiUrl)
//				.then(function(response){
//					if(typeof response.data === 'object' ) {
//						return response.data;
//					} else
//					{
//                        console.log("error");
//						$q.reject(response.data);
//					}
//				},function errorCallback(response) {
//                    console.log("error");
//					$q.reject(response.data);
//				});
//        }
//}]);

