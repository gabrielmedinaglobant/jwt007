(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate12ratioConstant', {
            QAPI: {
                API: '/questions/other-topics-care-about/ratio-per-category',
                PREFIX: true
            },
            INFO: {
                    'title': 'User Interests',
                    'qText': 'What other topics do they care about?',
                    'apiUrl': '/questions/other-topics-care-about/ratio-per-category',
                    'colors': {
                        'barColor': ['#a0e3d2', '#82c2b2', '#64988b', '#a392ea', '#7f6dc8', '#6d5ea7', '#e7985b', '#d7803c', '#b76f37', '#f6db5d', '#eccb30', '#c7ac2d', '#78ce7a', '#59a95b', '#49894b'],
                        'fontColor': ['#16252f', '#16252f', '#16252f', '#16252f', '#16252f']
                    },
                    'description': 'Other topics that are mentioned with the brand’s posts.'
                }

        });
})();
