(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate11Constant', {
            QAPI: {
                API: '/questions/talking-about-me',
                PREFIX: true
            },
            INFO: {
                'title': 'Demographics',
                'qText': 'Who is talking about my brand?',
                'colors': { 'female': '#f25cad', 'male': '#51a5e8', 'mixed': '#b78637', 'age-distribution': '#b78637' },
                'phrase': {
                    'phrase1': 'This query received higher than average engagement (',
                    'phrase2': ') among people that make between ',
                    'phrase3': ' a year. Participants for this query have a median disposable income of ',
                    'phrase4': ' and on average Own a home value between ',
                    'phrase5': '. ',
                    'phrase6': ' of these participants have some college education.'
                },
                'description' : 'Demographics of users mentioning the brand online.'
              }

        });
})();
