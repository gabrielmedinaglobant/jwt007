/* --------- Service for Question 11 --------------*|
|---  Q11: Who is talking about me?--|
|------------------------------------------------*/


angular
    .module('app.components')
    .factory('jwtQuestionEvaluate11Service', ['commonService', 'jwtQuestionEvaluate11Constant', function(commonService, jwtQuestionEvaluate11Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate11Constant
        }, commonService);

    }]);

//angular
//   .module('app.common')
//   .service('jwtQuestionEvaluate11Service',['$http','$q','httpService','jwtAppConstant',function($http,$q,httpService,jwtAppConstant){
//
//   // jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//   /* --------- get chart data  --------------*/
//       this.getData = function(){        
//           return httpService.getData(jwtAppConstant.evaluate.q11.questionAPI)
//       .then(function(response){
//         if(typeof response.data === 'object' ) {
//           return response.data;
//         } else
//         {
//           $q.reject(response.data);
//         }
//       },function errorCallback(response) {
//           $q.reject(response.data);
//       });
//       }
//}]);
