/* --------- Controller for Question 11  --------------*|
 |---  Q11: Who is talking about me? (US only) --------|
 |----------------------------------------------------*/

questionEvaluate11Controller.$inject = ['$scope', 'jwtAppConstant', '$element', '$rootScope', '$log', 'jwtQuestionEvaluate11Service'];

function questionEvaluate11Controller($scope, jwtAppConstant, $element, $rootScope, $log, jwtQuestionEvaluate11Service) {
  var $ctrl = this;
  //$ctrl.viewModel.expanded = false;
  $ctrl.viewModel = { expanded: false };
  $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: null };
  $scope.question = jwtQuestionEvaluate11Service.getConstant('INFO');
  $scope.femaleChartHeight = $('.chart-cont-q11 .femaleGenderTrendOver').height();
  $scope.maleChartHeight = $('.chart-cont-q11 .maleGenderTrendOver').height();
  var q11timeOut;

  $ctrl.$onInit = function () {
    callApi();
  }
  $ctrl.$onDestroy = function () {
    clearTimeout(q11timeOut);
  }
  /* ** Getting data from the service ** */
  function callApi() {
    jwtQuestionEvaluate11Service.getData(jwtQuestionEvaluate11Service.getConstant('QAPI'))
      .then(function (response) {
        if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
          $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
          $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        }
        $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        if (response.data != undefined) {
          $('.chart-cont-q11 .genderTrend').show();
          //$scope.showQuestion = false;
          $ctrl.data = response.data.value;
          isWorkspaceElement($ctrl.data);
        }
      }, function (error) {
        $ctrl.showLoader();
      });
  }
  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace == true) {
      $('.chart-cont-q11 .demographic').show();
      $('.chart-cont-q11 .langAgeChart').show();
      $('.chart-cont-q11 .infoTrend').show();
      $('.chart-cont-q11 .genderTrendChart').show();
      $('.chart-cont-q11 .colapsTrendInfo').hide();
      //$scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.75);
      $ctrl.drawChart(newWindWid, 365, data);
    } else {
      var width = $('.chart-cont-q11').width();
      $ctrl.drawChart(width, 360, data);
    }
  }

  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q11');
    $('.chart-cont-q11 .graphTrend').hide();
    $('.chart-cont-q11 .demographic').hide();
    $('.chart-cont-q11 .langAgeChart').hide();
    $('.chart-cont-q11 .infoTrend').hide();
    $('.chart-cont-q11 .genderTrendChart').hide();
    $('.chart-cont-q11 .colapsTrendInfo').hide();
    $('.chart-cont-q11 .nodisplay').removeClass('nodisplay');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data != undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    } else {
      $ctrl.showLoader();
    }
  }

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
      $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
      clearTimeout(q11timeOut);
      q11timeOut = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
    //    if (targetElement.hasClass('big-tile')) {
    //      $ctrl.viewModel.expanded = false;
    //    } else {
    //      $ctrl.viewModel.expanded = true;
    //    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.innerWidth());
    var nHeight = Math.round(targetElement.innerHeight());
    if (!$ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 360 : nHeight;
      $('.chart-cont-q11 .demographic').hide();
      $('.chart-cont-q11 .langAgeChart').hide();
      $('.chart-cont-q11 .infoTrend').hide();
      $('.chart-cont-q11 .genderTrendChart').hide();
      $('.chart-cont-q11 .colapsTrendInfo').show();
    } else {
      $('.chart-cont-q11 .demographic').show();
      $('.chart-cont-q11 .langAgeChart').show();
      $('.chart-cont-q11 .infoTrend').show();
      $('.chart-cont-q11 .genderTrendChart').show();
      $('.chart-cont-q11 .colapsTrendInfo').hide();
    }
    $('.chart-cont-q11 .graphTrend').show();

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var ageDistChart = $('.chart-cont-q11 .ageDistChart');
    var langChart = $('.chart-cont-q11 .langChart');
    var demographic = $('.chart-cont-q11 .demographic');
    var genderTrendChart = $('.chart-cont-q11 .genderTrendChart');
    var titleTrend = $('.chart-cont-q11 .titleTrend');
    var infoTrend = $('.chart-cont-q11 .infoTrend');
    var colapsTrendInfo = $('.chart-cont-q11 .colapsTrendInfo');
    var loaderDiv = $('.chart-cont-q11 .loader');
    ageDistChart.empty();
    langChart.empty();
    demographic.empty();
    genderTrendChart.empty();
    titleTrend.empty();
    infoTrend.empty();
    colapsTrendInfo.empty();
    loaderDiv.addClass('nodisplay');


    if (!$ctrl.viewModel.expanded) {
      $('.chart-cont-q11 .demographic').hide();
      $('.chart-cont-q11 .langAgeChart').hide();
      $('.chart-cont-q11 .infoTrend').hide();
      $('.chart-cont-q11 .genderTrendChart').hide();
      $('.chart-cont-q11 .colapsTrendInfo').show();
    } else {
      $('.chart-cont-q11 .demographic').show();
      $('.chart-cont-q11 .langAgeChart').show();
      $('.chart-cont-q11 .infoTrend').show();
      $('.chart-cont-q11 .genderTrendChart').show();
      $('.chart-cont-q11 .colapsTrendInfo').hide();
    }
    $('.chart-cont-q11 .graphTrend').show();

    //    if ($ctrl.isWorkspace)
    //      $scope.showQuestion = !$scope.showQuestion;

    this.drawChart(w, h, qservice);
  },

    /* --------- Rendering the chart  -------------------*|
    |- Params : Width, Height and data from the service --|
    |-----------------------------------------------------*/
    this.drawChart = function (w, h, data) {

      $ctrl.drawGenderChart(w, h, data);
      $ctrl.languageChart(w, h, data);
      $ctrl.drawAgeDistChart(w, h, data);
      $ctrl.demographicChart(w, h, data);
    };


  /* ---------function for Rendering Gender Distribution chart  -------------------*/
  $ctrl.drawGenderChart = function (w, h, data) {

    if ($ctrl.viewModel.expanded) {
      $('.chart-cont-q11 .femaleTrend').css("width", 20 + "%");
      $('.chart-cont-q11 .maleTrend').css("width", 20 + "%");
      //--------Rendering gender title--- //
      var gender = d3.select(".chart-cont-q11 .genderTrend");
      d3.select(".chart-cont-q11 .titleTrend").append("span").text("GENDER TREND").attr("class", "gender-graph-titleText");

      //-------Rendering female and male gender images---- //

      // data for rener---------//
      var femaleFillChart = 100 - (data.immigrationGenderTrend.femalePersonFillChart.value * 100);
      var maleFillChart = 100 - (data.immigrationGenderTrend.malePersonFillChart.value * 100);

      var femaleGenderTrendOverHeight = $scope.femaleChartHeight;
      var maleGenderTrendOverHeight = $scope.maleChartHeight;

      $('.chart-cont-q11 .femaleGenderTrendOver').css("height", femaleFillChart.toFixed(0) + "%");
      $('.chart-cont-q11 .maleGenderTrendOver').css("height", maleFillChart.toFixed(0) + "%");


      //---------- Rendering gender chart information ----------------//

      var infoTrend = d3.select(".chart-cont-q11 .infoTrend");
      var infoDiv;
      $.map(data.immigrationGenderTrend.femaleIndicators, function (data, index) {
        switch (data.key) {
          case "percentage":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-percent-Female");
            infoDiv.append("span").text(Math.round(data.value) + data.unit)
            break;
          case "universe-count":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-UniverseCount-Female");
            infoDiv.append("span").text(jwtUtil.formatWithCommas(data.value, "f")).attr("class", "valuesTextFemale")
            infoDiv.append("span").text(data.title).attr("class", "titleText")
            break;
          case "days-leading":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-DaysLeading-Female");
            infoDiv.append("span").text(data.value + data.unit).attr("class", "valuesTextFemale")
            infoDiv.append("span").text(data.title).attr("class", "titleText")
            break;
        }
      });
      $.map(data.immigrationGenderTrend.indicators, function (data, index) {
        infoDiv = infoTrend.append("div").attr("class", "infoTrend-GenderUniverse");
        infoDiv.append("span").text(Math.round(data.value) + data.unit).attr("class", "valuesText")
        infoDiv.append("span").text(data.title).attr("class", "titleText")
      });

      $.map(data.immigrationGenderTrend.maleIndicators, function (data, index) {
        switch (data.key) {
          case "percentage":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-percent-Male");
            infoDiv.append("span").text(Math.round(data.value) + data.unit)
            break;
          case "universe-count":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-UniverseCount-Male");
            infoDiv.append("span").text(jwtUtil.formatWithCommas(data.value, "f")).attr("class", "valuesTextmale")
            infoDiv.append("span").text(data.title).attr("class", "titleText")
            break;
          case "days-leading":
            infoDiv = infoTrend.append("div").attr("class", "infoTrend-DaysLeading-Male");
            infoDiv.append("span").text(data.value + data.unit).attr("class", "valuesTextmale")
            infoDiv.append("span").text(data.title).attr("class", "titleText")
            break;
        }
      });

      //---------rendering gender trnd chart -------------//
      //************************************************//

      //------- data for Gender Distribution chart-------------- //
      var dataArrayGenderTrend = new Array();
      $.map(data.immigrationGenderTrend.genderSplineChart.lines, function (value, index) {
        $.map(value.values, function (data, i) {
          var nDate = new Date(data.key);
          var percent = data.value[1]
          dataArrayGenderTrend.push({ "gender": value.key, "sdate": nDate, "spercent": percent });
        });
      });

      // Get the data
      var data = dataArrayGenderTrend;

      var margin = { top: 20, right: 40, bottom: 20, left: 40 }, width, height;
      width = $('.chart-cont-q11 .genderTrendChart').width();
      height = $('.chart-cont-q11 .genderTrendChart ').height();

      var x = d3.scaleTime().rangeRound([80, width - 80]);
      var y = d3.scaleLinear().rangeRound([height - 100, 0]);
      var area = d3.area().curve(d3.curveBasis)
        .x(function (d) { return x(d.sdate); })
        .y0(y(0))
        .y1(function (d) { return y(d.spercent); });

      var xMin = d3.min(data, function (d) { return d.sdate; });
      var xMax = d3.max(data, function (d) { return d.sdate; });
      x.domain([xMin, xMax]);
      var mPercentY = Math.max.apply(Math, data.map(function (o) { return Math.abs(o.spercent); }));
      var maxY = mPercentY + (10 - (mPercentY % 10).toFixed(2));

      y.domain([0, maxY]);
      var color = $scope.question.colors;
      var svg = d3.select('.chart-cont-q11 .genderTrendChart')
        .append("svg")
        .attr("width", width)
        .attr("height", height)

      data.forEach(function (d) {
        d.sdate = d.sdate;
        d.spercent = +d.spercent;
      });
      // Nest the entries by symbol
      var dataNest = d3.nest()
        .key(function (d) { return d.gender; })
        .entries(data);

      dataNest.forEach(function (d) {
        svg.append("path")
          .attr("class", "gender-chart")
          .style("fill", function () {
            return d.color = color[d.key];
          })
          .datum(data)
          .attr("d", area(d.values));

      });

      var xAxis = d3.axisBottom(x).tickFormat(function (d) {
        var x = d3.timeFormat('%b/%M/%Y');
        var tdate = new Date(d);
        var temp = tdate.getMonth() + "/" + tdate.getDate() + "/" + tdate.getFullYear();
        return jwtUtil.addOrdinal(temp, true);
      }).tickValues([xMin, xMax]);

      // Add the X Axis
      svg.append("g")
        .attr("class", "path-axis-group")
        .attr("transform", "translate(0," + (height - 100) + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("class", "axis-info-labels")
        .attr("text-anchor", "middle")
        //.attr("dx", "-3em");
        .attr("dx", function (d, i) {
          if (i == 0) {
            return "-3.2em";
          }
          else {
            return "3.2em";
          }
        })

      $ctrl.drawGenderMouseHover(svg, width, height, color, x, y, dataNest);

    }
    else {

      $('.chart-cont-q11 .femaleTrend').css("width", 50 + "%");
      $('.chart-cont-q11 .maleTrend').css("width", 50 + "%");
      var femaleFillChart = 100 - (data.immigrationGenderTrend.femalePersonFillChart.value * 100);
      var maleFillChart = 100 - (data.immigrationGenderTrend.malePersonFillChart.value * 100);

      var femaleGenderTrendOverHeight = $scope.femaleChartHeight + 34;
      var maleGenderTrendOverHeight = $scope.maleChartHeight + 34;

      $('.chart-cont-q11 .femaleGenderTrendOver').css("height", femaleFillChart.toFixed(0) + "%");
      $('.chart-cont-q11 .maleGenderTrendOver').css("height", maleFillChart.toFixed(0) + "%");

      $.map(data.immigrationGenderTrend.femaleIndicators, function (data, index) {
        switch (data.key) {
          case "percentage":
            infoDiv = d3.select('.chart-cont-q11 .colapsTrendInfo').append("div").attr("class", "infoTrend-percent-Female");
            infoDiv.append("span").text(Math.round(data.value) + data.unit)
            break;
        }
      });
      $.map(data.immigrationGenderTrend.maleIndicators, function (data, index) {
        switch (data.key) {
          case "percentage":
            infoDiv = d3.select('.chart-cont-q11 .colapsTrendInfo').append("div").attr("class", "infoTrend-percent-Male");

            infoDiv.append("span").text(Math.round(data.value) + data.unit)
            break;
        }
      });

    } // expand condition end
  } //function end



  /* ---------function for Rendering Age Distribution chart  -------------------*/
  $ctrl.drawAgeDistChart = function (w, h, data) {
    if ($ctrl.viewModel.expanded) {
      var age = d3.select(".chart-cont-q11 .ageDistChart");

      age.append("div").attr("class", "age-graph-title")
        .append("span").text("AGE DISTRIBUTION").attr("class", "age-graph-titleText");

      //------- data for age Distribution chart-------------- //
      var dataArray = new Array();
      var dataArrayTooltip = new Array();
      var color = "#82c2b2";
      var keysDataArray = [];
      $.map(data.ageDistribution.splineChart.lines, function (value, index) {
        dataArrayTooltip.push(value);
        $.map(value.values, function (data, i) {
          var age = data.value[0];
          var percent = data.value[1]
          dataArray.push({ "key": data.key, "sdate": age, "spercent": percent });
          if (!(data.key == "")) {
            keysDataArray.push(data.key);
          }
        });
      });

      // Get the data
      var data = dataArray;
      var margin = { top: 20, right: 20, bottom: 20, left: 20 }, width, height;
      width = $('.chart-cont-q11 .ageDistChart').width();
      height = $('.chart-cont-q11 .ageDistChart ').height();

      var svg = d3.select('.chart-cont-q11 .ageDistChart').append("div")
        .append("svg")
        .attr("width", width)
        .attr("height", height + margin.top + margin.bottom)

      data.forEach(function (d) {
        d.sdate = d.sdate;
        d.spercent = +d.spercent;
      });

      var x = d3.scaleLinear().rangeRound([0, width]);
      var y = d3.scaleLinear().rangeRound([height, 0]);
      var xBand = d3.scaleBand()
        .domain(keysDataArray)
        .rangeRound([0, width]);

      var area = d3.area().curve(d3.curveBasis)
        .x(function (d) { return x(d.sdate); })
        .y1(function (d) { return y(d.spercent); });


      var totalDataLength = keysDataArray.length;
      x.domain(d3.extent(data, function (d) { return d.sdate; }));

      var mPercentY = Math.max.apply(Math, data.map(function (o) { return o.spercent; }));
      y.domain([0, mPercentY]);

      area.y0(y(0));

      var xAxis = d3.axisBottom(xBand);


      svg.append("path")
        .attr("class", "age-chart")
        .attr("fill", color)
        .datum(data)
        .attr("d", area);

      // Add the X Axis
      svg.append("g")
        .attr("class", "path-axis-group-age")
        .attr("transform", "translate(-6," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("class", "axis-info-labels")
        .attr("text-anchor", "middle")

      $ctrl.drawAgeMouseHover(svg, width, height, color, x, y, dataArrayTooltip);

    }
  }
  /* ---------function for Rendering language chart  -------------------*/
  $ctrl.languageChart = function (w, h, data) {

    if ($ctrl.viewModel.expanded) {
      var language = d3.select(".chart-cont-q11 .langChart");

      language.append("div").attr("class", "lang-graph-title")
        .append("span").text("LANGUAGE DISTRIBUTION")

      var logo_icon;
      var dataArray = new Array();
      $.map(data.languageDistribution.languageIndicator, function (data, index) {
        var languageName = data.languageName;
        var value = data.value
        var unit = data.unit

        dataArray.push({ "languageName": languageName, "value": value, "unit": unit });


      });

      dataArray.sort(function (a, b) {
        return parseFloat(b.value) - parseFloat(a.value);
      });

      $.map(dataArray, function (data, index) {
        var className;
        if (index == 0) {
          className = "large";
        } else if (index == 1) {
          className = "medium";
        } else if (index == 2) {
          className = "small";
        } else {
          className = "others";
        }

        var category = language.append("div").attr("class", "languageCateg");
        if (index == 0) {
          category.style("width", "28%");
        } else if (index == 1) {
          category.style("width", "25%");
        } else if (index == 2) {
          category.style("width", "23%");
        } else if (index == 3) {
          category.style("width", "23%");
        }
        category.append("div").style("float", "left")

          .append("img").attr("src", "app/assets/img/Q11/flags/logo-" + data.languageName + ".svg")
          .attr("class", "logo " + className)
        var lanInfo = category.append("div").attr("class", "langInfo")
        lanInfo.append("span").text(data.value + data.unit).attr("class", "valuesText")
        lanInfo.append("span").text(data.languageName).attr("class", "titleText")
        // category.append("span").text("Mean "+meanUnit+meanValue).attr("class", "titleText");
      });

    }

  }
  /* ---------function for Rendering Demographic chart  -------------------*/
  $ctrl.demographicChart = function (w, h, data) {
    if ($ctrl.viewModel.expanded) {
      var phrase = $scope.question.phrase.phrase1 + "26%";
      phrase += $scope.question.phrase.phrase2 + "$25,000-$50,000";
      phrase += $scope.question.phrase.phrase3 + "$21,643";
      phrase += $scope.question.phrase.phrase4 + "$100,000 - $200,000";
      phrase += $scope.question.phrase.phrase5 + "50%";
      phrase += $scope.question.phrase.phrase6;
      var keyOrder = ["household-income", "disposable-income", "household-size", "household-value", "higher-education"];
      var demographic = d3.select(".chart-cont-q11 .demographic");
      //---- Title ---//
      demographic.append("div").attr("class", "demographic-graph-title")
        .append("span").text("U.S. DEMOGRAPHICS").attr("class", "demographic-graph-titleText");
      //--- phrase -------//
      // demographic.append("div").attr("class", "demographic-graph-phrase")
      //   .append("span").text(phrase).attr("class", "demographic-graph-phraseText");

      var dataArray = new Array();
      $.map(data.usDemographics.indicators, function (data, index) {
        var key = data.key;
        var trend = data.trend
        var value = data.value
        var unit = data.unit
        var title = data.title
        var meanValue = data.meanValue
        var meanUnit = data.meanUnit
        dataArray.push({
          "dkey": key, "dtrend": trend, "dvalue": value, "dunit": unit,
          "dtitle": title, "dmeanValue": meanValue, "dmeanUnit": meanUnit
        });

      });

      var result = [];
      keyOrder.forEach(function (key) {
        dataArray = dataArray.filter(function (data) {
          if (data.dkey == key) {
            result.push(data);
            return false;
          } else
            return true;
        })
      })

      $.map(result, function (data, index) {
        var logo_icon;
        switch (data.dkey) {
          case "household-income":
            logo_icon = "icon-jwt-doller-border";
            break;
          case "disposable-income":
            logo_icon = "icon-jwt-doller";
            break;
          case "household-size":
            logo_icon = "icon-jwt-family";
            break;
          case "household-value":
            logo_icon = "icon-jwt-home";
            break;
          case "higher-education":
            logo_icon = "icon-jwt-education";
            break;

        }
        var category = demographic.append("div").attr("class", "demographicCateg");
        category.append("span").attr("class", "logo").append("i").attr("class", logo_icon);
        category.append("span").text(jwtUtil.formatWithCommas(data.dvalue, "fct", data.dunit, data.dtrend)).attr("class", "valuesText")
        category.append("span").text(data.dtitle).attr("class", "titleText")
        category.append("span").text("Mean " + jwtUtil.formatWithCommas(data.dmeanValue, "fc", data.dmeanUnit)).attr("class", "titleText1");
        // if (data.dkey == "household-size") {
        //   category.append("span").text("Sample size 73").attr("class", "titleText")
        // }

      });
    }

  }


  /* To create the mouse over effects for gender cahrt */

  $ctrl.drawGenderMouseHover = function (svg, width, height, color, x, y, dataNest) {

    var sentimentMouseComp = svg.append("g")
      .attr("class", "gender-mouse-over-component")
      .attr("transform", "translate(0,0)");

    // Adding vertical Line
    sentimentMouseComp.append("path")
      .attr("class", "gender-mouse-over-line")
      .style("stroke-width", "1px")
      .style("stroke-dasharray", ("3, 3"))
      .style("opacity", "0");

    //text for new tooltip//

    sentimentMouseComp.append("g")
      .attr('class', 'gender-mouse-over-text-all');

    d3.select('.gender-mouse-over-text-all')
      .append('svg:rect')
      .attr('class', 'gender-mouse-over-text-bg gender-date-bg')
      .attr('x', '5')
      .attr('y', '-8')
      .attr('width', '90')
      .attr('height', '16')
      .style("opacity", "0");

    d3.select('.gender-mouse-over-text-all')
      .append("text")
      .attr("class", "gender-mouse-over-line-date")
      .attr("transform", "translate(10,3)")
      .attr("dx", "1em");

    for (var k = 0; k < dataNest.length; k++) {
      d3.select('.gender-mouse-over-text-all')
        .append('text')
        .attr('class', 'gender-mouse-over-text' + k)
        .attr("dx", "1em");
    }


    var sentiments = document.getElementsByClassName('gender-chart');

    // Adding "g" for each line
    var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-gender')
      .data(dataNest)
      .enter()
      .append("g")
      .attr("class", "component-per-gender");

    // Adding circles for each Line
    perSentimentMouseComp.append("circle")
      .attr("r", 5)
      .attr("class", "mouse-circles")
      .style("stroke", function (d, i) {
        return d.color = color[d.key];
      })
      .style("fill", "#172530")
      .style("stroke-width", "2px")
      .style("opacity", "0");


    // Adding canvas to catch mouse movements
    sentimentMouseComp.append('svg:rect')
      .attr('width', width - 80)
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      // ------ On mouse out hide this component -----
      .on('mouseout', function (d, i) {
        d3.select(".gender-mouse-over-line")
          .style("opacity", "0");
        d3.selectAll(".component-per-gender circle")
          .style("opacity", "0");
        d3.selectAll(".component-per-gender text")
          .style("opacity", "0");
        d3.selectAll(".gender-mouse-over-text-bg")
          .style("opacity", "0");
        d3.select(".gender-mouse-over-text-all")
          .style("opacity", "0");
        d3.selectAll(".gender-mouse-over-line-date")
          .style("opacity", "0");
      })
      // ------ On mouse over show this component -----
      .on('mouseover', function () {
        d3.select(".gender-mouse-over-line")
          .style("opacity", "1");
        d3.selectAll(".component-per-gender circle")
          .style("opacity", "1");
        d3.selectAll(".component-per-gender text")
          .style("opacity", "1");
        d3.selectAll(".gender-mouse-over-text-bg")
          .style("opacity", "0.8");
        d3.selectAll(".gender-mouse-over-line-date")
          .style("opacity", "1");
        d3.select(".gender-mouse-over-text-all")
          .style("opacity", "1");

      })
      // ------ On mouse move capture mouse positions -----
      .on('mousemove', function () {

        var mouse = d3.mouse(this);
        var transformY1, transformY2, transformY3, transformX;
        if (mouse[0] < 79 || mouse[0] > width - 80) {
          d3.select(".gender-mouse-over-line")
            .style("opacity", "0");
          d3.selectAll(".component-per-gender circle")
            .style("opacity", "0");
          d3.selectAll(".component-per-gender text")
            .style("opacity", "0");
          d3.selectAll(".gender-mouse-over-text-bg")
            .style("opacity", "0");
          d3.selectAll(".gender-mouse-over-line-date")
            .style("opacity", "0");
          d3.select(".gender-mouse-over-text-all")
            .style("opacity", "0");
        } else {
          d3.select(".gender-mouse-over-line")
            .style("opacity", "1");
          d3.selectAll(".component-per-gender circle")
            .style("opacity", "1");
          d3.selectAll(".component-per-gender text")
            .style("opacity", "1");
          d3.selectAll(".gender-mouse-over-text-bg")
            .style("opacity", "0.8");
          d3.selectAll(".gender-mouse-over-line-date")
            .style("opacity", "1");
          d3.select(".gender-mouse-over-text-all")
            .style("opacity", "1");
        }

        d3.select(".gender-mouse-over-line")
          .attr("d", function () {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });
        // ------ Creating the continuous transformation -----
        d3.selectAll(".component-per-gender")
          .attr("transform", function (d, i) {
            var xDate = x.invert(mouse[0]);
            //   bisect = d3.bisector(function (d) {
            //     return d.values[i].sdate;
            //   }).right;
            //var idx = bisect(d.values[i].spercent, xDate);

            var beginning = 0, end = 0, target = null;
            if (sentiments[i] != undefined) {

              end = sentiments[i].getTotalLength();
              // if (dataNest[0].values.length > 15) {
              //
              //   if (jwtUtil.getViewport().width < 801) {
              //     if (end > 550) {
              //       if (i == 0) {
              //         end = end + 118;
              //       } else if (i == 1) {
              //         end = end + 139;
              //       }
              //     }
              //   } else if (jwtUtil.getViewport().width > 801 && jwtUtil.getViewport().width < 1281) {
              //     if (end > 650) {
              //       if (i == 0) {
              //         end = end + 135;
              //       } else if (i == 1) {
              //         end = end + 175;
              //       }
              //     }
              //   } else if (jwtUtil.getViewport().width > 1281 && jwtUtil.getViewport().width < 1601) {
              //     if (end > 750) {
              //       if (i == 0) {
              //         end = end + 130;
              //       } else if (i == 1) {
              //         end = end + 180;
              //       }
              //     }
              //   } else if (jwtUtil.getViewport().width > 1601 && jwtUtil.getViewport().width < 1921) {
              //     if (end > 1185) {
              //       if (i == 0) {
              //         end = end + 140;
              //       } else if (i == 1) {
              //         end = end + 168;
              //       }
              //     }
              //   }
              //
              // }
              if (i == 0) {
                end = end + 0;
              } else if (i == 1) {
                end = end - 15
              }

            }


            while (true) {
              var pos = {};
              target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
              if (sentiments[i] != undefined) {
                pos = sentiments[i].getPointAtLength(target);
              }

              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
              }
              if (pos.x != 0 && pos.x > mouse[0]) {
                end = target;
              } else if (pos.x != 0 && pos.x < mouse[0]) {
                beginning = target;
              } else break; //position found
            }

            if (mouse[0] < width - 100) {
              d3.select(".gender-mouse-over-line-date")
                .attr('dx', '1em');
              d3.selectAll(".gender-mouse-over-text-bg")
                .attr('x', '15');
              d3.select(".gender-date-bg")
                .attr('x', '10');
              d3.selectAll(".gender-mouse-over-text" + i)
                .attr('dx', '1em');
            } else {
              d3.select(".gender-mouse-over-line-date")
                .attr('dx', '-100');
              d3.selectAll(".gender-mouse-over-text-bg")
                .attr('x', '-70');
              d3.select(".gender-date-bg")
                .attr('x', '-100');
              d3.selectAll(".gender-mouse-over-text" + i)
                .attr('dx', '-100');
            }

            if (pos.x != 0) {
              // d3.select(this).select('text')
              //   .text(Math.abs(y.invert(pos.y).toFixed()));


              d3.select(".gender-mouse-over-text" + i)
                .text(jwtUtil.truncate(jwtUtil.toTitleCase(d.key),10) + " : " + Math.abs(y.invert(pos.y).toFixed()))
                //.text(jwtUtil.toTitleCase(d.key)+" "+pos.y)
                .attr("transform", "translate(0, " + (((i + 1) * 15) + 10) + ")")
                .style("fill", color[d.key]);


              d3.select(".gender-mouse-over-text-all")
                .attr("transform", "translate(" + mouse[0] + ", 10)");


              if (i == dataNest.length - 1) {
                d3.selectAll(".gender-mouse-over-text-bg")
                  .attr('height', (((i + 1) * 15) + 30))
              }


              var dText = jwtUtil.monthNamesAbbr(xDate.getMonth()) + "/" + jwtUtil.padDigit(xDate.getDate()) + "/" + xDate.getFullYear();


              d3.select(".gender-mouse-over-line-date")
                .text(dText).attr("transform", "translate(0, 10)");
              d3.select(".gender-date-bg")
                .text(dText).attr("transform", "translate(0, 0)");

              if (i == 0) {
                transformY1 = pos.y;
              } else if (i == 1) {
                transformY2 = pos.y;
              } else {
                transformY3 = pos.y;
              }
              transformX = mouse[0];


              return "translate(" + mouse[0] + "," + pos.y + ")";

            }

          });
      });
  };


  /* To create the mouse over effects for Age Distribution cahrt */

  $ctrl.drawAgeMouseHover = function (svg, width, height, color, x, y, dataNest) {

    var sentimentMouseComp = svg.append("g")
      .attr("class", "age-mouse-over-component")
      .attr("transform", "translate(0,0)");

    // Adding vertical Line
    sentimentMouseComp.append("path")
      .attr("class", "age-mouse-over-line")
      .style("stroke-width", "1px")
      .style("stroke-dasharray", ("3, 3"))
      .style("opacity", "0");
    // sentimentMouseComp.append('svg:rect')
    //   .attr('class', 'age-mouse-over-text-bg age-date-bg')
    //   .attr('x','10')
    //   .attr('y', '-12')
    //   .attr('width', '75')
    //   .attr('height', '16')
    //   .style("opacity", "0");
    // sentimentMouseComp.append("text")
    //   .attr("class", "age-mouse-over-line-date")
    //   .attr("transform", "translate(10,3)")
    //   .attr("dx", "1em");

    var sentiments = document.getElementsByClassName('age-chart');

    // Adding "g" for each line
    var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-age')
      .data(dataNest)
      .enter()
      .append("g")
      .attr("class", "component-per-age");

    // Adding circles for each Line
    perSentimentMouseComp.append("circle")
      .attr("r", 5)
      .attr("class", "mouse-circles")
      .style("stroke", function (d, i) {
        return d.color = color;
      })
      .style("fill", "#172530")
      .style("stroke-width", "2px")
      .style("opacity", "0");

    // Adding background for text
    perSentimentMouseComp.append('svg:rect')
      .attr('class', 'age-mouse-over-text-bg')
      .attr('x', '15')
      .attr('y', '-10')
      .attr('width', '60')
      .attr('height', '16')
      .style("opacity", "0");


    // Adding text values to the circles
    perSentimentMouseComp.append("text")
      .attr("transform", "translate(10,3)")
      .attr("class", "age-mouse-over-text")
      .attr("dx", "0.65em");

    // Adding canvas to catch mouse movements
    sentimentMouseComp.append('svg:rect')
      .attr('width', width)
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      // ------ On mouse out hide this component -----
      .on('mouseout', function () {
        d3.select(".age-mouse-over-line")
          .style("opacity", "0");
        d3.selectAll(".component-per-age circle")
          .style("opacity", "0");
        d3.selectAll(".component-per-age text")
          .style("opacity", "0");
        d3.selectAll(".age-mouse-over-text-bg")
          .style("opacity", "0");
        d3.selectAll(".age-mouse-over-line-date")
          .style("opacity", "0");
      })
      // ------ On mouse over show this component -----
      .on('mouseover', function () {
        d3.select(".age-mouse-over-line")
          .style("opacity", "1");
        d3.selectAll(".component-per-age circle")
          .style("opacity", "1");
        d3.selectAll(".component-per-age text")
          .style("opacity", "1");
        d3.selectAll(".age-mouse-over-text-bg")
          .style("opacity", "1");
        d3.selectAll(".age-mouse-over-line-date")
          .style("opacity", "1");

      })
      // ------ On mouse move capture mouse positions -----
      .on('mousemove', function () {
        var mouse = d3.mouse(this);
        var transformY1, transformY2, transformY3, transformX;
        if (mouse[0] < 1) {
          d3.select(".age-mouse-over-line")
            .style("opacity", "0");
          d3.selectAll(".component-per-age circle")
            .style("opacity", "0");
          d3.selectAll(".component-per-age text")
            .style("opacity", "0");
          d3.selectAll(".age-mouse-over-text-bg")
            .style("opacity", "0");
          d3.selectAll(".age-mouse-over-line-date")
            .style("opacity", "0");
        } else {
          d3.select(".age-mouse-over-line")
            .style("opacity", "1");
          d3.selectAll(".component-per-age circle")
            .style("opacity", "1");
          d3.selectAll(".component-per-age text")
            .style("opacity", "1");
          d3.selectAll(".age-mouse-over-text-bg")
            .style("opacity", "1");
          d3.selectAll(".age-mouse-over-line-date")
            .style("opacity", "1");
        }

        d3.select(".age-mouse-over-line")
          .attr("d", function () {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        // ------ Creating the continuous transformation -----
        d3.selectAll(".component-per-age")
          .attr("transform", function (d, i) {
            var xDate = x.invert(mouse[0]),
              bisect = d3.bisector(function (d) {
                return d.values[i].sdate;
              }).right;
            //var idx = bisect(d.values[i].spercent, xDate);

            var beginning = 0, end = 0, target = null;
            if (sentiments[i] != undefined) {
              end = sentiments[i].getTotalLength();
              if (end > 450) {
                  end = end -270;
              }
            }

            while (true) {
              var pos = {};
              target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
              if (sentiments[i] != undefined) {
                pos = sentiments[i].getPointAtLength(target);
              }

              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
              }
              if (pos.x != 0 && pos.x > mouse[0]) end = target;
              else if (pos.x != 0 && pos.x < mouse[0]) beginning = target;
              else break; //position found
            }

            if (mouse[0] < width - 80) {
              // d3.select(".age-mouse-over-line-date")
              //   .attr('dx', '1em');
              d3.selectAll(".age-mouse-over-text-bg")
                .attr('x', '15');
              // d3.select(".age-date-bg")
              //   .attr('x', '10');
              d3.selectAll(".age-mouse-over-text")
                .attr('dx', '0.65em');
            } else {
              // d3.select(".age-mouse-over-line-date")
              //   .attr('dx', '-90');
              d3.selectAll(".age-mouse-over-text-bg")
                .attr('x', '-70');
              // d3.select(".age-date-bg")
              //   .attr('x', '-90');
              d3.selectAll(".age-mouse-over-text")
                .attr('dx', '-75');
            }

            if (pos.x != 0) {
              d3.select(this).select('text')
                .text((y.invert(pos.y)).toFixed(2));

              // var dText = jwtUtil.monthNamesAbbr(xDate.getMonth())+"/"+jwtUtil.padDigit(xDate.getDate())+"/"+xDate.getFullYear();
              //
              // d3.select(".age-mouse-over-line-date")
              //   .text(dText).attr("transform", "translate(" + mouse[0] + ", 10)");
              // d3.select(".date-bg")
              //   .text(dText).attr("transform", "translate(" + mouse[0] + ", 10)");

              if (i == 0) {
                transformY1 = pos.y;
              } else if (i == 1) {
                transformY2 = pos.y;
              } else {
                transformY3 = pos.y;
              }
              transformX = mouse[0];


              return "translate(" + mouse[0] + "," + pos.y + ")";

            }

          });
      });
  };


}
