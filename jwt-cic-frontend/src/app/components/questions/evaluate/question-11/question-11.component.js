(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate11', {
      controller: questionEvaluate11Controller,
      templateUrl: 'app/components/questions/evaluate/question-11/evaluate.question-11.html',
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'
      }
    });

})();
