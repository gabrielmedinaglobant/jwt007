(function() {
    'use strict';
    angular
        .module('app.components')
        .component('jwtQuestionEvaluate7', {
            controller: questionEvaluate7Controller,
            templateUrl: 'app/components/questions/evaluate/question-7/evaluate.question-7.html',
            bindings: {
                data: '<',
                isWorkspace: '<',
                showWorkspaceSelect: '<',
                jwtChartId: '<'
            }
        });

})();