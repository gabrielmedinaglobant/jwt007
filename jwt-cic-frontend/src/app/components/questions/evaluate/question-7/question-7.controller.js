/* --------- Controller for Question 7  --------------*|
 |---  Q7: What brands are people talking about and how do they feel about them?----|
 |----------------------------------------------------*/

questionEvaluate7Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$log', '$element', 'D3Service', 'jwtQuestionEvaluate7Service'];

function questionEvaluate7Controller($scope, jwtAppConstant, $rootScope, $log, $element, d3, jwtQuestionEvaluate7Service) {

    var $ctrl = this;
    $scope.question = jwtQuestionEvaluate7Service.getConstant('INFO');


    //$ctrl.expanded = $scope.expanded;
    $scope.selectedBrand = '';
    $scope.brands = [];
    $ctrl.viewModel = { expanded: false };
    $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: null };
    $ctrl.summary = [];
    $ctrl.color = $scope.question.colors;
    $ctrl.dataLoad = false;
    var q2timeOut, q2timeOut2;

    //added for Q7
    $scope.brandSplineChartData = [];


    // code end

    /* ** Getting data from the service ** */

    $ctrl.$onDestroy = function() {
        clearTimeout(q2timeOut);
        clearTimeout(q2timeOut2);
    }

    $ctrl.$onInit = function() {

        jwtQuestionEvaluate7Service.getData(jwtQuestionEvaluate7Service.getConstant('QAPI'))
            .then(function(response) {
                if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
                    $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                    $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                }
                $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
                $scope.responseData = response.data;
                var data = response.data;
                $scope.brands = data.value.brands;
                //$scope.average = data.value.sentimentOvertimeSplineChart.horizontalMarkers;

                var lastRefreshedDate = new Date(data.value.lastUpdatedDate * 1000);
                $scope.lastRefreshedDate = lastRefreshedDate.getDate() + "/" + (lastRefreshedDate.getMonth() + 1) + "/" + lastRefreshedDate.getFullYear();
                $scope.brands.forEach(function(value) {

                    value.key = jwtUtil.toTitleCase(value.key)
                });

                if (data != undefined) {
                    $ctrl.dataLoad = true;
                }

                $.map(data.value.sentimentOvertimeSplineChart.lines, function(value, index) {
                    $scope.brandSplineChartData[index] = [];
                    $.map(value.values, function(data, i) {
                        var nDate = new Date(data.value[0] * 1000);
                        var percent = data.value[1];
                        $scope.brandSplineChartData[index].push({ "sentiment": value.key, "sdate": nDate, "spercent": percent, "ratio": value.ratio });
                    });
                });

                $ctrl.data = $scope.brandSplineChartData[0];

                $scope.selectedBrand = $scope.brands[0].key + " " + $scope.brands[0].ratio;
                $scope.average = $scope.responseData.value.sentimentOvertimeSplineChart.horizontalMarkers.filter(function(el) {
                    return jwtUtil.toTitleCase(el.tag) === $scope.selectedBrand.split(" ")[0];
                });

                d3.select("." + $scope.brands[0].key).classed("selected", true);


                $ctrl.initDrawChart();

            }, function(error) {
                $ctrl.showLoader();
            });

    }

    /* ** Class for Grid layout ** */
    $scope.masonryelementclass = "masonry-brick";

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function(ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {

            $ctrl.showLoader();
            $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
            q2timeOut2 = setTimeout(function() {
                $scope.expandTile(data.event, data.elem);
            }, 300);
        }
    });

    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function(e, args) {
        $ctrl.initRedrawChart(args.resized);
    });


    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function(resize, elem) {
        var targetElement, chartCont;
        if (resize && $ctrl.data != undefined) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
            $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
        } else {
            $ctrl.showLoader();
        }
    }

    $ctrl.initDrawChart = function() {
        if ($ctrl.isWorkspace) {
            //$scope.showQuestion = true;
            $ctrl.viewModel.expanded = true;
            $('.masonry-brick').addClass('big-tile popped-window');
            //$scope.expanded = true;
            //$ctrl.expanded = true;
            $ctrl.viewModel.expanded = true;
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
            if (poppedContWidth < 650) {
              poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            newWindWid = Math.round(parseInt(newWindWid) * 0.75);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
            $ctrl.drawChart(newWindWid, newWindHgt, $ctrl.data);
        } else {
            var width = $('.chart-cont-q7').width();
            var height = 360;

            $ctrl.drawChart(width, height, $ctrl.data);
        }
    }

    /* ** Show loader ** */
    $ctrl.showLoader = function() {
        var elem = $('.chart-cont-q7 .mention-line-chart');
        elem.empty();
        elem.html('<div class="loader"></div>');
    }

    /* ** Function for expanding the card ** */
    $scope.expandTile = function(e, elem) {
        setTimeout(function() {
            e.stopPropagation();
            e.preventDefault();
            var targetElement = $(elem).parents('.masonry-brick');
            targetElement.siblings().removeClass('big-tile');
            targetElement.toggleClass('big-tile');
            var nWidth = Math.round(targetElement.innerWidth());
            var nHeight = Math.round(targetElement.innerHeight());
            if (!$ctrl.viewModel.expanded) {
                nHeight = (nHeight > 360) ? 360 : nHeight;
            } else if ($ctrl.viewModel.expanded) {
                nHeight = (nHeight > 360) ? 420 : nHeight;
            }

            $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

        }, 100);

    };

    $scope.updateSplineChart = function(brandName) {
        d3.selectAll("button").classed("selected", false);

        d3.select("." + brandName).classed("selected", true);

        var selectedBrandIndex = jwtUtil.ValueIndexInArrayObject($scope.brands, brandName, "key");
        if (selectedBrandIndex != -1) {
            $ctrl.data = $scope.brandSplineChartData[selectedBrandIndex];
        }

        $scope.selectedBrand = $scope.brands[selectedBrandIndex].key + " " + $scope.brands[selectedBrandIndex].ratio;
        $scope.average = $scope.responseData.value.sentimentOvertimeSplineChart.horizontalMarkers.filter(function(el) {
            return jwtUtil.toTitleCase(el.tag) === $scope.selectedBrand.split(" ")[0];
        });

        if (!$ctrl.isWorkspace) {
            var targetElement = $('.masonry-brick');

            var nWidth = Math.round(targetElement.innerWidth());
            var nHeight = Math.round(targetElement.innerHeight());
            if ($ctrl.viewModel.expanded) {
                nHeight = (nHeight > 360) ? 420 : nHeight;
            }
            $ctrl.drawChart(nWidth, nHeight, $ctrl.data);
        } else {
            $ctrl.initDrawChart();
        }

    }

    /* ** Re rendering the chart on resize of the card ** */
    this.redrawChart = function(w, h, qservice) {
            var elem = $('.chart-cont-q7 .mention-line-chart');
            elem.empty();
            this.drawChart(w, h, qservice);
        },



        /* --------- Rendering the chart  -------------------*|
         |- Params : Width, Height and data from the service --|
         |-----------------------------------------------------*/
        this.drawChart = function(w, h, data) {

            // Set the dimensions of the canvas / graph
            var margin = { top: 30, right: 20, bottom: 30, left: 50 },
                width, height;
            if (!$ctrl.viewModel.expanded) {
                width = w - margin.left - 40;
                height = h - margin.top - margin.bottom - 40;
            } else {
                width = w - margin.left - 90;
                height = h - margin.top - margin.bottom - 75;
            }

            // Set the ranges
            var x = d3.scaleTime().range([0, width - 40]);
            var y = d3.scaleLinear().range([height, 0]);

            // Define the line
            var percentline = d3.line()
                .curve(d3.curveBasis)
                .x(function(d) {
                    return x(d.sdate);
                })
                .y(function(d) {
                    return y(d.spercent);
                });

            // Define the average line
            var averageline = d3.line()
                //.curve(d3.curveBasis)
                .x(function(d) {
                    return x(d.sdate);
                })
                .y(function(d) {
                    return y($scope.average[0].value);
                });

            $('.chart-cont-q7 .mention-line-chart').empty();
            var svg = d3.select('.chart-cont-q7 .mention-line-chart')
                .append("svg")
                .attr("width", width + 80)
                .attr("height", height + margin.top + margin.bottom)

            .append("g")
                .attr("transform", function(d, i) {
                    if (!$ctrl.viewModel.expanded) {
                        return "translate(40,30)";
                    } else {
                        return "translate(60,30)";
                    }
                })

            // Get the data
            var data = data;

            data.forEach(function(d) {
                d.sdate = d.sdate;
                d.spercent = +d.spercent;
            });

            // Scale the range of the data
            x.domain(d3.extent(data, function(d) {
                return d.sdate;
            }));

            var mPercent = Math.max.apply(Math, data.map(function(o) {
                return o.spercent;
            }));
            var maxY = mPercent + (10 - (mPercent % 10));
            y.domain([0, maxY])
                .nice();

            var xAxis;
            var intDate;
            if ($ctrl.viewModel.expanded) {
                intDate = 1;
            } else {
                intDate = 2;
            }

            xAxis = d3.axisBottom(x)
                .ticks(d3.timeMonday, 2)
                .tickSizeOuter(0)
                .tickFormat(d3.timeFormat("%B %Y"))

            var yAxis = d3.axisLeft(y)

            .tickSizeOuter(0);

            // //appending the average line
            svg.append("path")
                .data([data])
                .attr("class", "line average-line")
                .style("stroke-dasharray", ("3, 3"))
                .attr("d", averageline);

            //appending the Average text

            svg.append("text")
                .attr("transform", "translate(" + (width - margin.top) + "," + y($scope.average[0].value) + ")")
                .attr("dy", ".35em")
                .attr("text-anchor", "start")
                .style("fill", "#5e83a0")
                .text($scope.average[0].displayName);

            // Nest the entries by symbol
            var dataNest = d3.nest()
                .key(function(d) {
                    return d.sentiment;
                })
                .entries(data);


            // set the colour scale
            var color = $ctrl.color;

            // Loop through each symbol / key
            dataNest.forEach(function(d, i) {
                svg.append("path")
                    .attr("class", "mention")
                    .style("stroke", color[i])
                    .style("fill", "none")
                    .attr("d", percentline(d.values));
            });

            // Add the X Axis
            svg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")

            // Add the Y Axis
                svg.append("g")
                    .attr("class", "axis")
                    .call(yAxis)
                    .append("text")
                    .attr("class", "axis-info-labels")
                    .attr("text-anchor", "middle")
                    .attr("transform", "translate(-30," + (height / 2) + ")rotate(-90)")
                    .text("Documents")
                    .style("font-size", "14px");


            $ctrl.drawMouseHover(svg, width, height, color, x, y, dataNest);
        };

    $ctrl.drawMouseHover = function(svg, width, height, color, x, y, dataNest) {
        /* To create the mouse over effects */

        var sentimentMouseComp = svg.append("g")
            .attr("class", "brands-mouse-over-component");

        // Adding vertical Line
        sentimentMouseComp.append("path")
            .attr("class", "brands-mouse-over-line")
            .style("stroke-width", "1px")
            .style("stroke-dasharray", ("3, 3"))
            .style("opacity", "0");

        //text for new tooltip//

        sentimentMouseComp.append("g")
            .attr('class', 'brands-mouse-over-text-all');

        d3.select('.brands-mouse-over-text-all')
            .append('svg:rect')
            .attr('class', 'brands-mouse-over-text-bg brands-date-bg')
            .attr('x', '5')
            .attr('y', '-20')
            .attr('width', '120')
            .attr('height', '16')
            .style("opacity", "0");

        d3.select('.brands-mouse-over-text-all')
            .append("text")
            .attr("class", "brands-mouse-over-line-date")
            .attr("transform", "translate(10,3)")
            .attr("dx", "1em");

        for (var k = 0; k < dataNest.length; k++) {
            d3.select('.brands-mouse-over-text-all')
                .append('text')
                .attr('class', 'brands-mouse-over-text' + k)
                .attr("dx", "1em");
        }


        var sentiments = document.getElementsByClassName('mention');

        // Adding "g" for each line
        var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-brands')
            .data(dataNest)
            .enter()
            .append("g")
            .attr("class", "component-per-brands");

        // Adding circles for each Line
        perSentimentMouseComp.append("circle")
            .attr("r", 5)
            .attr("class", "mouse-circles")
            .style("stroke", function(d, i) {
                return d.color = color[i];
            })
            .style("fill", "#172530")
            .style("stroke-width", "2px")
            .style("opacity", "0");

        // // Adding background for text
        // perSentimentMouseComp.append('svg:rect')
        //   .attr('class', 'sentiment-mouse-over-text-bg')
        //   .attr('x', '15')
        //   .attr('y', '-10')
        //   .attr('width', '60')
        //   .attr('height', '16')
        //   .style("opacity", "0");
        //
        //
        // // Adding text values to the circles
        // perSentimentMouseComp.append("text")
        //   .attr("transform", "translate(10,3)")
        //   .attr("class", "sentiment-mouse-over-text")
        //   .attr("dx", "0.65em");

        // Adding canvas to catch mouse movements
        sentimentMouseComp.append('svg:rect')
            .attr('width', width)
            .attr('height', height)
            .attr('fill', 'none')
            .attr('pointer-events', 'all')
            // ------ On mouse out hide this component -----
            .on('mouseout', function() {
                d3.select(".brands-mouse-over-line")
                    .style("opacity", "0");
                d3.selectAll(".component-per-brands circle")
                    .style("opacity", "0");
                d3.selectAll(".component-per-brands text")
                    .style("opacity", "0");
                d3.selectAll(".brands-mouse-over-text-bg")
                    .style("opacity", "0");
                d3.select(".brands-mouse-over-text-all")
                    .style("opacity", "0");
                d3.selectAll(".brands-mouse-over-line-date")
                    .style("opacity", "0");
            })
            // ------ On mouse over show this component -----
            .on('mouseover', function() {
                d3.select(".brands-mouse-over-line")
                    .style("opacity", "1");
                d3.selectAll(".component-per-brands circle")
                    .style("opacity", "1");
                d3.selectAll(".component-per-brands text")
                    .style("opacity", "1");
                d3.selectAll(".brands-mouse-over-text-bg")
                    .style("opacity", "0.8");
                d3.selectAll(".brands-mouse-over-line-date")
                    .style("opacity", "1");
                d3.select(".brands-mouse-over-text-all")
                    .style("opacity", "1");

            })
            // ------ On mouse move capture mouse positions -----
            .on('mousemove', function() {
                var mouse = d3.mouse(this);
                var transformY1, transformY2, transformY3, transformX;
                if (mouse[0] < 2 || mouse[0] > width - 40) {
                    d3.select(".brands-mouse-over-line")
                        .style("opacity", "0");
                    d3.selectAll(".component-per-brands circle")
                        .style("opacity", "0");
                    d3.selectAll(".component-per-brands text")
                        .style("opacity", "0");
                    d3.selectAll(".brands-mouse-over-text-bg")
                        .style("opacity", "0");
                    d3.selectAll(".brands-mouse-over-line-date")
                        .style("opacity", "0");
                    d3.select(".brands-mouse-over-text-all")
                        .style("opacity", "0");
                } else {
                    d3.select(".brands-mouse-over-line")
                        .style("opacity", "1");
                    d3.selectAll(".component-per-brands circle")
                        .style("opacity", "1");
                    d3.selectAll(".component-per-brands text")
                        .style("opacity", "1");
                    d3.selectAll(".brands-mouse-over-text-bg")
                        .style("opacity", "0.8");
                    d3.selectAll(".brands-mouse-over-line-date")
                        .style("opacity", "1");
                    d3.select(".brands-mouse-over-text-all")
                        .style("opacity", "1");
                }

                d3.select(".brands-mouse-over-line")
                    .attr("d", function() {
                        var d = "M" + mouse[0] + "," + height;
                        d += " " + mouse[0] + "," + 0;
                        return d;
                    });

                // ------ Creating the continuous transformation -----
                d3.selectAll(".component-per-brands")
                    .attr("transform", function(d, i) {
                        var xDate = x.invert(mouse[0]),
                            bisect = d3.bisector(function(d) {
                                return d.values[i].sdate;
                            }).right;
                        //var idx = bisect(d.values[i].spercent, xDate);

                        var beginning = 0,
                            end = 0,
                            target = null;
                        if (sentiments[i] != undefined) {
                            end = sentiments[i].getTotalLength();
                        }

                        while (true) {
                            var pos = {};
                            target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
                            if (sentiments[i] != undefined) {
                                pos = sentiments[i].getPointAtLength(target);
                            }

                            if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                                break;
                            }
                            if (pos.x != 0 && pos.x > mouse[0]) end = target;
                            else if (pos.x != 0 && pos.x < mouse[0]) beginning = target;
                            else break; //position found
                        }

                        if (mouse[0] < width - 110) {
                            d3.select(".brands-mouse-over-line-date")
                                .attr('dx', '1em');
                            d3.selectAll(".brands-mouse-over-text-bg")
                                .attr('x', '15');
                            d3.select(".brands-date-bg")
                                .attr('x', '10');
                            d3.selectAll(".brands-mouse-over-text" + i)
                                .attr('dx', '1em');
                        } else {
                            d3.select(".brands-mouse-over-line-date")
                                .attr('dx', '-120');
                            d3.selectAll(".brands-mouse-over-text-bg")
                                .attr('x', '-70');
                            d3.select(".brands-date-bg")
                                .attr('x', '-120');
                            d3.selectAll(".brands-mouse-over-text" + i)
                                .attr('dx', '-120');
                        }

                        if (pos.x != 0) {
                            // d3.select(this).select('text')
                            //   .text(y.invert(pos.y).toFixed());


                            d3.select(".brands-mouse-over-text" + i)
                                .text(jwtUtil.truncate(jwtUtil.toTitleCase(d.key),10) + " : " + Math.abs(y.invert(pos.y).toFixed()))
                                //.text(jwtUtil.toTitleCase(d.key)+" "+pos.y)
                                .attr("transform", "translate(5, " + (((i + 1) * 15) + 10) + ")")
                                .style("fill", color[i]);


                            d3.select(".brands-mouse-over-text-all")
                                .attr("transform", "translate(" + mouse[0] + ", 10)");


                            if (i == dataNest.length - 1) {
                                d3.selectAll(".brands-mouse-over-text-bg")
                                    .attr('height', (((i + 1) * 15) + 30))
                            }

                            var dText = jwtUtil.monthNamesAbbr(xDate.getMonth()) + "/" + jwtUtil.padDigit(xDate.getDate()) + "/" + xDate.getFullYear();

                            d3.select(".brands-mouse-over-line-date")
                                .text(dText).attr("transform", "translate(5, 10)");
                            d3.select(".brands-date-bg")
                                .text(dText).attr("transform", "translate(0, 10)");

                            if (i == 0) {
                                transformY1 = pos.y;
                            } else if (i == 1) {
                                transformY2 = pos.y;
                            } else {
                                transformY3 = pos.y;
                            }
                            transformX = mouse[0];


                            return "translate(" + mouse[0] + "," + pos.y + ")";

                        }

                    });
            });
    };

}
