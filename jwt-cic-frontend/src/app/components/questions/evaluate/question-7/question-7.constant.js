(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate7Constant', {
            QAPI: {
                API: '/questions/brands-people-talking-about',
                PREFIX: true
            },
            INFO: {
                'title': 'Brand Sentiment',
                'qText': 'How do people perceive my brand versus competitors?',
                'description': 'A positive or negative sentiment is allocated to each content.',
                'colors': ['#FF8205']
            }

        });
})();