/* --------- Service for Question 7 --------------*|
|---  Q7: What brands are people talking about and how do they feel about them?----|
|----------------------------------------------------*/

angular
    .module('app.common')
    .factory('jwtQuestionEvaluate7Service', ['commonService', 'jwtQuestionEvaluate7Constant', function(commonService, jwtQuestionEvaluate7Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate7Constant
        }, commonService);

    }]);