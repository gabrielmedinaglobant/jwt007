/* --------- Controller for Question 9  --------------*|
 |---  Q9: Has sentiment changed day over day? --------|
 |----------------------------------------------------*/
questionEvaluate9Controller.$inject = ['$scope', '$rootScope', 'jwtAppConstant', '$element', '$log', 'jwtQuestionEvaluate9Service'];

function questionEvaluate9Controller($scope, $rootScope,  jwtAppConstant, $element, $log, jwtQuestionEvaluate9Service) {
  var $ctrl = this;
  $log.info('in:question:discover:1');
$ctrl.viewModel = {expanded:false};
$ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  //$scope.expanded = false;

  var q9timeOut,q9timeOut2,q9timeOut3;

  var CONST_OBJ_Q9 = jwtQuestionEvaluate9Service.getConstant('QAPI');
  $scope.question = CONST_OBJ_Q9.STATIC_INFO;


  $ctrl.$onInit = function () {
    callApi();
  }
    $ctrl.$onDestroy = function(){
      clearTimeout(q9timeOut);
      clearTimeout(q9timeOut2);
      clearTimeout(q9timeOut3);
    }

  /* ** Getting data from the service ** */
function callApi(){
  jwtQuestionEvaluate9Service.getData(jwtQuestionEvaluate9Service.getConstant('QAPI'))
    .then(function (response) {
      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      }
     $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      $ctrl.data = response.data.value;
      //$scope.showQuestion = false;
      var dataArray = new Array();

      $.map($ctrl.data.splineChart.lines, function (value, index) {
        $.map(value.values, function (data, i) {
          var nDate = new Date(data.key);
          var lDate = new Date(nDate.getFullYear(), nDate.getMonth(), nDate.getDate());
          var percent = data.value[1].toFixed(2);
          dataArray.push({ "sentiment": value.key, "sdate": lDate, "spercent": percent });
        });
      });

      $ctrl.data = dataArray;
      $ctrl.cValue = '0.05';
//      var drawChart = function () {
        if ($ctrl.isWorkspace) {
          //$scope.showQuestion = true;
          $('.masonry-brick').addClass('big-tile popped-window');
          $ctrl.viewModel.expanded = true;
          var newWindWid = jwtUtil.getViewport().width;
          var newWindHgt = jwtUtil.getViewport().height;
          var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
          if (poppedContWidth < 650) {
            poppedContWidth = 680;
          }
          $('.masonry-brick').css('width', poppedContWidth + 'px');
          newWindWid = Math.round(parseInt(newWindWid) * 0.75);
          newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
          $ctrl.drawChart(newWindWid, newWindHgt, dataArray);
        } else {
          var width = $('.chart-cont-q9').width();
          //$scope.showQuestion = false;
          $ctrl.drawChart(width, 360, dataArray);
        }
//      }
//      clearTimeout(q9timeOut);
//      q9timeOut = setTimeout(drawChart, 100);

    }, function (error) {
      $ctrl.showLoader();
    });
}
  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data != undefined) {
      $ctrl.showLoader();
      //$scope.showQuestion = !$scope.showQuestion;
      $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
        clearTimeout(q9timeOut2);
      q9timeOut2 = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q9');
    elem.empty();

    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data != undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }


  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(e.target).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
//    if (targetElement.hasClass('big-tile')) {
//      $ctrl.viewModel.expanded = false;
//    } else {
//      $ctrl.viewModel.expanded = true;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.innerWidth());
    var nHeight = Math.round(targetElement.innerHeight());
    if (!$ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 360 : nHeight;
    }

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q9');
    elem.empty();
//    if ($ctrl.isWorkspace)
//      $scope.showQuestion = !$scope.showQuestion;
    this.drawChart(w, h, qservice);
  },

    /* --------- Rendering the chart  -------------------*|
     |- Params : Width, Height and data from the service --|
     |-----------------------------------------------------*/
    this.drawChart = function (w, h, data) {

      if ($ctrl.isWorkspace && jwtUtil.getViewport().width <= '800') {
        $ctrl.cValue = '0.07';
        w = Math.round(parseInt(jwtUtil.getViewport().width) * 0.90);
      }

      // else if(!$ctrl.isWorkspace && jwtUtil.getViewport().width <= 1280 && jwtUtil.getViewport().width > 800){
      //   $ctrl.cValue = '0.4';
      // }

      // Set the dimensions of the canvas / graph
      var margin = { top: 30, right: 20, bottom: 30, left: 50 }, width, height;
      if (!$ctrl.viewModel.expanded) {
        width = w - margin.left - margin.right;
        height = h - margin.top - margin.bottom;
      } else {
        width = w - margin.left - 30;
        height = h - margin.top - margin.bottom - 100;
      }

      // Set the ranges
      var x = d3.scaleTime().range([0, width]);
      var y = d3.scaleLinear().range([height, 0]);

      // Define the line
      var percentline = d3.line()
        .curve(d3.curveBasis)
        .x(function (d) {
          return x(d.sdate);
        })
        .y(function (d) {
          return y(d.spercent);
        });


      var svg = d3.select('.chart-cont-q9')
        .append("svg").attr("class", "sentiment-graph")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)

        .append("g")
        .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

      if ($ctrl.viewModel.expanded) {
        svg.classed("sentiment-graph", true);
      } else {
        svg.classed("sentiment-graph", false);
      }


      // Get the data
      var data = data;
      data.forEach(function (d) {
        d.sdate = d.sdate;
        d.spercent = +d.spercent;
      });

      // Scale the range of the data
      x.domain(d3.extent(data, function (d) {
        return d.sdate;
      }));

      var mPercent = Math.max.apply(Math, data.map(function (o) {
        return o.spercent;
      }));
      var maxY = mPercent + (10 - (mPercent % 10).toFixed(2));
      y.domain([0, maxY]);

      var xAxis;
      if ($ctrl.viewModel.expanded) {
        xAxis = d3
          .axisBottom(x)
          .ticks(d3.timeMonday, 1)
          .tickFormat(d3.timeFormat("%b/%d/%y"));
      } else {
        xAxis = d3
          .axisBottom(x)
          .ticks(d3.timeMonday, 2)
          .tickFormat(d3.timeFormat("%b %y"));
      }

      // Nest the entries by symbol
      var dataNest = d3.nest()
        .key(function (d) {
          return d.sentiment;
        })
        .entries(data);

      // set the colour scale
      var color = $scope.question.colors;

      // Loop through each symbol / key
      dataNest.forEach(function (d, i) {
        svg.append("path")
          .attr("class", "sentiment")
          .style("stroke", function () {
            return d.color = color[d.key];
          })
          .attr("d", percentline(d.values));
      });

      var pathObjArr = [];
      d3.selectAll('path.sentiment').each(function (d, i) {
        pathObjArr.push(d3.select(this));
      });

      /* ** For creating intersection points and sentiment range ** */
      var sentiments = document.getElementsByClassName('sentiment');

      var createIntersection = function () {

        $ctrl.createIntersection(pathObjArr, sentiments, svg, width, height, x, y);

        d3.select('.sentiment-inter-component').style("opacity", "1");

      };

      if ($ctrl.viewModel.expanded) {
          clearTimeout(q9timeOut3);
        q9timeOut3 = setTimeout(createIntersection, 100);
      }
      /* ---------------------- End ---------------------------- */

      // Add the X Axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("dx", "-0.75em");

      // Add the Y Axis
      svg.append("g")
        .attr("class", "axis")
        .call(d3.axisLeft(y))
        .append("text")
        .attr("class", "axis-info-labels")
        .attr("text-anchor", "middle")
        .attr("transform", "translate(-30," + (height / 2) + ")rotate(-90)")
        .text($scope.question.yLabel);


      $ctrl.drawMouseHover(svg, width, height, color, x, y, dataNest);

    };

  $ctrl.drawMouseHover = function (svg, width, height, color, x, y, dataNest) {
    /* To create the mouse over effects */

    var sentimentMouseComp = svg.append("g")
      .attr("class", "sentiment-mouse-over-component");

    // Adding vertical Line
    sentimentMouseComp.append("path")
      .attr("class", "sentiment-mouse-over-line")
      .style("stroke-width", "1px")
      .style("stroke-dasharray", ("3, 3"))
      .style("opacity", "0");
      //text for new tooltip//

    sentimentMouseComp.append("g")
      .attr('class', 'sentiment-mouse-over-text-all');

      d3.select('.sentiment-mouse-over-text-all')
        .append('svg:rect')
        .attr('class', 'sentiment-mouse-over-text-bg date-bg')
        .attr('x','5')
        .attr('y', '-20')
        .attr('width', '100')
        .attr('height', '16')
        .style("opacity", "0");

      d3.select('.sentiment-mouse-over-text-all')
        .append("text")
        .attr("class", "sentiment-mouse-over-line-date")
        .attr("transform", "translate(10,3)")
        .attr("dx", "1em");

    for(var k=0; k<dataNest.length; k++){
      d3.select('.sentiment-mouse-over-text-all')
        .append('text')
        .attr('class', 'sentiment-mouse-over-text'+k)
        .attr("dx", "1em");
    }


    var sentiments = document.getElementsByClassName('sentiment');

    // Adding "g" for each line
    var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-sentiment')
      .data(dataNest)
      .enter()
      .append("g")
      .attr("class", "component-per-sentiment");

    // Adding circles for each Line
    perSentimentMouseComp.append("circle")
      .attr("r", 5)
      .attr("class", "mouse-circles")
      .style("stroke", function (d) {
        return d.color = color[d.key];
      })
      .style("fill", "#172530")
      .style("stroke-width", "2px")
      .style("opacity", "0");

    // Adding background for text
    // perSentimentMouseComp.append('svg:rect')
    //   .attr('class', 'sentiment-mouse-over-text-bg')
    //   .attr('x', '15')
    //   .attr('y', '-10')
    //   .attr('width', '35')
    //   .attr('height', '16')
    //   .style("opacity", "0");
    //
    //
    // // Adding text values to the circles
    // perSentimentMouseComp.append("text")
    //   .attr("transform", "translate(10,3)")
    //   .attr("class", "sentiment-mouse-over-text")
    //   .attr("dx", "0.65em");

    // Adding canvas to catch mouse movements
    sentimentMouseComp.append('svg:rect')
      .attr('width', width)
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      // ------ On mouse out hide this component -----
      .on('mouseout', function () {
        d3.select(".sentiment-mouse-over-line")
          .style("opacity", "0");
        d3.selectAll(".component-per-sentiment circle")
          .style("opacity", "0");
        d3.selectAll(".component-per-sentiment text")
          .style("opacity", "0");
        d3.selectAll(".sentiment-mouse-over-text-bg")
          .style("opacity", "0");
        d3.select(".sentiment-mouse-over-text-all")
          .style("opacity", "0");
        d3.selectAll(".sentiment-mouse-over-line-date")
          .style("opacity", "0");
      })
      // ------ On mouse over show this component -----
      .on('mouseover', function () {
        d3.select(".sentiment-mouse-over-line")
          .style("opacity", "1");
        d3.selectAll(".component-per-sentiment circle")
          .style("opacity", "1");
        d3.selectAll(".component-per-sentiment text")
          .style("opacity", "1");
        d3.selectAll(".sentiment-mouse-over-text-bg")
          .style("opacity", "0.8");
        d3.selectAll(".sentiment-mouse-over-line-date")
          .style("opacity", "1");
        d3.select(".sentiment-mouse-over-text-all")
          .style("opacity", "1");

      })
      // ------ On mouse move capture mouse positions -----
      .on('mousemove', function () {
        var mouse = d3.mouse(this);
        var transformY1, transformY2, transformY3, transformX;
        if (mouse[0] < 1) {
          d3.select(".sentiment-mouse-over-line")
            .style("opacity", "0");
          d3.selectAll(".component-per-sentiment circle")
            .style("opacity", "0");
          d3.selectAll(".component-per-sentiment text")
            .style("opacity", "0");
          d3.selectAll(".sentiment-mouse-over-text-bg")
            .style("opacity", "0");
          d3.selectAll(".sentiment-mouse-over-line-date")
            .style("opacity", "0");
          d3.select(".sentiment-mouse-over-text-all")
            .style("opacity", "0");
        } else {
          d3.select(".sentiment-mouse-over-line")
            .style("opacity", "1");
          d3.selectAll(".component-per-sentiment circle")
            .style("opacity", "1");
          d3.selectAll(".component-per-sentiment text")
            .style("opacity", "1");
          d3.selectAll(".sentiment-mouse-over-text-bg")
            .style("opacity", "1");
          d3.selectAll(".sentiment-mouse-over-line-date")
            .style("opacity", "1");
          d3.select(".sentiment-mouse-over-text-all")
            .style("opacity", "1");
        }

        d3.select(".sentiment-mouse-over-line")
          .attr("d", function () {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        // ------ Creating the continuous transformation -----
        d3.selectAll(".component-per-sentiment")
          .attr("transform", function (d, i) {
            var xDate = x.invert(mouse[0]),
              bisect = d3.bisector(function (d) {
                return d.values[i].sdate;
              }).right;
            var idx = bisect(d.values[i].spercent, xDate);

            var beginning = 0, end = 0, target = null;
            if (sentiments[i] != undefined) {
              end = sentiments[i].getTotalLength();
            }

            while (true) {
              var pos = {};
              target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
              if (sentiments[i] != undefined) {
                pos = sentiments[i].getPointAtLength(target);
              }

              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
              }
              if (pos.x != 0 && pos.x > mouse[0]) end = target;
              else if (pos.x != 0 && pos.x < mouse[0]) beginning = target;
              else break; //position found
            }

            if (mouse[0] < width - 80) {
              d3.select(".sentiment-mouse-over-line-date")
                .attr('dx', '1em');
              d3.selectAll(".sentiment-mouse-over-text-bg")
                .attr('x', '15');
              d3.select(".date-bg")
                .attr('x', '10');
              d3.selectAll(".sentiment-mouse-over-text"+i)
                .attr('dx', '1em');
            } else {
              d3.select(".sentiment-mouse-over-line-date")
                .attr('dx', '-100');
              d3.selectAll(".sentiment-mouse-over-text-bg")
                .attr('x', '-45');
              d3.select(".date-bg")
                .attr('x', '-100');
              d3.selectAll(".sentiment-mouse-over-text"+i)
                .attr('dx', '-100');
            }

            if (pos.x != 0) {
              // d3.select(this).select('text')
              //   .text(y.invert(pos.y).toFixed(2));

              d3.select(".sentiment-mouse-over-text"+i)
                .text(jwtUtil.truncate(jwtUtil.toTitleCase(d.key),10)+" : "+Math.abs(y.invert(pos.y).toFixed()))
                //.text(jwtUtil.S(d.key)+" "+pos.y)
                .attr("transform", "translate(5, "+(((i+1)*15)+10)+")")
                .style("fill", color[d.key]);


              d3.select(".sentiment-mouse-over-text-all")
                .attr("transform", "translate(" + mouse[0] + ", 10)");


              if(i==dataNest.length-1){
                d3.selectAll(".sentiment-mouse-over-text-bg")
                  .attr('height', (((i+1)*15)+30))
              }

              var dText = jwtUtil.monthNamesAbbr(xDate.getMonth()) + "/" + jwtUtil.padDigit(xDate.getDate()) + "/" + xDate.getFullYear();

              d3.select(".sentiment-mouse-over-line-date")
                .text(dText).attr("transform", "translate(5, 10)");
              d3.select(".date-bg")
                .text(dText).attr("transform", "translate(0, 10)");

              if (i == 0) {
                transformY1 = pos.y;
              } else if (i == 1) {
                transformY2 = pos.y;
              } else {
                transformY3 = pos.y;
              }
              transformX = mouse[0];


              return "translate(" + mouse[0] + "," + pos.y + ")";

            }

          });
      });
  };

  /* ** calculate intersection points by comparing arrays ** */
  $ctrl.getIntersections = function (array1, array2, x, y) {
    var temp = new Array();
    if ((!array1[0]) || (!array2[0])) { // If either is not an array
      return false;
    }
    if (array1.length != array2.length) {
      return false;
    }
    for (var i = 0; i < array1.length; i++) {

      var curX = parseFloat((array1[i].x).toFixed(2)),
        curY = parseFloat(y.invert(array1[i].y).toFixed(2));

      /*var curY = array1[i].y;
       var curX = array1[i].x;*/
      var date1 = x.invert(curX);
      var curArr2X, curArr2Y;

      for (var j = 0; j < array2.length; j++) {

        curArr2X = parseFloat((array2[j].x).toFixed(2));
        curArr2Y = parseFloat(y.invert(array2[j].y).toFixed(2));


        var date2 = x.invert(curArr2X);

        var ypart1Arr1 = Math.floor(curY);
        var ypart1Arr2 = Math.floor(curArr2Y);

        var ypart2Arr1 = parseFloat((curY % 1).toFixed(2));
        var ypart2Arr2 = parseFloat((curArr2Y % 1).toFixed(2));

        if (ypart1Arr2 - ypart1Arr1 == 0 && (Math.abs(ypart2Arr2 - ypart2Arr1)) <= $ctrl.cValue) {

          if ((date1.getMonth() == date2.getMonth() && date1.getDate() == date2.getDate() && date1.getFullYear() == date2.getFullYear())) {

            var dateUnique = true;
            if (temp.length != undefined && temp.length > 0) {
              for (var k = 0; k < temp.length; k++) {
                var tdate = x.invert(temp[k].x);
                var tval = y.invert(temp[k].y);
                if ((tdate.getDate() == date2.getDate())) {
                  if (tdate.getMonth() == date2.getMonth()) {
                    if (tdate.getFullYear() == date2.getFullYear()) {
                      dateUnique = false;
                      break;
                    } else {
                      dateUnique = true;
                    }
                  } else {
                    dateUnique = true;
                  }
                } else {
                  dateUnique = true;
                }
              }
            } else {
              var tObj = array2[j];
              tObj.key = j;
              temp.push(tObj);
              dateUnique = false;
            }

            if (dateUnique) {
              var tObj = array2[j];
              tObj.key = j;
              temp.push(tObj);
              dateUnique = false;
            }

            break;
          }

        }
      }
      if (i == array1.length - 1) {
        if (curArr2Y > curY) {
          var tObj = array2[i];
          tObj.key = i;
          temp.push(tObj);
        } else {
          var tObj = array1[i];
          tObj.key = i;
          temp.push(tObj);
        }
      }
    }
    return temp;

  };

  /* ** Init getting intersection points ** */
  $ctrl.createIntersection = function (pathObjArr, sentiments, svg, width, height, x, y) {

    var arr = [];
    $.map(pathObjArr, function (item, index) {
      var p = [];
      var w = width;
      var pos = { x: 0, y: 0 };
      for (var j = 0; j <= w; j++) {
        pos = sentiments[index].getPointAtLength(j);
        p.push({ x: pos.x, y: pos.y });
        //p.push({x: parseFloat((pos.x).toFixed(2)), y: parseFloat(y.invert(pos.y).toFixed(2))});
      }
      var pNode = {};
      pNode["path"] = p;
      arr.push(pNode);
    });

    var p1, p2, p3;
    $.map(arr, function (data, i) {
      if (i == 0) {
        p1 = data;
      } else if (i == 1) {
        p2 = data
      } else if (i == 2) {
        p3 = data;
      }
    });

    // Getting intersection points and sentiment range
    var getPoints = $ctrl.getIntersections(p1.path, p2.path, x, y);
    var getSentimentRange = $ctrl.getSentimentRange(p1.path, p2.path, p3.path, getPoints, x, y);

    // Creating svg group for intersection points
    var sentimentIntersectionComp = svg.append("g")
      .attr("class", "sentiment-inter-component")
      .style("opacity", "0");

    for (var i = 0; i < getSentimentRange.length - 1; i++) {

      sentimentIntersectionComp.append("path")
        .attr("class", "sentiment-intersection-line")
        .style("stroke-width", "1px")
        .style("stroke-dasharray", ("3, 3"))
        .attr("d", function () {
          var d = "M" + getSentimentRange[i].x + "," + height;
          d += " " + getSentimentRange[i].x + "," + 0;
          return d;
        });
    }

    $ctrl.createBarChart(getSentimentRange, svg, x, y, width, height);

  };

  /* ** Init bar chart creation ** */
  $ctrl.createBarChart = function (getPoints, svg, x, y, width, height) {
    d3.select('.sentiment-graph').attr("height", parseInt(height) + 140);

    var sentimentBarComp = svg.append("g")
      .attr("class", "sentiment-graph-bar")
      .attr("transform", "translate(0, " + (parseInt(height) + 40) + ")");

    for (var i = 0; i < getPoints.length; i++) {

      var stPoint = 0, barcolor, barWidth;
      if (i != 0) {
        stPoint = getPoints[i - 1].x;
      }
      barcolor = $scope.question.colors[getPoints[i].sentimentType];

      var barsGroup = sentimentBarComp.append("g")
        .attr("transform", "translate(" + stPoint + ", 20)");
      barsGroup.append("svg:rect")
        .attr("class", "sentiment-bar")
        .attr("fill", barcolor)
        .attr("width", function () {
          if (i != 0) {
            if (i == getPoints.length - 1) {
              var w1 = width - parseInt(getPoints[i].x);
              var w2 = (getPoints[i].x - getPoints[i - 1].x).toFixed(2);
              barWidth = parseInt(w1) + parseInt(w2);
            } else {
              barWidth = (getPoints[i].x - getPoints[i - 1].x).toFixed(2);
            }
          } else {
            barWidth = (getPoints[i].x).toFixed(2);
          }
          return barWidth;
        })
        .attr("height", "22px");

      if (i < getPoints.length - 1) {
        var bDate = x.invert(getPoints[i].x);
        var nFormat = d3.timeFormat("%b/%d/%y");
        barsGroup.append("text")
          .attr("class", "bar-range-date")
          .attr("transform", function () {
            var tempWidth;
            if (i != 0) {
              var w1 = parseFloat(getPoints[i].x).toFixed(2);
              var w2 = parseFloat(getPoints[i - 1].x).toFixed(2);
              tempWidth = w1 - w2;
            } else {
              tempWidth = parseFloat(getPoints[i].x).toFixed(2);
            }
            return "translate(" + tempWidth + ", 40)";
          })
          .attr("dx", "-2em")
          .attr("text-anchor", "left")
          .text(nFormat(bDate));
      }
    }

  };

  /* ** Get sentiments range and map intersection points ** */
  $ctrl.getSentimentRange = function (arr1, arr2, arr3, points, x, y) {
    var sentiments = points;
    var tempArr1, tempArr2, tempArr3, max1, max2, max3;
    for (var i = 0; i < points.length; i++) {
      if (i == 0) {
        tempArr1 = arr1.slice(0, points[i].key);
        tempArr2 = arr2.slice(0, points[i].key);
        tempArr3 = arr3.slice(0, points[i].key);
      } else {
        tempArr1 = arr1.slice(points[i - 1].key, points[i].key);
        tempArr2 = arr2.slice(points[i - 1].key, points[i].key);
        tempArr3 = arr3.slice(points[i - 1].key, points[i].key);
      }

      max1 = parseFloat(y.invert(Math.max.apply(Math, tempArr1.map(function (o) { return o.y; }))).toFixed(2));
      max2 = parseFloat(y.invert(Math.max.apply(Math, tempArr2.map(function (o) { return o.y; }))).toFixed(2));
      max3 = parseFloat(y.invert(Math.max.apply(Math, tempArr3.map(function (o) { return o.y; }))).toFixed(2));

      var maxVal = Math.max(max1, max2, max3);
      if (maxVal == max1) {
        sentiments[i].sentimentType = "positive";
      } else if (maxVal == max2) {
        sentiments[i].sentimentType = "negative";
      } else if (maxVal == max3) {
        sentiments[i].sentimentType = "mixed";
      }
    }

    return sentiments;

  };
}
