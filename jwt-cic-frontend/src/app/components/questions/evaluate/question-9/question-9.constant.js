(function () {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionEvaluate9Constant', {
            QAPI: {
                API: '/questions/sentiment-changed-day-over-day',
                PREFIX: true,
                STATIC_INFO: {
                    'title': 'Sentiment',
                    'qText': 'Has sentiment changed day over day?',
                    'colors': { 'positive': '#59a95b', 'negative': '#bd514c', 'mixed': '#eccb30' },
                    'xLabel': 'Date',
                    'yLabel': 'Subject Sentences',
                    'description': 'A day to day view of how the brand is performing by positive, negative and mixed sentiment.',
                    'lblRefreshDate': 'Last Refresh Date',
                    'lblDateRange': 'Data Date Range',
                    'lblFrom': 'From',
                    'lblTo': 'To'
                }
            }
        });
})();
