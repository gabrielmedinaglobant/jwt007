(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionEvaluate9', {
      templateUrl: 'app/components/questions/evaluate/question-9/evaluate.question-9.html',
      controller: questionEvaluate9Controller,
      bindings: {
        data: '<',
        isWorkspace:'<',
        showWorkspaceSelect:'<',
        jwtChartId: '<'  
      }
    });

})();
