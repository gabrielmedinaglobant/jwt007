 /* --------- Service for Question 9  --------------*|
 |---  Q9: Has sentiment changed day over day?--------|
 |------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionEvaluate9Service', ['commonService', 'jwtQuestionEvaluate9Constant', function(commonService, jwtQuestionEvaluate9Constant) {

        return angular.extend({
            cnst: jwtQuestionEvaluate9Constant
        }, commonService);

    }]);

// angular
//    .module('app.common')
//    .service('jwtQuestionEvaluate9Service',['$http','$q','httpService',function($http,$q,httpService){
//
//        // https://demo7088558.mockable.io/linedata
//		// jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//		
//		/* --------- get chart data  --------------*/
//        this.getLineData = function(){
//            return httpService.getData('/questions/sentiment-changed-day-over-day')
//				.then(function(response){
//					if(typeof response.data === 'object' ) {
//						return response.data;
//					} else
//					{
//                        console.log("error");
//						$q.reject(response.data);
//					}
//				},function errorCallback(response) {
//                    console.log("error");
//					$q.reject(response.data);
//				});
//        }
//}]);

