/* --------- Controller for Header ----------*|
 |---  tabs at top for all questions  --------|
 |-------------------------------------------*/
headerController.$inject = ['$scope', '$state', '$timeout', 'jwtAppConstant', '$element', '$log', '$rootScope','headerService'];

function headerController($scope, $state, $timeout, jwtAppConstant, $element, $log, $rootScope, headerService) {
    var $ctrl = this;
    console.log("in:question:header");
    $ctrl.viewModel = {Tab1SelectExpandedState:false,Tab2SelectExpandedState:false};
    //$scope.currtab = $rootScope.state.current.name;
    $scope.currtab = {};
    $scope.headerConst = jwtAppConstant.headerConst;

    /* ** Function for tab navigation ** */
    $scope.changeTab = function(tab) {
        $scope.currtab = $scope.headerConst[tab].state;
        $state.go($scope.currtab);
    };

    $ctrl.onTab1SelectExpanded = function(state){
        $ctrl.viewModel.Tab1SelectExpandedState = state;
            if(headerService.getSessionData('WORSPACES')){
                $ctrl.heightClass1 = "totalHeight" + angular.fromJson(headerService.getSessionData('WORSPACES')).length ;
            }else{
                $ctrl.heightClass1 = 'totalHeight0';
            }
    }

    $ctrl.onTab2SelectExpanded = function(state){
        $ctrl.viewModel.Tab2SelectExpandedState = state;
            if(headerService.getSessionData('WORSPACES')){
                $ctrl.heightClass2 = "totalHeight" + angular.fromJson(headerService.getSessionData('WORSPACES')).length ;
            }else{
                $ctrl.heightClass2 = 'totalHeight0';
            }
    }

    $(document).click(function(e){
     if($(e.target).is("#evaulateTab") || $(e.target).closest("#evaulateTab").length){

     }
    else {
        $ctrl.tab2IsActive = false;
        $ctrl.heightClass2 = 'totalHeightnormal';
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

    })    
    $(document).click(function(e){
     if($(e.target).is("#discoverTab") || $(e.target).closest("#discoverTab").length){

     }
    else {
        $ctrl.tab1IsActive = false;
        $ctrl.heightClass1 = 'totalHeightnormal';
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

    })

}
