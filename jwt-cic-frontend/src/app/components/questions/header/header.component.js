(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionHeader', {
      templateUrl: 'app/components/questions/header/header.html',
      controller: headerController,
      bindings: {
        data: '<'
      }
    });

})();
