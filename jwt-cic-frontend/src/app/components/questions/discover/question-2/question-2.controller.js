/* --------- Controller for Question 2  --------------*|
 |---  Q2: How much have people mentioned me in comparison to competitors?----|
 |----------------------------------------------------*/

questionDiscover2Controller.$inject = ['$scope', 'jwtAppConstant', '$rootScope', '$log', '$element', 'D3Service', 'jwtQuestionDiscover2Service'];

function questionDiscover2Controller($scope, jwtAppConstant, $rootScope, $log, $element, d3, jwtQuestionDiscover2Service) {
  var $ctrl = this;
  $scope.question = jwtQuestionDiscover2Service.getConstant('INFO');
  //$ctrl.expanded = $scope.expanded;
  $ctrl.viewModel = {expanded:false};
  $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  $ctrl.summary = [];
  $ctrl.cloudWidth = '';
  $ctrl.cloudHeight = '';
  $ctrl.type = null;
  $ctrl.color = $scope.question.colors;
  $ctrl.dataLoad = false;
  $ctrl.lowestDate = null;
  $ctrl.highestDate = null;
  var q2timeOut,q2timeOut2;

  /* ** Getting data from the service ** */

    $ctrl.$onDestroy = function(){
      clearTimeout(q2timeOut);
      clearTimeout(q2timeOut2);
    }

  $ctrl.$onInit = function () {
    jwtQuestionDiscover2Service.getData(jwtQuestionDiscover2Service.getConstant('QAPI'))
      .then(function (response) {

      if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US');
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US');
      }
      $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US');

        var data = response.data.value;
        if (data != undefined) {
          $ctrl.dataLoad = true;
        }
        //$scope.showQuestion = false;
        var dataArray = new Array();
        var dataSummary = new Array();

        $.map(data.splineChart.lines, function (value, index) {
          $.map(value.values, function (data, i) {
            var nDate = new Date(data.value[0] * 1000);
            var percent = data.value[1];
            dataArray.push({ "sentiment": value.key, "sdate": nDate, "spercent": percent });
          });
        });

        $.map(data.summary.rows, function (value, index) {
          if (index == 0) {
            $ctrl.lowestDate = (new Date(value.lowestDate * 1000)).toLocaleDateString('en-US');
            $ctrl.highestDate = (new Date(value.highestDate * 1000)).toLocaleDateString('en-US');
          }

          var brand = jwtUtil.toTitleCase(value.brand);
          var brandKey = value.brandKey;
          var total = jwtUtil.formatWithCommas(value.total, "f");
          var totalPercentage = value.totalPercentage;
          var lowest = jwtUtil.formatWithCommas(value.lowest, "f");
          var lowestDate = new Date(value.lowestDate * 1000);
          var average = (jwtUtil.formatWithCommas((value.average).toFixed(0), "f"));
          var highest = jwtUtil.formatWithCommas(value.highest, "f");
          var highestDate = new Date(value.highestDate * 1000);
          var changeValue = value.changeValue;
          var changeUnit = value.changeUnit;

          dataSummary.push({
            "brand": brand, "brandKey": brandKey, "total": total, "totalPercentage": totalPercentage, "lowest": lowest,
            "lowestDate": lowestDate, "average": average, "highest": highest, "highestDate": highestDate,
            "changeValue": changeValue, "changeUnit": changeUnit
          });
        });


        $ctrl.summary = dataSummary;
        $ctrl.data = dataArray;
        var drawChart = function () {
          if ($ctrl.isWorkspace) {
            //$scope.showQuestion = true;
            $ctrl.viewModel.expanded = true;
            $('.masonry-brick').addClass('big-tile popped-window');
            //$scope.expanded = true;
            //$ctrl.expanded = true;
            $ctrl.viewModel.expanded = true;
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
            if (poppedContWidth < 650) {
              poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            newWindWid = Math.round(parseInt(newWindWid) * 0.75);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
            $ctrl.drawChart(newWindWid, newWindHgt, dataArray);
          } else {
            var width = $('.chart-cont-q2').width();
            //$scope.showQuestion = false;
            $ctrl.drawChart(width, 360, dataArray);
          }
        }
        drawChart();
        //q2timeOut = setTimeout(drawChart, 100);

    },
    function(error) {
        $ctrl.showLoader();
    });
  }
  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data!=undefined) {
      $ctrl.showLoader();
        $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;
//      $scope.showQuestion = !$scope.showQuestion;
//      $scope.expanded = !$scope.expanded ;
//      $ctrl.expanded = $scope.expanded;
      q2timeOut2 = setTimeout(function () {
        $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });


  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data !== undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q2 .mention-line-chart');
    elem.empty();
    elem.html('<div class="loader"></div>');
  }

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();

    var targetElement = $(elem).parents('.masonry-brick');

    targetElement.siblings().removeClass('big-tile');
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.innerWidth());
    var nHeight = Math.round(targetElement.innerHeight());
    if (!$ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 360 : nHeight;
    }else if ($ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 420 : nHeight;
    }

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  this.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q2 .mention-line-chart');
    elem.empty();
      this.drawChart(w, h, qservice);
  },



    /* --------- Rendering the chart  -------------------*|
     |- Params : Width, Height and data from the service --|
     |-----------------------------------------------------*/
    this.drawChart = function (w, h, data) {
      // Set the dimensions of the canvas / graph
      var margin = { top: 30, right: 20, bottom: 30, left: 50 }, width, height;
      if (!$ctrl.viewModel.expanded) {
        width = w - margin.left - 40;
        height = h - margin.top - margin.bottom - 40;
      } else {
        width = w - margin.left - 90;
        height = h - margin.top - margin.bottom - 75;
      }

      // Set the ranges
      var x = d3.scaleTime().range([0, width]);
      var y = d3.scaleLinear().range([height, 0]);

      // Define the line
      var percentline = d3.line()
        .curve(d3.curveBasis)
        .x(function (d) {
          return x(d.sdate);
        })
        .y(function (d) {
          return y(d.spercent);
        });


      var svg = d3.select('.chart-cont-q2 .mention-line-chart')
        .append("svg")
        .attr("width", width + 80)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(40,30)");


      // Get the data
      var data = data;
      data.forEach(function (d) {
        d.sdate = d.sdate;
        d.spercent = +d.spercent;
      });

      // Scale the range of the data
      x.domain(d3.extent(data, function (d) {
        return d.sdate;
      }));

      var mPercent = Math.max.apply(Math, data.map(function (o) {
        return o.spercent;
      }));
      var maxY = mPercent + (10 - (mPercent % 10));
      y.domain([0, maxY]);
      var xAxis;
      var intDate;
      if ($ctrl.viewModel.expanded) {
        intDate = 1;
      } else {
        intDate = 2;
      }

      xAxis = d3.axisBottom(x)
        .ticks(d3.timeMonday, intDate)
        .tickSizeOuter(0)
        .tickFormat(d3.timeFormat("%m/%d/%y"))

      var yAxis = d3.axisLeft(y)
        .tickValues([0, maxY]).tickSizeOuter(0);

      // Nest the entries by symbol
      var dataNest = d3.nest()
        .key(function (d) {
          return d.sentiment;
        })
        .entries(data);

      // set the colour scale
      var color = $ctrl.color;
      // Loop through each symbol / key
      dataNest.forEach(function (d, i) {
        svg.append("path")
          .attr("class", "mention")
          .style("stroke", function () {
            return d.color = color[i];
          })
          .style("fill", "none")
          .attr("d", percentline(d.values));
      });

      // Add the X Axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
      //.attr("dx", "-0.75em");

      // Add the Y Axis

      var yAx =   svg.append("g")
          .attr("class", "axis");

       yAx.call(yAxis)
          .selectAll("text")
          .attr("dx", "1.1em")
          .attr("dy", "-1em")
          .attr("transform", "rotate(-90)")
          .style("font-size", "14px")

       yAx.append("text")
          .attr("class", "axis-info-labels")
          .attr("text-anchor", "middle")
          .attr("transform", "translate(-30," + (height / 2) + ")rotate(-90)")
          .text("Posts");

      $ctrl.drawMouseHover(svg, width, height, color, x, y, dataNest);

    };

  $ctrl.drawMouseHover = function (svg, width, height, color, x, y, dataNest) {
    /* To create the mouse over effects */

    var sentimentMouseComp = svg.append("g")
      .attr("class", "sentiment-mouse-over-component");

    // Adding vertical Line
    sentimentMouseComp.append("path")
      .attr("class", "sentiment-mouse-over-line")
      .style("stroke-width", "1px")
      .style("stroke-dasharray", ("3, 3"))
      .style("opacity", "0");

      //text for new tooltip//

    sentimentMouseComp.append("g")
      .attr('class', 'sentiment-mouse-over-text-all');

      d3.select('.sentiment-mouse-over-text-all')
        .append('svg:rect')
        .attr('class', 'sentiment-mouse-over-text-bg date-bg')
        .attr('x','5')
        .attr('y', '-20')
        .attr('width', '120')
        .attr('height', '16')
        .style("opacity", "0");

      d3.select('.sentiment-mouse-over-text-all')
        .append("text")
        .attr("class", "sentiment-mouse-over-line-date")
        .attr("transform", "translate(10,3)")
        .attr("dx", "1em");

    for(var k=0; k<dataNest.length; k++){
      d3.select('.sentiment-mouse-over-text-all')
        .append('text')
        .attr('class', 'sentiment-mouse-over-text'+k)
        .attr("dx", "1em");
    }


    var sentiments = document.getElementsByClassName('mention');

    // Adding "g" for each line
    var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-sentiment')
      .data(dataNest)
      .enter()
      .append("g")
      .attr("class", "component-per-sentiment");

    // Adding circles for each Line
    perSentimentMouseComp.append("circle")
      .attr("r", 5)
      .attr("class", "mouse-circles")
      .style("stroke", function (d, i) {
        return d.color = color[i];
      })
      .style("fill", "#172530")
      .style("stroke-width", "2px")
      .style("opacity", "0");

    // // Adding background for text
    // perSentimentMouseComp.append('svg:rect')
    //   .attr('class', 'sentiment-mouse-over-text-bg')
    //   .attr('x', '15')
    //   .attr('y', '-10')
    //   .attr('width', '60')
    //   .attr('height', '16')
    //   .style("opacity", "0");
    //
    //
    // // Adding text values to the circles
    // perSentimentMouseComp.append("text")
    //   .attr("transform", "translate(10,3)")
    //   .attr("class", "sentiment-mouse-over-text")
    //   .attr("dx", "0.65em");

    // Adding canvas to catch mouse movements
    sentimentMouseComp.append('svg:rect')
      .attr('width', width)
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      // ------ On mouse out hide this component -----
      .on('mouseout', function () {
        d3.select(".sentiment-mouse-over-line")
          .style("opacity", "0");
        d3.selectAll(".component-per-sentiment circle")
          .style("opacity", "0");
        d3.selectAll(".component-per-sentiment text")
          .style("opacity", "0");
        d3.selectAll(".sentiment-mouse-over-text-bg")
          .style("opacity", "0");
        d3.select(".sentiment-mouse-over-text-all")
          .style("opacity", "0");
        d3.selectAll(".sentiment-mouse-over-line-date")
          .style("opacity", "0");
      })
      // ------ On mouse over show this component -----
      .on('mouseover', function () {
        d3.select(".sentiment-mouse-over-line")
          .style("opacity", "1");
        d3.selectAll(".component-per-sentiment circle")
          .style("opacity", "1");
        d3.selectAll(".component-per-sentiment text")
          .style("opacity", "1");
        d3.selectAll(".sentiment-mouse-over-text-bg")
          .style("opacity", "0.8");
        d3.selectAll(".sentiment-mouse-over-line-date")
          .style("opacity", "1");
        d3.select(".sentiment-mouse-over-text-all")
          .style("opacity", "1");

      })
      // ------ On mouse move capture mouse positions -----
      .on('mousemove', function () {
        var mouse = d3.mouse(this);
        var transformY1, transformY2, transformY3, transformX;
        if (mouse[0] < 2) {
          d3.select(".sentiment-mouse-over-line")
            .style("opacity", "0");
          d3.selectAll(".component-per-sentiment circle")
            .style("opacity", "0");
          d3.selectAll(".component-per-sentiment text")
            .style("opacity", "0");
          d3.selectAll(".sentiment-mouse-over-text-bg")
            .style("opacity", "0");
          d3.selectAll(".sentiment-mouse-over-line-date")
            .style("opacity", "0");
          d3.select(".sentiment-mouse-over-text-all")
            .style("opacity", "0");
        } else {
          d3.select(".sentiment-mouse-over-line")
            .style("opacity", "1");
          d3.selectAll(".component-per-sentiment circle")
            .style("opacity", "1");
          d3.selectAll(".component-per-sentiment text")
            .style("opacity", "1");
          d3.selectAll(".sentiment-mouse-over-text-bg")
            .style("opacity", "0.8");
          d3.selectAll(".sentiment-mouse-over-line-date")
            .style("opacity", "1");
          d3.select(".sentiment-mouse-over-text-all")
            .style("opacity", "1");
        }

        d3.select(".sentiment-mouse-over-line")
          .attr("d", function () {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        // ------ Creating the continuous transformation -----
        d3.selectAll(".component-per-sentiment")
          .attr("transform", function (d, i) {
            var xDate = x.invert(mouse[0]),
              bisect = d3.bisector(function (d) {
                return d.values[i].sdate;
              }).right;
            //var idx = bisect(d.values[i].spercent, xDate);

            var beginning = 0, end = 0, target = null;
            if (sentiments[i] != undefined) {
              end = sentiments[i].getTotalLength();
            }

            while (true) {
              var pos = {};
              target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
              if (sentiments[i] != undefined) {
                pos = sentiments[i].getPointAtLength(target);
              }

              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
              }
              if (pos.x != 0 && pos.x > mouse[0]) end = target;
              else if (pos.x != 0 && pos.x < mouse[0]) beginning = target;
              else break; //position found
            }

            if (mouse[0] < width - 110) {
              d3.select(".sentiment-mouse-over-line-date")
                .attr('dx', '1em');
              d3.selectAll(".sentiment-mouse-over-text-bg")
                .attr('x', '15');
              d3.select(".date-bg")
                .attr('x', '10');
              d3.selectAll(".sentiment-mouse-over-text"+i)
                .attr('dx', '1em');
            } else {
              d3.select(".sentiment-mouse-over-line-date")
                .attr('dx', '-120');
              d3.selectAll(".sentiment-mouse-over-text-bg")
                .attr('x', '-70');
              d3.select(".date-bg")
                .attr('x', '-120');
              d3.selectAll(".sentiment-mouse-over-text"+i)
                .attr('dx', '-120');
            }

            if (pos.x != 0) {
              // d3.select(this).select('text')
              //   .text(y.invert(pos.y).toFixed());


              d3.select(".sentiment-mouse-over-text"+i)
                .text(jwtUtil.truncate(jwtUtil.toTitleCase(d.key),10)+" : "+Math.abs(y.invert(pos.y).toFixed()))
                //.text(jwtUtil.toTitleCase(d.key)+" "+pos.y)
                .attr("transform", "translate(5, "+(((i+1)*15)+10)+")")
                .style("fill", color[i]);


              d3.select(".sentiment-mouse-over-text-all")
                .attr("transform", "translate(" + mouse[0] + ", 10)");


              if(i==dataNest.length-1){
                d3.selectAll(".sentiment-mouse-over-text-bg")
                  .attr('height', (((i+1)*15)+30))
              }

              var dText = jwtUtil.monthNamesAbbr(xDate.getMonth()) + "/" + jwtUtil.padDigit(xDate.getDate()) + "/" + xDate.getFullYear();

              d3.select(".sentiment-mouse-over-line-date")
                .text(dText).attr("transform", "translate(5, 10)");
              d3.select(".date-bg")
                .text(dText).attr("transform", "translate(0, 10)");

              if (i == 0) {
                transformY1 = pos.y;
              } else if (i == 1) {
                transformY2 = pos.y;
              } else {
                transformY3 = pos.y;
              }
              transformX = mouse[0];


              return "translate(" + mouse[0] + "," + pos.y + ")";

            }

          });
      });
  };

}
