/* --------- Service for Question 2 --------------*|
|---  Q2: How much have people mentioned me in comparison to competitors?--|
|------------------------------------------------*/
// angular
//    .module('app.common')
//    .service('jwtQuestionDiscover2Service',['$http','$q','httpService','jwtAppConstant',function($http,$q,httpService,jwtAppConstant){
//
//    // jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com
//    /* --------- get chart data  --------------*/
//        this.getData = function(){
//            return httpService.getData(jwtAppConstant.discover.q2.questionAPI)
//        .then(function(response){
//          if(typeof response.data === 'object' ) {
//            return response.data;
//          } else
//          {
//            $q.reject(response.data);
//          }
//        },function errorCallback(response) {
//            $q.reject(response.data);
//        });
//        }
// }]);

angular
   .module('app.common')
   .factory('jwtQuestionDiscover2Service', ['commonService','jwtQuestionDiscover2Constant',function(commonService,jwtQuestionDiscover2Constant) {

       return angular.extend({
           cnst:jwtQuestionDiscover2Constant
       },commonService);

   }]);
