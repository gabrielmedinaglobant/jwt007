(function() {
'use strict';

  angular
    .module('app.components')
    .constant('jwtQuestionDiscover2Constant', {
      QAPI :{
          API:'/questions/people-mentioned-competitors',
          PREFIX:true
      },
      INFO: {
          'title': 'Brand Mentions',
          'qText': 'Are people talking about me or competitors?',
          'description' : 'Total volume of online mentions of a brand across social media, forums, blogs and news sites.',
          'colors': ['#82c2b2','#eccb30','#9983f5','#d7803c' ,'#59a95b']
        }

    });
})();
