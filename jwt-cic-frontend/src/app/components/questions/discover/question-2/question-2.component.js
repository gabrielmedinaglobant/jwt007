(function () {
    'use strict';

    angular
        .module('app.components')
        .component('jwtQuestionDiscover2', {
            controller: questionDiscover2Controller,
            templateUrl: 'app/components/questions/discover/question-2/discover.question-2.html',
            bindings: {
                data: '<',
                isWorkspace: '<',
                showWorkspaceSelect: '<',
                jwtChartId: '<'
            }
        });

})();
