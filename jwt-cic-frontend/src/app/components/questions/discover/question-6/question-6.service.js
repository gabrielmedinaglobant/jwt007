/* --------------- Service for Question 6 ---------------------*|
|---  Q6: What are people talking about overall ----------------|
|--------------------------------------------------------------*/
//angular
//	.module('app.services',[])
//	.factory('jwtQuestionDiscover6Service', ['commonService', 'jwtQ6Constant', function (commonService, jwtQ6Constant) {
//		return angular.extend({
//			cnst: jwtQ6Constant
//		}, commonService);
//	}]);

angular
   .module('app.common')
   .factory('jwtQuestionDiscover6Service', ['commonService','jwtQ6Constant',function(commonService,jwtQ6Constant) {

       return angular.extend({
           cnst:jwtQ6Constant
       },commonService);

   }]);