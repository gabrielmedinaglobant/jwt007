/* -------- Controller for Question 6 discover  ---------------*|
|---  Q6: What are people talking about overall ----------------|
|--------------------------------------------------------------*/
questionDiscover6Controller.$inject = ['$scope', '$rootScope', '$interval', 'jwtAppConstant', '$element', '$log', 'jwtQuestionDiscover6Service'];

function questionDiscover6Controller($scope, $rootScope,  $interval, jwtAppConstant, $element, $log, jwtQuestionDiscover6Service) {
  var $ctrl = this;
  var CONST_OBJ = jwtQuestionDiscover6Service.getConstant('Q6');
  //$scope.expanded = false;
  $ctrl.viewModel = {expanded:false};
  $ctrl.dateRanges = {dateFrom:false,dateTo:null,lastUpdatedDate:null};
  $scope.question = CONST_OBJ.STATIC_INFO;
  var colorBlocks = $scope.question.colors.blockColor.length;
  var stop = '';
  var interval,interval1,q6timeOut,q6timeOut2;
  var textNodeAnimTimer = [];
  var apiTimer = CONST_OBJ.API_TIMER;
  var animTimer = CONST_OBJ.ANIM_TIMER;
  var animTypeSpeed = parseInt(CONST_OBJ.ANIM_TYPESPEED);

  $ctrl.$onInit = function () {
    $ctrl.chartDrawn = false;
    //$scope.showQuestion = false;
    //$scope.expanded = false;
    //$ctrl.viewModel.expanded = false;
    $scope.data = {};
    $ctrl.getService();
  }

    $ctrl.$onDestroy = function(){
      clearInterval(interval);
      clearInterval(interval1);
        textNodeAnimTimer.forEach(function(v,i){
            clearTimeout(textNodeAnimTimer[i]);
        });

      clearTimeout(q6timeOut);
      //clearTimeout(q6timeOut2);
      stop = undefined;
    }

  /* ** Function for getting data from the service ** */
  $ctrl.getService = function () {
    jwtQuestionDiscover6Service.getData(CONST_OBJ)
      .then(function (response) {
        $ctrl.initData(response);
      }, function (error) {
        $ctrl.showLoader();
      });
  };
  /* ** Call function to fetch from service ** */


  clearInterval(interval1);
  interval1 = setInterval(function () {
    $ctrl.getService();
  }, apiTimer);

  /* ** Function for setting data to local scope ** */
  $ctrl.initData = function (response) {
//    if (!$scope.expanded) {
//      $scope.showQuestion = false;
//    } else {
//      $scope.showQuestion = true;
//    }
    if (response.data.dateFrom !="" && response.data.dateFrom != undefined) {
        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
      }
    $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US', { timeZone: 'UTC' });
    var dataArray = response.data.value;
    $scope.data = dataArray;
    if (!$ctrl.chartDrawn) {
      var drawChart = function () {
        isWorkspaceElement($scope.data);
      }
      //q6timeOut = setTimeout(drawChart, 200);
      drawChart();
    }
    $ctrl.data = $scope.data;
  };

  $scope.$watch('data', function () {
    if ($ctrl.chartDrawn) {
      $ctrl.startAnimateTrends();
    }
  });

  function isWorkspaceElement(data) {
    if ($ctrl.isWorkspace == true) {
      $scope.showQuestion = true;
      $('.masonry-brick').addClass('big-tile popped-window');
      //$scope.expanded = true;
      $ctrl.viewModel.expanded = true;
      var newWindWid = jwtUtil.getViewport().width;
      var newWindHgt = jwtUtil.getViewport().height;
      var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
      if (poppedContWidth < 650) {
        poppedContWidth = 680;
      }
      $('.masonry-brick').css('width', poppedContWidth + 'px');
      newWindWid = Math.round(parseInt(newWindWid) * 0.70);
      newWindHgt = Math.round(parseInt(newWindHgt) * 0.75);
      $ctrl.drawChart(newWindWid, newWindHgt, data);
    } else {
      var width = $('.chart-cont-q6').width();
      $ctrl.drawChart(width, 365, data);
    }
  }

  /* ** Listen to resize event ** */
  $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function (e, args) {
    $ctrl.initRedrawChart(args.resized);
  });

  /* ** Listner for expanding the tile ** */
  $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function (ev, data) {
    if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1] && $ctrl.data!=undefined) {
      $ctrl.showLoader();
      clearTimeout(q6timeOut2);
      $ctrl.viewModel.expanded = !$ctrl.viewModel.expanded;

      q6timeOut2 = setTimeout(function () {
          //$scope.showQuestion = !$scope.showQuestion;
          //$ctrl.expanded = !$ctrl.expanded;
          //$scope.expanded = !$scope.expanded ;
          $scope.expandTile(data.event, data.elem);
      }, 300);
    }
  });

  /* ** Show loader ** */
  $ctrl.showLoader = function () {
    var elem = $('.chart-cont-q6');
    elem.empty();
    elem.html('<div class="loader"></div>');
  }

  /* ** Init Re rendering the chart ** */
  $ctrl.initRedrawChart = function (resize, elem) {
    var targetElement, chartCont;
    if (resize && $ctrl.data !== undefined) {
      $ctrl.showLoader();
      var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
      $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
    }else{
      $ctrl.showLoader();
    }
  }

  /* ** Class for Grid layout ** */
  $scope.masonryelementclass = "masonry-brick";

  /* ** Function for expanding the card ** */
  $scope.expandTile = function (e, elem) {
    e.stopPropagation();
    e.preventDefault();
    $ctrl.stopAnimateTrends();

    var targetElement = $(e.target).parents('.masonry-brick');
//    $scope.showQuestion = !$scope.showQuestion;

    targetElement.siblings().removeClass('big-tile');
//    if (targetElement.hasClass('big-tile')) {
//      $scope.expanded = false;
//    } else {
//      $scope.expanded = true;
//    }
    targetElement.toggleClass('big-tile');
    var nWidth = Math.round(targetElement.innerWidth());
    var nHeight = Math.round(targetElement.innerHeight());
    if (!$ctrl.viewModel.expanded) {
      nHeight = (nHeight > 360) ? 360 : nHeight;
    }

    $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

  };

  /* ** Re rendering the chart on resize of the card ** */
  $ctrl.redrawChart = function (w, h, qservice) {
    var elem = $('.chart-cont-q6');
    elem.empty();

    clearInterval(interval);
    interval = setInterval(function () {
      $ctrl.getService();
    }, apiTimer);

    if ($ctrl.isWorkspace) {
      $scope.showQuestion = $scope.showQuestion;
    }

    $ctrl.drawChart(w, h, qservice);
  },

    /* --------- Rendering the chart  -------------------*|
    |- Params : Width, Height and data from the service --|
    |-----------------------------------------------------*/
    $ctrl.drawChart = function (w, h, data) {

      // Set the dimensions of the canvas / graph
      var margin = { top: 15, right: 15, bottom: 15, left: 15 },
        width = w - margin.left - margin.right,
        height = h - margin.top - margin.bottom,
        color = $scope.question.colors;

      if (!$ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width;
      } else if ($ctrl.viewModel.expanded && $ctrl.chartDrawn) {
        width = width;
        height = height;
      }

      // Create chart container block
      var chartContDiv = angular.element('<div/>');
      var matrixBlock = 5;
      var matrixRows = 3;
      var fontSize = 20;
      var textNodeHeight = 30;

      chartContDiv.attr("class", "trends-chart-cont");

      var trendsData;
      if (!$ctrl.viewModel.expanded) {
        trendsData = $scope.data.slice(0, 9);
        matrixBlock = 3;
        fontSize = 17;
        textNodeHeight = 24;
      } else {
        trendsData = $scope.data.slice(0, 15);
      }

      // Looping over the trends array
      angular.forEach(trendsData, function (node, index) {
        var qnt = Math.floor((Math.random() * colorBlocks - 1) + 1);
        var trendContW = angular.element(".chart-cont-q6").width();
        var trendContH = angular.element(".chart-cont-q6").height();
        var trendsDiv = angular.element('<div/>');
        var textNode = angular.element('<span/>');

        // Creating the text node
        textNode.attr("class", "trend-node-text");
        textNode.css({
          'min-height': textNodeHeight + 'px',
          'margin-top': '30px'
        });
        textNode.text(node.value);
        textNode.attr({ 'data-placement': 'top', 'title': node.value });

        var topV; var leftV;
        if (index >= 0 && index < matrixBlock) {
          topV = 0;
        } else if (index % matrixBlock >= 0 && index % matrixBlock < matrixBlock && Math.floor(index / matrixBlock) == 1) {
          topV = 0 + Math.floor(trendContH / matrixRows);
        } else {
          topV = 0 + (2 * Math.floor(trendContH / matrixRows));
        }
        if (index == 0 || index % matrixBlock == 0) {
          leftV = 0;
        } else {
          leftV = (index % matrixBlock) * Math.floor(trendContW / matrixBlock);
        }
        // Creating the trend blocks
        trendsDiv.attr("class", "trend-node");
        trendsDiv.append(textNode);
        trendsDiv.css({
          backgroundColor: $scope.question.colors.blockColor[qnt],
          'color': $scope.question.colors.fontColor[qnt],
          'height': Math.floor(trendContH / matrixRows) + 'px',
          width: Math.floor(trendContW / matrixBlock) + 'px',
          fontSize: fontSize + 'px',
          position: 'absolute',
          'top': topV + 'px',
          'left': leftV + 'px'
        });

        // Adding the blocks into the trends container wrapper
        chartContDiv.append(trendsDiv);

        var tVal = node.value;
        textNode.css('display', 'inline').typed({
          strings: [tVal],
          typeSpeed: animTypeSpeed,
          callback: function () {
            textNode.text(tVal);
            textNode.css({ 'display': 'inline-block', 'width': Math.floor((trendContW / matrixBlock) - 30) + 'px' });
          }
        });

      });

      //angular.element('.trend-node-text').tooltip();

      // Adding the trends container wrapper to the chart container
      angular.element(".chart-cont-q6").append(chartContDiv);

      $ctrl.chartDrawn = true;
    };

  /* --------- Starting the animation -----------------*|
  |--- initData function is called at interval  --------|
  |----------------------------------------------------*/
  $ctrl.startAnimateTrends = function () {

    $ctrl.initAnimateTrends();

    var trendnodes = angular.element('.trend-node');

    var trendsData;
    var blockWidth = '';
    var blockHeight = '';
    var txtNHeight = 30;

    var matrixBlock = 5;
    if (!$ctrl.viewModel.expanded) {
      matrixBlock = 3;
      txtNHeight = 24;
    }

    trendsData = $scope.data.slice(0, trendnodes.length);

      textNodeAnimTimer.forEach(function(v,i){
            clearTimeout(textNodeAnimTimer[i]);
      });
      textNodeAnimTimer = [];

    angular.forEach(trendsData, function (node, index) {
      var nodeElement = $(trendnodes[index]);
      var qnt = Math.floor((Math.random() * colorBlocks - 1) + 1);
      var nBcolor = $scope.question.colors.blockColor[qnt];
      var nFcolor = $scope.question.colors.fontColor[qnt];
      var tnHeight = nodeElement.find('.trend-node-text').height();

      do {
        qnt = Math.floor((Math.random() * colorBlocks - 1) + 1);
        nBcolor = $scope.question.colors.blockColor[qnt];
      }
      while (nodeElement.css('background-color') == nBcolor);

      var matchItem = false;
      if (blockWidth == '' && blockHeight == '') {
        blockWidth = nodeElement.width();
        blockHeight = nodeElement.height()
      }

      //clearTimeout(textNodeAnimTimer);
      textNodeAnimTimer[index] = setTimeout(function () {
        if (index == qnt && index % matrixBlock != 0) {
          nodeElement.css('width', 0);
          nodeElement.animate({
            width: blockWidth
          },
            {
              duration: Math.floor((Math.random() * animTimer - 1) + 1),
              start: animateStart,
              complete: animateComplete
            });
        } else {
          nodeElement.css('height', 0);
          nodeElement.animate({
            height: blockHeight
          },
            {
              duration: Math.floor((Math.random() * animTimer - 1) + 1),
              start: animateStart,
              complete: animateComplete
            });
        }
      }, animTimer / trendsData.length);

      var animateStart = function () {
        nodeElement.find('.trend-node-text').text('');
        nodeElement.css('background-color', nBcolor);
        nodeElement.find('.trend-node-text').css({ 'white-space': 'nowrap', 'border-right-color': nFcolor, 'display': 'inline' });
      };

      var animateComplete = function () {
        nodeElement.css('width', blockWidth + 'px');
        nodeElement.css('height', blockHeight + 'px');
        nodeElement.css('color', $scope.question.colors.fontColor[qnt]);
        nodeElement.find('.trend-node-text').text(node.value);
        nodeElement.find('.trend-node-text').css({ 'white-space': 'inherit', 'margin-top': (Math.floor(blockHeight - tnHeight) / 2 + 'px') });

        var tVal = nodeElement.find('.trend-node-text').text();
        nodeElement.find('.trend-node-text').css('display', 'inline').typed({
          strings: [tVal],
          typeSpeed: animTypeSpeed,
          callback: function () {
            nodeElement.find('.trend-node-text').text(tVal);
            nodeElement.find('.trend-node-text').css({ 'display': 'inline-block' });
          }
        });
      };
    });
  };

  /** Check if animation if already started */
  $ctrl.initAnimateTrends = function () {
    if (stop != '') {
      return false;
    } else {
      stop = 'started';
    }
  };

  /** Function for clearing the interval */
  $ctrl.stopAnimateTrends = function () {
    if (stop != '') {
      clearInterval(interval);
      clearInterval(interval1);
        textNodeAnimTimer.forEach(function(v,i){
            clearTimeout(textNodeAnimTimer[i]);
        });

      stop = undefined;
    }
  };

  /** Destroying the interval too */
//  $scope.$on('$destroy', function () {
//    $ctrl.stopAnimateTrends();
//  });

}
