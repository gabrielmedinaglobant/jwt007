/* -------------- Question 6 Constant  ---------------------*|
|--- This is a  constant file for the Q6 -> Discover   ------|
|-----------------------------------------------------------*/
//(function () {
//  'use strict';
//
//  angular
//    .module('app.constants', [])
//    .constant('jwtQ6Constant', {
//      'Q6': {
//        API: '/questions/people-talking-about-overall',
//        PREFIX: true,
//        API_TIMER: 900000,
//        ANIM_TIMER: 3000,
//        ANIM_TYPESPEED: 45,
//        STATIC_INFO: {
//          'title': 'Overall Conversation',
//          'qText': 'What are people talking about overall?',
//          'colors': {
//            'blockColor': ['#5b37a3', '#41a899', '#4892cc', '#ffffff'],
//            'fontColor': ['#ffffff', '#ffffff', '#ffffff', '#673ab7']
//          }
//        }
//      }
//    });
//})();

(function() {
'use strict';

  angular
    .module('app.components')
    .constant('jwtQ6Constant', {
      'Q6': {
        API: '/questions/people-talking-about-overall',
        PREFIX: true,
        API_TIMER: 900000,
        ANIM_TIMER: 3000,
        ANIM_TYPESPEED: 45,
        STATIC_INFO: {
          'title': 'Search Trends',
          'qText': 'What search topics are trending now?',
          'colors': {
            'blockColor': ['#7f6dc8', '#82c2b2', '#eccb30', '#d7803c'],
            'fontColor': ['#ffffff', '#385d54', '#776820', '#74411a']
          },
          'description' : 'The top searches that people are googling in all regions.'
        }
      }
    });
})();
