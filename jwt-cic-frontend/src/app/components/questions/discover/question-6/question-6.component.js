(function () {
  'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionDiscover6', {
      templateUrl: 'app/components/questions/discover/question-6/discover.question-6.html',
      controller: questionDiscover6Controller,
      bindings: {
        data: '<',
        isWorkspace: '<',
        showWorkspaceSelect: '<',
        jwtChartId: '<'
      }
    });

})();
