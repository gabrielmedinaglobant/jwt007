(function() {
'use strict';

  angular
    .module('app.components')
    .component('jwtQuestionDiscoverHome', {
      templateUrl: 'app/components/questions/discover/home/discover.home.html',
      controller: questionDiscoverHomeController,
      bindings: {
        data: '<',
        isWorkspace:'<'
      }
    });
  
})();
