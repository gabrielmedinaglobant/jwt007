(function() {
    'use strict';

    angular
        .module('app.components')
        .constant('jwtQuestionDiscover1Constant', {
            QAPI: {
                API: '/questions/people-looked-competitors',
                SEARCHBRANDS: '/brands?name=',
                PREFIX: true
            },
            INFO: {
                'title': 'Search Interest',
                'qText': 'How popular is my brand versus competitors?',
                'xLabel': 'Average',
                'barColors': ['#82c2b2', '#7f6dc8', '#d7803c', '#eccb30', '#59a95b'],
                'errorMessage': 'No data found. Please retry!',
                'description' : 'Total volume of Google searches of a term that has been queried over a specific period of time.'
            }

        });
})();
