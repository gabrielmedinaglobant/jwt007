/* --------- Controller for Question 1 --------------*|
 |---  Q1: How much have people looked for me in comparison to competitors? --------|
 |----------------------------------------------------*/
questionDiscover1Controller.$inject = ['$scope', '$compile', 'sessionService', 'jwtAppConstant', '$rootScope', '$element', '$log', 'jwtQuestionDiscover1Service'];

function questionDiscover1Controller($scope, $compile, sessionService, jwtAppConstant, $rootScope, $element, $log, jwtQuestionDiscover1Service) {
    var $ctrl = this;
    var searchBrandText;
    $scope.question = jwtQuestionDiscover1Service.getConstant('INFO');
    $ctrl.viewModel = { expanded: false };
    //$scope.expanded = false;
    $ctrl.dateRanges = { dateFrom: false, dateTo: null, lastUpdatedDate: false };
    $ctrl.errorMessage = '';
    $scope.selectedBrandNames = [];
    $scope.enteredBrandNames = [];
    $scope.addComparisonClicked = 0;
    $scope.allBrands = [];
    $scope.brandTextValue0 = '';
    $scope.brandTextValue1 = '';
    $scope.brandTextValue2 = '';
    $scope.brandTextValue3 = '';
    $scope.brandTextValue4 = '';
    $scope.color = jwtQuestionDiscover1Service.getConstant('INFO').barColors;
    $scope.showLegends = true;

    var q1timeOut, q1timeOut2;


    $scope.selectedBrandNames = (sessionService.getSessionData("selectedBrandNames") == null) ? [] : sessionService.getSessionData("selectedBrandNames").split(",");

    $ctrl.$onInit = function() {
        $ctrl.chartDrawn = false;
        if ($ctrl.isWorkspace) {
            //first time workspace launched
            if (sessionService.getTempStorage("workspaceSelectedBrands") == null) {
                $scope.workspaceIdentifier = Math.floor(1000 + Math.random() * 9000);
                sessionService.setTempStorage("workspaceSelectedBrands", $scope.selectedBrandNames);
            } else {
                if (sessionService.getTempStorage("workspaceSelectedBrands") === '') {
                    $ctrl.data = undefined;
                    $ctrl.expanded = true;
                    $scope.selectedBrandNames = [];
                    sessionService.setTempStorage("workspaceSelectedBrands", [])
                    for (var i = 0; i < 5; i++) {
                        $scope["brandTextValue" + i] = '';
                    }
                    isWorkspaceElement($ctrl.data);

                } else {
                    $scope.selectedBrandNames = sessionService.getTempStorage("workspaceSelectedBrands");
                    if ($scope.selectedBrandNames.indexOf(',') > -1) {
                        var brandNamesArray = $scope.selectedBrandNames.split(",");
                        $scope.selectedBrandNames = brandNamesArray;
                        $scope.selectedBrandNames.forEach(function(value, key) {
                            $scope["brandTextValue" + key] = value;
                        })

                    } else {
                        $scope["brandTextValue0"] = $scope.selectedBrandNames;
                        for (var i = 1; i < 5; i++) {
                            $scope["brandTextValue" + i] = '';
                        }
                    }

                }
            }
        }

    }

    $ctrl.$onDestroy = function() {
        clearTimeout(q1timeOut);
        clearTimeout(q1timeOut2);
    }

    $scope.getGoogleTrendsData = function() {
        var brandNames = $scope.selectedBrandNames;
        var selectedBrand = "?";

        brandNames.forEach(function(brandValue, key) {

            var value = encodeURIComponent(brandValue).replace(/[~'()*]/g, function(c) {
                if (c == '*') {
                    return ' ';
                } else if (c == '()' || c == '(' || c == ')' || c == '~') {
                    return ' ';
                } else {
                    return '';
                }
            });
            selectedBrand = (selectedBrand === "?") ? (selectedBrand + "brand=" + value) : (selectedBrand + "&brand=" + value);
        })


        jwtQuestionDiscover1Service.getData(jwtQuestionDiscover1Service.getConstant('QAPI').API + selectedBrand)
            .then(
                function(response) {

                    $ctrl.data = response.data.value;
                    if (response.data.dateFrom != "" && response.data.dateFrom != undefined) {
                        $ctrl.dateRanges.dateFrom = (new Date(response.data.dateFrom * 1000)).toLocaleDateString('en-US');
                        $ctrl.dateRanges.dateTo = (new Date(response.data.dateTo * 1000)).toLocaleDateString('en-US');
                    }
                    $ctrl.dateRanges.lastUpdatedDate = (new Date(response.data.lastUpdatedDate * 1000)).toLocaleDateString('en-US');

                    //save when compare hit and launched workspace
                    if ($ctrl.isWorkspace) {
                        sessionService.setTempStorage("workspaceSelectedBrands", $scope.selectedBrandNames);

                    } else {
                        sessionService.setSessionData("selectedBrandNames", brandNames);

                    }

                    // manipulating data as in required format
                    $ctrl.data.splineChart.lines.forEach(function(data) {
                        data.values.forEach(function(value) {
                            value["date"] = value.value[0]
                            value[data.key] = value.value[1];
                            value["brand"] = data.key;
                            delete value.value;
                        });
                        delete data.key;
                        data = data.values;
                    });

                    var splineChartData = [];
                    var resultObject = [];
                    for (var i = 0; i < $ctrl.data.splineChart.lines[0].values.length; i++) {
                        var d = [];

                        $ctrl.data.splineChart.lines.forEach(function(data, index) {
                            if (data.values[i] !== undefined) {
                                d.push(data.values[i])
                            }
                        })
                        if (d.length > 0) {
                            splineChartData.push(d.reduce(function(result, currentObject) {
                                for (var key in currentObject) {
                                    if (currentObject.hasOwnProperty(key)) {
                                        result[key] = currentObject[key];
                                    }
                                }
                                return result;
                            }, {}));
                        }
                    }
                    // main data needed for multi line chart
                    $ctrl.mainSplineChartData = splineChartData;

                    $scope.data = $ctrl.data;
                    $ctrl.viewModel.expanded = $ctrl.showExpanded;

                    if ($ctrl.chartDrawn) {
                        var targetElement = $('.masonry-brick');

                        if ($ctrl.isWorkspace == true) {
                            $ctrl.viewModel.expanded = true;
                            isWorkspaceElement($ctrl.data);
                        } else {
                            var nWidth = Math.round(targetElement.innerWidth());
                            var nHeight = Math.round(targetElement.innerHeight());
                            if (!$ctrl.viewModel.expanded) {
                                nHeight = (nHeight > 360) ? 360 : nHeight;
                            }
                            $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);
                        }
                    } else {
                        isWorkspaceElement($ctrl.data);
                    }

                    //

                },
                function(error) {
                    if (error.data.status === 500 && error.data.message == 'Invalid data from source') {

                        // $logProvider.debugEnabled(false);
                        $ctrl.data = undefined;
                        sessionService.unsetSessionData("selectedBrandNames")
                        $scope.showLegends = false;
                        $ctrl.errorMessage = jwtQuestionDiscover1Service.getConstant('INFO').errorMessage;
                        isWorkspaceElement($ctrl.data)
                    }
                })
    }

    if ($scope.selectedBrandNames.length > 0) {

        $scope.addComparisonClicked = $scope.selectedBrandNames.length;
        $scope.selectedBrandNames.forEach(function(value, key) {
            $scope["brandTextValue" + key] = value;
        })
        $scope.getGoogleTrendsData()
    } else {

        q1timeOut2 = setTimeout(function() {
            $ctrl.data = undefined;
            isWorkspaceElement($ctrl.data);
        }, 3000);
    }

    //compare the brands
    $scope.compare = function() {
        if (d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare").classed("disabled")) {
            return false;
        } else {
            $ctrl.errorMessage = '';
            var searchBrands;
            $scope.selectedBrandNames = [];
            searchBrands = d3.selectAll(".buttonDiv .textBoxesContainer .inputContainer input");


            searchBrands.each(function() {
                if ($(this).val() !== '') {
                    //contains every value entered in textbox
                    $scope.selectedBrandNames.push($(this).val())
                }
            })

            //skip empty boxes if any and assign text to sequential order
            for (var i = 0; i < 5; i++) {
                $scope["brandTextValue" + i] = $scope.selectedBrandNames[i];

            }

            $scope.addComparisonClicked = $scope.selectedBrandNames.length;

            if ($scope.selectedBrandNames.length === 0) {
                $ctrl.data = undefined;
                sessionService.unsetSessionData("selectedBrandNames")
                sessionService.unsetSessionData("workspaceSelectedBrands")
                isWorkspaceElement($ctrl.data)
            } else {

                $scope.getGoogleTrendsData();
            }
        }
    };

    $scope.textChanged = function() {
        d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare").attr("disabled", null).classed("disabled", false)
            // d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", null)
    }

    $scope.removeBrand = function(e) {
        var removedIndex = $scope.selectedBrandNames.indexOf($scope["brandTextValue" + e]);
        $scope.selectedBrandNames.splice(removedIndex, 1);

        $scope["brandTextValue" + e] = '';

        if ($scope.brandTextValue0 != '' || $scope.brandTextValue1 != '' || $scope.brandTextValue2 != '' || $scope.brandTextValue3 != '' || $scope.brandTextValue4 != '') {
            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                .attr("disabled", null)
                .classed("disabled", false)
            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", null)
        }

        if (($scope.brandTextValue0 == '' || $scope.brandTextValue0 == undefined) &&
            ($scope.brandTextValue1 == '' || $scope.brandTextValue1 == undefined) &&
            ($scope.brandTextValue2 == '' || $scope.brandTextValue2 == undefined) &&
            ($scope.brandTextValue3 == '' || $scope.brandTextValue3 == undefined) &&
            ($scope.brandTextValue4 == '' || $scope.brandTextValue4 == undefined)) {
            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                .attr("disabled", true)
                .classed("disabled", true)
        }

        if ($scope.brandTextValue0 != '' && $scope.brandTextValue1 != '' && $scope.brandTextValue2 != '' && $scope.brandTextValue3 != '' && $scope.brandTextValue4 != '') {
            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                .attr("disabled", true)

            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", true)
        }

        for (var i = 0; i < 5; i++) {
            if ($scope["brandTextValue" + i] == '') {
                var nextIndex = i + 1;
                $scope["brandTextValue" + i] = $scope["brandTextValue" + nextIndex];
                $scope["brandTextValue" + nextIndex] = '';
            }
        }

        $('.textBoxesContainer .inputContainer input:visible:last').css("display", "none")
        $('.textBoxesContainer .inputContainer i:visible:last').css("display", "none")
        $('.textBoxesContainer .inputContainer span').css("display", "none")
            //when a single brand is removed, unless compare clicked again the remaining brands shouldnt be in session and legends shouldnt be associated with it in any view.
        if (!$ctrl.isWorkspace)
            sessionService.unsetSessionData("selectedBrandNames");

        $scope.addComparisonClicked--;

        if ($scope.addComparisonClicked != 5) {
            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", null)
        }

        if ($scope.addComparisonClicked === 0) {
            $ctrl.data = undefined;
            $ctrl.errorMessage = '';
            $scope.selectedBrandNames = [];
            if ($ctrl.isWorkspace)
                sessionService.setTempStorage("workspaceSelectedBrands", [])
            isWorkspaceElement($ctrl.data);
        }

        if ($ctrl.isWorkspace) {
            sessionService.setTempStorage("workspaceSelectedBrands", $scope.selectedBrandNames)
        }

    }

    // add brand input box
    $scope.addBrands = function() {
        $scope.addComparisonClicked = $(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer input:visible").length;

        d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
            .attr("disabled", null)

        if ($scope.addComparisonClicked < 5) {

            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer .brandText" + $scope.addComparisonClicked)
                .attr("placeholder", "Enter Brand")
                .style("display", "block")

            d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv i.icon-jwt-cancel.brandText" + $scope.addComparisonClicked)
                .attr("placeholder", "Enter Brand")
                .style("display", "block")

            $scope.addComparisonClicked++;
            if ($scope.addComparisonClicked == 5) {
                d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", true)
                if (($scope.brandTextValue0 == '' || $scope.brandTextValue0 == undefined) &&
                    ($scope.brandTextValue1 == '' || $scope.brandTextValue1 == undefined) &&
                    ($scope.brandTextValue2 == '' || $scope.brandTextValue2 == undefined) &&
                    ($scope.brandTextValue3 == '' || $scope.brandTextValue3 == undefined) &&
                    ($scope.brandTextValue4 == '' || $scope.brandTextValue4 == undefined)) {
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                        .attr("disabled", true)
                        .classed("disabled", true)
                }
            }

        }

    }



    /* ** Class for Grid layout ** */
    $scope.masonryelementclass = "masonry-brick";

    function isWorkspaceElement(data) {
        if ($ctrl.isWorkspace == true && data != undefined) {

            $scope.showQuestion = true;
            $('.masonry-brick').addClass('big-tile popped-window');
            $ctrl.viewModel.expanded = true;
            var newWindWid = jwtUtil.getViewport().width;
            var newWindHgt = jwtUtil.getViewport().height;
            var poppedContWidth = Math.round(parseInt(newWindWid) * 0.8);
            if (poppedContWidth < 650) {
              poppedContWidth = 680;
            }
            $('.masonry-brick').css('width', poppedContWidth + 'px');
            newWindWid = Math.round(parseInt(newWindWid) * 0.80);
            newWindHgt = Math.round(parseInt(newWindHgt) * 0.65);
            $ctrl.drawChart(newWindWid, newWindHgt, data);
        } else {
            var width = $('.chart-cont-q1').width();
            $ctrl.drawChart(width, 360, data);
        }
    }

    /* ** Listen to resize event ** */
    $rootScope.$on(jwtAppConstant.EVENT_NAME.CHART.RESIZED, function(e, args) {
        $ctrl.initRedrawChart(args.resized);
    });

    /* ** Listner for expanding the tile ** */
    $scope.$on(jwtAppConstant.EVENT_NAME.CHART.MAXIMIZED, function(ev, data) {
        if ($ctrl.jwtChartId == data.elem.parents('.grid-item-content').attr('id').split('chartId-')[1]) {

            $scope.showQuestion = !$scope.showQuestion;
            q1timeOut = setTimeout(function() {
                $scope.expandTile(data.event, data.elem);
            }, 300);
        }
    });

    /* ** Show loader ** */
    $ctrl.showLoader = function() {
        var elem = $('.chart-cont-q1');
        elem.empty();
        elem.html('<div class="loader"></div>');
    }

    /* ** Init Re rendering the chart ** */
    $ctrl.initRedrawChart = function(resize, elem) {
        var targetElement, chartCont;
        if (resize) {
            $ctrl.showLoader();
            var newContAttrObj = jwtUtil.getNewContAttrs($ctrl, $ctrl.viewModel.expanded);
            $ctrl.redrawChart(newContAttrObj.nwidth, newContAttrObj.nheight, $ctrl.data);
        }
    }

    /* ** Function for expanding the card ** */
    $scope.expandTile = function(e, elem) {
        e.stopPropagation();
        e.preventDefault();

        var targetElement = $(elem).parents('.masonry-brick');

        targetElement.siblings().removeClass('big-tile');
        if (targetElement.hasClass('big-tile')) {
            $ctrl.viewModel.expanded = false;
        } else {
            $ctrl.viewModel.expanded = true;
        }


        $ctrl.showExpanded = $ctrl.viewModel.expanded;
        targetElement.toggleClass('big-tile');

        var nWidth = Math.round(targetElement.innerWidth());
        var nHeight = Math.round(targetElement.innerHeight());
        if (!$ctrl.viewModel.expanded) {
            nHeight = (nHeight > 360) ? 360 : nHeight;
        }
        $ctrl.redrawChart(nWidth, nHeight, $ctrl.data);

    };

    /* ** Re rendering the chart on resize of the card ** */
    this.redrawChart = function(w, h, qservice) {
            var elem = $('.chart-cont-q1');
            elem.empty();
            if ($ctrl.isWorkspace) {
                $scope.showQuestion = !$scope.showQuestion;
            }
            this.drawChart(w - 30, h, qservice);
        },

        /* --------- Rendering the chart  -------------------*|
        |- Params : Width, Height and data from the service --|
        |-----------------------------------------------------*/

        this.drawChart = function(w, h, data) {


            //when no data present or no data selected
            if ($scope.selectedBrandNames.length === 0) {
                $ctrl.data = undefined;
                data = undefined;
                $ctrl.dateRanges.lastUpdatedDate = false;
            }

            if ($ctrl.isWorkspace && typeof $scope.selectedBrandNames === 'string') {
                var brandNamesArray = $scope.selectedBrandNames.split(",");
                $scope.selectedBrandNames = brandNamesArray;
            }

            d3.select(".chart-cont-q1 .toolTip").remove();
            d3.select(".chart-cont-q1 .leftDiv").remove();
            d3.select(".chart-cont-q1 .rightTopDiv").remove();
            d3.select(".chart-cont-q1 .rightBottomDiv").remove();

            if ($scope.brandTextValue0 == '' && $scope.brandTextValue1 == '' && $scope.brandTextValue2 == '' && $scope.brandTextValue3 == '' && $scope.brandTextValue4 == '')
                $scope.addComparisonClicked = 0;

            var leftDiv = d3.select(".chart-cont-q1").append("div").
            attr("class", "leftDiv");

            var rightTopDiv = d3.select(".chart-cont-q1").append("div").
            attr("class", "rightTopDiv");

            var buttonDiv = d3.select(".chart-cont-q1 .rightTopDiv").append("div")
                .attr("class", "buttonDiv")

            var textBoxesContainer = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv").append("div")
                .attr("class", "textBoxesContainer")
                .attr("ng-form", "brandInputs");

            if ($ctrl.viewModel.expanded) {
                textBoxesContainer
                    .style("height", "auto")
                    .style("clear", "both")
                    .style("display", "table")
                    .style("margin-bottom", "10px")
            } else {
                textBoxesContainer
                    .style("height", "210px")

            }

            //re-assign all the brands in a sequential manner in which they are added

            $scope.allBrands = [];

            for (var i = 0; i < 5; i++) {
                $scope.allBrands.push($scope["brandTextValue" + i])
            }
            for (var i = 0; i < 5; i++) {
                $scope["brandTextValue" + i] = ''
            }
            $scope.allBrands = $scope.allBrands.filter(function(n) { return n != undefined && n != '' });

            $scope.allBrands.forEach(function(value, key) {
                $scope["brandTextValue" + key] = value;
            })

            $scope.addComparisonClicked = $scope.allBrands.length;


            // re-assigning code completed

            var brandDataPresent = (sessionService.getSessionData("selectedBrandNames") == null) ? [] : sessionService.getSessionData("selectedBrandNames").split(",");
            if ($ctrl.isWorkspace) {
                brandDataPresent = (sessionService.getTempStorage("workspaceSelectedBrands") == null) ? [] : sessionService.getTempStorage("workspaceSelectedBrands").split(",");
            }
            for (var i = 0; i < 5; i++) {

                var inputContainer, inputBox, removeInputBox, legend;

                inputContainer = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer").append("div")
                    .attr("class", "inputContainer brandText" + i)

                inputBox = inputContainer.append("input")
                    .attr("type", "text")
                    .attr("placeholder", "Enter Brand")
                    .style("color", "#5e829f")
                    .style("font-size", "14px")
                    .attr("ng-model", "brandTextValue" + i)
                    .attr("ng-keyup", "textChanged()")
                    .style("display", "none")
                    .style("text-overflow", "ellipsis")
                    // .style("padding-left", "20px")
                    .attr("class", "brandText" + i)

                removeInputBox = inputContainer.append("i")
                    .attr("ng-click", "removeBrand(" + i + ")")
                    .style("display", "none")
                    .attr("class", "icon-jwt-cancel brandText" + i)

                legend = inputContainer.append("span")
                    .attr("class", "brand-legend")
                    .style("background-color", $scope.color[i])


                if ($scope["brandTextValue" + i] !== '') {
                    if ($scope["brandTextValue" + i] !== undefined) {
                        inputBox.style("display", "block")
                        removeInputBox.style("display", "block")
                        if (brandDataPresent.indexOf($scope["brandTextValue" + i]) > -1)
                            legend.style("display", "block")
                    }
                }

            }





            if ($scope.selectedBrandNames.length > 0) {
                if ($ctrl.isWorkspace && typeof $scope.selectedBrandNames === 'string') {
                    var brandNamesArray = $scope.selectedBrandNames.split(",");
                    $scope.selectedBrandNames = brandNamesArray;
                }

                $scope.selectedBrandNames.forEach(function(value, key) {
                    if ($scope["brandTextValue" + key] !== '' && $scope["brandTextValue" + key] !== undefined) {
                        d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer .brandText" + key)
                            .attr("placeholder", "Enter Brand")
                            .attr("title", value)
                            .style("display", "block")

                        d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer .jwt-icon-cancel .brandText" + key)
                            .style("display", "block")
                        d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer .brandText" + key + " span")
                            .style("display", "block")


                    }

                });
            }


            if ($ctrl.data === undefined) {
                d3.selectAll(".chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer span")
                    .style("display", "none")

            }


            if (!$ctrl.viewModel.expanded) {
                if (data === undefined) {
                    d3.select(".chart-cont-q1 .leftDiv").append("div").
                    attr("class", "barChart barChartAltText");
                } else {
                    d3.select(".chart-cont-q1 .leftDiv").append("div").
                    attr("class", "barChart")
                        .style("height", null)
                        .style("margin-top", "20px")

                }

                d3.select(".chart-cont-q1 .leftDiv .barChart")
                    .text("Please,enter search terms and select Compare")
                    .style("color", "#5e829f")
                    .style("text-align", "center")

                var addComparison = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv").append("button")
                    .attr("class", "addComparison")
                    .attr("ng-click", "addBrands()")
                    .text("+ Add comparison");

                var compare = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv").append("button")
                    .attr("class", "compare disabled")
                    .attr("disabled", true)
                    .attr("ng-click", "compare()")
                    .text("Compare")
                    .style("color", "#5e829f")

                if ($scope.brandTextValue0 != '' || $scope.brandTextValue1 != '' || $scope.brandTextValue2 != '' || $scope.brandTextValue3 != '' || $scope.brandTextValue4 != '') {
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                        .attr("disabled", null)
                        .classed("disabled", false)
                }

                if ($('.chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer span:visible').length === 5) {
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                        .attr("disabled", true)
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", true)
                }

                this.drawBarChart(w, h, data);
            } else {


                leftDiv.select(".barChart").remove();

                leftDiv.append("div").
                attr("class", "summaryInfo")
                    .style("height", "90px");

                leftDiv.append("div")
                    .attr("class", "barChart")
                    .style("height", (h / 2) - 30 + "px")
                    .style("border", "0px")
                    .style("background-color", "#1d2e3a");

                if ($ctrl.viewModel.expanded) {
                    leftDiv.style("height", null)
                        // .style("margin-top", "20px")
                }

                var addComparison = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv").append("button")
                    .attr("class", "addComparison")
                    .attr("ng-click", "addBrands()")
                    .text("+ Add comparison")
                    .style("display", "block");

                var compare = d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv").append("button")
                    .attr("class", "compare disabled")
                    .attr("disabled", "true")
                    .attr("ng-click", "compare()")
                    .text("Compare")

                if ($scope.brandTextValue0 != '' || $scope.brandTextValue1 != '' || $scope.brandTextValue2 != '' || $scope.brandTextValue3 != '' || $scope.brandTextValue4 != '')
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                    .attr("disabled", null)
                    .classed("disabled", false)

                if ($('.chart-cont-q1 .rightTopDiv .buttonDiv .textBoxesContainer .inputContainer span:visible').length === 5) {
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .compare")
                        .attr("disabled", true)
                        .classed("disabled", true)
                    d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", true)
                }

                if ($ctrl.viewModel.expanded) {
                    buttonDiv
                    //.style("width", "100%")
                        .style("float", "left")
                        .style("margin-top", "5px")
                        .style("position", "relative")


                    addComparison
                        .style("float", "left")
                        //.style("margin-left", "50px")

                    compare
                        .style("float", "right")
                }


                var rightBottomDiv = d3.select(".chart-cont-q1").append("div").
                attr("class", "rightBottomDiv")

                rightBottomDiv
                    .style("float", "right")
                    .style("height", (h / 2) + "px");

                if (data === undefined) {
                    rightBottomDiv
                        .style("border", "2px solid #1d2e3a")
                        .style("margin-top", "27px")
                        .text("Please, enter search items and select Compare")
                        .style("color", "#5e829f")
                        .style("text-align", "center")
                        .style("padding", "11% 15%")
                }

                rightBottomDiv.append("div").
                attr("class", "splineChart");


                $ctrl.showSummaryInfo(w / 2, h / 2, data);
                $ctrl.drawBarChart(w / 2, h / 2, data);
                $ctrl.drawSplineChart(w / 2, h / 2, data);
            }


            if ($scope.addComparisonClicked == 5) {
                d3.select(".chart-cont-q1 .rightTopDiv .buttonDiv .addComparison").attr("disabled", true)
            }
            $ctrl.chartDrawn = true;
        };


    $ctrl.drawBarChart = function(w, h, data) {

        if (data !== undefined) {

            // set the dimensions and margins of the graph
            var margin = { top: 20, right: 20, bottom: 30, left: 40 },
                width = w - margin.left - margin.right,
                height = h - margin.top - (margin.bottom * 2);

            var color = d3.scaleOrdinal()
                .range($scope.question.barColors);

            $(".barChart").empty();

            // set the ranges
            var x = d3.scaleBand()
                .range([0, width])
                .padding(0.1);
            var y = d3.scaleLinear()
                .range([height, 0]);

            d3.select(".chart-cont-q1 .leftDiv .barChart").text("");

            // append the svg object to the body of the page
            // append a 'group' element to 'svg'
            // moves the 'group' element to the top left margin
            if (!$ctrl.viewModel.expanded) {
                var svg = d3.select(".chart-cont-q1 .leftDiv .barChart")
                    .style("border", "0px")
                    .append("svg")
                    .attr("class", "barChartSvg")
                    .attr("width", width - (2 * margin.left))
                    .attr("height", height + (2 * margin.top))
                    .append("g")
                    .attr("transform",
                        "translate(" + margin.bottom + ",10)");
            } else {
                d3.select(".chart-cont-q1 .leftDiv .barChart")
                    .style("border", "0px")

                .append("div")
                    .style("text-align", "center")
                    .append("text")
                    .attr("class", "barChartTitle")
                    .attr("text-anchor", "middle")
                    .text($scope.question.xLabel);

                var svg = d3.select(".chart-cont-q1 .leftDiv .barChart")
                    .style("background-color", "#1d2e3a")
                    .style("height", null)
                    .append("svg")
                    .attr("class", "barChartSvg")
                    .attr("width", width - (1.6 * margin.left))
                    .attr("height", height + margin.left + margin.top)
                    .append("g")
                    .attr("transform",
                        "translate(25,35)");
            }

            var barData = data.barChart.values;
            var color = d3.scaleOrdinal()
                .range($scope.question.barColors);

            // Scale the range of the data in the domains
            x.domain(barData.map(function(d) { return d.key; }))
                .range([0, width * 0.5]);

            y.domain([0, d3.max(barData, function(d) { return d.value; })]).nice();

            var tooltip = d3.select(".chart-cont-q1").append("div").attr("class", "toolTip");


            // append the rectangles for the bar chart
            svg.selectAll(".bar")
                .data(barData)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function(d) { return x(d.key); })
                .attr("width", x.bandwidth() * 0.8)
                .attr("y", function(d) { return y(d.value); })
                .attr("height", function(d) { return height - y(d.value); })
                .style("fill", function(d) {
                    return color(d.key);
                })
                .on("mousemove", function(d) {
                    tooltip
                        .style("left", d3.event.pageX - 100 + "px")
                        .style("top", d3.event.pageY - 200 + "px")
                        .style("display", "inline-block")
                        .style("color", "#5e829f")
                        .style("text-align", "center")
                        .style("height", "auto")
                        .style("width", "70px")
                        .html((d.value));
                })
                .on("mouseout", function(d) { tooltip.style("display", "none"); });

            // add the x Axis
            svg.append("g")
                .attr("class", "axis x-axis")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x)
                    .tickSize(0)
                    .tickFormat("")
                )
                .selectAll("text")
                .attr("fill", "#5e829f");

            if (!$ctrl.viewModel.expanded) {
                d3.select(".x-axis").append("text")
                    .attr("class", "x-axis-label")
                    .attr("transform", "translate(" + ((width * 0.5) / 2) + "," + (margin.bottom - 5) + ")")
                    .text($scope.question.xLabel)
            }

            var yAxisval = d3.axisLeft(y).tickSizeOuter(0);
            if (jwtUtil.getViewport().width < 900) {
                yAxisval = d3.axisLeft(y).ticks(4).tickSizeOuter(0);
            }
            // add the y Axis
            svg.append("g")
                .attr("class", "axis")
                .call(yAxisval)
                .selectAll("text")
                .attr("fill", "#5e829f")


        }
        var button = angular.element(".buttonDiv");
        $compile(button)($scope);



    }

    $ctrl.drawSplineChart = function(w, h, data) {
        var color = $scope.question.barColors;
        if (data !== undefined && $ctrl.data !== undefined) {
            data = data.splineChart.lines;
            var margin = { top: 20, right: 80, bottom: 30, left: 50 },
                width = w,
                height = h - margin.top;

            var svg = d3.select(".chart-cont-q1 .rightBottomDiv .splineChart").append("svg")
                .attr("width", w + 87)
                .attr("height", h + margin.bottom),

                g = svg.append("g").attr("transform", "translate(" + (margin.bottom) + "," + margin.top + ")")

            var x = d3.scaleTime().range([0, width + margin.bottom]),
                y = d3.scaleLinear().range([height, 0]),
                z = d3.scaleOrdinal().range($scope.question.barColors);

            var line = d3.line()
                .curve(d3.curveBasis)
                .x(function(d) {
                    var dt = new Date(d.date * 1000);
                    return x(dt);
                })
                .y(function(d) {
                    return y(d[Object.keys(d)[1]]);
                });

            $ctrl.mainSplineChartData.columns = $ctrl.data.brands.map(function(a) { return a.key; });

            var brands = data.map(function(id) {
                return {
                    id: Object.keys(id.values[0])[1],
                    values: id.values
                };

            });

            x.domain(d3.extent($ctrl.mainSplineChartData, function(d) {
                    var dt = new Date(d.date * 1000);
                    return dt;
                }))
                .range([0, w + margin.bottom]);

            y.domain([
                    0,
                    d3.max(brands, function(c) { return d3.max(c.values, function(d) { return d[Object.keys(d)[1]]; }); })
                ])
                .range([height, 0]);

            z.domain(brands.map(function(c) { return c.id; }));

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x)
                    .ticks(3)
                    .tickFormat(d3.timeFormat("%_m/%d/%Y")));

            g.append("g")
                .attr("class", "axis axis--y")
                .call(d3.axisLeft(y)
                    .ticks(4)
                    .tickSizeInner(-w - 45)
                    .tickSizeOuter(0)
                    .tickPadding(10)
                )
                .append("text")
                .attr("transform", "rotate(-90)")

            g.selectAll("text")
                .style("fill", "#5e829f");

            g.selectAll(".domain")
                .style("display", "none")

            var city = g.selectAll(".city")
                .data(brands)
                .enter().append("g")
                .attr("class", "city");

            city.append("path")
                .attr("class", "searches")
                .attr("d", function(d) { return line(d.values); })
                .style("stroke", function(d) { return z(d.id); })
                .style("stroke-width", "3px")
                .style("fill", "none");

        }
        if (svg) {
            $ctrl.drawMouseHover(svg, width, height, color, x, y, data);
        }


    }

    //------  Tootip on mouse over------ //
    $ctrl.drawMouseHover = function(svg, width, height, color, x, y, dataNest) {
        /* To create the mouse over effects */

        var sentimentMouseComp = svg.append("g")
            .attr("class", "searches-mouse-over-component")
            .attr("transform", "translate(30,20)");

        // Adding vertical Line
        sentimentMouseComp.append("path")
            .attr("class", "searches-mouse-over-line")
            .style("stroke-width", "1px")
            .style("stroke-dasharray", ("3, 3"))
            .style("opacity", "0");

        //text for new tooltip//

        sentimentMouseComp.append("g")
            .attr('class', 'searches-mouse-over-text-all');

        d3.select('.searches-mouse-over-text-all')
            .append('svg:rect')
            .attr('class', 'searches-mouse-over-text-bg searches-date-bg')
            .attr('x', '5')
            .attr('y', '-20')
            .attr('width', '120')
            .attr('height', '16')
            .style("opacity", "0");

        d3.select('.searches-mouse-over-text-all')
            .append("text")
            .attr("class", "searches-mouse-over-line-date")
            .attr("transform", "translate(10,3)")
            .attr("dx", "1em");

        for (var k = 0; k < dataNest.length; k++) {
            d3.select('.searches-mouse-over-text-all')
                .append('text')
                .attr('class', 'searches-mouse-over-text' + k)
                .attr("dx", "1em");
        }


        var sentiments = document.getElementsByClassName('searches');

        // Adding "g" for each line
        var perSentimentMouseComp = sentimentMouseComp.selectAll('.component-per-searches')
            .data(dataNest)
            .enter()
            .append("g")
            .attr("class", "component-per-searches");

        // Adding circles for each Line
        perSentimentMouseComp.append("circle")
            .attr("r", 5)
            .attr("class", "mouse-circles")
            .style("stroke", function(d, i) {
                return d.color = color[i];
            })
            .style("fill", "#172530")
            .style("stroke-width", "2px")
            .style("opacity", "0");

        // Adding canvas to catch mouse movements
        sentimentMouseComp.append('svg:rect')
            .attr('width', width + 30)
            .attr('height', height)
            .attr('fill', 'none')
            .attr('pointer-events', 'all')
            // ------ On mouse out hide this component -----
            .on('mouseout', function() {
                d3.select(".searches-mouse-over-line")
                    .style("opacity", "0");
                d3.selectAll(".component-per-searches circle")
                    .style("opacity", "0");
                d3.selectAll(".component-per-searches text")
                    .style("opacity", "0");
                d3.selectAll(".searches-mouse-over-text-bg")
                    .style("opacity", "0");
                d3.select(".searches-mouse-over-text-all")
                    .style("opacity", "0");
                d3.selectAll(".searches-mouse-over-line-date")
                    .style("opacity", "0");
            })
            // ------ On mouse over show this component -----
            .on('mouseover', function() {
                d3.select(".searches-mouse-over-line")
                    .style("opacity", "1");
                d3.selectAll(".component-per-searches circle")
                    .style("opacity", "1");
                d3.selectAll(".component-per-searches text")
                    .style("opacity", "1");
                d3.selectAll(".searches-mouse-over-text-bg")
                    .style("opacity", "0.8");
                d3.selectAll(".searches-mouse-over-line-date")
                    .style("opacity", "1");
                d3.select(".searches-mouse-over-text-all")
                    .style("opacity", "1");

            })
            // ------ On mouse move capture mouse positions -----
            .on('mousemove', function() {
                var mouse = d3.mouse(this);
                var transformY1, transformY2, transformY3, transformX;
                if (mouse[0] < 2) {
                    d3.select(".searches-mouse-over-line")
                        .style("opacity", "0");
                    d3.selectAll(".component-per-searches circle")
                        .style("opacity", "0");
                    d3.selectAll(".component-per-searches text")
                        .style("opacity", "0");
                    d3.selectAll(".searches-mouse-over-text-bg")
                        .style("opacity", "0");
                    d3.selectAll(".searches-mouse-over-line-date")
                        .style("opacity", "0");
                    d3.select(".searches-mouse-over-text-all")
                        .style("opacity", "0");
                } else {
                    d3.select(".searches-mouse-over-line")
                        .style("opacity", "1");
                    d3.selectAll(".component-per-searches circle")
                        .style("opacity", "1");
                    d3.selectAll(".component-per-searches text")
                        .style("opacity", "1");
                    d3.selectAll(".searches-mouse-over-text-bg")
                        .style("opacity", "0.8");
                    d3.selectAll(".searches-mouse-over-line-date")
                        .style("opacity", "1");
                    d3.select(".searches-mouse-over-text-all")
                        .style("opacity", "1");
                }

                d3.select(".searches-mouse-over-line")
                    .attr("d", function() {
                        var d = "M" + mouse[0] + "," + height;
                        d += " " + mouse[0] + "," + 0;
                        return d;
                    });

                // ------ Creating the continuous transformation -----
                d3.selectAll(".component-per-searches")
                    .attr("transform", function(d, i) {
                        var xDate = x.invert(mouse[0]),
                            bisect = d3.bisector(function(d) {
                                return d.values[i].sdate;
                            }).right;
                        //var idx = bisect(d.values[i].spercent, xDate);

                        var beginning = 0,
                            end = 0,
                            target = null;
                        if (sentiments[i] != undefined) {
                            end = sentiments[i].getTotalLength();
                        }

                        while (true) {
                            var pos = {};
                            target = Math.floor((beginning + end) / 2), pos = { x: 0, y: 0 };
                            if (sentiments[i] != undefined) {
                                pos = sentiments[i].getPointAtLength(target);
                            }

                            if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                                break;
                            }
                            if (pos.x != 0 && pos.x > mouse[0]) end = target;
                            else if (pos.x != 0 && pos.x < mouse[0]) beginning = target;
                            else break; //position found
                        }

                        if (mouse[0] < width - 110) {
                            d3.select(".searches-mouse-over-line-date")
                                .attr('dx', '1em');
                            d3.selectAll(".searches-mouse-over-text-bg")
                                .attr('x', '15');
                            d3.select(".searches-date-bg")
                                .attr('x', '10');
                            d3.selectAll(".searches-mouse-over-text" + i)
                                .attr('dx', '1em');
                        } else {
                            d3.select(".searches-mouse-over-line-date")
                                .attr('dx', '-120');
                            d3.selectAll(".searches-mouse-over-text-bg")
                                .attr('x', '-70');
                            d3.select(".searches-date-bg")
                                .attr('x', '-120');
                            d3.selectAll(".searches-mouse-over-text" + i)
                                .attr('dx', '-120');
                        }

                        if (pos.x != 0) {
                            // d3.select(this).select('text')
                            //   .text(y.invert(pos.y).toFixed());

                            d3.select(".searches-mouse-over-text" + i)

                            .text(jwtUtil.truncate((d.values[i].brand), 10) + " : " + Math.abs(y.invert(pos.y).toFixed()))
                                .attr("transform", "translate(5, " + (((i + 1) * 15) + 10) + ")")
                                .style("fill", color[i]);


                            d3.select(".searches-mouse-over-text-all")
                                .attr("transform", "translate(" + mouse[0] + ", 10)");


                            if (i == dataNest.length - 1) {
                                d3.selectAll(".searches-mouse-over-text-bg")
                                    .attr('height', (((i + 1) * 15) + 30))
                            }

                            var dText = jwtUtil.monthNamesAbbr(xDate.getMonth()) + "/" + jwtUtil.padDigit(xDate.getDate()) + "/" + xDate.getFullYear();

                            d3.select(".searches-mouse-over-line-date")
                                .text(dText).attr("transform", "translate(5, 10)");
                            d3.select(".searches-date-bg")
                                .text(dText).attr("transform", "translate(0, 10)");

                            if (i == 0) {
                                transformY1 = pos.y;
                            } else if (i == 1) {
                                transformY2 = pos.y;
                            } else {
                                transformY3 = pos.y;
                            }
                            transformX = mouse[0];


                            return "translate(" + mouse[0] + "," + pos.y + ")";

                        }

                    });
            });
    }; //mouse over function end//


    $ctrl.showSummaryInfo = function(w, h, data) {
        if ($ctrl.mainSplineChartData !== undefined && data !== undefined) {
            var domain = d3.extent($ctrl.mainSplineChartData, function(d) {
                var dt = new Date(d.date * 1000);
                return dt;
            });
            var startDate = domain[0];
            var endDate = domain[1];

            startDate = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
            endDate = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();

            var summaryData = data.indicators;

            var margin = { top: 20, right: 20, bottom: 30, left: 40 },
                width = w - margin.left - margin.right,
                height = h - margin.top - margin.bottom;

            var summaryDiv = d3.select(".chart-cont-q1 .leftDiv .summaryInfo")

            var topSummaryDiv = summaryDiv.append("div")
                .attr("class", "topSummaryDiv");

            topSummaryDiv.append("span")
                .text("Values from " + startDate + " to " + endDate + "")
                .attr("class", "topDivText")


            var leftSummaryDiv = summaryDiv.append("div")
                .attr("class", summaryData[0].key);


            leftSummaryDiv.append("span")
                .text(summaryData[0].value + summaryData[0].unit)
                .attr("class", "percent")
            leftSummaryDiv.append("span")
                .text(summaryData[0].title)
                .attr("class", "maximumText")

            var rightSummaryDiv = summaryDiv.append("div")
                .attr("class", summaryData[1].key);

            rightSummaryDiv.append("span")
                .text(summaryData[1].value + summaryData[1].unit)
                .attr("class", "percent")
            rightSummaryDiv.append("span")
                .text(summaryData[1].title)
                .attr("class", "minimumText")

        }

    }


}
