/* --------- Controller for Question 1 --------------*|
 |---  Q1: How much have people looked for me in comparison to competitors? --------|
 |----------------------------------------------------*/

angular
    .module('app.components')
    .factory('jwtQuestionDiscover1Service', ['commonService', 'jwtQuestionDiscover1Constant', function(commonService, jwtQuestionDiscover1Constant) {

        return angular.extend({
            cnst: jwtQuestionDiscover1Constant
        }, commonService);

    }]);