(function() {
    'use strict';

    angular
        .module('app.components')
        .component('jwtQuestionDiscover1', {
            controller: questionDiscover1Controller,
            templateUrl: 'app/components/questions/discover/question-1/discover.question-1.html',
            bindings: {
                data: '<',
                isWorkspace: '<',
                showWorkspaceSelect:'<',
                jwtChartId: '<'
            }
        });

})();
