/* ------------------- App Constants  ---------------------------*|
 |--- This is a common constant files for the application   ------|
 |---------------------------------------------------------------*/
angular.module('app.common.constants', [])
    .constant('jwtAppConstant', {

        /* Constants for header tabs  */
        headerConst: {
            tab1: {
                'text': 'discover',
                'state':'app.discover'
            },
            tab2: {
                'text': 'evaluate',
                'state':'app.evaluate'
            },
            tab3: {
                'text': 'engage'
            },
            tab4: {
                'text': 'purchase'
            }
        },
        googleHotTrend: {
            apiUrl: 'https://trends.google.com/trends/hottrends/visualize?nrow=5&ncol=5'
        },

        /* Constant for discover tab */

        /* Constant for evaluate tab */
        evaluate: {
            'q7': {
                'title': 'Brands',
                'qText': 'What brands are people talking about and how do they feel about them?',
                'lineColor': ["#83888b", "#49BFAB", "#597C98"],
                'barColor': ["#86AB66", "#b24547", "#c08b36"],
                'splineChartTitle': {
                    'documents': 'SENTIMENT OVER TIME - DOCUMENTS',
                    'sentences': 'SENTIMENT OVER TIME - SENTENCES',
                    'subjects': 'SENTIMENT OVER TIME - SUBJECT SENTENCES'
                }
            }
        },
        /* Constant for client mapping */
        clientMapping: {
            'client': {
                'title': 'Client Mapping',
                'messages': {
                    'title': 'Client Messages',
                    'clientEmpty': 'The client name should not be empty.',
                    'brandEmpty': 'The brand name should not be empty.',
                    'projectEmpty': 'The project name should not be empty.',
                    'projectExists': 'The project you selected already exists. Please select a new one.',
                    'projectSuccess': 'Project updated successfully',
                    'projectRoleExists': 'Project updated successfully'
                }
            }
        },
        EVENT_NAME: {
            WORKSPACE: {
                ADDED: 'NEW_WORKSPACE_ADDED',
                DELETED: 'WORKSPACE_DELETED',
                EXPANDED:'WORKSPACE_EXPANDED'
            },
            CHART: {
                RESIZED: 'WINDOW_RESIZED',
                MAXIMIZED: 'TILE_EXPANDED'
            }
        },
        CHART_COMPONENTS: {
            q1: { id: 'q1', name: '', html: '<jwt-question-discover-1 is-workspace="true"></jwt-question-discover-1>', snapShot: "app/assets/img/snapshot/q1.png", type: 'INTERNAL' },
            q2: { id: 'q2', name: '', html: '<jwt-question-discover-2 is-workspace="true"></jwt-question-discover-2>', snapShot: "app/assets/img/snapshot/q2.png", type: 'INTERNAL' },
            q6: { id: 'q6', name: '', html: '<jwt-question-discover-6 is-workspace="true"></jwt-question-discover-6>', snapShot: "app/assets/img/snapshot/q6.png", type: 'INTERNAL' },
            q7: { id: 'q7', name: '', html: '<jwt-question-evaluate-7 is-workspace="true"></jwt-question-evaluate-7>', snapShot: "app/assets/img/snapshot/q7.png", type: 'INTERNAL' },
            q8: { id: 'q8', name: '', html: '<jwt-question-evaluate-8 is-workspace="true"></jwt-question-evaluate-8>', snapShot: "app/assets/img/snapshot/q8.png", type: 'INTERNAL' },
            q9: { id: 'q9', name: '', html: '<jwt-question-evaluate-9 is-workspace="true"></jwt-question-evaluate-9>', snapShot: "app/assets/img/snapshot/q9.png", type: 'INTERNAL' },
            q10: { id: 'q10', name: '', html: '<jwt-question-evaluate-10 is-workspace="true"></jwt-question-evaluate-10>', snapShot: "app/assets/img/snapshot/q10.png", type: 'INTERNAL' },
            q11: { id: 'q11', name: '', html: '<jwt-question-evaluate-11 is-workspace="true"></jwt-question-evaluate-11>', snapShot: "app/assets/img/snapshot/q11.png", type: 'INTERNAL' },
            q12ratio: { id: 'q12ratio', name: '', html: '<jwt-question-evaluate-12-ratio is-workspace="true"></jwt-question-evaluate-12-ratio>', snapShot: "app/assets/img/snapshot/q12ratio.png", type: 'INTERNAL' },
            q12post: { id: 'q12post', name: '', html: '<jwt-question-evaluate-12-post is-workspace="true"></jwt-question-evaluate-12-post>', snapShot: "app/assets/img/snapshot/q12post.png", type: 'INTERNAL' },
            q13: { id: 'q13', name: '', html: '<jwt-question-evaluate-13 is-workspace="true"></jwt-question-evaluate-13>', snapShot: "app/assets/img/snapshot/q13.png", type: 'INTERNAL' },
            q14: { id: 'q14', name: '', html: '<jwt-question-evaluate-14 is-workspace="true"></jwt-question-evaluate-14>', snapShot: "app/assets/img/snapshot/q14.png", type: 'INTERNAL' },
            q15: { id: 'q15', name: '', html: '<jwt-question-evaluate-15 is-workspace="true"></jwt-question-evaluate-15>', snapShot: "app/assets/img/snapshot/q15.png", type: 'INTERNAL' },
            g1: { id: 'g1', name: '', html: '<jwt-google-hot-trend-g1 is-workspace="true"></jwt-google-hot-trend-g1>', snapShot: "app/assets/img/snapshot/g1.png", type: 'INTERNAL' },
            g2: { id: 'g2', name: '', html: '<jwt-google-hot-trend-g2 is-workspace="true"></jwt-google-hot-trend-g2>', snapShot: "app/assets/img/snapshot/g2.png", type: 'INTERNAL' },
            g3: { id: 'g3', name: '', html: '<jwt-google-hot-trend-g3 is-workspace="true"></jwt-google-hot-trend-g3>', snapShot: "app/assets/img/snapshot/g3.png", type: 'INTERNAL' },
            g4: { id: 'g4', name: '', html: '<jwt-google-hot-trend-g4 is-workspace="true"></jwt-google-hot-trend-g4>', snapShot: "app/assets/img/snapshot/g4.png", type: 'INTERNAL' },
            g5: { id: 'g5', name: '', html: '<jwt-google-hot-trend-g5 is-workspace="true"></jwt-google-hot-trend-g5>', snapShot: "app/assets/img/snapshot/g5.png", type: 'INTERNAL' },
            discover: { id: 'discover', name: '', html: '<jwt-question-discover-home is-workspace="true"></jwt-question-discover-home>', snapShot: "app/assets/img/snapshot/discover.png", type: 'INTERNAL' },
            evaluate: { id: 'evaluate', name: '', html: '<jwt-question-evaluate-home is-workspace="true"></jwt-question-evaluate-home>', snapShot: "app/assets/img/snapshot/evaluate.png", type: 'INTERNAL' },
            infegy: { id: 'infegy', name: '', html: 'https://atlas.infegy.com/login', snapShot: "app/assets/img/snapshot/infegy.png", type: 'EXTERNAL' },
            googleTrend: { id: 'googleTrend', name: '', html: '<jwt-google-hot-trend-home is-workspace="true"></jwt-google-hot-trend-home>', snapShot: "app/assets/img/snapshot/google-trend.png", type: 'INTERNAL' }


        }
    });
