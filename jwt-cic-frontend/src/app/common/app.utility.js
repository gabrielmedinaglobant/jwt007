/* --------------------------- App utility  -----------------------------*|
 |--- This is a common place for utility functions for the application ---|
 |-----------------------------------------------------------------------*/
(function(window, document) {
    'use strict';

    var monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var dayNamesFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    // attach myLibrary as a property of window
    var jwtUtil = window.jwtUtil || (window.jwtUtil = {});

    /*  -------- BEGIN Utility Functions -------------- */

    // Adding 0 for single digits
    function padDigit(d) {
        return (d < 10) ? '0' + d.toString() : d.toString();
    }

    // Fetching short month names
    function monthNamesAbbr(index) {
        return monthNamesShort[parseInt(index)];
    }

    // Adding "th, st, rd, nd" to digits
    function addOrdinal(n, isDate) {
        var temp, num;
        if (isDate) {
            temp = n.split('/');
            num = temp[1]
        } else {
            num = n;
        }
        var ords = ['th', 'st', 'nd', 'rd'];
        var o = ('' + (num % 10))
        if (num > 10 && num < 14) {
            num = num + 'th';
        } else {
            num = num + (ords[o] || 'th');
        }
        if (isDate) {
            return monthNamesAbbr(temp[0]) + " " + num + " " + temp[2];
        } else {
            return num;
        }

    }

    // conver string into camel case
    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
    }

    //truncate string and add ...
    function truncate(string,pos){
       if (string.length > pos)
          return string.substring(0,pos)+'...';
       else
          return string;
    };

    // Adding commas to currency, checking and adding units
    function formatWithCommas(number, type, unit, trend) {
        var result = null;
        var unitsArr = ["$"];
        var trendObj = { "plus": "+", "minus": "-" };

        switch (type) {
            case "f":
                result = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                break;
            case "c":
                if (unitsArr.indexOf(unit) !== -1) {
                    result = unit + number;
                } else {
                    result = number + unit;
                }
                break;
            case "t":
                if (trendObj[trend] != "" || trendObj[trend] != "undefined") {
                    result = trendObj[trend] + " " + number;
                }
                break;
            case "fc":
                var temp = formatWithCommas(number, "f", unit);
                result = formatWithCommas(temp, "c", unit);
                break;
            case "fct":
                var temp = formatWithCommas(number, "f", unit, trend);
                var temp1 = formatWithCommas(temp, "c", unit, trend);
                result = formatWithCommas(temp1, "t", unit, trend);
                break;
        }
        return result;
    }

    // Get Viewport width and height
    function getViewport() {
        var viewPortWidth;
        var viewPortHeight;

        // -- the more standards compliant browsers use window.innerWidth and window.innerHeight ---//
        if (typeof window.innerWidth != 'undefined') {
            viewPortWidth = window.innerWidth,
                viewPortHeight = window.innerHeight
        }
        // -- older browsers --//
        else {
            viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
                viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
        }
        return { 'width': viewPortWidth, 'height': viewPortHeight };
    }

    // Check if browser is safari
    function whichBrowser() {
        var browserObj = { 'is_safari': false, 'is_firefox': false, 'is_chrome': false };

        var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
        var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        var is_chrome = !!window.chrome && !!window.chrome.webstore;

        browserObj.is_safari = is_safari;
        browserObj.is_firefox = is_firefox;
        browserObj.is_chrome = is_chrome;

        return browserObj
    }

    // Get New height and width for the containers
    function getNewContAttrs(curCtrl, expanded) {
        var targetElement, chartCont, newContData, startResize, chartContId;
        if (curCtrl.jwtChartId == 'q12ratio') {
            chartContId = 'q12-ratio';
        } else if (curCtrl.jwtChartId == 'q12post') {
            chartContId = 'q12-post';
        } else {
            chartContId = curCtrl.jwtChartId;
        }

        chartCont = $('.chart-cont-' + chartContId).parents('.masonry-brick');

        var calAttrs = function(obj) {
            targetElement = $(obj);
            var nWidth = Math.round(targetElement.innerWidth());
            var nHeight = Math.round(targetElement.innerHeight());
            if (!expanded) {
                nHeight = (nHeight > 360) ? 360 : nHeight;
            }

            newContData = {
                'nwidth': nWidth,
                'nheight': nHeight
            };

            if (curCtrl.jwtChartId == 'q7') {

                var nWidth1 = Math.round(targetElement.find('.chart-cont-q7').width());

                newContData = {
                    'nwidth': nWidth,
                    'nheight': nHeight,
                    'elem': targetElement.find('.chart-cont-q7')
                };
            }

            curCtrl.showLoader();
        };
        calAttrs(chartCont);
        return newContData;
    }

    //get index of a particular value in an array of objects
    function ValueIndexInArrayObject(myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm) return i;
        }
        return -1;
    }

    // ****** - * - * - ----- END Utility Functions ------ - * - * - ----- ****** //

    // publish external API by extending myLibrary
    function publishJWTUtility(jwtUtility) {
        angular.extend(jwtUtility, {
            'padDigit': padDigit,
            'monthNamesAbbr': monthNamesAbbr,
            'addOrdinal': addOrdinal,
            'formatWithCommas': formatWithCommas,
            'toTitleCase': toTitleCase,
            'truncate':truncate,
            'getViewport': getViewport,
            'whichBrowser': whichBrowser,
            'getNewContAttrs': getNewContAttrs,
            'ValueIndexInArrayObject': ValueIndexInArrayObject
        });
    }

    publishJWTUtility(jwtUtil);

})(window, document);
