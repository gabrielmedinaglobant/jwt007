angular
    .module('app.common')
    .factory('httpAuthInterceptor', ['$q', 'userService', 'jwtConfigConstant', function($q, userService, jwtConfigConstant) {

        var requestQueue = {};

        function getUrl(config){
            var url = '';
            url = config.url;
            _.each(config.params,function(o){
                url = url + o;
            })
            return url;
        }    
        
        return {
            'request': function(config) {
                if (userService.isLoggedIn()) {
                    config.headers.Authorization = "Bearer " + userService.getAuthToken();
                }
                config.timeout = 10000;
                if (config.method == "GET") {
                    var rUrl = getUrl(config);
                    if (angular.isDefined(requestQueue[getUrl(config)])) {
                        config.timeout = 1;
                    } else {
                        requestQueue[getUrl(config)] = true;
                    }
                }
                return config;
            },
            'response': function(response) {
                if (_.contains([401], response.status)) {
                    window.location.assign(jwtConfigConstant['AUTH_APP']);
                }
                if (response.config.method == "GET") {
                    if (angular.isDefined(requestQueue[getUrl(response.config)])) {
                        delete requestQueue[getUrl(response.config)];
                    }
                }
                return response;
            },
            responseError: function(response) {
                if (response.config.method == "GET") {
                    if (angular.isDefined(requestQueue[getUrl(response.config)])) {
                        delete requestQueue[getUrl(response.config)];
                    }
                }
                return $q.reject(response);
            }
        };
    }]);