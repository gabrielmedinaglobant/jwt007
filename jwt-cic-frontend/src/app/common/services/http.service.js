(function() {
    'use strict';

    angular
        .module('app.common')
        .factory('httpService', httpService)

    httpService.$inject = ['$window', '$http', 'jwtConfigConstant','userService'];


    function httpService($window, $http, jwtConfigConstant,userService) {


        function prepareUrl(urlConfig, urlData) {
            var api = "";
            if (urlData) {
                if (_.isFunction(urlConfig.API)) {
                    api = urlConfig.API(urlData);
                }
            } else {
                api = urlConfig.API;
            }
            return api;
        }

        function getUrl(urlConfig, urlData) {
            var url = '';
            if (angular.isObject(urlConfig)) {
                if (urlConfig.PREFIX) {
                    url = jwtConfigConstant.BASE_API + prepareUrl(urlConfig, urlData);
                } else {
                    url = prepareUrl(urlConfig, urlData);
                }
            } else {
                url = jwtConfigConstant.BASE_API + urlConfig;
            }
            return url;
        }

        this.getData = function(urlConfig, params, urlData) {
            if(_.isUndefined(params)){
                params = {};
                params['project-id'] = userService.getUserProjectId();
            }else{
                if(_.isUndefined(params['project-id'])){
                     params['project-id'] = userService.getUserProjectId();
                }
            }
            var url = getUrl(urlConfig, urlData);
            return $http({
                method: 'GET',
                url: url,
                params: params
            });

        }

        this.putData = function(urlConfig, data, urlData) {
            var url = getUrl(urlConfig, urlData);
            return $http({
                method: 'PUT',
                url: url,
                data: angular.toJson(data)
            });
        }

        this.postData = function(urlConfig, data, urlData) {
            var url = getUrl(urlConfig, urlData);
            return $http({
                method: 'POST',
                url: url,
                data: data
            });
        }

        this.deleteData = function(urlConfig, data, urlData) {
            var url = getUrl(urlConfig, urlData);
            return $http({
                method: 'DELETE',
                url: url,
                data: angular.toJson(data),
                headers: {
                    "Content-Type": "application/json"
                }
            });

        }

        return this;

    }


})();