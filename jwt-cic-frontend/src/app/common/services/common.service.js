(function() {
'use strict';

  angular
    .module('app.common')
    .factory('commonService', commonService)

    commonService.$inject = ['httpService','jwtAppConstant','jwtConfigConstant'];
 
    function commonService(httpService,jwtAppConstant,jwtConfigConstant) {
        
        function getConstant(key){
            return this.cnst[key] || jwtAppConstant[key] || jwtConfigConstant[key];
        }


        return angular.extend({
                getConstant:getConstant
            },httpService);
    }
    
})();
