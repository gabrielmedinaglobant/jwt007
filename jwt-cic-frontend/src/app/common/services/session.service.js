angular
    .module('app.common')
    .factory('sessionService', [function() {

        this.userData = {};

        function setSessionData(key, data) {
            localStorage.setItem(key, data);
        }

        function getSessionData(key) {
            return localStorage.getItem(key);
        }

        function unsetSessionData(key) {
            localStorage.removeItem(key);
        }

        function clearSessionData() {
            localStorage.clear();
        }

        function setTempStorage(key, data) {
            sessionStorage.setItem(key, data);
        }

        function getTempStorage(key) {
            return sessionStorage.getItem(key);
        }

        function unsetTempStorage(key) {
            sessionStorage.removeItem(key);
        }

        function clearTempStorage() {
            sessionStorage.clear();
        }

        return angular.extend({
            setSessionData: setSessionData,
            getSessionData: getSessionData,
            unsetSessionData: unsetSessionData,
            clearSessionData: clearSessionData,
            setTempStorage: setTempStorage,
            getTempStorage: getTempStorage,
            unsetTempStorage: unsetTempStorage,
            clearTempStorage: clearTempStorage
        })

    }]);