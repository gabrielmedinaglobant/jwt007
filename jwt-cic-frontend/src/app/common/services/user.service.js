angular
.module('app.common')
.factory('userService',['sessionService',function(sessionService) {

    //this.userData = {};
    
    function setUserData(data){
        sessionService.setSessionData('USERDATA',angular.toJson(data));
    }

    function getUserData(){
        var d = sessionService.getSessionData('USERDATA');
        if(_.isNull(d)){
            return undefined;
        }
        return angular.fromJson(d);
    }

    function isLoggedIn(){
        if(angular.isUndefined(getUserData())){
            return false;
        }else{
            return true;
        }
    }
    
    function getAuthToken(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return getUserData();
        }
    }

    function getUserInfo(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return jwt_decode(getUserData());
        }
    }

    function getUserRole(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return jwt_decode(getUserData())['role'];
        }
    }

    function getUserProjects(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return jwt_decode(getUserData())['projectAssignments'];
        }
    }

    function getUserNameId(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return jwt_decode(getUserData())['nameId'];
        }
    }

    function getUserProjectId(){
        if(angular.isUndefined(getUserData())){
            return undefined;
        }else{
            return jwt_decode(getUserData())['projectId'];
        }
    }
    
    return angular.extend({
        setUserData:setUserData,
        getUserData:getUserData,
        isLoggedIn:isLoggedIn,
        getAuthToken:getAuthToken,
        getUserInfo:getUserInfo,
        getUserRole:getUserRole,
        getUserProjects:getUserProjects,
        getUserNameId:getUserNameId,
        getUserProjectId:getUserProjectId
    },sessionService);

}]);

