angular.module('app').
config(routesConfig);

routesConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('discover');
    $stateProvider.state('app', {
        url: '/',
        views:{
            'sidebar@app':{
                template: '<jwt-sidebar></jwt-sidebar>'
            },
            'questionheader@app':{
                template: '<jwt-question-header></jwt-question-header>'
            },
            'workspace@app':{
                template: '<jwt-workspace-list></jwt-workspace-list>'
            },
            '@':{
                template: '<jwt-main-view></jwt-main-view>',
            }            
        }
    }).state('app.discover', {
        url: 'discover',
        template: '<jwt-question-discover-home></jwt-question-discover-home>',
        authenticate: true
    }).state('app.evaluate', {
        url: 'evaluate',
        template: '<jwt-question-evaluate-home></jwt-question-evaluate-home>',
        authenticate: true
    }).state('app.infegy', {
        url: 'infegy',
        template:   '<jwt-infegy-home></jwt-infegy-home>',
        authenticate: true
    }).state('app.clients', {
        url: 'clients',
        template:  '<jwt-client-home><jwt-client-home>',
        authenticate: false
    }).state('workspace', {
        url: '/workspace/:id',
        template: '<jwt-workspace-view><jwt-workspace-view>',
        authenticate: false
    }).state('app.googlehottrend', {
        url: 'google-hot-trend/',
        template:  '<jwt-google-hot-trend-home><jwt-google-hot-trend-home>',
        authenticate: true
    }).state('receive-token', {
        url: '/receive-token?:token',
        template:'<jwt-auth><jwt-auth>',
        authenticate: true
    }).state('logout', {
        url: 'logout',
        template:'<jwt-logout><jwt-logout>',
        authenticate: true
    });
}