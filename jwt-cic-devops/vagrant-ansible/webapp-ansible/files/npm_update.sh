#!/bin/bash

echo "UPDATING NPM"
npm cache clean -f
npm install -g n
#n stable
n 6.10.1
cd /data/jwt-cic-frontend/ && npm install grunt --save-dev
npm install -g grunt-cli
npm install -g bower
#npm install -g pm2
node -v

echo "DONE"

exit 0