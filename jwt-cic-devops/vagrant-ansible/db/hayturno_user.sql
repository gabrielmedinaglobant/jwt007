CREATE USER 'app_hayturno'@'%' IDENTIFIED BY 'pass';
GRANT SELECT ON hayturno.* TO 'app_hayturno'@'%';
GRANT INSERT ON hayturno.* TO 'app_hayturno'@'%';
GRANT DELETE ON hayturno.* TO 'app_hayturno'@'%';
GRANT UPDATE ON hayturno.* TO 'app_hayturno'@'%';
GRANT EXECUTE ON hayturno.* TO 'app_hayturno'@'%';