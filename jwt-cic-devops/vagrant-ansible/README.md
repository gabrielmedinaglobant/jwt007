JWT DCC Vagrant Local Development environment
===============

This git repo provides the code to orchestrate an **JWT DCC** environment in your local workstation 100% automated by using Vagrant + Bvox + Ansible defined entirely as code.
So the developer can just clone de repo navigate to the Vagrantfile and run "vagrant up" then any change in this repo will be automatically repflected (deployed) in this local environment.

![Alt text](./figures/VagrantVBAnsible.png)

## Objectives & Benefits
* DevTeam can **generate a complete New Local Development Environment** from scratch with the required characteristics.
    1. Isolate dependencies and their configuration within a single disposable, consistent environment, without sacrificing any of the tools you are used to working with (editors, browsers, debuggers, etc.).
    2. Once you or someone else creates a single **Vagrantfile**, you just need to **vagrant up** and everything is installed and configured for you to work.
    3. Members of your team create their development environments from the same configuration, so whether you are working on Linux, Mac OS X, or Windows, all your team members are running code in the same environment, against the same dependencies, all configured the same way.
    4. Say goodbye to "works on my machine" bugs.
    5. You will be able to **stop / start** this New Environment on demand optimizing the use of your workstation resources.

## Prerequisites:
* You must install:
    1. Virtual Box
    2. Vagrant
    3. PuTTY and PuTTYGen (Windows only)

### Vagrant & Vbox Official reference links:
* Installing VirtualBox:* [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
* Installing Vagrant:* [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)
* Installing Vagrant Plugins:* [https://www.vagrantup.com/docs/plugins/usage.html](https://www.vagrantup.com/docs/plugins/usage.html)
* Vagrant Plugins :* [https://www.vagrantup.com/docs/cli/plugin.html](https://www.vagrantup.com/docs/cli/plugin.html)

### Vagrant Win/MacOS installation reference links:
* Installing Vagrant on Windows:* [https://www.sitepoint.com/getting-started-vagrant-windows/](https://www.sitepoint.com/getting-started-vagrant-windows/)
* Installing Vagrant on MacOS:* [http://sourabhbajaj.com/mac-setup/Vagrant/README.html](http://sourabhbajaj.com/mac-setup/Vagrant/README.html)

**Consle output as reference:**
```shell
$ vagrant plugin
Usage: vagrant plugin <command> [<args>]

Available subcommands:
     expunge
     install
     license
     list
     repair
     uninstall
     update

For help on any individual command run `vagrant plugin COMMAND -h`


# delivery @ delivery-E5450 in ~/Binbash/repos/devops/vagrant/workstation on git:dev-ebarrirero x [18:51:47]
$ vagrant plugin list
vagrant-share (1.1.7)
vagrant-vbguest (0.13.0)
```

## Deploying your JWT DCC Vagrant local development environment:
Just navigate to the your repository folder and fetch de Vagrantfile so as to be sit in this context and the just **"vagrant up --provision"**.
Then access in your local browser **http://20.0.0.10** and you'll get the JWT DCC webapp.

## Main Configuration Files:
- **Vagrantfile** (Virtual Environment Setup)
+ **setup.yml** (webapp-ansible playbook - Complete AppStack Detailed Nginx1.12.0/php5.6fmp/Mysql5.6)
