#!/bin/bash

# set -e
# Start JWT services in background

DATE=$(date)
pgrep -l grunt > /dev/null
if [ $? -eq 0 ]; then
  echo $DATE 'grunt is running! :) stopping it'
  sudo pkill grunt
else
  echo $DATE 'grunt was not running! :('
fi

sudo service tomcat stop
sudo service nginx stop

pgrep -l java > /dev/null
if [ $? -eq 0 ]; then
  echo $DATE 'java tomcat is running! :) stopping it'
  sudo pkill java
else
  echo $DATE 'java tomcat was not running! :('
fi

# if [ -z "$STY" ]; then exec screen -dm -S screenName /bin/bash "$0"; fi

echo "STARTING BACK-END CIC SERVICES"
#sudo chown jenkins:jenkins -R /data/src/
# consider jwt-sic-frontend/src/jwt.confing.json
# cd /src/www/current/jwt-dcc-dev/jwt-cic-services && mvn clean package -P qa
java -jar /data/jwt-cic-services/target/jwt-cic-services-*.jar &

echo "STARTING FRON-END SERVER"
#sudo chown jenkins:jenkins -R /home/jenkins/
#sed -i -e 's/jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com/jwt-dcc-a-ElasticL-XKTQ97W6246C-2021751421.us-west-2.elb.amazonaws.com/g' \
#/src/www/current/jwt-dcc-dev/jwt-cic-frontend/src/jwt.config.json
rm -rf /data/jwt-cic-frontend/.bowerrc
cd /data/jwt-cic-frontend && npm install
cd /data/jwt-cic-frontend && bower install
grunt dist --gruntfile /src/www/current/jwt-dcc-dev/jwt-cic-frontend/Gruntfile.js --env=LOCAL

echo "Done"

exit 0
