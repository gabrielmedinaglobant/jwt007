#!/bin/bash

DATE=$(date)
echo $DATE 'Syncing development branch to Bitbucket repo'
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && checkout development
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git checkout .
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git pull

sed -i -e 's/url = https:\/\/e.barrirero/#url = https:\/\/e.barrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config
sed -i -e 's/#url = https:\/\/ebarrirero/url = https:\/\/ebarrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config

cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git push origin development

sed -i -e 's/#url = https:\/\/e.barrirero/url = https:\/\/e.barrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config
sed -i -e 's/url = https:\/\/ebarrirero/#url = https:\/\/ebarrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config


echo $DATE 'Syncing qa branch to Bitbucket repo'
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git checkout qa
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git checkout .
cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git pull

sed -i -e 's/url = https:\/\/e.barrirero/#url = https:\/\/e.barrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config
sed -i -e 's/#url = https:\/\/ebarrirero/url = https:\/\/ebarrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config

cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git push origin qa

sed -i -e 's/#url = https:\/\/e.barrirero/url = https:\/\/e.barrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config
sed -i -e 's/url = https:\/\/ebarrirero/#url = https:\/\/ebarrirero/g' \
/home/delivery/Globant/JWT/jwt_repos/JWT007/.git/config

cd /home/delivery/Globant/JWT/jwt_repos/JWT007 && git push origin development

exit0
