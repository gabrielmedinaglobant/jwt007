from fabric.api import run, sudo, settings, env
from fabric.context_managers import cd, shell_env
from fabric.contrib.files import exists
from fabric.decorators import task
from termcolor import colored

env.user = "ubuntu"
# env.password = "ubuntu"

# In env.roledefs we define the remote servers. It can be IP Addrs or domain names.
env.roledefs = {
    'local': ['localhost'],
}


@task
def jwt_ubu_ser_provision():
    with settings(warn_only=True):
        print colored('###########################################', 'blue')
        print colored('######### JWT SERVER PROVISIONING #########', 'blue')
        print colored('###########################################', 'blue')

        print colored('##############################', 'blue')
        print colored('## UPGRADING & DEPENDENCIES ##', 'blue')
        print colored('##############################', 'blue')
        sudo('apt-get update')
        sudo('apt-get -y upgrade')
        sudo('apt-get install -y git')

        print colored('===================================================================', 'blue')
        print colored('DEPENDENCIES PROVISIONING                          ', 'blue', attrs=['bold'])
        print colored('===================================================================', 'blue')

        with cd('/home/ubuntu'):
            if exists('/home/ubuntu/proton', use_sudo=True):
                with cd('/home/ubuntu/proton'):
                    run('git checkout dev-test')
                    run('git pull')
                    sudo('./requirements.sh')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_maven:ubuntu')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_oracle_java8')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_node')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_py_libs')
            else:
                run('git clone https://github.com/exequielrafaela/proton.git')
                with cd('/home/ubuntu/proton'):
                    run('git checkout dev-test')
                    sudo('./requirements.sh')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_maven:ubuntu')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_oracle_java8')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_node')
                    run('fab -f ~/proton/fabfile.py -R local inst_ubu_14_fab.install_py_libs')


@task
def jwt_app_mvn_package():
    with settings(warn_only=True):
        print colored('======================================', 'blue')
        print colored('MAVEN PACKAGE JWT APP ', 'blue', attrs=['bold'])
        print colored('======================================', 'blue')

        if exists('/data/pom.xml', use_sudo=True):
            with cd('/data'):
                sudo('npm install -g grunt-cli')
                # ubuntu@jwt:/data$ mvn -DskipTests clean package -P development
                with shell_env(M2_HOME='/usr/local/apache-maven/apache-maven-3.3.9',
                               M2='/usr/local/apache-maven/apache-maven-3.3.9/bin',
                               MAVEN_OPTS='-Xms256m -Xmx512m',
                               PATH='/usr/local/apache-maven/apache-maven-3.3.9/bin:$PATH'):
                    run('mvn -v')
                    run('mvn clean package -P development')

                    # jwt-cic-services % jwt-cic-user-services are packaged like jar in order to be run as below
                    # Still needed to be packaged as war (.zip) order to be hosted in an apache tomcat server.
        else:
            print colored('========================================', 'blue')
            print colored('Pom.xml does not exists ', 'blue', attrs=['bold'])
            print colored('========================================', 'blue')


@task
def runbg(cmd, sockname="dtach"):
    return run('dtach -n `mktemp -u /tmp/%s.XXXX` %s' % (sockname, cmd))


@task
def jwt_app_deploy_vagrant():
    with settings(warn_only=True):
        print colored('======================================', 'blue')
        print colored('START JWT APP ', 'blue', attrs=['bold'])
        print colored('======================================', 'blue')

        try:
            sudo('pkill grunt')
        except SystemExit:
            print colored('========================================', 'blue')
            print colored('GRUNT Server Not running', 'blue', attrs=['bold'])
            print colored('========================================', 'blue')

            # with cd('/data'):
            # run('git checkout .')
            # run('git checkout qa')
            # run('git pull')

        with cd('/data/jwt-cic-frontend'):
            run('pwd')
            run('npm install')
            run('bower install')
            # runbg('grunt serve --env=QA')

        try:
            sudo('pkill java')
        except SystemExit:
            print colored('========================================', 'blue')
            print colored('Java Service Not running', 'blue', attrs=['bold'])
            print colored('========================================', 'blue')

        # alt solution sudo('nohup python app.py >& /dev/null < /dev/null &')
        with cd('/data/jwt-cic-services'):
            # run('mvn clean package -P development')
            with shell_env(M2_HOME='/usr/local/apache-maven/apache-maven-3.3.9',
                           M2='/usr/local/apache-maven/apache-maven-3.3.9/bin',
                           MAVEN_OPTS='-Xms256m -Xmx512m',
                           PATH='/usr/local/apache-maven/apache-maven-3.3.9/bin:$PATH'):
                run('mvn -v')
                run('mvn clean package -P qa')
                # runbg('java -Dserver.port=8080'
                # ' -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.1-SNAPSHOT.jar')
        # sudo('nohup java -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.1-SNAPSHOT.jar
        #  >& /dev/null < /dev/null &')
        # to test the API http://jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com:8080 \
        # /api/v1/questions/brands-people-talking-about

        with cd('/data/jwt-cic-services'):
            # run('mvn clean package -P development')
            with shell_env(M2_HOME='/usr/local/apache-maven/apache-maven-3.3.9',
                           M2='/usr/local/apache-maven/apache-maven-3.3.9/bin',
                           MAVEN_OPTS='-Xms256m -Xmx512m',
                           PATH='/usr/local/apache-maven/apache-maven-3.3.9/bin:$PATH'):
                run('mvn -v')
                run('mvn clean package -P qa')
                # runbg('java -Dserver.port=8081 -jar'
                # ' /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.1-SNAPSHOT.jar')
                # sudo('nohup java -Dserver.port=8081
                #  -jar /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.1-SNAPSHOT.jar')


                # run('nohup sh /scripts/start_jwt_serv.sh >& /dev/null < /dev/null &')


@task
def jwt_app_deploy_aws():
    with settings(warn_only=True):
        print colored('======================================', 'blue')
        print colored('START JWT APP ', 'blue', attrs=['bold'])
        print colored('======================================', 'blue')

        try:
            sudo('pkill grunt')
        except SystemExit:
            print colored('========================================', 'blue')
            print colored('GRUNT Server Not running', 'blue', attrs=['bold'])
            print colored('========================================', 'blue')

        with cd('/data'):
            run('git checkout .')
            run('git checkout qa')
            run('git pull')

        with cd('/data/jwt-cic-frontend'):
            run('pwd')
            run('npm install')
            run('bower install')
            # runbg('grunt serve --env=QA')

        try:
            sudo('pkill java')
        except SystemExit:
            print colored('========================================', 'blue')
            print colored('Java Service Not running', 'blue', attrs=['bold'])
            print colored('========================================', 'blue')

        # alt solution sudo('nohup python app.py >& /dev/null < /dev/null &')
        with cd('/data/jwt-cic-services'):
            # run('mvn clean package -P development')
            with shell_env(M2_HOME='/usr/local/apache-maven/apache-maven-3.3.9',
                           M2='/usr/local/apache-maven/apache-maven-3.3.9/bin',
                           MAVEN_OPTS='-Xms256m -Xmx512m',
                           PATH='/usr/local/apache-maven/apache-maven-3.3.9/bin:$PATH'):
                run('mvn -v')
                run('mvn clean package -P qa')
                # runbg('java -Dserver.port=8080'
                # ' -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.1-SNAPSHOT.jar')
        # sudo('nohup java -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.1-SNAPSHOT.jar
        #  >& /dev/null < /dev/null &')
        # to test the API http://jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com:8080 \
        # /api/v1/questions/brands-people-talking-about

        with cd('/data/jwt-cic-services'):
            # run('mvn clean package -P development')
            with shell_env(M2_HOME='/usr/local/apache-maven/apache-maven-3.3.9',
                           M2='/usr/local/apache-maven/apache-maven-3.3.9/bin',
                           MAVEN_OPTS='-Xms256m -Xmx512m',
                           PATH='/usr/local/apache-maven/apache-maven-3.3.9/bin:$PATH'):
                run('mvn -v')
                run('mvn clean package -P qa')
                # runbg('java -Dserver.port=8081 -jar'
                # ' /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.1-SNAPSHOT.jar')
                # sudo('nohup java -Dserver.port=8081
                #  -jar /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.1-SNAPSHOT.jar')


                # run('nohup sh /scripts/start_jwt_serv.sh >& /dev/null < /dev/null &')


def orchestrate_jwt():
    with settings(warn_only=True):
        jwt_ubu_ser_provision()
        jwt_app_mvn_package()
        jwt_app_deploy_aws()


@task
def pkg_deploy_jwt():
    with settings(warn_only=True):
        jwt_app_mvn_package()
        jwt_app_deploy_aws()