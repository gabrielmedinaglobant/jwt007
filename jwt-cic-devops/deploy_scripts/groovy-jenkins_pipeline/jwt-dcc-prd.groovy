/**
 * Created by Exequiel Barrirero on 04/04/17.
 * Modified by Manish Rangari on 28/06/2017
 */

def ROOT_DIR

node {
    try {
        notifyBuild('STARTED')
    ROOT_DIR = pwd()
	stage ('Checkout JWT DCC Production application code') {
	            git branch: 'production',
	                    credentialsId: 'f0547d46-cc9a-4d7a-8188-21621367d86b',
	                    url: 'https://wdgit.globant.com/git/JWT007'
	        }
	
	        stage ('Creating FrontEnd Node dist folder via grunt build') {
	            dir("${ROOT_DIR}" + "/jwt-cic-frontend/") {
			        echo "I am currently in " + pwd()
			
	                sh 'rm -rf .bowerrc'
	                sh 'npm install'
	                sh 'bower install'
	                sh 'grunt build --gruntfile Gruntfile.js --env=PROD'
	                sh 'aws s3 sync dist/ s3://jwt-artifacts/production/frontend_$(date +%d-%m-%Y)/'
	            }
	        }
	        
	        /*
	        stage ('Building FrontEnd 2 application via npm') {
	            dir("${ROOT_DIR}" + "/jwt-cic-manager-frontend/") {
			        echo "I am currently in " + pwd()
		            sh 'npm install'
	               // sh 'npm run build --env=X'
	                sh 'npm run build'
	                sh 'aws s3 sync dist/ s3://jwt-artifacts/production/frontend2_$(date +%d-%m-%Y)/'
	            }
	        }
	        */
	
	        stage ('Creating jar package for Back-End Services') {
	            dir("${ROOT_DIR}" + "/jwt-cic-services/") {
			echo "I am currently in " + pwd()
	                sh 'mvn clean package -P production,war'
	                sh 'aws s3 cp target/*.war s3://jwt-artifacts/production/backend_$(date +%d-%m-%Y)/'
	            }
	        }
	        
	        stage ('Creating artifact for Auth Services') {
	            dir("${ROOT_DIR}" + "/jwt-cic-auth/") {
			echo "I am currently in " + pwd()
	                sh 'mvn clean package -P production,war'
	                sh 'aws s3 cp target/*.war s3://jwt-artifacts/production/backend_$(date +%d-%m-%Y)/'
	            }
	        }
	        
	        stage ('Running Flyway migration script') {
	            dir("${ROOT_DIR}" + "/jwt-cic-db/") {
	                sh 'mvn flyway:migrate -P production'
	            }
	        }
	
	        stage ('Deploy Auth applications to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/auth_prd/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	        
	        stage ('Deploy REST API applications to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/apiservices_prd/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	
	        stage ('Deploy FrontEnd application to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/ansistrano_fe_prd/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	        /*
	        stage ('Deploy second frontEnd application to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/fe2_dev/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	        */
    } catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.result = "FAILED"
        throw e as Throwable

    } finally {
        // Success or failure, always send notifications
        notifyBuild(currentBuild.result)
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESSFUL'

    // Default values
    // java.lang.String colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'" as Object
    def summary = "${subject} (${env.BUILD_URL})" as Object
    // def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    // <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }
    // Send notifications
    slackSend(color: colorCode, message: summary)
}
