/**
 * Created by Exequiel Barrirero on 04/04/17.
 * Modified by Manish Rangari on 30/05/2017
 */

def ROOT_DIR

node {
    try {
        notifyBuild('STARTED')
    ROOT_DIR = pwd()
	stage ('Checkout JWT DCC Development application code') {
	            git branch: 'development',
	                    credentialsId: 'f0547d46-cc9a-4d7a-8188-21621367d86b',
	                    url: 'https://wdgit.globant.com/git/JWT007'
	        }
	
	        stage ('Creating FrontEnd Node dist folder via grunt build') {
	            dir("${ROOT_DIR}" + "/jwt-cic-frontend/") {
			echo "I am currently in " + pwd()
			sed_command_status = sh (
				script: 'sed -i -e \'s|jwt-dcc-a-elasticl-umtrlt5fkngp-1533616561.us-west-2.elb.amazonaws.com:8080|jwt-dcc-a-ElasticL-UMTRLT5FKNGP-1533616561.us-west-2.elb.amazonaws.com/services|\' -e \'s|jwt-cic-auth-dev.ngrok.io|jwt-dcc-a-ElasticL-UMTRLT5FKNGP-1533616561.us-west-2.elb.amazonaws.com|\' src/jwt.config.json',
				returnStatus: true
			) == 0
			echo "Sed command status: ${sed_command_status}"
	                sh 'rm -rf .bowerrc'
	                sh 'npm install'
	                sh 'bower install'
	                sh 'grunt build --gruntfile Gruntfile.js --env=DEV'
	            }
	        }
	
	        stage ('Creating jar package for Back-End Services') {
	            dir("${ROOT_DIR}" + "/jwt-cic-services/") {
			echo "I am currently in " + pwd()
	                sh 'mvn clean package -P development,war'
	            }
	        }
	        
	        stage ('Creating artifact for Auth Services') {
	            dir("${ROOT_DIR}" + "/jwt-cic-auth/") {
			echo "I am currently in " + pwd()
	                sh 'mvn clean package -P development,war'
	            }
	        }
	        
	        stage ('Running Flyway migration script') {
	            dir("${ROOT_DIR}" + "/jwt-cic-db/") {
	                sh 'mvn flyway:migrate -P development'
	            }
	        }
	
	        stage ('Deploy BackEnd applications to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/backend_dev/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	
	        stage ('Deploy FrontEnd application to instances') {
	            ansiblePlaybook(
	                    playbook: ROOT_DIR + '/jwt-cic-devops/aws_cf_ansible_jwt/ansistrano_fe_dev/setup.yml',
	                    inventory: '/usr/local/bin/ec2.py',
	                    credentialsId: 'c3f51524-fc8f-4876-99be-3613de8339bb',
	                    colorized: true
	            )
	        }
	        
    } catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.result = "FAILED"
        throw e as Throwable

    } finally {
        // Success or failure, always send notifications
        notifyBuild(currentBuild.result)
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESSFUL'

    // Default values
    // java.lang.String colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'" as Object
    def summary = "${subject} (${env.BUILD_URL})" as Object
    // def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    // <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }
    // Send notifications
    slackSend(color: colorCode, message: summary)
}
