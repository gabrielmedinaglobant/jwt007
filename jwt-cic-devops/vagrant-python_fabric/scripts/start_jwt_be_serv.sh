#!/bin/bash
# Start JWT services in background

echo "STARTING BACK-END CIC SERVICES"
java -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.1-SNAPSHOT.jar &

echo "STARTING BACK-END USER CIC SERVICES"
java -Dserver.port=8081 -jar /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.1-SNAPSHOT.jar &

echo "Done"

exit 0