#!/bin/bash
# Start JWT services in background
if [ -z "$STY" ]; then exec screen -dm -S screenName /bin/bash "$0"; fi

echo "STARTING FRON-END SERVER"
grunt serve --gruntfile /data/jwt-cic-frontend/Gruntfile.js --env=QA

echo "Done"

exit 0