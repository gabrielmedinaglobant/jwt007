#!/bin/bash
# Start JWT services in background

if [ -z "$STY" ]; then exec screen -dm -S screenName /bin/bash "$0"; fi

echo "STARTING BACK-END CIC SERVICES"
java -jar /data/jwt-cic-services/target/jwt-cic-services-0.0.2.jar &

# echo "STARTING BACK-END USER CIC SERVICES"
# java -Dserver.port=8081 -jar /data/jwt-cic-user-services/target/jwt-cic-user-services-0.0.2.jar &

echo "STARTING FRON-END SERVER"
grunt serve --gruntfile /data/jwt-cic-frontend/Gruntfile.js --env=QA

echo "Done"

exit 0
