#!/bin/bash

# set -e
# Start JWT services in background

sudo service nginx stop

DATE=$(date)
pgrep -l grunt > /dev/null
if [ $? -eq 0 ]; then
  echo $DATE 'grunt is running! :) stopping it'
  sudo pkill grunt
else
  echo $DATE 'grunt was not running! :('
fi

# if [ -z "$STY" ]; then exec screen -dm -S screenName /bin/bash "$0"; fi

echo "STARTING FRON-END SERVER"
sudo chown jenkins:jenkins -R /src/www/
sudo chown jenkins:jenkins -R /home/jenkins/
sed -i -e 's/jwt-dcc-qa-01-elb-137856687.us-east-1.elb.amazonaws.com/jwt-dcc-a-ElasticL-XKTQ97W6246C-2021751421.us-west-2.elb.amazonaws.com/g' \
/src/www/current/jwt-dcc-dev/jwt-cic-frontend/src/jwt.config.json
rm -rf /src/www/current/jwt-dcc-dev/jwt-cic-frontend/.bowerrc
cd /src/www/current/jwt-dcc-dev/jwt-cic-frontend && npm install
cd /src/www/current/jwt-dcc-dev/jwt-cic-frontend && bower install
grunt serve --gruntfile /src/www/current/jwt-dcc-dev/jwt-cic-frontend/Gruntfile.js --env=QA

echo "Done"

exit 0
