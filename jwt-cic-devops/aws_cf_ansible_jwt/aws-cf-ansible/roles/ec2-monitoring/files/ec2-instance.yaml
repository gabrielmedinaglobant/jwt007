---
AWSTemplateFormatVersion: '2010-09-09'
Description: 'EC2 instance with auto-recovery for monitoring host and ELK'
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentVPCStack
    - Label:
        default: 'EC2 Parameters'
      Parameters:
      - InstanceType
      - Name
      - SubnetName
      - KeyName
      - LogsRetentionInDays
Parameters:
  ParentVPCStack:
    Description: 'Stack name of parent VPC stack based template.'
    Type: String
  KeyName:
    Description: 'Optional key pair of the ec2-user to establish a SSH connection to the EC2 instance.'
    Type: String
    Default: ''
  InstanceType:
    Description: 'The instance type for the EC2 instance.'
    Type: String
    Default: 't2.micro'
  Name:
    Description: 'The name for the EC2 instance.'
    Type: String
    Default: 'test'
  SubnetName:
    Description: 'Subnet name of parent VPC stack template.'
    Type: String
    Default: SubnetAPublicProduction
    AllowedValues:
    - SubnetAPublicProduction
    - SubnetBPublicProduction
    - SubnetAPublicStaging
    - SubnetBPublicStaging
  LogsRetentionInDays:
    Description: 'Specifies the number of days you want to retain log events.'
    Type: Number
    Default: 14
    AllowedValues: [1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653]
Mappings:
  RegionMap:
    'ap-southeast-1':
      AMI: 'ami-19c87b7a'
    'ca-central-1':
      AMI: 'ami-835be6e7'
    'eu-central-1':
      AMI: 'ami-2a3de945'
    'eu-west-1':
      AMI: 'ami-ee517b88'
    'sa-east-1':
      AMI: 'ami-3a701156'
    'us-east-1':
      AMI: 'ami-7fa00169'
    'us-west-1':
      AMI: 'ami-a1722bc1'
    'us-west-2':
      AMI: 'ami-b92fa3d9'
Conditions:
  HasKeyName: !Not [!Equals [!Ref KeyName, '']]
Resources:
  Logs:
    Type: 'AWS::Logs::LogGroup'
    Properties:
      RetentionInDays: !Ref LogsRetentionInDays
  SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Ref Name
      VpcId:
        'Fn::ImportValue': !Sub '${ParentVPCStack}-VPC'
  SecurityGroupInSSHWorld:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref SecurityGroup
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      CidrIp: '0.0.0.0/0'
  SecurityGroupInHTTPWorld:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref SecurityGroup
      IpProtocol: tcp
      FromPort: 80
      ToPort: 80
      CidrIp: '0.0.0.0/0'
  InstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: '/'
      Roles:
      - !Ref IAMRole
  IAMRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - 'ec2.amazonaws.com'
          Action:
          - 'sts:AssumeRole'
      Path: '/'
      Policies:
      - PolicyName: 'ec2'
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - 'ec2:AssociateAddress'
            Resource:
            - '*'
      - PolicyName: logs
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - 'logs:CreateLogGroup'
            - 'logs:CreateLogStream'
            - 'logs:PutLogEvents'
            - 'logs:DescribeLogStreams'
            Resource:
            - 'arn:aws:logs:*:*:*'
      - PolicyName: ec2-readonly
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - 'ec2:Describe*'
            Resource:
            - '*'
  VirtualMachine:
    Type: 'AWS::EC2::Instance'
    Metadata:
      'AWS::CloudFormation::Init':
        config:
          files:
            '/var/awslogs/etc/awscli.conf':
              content: !Sub |
                [default]
                region = ${AWS::Region}
                [plugins]
                cwlogs = cwlogs
              mode: '000644'
              owner: root
              group: root
            '/var/awslogs/etc/awslogs.conf':
              content: !Sub |
                [general]
                state_file = /var/lib/awslogs/agent-state
                [/var/log/dmesg]
                datetime_format = %b %d %H:%M:%S
                file = /var/log/dmesg
                log_stream_name = {instance_id}/var/log/dmesg
                log_group_name = ${Logs}
                [/var/log/secure]
                datetime_format = %b %d %H:%M:%S
                file = /var/log/secure
                log_stream_name = {instance_id}/var/log/secure
                log_group_name = ${Logs}
                [/var/log/cron]
                datetime_format = %b %d %H:%M:%S
                file = /var/log/cron
                log_stream_name = {instance_id}/var/log/cron
                log_group_name = ${Logs}
                [/var/log/cloud-init.log]
                datetime_format = %b %d %H:%M:%S
                file = /var/log/cloud-init.log
                log_stream_name = {instance_id}/var/log/cloud-init.log
                log_group_name = ${Logs}
                [/var/log/cfn-init.log]
                datetime_format = %Y-%m-%d %H:%M:%S
                file = /var/log/cfn-init.log
                log_stream_name = {instance_id}/var/log/cfn-init.log
                log_group_name = ${Logs}
                [/var/log/cfn-hup.log]
                datetime_format = %Y-%m-%d %H:%M:%S
                file = /var/log/cfn-hup.log
                log_stream_name = {instance_id}/var/log/cfn-hup.log
                log_group_name = ${Logs}
                [/var/log/cfn-init-cmd.log]
                datetime_format = %Y-%m-%d %H:%M:%S
                file = /var/log/cfn-init-cmd.log
                log_stream_name = {instance_id}/var/log/cfn-init-cmd.log
                log_group_name = ${Logs}
                [/var/log/cloud-init-output.log]
                file = /var/log/cloud-init-output.log
                log_stream_name = {instance_id}/var/log/cloud-init-output.log
                log_group_name = ${Logs}
                [/var/log/dmesg]
                file = /var/log/dmesg
                log_stream_name = {instance_id}/var/log/dmesg
                log_group_name = ${Logs}
              mode: '000644'
              owner: root
              group: root
    Properties:
      ImageId: !FindInMap [RegionMap, !Ref 'AWS::Region', AMI]
      IamInstanceProfile: !Ref InstanceProfile
      InstanceType: !Ref InstanceType
      SecurityGroupIds:
      - !Ref SecurityGroup
      KeyName: !If [HasKeyName, !Ref KeyName, !Ref 'AWS::NoValue']
      UserData:
        'Fn::Base64': !Sub |
             #!/bin/bash -x
             apt-get update
             apt-get install -y python-dev python-pip libffi-dev libssl-dev libxml2-dev libxslt1-dev libjpeg8-dev zlib1g-dev python-setuptools git
             pip install ansible
             mkdir aws-cfn-bootstrap-latest
             curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1
             easy_install aws-cfn-bootstrap-latest
             /usr/local/bin/cfn-init -v --stack ${AWS::StackName} --resource VirtualMachine --region ${AWS::Region}
             /usr/local/bin/cfn-signal -e $? --region ${AWS::Region} --stack ${AWS::StackName} --resource VirtualMachine
      SubnetId:
        'Fn::ImportValue': !Sub '${ParentVPCStack}-${SubnetName}'
      Tags:
      - Key: Name
        Value: !Ref Name
    CreationPolicy:
      ResourceSignal:
        Count: 1
        Timeout: PT10M
  MyEIP:
    Type: AWS::EC2::EIP
    Properties:
      InstanceId: !Ref VirtualMachine
Outputs:
  TemplateID:
    Description: 'template id'
    Value: 'ec2/ec2-auto-recovery'
  IPAddress:
    Description: 'The public IP address instance.'
    Value: !Ref MyEIP
    Export:
      Name: !Sub '${AWS::StackName}-IPAddress'
