#!/bin/bash

# set -e
# Start JWT services in background

DATE=$(date)

sudo service tomcat stop

pgrep -l java > /dev/null
if [ $? -eq 0 ]; then
  echo $DATE 'java tomcat is running! :) stopping it'
  sudo pkill java
else
  echo $DATE 'java tomcat was not running! :('
fi

echo "STARTING BACK-END CIC SERVICES"
sudo chown jenkins:jenkins -R /src/www/
# consider jwt-sic-frontend/src/jwt.confing.json
# cd /src/www/current/jwt-dcc-dev/jwt-cic-services && mvn clean package -P qa
java -jar /src/www/current/jwt-dcc-dev/jwt-cic-services/target/jwt-cic-services-*.jar &

echo "Done"

exit 0
