package com.jwt.cic.tasks.services;

public interface TaskService {
	void reportCurrentTime();
	void startPythonHelloWorld();
	void runInfegyFetchAndCalculations();
}
