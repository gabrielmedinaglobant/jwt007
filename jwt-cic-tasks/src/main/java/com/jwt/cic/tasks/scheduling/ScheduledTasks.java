package com.jwt.cic.tasks.scheduling;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jwt.cic.tasks.services.TaskService;

/***
 * 
 * Component that holds Scheduled Tasks. Each scheduled task corresponds to a method of TasksService.
 * 
 * @author gabriel.medina
 *
 */
@Component
public class ScheduledTasks {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private TaskService tasksService;
    
    @Autowired
    public ScheduledTasks(TaskService tasksService) {
		this.tasksService = tasksService;
	}

	// @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
		LOG.info("reportCurrentTime started!");
		tasksService.reportCurrentTime();
    }
    	
	/**
     * 
     * Gets executed every day at 6:00 AM greenwich time
     * 
     */
	@Scheduled(cron ="${cron-expression}", zone="${zone}")
    // @Scheduled(fixedRate = 60000)
	public void runInfegyFetchAndCalculations(){
		LOG.info("Infegy data fetch and calculations have been triggered!");
		tasksService.runInfegyFetchAndCalculations();
	}
	
	
}
