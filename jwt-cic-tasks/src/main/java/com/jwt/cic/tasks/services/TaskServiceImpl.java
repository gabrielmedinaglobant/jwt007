package com.jwt.cic.tasks.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jwt.cic.tasks.scheduling.ScheduledTasks;

@Service
public class TaskServiceImpl implements TaskService {

	private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	@Value("${pycalc-batch-args}")
	private String infegyFetchAndCalculationsArgs;
	@Value("${pycalc-batch-directory}")
	private String infegyFetchAndCalculationsDir;
	
	@Override
	public synchronized void reportCurrentTime() {
		LOG.info("The time is now {}", dateFormat.format(new Date()));
	}

	@Override
	public synchronized void startPythonHelloWorld() {
		
		boolean isWindows = System.getProperty("os.name")
				  .toLowerCase().startsWith("windows");
		Process process;
		
		
	    try {
	    	
			process = Runtime.getRuntime()
			  .exec("python --help");
			
			LOG.info("Excecuting python 2.7");
			int exitCode = process.waitFor();
			LOG.info( inputStreamToStringWithLines(process.getInputStream()) );
			
			if(exitCode == 0){
				LOG.info("Python process was executed successfully");
			}
			
		} catch (IOException e) {
			LOG.info("Unable to open python process");
			e.printStackTrace();
		} catch (InterruptedException e) {
			LOG.info("InterruptedException happened while executing python process");
			e.printStackTrace();
		}
	    
	}
	
	protected final String inputStreamToStringWithLines(InputStream is){
		StringBuilder sb = new StringBuilder(); 
		
		sb.append(LINE_SEPARATOR);
		new BufferedReader(new InputStreamReader(is)).lines()
        .forEach( (str) -> sb.append(str).append(LINE_SEPARATOR) );
		
		return sb.toString();
	}

	@Override
	public void runInfegyFetchAndCalculations() {
		Process process;

	    try {
	    	
			process = Runtime.getRuntime()
			  .exec("python " + infegyFetchAndCalculationsArgs, null, new File(infegyFetchAndCalculationsDir));
			
			LOG.info("Excecuting python process");
			
			// int exitCode = process.waitFor(2, TimeUnit.SECONDS);
			int exitCode = 0;
			while(!process.waitFor(1, TimeUnit.SECONDS)){
				// LOG.info("Process is still being executed...");
				LOG.info( inputStreamToStringWithLines(process.getInputStream()) );
			}
			exitCode = process.waitFor();
			
			
			if(exitCode == 0){
				LOG.info("Python process was executed successfully");
			}
			else{
				LOG.info("There has been an error on execution. Exit code: " + exitCode);
			}
			
		} catch (IOException e) {
			LOG.info("Unable to open python process");
			e.printStackTrace();
		} catch (InterruptedException e) {
			LOG.info("InterruptedException happened while executing python process");
			e.printStackTrace();
		}
	}
	
}
