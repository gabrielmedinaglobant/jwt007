package com.jwt.tasks.model;

public interface TaskUnit {
	public int execute();
}
