import json
import math
import operator

import logging as log
import logging as info
from dateutil import parser
import ConfigParser
import iso8601
import pytz
#
#
import pandas as pd
import numpy as np
import itertools

from datetime import datetime

log.basicConfig(filename='/var/log/pycalcs/infegyproc.log',level=log.ERROR)
# in this level log i'll save info about of calc.
info.basicConfig(filename='/var/log/pycalcs/infegyproc.info.log',level=info.INFO)



class infegy:
    def ConfigSectionMap(self,section):
        dict1 = {}
        Config = ConfigParser.ConfigParser()
       # Config.read("/GLOBANT/Trends/json.ini")
        Config.read("json.ini")
        options = Config.options(section)
        for option in options:
            try:
                dict1[option] = Config.get(section, option)
                if dict1[option] == -1:
                    print("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def WhatbrandspeopletalkingNew(self, path, braddic, dirout):
        ArrayBrand = path.split(',')
        json_brand = []
        json_result = {}
        header ={}
        headerCom=[]
        json_global=[]
        c =0
        marca=""
        json_data=""
        count=0

        print("AQUI LLEGO #1")

        brands_list = []
        for fileIn in ArrayBrand:

            try:

                header={}

                file = json.loads(open(fileIn).read())
                json_data = json.dumps(file['output'], indent=4, sort_keys=True)
                df = pd.read_json(json_data, dtype=object, encoding='utf-8')
                result = np.asscalar( df.groupby("documents").agg({"documents": np.sum}).apply(sum)['documents'] )

                myAvg = round (df['documents'].mean(),0)
                mySum = df['documents'].sum()
                # NEW END
                print("AQUI LLEGO #2")
                count = df['documents'].count() + count
                str = fileIn.split('/')[-2]
                print str
                if not str == 'infegy-my-brand':
                    print("AQUI LLEGO #2.1")
                    print(json_result)
                    print(braddic)
                    json_result[braddic[str]] = result
                    header["brand"] = braddic[str]
                    header["average"] = myAvg
                    header["ratio"] = mySum
                    

                print("AQUI LLEGO #3")
                if str =='infegy-my-brand':
                    header["brand"] = braddic[str]
                    header["average"] = myAvg
                    header["ratio"] = mySum
                    myBrandSum = mySum
                    marca = braddic[str]


                document_dict = {}
                document_results = []
                for data in file['output']:
                    document_dict['date'] = data['date']
                    document_dict['value'] = data['documents']
                    document_results.append(document_dict)
                    document_dict = {}

                header['sentiment_over_time'] = document_results

                if str =='infegy-my-brand':
                    brands_list.insert(0, header)
                else:
                    brands_list.append(header)

            
            except Exception as e:
                print(e)

        for brand in brands_list:
            brand['ratio'] = int(round(float(brand["ratio"]) / float(myBrandSum), 0))
        


        #dict_ = {
        #     "brands":headerCom
        #     #"brandResults": document_results
        # }
        document_json = json.dumps({
            "brand_results" : brands_list,
            "last_updated_date" : datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
        }, indent=4)
        jsonfile = open(dirout, "w")
        jsonfile.write(document_json)
        jsonfile.close()

        print "WhatbrandspeopletalkingNew -:!"

    # What brands are people talking about and how do they feel about them?
    # the firt question
    def Whatbrandspeopletalking(self , path, dirout):
        with open(path) as data_file:
            data = json.load(data_file)
        json_data = json.dumps(data['output'], indent=4, sort_keys=True)
        df = pd.read_json(json_data, dtype=object, encoding='utf-8')
        dfs = pd.to_datetime(df.date)
        df = df.set_index(dfs)
        dfs_ = df.groupby(pd.TimeGrouper("W-MON"))
        document_json = []
        document_dict = {}
        document_summary={}
        mixed_documents_dist = ((df.groupby("mixed_documents").
                                    agg({"mixed_documents": np.sum}).apply(sum))['mixed_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).\
                                    apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).\
                                    apply(sum)['mixed_documents']) * 100

        mixed_sentences_dist = ((df.groupby("mixed_sentences").
                                        agg({"mixed_sentences": np.sum}).apply(sum))['mixed_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).\
                                        apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).\
                                        apply(sum)['mixed_sentences']) * 100

        mixed_subject_sentences_dist = ((df.groupby("mixed_subject_sentences").
                                    agg({"mixed_subject_sentences": np.sum}).apply(sum))['mixed_subject_sentences']) / float(
                                    df.groupby("positive_subject_sentences").
                                    agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                    df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}).\
                                    apply(sum)['negative_subject_sentences'] + \
                                    df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}).\
                                    apply(sum)['mixed_subject_sentences']) * 100

        #end mixed
        # begin positive document
        positive_sentences_dist =((df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum))['positive_sentences'])/float(df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences']+\
                                  df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences']+\
                                  df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        positive_documents_dist = ((df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum))['positive_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        positve_subject_sentences_dist = ((df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum))['positive_subject_sentences']) / float(
                                        df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                        df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                        apply(sum)['negative_subject_sentences'] + \
                                        df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                        apply(sum)['mixed_subject_sentences']) * 100

         #end positive documents
        negative_sentences_dist = ((df.groupby("negative_sentences").
                                        agg({"negative_sentences": np.sum}).apply(sum))['negative_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        negative_documents_dist = ((df.groupby("negative_documents").
                                    agg({"negative_documents": np.sum}).apply(sum))['negative_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        negative_subject_sentences_dist = ((df.groupby("negative_subject_sentences").
                                           agg({"negative_subject_sentences": np.sum}).apply(sum))[
                                              'negative_subject_sentences']) / float(
                                           df.groupby("positive_subject_sentences").
                                           agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                           df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                           apply(sum)['negative_subject_sentences'] + \
                                           df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                           apply(sum)['mixed_subject_sentences']) * 100

        document_summary['mixed_documents_prom'] =mixed_documents_dist
        document_summary['mixed_sentences_prom'] =mixed_sentences_dist
        document_summary['mixed_subject_sentences_prom'] =mixed_subject_sentences_dist
        document_summary['negative_documents_prom'] =negative_documents_dist
        document_summary['negative_sentences_prom'] =negative_sentences_dist
        document_summary['negative_subject_sentences_prom'] =negative_subject_sentences_dist
        document_summary['positive_documents_prom'] =positive_documents_dist
        document_summary['positive_sentences_prom'] =positive_sentences_dist
        document_summary['positive_subject_sentences_prom'] =positve_subject_sentences_dist

        # positive sentences
        positive_sentences = (dfs_['positive_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        negative_sentences=  (dfs_['negative_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        mixed_sentences =  (dfs_['mixed_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        sentences_weekly_diff =np.abs(positive_sentences - negative_sentences)
        # positive documents
        positive_documents =(dfs_['positive_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        negative_documents =(dfs_['negative_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        mixed_documents =(dfs_['mixed_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        documents_weekly_diff = np.abs(positive_documents - negative_documents)
        # positive subject sentences
        positive_subject_sentences = (dfs_['positive_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        negative_subject_sentences = (dfs_['negative_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        mixed_subject_sentences = (dfs_['mixed_subject_sentences'].apply(np.sum) / (dfs_['positive_subject_sentences'].apply(np.sum) + dfs_['negative_subject_sentences'].apply(np.sum) + dfs_['mixed_subject_sentences'].apply(np.sum))) * 100
        subject_sentences_weekly_diff = np.abs(positive_subject_sentences - negative_subject_sentences)

        negative_sentences=negative_sentences.replace(np.nan, 0, regex=True)
        mixed_sentences=mixed_sentences.replace(np.nan, 0, regex=True)
        sentences_weekly_diff=sentences_weekly_diff.replace(np.nan, 0, regex=True)
        positive_documents=positive_documents.replace(np.nan, 0, regex=True)
        negative_documents=negative_documents.replace(np.nan, 0, regex=True)
        mixed_documents=mixed_documents.replace(np.nan, 0, regex=True)
        documents_weekly_diff=documents_weekly_diff.replace(np.nan, 0, regex=True)
        positive_subject_sentences=positive_subject_sentences.replace(np.nan, 0, regex=True)
        negative_subject_sentences=negative_subject_sentences.replace(np.nan, 0, regex=True)
        mixed_subject_sentences=mixed_subject_sentences.replace(np.nan, 0, regex=True)
        subject_sentences_weekly_diff = subject_sentences_weekly_diff.replace(np.nan, 0, regex=True)

        for a,b,c,d,e,f,g,h,i,j,k,y in  itertools.izip(positive_sentences.index,
                                     negative_sentences.index,
                                     mixed_sentences.index,
                                     sentences_weekly_diff.index,
                                     positive_documents.index,
                                     negative_documents.index,
                                     mixed_documents.index,
                                     documents_weekly_diff.index,
                                     positive_subject_sentences.index,
                                     negative_subject_sentences.index,
                                     mixed_subject_sentences.index,
                                     subject_sentences_weekly_diff.index
                                     ):
            document_dict['date']=a.strftime('%Y-%m-%dT%H:%M:%SZ')
            document_dict['sentences_positivity'] = positive_sentences[a]
            document_dict['sentences_negativity'] = negative_sentences[b]
            document_dict['mixed_sentences'] = mixed_sentences[c]
            document_dict['sentences_weekly_diff'] = sentences_weekly_diff[d]
            document_dict['documents_positivity'] = positive_documents[e]
            document_dict['documents_negativity'] = negative_documents[f]
            document_dict['mixed_documents'] = mixed_documents[g]
            document_dict['documents_weekly_diff'] = documents_weekly_diff[h]
            document_dict['subject_sentences_positivity'] = positive_subject_sentences[i]
            document_dict['subject_sentences_negativity'] = negative_subject_sentences[j]
            document_dict['mixed_subject_sentences'] = mixed_subject_sentences[k]
            document_dict['subject_sentences_weekly_diff'] = subject_sentences_weekly_diff[y]
            document_json.append(document_dict)
            document_dict = {}

        document_json.append(document_summary)
        document_json = json.dumps(document_json,indent=4,sort_keys=True)
        #file __
        jsonfile = open(dirout, "w")
        jsonfile.write(document_json)
        jsonfile.close()
        print "Ok -Whatbrandspeopletalking"

    def WhatbrandspeopletalkingVersionNew(self , path, dirout):
        with open(path) as data_file:
            data = json.load(data_file)
        json_data = json.dumps(data['output'], indent=4, sort_keys=True)
        df = pd.read_json(json_data, dtype=object, encoding='utf-8')
        dfs = pd.to_datetime(df.date)
        df = df.set_index(dfs)
        dfs_ = df.groupby(pd.TimeGrouper("W-MON"))
        document_json = []
        document_dict = {}
        document_summary={}
        mixed_documents_dist = ((df.groupby("mixed_documents").
                                    agg({"mixed_documents": np.sum}).apply(sum))['mixed_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).\
                                    apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).\
                                    apply(sum)['mixed_documents']) * 100

        mixed_sentences_dist = ((df.groupby("mixed_sentences").
                                        agg({"mixed_sentences": np.sum}).apply(sum))['mixed_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).\
                                        apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).\
                                        apply(sum)['mixed_sentences']) * 100

        mixed_subject_sentences_dist = ((df.groupby("mixed_subject_sentences").
                                    agg({"mixed_subject_sentences": np.sum}).apply(sum))['mixed_subject_sentences']) / float(
                                    df.groupby("positive_subject_sentences").
                                    agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                    df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}).\
                                    apply(sum)['negative_subject_sentences'] + \
                                    df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}).\
                                    apply(sum)['mixed_subject_sentences']) * 100

        #end mixed
        # begin positive document
        positive_sentences_dist =((df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum))['positive_sentences'])/float(df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences']+\
                                  df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences']+\
                                  df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        positive_documents_dist = ((df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum))['positive_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        positve_subject_sentences_dist = ((df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum))['positive_subject_sentences']) / float(
                                        df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                        df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                        apply(sum)['negative_subject_sentences'] + \
                                        df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                        apply(sum)['mixed_subject_sentences']) * 100

         #end positive documents
        negative_sentences_dist = ((df.groupby("negative_sentences").
                                        agg({"negative_sentences": np.sum}).apply(sum))['negative_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        negative_documents_dist = ((df.groupby("negative_documents").
                                    agg({"negative_documents": np.sum}).apply(sum))['negative_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        negative_subject_sentences_dist = ((df.groupby("negative_subject_sentences").
                                           agg({"negative_subject_sentences": np.sum}).apply(sum))[
                                              'negative_subject_sentences']) / float(
                                           df.groupby("positive_subject_sentences").
                                           agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                           df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                           apply(sum)['negative_subject_sentences'] + \
                                           df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                           apply(sum)['mixed_subject_sentences']) * 100

        document_summary['mixed_documents_prom'] =mixed_documents_dist
        document_summary['mixed_sentences_prom'] =mixed_sentences_dist
        document_summary['mixed_subject_sentences_prom'] =mixed_subject_sentences_dist
        document_summary['negative_documents_prom'] =negative_documents_dist
        document_summary['negative_sentences_prom'] =negative_sentences_dist
        document_summary['negative_subject_sentences_prom'] =negative_subject_sentences_dist
        document_summary['positive_documents_prom'] =positive_documents_dist
        document_summary['positive_sentences_prom'] =positive_sentences_dist
        document_summary['positive_subject_sentences_prom'] =positve_subject_sentences_dist

        # positive sentences
        positive_sentences = (dfs_['positive_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        negative_sentences=  (dfs_['negative_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        mixed_sentences =  (dfs_['mixed_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        sentences_weekly_diff =np.abs(positive_sentences - negative_sentences)
        # positive documents
        positive_documents =(dfs_['positive_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        negative_documents =(dfs_['negative_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        mixed_documents =(dfs_['mixed_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        documents_weekly_diff = np.abs(positive_documents - negative_documents)
        # positive subject sentences
        positive_subject_sentences = (dfs_['positive_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        negative_subject_sentences = (dfs_['negative_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        mixed_subject_sentences = (dfs_['mixed_subject_sentences'].apply(np.sum) / (dfs_['positive_subject_sentences'].apply(np.sum) + dfs_['negative_subject_sentences'].apply(np.sum) + dfs_['mixed_subject_sentences'].apply(np.sum))) * 100
        subject_sentences_weekly_diff = np.abs(positive_subject_sentences - negative_subject_sentences)

        negative_sentences=negative_sentences.replace(np.nan, 0, regex=True)
        mixed_sentences=mixed_sentences.replace(np.nan, 0, regex=True)
        sentences_weekly_diff=sentences_weekly_diff.replace(np.nan, 0, regex=True)
        positive_documents=positive_documents.replace(np.nan, 0, regex=True)
        negative_documents=negative_documents.replace(np.nan, 0, regex=True)
        mixed_documents=mixed_documents.replace(np.nan, 0, regex=True)
        documents_weekly_diff=documents_weekly_diff.replace(np.nan, 0, regex=True)
        positive_subject_sentences=positive_subject_sentences.replace(np.nan, 0, regex=True)
        negative_subject_sentences=negative_subject_sentences.replace(np.nan, 0, regex=True)
        mixed_subject_sentences=mixed_subject_sentences.replace(np.nan, 0, regex=True)
        subject_sentences_weekly_diff = subject_sentences_weekly_diff.replace(np.nan, 0, regex=True)

        for a,b,c,d,e,f,g,h,i,j,k,y in  itertools.izip(positive_sentences.index,
                                     negative_sentences.index,
                                     mixed_sentences.index,
                                     sentences_weekly_diff.index,
                                     positive_documents.index,
                                     negative_documents.index,
                                     mixed_documents.index,
                                     documents_weekly_diff.index,
                                     positive_subject_sentences.index,
                                     negative_subject_sentences.index,
                                     mixed_subject_sentences.index,
                                     subject_sentences_weekly_diff.index
                                     ):
            document_dict['date']=a.strftime('%Y-%m-%dT%H:%M:%SZ')
            document_dict['sentences_positivity'] = positive_sentences[a]
            document_dict['sentences_negativity'] = negative_sentences[b]
            document_dict['mixed_sentences'] = mixed_sentences[c]
            document_dict['sentences_weekly_diff'] = sentences_weekly_diff[d]
            document_dict['documents_positivity'] = positive_documents[e]
            document_dict['documents_negativity'] = negative_documents[f]
            document_dict['mixed_documents'] = mixed_documents[g]
            document_dict['documents_weekly_diff'] = documents_weekly_diff[h]
            document_dict['subject_sentences_positivity'] = positive_subject_sentences[i]
            document_dict['subject_sentences_negativity'] = negative_subject_sentences[j]
            document_dict['mixed_subject_sentences'] = mixed_subject_sentences[k]
            document_dict['subject_sentences_weekly_diff'] = subject_sentences_weekly_diff[y]
            document_json.append(document_dict)
            document_dict = {}

        document_json.append(document_summary)
        document_json = json.dumps(document_json,indent=4,sort_keys=True)
        #file __
        jsonfile = open(dirout, "w")
        jsonfile.write(document_json)
        jsonfile.close()
        print "Ok -Whatbrandspeopletalking New Version"

    
    # GMG - 20170606 - JWT007-1828 Refresh and date ranges
    """ Q15/23 Where is the conversation happening?"""
    def Whereisconversationhappening(self, path, dirout):
        with open(path) as data_file:
            data_channel = json.load(data_file)

        json_channel = {}

        #df = pd.DataFrame(data_channel["output"],columns=['forums','blogs','microblogs','uncategorized','images'],dtype=object)
        jsonData = json.dumps(data_channel["output"], indent=4, sort_keys=True)
        query_info = data_channel["query_info"]

        df = pd.read_json(jsonData, dtype=object, encoding='utf-8')

        results_list = []

        results_list.append( { "type" : "forums" , "value" : np.asscalar(df['forums'].apply(lambda forums: forums['universe']).sum()) } )
        results_list.append( { "type" : "blogs" , "value" : np.asscalar(df['blogs'].apply(lambda forums: forums['universe']).sum()) } )
        results_list.append( { "type" : "microblogs" , "value" : np.asscalar(df['microblogs'].apply(lambda forums: forums['universe']).sum()) } )
        results_list.append( { "type" : "uncategorized" , "value" : np.asscalar(df['uncategorized'].apply(lambda forums: forums['universe']).sum()) } )
        results_list.append( { "type" : "images" , "value" : np.asscalar(df['images'].apply(lambda forums: forums['universe']).sum()) } )

        info_dict = {}
        
        info_dict['last_updated_date'] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
        info_dict['date_from'] = query_info['start_date']
        info_dict['date_to'] = query_info['end_date']

        document_json = json.dumps({ "values" : results_list, "info" : info_dict }, indent=4, sort_keys=True)

        jsonfile = open(dirout, "w")
        jsonfile.write(document_json)
        jsonfile.close()

        print "Ok -Whereisconversationhappening"

    """ What are the key themes they talk about?"""
    def whatarekeythemesabout(self, path,dirout):
        with open(path) as data_file:
          data = json.load(data_file)

          data_ =[]
        for T in data['output']:
            for K in T['themes']:
                data_.append(K)

        query_info = data["query_info"]

        json_all = json.dumps(data['output'], indent=4, sort_keys=True)
        df_general = pd.read_json(json_all, dtype=object, encoding='utf-8')
        doctotal = df_general.groupby("documents").agg({"documents": np.sum}).sum()
        json_all = json.dumps(data_, indent=4, sort_keys=True)

        df = pd.read_json(json_all, dtype=object, encoding='utf-8')
        df = df.groupby("name").agg({"positive_documents": np.sum,\
                                     "negative_documents":np.sum,\
                                     "documents":np.sum, "name": lambda x: x.nunique()})

        df_negative_frame= (df['negative_documents']/df['documents'] ) * 100
        df_positive_frame= (df['positive_documents']/df['documents'] ) * 100
        df_totals = (df['documents']/doctotal['documents'] ) * 100
        jsonbuild = {}
        for a,b,c in itertools.izip(df_totals.index,df_positive_frame.index,df_negative_frame.index):
            jsonbuild[a.replace(' ','_')] = df_totals[a]
            jsonbuild[(b+'_positive').replace(' ','_')] = df_positive_frame[b]
            jsonbuild[(c+'_negative').replace(' ','_')] = df_negative_frame[c]

        # GMG - 20170606 - JWT007-1828 Refresh and date ranges
        jsonbuild['last_updated_date']= datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
        jsonbuild['date_from'] = query_info['start_date']
        jsonbuild['date_to'] = query_info['end_date']

        json_data_dump = json.dumps(jsonbuild, indent=4, sort_keys=True)
        jsonfile = open(dirout, "w")
        jsonfile.write(json_data_dump)
        jsonfile.close()
        print "Ok-whatarekeythemesabout"


        """Has sentiment changed day over day?"""
    def sentimentchangeddayoverday(self, path,dirout):
        
        sentiment_change_day_over_day = {}
        values = []
        info = {}

        with open(path) as data_file:
            data = json.load(data_file)

        json_data = json.dumps(data['output'], indent=4, sort_keys=True)
        df = pd.read_json(json_data, dtype=object, encoding='utf-8')

        dfs = pd.to_datetime(df.date)
        df = df.set_index(dfs)
        dfs_ = df.groupby(pd.TimeGrouper("W-MON"))
        # positive subject sentences
        positive_subject_sentences = (dfs_['positive_subject_sentences'].apply(np.sum) / (
        dfs_['positive_subject_sentences'].apply(np.sum) + dfs_['negative_subject_sentences'].apply(np.sum) + dfs_[
            'mixed_subject_sentences'].apply(np.sum))) * 100

        negative_subject_sentences = (dfs_['negative_subject_sentences'].apply(np.sum) / (
            dfs_['positive_subject_sentences'].apply(np.sum) + \
            dfs_['negative_subject_sentences'].apply(np.sum) + dfs_[
                'mixed_subject_sentences'].apply(np.sum))) * 100
        mixed_subject_sentences = (dfs_['mixed_subject_sentences'].apply(np.sum) / (
            dfs_['positive_subject_sentences'].apply(np.sum) + dfs_['negative_subject_sentences'].apply(np.sum) \
            + dfs_['mixed_subject_sentences'].apply(np.sum))) * 100

        json_data_result = []
        json_dict = {}
        mixed_subject_sentences=mixed_subject_sentences.replace(np.nan, 0, regex=True)
        negative_subject_sentences=negative_subject_sentences.replace(np.nan, 0, regex=True)
        positive_subject_sentences=positive_subject_sentences.replace(np.nan, 0, regex=True)

        for a, b, c in itertools.izip(mixed_subject_sentences.index, negative_subject_sentences.index,
                                        positive_subject_sentences.index):

            json_dict['date'] = a.strftime('%Y-%m-%dT%H:%M:%SZ')
                #a.isoformat()
            #a.strftime('%Y-%m-%d')
            json_dict['mixed_subject_sentences_prom'] = mixed_subject_sentences[a]
            json_dict['negative_subject_sentences_prom'] = negative_subject_sentences[b]
            json_dict['positive_subject_sentences_prom'] = positive_subject_sentences[b]

            # this is to avoid "future" mondays
            # only insert if "today" is monday or if it is a past monday
            if( a.to_datetime() <= datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) ):
                #json_dict['date'] = datetime.now().date().strftime('%Y-%m-%dT%H:%M:%SZ') current day
                json_data_result.append(json_dict)

            json_dict = {}

        values = json_data_result
        info = {
            "last_updated_date": datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ'),
            "date_from": data['query_info']['start_date'],
            "date_to": data['query_info']['end_date']
        }
        sentiment_change_day_over_day = {
            "values": values,
            "info": info
        }

        jresult = json.dumps(sentiment_change_day_over_day, indent=4, sort_keys=True)
        jsonfile = open(dirout, "w")
        jsonfile.write(jresult)
        jsonfile.close()

        print "OK-sentimentchangeddayoverday"

            # 4 question
    """Has Volume changed day over day?"""
    def HasVolumechangeddayoverdayNew(self, path, dirout):

        with open(path) as data_file:
            data = json.load(data_file)

        file_json = []
        data_json={}

        jsonCalculations = json.dumps(data['output'], indent=4, sort_keys=True)
        dfvolume = pd.read_json(jsonCalculations, dtype=object, encoding='utf-8')
        valorMin = dfvolume['posts_word_normalized'].min()
        valorMax = dfvolume['posts_word_normalized'].max()
        contador = dfvolume['posts_word_normalized'].count()
        valorUnit = valorMax / float(contador)
        valorTend=""

        for i in range(1,len(data['output'])):
            try:
                valueCurrent = float(data['output'][i]['posts_word_normalized'])
                valuePrevius  = float(data['output'][i-1]['posts_word_normalized'])
                valorDiff = abs(float(valueCurrent) - float(valuePrevius))

                valorSize = 0
                if valorSize == 0 and valorDiff <= valuePrevius * 0.25:
                    valorSize = 1
                if valorSize == 0 and valorDiff <= valuePrevius * 0.50:
                    valorSize = 2
                if valorSize == 0 and valorDiff <= valuePrevius * 0.75:
                    valorSize = 3
                if valorSize == 0 and valorDiff < valuePrevius:
                    valorSize = 4
                if valorSize == 0 and valorDiff >= valuePrevius:
                    valorSize = 4
                if valorSize == 0 and valorDiff >= valuePrevius * 0.75:
                    valorSize = 3
                if valorSize == 0 and valorDiff >= valuePrevius * 0.50:
                    valorSize = 2
                if valorSize == 0 and valorDiff >= valuePrevius * 0.25:
                    valorSize = 1
                if valorDiff == 0:  # no Box
                    valorSize = 0

                if valorSize == 1:
                    absoluteLow = valueCurrent - valorUnit * 0.25
                    absoluteHigh = valueCurrent + valorUnit * 0.25
                if valorSize == 2:
                    absoluteLow = valueCurrent - valorUnit * 0.50
                    absoluteHigh = valueCurrent + valorUnit * 0.50
                if valorSize == 3:
                    absoluteLow = valueCurrent - valorUnit * 0.75
                    absoluteHigh = valueCurrent + valorUnit * 0.75
                if valorSize == 4:
                    absoluteLow = valueCurrent - valorUnit
                    absoluteHigh = valueCurrent + valorUnit
                if valorSize == 0:
                    absoluteLow = valueCurrent
                    absoluteHigh = valueCurrent
                if valueCurrent > valuePrevius:
                    valorTend = 'up'
                if valueCurrent < valuePrevius:
                    valorTend = 'down'
                if valueCurrent == valuePrevius:
                    valorTend = 'none'


                data_json = {}
                data_json['absolute_high'] = absoluteHigh
                data_json['absolute_low'] = absoluteLow
                data_json['date'] = data['output'][i]['date']
                data_json['relative_high'] = 0
                data_json['relative_low'] = 0
                data_json['trend'] = valorTend
                data_json['value'] = valueCurrent
                data_json['last_updated_date'] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
                file_json.append(data_json)

            except Exception as e:
                print e
                log.error(e.message)

        json_file = json.dumps(file_json, indent=4, sort_keys=True)
        jsonfile = open(dirout, "w")
        jsonfile.write(json_file)
        jsonfile.close()


    """Who is talking about me?"""
    def Whoistalkingaboutme(self, path, dirout):

        print("Whoistalkingaboutme #1")

        education,ownership,household,income,demographics,gender,age,languaje = path.split(",")

        print(education,ownership,household,income,demographics,gender,age,languaje)

        with open(languaje) as data_languaje:
            language_data = json.load(data_languaje)
        print("Whoistalkingaboutme -> languaje")
        with open(age) as data_age:
            age_data = json.load(data_age)
        print("Whoistalkingaboutme -> age")
        with open(gender) as data_gender:
            gender_data = json.load(data_gender)
        print("Whoistalkingaboutme -> gender")
        with open(demographics) as demographics_gender:
            demographics_data = json.load(demographics_gender)
        print("Whoistalkingaboutme -> demographics")
        with open(income) as data_income:
            income_data = json.load(data_income)
        print("Whoistalkingaboutme -> income")

        print("Whoistalkingaboutme -> open -> " + household)
        with open(household) as data_household:
            household_data = json.load(data_household)
        print("Whoistalkingaboutme -> household")
        with open(ownership) as data_ownership:
            ownership_data = json.load(data_ownership)
        print("Whoistalkingaboutme -> ownership")
        with open(education) as data_education:
            education_data = json.load(data_education)
        # gender process.

        print("Whoistalkingaboutme #2")

        female_count = 0
        male_count =0
        female_total =0
        male_total = 0
        gendered_total =0
        json_=[]
        data_json={}
        sentinel=0
        female_count_by_week=0
        male_count_by_week = 0
        female_total_by_week=0
        male_total_by_week =0
        daysLeading = ""
        #json_gender_data = json.dumps(gender_data, indent=4, sort_keys=True)
        daysLeading_female = 0
        daysLeading_male =   0
        femaleSumByWeek = 0
        maleSumByWeek =0
        for gender_record in gender_data['output']:

             # sumo mas + fema si es > 0 proceso sino nada.
             if gender_record['female']['universe'] >= gender_record['male']['universe']:
                 daysLeading_female = daysLeading_female + 1
             if gender_record['male']['universe'] >= gender_record['female']['universe']:
                 daysLeading_male = daysLeading_male + 1

#             cvalue = gender_record['female']['count'] + gender_record['male']['count']
#             if cvalue!=2000:
             female_count = female_count +gender_record['female']['universe']
             male_count = male_count + gender_record['male']['universe']
             female_count_by_week = female_count_by_week +gender_record['female']['universe']
             male_count_by_week = male_count_by_week + gender_record['male']['universe']
             dayofweek = parser.parse(gender_record['date']).weekday()

             if dayofweek == 0:
                data_json={}
                female_total_by_week = float(female_count_by_week) / (float(female_count_by_week) + float(male_count_by_week))
                male_total_by_week = float(male_count_by_week) / (float(male_count_by_week) + float(female_count_by_week))
                #jaime
                data_json['date'] = gender_record['date']
              #  print data_json['date']
                data_json['femalebyWeek'] = female_total_by_week
                data_json['malebyWeek'] = male_total_by_week
                data_json['femaleSumByWeek'] =female_count_by_week
                data_json['maleSumByWeek'] = male_count_by_week
                json_.append(data_json)
                female_count_by_week=0
                male_count_by_week =0

        female_total = (float(female_count) /( float(female_count) + float(male_count))) * 100
        male_total = ( float(male_count) / (float(male_count) + float(female_count)) ) *100
        universe_total = language_data['query_meta']['total_universe']
        gendered_total =( (float(female_count) + float(male_count)) / float(universe_total) )*100
        data_json={}
       # json_gender_data = json.dumps(json_, indent=4, sort_keys=True)
       # print json_gender_data
        data_json['femaleUniverseavg'] = female_total
        data_json['famaleUniverse'] = female_count
        data_json['maleUniverse'] = male_count
        data_json['maleUniverseavg'] = male_total
        data_json['genderedUniverseavg']=gendered_total
        data_json['daysleadingfemale'] = daysLeading_female
        data_json['daysleadingmale'] = daysLeading_male
        json_.append(data_json)
        json_all = json.dumps(json_, indent=4, sort_keys=True)
        #print json_all
        json_ages_ = {}
        for age_key in age_data['output']['ranges']:
            json_ages_['display_name'] = age_key['display_name']
            json_ages_['probability'] = age_key['probability']
            json_.append(json_ages_)
            json_ages_ = {}

        doc_total =0
        income_json ={}
        for income_key in income_data["output"]["ranges"]:
              income_json = {}
              income_json["income_range"] = income_key["display_name"]
              income_json["value"] = income_key["probability"]
              json_.append(income_json)

        demographics_json ={}
        demographics_json['demo_value'] = demographics_data["output"]['db_median_disposable_income']
        #db_median_disposable_income

        ownership_json = {}
        for ownership_key in ownership_data["output"]:
            ownership_json = {}
            ownership_json["rent"] = ownership_key["display_name"]
            ownership_json["value"]= ownership_key["probability"]
            json_.append(ownership_json)

        household_json_ant ={}

        for household_key in household_data["output"]["ranges"]:
            household_json_ant = {}
            household_json_ant["display_name"] = household_key["id"]
            household_json_ant["value"] = household_key["probability"]

            json_.append(household_json_ant)

        education_json ={}
        value_education_lt=0
        value_education_h =0
        counter_mid_h = 0
        ks={}

        list_ =[]
        languages= set()
        counts=[]
        json_lan  = json.dumps(language_data, indent=4, sort_keys=True)
        #print json_lan
        for key_lang in language_data["output"]:
            for key_, value_ in key_lang.iteritems():
                if len(key_)==2:
                   languages.add(key_)

        l = list(languages)
        dic ={}
        for key_lang in language_data["output"]:
            for lang in l:
                for key,value in key_lang.iteritems():
                    if len(key) == 2:
                        #print key
                        dic[key.encode('utf-8')] = dic.get(key.encode('utf-8'), 0) + value

        json_language={}
        result = sorted(dic.items(), key=operator.itemgetter(1))
        result.reverse()
        total = sum(dic.values())

        #print  result[0][0] , result[0][1]
        try:
            json_language[result[0][0]] = (float(result[0][1]) / float( total ) ) *100
            json_language[result[1][0]] = (float(result[1][1]) / float(total)) * 100
            json_language[result[2][0]] = (float(result[2][1]) / float( total ) ) *100
            other = total - result[0][1] - result[1][1] -result[2][1]
            json_language['other'] = (float(other) / float( total ) ) *100
            json_.append(json_language)
        except :
            json_.append(json_language)
            pass

        

        household_json={}
        household_json["demographics_type"] = "household-income"
        household_json["demographics_value"] = float(
            demographics_data["output"]['median_household_income']) - float(
            demographics_data["output"]['db_median_household_income'])
        household_json["demographics_mean_value"] = demographics_data["output"]['median_household_income']
        json_.append(household_json)

        householdisposable={}
        householdisposable["demographics_type"] = "disposable-income"
        householdisposable["demographics_value"] = float(demographics_data["output"]['median_disposable_income'] )- float(demographics_data["output"]['db_median_disposable_income'] )
        householdisposable["demographics_mean_value"] = demographics_data["output"]['median_disposable_income']
        json_.append(householdisposable)

        householdsize = {}
        householdsize["demographics_type"] = "household-size"
        householdsize["demographics_value"] = float(demographics_data["output"]['average_household_size']) - float(demographics_data["output"]['db_average_household_size'])
        householdsize["demographics_mean_value"] = demographics_data["output"]['average_household_size']
        householdsize["demographics_sample_size"] = demographics_data["output"]['sample_size']
        json_.append(householdsize)

        householdvalue = {}
        householdvalue["demographics_type"] = "household-value"
        householdvalue["demographics_value"] = float(demographics_data["output"]['median_house_value']) - float(
            demographics_data["output"]['db_median_house_value'])
        householdvalue["demographics_mean_value"] = demographics_data["output"]['median_house_value']
        json_.append(householdvalue)

        education_any_college = float(demographics_data["output"]['education_any_college'])
        db_education_any_college = float(demographics_data["output"]['db_education_any_college'])
        education_json["demographics_type"] =  "higher-education"
        education_json["demographics_value"] = ((education_any_college - db_education_any_college) / db_education_any_college) * 100
        education_json["demographics_mean_value"] = demographics_data["output"]['education_any_college']
        json_.append(education_json)

        json_.append( {
            "last_updated_date": datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ'),
            "date_from": demographics_data['query_info']['start_date'],
            "date_to": demographics_data['query_info']['end_date']
        } )

        json_all = json.dumps(json_, indent=4, sort_keys=True)
        jsonfile = open(dirout, "w")
        jsonfile.write(json_all)
        jsonfile.close()

        print 'Whoistalkingaboutme'

    """What other topics do they care about?"""
    def Whatothtopicstheycareabout(self, path,dirout):
        postinterest,interests = path.split(",")
        with open(interests) as data_interests:
            interest_data = json.load(data_interests)

        with open(postinterest) as data_postinterest:
            data_post = json.load(data_postinterest)

        interest_sort_ratio = sorted(interest_data['output'],key=lambda d: d["ratio"],reverse=True)
        isize =len( list(interest_data['output']))
        irang =15
        interest_json=[]
        sum_t = 0
        if isize <=15:
            irang =isize
        for i_ in range(irang):
            interest_json.append(interest_sort_ratio[i_])
            sum_t +=interest_sort_ratio[i_]['ratio']

        #print  sum_t
        json_final ={}
        json_array =[]
        for y_ in interest_json:
           parcial = float(y_['ratio']) / float(sum_t) * 100
           json_final['name'] = y_['name']
           json_final['distribution'] =y_['distribution']
           json_final['ratio'] = y_['ratio']
           json_final['ratioPercentage'] = parcial

           json_final['last_updated_date'] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
           json_final['date_from'] = interest_data['query_info']['start_date']
           json_final['date_to'] = interest_data['query_info']['end_date']

           json_array.append(json_final)
           json_final={}
        icount =0
        for y_ in data_post['output']:
            if icount < 10:
                json_final['name'] = y_['name']
                json_final['distribution'] = y_['distribution']
                json_final['score'] = y_['score']

                json_final['last_updated_date'] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
                json_final['date_from'] = data_post['query_info']['start_date']
                json_final['date_to'] = data_post['query_info']['end_date']

                json_array.append(json_final)
                json_final = {}
                icount += 1

            else:
                break

        # interest.json
        # 1. ordenar por ratio el json descendentes y me quedo con los 10 primeros
        #   y de esos 10 paso el name y distribu. ratio  varnueva = Ratioper(x) = R(1) / R(0-9)

        # 2. post-interest.json
        #   tomo los primeros 10 y los vuelco al nuevo json sin computar.
        json_all = json.dumps(json_array, indent=4, sort_keys=True)
        jsonfile = open(dirout, "w")
        jsonfile.write(json_all)
        jsonfile.close()
        print 'Whatothtopicstheycareabout'

    """do feel like"""
    def Howpeoplefeelaboutbrand(self, path, dirout):
        feel,like,do = path.split(",")
        with open(do) as data_do:
            json_do = json.load(data_do)
        with open(feel) as data_feel:
            json_feel = json.load(data_feel)
        with open(like) as data_like:
            json_like = json.load(data_like)
            
            json_do_data = json.dumps(json_do['output'], indent=4, sort_keys=True)

            df_do = pd.read_json(json_do_data, dtype=object, encoding='utf-8')
            #jaimito
            df_do.replace(np.nan, 0, regex=True)
            df_do = df_do.head(40)
            df_do = df_do.groupby("topic").agg({"positive_appearances": np.sum, \
                                         "negative_appearances": np.sum, \
                                         "score":np.sum}).sort_values(by='score')
            df_do['distribution'] = (df_do["positive_appearances"] / (
                df_do["positive_appearances"] + df_do["negative_appearances"])) * 100
            df_do['size_color'] = 1 + ((df_do["score"] - np.min(df_do["score"])) / (
                (np.max(df_do["score"]) - np.min(df_do["score"])) * 0.1))
            # print df_feel
            top_dic = {}
            json_topics = []
            for k in df_do.index:
                #    break
                df_do.loc[k] = df_do.loc[k].fillna(0)
                size_color = int(df_do.loc[k]["size_color"])
                result = int(df_do.loc[k]["distribution"])
                top_dic["positive_appearances"] = df_do.loc[k]["positive_appearances"]
                top_dic["negative_appearances"] = df_do.loc[k]["negative_appearances"]
                top_dic["topic"] = k
                top_dic["score"] = df_do.loc[k]["score"]
                top_dic["option"] = "do"
                if size_color > 10:
                    size_color = 10

                if top_dic["positive_appearances"] == top_dic["negative_appearances"]:
                    color = 3
                elif result <= 25:
                    color = 1
                elif result <= 45 and result >= 25:
                    color = 2
                elif result <= 55 and result >= 45:
                    color = 3
                elif result <= 75 and result >= 55:
                    color = 4
                elif result <= 100 and result >= 75:
                    color = 5
                top_dic["color"] = color
                top_dic["size"] = size_color

                top_dic["last_updated_date"] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
                top_dic["date_from"] = json_do['query_info']['start_date']
                top_dic["date_to"] = json_do['query_info']['end_date']

                json_topics.append(top_dic)
                top_dic = {}

            json_feel_data = json.dumps(json_feel['output'], indent=4, sort_keys=True)
            df_feel = pd.read_json(json_feel_data, dtype=object, encoding='utf-8')

            df_feel.replace(np.nan, 0, regex=True)
            df_feel = df_feel.head(40)
            df_feel = df_feel.groupby("topic").agg({"positive_appearances": np.sum, \
                                               "negative_appearances": np.sum, \
                                               "score": np.sum}).sort_values(by='score')


            df_feel['distribution'] = (df_feel["positive_appearances"] / (
                df_feel["positive_appearances"] + df_feel["negative_appearances"])) * 100

            df_feel['size_color'] = 1 + ((df_feel["score"] - np.min(df_feel["score"])) / (
                (np.max(df_feel["score"]) - np.min(df_feel["score"])) * 0.1))

            for k in  df_feel.index:
            #    break
                df_feel.loc[k] = df_feel.loc[k].fillna(0)
                size_color = int(df_feel.loc[k]["size_color"])
                result = int(df_feel.loc[k]["distribution"])
                top_dic["positive_appearances"] = df_feel.loc[k]["positive_appearances"]
                top_dic["negative_appearances"] = df_feel.loc[k]["negative_appearances"]
                top_dic["topic"] = k
                top_dic["score"] = df_feel.loc[k]["score"]
                top_dic["option"] ="feel"
                if size_color > 10:
                    size_color = 10

                if top_dic["positive_appearances"] == top_dic["negative_appearances"]:
                    color = 3
                elif result <= 25:
                    color = 1
                elif result <= 45 and result >= 25:
                    color = 2
                elif result <= 55 and result >= 45:
                    color = 3
                elif result <= 75 and result >= 55:
                    color = 4
                elif result <= 100 and result >= 75:
                    color = 5
                top_dic["color"]=color
                top_dic["size"] =size_color

                top_dic["last_updated_date"] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
                top_dic["date_from"] = json_feel['query_info']['start_date']
                top_dic["date_to"] = json_feel['query_info']['end_date']

                json_topics.append(top_dic)
                top_dic={}


            json_like_data = json.dumps(json_like['output'], indent=4, sort_keys=True)
            df_like = pd.read_json(json_like_data, dtype=object, encoding='utf-8')


            df_like = df_like.head(40)
            df_like = df_like.groupby("topic").agg({"positive_appearances": np.sum, \
                                               "negative_appearances": np.sum, \
                                               "score": np.sum}).sort_values(by='score')


            # jaimexy

            df_like['distribution'] = (df_like["positive_appearances"] / (
                df_like["positive_appearances"] + df_like["negative_appearances"])) * 100


            df_like['size_color'] = 1 + ((df_like["score"] - np.min(df_like["score"])) / (
                (np.max(df_like["score"]) - np.min(df_like["score"])) * 0.1))
            # print df_feel
            df_like.replace(np.nan, 0, regex=True)

            for k in df_like.index:
                #    break
                size_color = int(df_like.loc[k]["size_color"])
                df_like.loc[k] =df_like.loc[k].fillna(0)
                result = int(df_like.loc[k]["distribution"])
                top_dic["positive_appearances"] = df_like.loc[k]["positive_appearances"]
                top_dic["negative_appearances"] = df_like.loc[k]["negative_appearances"]
                top_dic["topic"] = k
                top_dic["score"] = df_like.loc[k]["score"]
                top_dic["option"] = "like"
                if size_color > 10:
                    size_color = 10

                if top_dic["positive_appearances"] == top_dic["negative_appearances"]:
                    color = 3
                elif result <= 25:
                    color = 1
                elif result <= 45 and result >= 25:
                    color = 2
                elif result <= 55 and result >= 45:
                    color = 3
                elif result <= 75 and result >= 55:
                    color = 4
                elif result <= 100 and result >= 75:
                    color = 5
                top_dic["color"] = color
                top_dic["size"] = size_color

                top_dic["last_updated_date"] = datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
                top_dic["date_from"] = json_like['query_info']['start_date']
                top_dic["date_to"] = json_like['query_info']['end_date']

                json_topics.append(top_dic)
                top_dic = {}

        js_ = json.dumps(json_topics, indent=4, sort_keys=True)

        jsonfile = open(dirout, "w")
        jsonfile.write(js_)
        jsonfile.close()
        print 'WHowpeoplefeelaboutbrand'

    """"How do people feel about my brand?"""
    def Howpeoplefeelaboutbrand_(self, path):
        do, feel,like = path.split(",")

        with open(do) as data_do:
            json_do = json.load(data_do)
        with open(feel) as data_feel:
            json_feel = json.load(data_feel)
        with open(like) as data_like:
            json_like = json.load(data_like)

        dosortbyscore = sorted(json_do['output'], key=lambda d: d["score"], reverse=True)
        feelsortbyscore = sorted(json_feel['output'], key=lambda d: d["score"], reverse=True)
        likesortbyscore = sorted(json_like['output'], key=lambda d: d["score"], reverse=True)

        names={}
        names['dosortbyscore']=dosortbyscore
        names['feelsortbyscore'] =feelsortbyscore
        names['likesortbyscore'] =likesortbyscore
        option=0
        top_dic_do = {}
        json_topics_do = []
        for instances in names:
         max = names[instances].__getitem__(0)['score']
         restricted_do = names[instances][:50]
         min = restricted_do.__getitem__(restricted_do.__len__() - 1)['score']
         print max ,', ', option, ' ' ,'min',min
         for do in restricted_do:
             score =do['score']
             positive_appearances=do['positive_appearances']
             negative_appearances=do['negative_appearances']
             size_color =1+ (  (score  - min) /(( max - min ) * 0.1))
             result = ((positive_appearances) /(positive_appearances + negative_appearances))*100
             if size_color > 10:
                 size_color=10
             if result <=25:
                color =1
             if result <= 45 and result>=25:
                 color =2
             if result <= 55 and result>=45:
                color = 3
             if result <= 75 and result>=55:
                color = 4
             if result <= 100 and result>=75:
                color = 5
             top_dic_do['topics'] = do['topic']
             top_dic_do['score']  = do['score']
             top_dic_do['positive_appearances'] = do['positive_appearances']
             top_dic_do['negative_appearances'] = do['negative_appearances']
             top_dic_do['color'] = int(color)
             top_dic_do['size'] =int(math.ceil(size_color))
             if option ==0:
                top_dic_do['option'] ='do'
             if option ==1:
                 top_dic_do['option'] = 'like'
             if option == 2:
                 top_dic_do['option'] = 'feel'
             json_topics_do.append(top_dic_do)
             top_dic_do={}
         option=option + 1
         #print option


        json_ending = json.dumps(json_topics_do, indent=4, sort_keys=True)
        print json_ending
        print max ,'   ', min
    """What emotions do people have when talking about the category?"""
    def Whatemotionspeoplehavetalking(self, path,dirout):

        emotions_about_category = {}
        info = {}
        values = []

        with open(path) as data_file:
            data_emotions = json.load(data_file)
        data=[]
        c =0
        for js in data_emotions['output']:
            for iss in js['emotions']:
                data.append(iss)

        json_all = json.dumps(data, indent=4, sort_keys=True)
        df = pd.read_json(json_all,dtype=object,encoding='utf-8')
        df = df.groupby("name").agg({"documents": np.sum, "name": lambda x: x.nunique()})
        hf = df.groupby("documents").agg({"documents": np.sum}).sum()

        json_emot={}
        json_vect=[]

        # for each emotion (name)
        for name in df.index:
           values.append( { "type": name, "value": (float( df.loc[name]["documents"]) / float(hf) ) } )
           
        info = {
            "last_updated_date": datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ'),
            "date_from": data_emotions['query_info']['start_date'],
            "date_to": data_emotions['query_info']['end_date']
        }

        emotions_about_category = { "values" : values, "info" : info }

        json_n = json.dumps(emotions_about_category,indent=4)
        jsonfile = open(dirout, "w")
        jsonfile.write(json_n)
        jsonfile.close()
        print 'Whatemotionspeoplehavetalking'





























