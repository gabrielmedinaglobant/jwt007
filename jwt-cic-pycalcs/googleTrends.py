
from pytrends.request import TrendReq

import io
import json
import math
import logging as log
import re
import logging as info
#
#
import pandas as pd
import numpy as np
from datetime import datetime, date, timedelta

log.basicConfig(filename='/var/log/pycalcs/googleTrends.log',level=log.ERROR)
# in this level log i'll save info about of calc.
info.basicConfig(filename='/var/log/pycalcs/googleTrends.info.log',level=info.INFO)



class googleTrends:

    """ How much have people looked for me in comparison to competitors"""
    def Howmuchhavepeoplelooked(self, dic, dirout):
       try:
            pytrend = TrendReq(dic['google_username'], dic['google_password'], custom_useragent='Jwt-trends-pity')
            pytrend_ = TrendReq(dic['google_username'], dic['google_password'], custom_useragent='Jwt-trends-pity')

            pytrend.build_payload(kw_list=[dic['brand_1'],dic['brand_2'],dic['brand_3']],timeframe='today 3-m')
            pytrend_.build_payload(kw_list=[dic['brand_1'], dic['brand_2'], dic['brand_3']], timeframe='now 1-d')


            resp = pytrend.interest_over_time()
            resp=resp.replace(np.nan, '', regex=True)
            resp_ =pytrend_.interest_over_time()
            resp_=resp_.replace(np.nan, '', regex=True)
            resp_ = resp_.loc[resp_.index < date.today().strftime('%Y-%m-%d')].mean()

            brand =[]
            brand_json={}
            mobil, castrol , valvoline = dic['brand_1'],dic['brand_2'],dic['brand_3']
            yesterday = date.today() - timedelta(1)

            for key  in resp.index:
                brand_json['date']= key.strftime('%Y-%m-%d')
                brand_json[mobil]=resp.mobil[key]
                brand_json[castrol]=resp.castrol[key]
                brand_json[valvoline] = resp.valvoline[key]

                brand.append(brand_json)
                brand_json={}
            brand_json = {}


            brand_json['date'] = yesterday.strftime('%Y-%m-%d')


            brand_json[mobil] =math.ceil(resp_.mobil)
            brand_json[castrol] = math.ceil(resp_.castrol)
            brand_json[valvoline] =math.ceil(resp_.valvoline)
            brand.append(brand_json)
            brand_json = {}


            brand_json['mobil_max'],brand_json['mobil_min'] = max(item['mobil'] for item in brand),min(item['mobil'] for item in brand)
            brand_json['castrol_max'],brand_json['castrol_min'] = max(item['castrol'] for item in brand),min(item['castrol'] for item in brand)
            brand_json['valvoline_max'],brand_json['valvoline_min'] = max(item['valvoline'] for item in brand),min(item['valvoline'] for item in brand)

            brand_json['mobil_avg'] =float(sum(item['mobil'] for item in brand)) / len(brand)
            brand_json['castrol_avg'] =float(sum(item['castrol'] for item in brand)) / len(brand)
            brand_json['valvoline_avg'] = float(sum(item['valvoline'] for item in brand)) / len(brand)

            brand.append(brand_json)
            json_data = json.dumps(brand,sort_keys=True,indent=4)
            jsonfile = open(dirout, "w")
            jsonfile.write(json_data)
            jsonfile.close()
            #print json_data
            print "Howmuchhavepeoplelooked"

       except Exception as e:
           log.error("Error found in googleTrend :",e.message)


    """" this method bla bla bla """
    """ How much have people mentioned me in comparison to competitors?"""
    def HowmuchhavepeoplementionedG(self, tokefiles, dicty, dirout):

             print "****HowmuchhavepeoplementionedG****"
             print tokefiles
             print "---------"
             print dicty
             print "---------"
             print dirout
             print "---------"

             if not re.search(dicty['id'] , tokefiles):
                 log.error("Error in Query Order. The information is inconsistents")

             ArrayBrand =tokefiles.split(',')
             file_brand=[]

             for icFile in ArrayBrand:
                 try:
                    file = json.loads(open(icFile).read())
                    file_brand.append(file)
                    # I expect 5 files
                 except Exception as e:
                    print(e)
                    log.error(e.message)

             brand_dic = {}
             brand_name ={}
             brand_summary={}
             brand = []
             brantotal=[]
             bv=[]
             try:
             # process 1er brand
                jsonOneBrand = json.dumps(file_brand[0]['output'], indent=4, sort_keys=True)
                dfOneBrand = pd.read_json(jsonOneBrand, dtype=object, encoding='utf-8')
                dfsOneBrand = pd.to_datetime(dfOneBrand.date)
                dfOneBrand = dfOneBrand.set_index(dfsOneBrand)
                dfsOneBrand= dfOneBrand.groupby(pd.TimeGrouper("M"))
                pos = dfsOneBrand.posts_universe.min().tail(-1)
                ind = 1


                if len(pos) == 0:
                     ind = 1
                else:
                     ind = -1

                sumaOneBrand = dfsOneBrand['posts_universe'].apply(np.sum).tail(ind)
                minOneBrand = dfsOneBrand.posts_universe.min().tail(ind)
                maxOneBrand = dfsOneBrand.posts_universe.max().tail(ind)
                meanOneBrand = dfsOneBrand['posts_universe'].apply(np.mean).tail(ind)

                max_date_one =""
                min_date_one=""
                for n in file_brand[0]['output']:
                     brand_dic['date'] = n['date']
                     brand_dic[dicty['infegy-my-brand'] + '_posts_universe'] = n['posts_universe']
                     value = float(n['posts_universe'])
                     if value == float(maxOneBrand):
                         max_date_one = brand_dic['date']
                     if value == float(minOneBrand):
                        min_date_one = brand_dic['date']
                     brand.append(brand_dic)
                     brand_dic = {}

                for name in meanOneBrand.index:
                     brand_summary['date'] = name.strftime('%Y-%m')
                     brand_summary[dicty['infegy-my-brand'] + '_total'] = sumaOneBrand[name]
                     brand_summary[dicty['infegy-my-brand'] + '_average'] = meanOneBrand[name]
                     brand_summary[dicty['infegy-my-brand'] + '_date_max'] = max_date_one
                     brand_summary[dicty['infegy-my-brand'] + '_date_min'] = min_date_one
                     brand_summary[dicty['infegy-my-brand'] + '_highest'] = maxOneBrand[name]
                     brand_summary[dicty['infegy-my-brand'] + '_lowest'] = minOneBrand[name]
                     brantotal.append(brand_summary)
                     print "1"
                     brand_summary = {}
                 # 4
             except Exception as e:
                 print(e)
                 log.error(e.message)

             # brand2 --------------------------------------------------------
             try:
             # process 1er brand
                jsonSecondBrand = json.dumps(file_brand[1]['output'], indent=4, sort_keys=True)
                dfSecondBrand = pd.read_json(jsonSecondBrand, dtype=object, encoding='utf-8')
                dfsSecondBrand = pd.to_datetime(dfSecondBrand.date)
                dfSecondBrand = dfSecondBrand.set_index(dfsSecondBrand)
                dfsSecondBrand= dfSecondBrand.groupby(pd.TimeGrouper("M"))
                pos = dfsSecondBrand.posts_universe.min().tail(-1)
                ind = 1


                if len(pos) == 0:
                     ind = 1
                else:
                     ind = -1

                sumaSecondBrand = dfsSecondBrand['posts_universe'].apply(np.sum).tail(ind)
                minSecondBrand = dfsSecondBrand.posts_universe.min().tail(ind)
                maxSecondBrand = dfsSecondBrand.posts_universe.max().tail(ind)
                meanSecondBrand = dfsSecondBrand['posts_universe'].apply(np.mean).tail(ind)

                max_date_second=""
                min_date_second = ""
                for n in file_brand[1]['output']:
                     brand_dic['date'] = n['date']
                     brand_dic[dicty['infegy-competitor-1'] + '_posts_universe'] = n['posts_universe']
                     value = float(n['posts_universe'])
                     if value == float(maxSecondBrand):
                         max_date_second = brand_dic['date']
                     if value == float(minSecondBrand):
                         min_date_second = brand_dic['date']
                     brand.append(brand_dic)
                     brand_dic = {}

                for name in meanSecondBrand.index:
                     brand_summary['date'] = name.strftime('%Y-%m')
                     brand_summary[dicty['infegy-competitor-1'] + '_total'] = sumaSecondBrand[name]
                     brand_summary[dicty['infegy-competitor-1'] + '_average'] = meanSecondBrand[name]
                     brand_summary[dicty['infegy-competitor-1'] + '_date_max'] = max_date_second
                     brand_summary[dicty['infegy-competitor-1'] + '_date_min'] = min_date_second
                     brand_summary[dicty['infegy-competitor-1'] + '_highest'] = maxSecondBrand[name]
                     brand_summary[dicty['infegy-competitor-1'] + '_lowest'] = minSecondBrand[name]
                     brantotal.append(brand_summary)
                     print "1"
                     brand_summary = {}
                 # 4
             except Exception as e:
                 print(e)
                 log.error(e.message)

             try:
             # process 1er brand
                jsonThreeBrand = json.dumps(file_brand[2]['output'], indent=4, sort_keys=True)
                dfThreeBrand = pd.read_json(jsonThreeBrand, dtype=object, encoding='utf-8')
                dfsThreeBrand = pd.to_datetime(dfThreeBrand.date)
                dfThreeBrand = dfThreeBrand.set_index(dfsThreeBrand)
                dfsThreeBrand= dfThreeBrand.groupby(pd.TimeGrouper("M"))
                pos = dfsThreeBrand.posts_universe.min().tail(-1)
                ind = 1


                if len(pos) == 0:
                     ind = 1
                else:
                     ind = -1

                sumaThreeBrand = dfsThreeBrand['posts_universe'].apply(np.sum).tail(ind)
                minThreeBrand = dfsThreeBrand.posts_universe.min().tail(ind)
                maxThreeBrand = dfsThreeBrand.posts_universe.max().tail(ind)
                meanThreeBrand = dfsThreeBrand['posts_universe'].apply(np.mean).tail(ind)

                max_date_three=""
                min_date_three = ""
                for n in file_brand[2]['output']:
                     brand_dic['date'] = n['date']
                     brand_dic[dicty['infegy-competitor-2'] + '_posts_universe'] = n['posts_universe']
                     value = float(n['posts_universe'])
                     if value == float(maxThreeBrand):
                         max_date_three = brand_dic['date']
                     if value == float(minThreeBrand):
                         min_date_three = brand_dic['date']
                     brand.append(brand_dic)
                     brand_dic = {}

                for name in meanThreeBrand.index:
                     brand_summary['date'] = name.strftime('%Y-%m')
                     brand_summary[dicty['infegy-competitor-2'] + '_total'] = sumaThreeBrand[name]
                     brand_summary[dicty['infegy-competitor-2'] + '_average'] = meanThreeBrand[name]
                     brand_summary[dicty['infegy-competitor-2'] + '_date_max'] = max_date_three
                     brand_summary[dicty['infegy-competitor-2'] + '_date_min'] = min_date_three
                     brand_summary[dicty['infegy-competitor-2'] + '_highest'] = maxThreeBrand[name]
                     brand_summary[dicty['infegy-competitor-2'] + '_lowest'] = minThreeBrand[name]
                     brantotal.append(brand_summary)
                     print "3"
                     brand_summary = {}
                 # 4
             except Exception as e:
                 print(e)
                 log.error(e.message)

             try:
             # process 1er brand
                jsonFourBrand = json.dumps(file_brand[3]['output'], indent=4, sort_keys=True)
                dfFourBrand = pd.read_json(jsonFourBrand, dtype=object, encoding='utf-8')
                dfsFourBrand = pd.to_datetime(dfFourBrand.date)
                dfFourBrand = dfFourBrand.set_index(dfsFourBrand)
                dfsFourBrand= dfFourBrand.groupby(pd.TimeGrouper("M"))
                pos = dfsFourBrand.posts_universe.min().tail(-1)
                ind = 1


                if len(pos) == 0:
                     ind = 1
                else:
                     ind = -1

                sumaFourBrand = dfsFourBrand['posts_universe'].apply(np.sum).tail(ind)
                minFourBrand = dfsFourBrand.posts_universe.min().tail(ind)
                maxFourBrand = dfsFourBrand.posts_universe.max().tail(ind)
                meanFourBrand = dfsFourBrand['posts_universe'].apply(np.mean).tail(ind)

                max_date_four=""
                min_date_four=""
                for n in file_brand[3]['output']:
                     brand_dic['date'] = n['date']
                     brand_dic[dicty['infegy-competitor-3'] + '_posts_universe'] = n['posts_universe']
                     value = float(n['posts_universe'])
                     if value == float(maxFourBrand):
                         max_date_four = brand_dic['date']
                     if value == float(minFourBrand):
                         min_date_four = brand_dic['date']
                     brand.append(brand_dic)
                     brand_dic = {}

                for name in meanFourBrand.index:
                     brand_summary['date'] = name.strftime('%Y-%m')
                     brand_summary[dicty['infegy-competitor-3'] + '_total'] = sumaFourBrand[name]
                     brand_summary[dicty['infegy-competitor-3'] + '_average'] = meanFourBrand[name]
                     brand_summary[dicty['infegy-competitor-3'] + '_date_max'] = max_date_four
                     brand_summary[dicty['infegy-competitor-3'] + '_date_min'] = min_date_four
                     brand_summary[dicty['infegy-competitor-3'] + '_highest'] = maxFourBrand[name]
                     brand_summary[dicty['infegy-competitor-3'] + '_lowest'] = minFourBrand[name]
                     brantotal.append(brand_summary)
                     print "4"
                     brand_summary = {}
                 # 4
             except Exception as e:
                 print(e)
                 log.error(e.message)

             try:
             # process 1er brand
                jsonFiveBrand = json.dumps(file_brand[4]['output'], indent=4, sort_keys=True)
                dfFiveBrand = pd.read_json(jsonFiveBrand, dtype=object, encoding='utf-8')
                dfsFiveBrand = pd.to_datetime(dfFiveBrand.date)
                dfFiveBrand = dfFiveBrand.set_index(dfsFiveBrand)
                dfsFiveBrand= dfFiveBrand.groupby(pd.TimeGrouper("M"))
                pos = dfsFiveBrand.posts_universe.min().tail(-1)
                ind = 1


                if len(pos) == 0:
                     ind = 1
                else:
                     ind = -1

                sumaFiveBrand = dfsFiveBrand['posts_universe'].apply(np.sum).tail(ind)
                minFiveBrand = dfsFiveBrand.posts_universe.min().tail(ind)
                maxFiveBrand = dfsFiveBrand.posts_universe.max().tail(ind)
                meanFiveBrand = dfsFiveBrand['posts_universe'].apply(np.mean).tail(ind)

                max_date_five=""
                min_date_five = ""
                for n in file_brand[4]['output']:
                     brand_dic['date'] = n['date']
                     brand_dic[dicty['infegy-competitor-4'] + '_posts_universe'] = n['posts_universe']
                     value = float(n['posts_universe'])
                     if value == float(maxFiveBrand):
                         max_date_five = brand_dic['date']
                     if value == float(minFiveBrand):
                         min_date_five = brand_dic['date']
                     brand.append(brand_dic)
                     brand_dic = {}

                for name in meanFiveBrand.index:
                     brand_summary['date'] = name.strftime('%Y-%m')
                     brand_summary[dicty['infegy-competitor-4'] + '_total'] = sumaFiveBrand[name]
                     brand_summary[dicty['infegy-competitor-4'] + '_average'] = meanFiveBrand[name]
                     brand_summary[dicty['infegy-competitor-4'] + '_date_max'] = max_date_five
                     brand_summary[dicty['infegy-competitor-4'] + '_date_min'] = min_date_five
                     brand_summary[dicty['infegy-competitor-4'] + '_highest'] = maxFiveBrand[name]
                     brand_summary[dicty['infegy-competitor-4'] + '_lowest'] = minFiveBrand[name]
                     print "5"
                     brantotal.append(brand_summary)
                     brand_summary = {}
                 # 4
             except Exception as e:
                 log.error(e.message)

             dict_ = {
                 'info': {
                    'brand': dicty['infegy-my-brand'],
                    'last_updated_date': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
                    'date_from': file_brand[0]['query_info']['start_date'],
                    'date_to': file_brand[0]['query_info']['end_date']
                 },
                 'daily': brand,
                 'summary': brantotal
             }
             json_data = json.dumps(dict_, sort_keys=True, indent=4)

             jsonfile = open(dirout, "w")
             jsonfile.write(json_data)
             jsonfile.close()
             print "OK - Howmuchhavepeoplementioned"

    """ GoogleTends once exe per hour"""
    """  """
    def Whatarepeopletalkingaboutoverall(self, dic,dirout):

        try:

           print "happy 1"
           pytrend = TrendReq(dic['google_username'], dic['google_password'], custom_useragent='Jwt-trends-pity')
           print "happy 2"
           c = math.ceil( np.asscalar(np.random.rand(1, 10000).sum()) )
           print "happy 2.1"
           rending_searches_df = pytrend.trending_searches()
           print "happy 2.2"
           trend = {}
           print "happy 3"
           for i in rending_searches_df['title'].index:
               trend[i] = rending_searches_df['title'][i]
               print type(trend[i])
           json_ = json.dumps(trend, indent=4, sort_keys=True)
           with io.open(dirout, 'w', encoding='utf-8') as f:
              f.write(unicode(json_))
           f.close()
           print "OK - Whatarepeopletalkingaboutoverall"
           print "END OF PROC"
        except Exception as e:
            print e.message
            log.error(e.message)

