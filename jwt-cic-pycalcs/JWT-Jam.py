import ParsingUtils as utils
import pyCommon as process
import logging as log
import os
import ConfigParser
import subprocess
from subprocess import Popen,PIPE,STDOUT,call

log.basicConfig(filename='/var/log/JWT-batch.log', level=log.ERROR)

""" this main method """
if __name__ == '__main__':
    try:
      """ ARGS """
      ut = utils.Utils()
      # python jwt-main -s DOWNLOAD-X
      cmd = ut.parameters()
      pr = process.pyAwsNetCommon()
      batch = pr.getDictonarySection(cmd[0])

      downloadCmdStr = 'python ' + batch['path'] + batch['cmd_1'] + ' ' + batch['download_parameters']
      mainCmdStr = 'python ' + batch['path']+batch['cmd_2']+' '+batch['parameters']
      uploadCmdStr = 'python ' + batch['path'] + batch['upload_cmd'] + '  ' + batch['cmd_3'] + '  ' + batch['cmd_4'] + '  ' + batch['cmd_5']

      print("JWT - CIC - DATA FETCHING & CALULATION JOB")

      print("EXECUTING DATA FETCHING (download.py) AS: ")
      print(downloadCmdStr)
      r = subprocess.call(downloadCmdStr, shell=True )
      if r == 0:
         print("EXECUTING CALCULATIONS (jwt-main.py) AS: ")
         print(mainCmdStr)
         p = subprocess.call(mainCmdStr, shell=True)
         if p ==0:
            print("EXECUTING UPLOAD TO S3 (uploads3.py) AS:")
            print(uploadCmdStr)
            rc = subprocess.call(uploadCmdStr, shell=True)
    except Exception as e:
        log.error(e.message)
