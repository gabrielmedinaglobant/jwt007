import ConfigParser
import sys

import requests
import json
import boto
from boto.s3.connection import S3Connection
import logging as log

# download information of infegy - crowdtangle - google - trend a and it's leave
# in input directory before to process
# in this clase we are found diferents methods for connect

log.basicConfig(filename='/var/log/JWT.log',level=log.ERROR)

class pyAwsNetCommon:

    def ProcessURL(self,uri):
         try:
            response = requests.get(uri)
            data = response.json()
            json_list = json.dumps(data, indent=4, sort_keys=True)
         except Exception as e:
             log.error(e.message)
             raise Exception(e.message)
    def Uploadfiles(self,path,file):
        try:
             key = self.getURLJSON('AWS', 'key')
             value = self.getURLJSON('AWS', 'value')
             bucket_aws = self.getURLJSON('AWS', 'bucket')
             conn = S3Connection(key, value)
             bucket = conn.get_bucket(bucket_aws)
             #for key in bucket.list():
             #    key.delete()
             k = bucket.new_key(file)
             k.set_contents_from_filename(path)
             for iy in bucket.list():
                  print iy
        except Exception, e:
             log.error(e.message)
             raise Exception(e.message)
    def getURLJSON(self,section , ask):
        # initializate ini parser
        try:
            Config = ConfigParser.ConfigParser()
            Config.read('./json.ini')
            path = Config.get(section, ask)
            return path
        except ConfigParser.Error as e:
            print (str(e))
            log.error(e)
            sys.exit(2)

    def getDictonarySection(self, section):
        # initializate ini parser
        try:
            Config = ConfigParser.ConfigParser()
            #after that I will change removing path
            Config.read('./json.ini')
            dict1 ={}
            options = Config.options(section)
            for option in options:
              try:
                 dict1[option] = Config.get(section, option)
                 if dict1[option] == -1:
                     print ("skip: %s" % option)
              except:
                 print("exception on %s!" % option)
                 dict1[option] = None
            return dict1
        except ConfigParser.Error as e:
            print (str(e))
            log.error(e)
            sys.exit(2)