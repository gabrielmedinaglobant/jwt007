import sys
import getopt
import csv
import json

class Utils:
    def read_csv(self,file):
        csv_rows = []
        with open(file) as csvfile:
            reader = csv.DictReader(csvfile)
            title = reader.fieldnames
            for row in reader:
                csv_rows.extend([{title[i]: row[title[i]] for i in range(len(title))}])
        return  self.convert_json(csv_rows)
    # Convert csv data into json and write it
    def convert_json(self,data):
                 json.dumps(data, sort_keys=False, indent=4, separators=(',', ': '), encoding="utf-8",
                                   ensure_ascii=False)
                 return data

    def parameters(self):
        cmd =[]
        try:
            myopts, args = getopt.getopt(sys.argv[1:], "s:c:o:")
        except getopt.GetoptError as e:
            print (str(e))
            print("Usage: %s -s section -a question" % sys.argv[0])
            sys.exit(2)

        for o, a in myopts:
            if o == '-s':
                cmd.append(a)
            if o == '-c':
                cmd.append(a)
            elif o == '-o':
                cmd.append(a)

              # Display input and output file name passed as the args
        if len(myopts) <2:
            print("Usage: %s -s section -a type" % sys.argv[0])
            sys.exit(2)
        return cmd