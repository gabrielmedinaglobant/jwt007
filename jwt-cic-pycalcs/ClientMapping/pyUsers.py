import ClientMapping.dao.daouser as user

import ParsingUtils as utils
import pyCommon as process

if __name__ == '__main__':
    ut = utils.Utils()
    pr = process.pyAwsNetCommon()
    cmd = ut.parameters()
    path = pr.getValuesDB(cmd[0], cmd[2])
    file = pr.getValuesDB(cmd[0],cmd[1])
    user = user.daouser()
    json_converted =ut.read_csv(file)
    user.insert(path,json_converted)
