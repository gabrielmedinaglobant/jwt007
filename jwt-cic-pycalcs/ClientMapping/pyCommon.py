import ConfigParser
import json
import math
import sys

import requests
import json
import boto
from boto.s3.connection import S3Connection
import logging as log


log.basicConfig(filename='mysql.log',level=log.ERROR)

class pyAwsNetCommon:


    def getValuesDB(self,section , ask):
        # initializate ini parser
        try:
            Config = ConfigParser.ConfigParser()
            Config.read('db.ini')
            path = Config.get(section, ask)
            return path
        except ConfigParser.Error as e:
            print (str(e))
            log.error(e)
            sys.exit(2)
