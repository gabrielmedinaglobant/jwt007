import sys
import getopt

class Utils:

    def parameters(self):
        cmd =[]
        try:
            myopts, args = getopt.getopt(sys.argv[1:], "s:a:o:p:")
        except getopt.GetoptError as e:
            print (str(e))
            print("Usage: %s -s section -a question" % sys.argv[0])
            sys.exit(2)

        for o, a in myopts:
            if o == '-s':
               cmd.append(a)
             # Display input and output file name passed as the args
        if len(myopts) <1:
            print("Usage: %s -s section -a type" % sys.argv[0])
            sys.exit(2)
        return cmd