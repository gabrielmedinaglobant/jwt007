import json
import math
import operator
import pyCommon as process

import simplejson
from decimal import Decimal

import logging as log
import logging as info
import re
from datetime import datetime
from dateutil import parser
#
#
import pandas as pd
import numpy as np

import itertools
log.basicConfig(filename='JWT.log',level=log.ERROR)
# in this level log i'll save info about of calc.
info.basicConfig(filename='information.log',level=info.INFO)

class infegy:

    # What brands are people talking about and how do they feel about them?
    # the firt question
    def brandspeopletalkingabout_(self , path):
        with open(path) as data_file:
            data = json.load(data_file)
        json_data = json.dumps(data['output'], indent=4, sort_keys=True)
        df = pd.read_json(json_data, dtype=object, encoding='utf-8')

        dfs = pd.to_datetime(df.date)
        df = df.set_index(dfs)
        dfs_ = df.groupby(pd.TimeGrouper("7D"))
        document_json = []
        document_dict = {}
        document_summary={}
        mixed_documents_dist = ((df.groupby("mixed_documents").
                                    agg({"mixed_documents": np.sum}).apply(sum))['mixed_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).\
                                    apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).\
                                    apply(sum)['mixed_documents']) * 100

        mixed_sentences_dist = ((df.groupby("mixed_sentences").
                                        agg({"mixed_sentences": np.sum}).apply(sum))['mixed_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).\
                                        apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).\
                                        apply(sum)['mixed_sentences']) * 100

        mixed_subject_sentences_dist = ((df.groupby("mixed_subject_sentences").
                                    agg({"mixed_subject_sentences": np.sum}).apply(sum))['mixed_subject_sentences']) / float(
                                    df.groupby("positive_subject_sentences").
                                    agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                    df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}).\
                                    apply(sum)['negative_subject_sentences'] + \
                                    df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}).\
                                    apply(sum)['mixed_subject_sentences']) * 100

        #end mixed
        # begin positive document
        positive_sentences_dist =((df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum))['positive_sentences'])/float(df.groupby("positive_sentences").
                                  agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences']+\
                                  df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences']+\
                                  df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        positive_documents_dist = ((df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum))['positive_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        positve_subject_sentences_dist = ((df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum))['positive_subject_sentences']) / float(
                                        df.groupby("positive_subject_sentences").
                                        agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                        df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                        apply(sum)['negative_subject_sentences'] + \
                                        df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                        apply(sum)['mixed_subject_sentences']) * 100

         #end positive documents
        negative_sentences_dist = ((df.groupby("negative_sentences").
                                        agg({"negative_sentences": np.sum}).apply(sum))['negative_sentences']) / float(
                                        df.groupby("positive_sentences").
                                        agg({"positive_sentences": np.sum}).apply(sum)['positive_sentences'] + \
                                        df.groupby("negative_sentences").agg({"negative_sentences": np.sum}).apply(sum)['negative_sentences'] + \
                                        df.groupby("mixed_sentences").agg({"mixed_sentences": np.sum}).apply(sum)['mixed_sentences']) * 100

        negative_documents_dist = ((df.groupby("negative_documents").
                                    agg({"negative_documents": np.sum}).apply(sum))['negative_documents']) / float(
                                    df.groupby("positive_documents").
                                    agg({"positive_documents": np.sum}).apply(sum)['positive_documents'] + \
                                    df.groupby("negative_documents").agg({"negative_documents": np.sum}).apply(sum)['negative_documents'] + \
                                    df.groupby("mixed_documents").agg({"mixed_documents": np.sum}).apply(sum)['mixed_documents']) * 100

        negative_subject_sentences_dist = ((df.groupby("negative_subject_sentences").
                                           agg({"negative_subject_sentences": np.sum}).apply(sum))[
                                              'negative_subject_sentences']) / float(
                                           df.groupby("positive_subject_sentences").
                                           agg({"positive_subject_sentences": np.sum}).apply(sum)['positive_subject_sentences'] + \
                                           df.groupby("negative_subject_sentences").agg({"negative_subject_sentences": np.sum}). \
                                           apply(sum)['negative_subject_sentences'] + \
                                           df.groupby("mixed_subject_sentences").agg({"mixed_subject_sentences": np.sum}). \
                                           apply(sum)['mixed_subject_sentences']) * 100

        document_summary['mixed_documents_prom'] =mixed_documents_dist
        document_summary['mixed_sentences_prom'] =mixed_sentences_dist
        document_summary['mixed_subject_sentences_prom'] =mixed_subject_sentences_dist
        document_summary['negative_documents_prom'] =negative_documents_dist
        document_summary['negative_sentences_prom'] =negative_sentences_dist
        document_summary['negative_subject_sentences_prom'] =negative_subject_sentences_dist
        document_summary['positive_documents_prom'] =positive_documents_dist
        document_summary['positive_sentences_prom'] =positive_sentences_dist
        document_summary['positive_subject_sentences_prom'] =positve_subject_sentences_dist

        # positive sentences
        positive_sentences = (dfs_['positive_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        negative_sentences=  (dfs_['negative_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        mixed_sentences =  (dfs_['mixed_sentences'].apply(np.sum) / ( dfs_['positive_sentences'].apply(np.sum) +dfs_['negative_sentences'].apply(np.sum) +dfs_['mixed_sentences'].apply(np.sum))) *100
        sentences_weekly_diff =np.abs(positive_sentences - negative_sentences)
        # positive documents
        positive_documents =(dfs_['positive_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        negative_documents =(dfs_['negative_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        mixed_documents =(dfs_['mixed_documents'].apply(np.sum) / ( dfs_['positive_documents'].apply(np.sum) +dfs_['negative_documents'].apply(np.sum) +dfs_['mixed_documents'].apply(np.sum))) *100
        documents_weekly_diff = np.abs(positive_documents - negative_documents)
        # positive subject sentences
        positive_subject_sentences = (dfs_['positive_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        negative_subject_sentences = (dfs_['negative_subject_sentences'].apply(np.sum) / ( dfs_['positive_subject_sentences'].apply(np.sum) +dfs_['negative_subject_sentences'].apply(np.sum) +dfs_['mixed_subject_sentences'].apply(np.sum))) *100
        mixed_subject_sentences = (dfs_['mixed_subject_sentences'].apply(np.sum) / (dfs_['positive_subject_sentences'].apply(np.sum) + dfs_['negative_subject_sentences'].apply(np.sum) + dfs_['mixed_subject_sentences'].apply(np.sum))) * 100
        subject_sentences_weekly_diff = np.abs(positive_subject_sentences - negative_subject_sentences)


        for a,b,c,d,e,f,g,h,i,j,k,y in  itertools.izip(positive_sentences.index,
                                     negative_sentences.index,
                                     mixed_sentences.index,
                                     sentences_weekly_diff.index,
                                     positive_documents.index,
                                     negative_documents.index,
                                     mixed_documents.index,
                                     documents_weekly_diff.index,
                                     positive_subject_sentences.index,
                                     negative_subject_sentences.index,
                                     mixed_subject_sentences.index,
                                     subject_sentences_weekly_diff.index
                                     ):
            document_dict['date']=a.strftime('%Y-%m-%d')
            document_dict['sentences_positivity'] = positive_sentences[a]
            document_dict['sentences_negativity'] = negative_sentences[b]
            document_dict['mixed_sentences'] = mixed_sentences[c]
            document_dict['sentences_weekly_diff'] = sentences_weekly_diff[d]
            document_dict['documents_positivity'] = positive_documents[e]
            document_dict['documents_negativity'] = negative_documents[f]
            document_dict['mixed_documents'] = mixed_documents[g]
            document_dict['documents_weekly_diff'] = documents_weekly_diff[h]
            document_dict['subject_sentences_positivity'] = positive_subject_sentences[i]
            document_dict['subject_sentences_negativity'] = negative_subject_sentences[j]
            document_dict['mixed_subject_sentences'] = mixed_subject_sentences[k]
            document_dict['subject_sentences_weekly_diff'] = subject_sentences_weekly_diff[y]
            document_json.append(document_dict)
            document_dict = {}
        document_json.append(document_summary)
        document_json = json.dumps(document_json,indent=4,sort_keys=True)
        print document_json


    def brandspeopletalkingabout(self , path):

            with open(path) as data_file:
                data = json.load(data_file)
            positive_documents_acum = 0
            negative_documents_acum = 0
            mixed_documents_acum = 0

            positive_sentences_acum =0
            negative_sentences_acum =0
            mixed_sentences_acum =0

            positive_subject_sentences_acum=0
            negative_subject_sentences_acum =0
            mixed_subject_sentences_acum =0


            sentinel =0
            jsonbuild = []

            indice=0
            data_json = {}
            contrl=6

            positive_documents_acum_general = 0
            negative_documents_acum_general = 0
            mixed_documents_acum_general = 0

            positive_sentences_acum_general = 0
            negative_sentences_acum_general = 0
            mixed_sentences_acum_general = 0

            positive_subject_sentences_acum_general = 0
            negative_subject_sentences_acum_general = 0
            mixed_subject_sentences_acum_general = 0

            for sentiment in data['output']:
                data_json = {}

                positive_sentences_acum =  positive_sentences_acum + sentiment['positive_sentences']
                negative_sentences_acum =  negative_sentences_acum + sentiment['negative_sentences']
                mixed_sentences_acum =     mixed_sentences_acum + sentiment['mixed_sentences']

                # general
                positive_sentences_acum_general = positive_sentences_acum_general + sentiment['positive_sentences']
                negative_sentences_acum_general = negative_sentences_acum_general + sentiment['negative_sentences']
                mixed_sentences_acum_general = mixed_sentences_acum_general + sentiment['mixed_sentences']
                # general

                positive_documents_acum = positive_documents_acum + sentiment['positive_documents']
                negative_documents_acum = negative_documents_acum + sentiment['negative_documents']
                mixed_documents_acum = mixed_documents_acum + sentiment['mixed_documents']

                positive_documents_acum_general = positive_documents_acum_general + sentiment['positive_documents']
                negative_documents_acum_general = negative_documents_acum_general + sentiment['negative_documents']
                mixed_documents_acum_general = mixed_documents_acum_general + sentiment['mixed_documents']


                positive_subject_sentences_acum = positive_subject_sentences_acum + sentiment['positive_subject_sentences']
                negative_subject_sentences_acum = negative_subject_sentences_acum + sentiment['negative_subject_sentences']
                mixed_subject_sentences_acum = mixed_subject_sentences_acum + sentiment['mixed_subject_sentences']

                positive_subject_sentences_acum_general = positive_subject_sentences_acum_general + sentiment[
                    'positive_subject_sentences']
                negative_subject_sentences_acum_general = negative_subject_sentences_acum_general + sentiment[
                    'negative_subject_sentences']
                mixed_subject_sentences_acum_general = mixed_subject_sentences_acum_general + sentiment['mixed_subject_sentences']

                if indice == contrl :

                     data_json['date'] = sentiment['date']
                     positive_documents_prom = (float(positive_documents_acum) / (
                     float(positive_documents_acum) + float(negative_documents_acum) + float(
                         mixed_documents_acum))) * 100
                     negative_documents_prom = (float(negative_documents_acum) / (
                     float(negative_documents_acum) + float(positive_documents_acum) + float(
                         mixed_documents_acum))) * 100
                     mixed_documents_prom = (float(mixed_documents_acum) / (
                     float(positive_documents_acum) + float(negative_documents_acum) + float(
                         mixed_documents_acum))) * 100

                     week_document_diff = positive_documents_prom - negative_documents_prom

                     positive_sentences_prom = (float(positive_sentences_acum) / (
                     float(positive_sentences_acum) + float(negative_sentences_acum) + float(
                         mixed_sentences_acum))) * 100

                     negative_sentences_prom = (float(negative_sentences_acum) / (
                     float(negative_sentences_acum) + float(positive_sentences_acum) + float(
                         mixed_sentences_acum))) * 100
                     mixed_sentences_prom = (float(mixed_sentences_acum) / (
                     float(positive_sentences_acum) + float(negative_sentences_acum) + float(
                         mixed_sentences_acum))) * 100

                     week_sentence_diff = positive_sentences_prom - negative_sentences_prom

                     positive_subject_sentences_prom = (float(positive_subject_sentences_acum) / (
                     float(positive_subject_sentences_acum) + float(negative_subject_sentences_acum) + float(
                         mixed_subject_sentences_acum))) * 100
                     negative_subject_sentences_prom = (float(negative_subject_sentences_acum) / (
                     float(positive_subject_sentences_acum) + float(negative_subject_sentences_acum) + float(
                         mixed_subject_sentences_acum))) * 100
                     mixed_subject_sentences_prom = (float(mixed_subject_sentences_acum) / (
                     float(positive_subject_sentences_acum) + float(negative_subject_sentences_acum) + float(
                         mixed_subject_sentences_acum))) * 100

                     week_subject_diff  =positive_subject_sentences_prom -negative_subject_sentences_prom

                     data_json['documents_positivity'] = positive_documents_prom
                     data_json['documents_negativity'] = negative_documents_prom
                     data_json['mixed_documents'] = mixed_documents_prom

                     data_json['sentences_positivity'] = positive_sentences_prom
                     data_json['sentences_negativity'] = negative_sentences_prom
                     data_json['mixed_sentences'] = mixed_sentences_prom


                     data_json['subject_sentences_positivity'] = positive_subject_sentences_prom
                     data_json['subject_sentences_negativity'] = negative_subject_sentences_prom
                     data_json['mixed_subject_sentences'] = mixed_subject_sentences_prom

                     data_json['sentences_weekly_diff'] = week_sentence_diff
                     data_json['documents_weekly_diff'] = week_document_diff
                     data_json['subject_sentences_weekly_diff'] = week_subject_diff

                     positive_sentences_acum = 0
                     negative_sentences_acum = 0
                     mixed_sentences_acum = 0
                     positive_documents_acum = 0
                     negative_documents_acum = 0
                     mixed_documents_acum=0
                     positive_subject_sentences_acum = 0
                     negative_subject_sentences_acum = 0
                     mixed_subject_sentences_acum = 0
                     indice=0
                     contrl=7
                     jsonbuild.append(data_json)
                indice = indice + 1

            data_json={}
            positive_documents_prom_general = (float(positive_documents_acum_general) / (
                float(positive_documents_acum_general) + float(negative_documents_acum_general) + float(
                    mixed_documents_acum_general))) * 100

            negative_documents_prom_general = (float(negative_documents_acum_general) / (
                float(negative_documents_acum_general) + float(positive_documents_acum_general) + float(
                    mixed_documents_acum_general))) * 100
            mixed_documents_prom_general = (float(mixed_documents_acum_general) / (
                float(positive_documents_acum_general) + float(negative_documents_acum_general) + float(
                    mixed_documents_acum_general))) * 100

            positive_sentences_prom_general = (float(positive_sentences_acum_general) / (
                float(positive_sentences_acum_general) + float(negative_sentences_acum_general) + float(
                    mixed_sentences_acum_general))) * 100

            negative_sentences_prom_general = (float(negative_sentences_acum_general) / (
                float(negative_sentences_acum_general) + float(positive_sentences_acum_general) + float(
                    mixed_sentences_acum_general))) * 100
            mixed_sentences_prom_general = (float(mixed_sentences_acum_general) / (
                float(positive_sentences_acum_general) + float(negative_sentences_acum_general) + float(
                    mixed_sentences_acum_general))) * 100

            positive_subject_sentences_prom_general = (float(positive_subject_sentences_acum_general) / (
                float(positive_subject_sentences_acum_general) + float(negative_subject_sentences_acum_general) + float(
                    mixed_subject_sentences_acum_general))) * 100
            negative_subject_sentences_prom_general = (float(negative_subject_sentences_acum_general) / (
                float(positive_subject_sentences_acum_general) + float(negative_subject_sentences_acum_general) + float(
                    mixed_subject_sentences_acum_general))) * 100
            mixed_subject_sentences_prom_general = (float(mixed_subject_sentences_acum_general) / (
                float(positive_subject_sentences_acum_general) + float(negative_subject_sentences_acum_general) + float(
                    mixed_subject_sentences_acum_general))) * 100

            data_json['positive_sentences_prom'] = positive_sentences_prom_general
            data_json['negative_sentences_prom'] = negative_sentences_prom_general
            data_json['mixed_sentences_prom'] = mixed_sentences_prom_general

            data_json['positive_subject_sentences_prom'] = positive_subject_sentences_prom_general
            data_json['negative_subject_sentences_prom'] = negative_subject_sentences_prom_general
            data_json['mixed_subject_sentences_prom'] = mixed_subject_sentences_prom_general

            data_json['mixed_documents_prom'] = mixed_documents_prom_general
            data_json['negative_documents_prom'] = negative_documents_prom_general
            data_json['positive_documents_prom'] = positive_documents_prom_general
            #jsonbuild.append(data_json)
            jsonbuild.append(data_json)
            json_list = json.dumps(jsonbuild, indent=4, sort_keys=True)
            print json_list
            json_process = process.pyAwsNetCommon()
            value = json_process.getURLJSON('AWS', 'bucket_current_out')
            keyfile = json_process.getURLJSON('AWS', 'bucket_file_name_one')
            with open(value + keyfile, 'w') as outfile:
                json.dump(data, outfile)
            path = value + keyfile
            print " Processing...."
           # json_process.Uploadfiles(path, keyfile)
            print " done"


    def keythemestalkabout(self, path):

        with open(path) as data_file:
            data = json.load(data_file)
            positive_subject_sentences_acum=0
            negative_subject_sentences_acum =0
            mixed_subject_sentences_acum =0

            positive_subject_sentences_prom=0
            negative_subject_sentences_prom =0
            mixed_subject_sentences_prom =0
            sentinel =0
            jsonbuild = []

            indice=0
            for sentiment in data['output']:
                positive_subject_sentences_acum = positive_subject_sentences_acum + sentiment['positive_subject_sentences']
                negative_subject_sentences_acum = negative_subject_sentences_acum + sentiment['negative_subject_sentences']
                mixed_subject_sentences_acum = mixed_subject_sentences_acum + sentiment['mixed_subject_sentences']
                sentinel=sentinel+1
                if sentinel==7:
                    # save to date in json file.
                   data_json = {}
                   sentinel =0
                   p = positive_subject_sentences_acum + negative_subject_sentences_acum + mixed_subject_sentences_acum
                   positive_subject_sentences_prom= (float(positive_subject_sentences_acum) / float(p) ) * 100
                   negative_subject_sentences_prom=(float(negative_subject_sentences_acum) / float(p) ) * 100
                   mixed_subject_sentences_prom = ( float(mixed_subject_sentences_acum) / float(p) ) * 100
                   data_json['date'] = sentiment['date']
                   data_json['positive_subject_sentences_prom'] = positive_subject_sentences_prom
                   data_json['negative_subject_sentences_prom'] = negative_subject_sentences_prom
                   data_json['mixed_subject_sentences_prom'] = mixed_subject_sentences_prom

                   jsonbuild.append(data_json)
                   positive_subject_sentences_acum = 0
                   negative_subject_sentences_acum = 0
                   mixed_subject_sentences_acum = 0
        json_process = process.pyAwsNetCommon()
        value = json_process.getURLJSON('AWS', 'bucket_current_out')
        keyfile = json_process.getURLJSON('AWS', 'bucket_file_name_two')
        with open(value + keyfile, 'w') as outfile:
            json.dump(data, outfile)
        path = value + keyfile
        json_process.Uploadfiles(path, keyfile)


    def sentimentchangeddayoverday(self,path):
            #What are the key themes they talk about
        with open(path) as data_file:
            data = json.load(data_file)
          #  json_list = json.dumps(data, indent=4, sort_keys=True)
            document_general_total=0
            positive_document_acquisition = 0
            document_internal_total_acquisition=0
            positive_document_service = 0
            document_internal_total_service = 0
            positive_document_purchase = 0
            document_internal_total_purchase = 0
            positive_document_health = 0
            document_internal_total_health = 0
            positive_document_creativity = 0
            document_internal_total_creativity = 0
            positive_document_attraction = 0
            document_internal_total_attraction = 0

            positive_document_cost = 0
            document_internal_total_cost = 0


            positive_document_quality = 0
            document_internal_total_quality = 0

            positive_document_expectation = 0
            document_internal_total_expectation = 0

            positive_document_taste = 0
            document_internal_total_taste = 0

            positive_document_humor = 0
            document_internal_total_humor = 0

            negative_document_acquisition =0
            negative_document_service=0
            negative_document_creativity=0
            negative_document_purchase=0
            negative_document_health=0
            negative_document_attraction=0
            negative_document_confidence=0
            negative_document_cost=0
            negative_document_quality=0
            negative_document_expectation=0
            negative_document_taste=0
            negative_document_humor=0
            positive_document_confidence = 0
            document_internal_total_confidence = 0
            for i in data['output'] :
                document_general_total =i['documents'] + document_general_total
            #    print document_general_total
                for j in i['themes']:
                    name = j['name']
                #    print name
                    if name =='acquisition' :
                        positive_document_acquisition = positive_document_acquisition +   j['positive_documents']
                        document_internal_total_acquisition= document_internal_total_acquisition + j['documents']
                        negative_document_acquisition = negative_document_acquisition + j['negative_documents']

                    if name == 'service':
                        positive_document_service = positive_document_service + j['positive_documents']
                        document_internal_total_service = document_internal_total_service + j['documents']
                        negative_document_service = negative_document_service + j['negative_documents']

                    if name == 'creativity':
                        positive_document_creativity = positive_document_creativity + j['positive_documents']
                        document_internal_total_creativity = document_internal_total_creativity + j['documents']
                        negative_document_creativity = negative_document_creativity + j['negative_documents']

                    if name == 'purchase intent':
                        positive_document_purchase = positive_document_purchase + j['positive_documents']
                        document_internal_total_purchase = document_internal_total_purchase + j['documents']
                        negative_document_purchase = negative_document_purchase + j['negative_documents']

                    if name == 'health':
                        positive_document_health = positive_document_health + j['positive_documents']
                        document_internal_total_health = document_internal_total_health + j['documents']
                        negative_document_health = negative_document_health + j['negative_documents']

                    if name == 'attraction':
                        positive_document_attraction = positive_document_attraction + j['positive_documents']
                        document_internal_total_attraction = document_internal_total_attraction + j['documents']
                        negative_document_attraction = negative_document_attraction + j['negative_documents']
                    if name == 'confidence':
                        positive_document_confidence = positive_document_confidence + j['positive_documents']
                        document_internal_total_confidence = document_internal_total_confidence + j['documents']
                        negative_document_confidence = negative_document_confidence + j['negative_documents']
                    #add new for issued 15-03-2016
                    if name == 'cost':
                        positive_document_cost = positive_document_cost + j['positive_documents']
                        document_internal_total_cost = document_internal_total_cost + j['documents']
                        negative_document_cost = negative_document_cost + j['negative_documents']
                    if name == 'quality':
                        positive_document_quality = positive_document_quality + j['positive_documents']
                        document_internal_total_quality = document_internal_total_quality + j['documents']
                        negative_document_quality = negative_document_quality + j['negative_documents']
                    if name == 'expectation':
                        positive_document_expectation = positive_document_expectation + j['positive_documents']
                        document_internal_total_expectation = document_internal_total_expectation + j['documents']
                        negative_document_expectation = negative_document_expectation + j['negative_documents']
                    if name == 'taste':
                        positive_document_taste = positive_document_taste + j['positive_documents']
                        document_internal_total_taste = document_internal_total_taste + j['documents']
                        negative_document_taste = negative_document_taste + j['negative_documents']
                    if name == 'humor':
                        positive_document_humor = positive_document_humor + j['positive_documents']
                        document_internal_total_humor = document_internal_total_humor + j['documents']
                        negative_document_humor = negative_document_humor + j['negative_documents']

            print negative_document_cost ,'  -- ', document_internal_total_cost ,'  ' , document_internal_total_cost,' ', positive_document_cost
            #acquisition
            acquisition = (float(document_internal_total_acquisition) / float(document_general_total)) * 100
            acquisition_negative =  ( float( negative_document_acquisition ) / float( document_internal_total_acquisition ) ) * 100
            acquisition_bar = ( float( positive_document_acquisition ) / float( document_internal_total_acquisition ) ) * 100
            #service
            service =  ( float( document_internal_total_service ) / float( document_general_total ) ) * 100
            service_bar = ( float( positive_document_service ) / float( document_internal_total_service ) ) * 100
            service_negative = (float(negative_document_service) / float(document_internal_total_service)) * 100

            #creativity
            creativity =  ( float( document_internal_total_creativity ) / float( document_general_total ) ) * 100
            creativity_bar = ( float( positive_document_creativity ) / float( document_internal_total_creativity ) ) * 100
            creativity_negative = (float(negative_document_creativity) / float(document_internal_total_creativity)) * 100

            #purchase intent
            purchase =  ( float( document_internal_total_purchase ) / float( document_general_total ) ) * 100
            purchase_bar = ( float( positive_document_purchase ) / float( document_internal_total_purchase ) ) * 100
            purchase_negative = (float(negative_document_purchase) / float(document_internal_total_purchase)) * 100

            #health
            health =  ( float( document_internal_total_health ) / float( document_general_total ) ) * 100
            health_bar = ( float( positive_document_health ) / float( document_internal_total_health ) ) * 100
            health_negative = (float(negative_document_health) / float(document_internal_total_health)) * 100
            #attraction
            attraction =  ( float( document_internal_total_attraction ) / float( document_general_total ) ) * 100
            attraction_bar = ( float( positive_document_attraction ) / float( document_internal_total_attraction ) ) * 100
            attraction_negative = (float(negative_document_attraction) / float(document_internal_total_attraction)) * 100
            #confidence
            confidence =  ( float( document_internal_total_confidence ) / float( document_general_total ) ) * 100
            confidence_bar = ( float( positive_document_confidence ) / float( document_internal_total_confidence ) ) * 100
            confidence_negative = (float(negative_document_confidence) / float(document_internal_total_confidence)) * 100
            # confidence
            cost = (float(document_internal_total_cost) / float(document_general_total)) * 100
            cost_bar = (float(positive_document_cost) / float(document_internal_total_cost)) * 100
            cost_negative = (float(negative_document_cost) / float(document_internal_total_cost)) * 100
            # confidence
            quality = (float(document_internal_total_quality) / float(document_general_total)) * 100
            quality_bar = (float(positive_document_quality) / float(document_internal_total_quality)) * 100
            quality_negative = (float(negative_document_quality) / float(document_internal_total_quality)) * 100
            # expectation
            expectation = (float(document_internal_total_expectation) / float(document_general_total)) * 100
            expectation_bar = (float(positive_document_expectation) / float(document_internal_total_expectation)) * 100
            expectation_negative = (float(negative_document_expectation) / float(document_internal_total_expectation)) * 100

            tasted = (float(document_internal_total_taste) / float(document_general_total)) * 100
            taste_bar = (float(positive_document_taste) / float(document_internal_total_taste)) * 100
            taste_negative = (float(negative_document_taste) / float(document_internal_total_taste)) * 100
            #humor
            humor = (float(document_internal_total_humor) / float(document_general_total)) * 100
            humor_bar = (float(positive_document_humor) / float(document_internal_total_humor)) * 100
            humor_negative = (float(negative_document_humor) / float(document_internal_total_humor)) * 100

            jsonbuild = {}
            jsonbuild['acquisition'] = acquisition
            jsonbuild['acquisition_positive'] = acquisition_bar
            jsonbuild['acquisition_negative'] = acquisition_negative

            jsonbuild['services'] = service
            jsonbuild['services_positive'] = service_bar
            jsonbuild['services_negative'] = service_negative

            jsonbuild['creativity'] = creativity
            jsonbuild['creativity_positive'] = creativity_bar
            jsonbuild['creativity_negative'] = creativity_negative

            jsonbuild['purchase_intent'] = purchase
            jsonbuild['purchase_intent_positive'] = purchase_bar
            jsonbuild['purchase_intent_negative'] = purchase_negative

            jsonbuild['health'] = health
            jsonbuild['health_positive'] = health_bar
            jsonbuild['health_negative'] = health_negative

            jsonbuild['attraction'] = attraction
            jsonbuild['attraction_positive'] = attraction_bar
            jsonbuild['attraction_negative'] = attraction_negative

            jsonbuild['confidence'] = confidence
            jsonbuild['confidence_positive'] = confidence_bar
            jsonbuild['confidence_negative'] = confidence_negative

            # 15-02-2017
            jsonbuild['cost'] = cost
            jsonbuild['cost_positive'] = cost_bar
            jsonbuild['cost_negative'] = cost_negative


            jsonbuild['quality'] = quality
            jsonbuild['quality_positive'] = quality_bar
            jsonbuild['quality_negative'] = quality_negative


            jsonbuild['expectation'] = expectation
            jsonbuild['expectation_positive'] = expectation_bar
            jsonbuild['expectation_negative'] = expectation_negative

            jsonbuild['taste'] = tasted
            jsonbuild['taste_positive'] = taste_bar
            jsonbuild['taste_negative'] = taste_negative

            jsonbuild['humor'] = humor
            jsonbuild['humor_positive'] = humor_bar
            jsonbuild['humor_negative'] = humor_negative
            #json_data_dump = json.dumps(jsonbuild)
            json_data_dump = json.dumps(jsonbuild, indent=4, sort_keys=True)
            print json_data_dump

            # call object to aws save
            json_process = process.pyAwsNetCommon()

            value = json_process.getURLJSON('AWS','bucket_current_out')
            keyfile = json_process.getURLJSON('AWS','bucket_file_name_four')
            with open( value +keyfile , 'w') as outfile:
                json.dump(data, outfile)
            path = value +keyfile
          #  json_process.Uploadfiles(path,keyfile)

   # 4 question
    def processJsonAskwerVolumeFour(self, path):
        week_ant = 0
        #
        #   r = acumulado - Mod( (acumulado - acomulado_ant)/2 )
        #
        controlr=0
        #with open(path) as data_file:
        #    data = json.load(data_file)

        #    data_json =data['output']
        #    json_all = json.dumps(data_json, indent=4, sort_keys=True)
        #    df = pd.read_json(json_all, dtype=object, encoding='utf-8')


        #    for name in df.index:
        #        print df.loc[name]["date"] ,' Normalized',df.loc[name]["posts_normalized"] ,'  posts_word_normalized ', df.loc[name]["posts_word_normalized"]
        controlr=0
        with open(path) as data_file:
            data = json.load(data_file)
            posts_word_normalized_acum = 0
            sentinel = 0
            jsonbuild = []
            indice = 0
            indice_=0
            indice_d=0
            posts_word_normalized_prom_c =0
            json_array =[]
            json_data_ac ={}
            for volume in data['output']:

                posts_word_normalized_acum = posts_word_normalized_acum + volume['posts_word_normalized']
                sentinel = sentinel + 1
                if sentinel == 7:
                    # save to date in json file.
                    data_json = {}
                    posts_word_normalized_prom = (float(posts_word_normalized_acum) / float(sentinel))
                    sentinel = 0
                    if indice_d==0:
                        json_data_ac['date'] = volume['date']
                        json_data_ac['value'] = posts_word_normalized_prom
                        json_data_ac['relativehigh'] = 0
                        json_data_ac['relativelow'] = 0
                        json_data_ac['absolutehigh'] = posts_word_normalized_prom
                        json_data_ac['absolutelow'] = posts_word_normalized_prom
                        json_data_ac['trend'] = 'up'
                        indice_d=1
                        jsonbuild.append(json_data_ac)
                        posts_word_normalized_acum = 0
                    else:
                       data_json['date'] = volume['date']
                       data_json['value'] = posts_word_normalized_prom
                       jsonbuild.append(data_json)
                       posts_word_normalized_acum = 0
                indice = indice + 1

        sum = 0
        resta =0
        json_final=[]
        data_json_end={}
        first_time =0
        print jsonbuild

        json_final.append(json_data_ac)
        for i in range(1,len(jsonbuild)):
           try:
               data_json_end={}
               diferencial_word =  float( jsonbuild[i]['value'] )
               diferencial_steword = float(jsonbuild[i-1]['value'])
               data_json_end['date'] = jsonbuild[i]['date']
               data_json_end['value'] = diferencial_word

               relativehigh =  abs((float(diferencial_word) - float(diferencial_steword)) / 2)
               relativelow = abs((float(diferencial_word) - float(diferencial_steword)) / 2)
               data_json_end['relativelow'] = relativelow
               data_json_end['relativehigh'] = relativehigh

               absolutehigh =diferencial_word + abs((float(diferencial_word) - float(diferencial_steword)) / 2)
               absolutelow = diferencial_word - abs((float(diferencial_word) - float(diferencial_steword)) / 2)
               data_json_end['absolutelow'] = absolutelow
               data_json_end['absolutehigh'] = absolutehigh


               if diferencial_word > diferencial_steword :
                  data_json_end['trend'] ='up'
               else:
                   data_json_end['trend'] = 'down'
               json_final.append(data_json_end)
           except:
             print ''
        json_file = json.dumps(json_final, indent=4, sort_keys=True)

        print  json_file
        json_process = process.pyAwsNetCommon()
        value = json_process.getURLJSON('AWS', 'bucket_current_out')
        keyfile = json_process.getURLJSON('AWS', 'bucket_file_name_three')
        with open(value + keyfile, 'w') as outfile:
            json.dump(data, outfile)
        path = value + keyfile



    def answer5(self, path):

        languaje,age,gender,demographics,income,household,ownership,education= path.split(",")
        with open(languaje) as data_languaje:
            language_data = json.load(data_languaje)

        with open(age) as data_age:
            age_data = json.load(data_age)

        with open(gender) as data_gender:
            gender_data = json.load(data_gender)

        with open(demographics) as demographics_gender:
            demographics_data = json.load(demographics_gender)

        with open(income) as data_income:
            income_data = json.load(data_income)

        with open(household) as data_household:
            household_data = json.load(data_household)

        with open(ownership) as data_ownership:
            ownership_data = json.load(data_ownership)

        with open(education) as data_education:
            education_data = json.load(data_education)

        # gender process.
        female_count = 0
        male_count =0
        female_total =0
        male_total = 0
        gendered_total =0
        json_=[]
        data_json={}
        sentinel=0
        female_count_by_week=0
        male_count_by_week = 0
        female_total_by_week=0
        male_total_by_week =0
        daysLeading = ""
        #json_gender_data = json.dumps(gender_data, indent=4, sort_keys=True)
        daysLeading_female = 0
        daysLeading_male =   0
        femaleSumByWeek = 0
        maleSumByWeek =0
        for gender_record in gender_data['output']:

             # sumo mas + fema si es > 0 proceso sino nada.
             if gender_record['female']['universe'] >= gender_record['male']['universe']:
                 daysLeading_female = daysLeading_female + 1
             if gender_record['male']['universe'] >= gender_record['female']['universe']:
                 daysLeading_male = daysLeading_male + 1

#             cvalue = gender_record['female']['count'] + gender_record['male']['count']
#             if cvalue!=2000:
             female_count = female_count +gender_record['female']['universe']
             male_count = male_count + gender_record['male']['universe']
             female_count_by_week = female_count_by_week +gender_record['female']['universe']
             male_count_by_week = male_count_by_week + gender_record['male']['universe']
             dayofweek = parser.parse(gender_record['date']).weekday();

             if dayofweek == 0:
                data_json={}
                female_total_by_week = float(female_count_by_week) / (float(female_count_by_week) + float(male_count_by_week))
                male_total_by_week = float(male_count_by_week) / (float(male_count_by_week) + float(female_count_by_week))
                #jaime
                data_json['date'] = gender_record['date']
              #  print data_json['date']
                data_json['femalebyWeek'] = female_total_by_week
                data_json['malebyWeek'] = male_total_by_week
                data_json['femaleSumByWeek'] =female_count_by_week
                data_json['maleSumByWeek'] = male_count_by_week
                json_.append(data_json)
                female_count_by_week=0
                male_count_by_week =0

        female_total = (float(female_count) /( float(female_count) + float(male_count))) * 100
        male_total = ( float(male_count) / (float(male_count) + float(female_count)) ) *100
        universe_total = language_data['query_meta']['total_universe']
        gendered_total =( (float(female_count) + float(male_count)) / float(universe_total) )*100
        data_json={}
       # json_gender_data = json.dumps(json_, indent=4, sort_keys=True)
       # print json_gender_data
        data_json['femaleUniverseavg'] = female_total
        data_json['famaleUniverse'] = female_count
        data_json['maleUniverse'] = male_count
        data_json['maleUniverseavg'] = male_total
        data_json['genderedUniverseavg']=gendered_total
        data_json['daysleadingfemale'] = daysLeading_female
        data_json['daysleadingmale'] = daysLeading_male
        json_.append(data_json)
        json_all = json.dumps(json_, indent=4, sort_keys=True)
        print json_all
        json_ages_ = {}
        for age_key in age_data['output']['ranges']:
            json_ages_['age'] = age_key['display_name']
            json_ages_['probability'] = age_key['probability']
            json_.append(json_ages_)
            json_ages_ = {}

        doc_total =0
        income_json ={}
        for income_key in income_data["output"]["ranges"]:
              income_json = {}
              income_json["income_range"] = income_key["display_name"]
              income_json["value"] = income_key["probability"]
              json_.append(income_json)

        demographics_json ={}
        demographics_json['demo_value'] = demographics_data["output"]['db_median_disposable_income']
        #db_median_disposable_income

        ownership_json = {}
        for ownership_key in ownership_data["output"]:
            ownership_json = {}
            ownership_json["rent"] = ownership_key["display_name"]
            ownership_json["value"]= ownership_key["probability"]
            json_.append(ownership_json)

        household_json_ant ={}

        for household_key in household_data["output"]["ranges"]:
            household_json_ant = {}
            household_json_ant["display_name"] = household_key["id"]
            household_json_ant["value"] = household_key["probability"]

            json_.append(household_json_ant)

        education_json ={}
        value_education_lt=0
        value_education_h =0
        counter_mid_h = 0
        ks={}

        list_ =[]
        languages= set()
        counts=[]
        json_lan  = json.dumps(language_data, indent=4, sort_keys=True)
        for key_lang in language_data["output"]:
            for key_, value_ in key_lang.iteritems():
                if len(key_)==2:
                   languages.add(key_)

        l = list(languages)
        dic ={}
        for key_lang in language_data["output"]:
            for lang in l:
                for key,value in key_lang.iteritems():
                    if len(key) == 2:
                        dic[key.encode('utf-8')] = dic.get(key.encode('utf-8'), 0) + value

        json_language={}
        result = sorted(dic.items(), key=operator.itemgetter(1))
        result.reverse()
        total = sum(dic.values())
        print total

        print  result[0][0] , result[0][1]
        json_language[result[0][0]] = (float(result[0][1]) / float( total ) ) *100
        json_language[result[1][0]] = (float(result[1][1]) / float(total)) * 100
        json_language[result[2][0]] = (float(result[2][1]) / float( total ) ) *100
        other = total - result[0][1] - result[1][1] -result[2][1]
        json_language['other'] = (float(other) / float( total ) ) *100
        json_.append(json_language)



        education_json["demographics_value"] = float(demographics_data["output"]['education_any_college']) - float(demographics_data["output"]['db_education_any_college'])
        education_json["demographics_type"] =  "higher-education"
        education_json["demographics_mean_value"] = demographics_data["output"]['education_any_college']


        json_.append(education_json)
        household_json={}
        household_json["demographics_type"] = "household-income"
        household_json["demographics_mean_value"] = demographics_data["output"]['median_household_income']
        household_json["demographics_value"] = float(
            demographics_data["output"]['median_household_income']) - float(
            demographics_data["output"]['db_median_household_income'])
        json_.append(household_json)

        householdisposable={}
        householdisposable["demographics_type"] = "disposable-income"
        householdisposable["demographics_value"] = demographics_data["output"]['db_median_disposable_income']
        householdisposable["demographics_mean_value"] = float(demographics_data["output"]['median_disposable_income'] )- float(demographics_data["output"]['db_median_disposable_income'] )

        json_.append(householdisposable)
        householdsize = {}
        householdsize["demographics_type"] = "household-size"
        householdsize["demographics_mean_value"] = float(demographics_data["output"]['average_household_size']) - float(demographics_data["output"]['db_average_household_size'])
        householdsize["demographics_value"] = demographics_data["output"]['average_household_size']
        householdsize["demographics_sample_size"] = demographics_data["output"]['sample_size']
        json_.append(householdsize)

        householdvalue = {}
        householdvalue["demographics_type"] = "household-value"
        householdvalue["demographics_value"] = float(demographics_data["output"]['median_house_value']) - float(
            demographics_data["output"]['db_median_house_value'])
        householdvalue["demographics_mean_value"] = demographics_data["output"]['median_house_value']
        json_.append(householdvalue)
        json_all = json.dumps(json_, indent=4, sort_keys=True)


    def answer6(self, path):
        interests,  postinterest = path.split(",")
        with open(interests) as data_interests:
            interest_data = json.load(data_interests)

        with open(postinterest) as data_postinterest:
            data_post = json.load(data_postinterest)


        interest_sort_ratio = sorted(interest_data['output'],key=lambda d: d["ratio"],reverse=True)
        isize =len( list(interest_data['output']))
        irang =15
        interest_json=[]
        sum_t = 0;
        if isize <=15:
            irang =isize
        for i_ in range(irang):
            interest_json.append(interest_sort_ratio[i_])
            sum_t +=interest_sort_ratio[i_]['ratio']

        #print  sum_t
        json_final ={}
        json_array =[]
        for y_ in interest_json:
           parcial = float(y_['ratio']) / float(sum_t) * 100
           json_final['name'] = y_['name']
           json_final['distribution'] =y_['distribution']
           json_final['ratio'] = y_['ratio']
           json_final['ratioPercentage'] = parcial
           json_array.append(json_final)
           json_final={}
        icount =0
        for y_ in data_post['output']:
            if icount < 10:
                json_final['name'] = y_['name']
                json_final['distribution'] = y_['distribution']
                json_final['score'] = y_['score']
                json_array.append(json_final)
                json_final = {}
                icount += 1

            else:
                break

        # interest.json
        # 1. ordenar por ratio el json descendentes y me quedo con los 10 primeros
        #   y de esos 10 paso el name y distribu. ratio  varnueva = Ratioper(x) = R(1) / R(0-9)

        # 2. post-interest.json
        #   tomo los primeros 10 y los vuelco al nuevo json sin computar.
        json_all = json.dumps(json_array, indent=4, sort_keys=True)
        print json_all
        print len(json_array)

    def answer13(self, path):
        do, feel,like = path.split(",")

        with open(do) as data_do:
            json_do = json.load(data_do)
        with open(feel) as data_feel:
            json_feel = json.load(data_feel)
        with open(like) as data_like:
            json_like = json.load(data_like)

        dosortbyscore = sorted(json_do['output'], key=lambda d: d["score"], reverse=True)
        feelsortbyscore = sorted(json_feel['output'], key=lambda d: d["score"], reverse=True)
        likesortbyscore = sorted(json_like['output'], key=lambda d: d["score"], reverse=True)

        names={}
        names['dosortbyscore']=dosortbyscore
        names['feelsortbyscore'] =feelsortbyscore
        names['likesortbyscore'] =likesortbyscore
        option=0
        top_dic_do = {}
        json_topics_do = []
        for instances in names:
         max = names[instances].__getitem__(0)['score']
         restricted_do = names[instances][:50]
         min = restricted_do.__getitem__(restricted_do.__len__() - 1)['score']
         print max ,', ', option, ' ' ,'min',min
         for do in restricted_do:
             score =do['score']
             positive_documents=do['positive_documents']
             negative_documents=do['negative_documents']
             size_color =1+ (  (score  - min) /(( max - min ) * 0.1))
             result = ((positive_documents) /(positive_documents + negative_documents))*100
             if size_color > 10:
                 size_color=10
             if result <=25:
                color =1
             if result <= 45 and result>=25:
                 color =2
             if result <= 55 and result>=45:
                color = 3
             if result <= 75 and result>=55:
                color = 4
             if result <= 100 and result>=75:
                color = 5
             top_dic_do['topics'] = do['topic']
             top_dic_do['score']  = do['score']
             top_dic_do['positive_documents'] = do['positive_documents']
             top_dic_do['negative_documents'] = do['negative_documents']
             top_dic_do['color'] = color
             top_dic_do['Size'] =int(size_color)
             if option ==0:
                top_dic_do['option'] ='do'
             if option ==1:
                 top_dic_do['option'] = 'like'
             if option == 2:
                 top_dic_do['option'] = 'feel'
             json_topics_do.append(top_dic_do)
             top_dic_do={}
         option=option + 1
         #print option


        json_ending = json.dumps(json_topics_do, indent=4, sort_keys=True)
        print json_ending
        print max ,'   ', min
    def answer14(self, path):
        with open(path) as data_file:
            data_emotions = json.load(data_file)
        data=[]
        c =0
        for js in data_emotions['output']:
            for iss in js['emotions']:
                data.append(iss)

        json_all = json.dumps(data, indent=4, sort_keys=True)
        df = pd.read_json(json_all,dtype=object,encoding='utf-8')
        df = df.groupby("name").agg({"documents": np.sum, "name": lambda x: x.nunique()})
        print df
        hf = df.groupby("documents").agg({"documents": np.sum}).sum()
        json_emot={}
        json_vect=[]
        for name in df.index:
           json_emot[name] = (float( df.loc[name]["documents"]) / float(hf ) )*100
        print json_emot




























