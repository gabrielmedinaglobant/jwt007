from urllib import quote_plus
from datetime import datetime, date
from types import MethodType
import dateutil.parser
import requests
import json


class atlas_jwt_request_error(Exception):
    pass


class atlas_jwt_server_error(Exception):
    pass


class atlas_jwt_request:
    ATLAS_API_KEY = None
    #ATLAS_API_BASE_URL = 'https://atlas.infegy.com/api/v2/'
    ATLAS_API_BASE_URL = 'http://jwt-cic-infegy-proxy.ngrok.io/api/v2/'

    def __init__(self, query, **kwargs):
        self.query = query

        # Parse args
        for k, v in kwargs.iteritems():
            if isinstance(k, basestring):
                k = k.strip()
            if isinstance(v, basestring):
                v = v.strip()
            if k and v:
                setattr(self, k, v)

        self.__request_cache = {}

    def __type_massage__(self, t):
        if isinstance(t, bool):
            return '1' if t else '0'
        if isinstance(t, list) or isinstance(t, tuple):
            return ','.join(str(v) for v in t)
        if isinstance(t, datetime):
            return t.date().isoformat()
        if isinstance(t, date):
            return t.isoformat()
        else:
            return str(t)

    def uri(self, endpoint):
        if self.ATLAS_API_KEY is None:
            raise atlas_jwt_request_error(
                'You must set an Atlas API key on this class before use, such as: from atlas_api import atlas_request; atlas_request.ATLAS_API_KEY=\'YOUR_KEY_HERE\'')

        uri_string = self.ATLAS_API_BASE_URL + endpoint + '?api_key=' + self.ATLAS_API_KEY
        print uri_string
        for k in [k for k in dir(self) if not callable(getattr(self, k)) and not k.startswith('_') and not k.isupper()]:
            v = getattr(self, k)
            if v is None:
                continue

            uri_string += '&%s=%s' % (k, quote_plus(self.__type_massage__(v)))
            print uri_string
        return uri_string

    def url(self, endpoint):
        return self.uri(endpoint)

    def run_raw(self, endpoint, skip_cache=False):
        if not skip_cache and endpoint in self.__request_cache:
            return self.__request_cache[endpoint]

        r = requests.get(self.uri(endpoint))

        if r.status_code >= 400 and r.status_code < 500:
            raise atlas_jwt_request_error(
                'Your request has an error (code %d): %s' % (r.status_code, r.json()['status_message']))
        elif r.status_code >= 500:
            if r.text.startswith('{'):
                raise atlas_jwt_server_error(
                    'Atlas has errored (code %d): %s' % (r.status_code, r.json()['status_message']))
            else:
                raise atlas_jwt_server_error('Atlas has errored (code %d): %s' % (r.status_code, r.text))

        try:
            raw_json = r.json()
        except:
            raise atlas_jwt_server_error('Atlas returned something that can\'t be parsed as JSON')
        if not isinstance(raw_json, object):
            raise atlas_jwt_server_error('Atlas returned something that isn\'t a JSON object')

        if 'status' not in raw_json:
            raise atlas_jwt_server_error('Atlas returned an object with no status')
        if raw_json['status'] != 'OK':
            raise atlas_jwt_server_error('Atlas a bad status with no error code. Status: %s, Status message: %s',
                                     raw_json['status'], raw_json.get('status_message', ''))

        self.__request_cache[endpoint] = raw_json

        return raw_json

    def run(self, endpoint, skip_cache=False):
            raw_json = self.run_raw(endpoint, skip_cache)
            return json.dumps(raw_json,indent=4,sort_keys=True)

    # The various endpoint utility functions...
    def method(self,arg):
        return self.run(arg)

    def posts(self):
        return self.run('posts')

    def topics(self):
        return self.run('topics')

    def positive_topics(self):
        return self.run('positive-topics')

    def negative_topics(self):
        return self.run('negative-topics')

    def brands(self):
        return self.run('brands')

    def hashtags(self):
        return self.run('hashtags')

    def topic_clusters(self):
        return self.run('topic-clusters')

    def headlines(self):
        return self.run('headlines')

    def sentiment(self):
        return self.run('sentiment')

    def positive_keywords(self):
        return self.run('positive-keywords')

    def negative_keywords(self):
        return self.run('negative-keywords')

    def linguistics_stats(self):
        return self.run('linguistics-stats')

    def themes(self):
        return self.run('themes')

    def emotions(self):
        return self.run('emotions')

    def languages(self):
        return self.run('languages')

    def timeofday(self):
        return self.run('timeofday')

    def channels(self):
        return self.run('channels')

    def gender(self):
        return self.run('gender')

    def states(self):
        return self.run('states')

    def countries(self):
        return self.run('countries')

    def home_ownership(self):
        return self.run('home-ownership')

    def income(self):
        return self.run('income')

    def household_value(self):
        return self.run('household-value')

    def education(self):
        return self.run('education')

    def demographics(self):
        return self.run('demographics')

    def ages(self):
        return self.run('ages')

    def influence_distribution(self):
        return self.run('influence-distribution')

    def influencers(self):
        return self.run('influencers')

    def interests(self):
        return self.run('interests')

    def post_interests(self):
        return self.run('post-interests')

    def query_test(self):
        return self.run('query-test')

    def events(self):
        return self.run('events')

    def stories(self):
        return self.run('stories')

    def entities(self):
        return self.run('entities')



class atlas_jwt_response(object):
    def __init__(self, obj):
        for k, v in obj.iteritems():
            if isinstance(v, basestring):
                if 8 < len(v) < 22:
                    try:
                        dt = dateutil.parser.parse(v)
                        setattr(self, k, dt)
                        continue
                    except ValueError:
                        pass
                setattr(self, k, v)
            elif type(v) in (float, int, date, datetime, bool, long):
                setattr(self, k, v)
            elif isinstance(v, dict):
                setattr(self, k, atlas_jwt_response(v))
            else:
                setattr(self, k, [])
                for o in v:
                    if isinstance(o, dict):
                        getattr(self, k).append(atlas_jwt_response(o))
                    else:
                        getattr(self, k).append(o)
