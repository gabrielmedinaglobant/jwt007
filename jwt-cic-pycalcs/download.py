import ParsingUtils as utils
import pyCommon as process
import logging as log
from datetime import date, datetime, timedelta
import time
import mysql.connector
import ConfigParser
import os
import request_infegy.request_jwt as jwt
#
#
"""it set log """
log.basicConfig(filename='/var/log/JWT-download.log',level=log.ERROR)
""" ConfigSectionMap method will return a dictionary loadly with key/value parameters """
def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1
""" this main method """
if __name__ == '__main__':
    try:

        """ ARGS """
        ut = utils.Utils()
        # python jwt-main -s DOWNLOAD-X
        cmd = ut.parameters()
        pr = process.pyAwsNetCommon()
        mysql_aws = pr.getDictonarySection(cmd[0])

        """it set mysql connect"""
        cnx = mysql.connector.connect(user=mysql_aws['user'],password=mysql_aws['password'], database=mysql_aws['db'], host=mysql_aws['host'], port=mysql_aws['port'])
        """it build a mysql cursor"""
        cursor = cnx.cursor(named_tuple=True, buffered=True)
        """it take a state 1 on 0 off"""
        active =int(mysql_aws['state'])

        """ it trasform datetime to string in %Y-%m-%d format because the next query need this"""
        today = datetime.today().strftime('%Y-%m-%d')
        """it declare query on parameters table"""
        query = ("SELECT project_uuid,query_type,infegy_api_key,query_string FROM vw_query_types_per_project")


        #query = ("SELECT id, keyname, value,date_created,date_updated FROM configuration_parameter "
        #         "WHERE active=%s and date_format(date_created,'%Y-%m-%d')=%s"  )
        # in next step we'll review this query and you should add exceptions points.
        # please add a update process for change flag in mysql
        # ??
        """ it shoot query"""
        cursor.execute(query)

        """it get all result of query"""
        result = cursor.fetchall()

        brandic = []
        """this loop if for get URL Query and  shoot querys on infegy. after this it save in the mysql_aws['directory']"""
        """mysql_aws['directory'] will be a json directory the where others scripting will take from this """
        cursorbrand = cnx.cursor(named_tuple=True, buffered=True)
        for k in result:
          dir_ = mysql_aws['directory'] + k[0] +'/input'
          """ at this moment we are ready for build directory tree"""
          if not os.path.isdir(dir_):
              os.makedirs(dir_)
              subdir=dir_+'/'+k[1]
              os.makedirs(subdir)
          else:
             try:
                 subdir = dir_ + '/' + k[1]
                 if not os.path.isdir(subdir):
                  print subdir
                  os.mkdir(subdir)
                 if not os.path.isdir(subdir):
                     try:
                         os.mkdir(subdir)
                     except Exception as e:
                         log.error(e.message)
             except Exception as e:
                print("Error while creating directories ")
                print(e.message)
                log.error("Error while creating directories ")

        """Now we are shooting the download process"""
        #print brandic

        download_start_date = datetime.utcnow() + timedelta(days=-30)
        download_end_date = datetime.utcnow() + timedelta(days=-1)

        for k in result:
            try:
                filepath=""
                jwt.atlas_jwt_request.ATLAS_API_KEY = k[2]
                q = jwt.atlas_jwt_request(k[3], start_date=download_start_date, end_date=download_end_date)
                query_type_files = ("SELECT * FROM vw_query_files_of_query_types WHERE query_type =%s")
                cursorbrand.execute(query_type_files, (k[1],))
                brandResult = cursorbrand.fetchall()
                for i in brandResult:
                    method = i[1].split('.')[0]
                    json_return_method = q.method(method)
                    filepath =mysql_aws['directory'] + k[0] +'/input/'+ k[1]+'/'+i[1]
                    jsonfile = open(filepath, "w")
                    jsonfile.write(json_return_method)
                    #time.sleep(0.5)

            except Exception as e:
                print("Error while downloading ")
                print(e.message)
                log.error("Error while downloading ")

        """close cursor"""
        cursor.close()
        """close myslq connect"""
        cnx.close()

    except Exception as e:
       log.error("Download failed. Error in get json file  :" + e.message)
       print e.message
       exit(1)
