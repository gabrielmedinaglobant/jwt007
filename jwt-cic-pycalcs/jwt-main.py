import ParsingUtils as utils
import infegyproc as infegy
import pyCommon as process
import time
from datetime import datetime 
import mysql.connector
import json
import string
import os
import io
import googleTrends
import logging as log
import pytz
import collections

log.basicConfig(filename='/var/log/pycalcs/jwt-main.log', level=log.ERROR)

def save_output_results(start_timestamp, end_timestamp, file_path):
    jsonfile = open(file_path, "w")
    jsonfile.write(json.dumps({
        "elapsed_timestamp": (end_timestamp - start_timestamp),
        "last_updated_timestamp": time.time(),
        "last_updated_date": datetime.now(pytz.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
    }, sort_keys=True, indent=4))
    jsonfile.close()

def removeduplicate(it):
    seen = set()
    for x in it:
        if x not in seen:
            yield x
            seen.add(x)


if __name__ == '__main__':

    # timestamps declaration
    start_timestamp = 0
    end_timestamp = 0

    project_uuids = set()

    ut = utils.Utils()
    # python jwt-main -s CALCS-LOCAL
    pr = process.pyAwsNetCommon()
    cmd = ut.parameters()
    infegyinstance = infegy.infegy()
    dictonary = pr.getDictonarySection(cmd[0])

    start_timestamp = time.time()

    cnx = mysql.connector.connect(user=dictonary['user'], password=dictonary['password'], database=dictonary['db'],
                                  host=dictonary['host'], port=dictonary['port'])
    """it build a mysql cursor"""
    cursor = cnx.cursor(named_tuple=True, buffered=True)
    """ it trasform datetime to string in %Y-%m-%d format because the next query need this"""
    query = ("SELECT distinct project_uuid FROM vw_query_types_per_project order by project_uuid")
    """ it shoot query"""
    cursor.execute(query)
    result = cursor.fetchall()
    uuid = []
    parameters_path_in = collections.OrderedDict()
    parameters_path_out = collections.OrderedDict()
    for i in result:
        parameters_path_in[i[0]] = dictonary['directory_base'] + i[0] + '/input/'
        parameters_path_out[i[0]] = dictonary['directory_out'] + i[0] + '/output/'
        project_uuids.add(i[0])
        if not os.path.isdir(parameters_path_out[i[0]]):
            os.makedirs(parameters_path_out[i[0]])
    # cursor for mybrands and competitors

    cursorBrand = cnx.cursor(named_tuple=True, buffered=True)

    """ it trasform datetime to string in %Y-%m-%d format because the next query need this"""
    queryBrand = ("select project_uuid, query_type, brand_name from vw_query_types_per_project where "
                  "  query_type = 'infegy-my-brand' or query_type like 'infegy-competitor-%'"
                  "  order by project_uuid, query_type")
    """ it shoot query"""

    cursorBrand.execute(queryBrand)
    resultBrand = cursorBrand.fetchall()
    #print "resultBrand = cursorBrand.fetchall()"
    #print resultBrand
    top = 0
    cfile = collections.OrderedDict()
    ind = 0
    c = collections.OrderedDict()

    cfile = set()
    dicId = collections.OrderedDict()

    for l in resultBrand:
        cfile.add(l[1])

    for i in result:
        for l in resultBrand:
            if l[0] == i[0]:
                dicId['id'] = i[0]
                dicId[l[1]] = l[2]
        c[i[0]] = dicId
        dicId = collections.OrderedDict()

    

    projects_brands_and_competitors_query_types = {}

    for project_uuid in project_uuids:
        projects_brands_and_competitors_query_types[project_uuid] = []

    for row in resultBrand:
        projects_brands_and_competitors_query_types[row[0]].append(row[1])

    #Q7 of 23

    total =0
    filePath=[]


    for key, value in parameters_path_in.iteritems():
        try:
            contact =""

            for qtype in cfile:
                contact = parameters_path_in[key] + qtype + "/sentiment.json," + contact
            path_in = contact[:-1]
            path_out = parameters_path_out[key] + dictonary['q7of23']

            print("question q7of23")
            print("******************************************************************")
            print(path_in)
            print(c[key])
            print(path_out)
            print("******************************************************************")

            infegyinstance.WhatbrandspeopletalkingNew(path_in, c[key], path_out)

        except Exception as e:
            print(e)
            log.error("Rutine Q7OF23: ")


    for key, value in parameters_path_in.iteritems():
        
        try:
            contact = ""
            for v in cfile:
                contact = parameters_path_in[key] + v + "/volume.json," + contact

            path_in = contact[:-1]
            path_out = parameters_path_out[key] + dictonary['q2of23']

            google = googleTrends.googleTrends()

            google.HowmuchhavepeoplementionedG(path_in, c[key], path_out)

        except Exception as e:
            print(e)
            log.error("Rutine Q2OF23: ")
            log.error(e.message)

    """Question 8 of 23"""
    QuestionEigthDict = pr.getDictonarySection("Q8OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            path_in = parameters_path_in[key] + QuestionEigthDict['themes']
            path_out = parameters_path_out[key] + dictonary['q8of23']
            infegyinstance.whatarekeythemesabout(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            log.error("Rutine Q8OF23: ")
            log.error(e.message)

    """ Question 9 of 23"""
    QuestionNineDict = pr.getDictonarySection("Q9OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            path_in = parameters_path_in[key] + QuestionNineDict['sentiment']
            path_out = parameters_path_out[key] + dictonary['q9of23']
            infegyinstance.sentimentchangeddayoverday(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            log.error("Rutine Q9OF23: ")
            print e.message
    """ Question 10 of 23"""

    QuestionTenDict = pr.getDictonarySection("Q10OF23")

    for key, value in parameters_path_in.iteritems():
        try:
            path_in = parameters_path_in[key] + QuestionTenDict['volume']
            path_out = parameters_path_out[key] + dictonary['q10of23']

            infegyinstance.HasVolumechangeddayoverdayNew(path_in, path_out)

            print path_in, '...', path_out
        except Exception as e:
            print e.message
            log.error("Rutine Q10OF23: " + e.message)

    """ Question 11 of 23"""

    print('------------------>Question 11 of 23')
    QuestionElevenDict = pr.getDictonarySection("Q11OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            splt = QuestionElevenDict['answerfive'].split(",")
            p = ""
            for v in splt:
                p = parameters_path_in[key] + v + "," + p
            path_in = p[:-1]
            path_out = parameters_path_out[key] + dictonary['q11of23']
            infegyinstance.Whoistalkingaboutme(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            print e.message
            log.error("Rutine Q11F23: ")


    """ Question 12 of 23"""
    QuestionTwelveDict = pr.getDictonarySection("Q12OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            splt = QuestionTwelveDict['answersix'].split(",")
            p = ""
            for v in splt:
                p = parameters_path_in[key] + v + "," + p
            path_in = p[:-1]
            path_out = parameters_path_out[key] + dictonary['q12of23']
            infegyinstance.Whatothtopicstheycareabout(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            print e.message
            log.error("Rutine Q120F23: ")
            

    """What emotions do people have when talking about the category?"""
    """ Question 10 of 23"""
    QuestionFourTenDict = pr.getDictonarySection("Q14OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            path_in = parameters_path_in[key] + QuestionFourTenDict['emotions']
            path_out = parameters_path_out[key] + dictonary['q14of23']
            infegyinstance.Whatemotionspeoplehavetalking(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            log.error("Rutine Q14F23: ")
            print e.message

    """ Question 12 of 23"""
    QuestionThirTeenDict = pr.getDictonarySection("Q13OF23")
    for key, value in parameters_path_in.iteritems():
        try:
            splt = QuestionThirTeenDict['topics'].split(",")
            p = ""
            for v in splt:
                p = parameters_path_in[key] + v + "," + p
            path_in = p[:-1]
            path_out = parameters_path_out[key] + dictonary['q13of23']
            infegyinstance.Howpeoplefeelaboutbrand(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            log.error("Rutine Q130F23: ")
            print e.message

    """ Question 10 of 23"""
    QuestionFifTeenDict = pr.getDictonarySection("Q15OF23")
    print "**** Q15OF23"
    print QuestionFifTeenDict
    for key, value in parameters_path_in.iteritems():
        try:
            path_in = parameters_path_in[key] + QuestionFifTeenDict['channels']
            path_out = parameters_path_out[key] + dictonary['q15of23']
            infegyinstance.Whereisconversationhappening(path_in, path_out)
            print path_in, '...', path_out
        except Exception as e:
            log.error("Rutine Q15OF23: ")
            print e.message
    print "calc of Q15OF23 finished"


    end_timestamp = time.time()

    save_output_results(start_timestamp, end_timestamp, dictonary['directory_out'] + "output-results.json")

    print "JWT-MAIN FINISHED"
