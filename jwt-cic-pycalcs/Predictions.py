# -*- coding: utf-8 -*-
import statsmodels.api as sm
import pandas as pd
from statsmodels.tsa.seasonal import seasonal_decompose
import statsmodels.tsa
import numpy as np
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.stattools import acf, pacf
import math
from matplotlib.pylab import rcParams

rcParams['figure.figsize'] = 15, 6
data = sm.datasets.co2.load_pandas()
co2 = data.data  ### En esta parte Ale solo hay que cambiar la base de entrada, podemos probar con los Json de Infegy y de Trends


##### Esta es la funcion diseñada para extraer lo de Seasonality

def seasonality_jwt(serie_usar):
    serie_usar = serie_usar.interpolate()
    decomposition = seasonal_decompose(serie_usar)
    seasonal = decomposition.seasonal
    print seasonal


##### Esta es la funcion diseñada para generar la predicción

def series_jwt(series_usar):
    series_usar = series_usar.interpolate()

    dftest = statsmodels.tsa.stattools.adfuller(series_usar["co2"], autolag='AIC')
    valores = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])

    if valores['p-value'] <= 0.1:

        lag_acf = acf(series_usar, nlags=20)
        lag_pacf = pacf(series_usar, nlags=20, method='ols')
        lag_pacf = np.array(lag_pacf).tolist()
        lag_pacf_true = lag_pacf > 1
        lag_pacf_true = lag_pacf_true.index("True")
        lag_pacf_true_pos = np.array(lag_pacf_true).tolist()
        lag_acf = np.array(lag_acf).tolist()
        lag_acf_true = lag_acf > 1
        lag_acf_true = lag_acf_true.index("True")
        lag_acf_true_pos = np.array(lag_acf_true).tolist()
        ar = max(lag_pacf_true_pos)
        ma = max(lag_acf_true_pos)
        model = ARIMA(series_usar, order=(ar, 0, ma))
        results_ARIMA = model.fit(disp=1)
        predictions_ARIMA = pd.Series(results_ARIMA.fittedvalues, copy=True)
        print predictions_ARIMA


    elif valores['p-value'] > 0.1:

        log_series_usar =np.log(series_usar['co2'])
        dftest = statsmodels.tsa.stattools.adfuller(log_series_usar, autolag='AIC')
        valores = pd.Series(dftest[0:4],
                            index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
        print valores
        if valores['p-value'] < 0.1:
            lag_acf = acf(log_series_usar, nlags=20)
            lag_pacf = pacf(log_series_usar, nlags=20, method='ols')
            lag_pacf = np.array(lag_pacf).tolist()
            lag_pacf_true = lag_pacf > 1
            lag_pacf_true = lag_pacf_true.index("True")
            lag_pacf_true_pos = np.array(lag_pacf_true).tolist()
            lag_acf = np.array(lag_acf).tolist()
            lag_acf_true = lag_acf > 1
            lag_acf_true = lag_acf_true.index("True")
            lag_acf_true_pos = np.array(lag_acf_true).tolist()
            ar = max(lag_pacf_true_pos)
            ma = max(lag_acf_true_pos)
            model = ARIMA(log_series_usar, order=(ar, 0, ma))
            results_ARIMA = model.fit(disp=1)
            predictions_ARIMA = pd.Series(results_ARIMA.fittedvalues, copy=True)
            print predictions_ARIMA

series_jwt(co2)