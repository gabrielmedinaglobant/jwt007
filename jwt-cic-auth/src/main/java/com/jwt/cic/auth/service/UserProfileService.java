package com.jwt.cic.auth.service;

import java.util.Optional;

import com.jwt.cic.auth.dto.UserProfileDTO;

/***
 * 
 * @author gabriel.medina
 *
 */
public interface UserProfileService {
	UserProfileDTO getFirstByNameIdWithRoles(String userNameId);
	UserProfileDTO getUserProfileDetails(String userNameId);
	UserProfileDTO requestUserProfileDetails(String userNameId, String firstName, String lastName, String email);
}
