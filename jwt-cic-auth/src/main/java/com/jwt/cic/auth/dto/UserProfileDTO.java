package com.jwt.cic.auth.dto;

import java.util.List;

/***
 * 
 * @author gabri
 *
 */
public class UserProfileDTO {
	private String userNameId;
	private String mail;
	private String role;
	private long projectId;
	private List<ProjectAssignmentDTO> projectAssignments;
	private String token;
	private boolean newUserCreated;
	
	public UserProfileDTO(String userNameId, String role) {
		this.userNameId = userNameId;
		this.role = role;
	}
	public UserProfileDTO(String userNameId, String role, long projectId,
			List<ProjectAssignmentDTO> projectAssignments) {
		this.userNameId = userNameId;
		this.role = role;
		this.projectId = projectId;
		this.projectAssignments = projectAssignments;
		this.newUserCreated = false;
	}

	public UserProfileDTO(String userNameId, String role, String mail, long projectId,
			List<ProjectAssignmentDTO> projectAssignments) {
		this.userNameId = userNameId;
		this.role = role;
		this.projectId = projectId;
		this.projectAssignments = projectAssignments;
		this.newUserCreated = false;
	}

	public UserProfileDTO() {
	}

	public String getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(String userNameId) {
		this.userNameId = userNameId;
	}
	
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public List<ProjectAssignmentDTO> getProjectAssignments() {
		return projectAssignments;
	}
	public void setProjectAssignments(List<ProjectAssignmentDTO> projectAssignments) {
		this.projectAssignments = projectAssignments;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isNewUserCreated() {
		return newUserCreated;
	}
	public void setNewUserCreated(boolean newUserCreated) {
		this.newUserCreated = newUserCreated;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
}
