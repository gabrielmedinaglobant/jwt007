package com.jwt.cic.auth.config;

import java.util.Arrays;
import java.util.Collection;

import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.pac4j.saml.client.SAML2Client;
import org.pac4j.saml.client.SAML2ClientConfiguration;

/**
 * 
 * PAC4J Configuration
 * 
 * @author gabriel.medina
 *
 */
@Configuration
public class Pac4jConfig {

	@Value("${jwt.cic.auth}")
	private String appUrl;
	
	@Bean
    public Config config() {
		
		final SAML2ClientConfiguration cfg = new SAML2ClientConfiguration("resource:jwt-cic-keystore.jks",
                "123456789",
                "123456789",
                "resource:metadata.xml");
		cfg.setKeystoreAlias("jwt-cic-keystore");
		cfg.setBlackListedSignatureSigningAlgorithms( blackListedSignatureAlgorithms() );

        cfg.setMaximumAuthenticationLifetime(3600);
        cfg.setServiceProviderEntityId(appUrl + "/callback?client_name=SAML2Client");
        // cfg.setServiceProviderMetadataPath("sp-metadata.xml");
        cfg.setDestinationBindingType(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
        final SAML2Client saml2Client = new SAML2Client(cfg);

        Clients clients = new Clients(appUrl+"/callback", saml2Client);
        Config config = new Config(clients);
                
        return config;
    }

	private Collection<String> blackListedSignatureAlgorithms() {
		return Arrays.asList(SignatureConstants.ALGO_ID_DIGEST_SHA512, SignatureConstants.ALGO_ID_DIGEST_SHA384, SignatureConstants.ALGO_ID_DIGEST_SHA256, SignatureConstants.ALGO_ID_DIGEST_SHA224 );
	}

	
	
}
