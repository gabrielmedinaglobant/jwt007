package com.jwt.cic.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jwt.cic.auth.ApplicationUrlHelper;
import com.jwt.cic.auth.dto.UserProfileDTO;
import com.jwt.cic.auth.service.UserProfileService;
import com.jwt.cic.auth.util.SAML2ProfileHelper;
import com.jwt.cic.auth.util.SAML2ProfileJWTAttributes;

@Controller
@RequestMapping("/auth/manager-app")
public class AuthManagerAppController {

	private static final Logger LOG = LoggerFactory.getLogger(AuthAppController.class);

	private static final String APPLICATION_NAME = "Common Information Center, Client Brand Manager";
	
	private UserProfileService userProfileService;
	private ApplicationUrlHelper applicationUrlHelper;
	private SAML2ProfileHelper sAML2ProfileHelper;
	
	@Autowired
	public AuthManagerAppController(UserProfileService userRoleService, ApplicationUrlHelper applicationUrlHelper, SAML2ProfileHelper sAML2ProfileHelper) {
		this.userProfileService = userRoleService;
		this.applicationUrlHelper = applicationUrlHelper;
		this.sAML2ProfileHelper = sAML2ProfileHelper;
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, value = {"/","/index",""})
	public String index(Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SAML2ProfileJWTAttributes attrs = sAML2ProfileHelper.getAttributesFromAuthentication(auth);
		UserProfileDTO userProfile = userProfileService.requestUserProfileDetails(attrs.getJwtUnique(), attrs.getFirstName(), attrs.getLastName(), attrs.getMail());
		
		model.addAttribute("username", auth.getName() );
		model.addAttribute("token", userProfile.getToken() );
		model.addAttribute("applicationName", APPLICATION_NAME );
		model.addAttribute("applicationUrl", applicationUrlHelper.getJwtCicManagerFrontendApplicationUrl(userProfile.getToken()) );
		model.addAttribute("isNewUserCreated", userProfile.isNewUserCreated() );
		
		return "auth-app-index";
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, value = {"/logout"})
	public String logout(Model model, HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){ 
			LOG.info(auth.getName() + "request a logout");
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		model.addAttribute("index", "/auth/manager-app");
		
		return "auth-app-logout";
	}
	
}
