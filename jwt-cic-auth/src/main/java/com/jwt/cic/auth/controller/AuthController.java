package com.jwt.cic.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



/***
 * 
 * @author gabriel.medina
 *
 */
@Controller
@RequestMapping("/auth")
public class AuthController {

	private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, value = {"/logout"})
	public String logout(HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){ 
			LOG.info(auth.getName() + "request a logout");
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		return "auth-logout";
	}
	
}
