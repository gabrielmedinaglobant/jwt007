package com.jwt.cic.auth.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.thymeleaf.spring.support.Layout;

import com.jwt.cic.auth.exception.UserWithoutRoleException;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ExceptionHandlerControllerAdvice.class);
	
	@ExceptionHandler(UserWithoutRoleException.class)
	public String handleUserWithoutRoleException(Model model){
		LOG.info("A user without a role has tried to access this application");
		model.addAttribute("view","exceptions/user-without-role");
		return "layouts/default";
	}
	
}
