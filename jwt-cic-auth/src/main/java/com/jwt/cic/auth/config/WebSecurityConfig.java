package com.jwt.cic.auth.config;

import org.pac4j.core.config.Config;
import org.pac4j.springframework.security.web.CallbackFilter;
import org.pac4j.springframework.security.web.Pac4jEntryPoint;
import org.pac4j.springframework.security.web.SecurityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private Config config;
	
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	
    	final SecurityFilter filter = new SecurityFilter(config);
		CallbackFilter callbackFilter = new CallbackFilter(config);
		callbackFilter.setMultiProfile(true);
		/*
        http
        	.authorizeRequests()
        		.antMatchers(method)
        	.antMatcher("/**").authorizeRequests().anyRequest().permitAll()
        	.and()
    		.antMatcher("/auth/app/go").authorizeRequests().anyRequest().authenticated()
    		.and()
    			.exceptionHandling().authenticationEntryPoint(new Pac4jEntryPoint(config, "Saml2Client"))
            .and()
		        .addFilterBefore(callbackFilter, BasicAuthenticationFilter.class)
		        .csrf().disable()
		        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
	        .and()
	            .logout()
	            	.logoutUrl("/auth/logout")
	                .logoutSuccessUrl("/");
        */
       
		http
	    	.authorizeRequests()
	    	.antMatchers("/auth/app").authenticated()
	    	.antMatchers("/auth/manager-app").authenticated()
    	.and()
    		.headers().frameOptions().sameOrigin()
    	.and()
    		.exceptionHandling().authenticationEntryPoint(new Pac4jEntryPoint(config, "Saml2Client"))
		.and()
	        .addFilterBefore(callbackFilter, BasicAuthenticationFilter.class)
	        .csrf().disable()
	        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        .and()
            .logout()
            	.logoutUrl("/logout")
                .logoutSuccessUrl("/");
		
    }
    
}
