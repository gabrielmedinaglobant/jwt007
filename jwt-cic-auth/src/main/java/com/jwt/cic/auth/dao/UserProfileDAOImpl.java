package com.jwt.cic.auth.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jwt.cic.auth.dto.UserProfileDTO;
import com.jwt.cic.auth.dto.ProjectAssignmentDTO;

/***
 * 
 * @author gabri
 *
 */
@Repository
public class UserProfileDAOImpl implements UserProfileDAO {

	private JdbcTemplate jdbcTemplate;
	
	
	@Value("${user.query}")
	private String userQuery;
	@Value("${user.role.query}")
	private String userRoleQuery;
	@Value("${user.new.query}")
	private String insertNewUserQuery;
	
	
	@Autowired
	public UserProfileDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Optional<UserProfileDTO> getFirstByNameId(String nameId) {
		return jdbcTemplate.query(userQuery, new Object[] { nameId }, (rs,rowNum) -> {
			UserProfileDTO dto = new UserProfileDTO();
			List<ProjectAssignmentDTO> projectAssignmentsDtoList = new ArrayList<>();
			
			dto.setUserNameId( rs.getString("name_id") );
			dto.setMail( rs.getString("email") );
			dto.setRole( null ); 
			dto.setProjectId( 0 );
			dto.setProjectAssignments(projectAssignmentsDtoList);
			
			return dto;
		}).stream().findFirst();
	}
	
	@Override
	public Optional<UserProfileDTO> getFirstByNameIdWithRoles(String nameId) {
		// TODO: Fetch all projects
		return jdbcTemplate.query(userRoleQuery, new Object[] { nameId }, (rs,rowNum) -> {
			UserProfileDTO dto = new UserProfileDTO();
			List<ProjectAssignmentDTO> projectAssignmentsDtoList = new ArrayList<>();
			
			dto.setUserNameId( rs.getString("name_id") );
			dto.setMail( rs.getString("email") );
			dto.setRole( rs.getString("role") ); 
			dto.setProjectId( rs.getLong("project_id") );
			
			projectAssignmentsDtoList.add(new ProjectAssignmentDTO(rs.getLong("project_id"), rs.getString("role")));
			dto.setProjectAssignments(projectAssignmentsDtoList);
			
			return dto;
		}).stream().findFirst();
	}

	@Override
	public void saveNewUser(String nameId, String firstName, String lastName, String email) {
		LocalDateTime currentTime = LocalDateTime.now();
		jdbcTemplate.update(insertNewUserQuery, nameId, firstName, lastName, email, Timestamp.valueOf(currentTime), Timestamp.valueOf(currentTime), true);
	}
		
}
