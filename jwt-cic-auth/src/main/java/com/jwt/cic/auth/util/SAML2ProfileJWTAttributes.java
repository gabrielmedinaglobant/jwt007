package com.jwt.cic.auth.util;

/***
 * 
 * Value holder class for SAML2 Attributes from myJWT
 * 
 * @author gabriel.medina
 *
 */
public class SAML2ProfileJWTAttributes {
	private String uid;
	private String mail;
	private String firstName;
	private String lastName;
	private String jwtUnique;
	
	public SAML2ProfileJWTAttributes(String uid, String mail, String firstName, String lastName, String jwtUnique) {
		this.uid = uid;
		this.mail = mail;
		this.firstName = firstName;
		this.lastName = lastName;
		this.jwtUnique = jwtUnique;
	}
	
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getJwtUnique() {
		return jwtUnique;
	}
	public void setJwtUnique(String jwtUnique) {
		this.jwtUnique = jwtUnique;
	}
	
	
}