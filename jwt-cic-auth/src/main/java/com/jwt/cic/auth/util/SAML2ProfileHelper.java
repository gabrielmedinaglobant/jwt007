package com.jwt.cic.auth.util;

import java.util.ArrayList;

import org.pac4j.saml.profile.SAML2Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/***
 * 
 * Helper calls to get attributes from Spring Security Authentication as SAML2Profile from myJWT SAML Responses
 * 
 * @author gabriel.medina
 *
 */
@Component
public class SAML2ProfileHelper {

	@SuppressWarnings("unchecked")
	public SAML2ProfileJWTAttributes getAttributesFromAuthentication(Authentication auth){
		SAML2Profile sAML2Profile = (SAML2Profile)auth.getPrincipal();
		
		String uid = ((ArrayList<String>)sAML2Profile.getAttribute("uid")).get(0);
		String mail = ((ArrayList<String>)sAML2Profile.getAttribute("mail")).get(0);
		String firstName = ((ArrayList<String>)sAML2Profile.getAttribute("firstName")).get(0);
		String lastName = ((ArrayList<String>)sAML2Profile.getAttribute("lastName")).get(0);
		String jwtUnique = ((ArrayList<String>)sAML2Profile.getAttribute("jwt-unique")).get(0);
		
		SAML2ProfileJWTAttributes attributes = new SAML2ProfileJWTAttributes(uid, mail, firstName, lastName, jwtUnique);
		
		return attributes;
	}	
	
}
