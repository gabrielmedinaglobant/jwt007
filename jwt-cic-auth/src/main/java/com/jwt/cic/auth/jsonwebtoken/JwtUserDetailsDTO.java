package com.jwt.cic.auth.jsonwebtoken;

public class JwtUserDetailsDTO {
	private String nameId;
	private String role;
	
	public JwtUserDetailsDTO(){
		
	}
	public JwtUserDetailsDTO(String nameId, String role) {
		this.nameId = nameId;
		this.role = role;
	}
	
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JwtUserDetailsDTO [nameId=").append(nameId).append(", role=").append(role).append("]");
		return builder.toString();
	}
	
}
