package com.jwt.cic.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/***
 * 
 * @author gabriel.medina
 *
 */
@Component
public class ApplicationUrlHelper {
	
	@Value("${jwt.cic.frontend}")
	private String jwtCicFrontendApplicationUrl;
	@Value("${jwt.cic.manager-frontend}")
	private String jwtCicManagerFrontendApplicationUrl;
	@Value("${jwt.cic.frontend.token-receiver}")
	private String jwtCicFrontendTokenReceiver;
	@Value("${jwt.cic.manager-frontend.token-receiver}")
	private String jwtCicManagerFrontendTokenReceiver;
	
	public String getJwtCicFrontendApplicationUrl(String token){
		return jwtCicFrontendApplicationUrl + jwtCicFrontendTokenReceiver + "?token=" + token;
	}
	
	public String getJwtCicManagerFrontendApplicationUrl(String token){
		return jwtCicManagerFrontendApplicationUrl + jwtCicManagerFrontendTokenReceiver + "?token=" + token;
	}
	
}
