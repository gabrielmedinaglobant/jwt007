package com.jwt.cic.auth.dao;

import java.util.Optional;

import com.jwt.cic.auth.dto.UserProfileDTO;

/***
 * 
 * @author gabri
 *
 */
public interface UserProfileDAO {
	Optional<UserProfileDTO> getFirstByNameId(String nameId);
	void saveNewUser(String nameId, String firstName, String lastName, String email);
	Optional<UserProfileDTO> getFirstByNameIdWithRoles(String nameId);
}
