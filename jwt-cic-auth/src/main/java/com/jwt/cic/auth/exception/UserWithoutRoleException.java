package com.jwt.cic.auth.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class UserWithoutRoleException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
