package com.jwt.cic.auth.jsonwebtoken;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.auth.dto.UserProfileDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String secret;
    private static ObjectMapper mapper = new ObjectMapper();
    
    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     * 
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public UserProfileDTO parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            UserProfileDTO u = new UserProfileDTO();
            u.setUserNameId( body.get("nameId", String.class) );
            u.setMail( body.get("email", String.class) );
            u.setRole( body.get("role", String.class) );
            // TODO: Project assignments
            
            return u;

        } catch (JwtException | ClassCastException e) {
        	e.printStackTrace();
            throw new JwtParsingException();
        }
    }
    
	/**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     * 
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(UserProfileDTO u) {
        Claims claims = Jwts.claims().setSubject(u.getUserNameId());
        
        claims.put("email", u.getMail() + "");
        claims.put("nameId", u.getUserNameId() + "");
        claims.put("role", u.getRole());
        claims.put("projectId", u.getProjectId());
        claims.put("projectAssignments", u.getProjectAssignments());
        
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
}
