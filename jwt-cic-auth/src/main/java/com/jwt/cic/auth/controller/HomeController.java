package com.jwt.cic.auth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Optional;
import com.jwt.cic.auth.ApplicationUrlHelper;
import com.jwt.cic.auth.dto.UserProfileDTO;
import com.jwt.cic.auth.service.UserProfileService;
import com.jwt.cic.auth.util.SAML2ProfileHelper;
import com.jwt.cic.auth.util.SAML2ProfileJWTAttributes;

@Controller
@RequestMapping("/")
public class HomeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);
	
	private final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	
	private UserProfileService userRoleService;
	private ApplicationUrlHelper applicationUrlHelper;
	private SAML2ProfileHelper sAML2ProfileHelper;
	
	@Autowired
	public HomeController(UserProfileService userRoleService, ApplicationUrlHelper applicationUrlHelper, SAML2ProfileHelper sAML2ProfileHelper) {
		this.userRoleService = userRoleService;
		this.applicationUrlHelper = applicationUrlHelper;
		this.sAML2ProfileHelper = sAML2ProfileHelper;
	}
	
	@RequestMapping(method = { RequestMethod.GET }, value = {"/","/home","/index",""})
	public String index(Model model){
		
		//Gets a JWT if user is SAML authenticated
		if(!isAnonymous()){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			SAML2ProfileJWTAttributes attrs = sAML2ProfileHelper.getAttributesFromAuthentication(auth);
			UserProfileDTO userProfile = userRoleService.getUserProfileDetails(attrs.getJwtUnique());
			model.addAttribute("token", userProfile.getToken() );
			model.addAttribute("jwtCicFrontendApplicationUrl", applicationUrlHelper.getJwtCicFrontendApplicationUrl( userProfile.getToken() ));
		
			return "index";
		}
		
		else {
			return "redirect:auth/app"; //redirects to "redirect:auth/app" if not logged in, where myJWT is going to be prompt
		}
	}
	
	private boolean isAnonymous(){
		return SecurityContextHolder.getContext()
		.getAuthentication().getAuthorities()
		.stream()
		.filter( (ga) -> { return ga.getAuthority().compareTo(ROLE_ANONYMOUS) == 0; } )
		.findFirst()
		.isPresent();
	}
	
}


