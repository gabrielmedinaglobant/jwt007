package com.jwt.cic.auth.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.auth.controller.ExceptionHandlerControllerAdvice;
import com.jwt.cic.auth.dao.UserProfileDAO;
import com.jwt.cic.auth.dto.UserProfileDTO;
import com.jwt.cic.auth.exception.UserWithoutRoleException;
import com.jwt.cic.auth.jsonwebtoken.JwtUtil;

/***
 * 
 * @author gabriel.medina
 *
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(UserProfileServiceImpl.class);
	
	private UserProfileDAO userProfileDAO;
	private JwtUtil jwtUtil;
	
	@Autowired
	public UserProfileServiceImpl(UserProfileDAO userProfileDAO, JwtUtil jwtUtil) {
		this.userProfileDAO = userProfileDAO;
		this.jwtUtil = jwtUtil;
	}

	@Override
	public UserProfileDTO requestUserProfileDetails(String userNameId, String firstName, String lastName, String email) {
		LOG.info("Token generation for user with [nameId=" + userNameId + "] was requested. If user doesn't exist, will be created on DB");
		
		//check that username is in db
		Optional<UserProfileDTO> user = userProfileDAO.getFirstByNameId(userNameId);
		String token;
		
		//if not, create a new user and return null
		if(user.isPresent()){
			//check if it has roles
			token = jwtUtil.generateToken( getFirstByNameIdWithRoles(userNameId) );
			user.get().setToken(token);
			user.get().setNewUserCreated(false);
		} else {
			createUser(userNameId, firstName, lastName, email);
			user = Optional.of(new UserProfileDTO());
			user.get().setUserNameId(userNameId);
			user.get().setToken(null);
			user.get().setNewUserCreated(true);
		}
		
		return user.get();
	}


	@Override
	public UserProfileDTO getUserProfileDetails(String userNameId) {
		LOG.info("Token generation for user with [uid=" + userNameId + "] was requested");
		
		UserProfileDTO userProfile = getFirstByNameIdWithRoles(userNameId);
		userProfile.setToken( jwtUtil.generateToken( userProfile ) );
		userProfile.setNewUserCreated(false);
		
		return userProfile;
	}
	
	@Override
	public UserProfileDTO getFirstByNameIdWithRoles(String userNameId) {
		Optional<UserProfileDTO> result = userProfileDAO.getFirstByNameIdWithRoles(userNameId);
		
		if( !result.isPresent() ){
			LOG.info(userNameId + " was not found in repository");
			throw new UserWithoutRoleException();
		}
		
		return result.get();
	}
	
	protected void createUser(String userNameId, String firstName, String lastName, String email){
		userProfileDAO.saveNewUser(userNameId, firstName, lastName, email);
	}

	
}
