package com.jwt.cic.auth.dto;

/**
 * 
 * @author gabri
 *
 */
public class ProjectAssignmentDTO {
	private long projectId;
	private String role;
	
	public ProjectAssignmentDTO(long projectId, String role) {
		this.projectId = projectId;
		this.role = role;
	}
	
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
