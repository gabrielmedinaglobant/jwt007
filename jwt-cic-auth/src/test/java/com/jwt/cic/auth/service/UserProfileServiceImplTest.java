package com.jwt.cic.auth.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.jwt.cic.auth.dao.UserProfileDAO;
import com.jwt.cic.auth.dto.ProjectAssignmentDTO;
import com.jwt.cic.auth.dto.UserProfileDTO;
import com.jwt.cic.auth.exception.UserWithoutRoleException;
import com.jwt.cic.auth.jsonwebtoken.JwtUtil;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserProfileServiceImplTest {
	
	@Mock
	private UserProfileDAO userProfileDAO;
	@Mock
	private JwtUtil jwtUtil;
	
	@InjectMocks
	private UserProfileServiceImpl userProfileServiceImpl; 
	
	
	@Test
	public void testRequestUserProfileDetailsNoUserInDatabase(){
		
		// Arrange
		UserProfileDTO expectedUserProfileDTO = buildExpectedUserProfileDTO();
		
		//user not in database
		when(userProfileDAO.getFirstByNameId("mocked.user@gmail.com")).thenReturn(Optional.ofNullable(null));
		
		// Act
		UserProfileDTO resultUserProfileDTO = userProfileServiceImpl.requestUserProfileDetails("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com");
		
		// Assert
		// Verify that an user is not in the database, then save it as a new user
		verify(userProfileDAO).getFirstByNameId("mocked.user@gmail.com"); // Verify that a find call is done
		verify(userProfileDAO).saveNewUser("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com"); // Verifies that a save new user call is done
		assertTrue(resultUserProfileDTO.getUserNameId() == expectedUserProfileDTO.getUserNameId());
	}
	
	@Test(expected=UserWithoutRoleException.class)
	public void testRequestUserProfileDetailsUserInDatabaseNoRoles(){
		
		// Arrange
		UserProfileDTO expectedUserProfileDTO = buildExpectedUserProfileDTOWithNoRoles();
		//user in database
		when(userProfileDAO.getFirstByNameId("mocked.user@gmail.com")).thenReturn(Optional.of(expectedUserProfileDTO));
		when(userProfileDAO.getFirstByNameIdWithRoles("mocked.user@gmail.com")).thenReturn(Optional.ofNullable(null));
		
		// Act
		UserProfileDTO resultUserProfileDTO = userProfileServiceImpl.requestUserProfileDetails("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com");
		
		// Assert
		// Verify that an user is not in the database, then save it as a new user
		verify(userProfileDAO).getFirstByNameId("mocked.user@gmail.com"); // Verify that a find call is done
		verify(userProfileDAO, times(0) ).saveNewUser("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com"); // Verifies that a save new user call is NOT done
		
	}
	
	@Test
	public void testRequestUserProfileDetailsUserInDatabaseWithRoles(){
		
		// Arrange
		UserProfileDTO expectedUserProfileDTO = buildExpectedUserProfileDTO();
		//user in database
		when(userProfileDAO.getFirstByNameId("mocked.user@gmail.com")).thenReturn(Optional.of(expectedUserProfileDTO));
		when(userProfileDAO.getFirstByNameIdWithRoles("mocked.user@gmail.com")).thenReturn(Optional.of(expectedUserProfileDTO));
		
		// Act
		UserProfileDTO resultUserProfileDTO = userProfileServiceImpl.requestUserProfileDetails("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com");
		
		// Assert
		// Verify that an user is in the database and it has roles
		verify(userProfileDAO).getFirstByNameId("mocked.user@gmail.com"); // Verify that a find call is done
		verify(userProfileDAO, times(0) ).saveNewUser("mocked.user@gmail.com", "Mocked", "User", "mocked.user@gmail.com"); // Verifies that a save new user call is NOT done
		assertTrue(resultUserProfileDTO.getUserNameId() == expectedUserProfileDTO.getUserNameId());
		
	}
	
	private UserProfileDTO buildExpectedUserProfileDTOWithNoRoles() {
		UserProfileDTO expectedUserProfileDTO = new UserProfileDTO();
		expectedUserProfileDTO.setProjectId(1);
		expectedUserProfileDTO.setRole("ROLE_ANALYST");
		expectedUserProfileDTO.setToken("");
		expectedUserProfileDTO.setUserNameId("mocked.user@gmail.com");
		expectedUserProfileDTO.setProjectAssignments( new ArrayList<ProjectAssignmentDTO>() );
		return expectedUserProfileDTO;
	}
	
	private UserProfileDTO buildExpectedUserProfileDTO() {
		UserProfileDTO expectedUserProfileDTO = new UserProfileDTO();
		expectedUserProfileDTO.setProjectId(1);
		expectedUserProfileDTO.setRole("ROLE_ANALYST");
		expectedUserProfileDTO.setToken("");
		expectedUserProfileDTO.setUserNameId("mocked.user@gmail.com");
		expectedUserProfileDTO.setProjectAssignments( buildProjectAssigments() );
		return expectedUserProfileDTO;
	}
	
	private List<ProjectAssignmentDTO> buildProjectAssigments() {

		List<ProjectAssignmentDTO> list = new ArrayList<ProjectAssignmentDTO>(9);
		
		list.add(new ProjectAssignmentDTO(1,"ROLE_ANALYST"));
		list.add(new ProjectAssignmentDTO(2,"ROLE_STRATEGIC"));
		list.add(new ProjectAssignmentDTO(3,"ROLE_ANALYST"));
		list.add(new ProjectAssignmentDTO(4,"ROLE_STRATEGIC"));

		return list;
	}
	
	
	
}
