/*
	JWT - Command Information Center
	Database Schema for MySQL
	V1__initial_ddl.sql
    Last update 2017 - 06 - 19
    
*/

-- Create Tables

---- Configuration parameters table

CREATE TABLE configuration_parameter (id bigint(19) NOT NULL AUTO_INCREMENT, keyname varchar(254) NOT NULL UNIQUE, value varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));

---- Authentication & Authorization 

CREATE TABLE jwt_user (id bigint(19) NOT NULL AUTO_INCREMENT, name_id varchar(254) NOT NULL UNIQUE, name varchar(254) NOT NULL, last_name varchar(254) NOT NULL, email varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE role (id bigint(19) NOT NULL AUTO_INCREMENT, display_name varchar(254) NOT NULL, value varchar(64) NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));

---- Command Center

CREATE TABLE workspace (user_project_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, name varchar(254) NOT NULL, loaded_by_default tinyint(1) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE screen (workspace_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, name varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE screen_attachment (screen_id bigint(19) NOT NULL, screen_element_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE screen_element (id bigint(19) NOT NULL AUTO_INCREMENT, keyname varchar(254) NOT NULL UNIQUE, screen_element_type_id bigint(19) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE screen_element_type (id bigint(19) NOT NULL AUTO_INCREMENT, keyname varchar(254) NOT NULL UNIQUE, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));

---- Client Mapping

CREATE TABLE client (id bigint(19) NOT NULL AUTO_INCREMENT, uuid varchar(36) NOT NULL UNIQUE, name varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE brand (client_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, uuid varchar(36) NOT NULL UNIQUE, name varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE project (brand_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, uuid varchar(36) NOT NULL UNIQUE, name varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE user_project (project_id bigint(19) NOT NULL, user_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, role_id bigint(19) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));

---- Question Queries

CREATE TABLE jwt_query (query_type_id bigint(19) NOT NULL, project_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, query_string varchar(4000) NOT NULL, query_withtin_string varchar(4000), dirty tinyint(1) NOT NULL, last_execution datetime NULL, last_execution_successful tinyint(1), last_execution_message varchar(4000), date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE query_file (query_type_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, input_filename varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE query_type (id bigint(19) NOT NULL AUTO_INCREMENT, keyname varchar(254) NOT NULL UNIQUE, description varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));

---- Third Party Source Crenditials (Deprecated)

CREATE TABLE source (id bigint(19) NOT NULL AUTO_INCREMENT, keyname varchar(16) NOT NULL UNIQUE, name varchar(254) NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));
CREATE TABLE source_credential (user_id bigint(19) NOT NULL, source_id bigint(19) NOT NULL, id bigint(19) NOT NULL AUTO_INCREMENT, username varchar(254) NOT NULL, password varchar(254) NOT NULL, date_created datetime NOT NULL, date_updated datetime NOT NULL, active tinyint(1) NOT NULL, PRIMARY KEY (id));


-- Create Foreign Keys

ALTER TABLE brand ADD INDEX fk_client_brand (client_id), ADD CONSTRAINT fk_client_brand FOREIGN KEY (client_id) REFERENCES client (id);
ALTER TABLE jwt_query ADD INDEX fk_project_query (project_id), ADD CONSTRAINT fk_project_query FOREIGN KEY (project_id) REFERENCES project (id);
ALTER TABLE jwt_query ADD INDEX fk_query_type_query (query_type_id), ADD CONSTRAINT fk_query_type_query FOREIGN KEY (query_type_id) REFERENCES query_type (id);
ALTER TABLE project ADD INDEX fk_brand_project (brand_id), ADD CONSTRAINT fk_brand_project FOREIGN KEY (brand_id) REFERENCES brand (id);
ALTER TABLE query_file ADD INDEX fk_query_type_q_file (query_type_id), ADD CONSTRAINT fk_query_type_q_file FOREIGN KEY (query_type_id) REFERENCES query_type (id);
ALTER TABLE screen ADD INDEX fk_workspace_screen (workspace_id), ADD CONSTRAINT fk_workspace_screen FOREIGN KEY (workspace_id) REFERENCES workspace (id);
ALTER TABLE screen_attachment ADD INDEX fk_screen_attachment_s_element (screen_element_id), ADD CONSTRAINT fk_screen_attachment_s_element FOREIGN KEY (screen_element_id) REFERENCES screen_element (id);
ALTER TABLE screen_attachment ADD INDEX fk_screen_s_attachment (screen_id), ADD CONSTRAINT fk_screen_s_attachment FOREIGN KEY (screen_id) REFERENCES screen (id);
ALTER TABLE screen_element ADD INDEX fk_screen_element_s_e_type (screen_element_type_id), ADD CONSTRAINT fk_screen_element_s_e_type FOREIGN KEY (screen_element_type_id) REFERENCES screen_element_type (id);
ALTER TABLE source_credential ADD INDEX fk_source_source_credential (source_id), ADD CONSTRAINT fk_source_source_credential FOREIGN KEY (source_id) REFERENCES source (id);
ALTER TABLE source_credential ADD INDEX fk_user_source_credential (user_id), ADD CONSTRAINT fk_user_source_credential FOREIGN KEY (user_id) REFERENCES jwt_user (id);
ALTER TABLE user_project ADD INDEX fk_project_user_project (project_id), ADD CONSTRAINT fk_project_user_project FOREIGN KEY (project_id) REFERENCES project (id);
ALTER TABLE user_project ADD INDEX fk_role_user_project (role_id), ADD CONSTRAINT fk_role_user_project FOREIGN KEY (role_id) REFERENCES role (id);
ALTER TABLE user_project ADD INDEX fk_user_user_project (user_id), ADD CONSTRAINT fk_user_user_project FOREIGN KEY (user_id) REFERENCES jwt_user (id);
ALTER TABLE workspace ADD INDEX fk_user_project_workspace (user_project_id), ADD CONSTRAINT fk_user_project_workspace FOREIGN KEY (user_project_id) REFERENCES user_project (id);

-- Create views

CREATE OR REPLACE VIEW vw_queries_for_output AS
	SELECT 
		p.uuid project_uuid,
		qt.keyname query_type,
		_url.value infegy_api_url,
		_apikey.value infegy_api_key,
        qf.input_filename filename,
		q.query_string query_string,
		q.query_withtin_string query_withtin_string
	FROM project p
		INNER JOIN jwt_query q ON q.project_id = p.id AND q.active = 1 
		INNER JOIN query_type qt ON qt.id = q.query_type_id AND qt.active = 1 
		INNER JOIN query_file qf ON qf.query_type_id = qt.id AND qf.active = 1 
		INNER JOIN configuration_parameter _url ON _url.keyname = 'infegy-api-url' AND _url.active = 1 
		INNER JOIN configuration_parameter _apikey ON _apikey.keyname = 'infegy-api-key' AND _apikey.active = 1 
	WHERE p.active = 1;
	
CREATE OR REPLACE VIEW vw_query_types_per_project AS 
		SELECT 
			p.uuid project_uuid,
			qt.keyname query_type,
			_url.value infegy_api_url,
			_apikey.value infegy_api_key,
			q.query_string query_string,
			q.query_withtin_string query_withtin_string
		FROM project p
			INNER JOIN jwt_query q ON q.project_id = p.id AND q.active = 1 
			INNER JOIN query_type qt ON qt.id = q.query_type_id AND qt.active = 1 
			INNER JOIN configuration_parameter _url ON _url.keyname = 'infegy-api-url' AND _url.active = 1 
			INNER JOIN configuration_parameter _apikey ON _apikey.keyname = 'infegy-api-key' AND _apikey.active = 1 
		WHERE p.active = 1;
    
CREATE OR REPLACE VIEW vw_query_files_of_query_types AS 
	SELECT 
		qt.keyname query_type,
		qf.input_filename filename
	FROM query_type qt
		INNER JOIN query_file qf ON qf.query_type_id = qt.id AND qf.active = 1 
	WHERE qt.active = 1;