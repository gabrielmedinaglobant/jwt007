/*
	JWT - Command Information Center
	Database Schema for MySQL
	V3__view_with_brand.sql
	Version 0.0.30
    Last update 2017 - 06 - 12
    
*/

CREATE OR REPLACE VIEW vw_query_types_per_project AS 
        SELECT 
            p.uuid project_uuid,
            qt.keyname query_type,
            _url.value infegy_api_url,
            _apikey.value infegy_api_key,
            q.query_string query_string,
            q.query_withtin_string query_withtin_string,
            SUBSTRING_INDEX(
            REPLACE(
            REPLACE(
            REPLACE(
            REPLACE(
            REPLACE(
            REPLACE(
                 q.query_string
            ,'"','')
            ,')','') 
            ,'#','') 
            ,'AND','') 
            ,'OR','') 
            ,'(','') 
            , ' ', 1) 
            brand_name
        FROM project p
            INNER JOIN jwt_query q ON q.project_id = p.id AND q.active = 1 
            INNER JOIN query_type qt ON qt.id = q.query_type_id AND qt.active = 1 
            INNER JOIN configuration_parameter _url ON _url.keyname = 'infegy-api-url' AND _url.active = 1 
            INNER JOIN configuration_parameter _apikey ON _apikey.keyname = 'infegy-api-key' AND _apikey.active = 1 
        WHERE p.active = 1;
		
UPDATE query_file SET active = 0;

UPDATE query_file SET active = 1 WHERE input_filename in ('volume.json','sentiment.json','themes.json','gender.json','languages.json','ages.json','demographics-meta.json','income.json','household-value.json','home-ownership.json','education.json','interests.json','post-interests.json','topics.json','emotions.json','channels.json');

