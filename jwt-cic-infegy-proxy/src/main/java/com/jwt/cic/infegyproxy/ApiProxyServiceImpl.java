package com.jwt.cic.infegyproxy;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiProxyServiceImpl implements ApiProxyService {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ApiProxyServiceImpl.class);
	private static final String ATLAS_API_URL = "https://atlas.infegy.com/api/v2/{infegy-endpoint}?api_key={api_key}&query={query}&start_date={start_date}&end_date={end_date}";
	
	@Autowired
	private CacheManager cacheManager;
	
	@Override
	@Cacheable("call-atlas-api")
	public String callAtlasApi(String infegyEndpoint, String apiKey, String query, String startDate, String endDate) {
		RestTemplate restTemplate = new RestTemplate();
		
		Map<String, String> uriVariables = new HashMap<>();
		uriVariables.put("infegy-endpoint", infegyEndpoint);
		uriVariables.put("api_key", apiKey);
		uriVariables.put("query", query);
		uriVariables.put("start_date", startDate);
		uriVariables.put("end_date", endDate);
		log.info("Requesting: " + ATLAS_API_URL);
		log.info(infegyEndpoint);
		log.info(apiKey);
		log.info(query);
		log.info(startDate);
		log.info(endDate);
		ResponseEntity<String> response = restTemplate.getForEntity(ATLAS_API_URL, String.class, uriVariables);

		return response.getBody();
	}
	
	@Scheduled(cron="0 55 5 * * MON-FRI", zone="UTC")
	public void refreshCache(){
		cacheManager
			.getCacheNames()
			.parallelStream()
			.forEach(name -> cacheManager.getCache(name).clear());		
		log.info("All cache was cleared");
	}
	
}
