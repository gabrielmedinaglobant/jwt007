package com.jwt.cic.infegyproxy;

public interface ApiProxyService {
	String callAtlasApi(String infegyEndpoint, String apiKey, String query, String startDate, String endDate);
	void refreshCache();
}
