package com.jwt.cic.infegyproxy;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/v2")
public class Controller {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Controller.class);
	
	private ApiProxyService jwtCicInfegymockService;
	
	@Autowired
	public Controller(ApiProxyService jwtCicInfegymockService) {
		this.jwtCicInfegymockService = jwtCicInfegymockService;
	}

	
	@RequestMapping(method = RequestMethod.GET, value = { "/{infegy-endpoint}" })
	public ResponseEntity<String> endpoint(
			@PathVariable("infegy-endpoint") String infegyEndpoint,
			@RequestParam(value = "api_key", defaultValue = "5hiyunvwb2ipldh3", required = false) String apiKey,
			@RequestParam(value = "query", required = false) String query,
			@RequestParam(value = "start_date", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "end_date", defaultValue = "", required = false) String endDate,
			HttpServletResponse response) {

		String jsonBody = jwtCicInfegymockService.callAtlasApi(infegyEndpoint, apiKey, query, startDate, endDate);
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

		return ResponseEntity.ok()
	        .headers(headers)
	        .body(jsonBody);
	}
	

}
