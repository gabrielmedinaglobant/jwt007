package com.jwt.cic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Error while fetching a resource") 
public class FetchingResourceException extends RuntimeException {

}