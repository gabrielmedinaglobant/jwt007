package com.jwt.cic.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Invalid data from source") 
public class InvalidJsonFromDataSourceException extends RuntimeException {

}
