package com.jwt.cic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Client name already exists") 
public class ClientNameAlreadyExistsException extends RuntimeException {

}
