package com.jwt.cic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Client contains Brands") 
public class ClientContainsBrandsException extends RuntimeException {

}
