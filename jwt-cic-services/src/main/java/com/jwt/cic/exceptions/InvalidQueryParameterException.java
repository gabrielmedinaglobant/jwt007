package com.jwt.cic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Invalid query parameter") 
public class InvalidQueryParameterException extends RuntimeException {

}
