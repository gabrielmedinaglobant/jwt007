package com.jwt.cic.questions.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.questions.dto.ConversationHappeningEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * 
 * Created by vishal.domale on 11-04-2017.
 */
@Primary
@Repository
public class ConversationHappeningDAOAWSImpl implements ConversationHappeningDAO {


    private ObjectMapper mapper = new ObjectMapper();

    private ResourceFetcher resourceFetcher;
    @Value("${question.s3.bucket}")
    private String bucketLocation;
    @Value("${question.s3.bucket.conversation-happening.json}")
    private String resourceLocation;

    @Autowired
    public ConversationHappeningDAOAWSImpl(ResourceFetcher resourceFetcher) {
        this.resourceFetcher = resourceFetcher;
    }

    @Override
    public ConversationHappeningEtlResultDTO get() {        
        return fetchData(bucketLocation + resourceLocation);
    }
    
    @Override
    public ConversationHappeningEtlResultDTO get(String projectUuid) {        
        return fetchData(bucketLocation + projectUuid + "/output/" + resourceLocation);
    }
    
    protected final ConversationHappeningEtlResultDTO fetchData(String location){
    	return resourceFetcher.fetchObject(location, ConversationHappeningEtlResultDTO.class);
    }
    
}
