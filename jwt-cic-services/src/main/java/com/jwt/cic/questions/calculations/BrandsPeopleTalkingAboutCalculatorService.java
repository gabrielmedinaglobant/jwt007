package com.jwt.cic.questions.calculations;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO;
import com.jwt.cic.questions.infegy.dao.SentimentDAO;
import com.jwt.cic.questions.infegy.dto.SentimentDTO;

@Service
public class BrandsPeopleTalkingAboutCalculatorService {
	
	private BrandsPeopleTalkingAboutStrategy brandsPeopleTalkingAboutStrategy;
	private SentimentDAO sentimentDAO;
	private ProjectDAO projectDAO;
	
	@Autowired
	public BrandsPeopleTalkingAboutCalculatorService(BrandsPeopleTalkingAboutStrategy brandsPeopleTalkingAboutStrategy,
			SentimentDAO sentimentDAO, ProjectDAO projectDAO) {
		this.brandsPeopleTalkingAboutStrategy = brandsPeopleTalkingAboutStrategy;
		this.sentimentDAO = sentimentDAO;
		this.projectDAO = projectDAO;
	}
	
	public BrandsPeopleTalkingAboutResultDTO getCalculatedResult(long projectId){
		Map<String, SentimentDTO> sentiments = new HashMap<>();
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		
		SentimentDTO sentimentDTO = sentimentDAO.get(project.getUuid(), "infegy-my-brand");
		String myBrand = getBrandNameFromQuery( sentimentDTO.getQueryInfo().getQuery() );
		sentiments.put( myBrand, sentimentDTO );
		
		sentimentDTO = sentimentDAO.get(project.getUuid(), "infegy-competitor-1");
		sentiments.put(getBrandNameFromQuery( sentimentDTO.getQueryInfo().getQuery() ), sentimentDTO);
		sentimentDTO = sentimentDAO.get(project.getUuid(), "infegy-competitor-2");
		sentiments.put(getBrandNameFromQuery( sentimentDTO.getQueryInfo().getQuery() ), sentimentDTO);
		sentimentDTO = sentimentDAO.get(project.getUuid(), "infegy-competitor-3");
		sentiments.put(getBrandNameFromQuery( sentimentDTO.getQueryInfo().getQuery() ), sentimentDTO);
		
		return brandsPeopleTalkingAboutStrategy.calculate(myBrand, sentiments);
	}
	
	private String getBrandNameFromQuery(String query){
		return query.replace("AND", "").replace("OR", "").replace("(", "").replace(")", "").replace("#", "").replace("\"", "").replace(".", "").replace(",", "").replace("?", "").replace("!","").split(" ")[0];
	}
	
}
