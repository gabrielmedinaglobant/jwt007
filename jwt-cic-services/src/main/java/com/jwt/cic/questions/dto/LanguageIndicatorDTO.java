package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class LanguageIndicatorDTO {
	private String languageIso3Code;
	private String languageIso2Code;
	private String languageName;
	private BigDecimal value;
	private String unit;
	
	
	public LanguageIndicatorDTO(String languageIso3Code, String languageIso2Code, String languageName, BigDecimal value,
			String unit) {
		this.languageIso3Code = languageIso3Code;
		this.languageIso2Code = languageIso2Code;
		this.languageName = languageName;
		this.value = value;
		this.unit = unit;
	}
	
	public LanguageIndicatorDTO() {
	}

	public String getLanguageIso3Code() {
		return languageIso3Code;
	}
	public void setLanguageIso3Code(String languageIso3Code) {
		this.languageIso3Code = languageIso3Code;
	}
	public String getLanguageIso2Code() {
		return languageIso2Code;
	}
	public void setLanguageIso2Code(String languageIso2Code) {
		this.languageIso2Code = languageIso2Code;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
}
