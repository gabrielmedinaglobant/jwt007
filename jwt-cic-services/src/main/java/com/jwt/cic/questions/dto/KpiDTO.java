package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

@Deprecated
public class KpiDTO {
	private String key;
	private IndicatorTrend trend;
	private BigDecimal value;
	private String title;
	private String subtitle;
	private String unit;
	
	public KpiDTO(String key, IndicatorTrend trend, BigDecimal value, String title, String subtitle, String unit) {
		this.key = key;
		this.trend = trend;
		this.value = value;
		this.title = title;
		this.subtitle = subtitle;
		this.unit = unit;
	}
	public KpiDTO() {
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public IndicatorTrend getTrend() {
		return trend;
	}
	public void setTrend(IndicatorTrend trend) {
		this.trend = trend;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
 