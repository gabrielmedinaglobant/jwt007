package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

public class PeopleLookedCompetitorsIndicatorDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String key;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal value;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String unit;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String title;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String subtitle;
	
	public PeopleLookedCompetitorsIndicatorDTO() {
	}
	
	public PeopleLookedCompetitorsIndicatorDTO(String key, BigDecimal value, String unit, String title) {
		this.key = key;
		this.value = value;
		this.unit = unit;
		this.title = title;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
}
