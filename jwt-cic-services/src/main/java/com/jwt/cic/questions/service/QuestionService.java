package com.jwt.cic.questions.service;

import org.json.simple.JSONObject;

import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutDTO;
import com.jwt.cic.questions.dto.KeyThemesTalkAboutDTO;
import com.jwt.cic.questions.dto.SentimentChangeDayOverDayDTO;
import com.jwt.cic.questions.util.BlockType;

public interface QuestionService {
	public BrandsPeopleTalkingAboutDTO getBrandsPeopleTalkingAbout(BlockType blockType);
	public JSONObject getBrandsPeopleTalkingAboutRaw(BlockType blockType);
	public KeyThemesTalkAboutDTO getKeyThemesTalkAbout();
	public JSONObject getKeyThemesTalkAboutRaw();
	public SentimentChangeDayOverDayDTO getSentimentChangeDayOverDay();
	public JSONObject getSentimentChangeDayOverDayRaw();
	public JSONObject getTalkingAboutMe();
	public JSONObject getVolumeChangedDayOverDay();
}
