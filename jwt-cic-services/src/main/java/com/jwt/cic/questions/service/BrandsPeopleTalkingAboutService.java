package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.util.BlockType;

public interface BrandsPeopleTalkingAboutService {
	@Deprecated
	QuestionSingleResultContainerDTO get(BlockType blockType, long projectId);
	@Deprecated
	QuestionSingleResultContainerDTO get(BlockType blockType);
	
	QuestionSingleResultContainerDTO get(long projectId);
}
