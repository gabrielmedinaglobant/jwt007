package com.jwt.cic.questions.dao;

import com.jwt.cic.questions.dto.EmotionsAboutCategoryEtlResultDTO;

public interface EmotionsAboutCategoryDAO {
	EmotionsAboutCategoryEtlResultDTO get(String projectUuid);
}
