package com.jwt.cic.questions.dao;

import com.jwt.cic.questions.dto.ConversationHappeningEtlResultDTO;

public interface ConversationHappeningDAO {
	ConversationHappeningEtlResultDTO get();
	ConversationHappeningEtlResultDTO get(String projectUuid);
}
