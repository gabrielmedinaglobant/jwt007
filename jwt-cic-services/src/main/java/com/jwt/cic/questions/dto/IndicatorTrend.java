package com.jwt.cic.questions.dto;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum IndicatorTrend {
	NONE("none",""), PLUS("plus","+"), MINUS("minus","-"), UP("up","↑"), DOWN("down","↓");
	
	private String value;
	private String symbol;
	private static final Map<String, IndicatorTrend> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private IndicatorTrend(String value, String symbol){
		this.value = value;
		this.symbol = symbol;
	}
	
	private static Map<String, IndicatorTrend> initializeValueMap(){
		Map<String, IndicatorTrend> valueMap = new HashMap<>();
		for (IndicatorTrend ct : IndicatorTrend.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static IndicatorTrend getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, IndicatorTrend> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public static IndicatorTrend getFromSignum(int signum){
		if(signum > 0){
			return IndicatorTrend.PLUS;
		} else if (signum == 0) {
			return IndicatorTrend.NONE;
		} else {
			return IndicatorTrend.MINUS;
		}
	}
    
}
