package com.jwt.cic.questions.calculations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO.Brand;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO.Sentiment;
import com.jwt.cic.questions.infegy.dto.*;
import com.jwt.cic.questions.infegy.dto.SentimentDTO.OutputElementDTO;

@Component
public class BrandsPeopleTalkingAboutStrategy {
	
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(BrandsPeopleTalkingAboutStrategy.class);
	
	public BrandsPeopleTalkingAboutResultDTO calculate(String myBrand, Map<String, SentimentDTO> sentiments) {

		BrandsPeopleTalkingAboutResultDTO dto = new BrandsPeopleTalkingAboutResultDTO();

		// get the sum of all my-brand documents
		Map<String, BigDecimal> totalDocumentsPerBrand = 
				sentiments.entrySet()
				.stream()
				.map((entry) -> {
					BigDecimal total = entry.getValue()
							.getOutput()
							.stream()
							.map((outputElement) -> outputElement.getDocuments())
							.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
					
					Map.Entry<String, BigDecimal> totalByBrand = Pair.of(entry.getKey(), total);
					
					return totalByBrand;
				}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		BigDecimal countOfElements = BigDecimal.valueOf(sentiments.get(myBrand).getOutput().size());
		BigDecimal myBrandTotalDocuments = totalDocumentsPerBrand.get(myBrand);
		
		BigDecimal average = myBrandTotalDocuments.divide(countOfElements, 2,RoundingMode.HALF_UP);
		
		// calculate the average of my brand
		dto.setLastUpdatedDate( sentiments.get(myBrand).getQueryInfo().getEndDate() );
		dto.setBrandResults( calculateResultsForEachBrand(myBrand, sentiments, totalDocumentsPerBrand) );
		
		LOG.info("Calulated DTO is :" +  dto.toString());
		
		return dto;
	}

	private List<Brand> calculateResultsForEachBrand(String myBrand, Map<String, SentimentDTO> sentiments, Map<String, BigDecimal> totalDocumentsPerBrand) {
		return sentiments.entrySet()
			.stream()
			.map((entry) -> {
				Brand b = new Brand();
				String brandName = entry.getKey();
				BigDecimal brandCountOfElements = BigDecimal.valueOf(sentiments.get( brandName ).getOutput().size());
				BigDecimal brandTotalDocuments = totalDocumentsPerBrand.get( brandName );
				
				b.setAverage( brandTotalDocuments.divide(brandCountOfElements, 2, RoundingMode.HALF_UP) );
				b.setBrand( entry.getKey() );
				b.setRatio( totalDocumentsPerBrand.get(entry.getKey()).divide( brandTotalDocuments, RoundingMode.HALF_UP ) );
				b.setSentimentOverTime( calculateSentimentOverTime( entry.getValue().getOutput() ) );
				
				return b;
			}).collect( Collectors.toList() );
	}

	private List<Sentiment> calculateSentimentOverTime(List<OutputElementDTO> output) {
		return output.stream().map( (element) -> {
			Sentiment s = new Sentiment();
			s.setDate( element.getDate() );
			s.setValue( element.getDocuments() );
			return s;
		} ).collect( Collectors.toList() );
	}
}
