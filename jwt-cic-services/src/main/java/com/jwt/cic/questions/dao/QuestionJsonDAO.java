package com.jwt.cic.questions.dao;

import org.json.simple.JSONObject;

public interface QuestionJsonDAO {
	JSONObject getBrandsPeopleTalkingAboutDocuments();
	JSONObject getBrandsPeopleTalkingAboutSentences();
	JSONObject getBrandsPeopleTalkingAboutSubjectSentences();
	JSONObject getKeyThemesTalkAbout();
	JSONObject getSentimentChangeDayOverDay();
	JSONObject getTalkingAboutMe();
	JSONObject getVolumeChangedDayOverDay();
	JSONObject getSentimentChangeDayOverDay(String projectUuid);
}
