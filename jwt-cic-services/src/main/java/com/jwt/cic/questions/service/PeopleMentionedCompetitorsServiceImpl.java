package com.jwt.cic.questions.service;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.PeopleMentionedCompetitorsDAO;
import com.jwt.cic.questions.dto.*;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class PeopleMentionedCompetitorsServiceImpl implements PeopleMentionedCompetitorsService {

	
	
	private PeopleMentionedCompetitorsDAO peopleMentionedCompetitorsDAO;
	private ProjectDAO projectDAO;
	private QuestionResultContainerBuilder questionResultContainerBuilder;

	@Autowired
	public PeopleMentionedCompetitorsServiceImpl(PeopleMentionedCompetitorsDAO peopleMentionedCompetitorsDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
		this.peopleMentionedCompetitorsDAO = peopleMentionedCompetitorsDAO;
		this.projectDAO = projectDAO;
		this.questionResultContainerBuilder = questionResultContainerBuilder;
	}
	
	@Override
	@Cacheable("question.people-mentioned-competitors.0")
	public QuestionSingleResultContainerDTO get() {
		InfoEtlResultDTO infoData = peopleMentionedCompetitorsDAO.getInfo();
		List<Map<String, String>> dailyData = peopleMentionedCompetitorsDAO.getDaily();
		List<Map<String, String>> summaryData = peopleMentionedCompetitorsDAO.getSummary();

		return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(infoData), buildDTO(dailyData, summaryData));
	}
	
	@Override
	@Cacheable("question.people-mentioned-competitors.1")
	public QuestionSingleResultContainerDTO get(Long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}

		InfoEtlResultDTO infoData = peopleMentionedCompetitorsDAO.getInfo(project.getUuid());
		List<Map<String, String>> dailyData = peopleMentionedCompetitorsDAO.getDaily(project.getUuid());
		List<Map<String, String>> summaryData = peopleMentionedCompetitorsDAO.getSummary(project.getUuid());

		PeopleMentionedCompetitorsDTO responseDTO= buildDTO(dailyData, summaryData);
		responseDTO.setLastUpdatedDate(DateConverter.fromDateTOSecond(new Date())); //TODO: dates are mocked
		return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(infoData), responseDTO);
	}
	
	protected final long getLastUpdatedDate( InfoEtlResultDTO infoData ){
		return DateConverter.fromISODateToEpochSeconds( infoData.getLastUpdatedDate() );
	}
	
	protected final PeopleMentionedCompetitorsDTO buildDTO(List<Map<String, String>> dailyData, List<Map<String, String>> summaryData){
		PeopleMentionedCompetitorsDTO dto = new PeopleMentionedCompetitorsDTO();
		Set<String> brands = buildBrandsKeyNameList(summaryData);
		List<DisplayableDTO> brandsDTOList = buildBrandsDTOList(brands);
		
		// Collect brands
		dto.setBrands(brandsDTOList);
		dto.setSplineChart(buildSplineChart(brands, dailyData));
		dto.setSummary(buildSummary(brands, summaryData));
		
		return dto;
	}
	
	private PeopleMentionedCompetitorsSummaryDTO buildSummary(Set<String> brands,
			List<Map<String, String>> summaryData) {
		PeopleMentionedCompetitorsSummaryDTO dto = new PeopleMentionedCompetitorsSummaryDTO();
		List<PeopleMentionedCompetitorsSummaryRowDTO> dtoRowList = new ArrayList<PeopleMentionedCompetitorsSummaryRowDTO>();
		
		for(String brand : brands){
			dtoRowList.add( buildSummaryRowDTO(brand, summaryData) );
		}
		
		//here we calculate totalPercentage
		final BigDecimal sumOfTotals = dtoRowList
						.stream()
						.map( (dtoItem) -> {
							return dtoItem.getTotal();
						} )
						.reduce( BigDecimal.ZERO , (sum , val) -> {
							return sum.add(val);
						});
		
		dtoRowList = dtoRowList
						.stream()
						.map( (dtoItem) -> {
							dtoItem.setTotalPercentage( dtoItem.getTotal().divide( sumOfTotals, 6, RoundingMode.HALF_UP ).multiply( BigDecimal.valueOf(100l) ) );
							return dtoItem;
						} )
						.collect( Collectors.toList() );
		
		dto.setRows(dtoRowList);
		return dto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildSummaryRowDTO(String brand,
			List<Map<String, String>> summaryData) {
		
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();
		
		String expectedKeyTotal = brand + "_total";
		String expectedKeyLowest = brand + "_lowest";
		String expectedKeyLowestDate = brand + "_date_min";
		String expectedKeyAverage = brand + "_average";
		String expectedKeyHighest = brand + "_highest";
		String expectedKeyHighestDate = brand + "_date_max";
		// change one is missing...
		String expectedKeyDate = "date";
		
		for(Map<String, String> mapElement : summaryData){
			
			//at least on of the properties is cointained
			if ( mapElement.containsKey(expectedKeyTotal) ){
				
				dto.setBrand( StringUtils.capitalize(brand) );
				dto.setBrandKey( brand.toLowerCase() );
				dto.setTotal( new BigDecimal( mapElement.get(expectedKeyTotal) ) ); 
				dto.setLowest( new BigDecimal( mapElement.get(expectedKeyLowest) ) );
				dto.setLowestDate( DateConverter.fromISODateToEpochSeconds( mapElement.get(expectedKeyLowestDate) ) );
				dto.setAverage( new BigDecimal( mapElement.get(expectedKeyAverage) ) );
				dto.setHighest( new BigDecimal( mapElement.get(expectedKeyHighest) ) );
				dto.setHighestDate( DateConverter.fromISODateToEpochSeconds( mapElement.get(expectedKeyHighestDate) ) );
				dto.setTotal( new BigDecimal( mapElement.get(expectedKeyTotal) ) );
				dto.setChangeUnit( UnitType.PERCENTAGE.getValue() );
				dto.setChangeValue( BigDecimal.ZERO );
			
				break;
			}
		}
		
		return dto;
	}

	protected final SplineChartDTO buildSplineChart(Set<String> brands, List<Map<String, String>> data){
		SplineChartDTO dto = new SplineChartDTO();
		List<SplineChartLineDTO> linesDTOList = new ArrayList<>();
		
		for(String brand : brands){
			linesDTOList.add(new SplineChartLineDTO(brand.toLowerCase(), buildSplineChartLineValues( brand, data )));
		}
		
		dto.setLines(linesDTOList);
		return dto;
	}

	protected final List<ArrayValueDTO> buildSplineChartLineValues(String brand, List<Map<String, String>> data) {
		List<ArrayValueDTO> values = new ArrayList<>();
		
		for(Map<String, String> mapElement : data){
			
			String expectedKey = brand + "_posts_universe";
			String expectedKeyDate = "date";
			String strDateValue;
			String strValue;
			
			if ( mapElement.containsKey(expectedKey) ){
				strValue = mapElement.get(expectedKey);
				strDateValue = mapElement.get(expectedKeyDate);
				
				values.add(
					new ArrayValueDTO(
						new BigDecimal[]{
							DateConverter.fromISODateToEpochSecondsAsBigDecimal(strDateValue),
							new BigDecimal( strValue )
						}
					)
				);
			}
		}
		
		return values;
	}

	protected final List<DisplayableDTO> buildBrandsDTOList(Set<String> brands){		
		return brands.stream().map( (brandName) -> {
			BrandIndicatorDTO dto = new BrandIndicatorDTO();
			dto.setDisplayName(StringUtils.capitalize(brandName));
			dto.setKey(brandName.toLowerCase());
			return dto;
		} ).collect( Collectors.toList() );
	}
	
	protected final Set<String> buildBrandsKeyNameList(List<Map<String, String>> summaryData){
		//Iteates over all array, so that any word prefixing _max is stored as brand
		//this is stored in a hashmap, so as not to repeat brands
		Set<String> brands = new LinkedHashSet<>();
		
		summaryData
			.stream()
			.map( ( element ) -> {
				return element.keySet();
			} )
			.forEach( (keySet) -> {
				keySet.stream()
					.filter( (key) -> { return key.compareTo("date") != 0; } )
					.forEach( (key) -> { brands.add( extractBrandName(key) ); } );
			} );
		
		return brands;
	}
	
	protected final String extractBrandName(String brandname){
		return brandname.split("_")[0];
	}
	
}
