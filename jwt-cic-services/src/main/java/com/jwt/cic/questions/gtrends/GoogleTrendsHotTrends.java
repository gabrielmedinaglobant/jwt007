package com.jwt.cic.questions.gtrends;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

@Component
public class GoogleTrendsHotTrends {
	
	private static String TRENDING_SEARCHES_URL = "https://trends.google.com/trends/hottrends/visualize/internal/data";
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Cacheable("google-trends-hot-trends-trending-searches-per-region")
	public Map<String, List<String>> getTrendingSearchesPerRegion(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(TRENDING_SEARCHES_URL,String.class);
		
		return convertJsonStringToMap(response.getBody());
	}
	
	private Map<String, List<String>> convertJsonStringToMap(String jsonString){
		try {
			
			Map<String,List<String>> regionsJsonStringMap = objectMapper.readValue(jsonString, new TypeReference<HashMap<String,List<String>>>() {});
			/*Map<String,List<String>> resultMap = new HashMap<String,List<String>>();
			
			for(String key : regionsJsonStringMap.keySet()){
				List<String> keywords = objectMapper.readValue( regionsJsonStringMap.get(key), TypeFactory.defaultInstance().constructCollectionLikeType(List.class, String.class));
				resultMap.put(key, keywords);
			}*/
			
			return regionsJsonStringMap;
			
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
