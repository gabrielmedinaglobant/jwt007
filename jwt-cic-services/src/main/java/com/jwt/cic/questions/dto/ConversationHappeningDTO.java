package com.jwt.cic.questions.dto;

import com.jwt.cic.charts.dto.PieChartDTO;

public class ConversationHappeningDTO implements QuestionResultDTO {
	private PieChartDTO pieChart;
	private long lastUpdatedDate;

	public ConversationHappeningDTO(PieChartDTO pieChart) {
		this.pieChart = pieChart;
	}

	public ConversationHappeningDTO() {
	}
	
	public PieChartDTO getPieChart() {
		return pieChart;
	}

	public void setPieChart(PieChartDTO pieChart) {
		this.pieChart = pieChart;
	}

	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
