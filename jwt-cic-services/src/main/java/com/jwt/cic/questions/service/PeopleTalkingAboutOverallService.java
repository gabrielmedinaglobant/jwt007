package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;

public interface PeopleTalkingAboutOverallService {
	QuestionResultListContainerDTO getAll();
	QuestionResultListContainerDTO getAll(long projectId);
}
