package com.jwt.cic.questions.dao;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.jwt.cic.utils.JsonFetcher;

@Primary
@Repository
public class QuestionJsonDAOAWSImpl implements QuestionJsonDAO {

	private final Logger log = LoggerFactory.getLogger(QuestionJsonDAOAWSImpl.class);
	
	private JsonFetcher jsonFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	
	@Value("${question.s3.bucket.brands-people-talking-about.json}")
	private String brandsPeopleTalkingAboutDocumentsLocation;
	@Value("${question.s3.bucket.brands-people-talking-about.json}")
	private String brandsPeopleTalkingAboutSentencesLocation;
	@Value("${question.s3.bucket.brands-people-talking-about.json}")
	private String brandsPeopleTalkingAboutSubjectSentencesLocation;
	@Value("${question.s3.bucket.key-themes-talk-about.json}")
	private String keyThemesTalkAboutLocation;
	@Value("${question.s3.bucket.sentiment-change-day-over-day.json}")
	private String sentimentChangeDayOverDayLocation;
	@Value("${question.s3.bucket.volume-change-day-over-day.json}")
	private String talkingAboutMeLocation;
	@Value("${question.s3.bucket.talking-about-me.json}")
	private String volumeChangedDayOverDayLocation;
	
	@Autowired
	public QuestionJsonDAOAWSImpl(JsonFetcher jsonFetcher){
		this.jsonFetcher = jsonFetcher;
	}
	
	@Override
	public JSONObject getBrandsPeopleTalkingAboutDocuments() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.brandsPeopleTalkingAboutDocumentsLocation);
	}

	@Override
	public JSONObject getBrandsPeopleTalkingAboutSentences() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.brandsPeopleTalkingAboutSentencesLocation);
	}

	@Override
	public JSONObject getBrandsPeopleTalkingAboutSubjectSentences() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.brandsPeopleTalkingAboutSubjectSentencesLocation);
	}

	@Override
	public JSONObject getKeyThemesTalkAbout() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.keyThemesTalkAboutLocation);
	}

	@Override
	public JSONObject getSentimentChangeDayOverDay() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.sentimentChangeDayOverDayLocation);
	}
	
	@Override
	public JSONObject getSentimentChangeDayOverDay(String projectUuid) {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + projectUuid + "/output/" + this.sentimentChangeDayOverDayLocation);
	}

	@Override
	public JSONObject getTalkingAboutMe() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.talkingAboutMeLocation);
	}

	@Override
	public JSONObject getVolumeChangedDayOverDay() {
		return this.jsonFetcher.fetchFromResource(this.bucketLocation + this.volumeChangedDayOverDayLocation);
	}

}
