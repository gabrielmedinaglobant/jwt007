package com.jwt.cic.questions.service;

import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.EmotionsAboutCategoryDAO;
import com.jwt.cic.questions.dto.EmotionsAboutCategoryEtlResultDTO;
import com.jwt.cic.questions.dto.EmotionsIndicatorDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.util.EmotionType;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;
import com.jwt.cic.utils.RoundingStrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmotionsAboutCategoryServiceImpl implements EmotionsAboutCategoryService {

    private EmotionsAboutCategoryDAO emotionsAboutCategoryDAO;
    private ProjectDAO projectDAO;
    private RoundingStrategy roundingStrategy;
    private QuestionResultContainerBuilder questionResultListContainerBuilder;
    
    private final Logger log = LoggerFactory.getLogger(EmotionsAboutCategoryServiceImpl.class);

    @Autowired
    public EmotionsAboutCategoryServiceImpl(EmotionsAboutCategoryDAO emotionsAboutCategoryDAO, 
    		@Qualifier("doubleDigitsPercentageRoundingStrategy") RoundingStrategy roundingStrategy, 
    		ProjectDAO projectDAO,
    		QuestionResultContainerBuilder questionResultListContainerBuilder) {
        this.emotionsAboutCategoryDAO = emotionsAboutCategoryDAO;
        this.roundingStrategy = roundingStrategy;
        this.projectDAO = projectDAO;
        this.questionResultListContainerBuilder = questionResultListContainerBuilder;
    }

    @Deprecated
    @Override
    @Cacheable("question.emotions-about-category.0")
    public QuestionResultListContainerDTO get() {
    	throw new RuntimeException("No longer valid");
    }
    
    @Override
    @Cacheable("question.emotions-about-category.1")
	public QuestionResultListContainerDTO get(long projectId) {
    	Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		EmotionsAboutCategoryEtlResultDTO eltResult = emotionsAboutCategoryDAO.get(project.getUuid());
        return questionResultListContainerBuilder.buildDTO( 
        		DateConverter.fromISODateToEpochSecondsTrimmed( eltResult.getInfo().getDateFrom() ), 
        		DateConverter.fromISODateToEpochSecondsTrimmed( eltResult.getInfo().getDateTo() ), 
        		DateConverter.fromISODateToEpochSeconds( eltResult.getInfo().getLastUpdatedDate() ), 
        		buildDTOList(eltResult) );
	}

    private List<EmotionsIndicatorDTO> buildDTOList(EmotionsAboutCategoryEtlResultDTO eltResult) {    	
    	return eltResult.getValues().stream().map( (res) -> {
    		String emotion = res.getType();
			BigDecimal value = res.getValue();
    		
    		EmotionsIndicatorDTO dto = new EmotionsIndicatorDTO(EmotionType.getByValue(emotion),
					roundingStrategy
							.round(value.multiply(BigDecimal.valueOf(100L))),
					UnitType.PERCENTAGE.getValue());
			return dto;
    	} ).collect(Collectors.toList());
    }
    
}
