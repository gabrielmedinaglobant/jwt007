package com.jwt.cic.questions.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.PeopleFeelAboutBrandEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Primary
@Repository
public class PeopleFeelAboutBrandDAOAWSImpl implements PeopleFeelAboutBrandDAO {

	private final Logger log = LoggerFactory.getLogger(PeopleFeelAboutBrandDAOAWSImpl.class);

	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.people-feel-about-brand.json}")
	private String resourceLocation;
	
	@Autowired
	public PeopleFeelAboutBrandDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}
	
	@Override
	public List<PeopleFeelAboutBrandEtlResultDTO> get() {
		return resourceFetcher.fetchObjects(bucketLocation+resourceLocation, PeopleFeelAboutBrandEtlResultDTO.class);
	}
	
	@Override
	public List<PeopleFeelAboutBrandEtlResultDTO> get(String projectUuid) {
		return resourceFetcher.fetchObjects(bucketLocation + projectUuid + "/output/" + resourceLocation, PeopleFeelAboutBrandEtlResultDTO.class);
	}

}
