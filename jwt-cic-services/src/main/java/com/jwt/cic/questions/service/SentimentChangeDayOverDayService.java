package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

public interface SentimentChangeDayOverDayService {
	QuestionSingleResultContainerDTO get();
	QuestionSingleResultContainerDTO get(long projectId);
}
