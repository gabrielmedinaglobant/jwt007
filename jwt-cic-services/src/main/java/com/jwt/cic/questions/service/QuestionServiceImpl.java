package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.WordUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.PieChartDTO;
import com.jwt.cic.charts.dto.SingleStackedBarDTO;
import com.jwt.cic.charts.dto.SingleValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.questions.dao.QuestionJsonDAO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutDTO;
import com.jwt.cic.questions.dto.KeyThemesTalkAboutDTO;
import com.jwt.cic.questions.dto.SentimentChangeDayOverDayDTO;
import com.jwt.cic.questions.dto.ThemeChartsDTO;
import com.jwt.cic.questions.util.BlockType;

@Deprecated
@Service
public class QuestionServiceImpl implements QuestionService {

	private final String POSITIVITY = "positivity";
	private final String NEGATIVITY = "negativity";
	private final String MIX = "mix";
	
	private final String JSON_POSITIVITY_SUFFIX = "_positivity";
	private final String JSON_NEGATIVITY_SUFFIX = "_negativity";
	
	
	private QuestionJsonDAO questionDAO;
	
	@Autowired
	public QuestionServiceImpl(QuestionJsonDAO questionDAO){
		this.questionDAO = questionDAO;
	}
	
	@Deprecated
	@Override
	public BrandsPeopleTalkingAboutDTO getBrandsPeopleTalkingAbout(BlockType blockType) {
		BrandsPeopleTalkingAboutDTO dto = new BrandsPeopleTalkingAboutDTO();
		JSONObject fetchedJSONObject = null;
		
		switch (blockType) {
			case DOCUMENTS:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutDocuments();
				break;
			case SENTENCES:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutSentences();
				break;
			case SUBJECT_SENTENCES:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutSubjectSentences();
				break;
			default:
				throw new IllegalArgumentException();
		}
		
		// dto.setPositivityPieChart(buildPositivityPieChart(fetchedJSONObject));
		// dto.setNegativityPieChart(buildNegativityPieChart(fetchedJSONObject));
		dto.setSentimentOvertimeSplineChart(buildOvertimeChartValues(blockType, fetchedJSONObject));
		// dto.setSingleStackedBarChart(buildSingleStackedBar(fetchedJSONObject));
		// dto.setNegativeSentimentNet(new BigDecimal("-1.4"));
		// dto.setBlockType(blockType.getQueryParamValue());
		
		return dto;
	}

	@Deprecated
	private PieChartDTO buildPositivityPieChart(JSONObject fetchedJSONObject){
		PieChartDTO pieChartDto = new PieChartDTO();
		List<SingleValueDTO> pieChartValuesDto = new LinkedList<>();
		
		pieChartValuesDto.add(new SingleValueDTO(POSITIVITY,new BigDecimal(45)));
		pieChartValuesDto.add(new SingleValueDTO(NEGATIVITY,new BigDecimal(46)));
		pieChartValuesDto.add(new SingleValueDTO(MIX,new BigDecimal(9)));
		
		pieChartDto.setIndicator(new BigDecimal(45));
		pieChartDto.setIndicatorUnit("%");
		pieChartDto.setValues(pieChartValuesDto);
		
		return pieChartDto;
	}
	
	@Deprecated
	private PieChartDTO buildNegativityPieChart(JSONObject fetchedJSONObject){
		PieChartDTO pieChartDto = new PieChartDTO();
		List<SingleValueDTO> pieChartValuesDto = new LinkedList<>();
		
		pieChartValuesDto.add(new SingleValueDTO(POSITIVITY,new BigDecimal(45)));
		pieChartValuesDto.add(new SingleValueDTO(NEGATIVITY,new BigDecimal(46)));
		pieChartValuesDto.add(new SingleValueDTO(MIX,new BigDecimal(9)));
		
		pieChartDto.setIndicator(new BigDecimal(46));
		pieChartDto.setIndicatorUnit("%");
		pieChartDto.setValues(pieChartValuesDto);
		
		return pieChartDto;
	}
	
	@Deprecated
	@SuppressWarnings("unchecked")
	private SplineChartDTO buildOvertimeChartValues(BlockType blockType, JSONObject fetchedJSONObject){
		SplineChartDTO splineChartDTO = new SplineChartDTO();
		List<SplineChartLineDTO> splineChartLineDTOList = new LinkedList<SplineChartLineDTO>();
		SplineChartLineDTO splineChartLineDTO = new SplineChartLineDTO();
		List<ArrayValueDTO> pointsValues = new LinkedList<ArrayValueDTO>();
		
		JSONArray outputJson = (JSONArray)fetchedJSONObject.get("output");
		final AtomicInteger count = new AtomicInteger();
		outputJson.forEach((x) -> {	
			JSONObject jsonObject = (JSONObject)x;
			String dateStrValue = (String)jsonObject.get("date");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
			LocalDateTime dateTime = LocalDateTime.parse(dateStrValue, formatter);
			
			pointsValues.add(new ArrayValueDTO( "week " + BigDecimal.valueOf(count.incrementAndGet()), new BigDecimal[]{ 
				BigDecimal.valueOf( dateTime.toEpochSecond(ZoneOffset.UTC) ),
				BigDecimal.valueOf( (Long)jsonObject.get("positive_" + blockType.getValue()) - (Long)jsonObject.get("negative_" + blockType.getValue()))
			}));
		});
		splineChartLineDTO.setKey("default-line");
		splineChartLineDTO.setValues(pointsValues);
		splineChartLineDTOList.add(splineChartLineDTO);
		splineChartDTO.setLines(splineChartLineDTOList);
		
		return splineChartDTO;
	}
	
	@Deprecated
	private SingleStackedBarDTO buildSingleStackedBar(JSONObject fetchedJSONObject){
		SingleStackedBarDTO singleStackedBarDTO = new SingleStackedBarDTO();
		List<SingleValueDTO> values = new LinkedList<>();
		values.add(new SingleValueDTO(POSITIVITY, new BigDecimal(45)));
		values.add(new SingleValueDTO(MIX, new BigDecimal(9)));
		values.add(new SingleValueDTO(NEGATIVITY, new BigDecimal(46)));
		singleStackedBarDTO.setValues(values);
		
		return singleStackedBarDTO;
	}
	
	
	@Override
	public JSONObject getBrandsPeopleTalkingAboutRaw(BlockType blockType) {
		// Should any further processing is needed, we can do it here
		JSONObject fetchedJSONObject = null;

		switch (blockType) {
			case DOCUMENTS:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutDocuments();
				break;
			case SENTENCES:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutSentences();
				break;
			case SUBJECT_SENTENCES:
				fetchedJSONObject = questionDAO.getBrandsPeopleTalkingAboutSubjectSentences();
				break;
			default:
				throw new IllegalArgumentException();
		}

		return fetchedJSONObject;
	}
	
	@Cacheable("question.key-themes-talk-about")
	@Override
	public KeyThemesTalkAboutDTO getKeyThemesTalkAbout() {
		KeyThemesTalkAboutDTO keyThemesTalkAboutDTO = new KeyThemesTalkAboutDTO();
		JSONObject fetchedJSONObject = questionDAO.getKeyThemesTalkAbout();
		
		Map<String, ThemeChartsDTO> themes = buildMapCollectingThemes(fetchedJSONObject);
		buildKeyThemesTalkAboutPieCharts(themes, fetchedJSONObject);
		buildKeyThemesTalkAboutSingleStackedLineCharts(themes, fetchedJSONObject);

		List<ThemeChartsDTO> orderedThemes = new ArrayList<>(themes.values());
		Collections.sort( orderedThemes, new Comparator<ThemeChartsDTO>(){

			@Override
			public int compare(ThemeChartsDTO o1, ThemeChartsDTO o2) {
				return -o1.getPieChart().getIndicator().compareTo(o2.getPieChart().getIndicator());
			}
			
		});
		
		keyThemesTalkAboutDTO.setThemes(orderedThemes);
		return keyThemesTalkAboutDTO;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, ThemeChartsDTO> buildMapCollectingThemes(JSONObject fetchedJSONObject){
		List<String> themes = new LinkedList<>();
		Map<String, ThemeChartsDTO> map = new HashMap<>();
		
		fetchedJSONObject.forEach( (k,v) -> {
			String sk = (String)k;
			
			if(sk.lastIndexOf(JSON_POSITIVITY_SUFFIX) == -1){
				// String themeName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sk );
				String themeName = WordUtils.capitalize( sk.replace("_", " ") );
				map.put(themeName, new ThemeChartsDTO(themeName));
			}
		} );
		
		return map;
	}
	
	@SuppressWarnings("unchecked")
	private void buildKeyThemesTalkAboutPieCharts(Map<String, ThemeChartsDTO> themes, JSONObject fetchedJSONObject){
		fetchedJSONObject.forEach( (k,v) -> {
			String sk = (String)k;
			Double dv = (Double)v;
			
			if(sk.lastIndexOf(JSON_POSITIVITY_SUFFIX) == -1){
				// String themeName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sk );
				String themeName = WordUtils.capitalize( sk.replace("_", " ") );
				PieChartDTO pieChart = buildKeyThemesTalkAboutPieChart(dv);
				themes.get(themeName).setPieChart(pieChart);
			}
		} );
	}
	
	private PieChartDTO buildKeyThemesTalkAboutPieChart(double distributionValue){
		PieChartDTO pieChartDTO = new PieChartDTO();
		List<SingleValueDTO> values = new ArrayList<SingleValueDTO>(3);
		
		pieChartDTO.setIndicator(BigDecimal.valueOf(distributionValue).setScale(0,RoundingMode.HALF_UP));
		pieChartDTO.setIndicatorUnit("%");
		values.add(new SingleValueDTO("distribution", BigDecimal.valueOf(distributionValue)) );
		values.add(new SingleValueDTO("leftover", BigDecimal.valueOf(100 - distributionValue)) );
		pieChartDTO.setValues(values);
		
		return pieChartDTO;
	}
	
	@SuppressWarnings("unchecked")
	private void buildKeyThemesTalkAboutSingleStackedLineCharts(Map<String, ThemeChartsDTO> themes, JSONObject fetchedJSONObject){
		fetchedJSONObject.forEach( (k,v) -> {
			String sk = (String)k;
			Double dv = (Double)v;
			
			if(sk.lastIndexOf(JSON_POSITIVITY_SUFFIX) != -1){
				// String themeName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sk.substring(0, sk.lastIndexOf("_bar")) );
				String themeName = WordUtils.capitalize( sk.substring(0, sk.lastIndexOf(JSON_POSITIVITY_SUFFIX)).replace("_", " ") );
				SingleStackedBarDTO pieChart = buildKeyThemesTalkAboutSingleStackedBarChart(dv);
				themes.get(themeName).setSingleStackedBar(pieChart);
			}
		} );
	}
	
	private SingleStackedBarDTO buildKeyThemesTalkAboutSingleStackedBarChart(double positivityValue){
		SingleStackedBarDTO singleStackedBarDTO = new SingleStackedBarDTO();
		List<SingleValueDTO> values = new ArrayList<SingleValueDTO>(3);
		
		singleStackedBarDTO.setIndicator(BigDecimal.valueOf(positivityValue).setScale(0,RoundingMode.HALF_UP));
		singleStackedBarDTO.setIndicatorUnit("%");
		
		values.add(new SingleValueDTO("positivity", BigDecimal.valueOf(positivityValue)) );
		values.add(new SingleValueDTO("non-positivity", BigDecimal.valueOf(100 - positivityValue)) );
		singleStackedBarDTO.setValues(values);
		
		return singleStackedBarDTO;
	}
	
	@Override
	public JSONObject getKeyThemesTalkAboutRaw() {
		// Should any further processing is needed, we can do it here
		return this.questionDAO.getKeyThemesTalkAbout();
	}

	@Deprecated
	@Cacheable("question.sentiment-change-day-over-day")
	public SentimentChangeDayOverDayDTO getSentimentChangeDayOverDay(){
		SentimentChangeDayOverDayDTO sentimentChangeDayOverDayDTO = new SentimentChangeDayOverDayDTO();
		SplineChartDTO splineChart = new SplineChartDTO();
		List<SplineChartLineDTO> lines = new LinkedList<SplineChartLineDTO>();
		JSONObject fetchedJSONObject = this.questionDAO.getSentimentChangeDayOverDay();
		
		lines.add(buildSplineChartLine("positive",fetchedJSONObject));
		lines.add(buildSplineChartLine("negative",fetchedJSONObject));
		lines.add(buildSplineChartLine("mixed",fetchedJSONObject));
		
		splineChart.setLines(lines);
		sentimentChangeDayOverDayDTO.setSplineChart(splineChart);
		return sentimentChangeDayOverDayDTO;
	}
	
	@Deprecated
	@SuppressWarnings("unchecked")
	private SplineChartLineDTO buildSplineChartLine(String sentiment, JSONObject fetchedJSONObject){
		SplineChartLineDTO splineChartLineDTO = new SplineChartLineDTO();
		List<ArrayValueDTO> values = new LinkedList<ArrayValueDTO>();
		JSONArray jsonArray = (JSONArray)((JSONObject)fetchedJSONObject).get("values");
		
		jsonArray.forEach( (v) -> {
			
			JSONObject jsonObject = (JSONObject)v;
			String dateStrValue = (String)jsonObject.get("date");
			double doubleValue = (Double)jsonObject.get(sentiment + "_subject_sentences_prom");
			ArrayValueDTO value = new ArrayValueDTO();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
			LocalDateTime dateTime = LocalDateTime.parse(dateStrValue, formatter);

			value.setKey(dateStrValue);
			value.setValue(new BigDecimal[]{ BigDecimal.valueOf( dateTime.toEpochSecond(ZoneOffset.UTC) ),BigDecimal.valueOf(doubleValue) });
			
			values.add(value);
		} );
		
		splineChartLineDTO.setKey(sentiment);
		splineChartLineDTO.setValues(values);
		
		return splineChartLineDTO;
	}
	
	@Deprecated
	@Override
	public JSONObject getSentimentChangeDayOverDayRaw() {
		// Should any further processing is needed, we can do it here
		return this.questionDAO.getSentimentChangeDayOverDay();
	}

	@Deprecated
	@Override
	public JSONObject getTalkingAboutMe() {
		// Should any further processing is needed, we can do it here
		return this.questionDAO.getTalkingAboutMe();
	}

	@Deprecated
	@Override
	public JSONObject getVolumeChangedDayOverDay() {
		// Should any further processing is needed, we can do it here
		return this.questionDAO.getVolumeChangedDayOverDay();
	}

}
