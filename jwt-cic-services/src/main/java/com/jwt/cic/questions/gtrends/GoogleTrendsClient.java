
package com.jwt.cic.questions.gtrends;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.exceptions.FetchingResourceException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class GoogleTrendsClient {

    @Autowired
    private HttpClient httpClient;

    public String execute(GoogleTrendsRequest request) {
        String content = null;
        try {
            HttpRequestBase httpRequest = request.buildCsvRequest(executeTokenRequest(request));
            HttpResponse response = httpClient.execute(httpRequest);
            content = toString(response.getEntity().getContent());
            httpRequest.releaseConnection();
        } catch (ClientProtocolException ex) {
            throw new FetchingResourceException();
        } catch (IOException ex) {
            throw new FetchingResourceException();
        } catch (FetchingResourceException ex) {
            throw new FetchingResourceException();
        }
        return content;
    }

    public String executeTokenRequest(GoogleTrendsRequest request) {
        String token = null;
        try {
            HttpRequestBase httpRequest = request.buildTokenRequest();
            HttpResponse response = httpClient.execute(httpRequest);
            String content = toString(response.getEntity().getContent());
            httpRequest.releaseConnection();
            content = content.replaceAll("^\\)\\]\\}'", "");
            ObjectMapper om = new ObjectMapper();
            JsonNode jsonNode = om.readTree(content);
            token = jsonNode.get("widgets").get(0).get("token").asText();
        } catch (ClientProtocolException ex) {
            throw new FetchingResourceException();
        } catch (IOException ex) {
            throw new FetchingResourceException();
        }
        return token;
    }

    private static String toString(InputStream in) throws IOException {
        String string;
        StringBuilder outputBuilder = new StringBuilder();
        if (in != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while (null != (string = reader.readLine())) {
                outputBuilder.append(string).append('\n');
            }
        }
        return outputBuilder.toString();
    }

}
