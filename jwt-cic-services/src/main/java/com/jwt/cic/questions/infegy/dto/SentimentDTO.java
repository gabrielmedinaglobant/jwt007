package com.jwt.cic.questions.infegy.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SentimentDTO {
	private List<OutputElementDTO> output;
	@JsonProperty("query_info")
	private QueryInfoDTO queryInfo;
	
	public List<OutputElementDTO> getOutput() {
		return output;
	}

	public void setOutput(List<OutputElementDTO> output) {
		this.output = output;
	}

	public QueryInfoDTO getQueryInfo() {
		return queryInfo;
	}

	public void setQueryInfo(QueryInfoDTO queryInfo) {
		this.queryInfo = queryInfo;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class OutputElementDTO {
		// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'hh:mm:ss'Z'")
		private String date;
		private BigDecimal documents;
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public BigDecimal getDocuments() {
			return documents;
		}
		public void setDocuments(BigDecimal documents) {
			this.documents = documents;
		}
			
	}
	
}
