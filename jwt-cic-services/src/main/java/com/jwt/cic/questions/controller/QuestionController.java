package com.jwt.cic.questions.controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.KeyThemesTalkAboutDTO;
import com.jwt.cic.questions.dto.QuestionResultDTO;
import com.jwt.cic.questions.service.QuestionService;

@RestController
@RequestMapping("api/v1/questions")
public class QuestionController {
	
	private QuestionService questionService;
	
	@Autowired
	public QuestionController(QuestionService questionService){
		this.questionService = questionService;
	}
	
	/*
	@RequestMapping("/key-themes-talk-about")
	public KeyThemesTalkAboutDTO getKeyThemesTalkAbout() {
		return questionService.getKeyThemesTalkAbout();
	}
	
	@RequestMapping("/key-themes-talk-about/raw")
	public JSONObject getKeyThemesTalkAboutRaw() {
		return questionService.getKeyThemesTalkAboutRaw();
	}
		
	
	
	@RequestMapping("/sentiment-changed-day-over-day")
	public SentimentChangeDayOverDayDTO getSentimentChangeDayOverDay() {
		return questionService.getSentimentChangeDayOverDay();
	}
	
	@RequestMapping("/sentiment-changed-day-over-day/raw")
	public JSONObject getSentimentChangeDayOverDayRaw() {
		return questionService.getSentimentChangeDayOverDayRaw();
	}
	
	
	
	@RequestMapping("/volume-changed-day-over-day")
	public JSONObject getVolumeChangedDayOverDay() {
		return questionService.getVolumeChangedDayOverDay();
	}
	
	@RequestMapping("/talking-about-me")
	public JSONObject getTalkingAboutMe() {
		return questionService.getTalkingAboutMe();
	}
	*/
}
