package com.jwt.cic.questions.dto;

import java.util.List;

public class PeopleMentionedCompetitorsSummaryDTO {
	private List<PeopleMentionedCompetitorsSummaryRowDTO> rows;

	public List<PeopleMentionedCompetitorsSummaryRowDTO> getRows() {
		return rows;
	}

	public void setRows(List<PeopleMentionedCompetitorsSummaryRowDTO> rows) {
		this.rows = rows;
	}
	
	
}
