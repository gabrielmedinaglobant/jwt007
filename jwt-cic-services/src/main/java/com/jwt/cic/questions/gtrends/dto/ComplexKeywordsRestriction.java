package com.jwt.cic.questions.gtrends.dto;

import java.util.List;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class ComplexKeywordsRestriction {

    private List<KeyWord> keyword;

    public List<KeyWord> getKeyword() {
        return keyword;
    }

    public void setKeyword(List<KeyWord> keyword) {
        this.keyword = keyword;
    }
}
