package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TalkingAboutMeEtlResult2DTO {
	private GenderTrendValueDTO genderTrend;
	private AgeDistributionValueDTO ageDistribution;
	private LanguageDistributionValueDTO languageDistribution;
	private DemograficsValueDTO demografics;
	
	private Long lastUpdatedDate;
	private Long dateFrom;
	private Long dateTo;
	
	
	public class GenderTrendValueDTO {
		private String date;
		@JsonProperty("femalebyWeek")
		private BigDecimal femaleByWeek;
		@JsonProperty("malebyWeek")
		private BigDecimal maleByWeek;
		private BigDecimal femaleSumByWeek;
		private BigDecimal maleSumByWeek;
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public BigDecimal getFemaleByWeek() {
			return femaleByWeek;
		}
		public void setFemaleByWeek(BigDecimal femaleByWeek) {
			this.femaleByWeek = femaleByWeek;
		}
		public BigDecimal getMaleByWeek() {
			return maleByWeek;
		}
		public void setMaleByWeek(BigDecimal maleByWeek) {
			this.maleByWeek = maleByWeek;
		}
		public BigDecimal getFemaleSumByWeek() {
			return femaleSumByWeek;
		}
		public void setFemaleSumByWeek(BigDecimal femaleSumByWeek) {
			this.femaleSumByWeek = femaleSumByWeek;
		}
		public BigDecimal getMaleSumByWeek() {
			return maleSumByWeek;
		}
		public void setMaleSumByWeek(BigDecimal maleSumByWeek) {
			this.maleSumByWeek = maleSumByWeek;
		}
		
	}
	
	public class AgeDistributionValueDTO {
		
	}
	
	public class LanguageDistributionValueDTO {
		
	}
	
	public class DemograficsValueDTO {
		
	}
	
}
