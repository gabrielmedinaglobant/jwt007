package com.jwt.cic.questions.dao;

import java.util.List;

import com.jwt.cic.questions.dto.OtherTopicsCareAboutEtlResultDTO;

public interface OtherTopicsCareAboutDAO {
	List<OtherTopicsCareAboutEtlResultDTO> get();
	List<OtherTopicsCareAboutEtlResultDTO> get(String projectUuid);
	List<OtherTopicsCareAboutEtlResultDTO> getTopPostInterestsData();
	List<OtherTopicsCareAboutEtlResultDTO> getTopPostInterestsData(String projectUuid);
	List<OtherTopicsCareAboutEtlResultDTO> getTopSourceInterestsData();
	List<OtherTopicsCareAboutEtlResultDTO> getTopSourceInterestsData(String projectUuid);
}
