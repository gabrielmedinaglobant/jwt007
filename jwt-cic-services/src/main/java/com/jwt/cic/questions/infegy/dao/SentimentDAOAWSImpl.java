package com.jwt.cic.questions.infegy.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dao.VolumeChangedDayOverDayDAOAWSImpl;
import com.jwt.cic.questions.infegy.dto.SentimentDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class SentimentDAOAWSImpl implements SentimentDAO {

	private final Logger log = LoggerFactory.getLogger(VolumeChangedDayOverDayDAOAWSImpl.class);
	
	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.input.sentiment.json}")
	private String resourceLocation;
	
	@Autowired
	public SentimentDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	public SentimentDTO get(String projectUuid, String queryTypeValue) {
		// TODO Auto-generated method stub
		return resourceFetcher.fetchObject(bucketLocation + projectUuid + "/input/" + queryTypeValue + "/" + resourceLocation, SentimentDTO.class);
	}

}
