package com.jwt.cic.questions.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InfoEtlResultDTO {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String brand;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("date_from")
	private String dateFrom;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("date_to")
	private String dateTo;
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
}
