package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.service.PeopleTalkingAboutOverallService;

/**
 * 
 * Controller for Q6/23 - Discover - Question "What are people talking about overall?"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/people-talking-about-overall")
public class PeopleTalkingAboutOverallController {
	
	private PeopleTalkingAboutOverallService peopleTalkingAboutOverallService;
	
	@Autowired
	public PeopleTalkingAboutOverallController(PeopleTalkingAboutOverallService peopleTalkingAboutOverallService) {
		this.peopleTalkingAboutOverallService = peopleTalkingAboutOverallService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/", ""})
	public QuestionResultListContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0) {
			return peopleTalkingAboutOverallService.getAll(projectId);
		}else {
			return peopleTalkingAboutOverallService.getAll();
		}
	}
	
}
