package com.jwt.cic.questions.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class BrandIndicatorDTO implements DisplayableDTO {

	private String key;
	private String displayName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String ratio;
	
	public BrandIndicatorDTO(String key, String displayName) {
		this.key = key;
		this.displayName = displayName;
	}
	public BrandIndicatorDTO(String key, String displayName, String ratio) {
		this.key = key;
		this.displayName = displayName;
		this.ratio = ratio;
	}
	
	
	public BrandIndicatorDTO() {
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getDisplayName() {
		return this.displayName;
	}
	
	public String getRatio() {
		return ratio;
	}
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((ratio == null) ? 0 : ratio.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BrandIndicatorDTO other = (BrandIndicatorDTO) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (ratio == null) {
			if (other.ratio != null)
				return false;
		} else if (!ratio.equals(other.ratio))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "BrandIndicatorDTO [key=" + key + ", displayName=" + displayName + ", ratio=" + ratio + "]";
	}

	
	
}
