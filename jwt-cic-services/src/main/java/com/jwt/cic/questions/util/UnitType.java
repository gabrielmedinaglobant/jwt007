package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UnitType {
	DOLLAR("$"), NONE(""), PERCENTAGE("%"), RATIO("x");
	
private String value;
	
	private static final Map<String, UnitType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private UnitType(String value){
		this.value = value;
	}
	
	private static Map<String, UnitType> initializeValueMap(){
		Map<String, UnitType> valueMap = new HashMap<>();
		for (UnitType ct : UnitType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static UnitType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, UnitType> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}
}
