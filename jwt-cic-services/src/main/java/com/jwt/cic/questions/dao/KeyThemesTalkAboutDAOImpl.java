package com.jwt.cic.questions.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class KeyThemesTalkAboutDAOImpl implements KeyThemesTalkAboutDAO {
	
	private ResourceFetcher resourceFetcher;
	private ObjectMapper mapper;
	
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	
	@Value("${question.s3.bucket.key-themes-talk-about.json}")
	private String keyThemesTalkAboutLocation;
	
	@Autowired
	public KeyThemesTalkAboutDAOImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
		this.mapper = new ObjectMapper();
	}

	public Map<String, Object> get(){		
		return fetchData(bucketLocation+keyThemesTalkAboutLocation);
	}
	
	public Map<String, Object> get(String projectUuid){		
		return fetchData(bucketLocation + projectUuid + "/output/" + keyThemesTalkAboutLocation);
	}
	
	protected final Map<String, Object> fetchData(String location){
		Map<String, Object> nodeMap = new HashMap<>();
		String jsonString = resourceFetcher.fetch(location);
		
		try {
			JsonNode jsonNode = mapper.readTree(jsonString);
			Streams.stream(jsonNode.fields()).forEach( (entry) -> {
				
				//TODO: This NEEDS a better approach
				if(entry.getKey().compareToIgnoreCase("date_from") == 0 || entry.getKey().compareToIgnoreCase("date_to") == 0 || entry.getKey().compareToIgnoreCase("last_updated_date") == 0 ){
					nodeMap.put(entry.getKey(), entry.getValue().asText() );
				}
				else{
					nodeMap.put(entry.getKey(), new BigDecimal(entry.getValue().asText()) );
				}
				
			} );
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidJsonFromDataSourceException();
		}
		
		return nodeMap;
	}
	
}
