package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.service.OtherTopicsCareAboutService;

/**
 * 
 * Controller for Question "What other topics do they care about?"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/other-topics-care-about")
public class OtherTopicsCareAboutController {
	
	private OtherTopicsCareAboutService otherTopicsCareAboutService;
	
	@Autowired
	public OtherTopicsCareAboutController(OtherTopicsCareAboutService otherTopicsCareAboutService) {
		this.otherTopicsCareAboutService = otherTopicsCareAboutService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/top-post-interests"})
	private QuestionResultListContainerDTO getTopPostInterests(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0){
			return otherTopicsCareAboutService.getTopPostInterests(projectId);
		}
		else{
			return otherTopicsCareAboutService.getTopPostInterests();
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/top-source-interests","/ratio-per-category"})
	//ratio-per-category needs to be deprecated
	private QuestionResultListContainerDTO getTopSourceInterests(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0){
			return otherTopicsCareAboutService.getTopSourceInterests(projectId);
		}
		else{
			return otherTopicsCareAboutService.getTopSourceInterests();
		}
	}
}
