package com.jwt.cic.questions.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jwt.cic.charts.dto.CandleStickChartDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;

public class VolumeChangedDayOverDayDTO implements QuestionResultDTO {
	@Deprecated
	@JsonIgnore
	private SplineChartDTO splineChart;
	private CandleStickChartDTO candleStickChart;
	private long lastUpdatedDate;


	@Deprecated
	public SplineChartDTO getSplineChart() {
		return splineChart;
	}

	@Deprecated
	public void setSplineChart(SplineChartDTO splineChart) {
		this.splineChart = splineChart;
	}

	public CandleStickChartDTO getCandleStickChart() {
		return candleStickChart;
	}

	public void setCandleStickChart(CandleStickChartDTO candleStickChart) {
		this.candleStickChart = candleStickChart;
	}

	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
