
package com.jwt.cic.questions.gtrends;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.exceptions.FetchingResourceException;
import com.jwt.cic.questions.gtrends.dto.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


public class GoogleTrendsRequest {

    private URIBuilder builder;
    private String[] keywords = null;
    private String csvUrlString;
    private String generalUrlString;


    public GoogleTrendsRequest(String[] keywords, String csvUrlString, String generalUrlString) {
        this.keywords = keywords;
        this.csvUrlString = csvUrlString;
        this.generalUrlString = generalUrlString;
    }

    public HttpRequestBase buildTokenRequest() {
        HttpRequestBase request = null;
        try {
            builder = new URIBuilder(generalUrlString);
            builder.addParameter("hl", "en-US");
            builder.addParameter("tz", "-330");
            builder.addParameter("req", buildTokenRequestParam(keywords));
            String uriString = builder.build().toString().replaceAll("\\+", "%20");
            request = new HttpGet(uriString);
        } catch (Exception ex) {
            throw new FetchingResourceException();
        }
        return request;
    }

    public HttpRequestBase buildCsvRequest(String token) {
        HttpRequestBase request = null;
        try {
            builder = new URIBuilder(csvUrlString);
            builder.addParameter("tz", "-330");
            builder.addParameter("token", token);
            builder.addParameter("req", buildCsvRequestParam(keywords).replace("\"{}\"", "{}"));
            String uriString = builder.build().toString().replaceAll("\\+", "%20");
            request = new HttpGet(uriString);
            setupHttpRequestDefaults(request);
        } catch (URISyntaxException ex) {
            throw new FetchingResourceException();
        }
        return request;
    }

    private String buildTokenRequestParam(String[] keyWords) throws IOException {
        TokenRequestDto json = new TokenRequestDto();
        json.setComparisonItem(
                Arrays.asList(keyWords).stream().map(
                        brand -> {

                            TokenComparisonItem comparisonItem = new TokenComparisonItem();
                            comparisonItem.setKeyword(brand);
                            comparisonItem.setTime("today 3-m");
                            return comparisonItem;
                        }
                ).collect(Collectors.toList()));
        StringWriter sw = new StringWriter();
        ObjectMapper om = new ObjectMapper();
        om.writeValue(sw, json);
        String jsonString = sw.toString();
        sw.close();
        return jsonString;
    }

    private String buildCsvRequestParam(String[] keyWords) {
        String jsonString = null;
        String formAndToDateString = getFormAndToDateString();
        try {
            CsvRequestDto json = new CsvRequestDto();
            json.setComparisonItem(getKeyWordList(keyWords));
            json.setTime(formAndToDateString);
            json.setRequestOptions(new RequestOptions());
            StringWriter sw = new StringWriter();
            ObjectMapper om = new ObjectMapper();
            om.writeValue(sw, json);
            jsonString = sw.toString();
            sw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jsonString;
    }

    private String getFormAndToDateString() {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        String from = sd.format(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        Date toDate = calendar.getTime();
        String to = sd.format(toDate);
        return to + " " + from;
    }

    private List<CsvComparisonItem> getKeyWordList(String keyWords[]) {
        return Arrays.asList(keyWords).stream().map(
                brand -> {
                    List<KeyWord> keyWordList = new ArrayList<>();
                    KeyWord keyWord = new KeyWord();
                    keyWord.setValue(brand);
                    keyWordList.add(keyWord);
                    ComplexKeywordsRestriction complexKeywordsRestriction = new ComplexKeywordsRestriction();
                    complexKeywordsRestriction.setKeyword(keyWordList);
                    CsvComparisonItem csvComparisonItem = new CsvComparisonItem();
                    csvComparisonItem.setComplexKeywordsRestriction(complexKeywordsRestriction);
                    return csvComparisonItem;
                }
        ).collect(Collectors.toList());
    }

    public static void setupHttpRequestDefaults(HttpRequestBase r) {
        r.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
        r.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        r.addHeader("Accept-Language", "en-US");
        r.addHeader("Accept-Encoding", "gzip, deflate");
        r.addHeader("Connection", "keep-alive");

    }
}
