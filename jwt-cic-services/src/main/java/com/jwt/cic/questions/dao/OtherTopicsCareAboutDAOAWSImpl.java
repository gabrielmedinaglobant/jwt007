package com.jwt.cic.questions.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.OtherTopicsCareAboutEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class OtherTopicsCareAboutDAOAWSImpl implements OtherTopicsCareAboutDAO {
	private final Logger log = LoggerFactory.getLogger(OtherTopicsCareAboutDAOAWSImpl.class);

	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.other-topics-care-about.json}")
	private String resourceLocation;
	
	@Autowired
	public OtherTopicsCareAboutDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}
	
	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> get() {
		return resourceFetcher.fetchObjects(bucketLocation+resourceLocation, OtherTopicsCareAboutEtlResultDTO.class);
	}
	
	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> get(String projectUuid) {
		return resourceFetcher.fetchObjects(bucketLocation + projectUuid + "/output/" + resourceLocation, OtherTopicsCareAboutEtlResultDTO.class);
	}

	/**
	 * 
	 * Collects and filters only ETL result data of Top Sources Interests
	 * TODO: This may probably fit better on DAO?
	 * 
	 * @param etlResultDTOList
	 * @return
	 * 
	 */
	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> getTopPostInterestsData() {
		return get().stream().filter( (etlResultDTO) -> {
			return etlResultDTO.getRatio() == null;
		} ).collect( Collectors.toList() );
	}

	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> getTopPostInterestsData(String projectUuid) {
		return get(projectUuid).stream().filter( (etlResultDTO) -> {
			return etlResultDTO.getRatio() == null;
		} ).collect( Collectors.toList() );
	}
	
	/**
	 * 
	 * Collects and filters only ETL result data of Top Sources Interests
	 * TODO: This may probably fit better on DAO?
	 * 
	 * @param etlResultDTOList
	 * @return
	 * 
	 */
	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> getTopSourceInterestsData() {
		return get().stream().filter( (etlResultDTO) -> {
			return etlResultDTO.getRatio() != null;
		} ).collect( Collectors.toList() );
	}

	@Override
	public List<OtherTopicsCareAboutEtlResultDTO> getTopSourceInterestsData(String projectUuid) {
		return get(projectUuid).stream().filter( (etlResultDTO) -> {
			return etlResultDTO.getRatio() != null;
		} ).collect( Collectors.toList() );
	}
}
