package com.jwt.cic.questions.dao;

import java.math.BigDecimal;
import java.util.Map;

public interface KeyThemesTalkAboutDAO {
	Map<String, Object> get();
	Map<String, Object> get(String projectUuid);
}
