package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
	MALE("male"), FEMALE("female"), UNKNOWN("unknown");
	
	private String value;
	
	private static final Map<String, Gender> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private Gender(String value){
		this.value = value;
	}
	
	private static Map<String, Gender> initializeValueMap(){
		Map<String, Gender> valueMap = new HashMap<>();
		for (Gender ct : Gender.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static Gender getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, Gender> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}
}
