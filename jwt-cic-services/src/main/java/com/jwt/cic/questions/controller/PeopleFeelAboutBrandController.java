package com.jwt.cic.questions.controller;

import com.jwt.cic.questions.service.PeopleFeelAboutBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;

/**
 * 
 * Controller for Question "How do people feel about my brand?"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/people-feel-about-brand")
public class PeopleFeelAboutBrandController {

	private PeopleFeelAboutBrandService peopleFeelAboutBrandService;

	@Autowired
	public PeopleFeelAboutBrandController(PeopleFeelAboutBrandService peopleFeelAboutBrandService){
		this.peopleFeelAboutBrandService=peopleFeelAboutBrandService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	private QuestionResultListContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0){
			return peopleFeelAboutBrandService.get(projectId);
		}
		else{
			return peopleFeelAboutBrandService.get();
		}
	}

}
