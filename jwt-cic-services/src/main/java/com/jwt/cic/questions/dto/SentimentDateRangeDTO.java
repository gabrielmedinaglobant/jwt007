package com.jwt.cic.questions.dto;

import com.jwt.cic.questions.util.SentimentType;

public class SentimentDateRangeDTO {
	private SentimentType sentimentType;
	private long starts;
	private long ends;
	
	public SentimentDateRangeDTO(SentimentType sentimentType, long starts, long ends) {
		this.sentimentType = sentimentType;
		this.starts = starts;
		this.ends = ends;
	}
	
	public SentimentType getSentimentType() {
		return sentimentType;
	}
	public void setSentimentType(SentimentType sentimentType) {
		this.sentimentType = sentimentType;
	}
	public long getStarts() {
		return starts;
	}
	public void setStarts(long starts) {
		this.starts = starts;
	}
	public long getEnds() {
		return ends;
	}
	public void setEnds(long ends) {
		this.ends = ends;
	}
	
	
}
