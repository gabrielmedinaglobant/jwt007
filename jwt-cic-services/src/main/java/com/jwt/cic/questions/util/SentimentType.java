package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SentimentType {
	POSITIVE("positive"), NEGATIVE("negative"), MIXED("mixed");
	
	private String value;
	
	private static final Map<String, SentimentType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private SentimentType(String value){
		this.value = value;
	}
	
	private static Map<String, SentimentType> initializeValueMap(){
		Map<String, SentimentType> valueMap = new HashMap<>();
		for (SentimentType ct : SentimentType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static SentimentType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, SentimentType> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}
	
}
