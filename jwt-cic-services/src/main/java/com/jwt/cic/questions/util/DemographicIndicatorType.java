package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum DemographicIndicatorType {
	HOUSEHOLD_INCOME("household-income","Household Income","$"), DISPOSABLE_INCOME("disposable-income","Disposable Income","$"), 
	HOUSEHOLD_SIZE("household-size","Household Size",""), HOUSEHOLD_VALUE("household-value","Household Value","$"), 
	HIGHER_EDUCATION("higher-education","Higher Education","%");
	
	private String value;
	private String displayName;
	private String unit;
	
	private static final Map<String, DemographicIndicatorType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private DemographicIndicatorType(String value, String displayName, String unit) {
		this.value = value;
		this.displayName = displayName;
		this.unit = unit;
	}
	private static Map<String, DemographicIndicatorType> initializeValueMap(){
		Map<String, DemographicIndicatorType> valueMap = new HashMap<>();
		for (DemographicIndicatorType ct : DemographicIndicatorType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	public static DemographicIndicatorType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
}
