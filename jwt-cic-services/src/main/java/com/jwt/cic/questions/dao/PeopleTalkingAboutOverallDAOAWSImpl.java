package com.jwt.cic.questions.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.utils.ResourceFetcher;

/**
 * 
 * AWS S3 DAO Implementation for Q6/23 - Discover - Question "What are people talking about overall?"
 * 
 * @author gabriel.medina
 *
 */
@Repository
public class PeopleTalkingAboutOverallDAOAWSImpl implements PeopleTalkingAboutOverallDAO {

	private ObjectMapper mapper = new ObjectMapper();

    private ResourceFetcher resourceFetcher;
    @Value("${question.s3.bucket}")
    private String bucketLocation;
    @Value("${question.s3.bucket.people-talking-about-overall.json}")
    private String resourceLocation;
	    
	public PeopleTalkingAboutOverallDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	public Map<String, String> getAll() {        
        return fetchData(bucketLocation + resourceLocation);
	}
	
	@Override
	public Map<String, String> getAll(String projectUuid) {
		return fetchData(bucketLocation + projectUuid + "/output/" + resourceLocation);
	}

	protected final Map<String, String> fetchData(String location){
		Map<String, String> nodeMap = new HashMap<>();
        String jsonString = resourceFetcher.fetch(location);

        try {
            JsonNode jsonNode = mapper.readTree(jsonString);
            Streams.stream(jsonNode.fields()).forEach((entry) -> {
                nodeMap.put(entry.getKey(), entry.getValue().asText());
            });
        } catch (IOException e) {
        	e.printStackTrace();
        	throw new InvalidJsonFromDataSourceException();
        }
        return nodeMap;
	}
	
}
