package com.jwt.cic.questions.controller;

import com.jwt.cic.questions.service.PeopleLookedCompetitorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

/**
 * 
 * Controller for Q6/23 - Discover - Question "How much have people looked for me in comparison to competitors?"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/people-looked-competitors")
public class PeopleLookedCompetitorsController {

	private PeopleLookedCompetitorsService peopleLookedCompetitorsService;

	@Autowired
	public PeopleLookedCompetitorsController(PeopleLookedCompetitorsService peopleLookedCompetitorsService) {
		this.peopleLookedCompetitorsService = peopleLookedCompetitorsService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/", ""})
	public QuestionSingleResultContainerDTO get(
			@RequestParam(value = "brand", defaultValue = "", required=false) String [] brands){
		return peopleLookedCompetitorsService.get(brands);
	}

}
