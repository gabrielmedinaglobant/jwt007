package com.jwt.cic.questions.dto;

public interface QuestionResultContainerDTO<T> {
	T getValue();
    void setValue(T value);
    Long getLastUpdatedDate();
    void setLastUpdatedDate(Long lastUpdatedDate);
    Long getDateFrom();
    void setDateFrom(Long dateFrom);
    Long getDateTo();
    void setDateTo(Long dateTo);
}
