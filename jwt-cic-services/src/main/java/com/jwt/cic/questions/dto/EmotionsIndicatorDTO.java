package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.jwt.cic.questions.util.EmotionType;

public class EmotionsIndicatorDTO implements QuestionResultDTO {
	private EmotionType emotionType;
	private BigDecimal value;
	private String unit;
	
	
	public EmotionsIndicatorDTO(EmotionType emotionType, BigDecimal value, String unit) {
		this.emotionType = emotionType;
		this.value = value;
		this.unit = unit;
	}
	public EmotionType getEmotionType() {
		return emotionType;
	}
	public void setEmotionType(EmotionType emotionType) {
		this.emotionType = emotionType;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
}
