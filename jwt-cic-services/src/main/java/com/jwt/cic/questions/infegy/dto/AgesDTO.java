package com.jwt.cic.questions.infegy.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgesDTO extends InfegyResultDTO {
	private List<AgesOutputElementDTO> output;
	
	public List<AgesOutputElementDTO> getOutput() {
		return output;
	}

	public void setOutput(List<AgesOutputElementDTO> output) {
		this.output = output;
	}
}
