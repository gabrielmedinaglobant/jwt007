package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputResultsDTO {
	
	@JsonProperty("last_updated_date")
	public String lastUpdatedDate;
	@JsonProperty("elapsed_timestamp")
	public BigDecimal elapsedTimestamp;
	@JsonProperty("last_updated_timestamp")
	public BigDecimal lastUpdatedTimestamp;
	
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public BigDecimal getElapsedTimestamp() {
		return elapsedTimestamp;
	}
	public void setElapsedTimestamp(BigDecimal elapsedTimestamp) {
		this.elapsedTimestamp = elapsedTimestamp;
	}
	public BigDecimal getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}
	public void setLastUpdatedTimestamp(BigDecimal lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}
	
}
