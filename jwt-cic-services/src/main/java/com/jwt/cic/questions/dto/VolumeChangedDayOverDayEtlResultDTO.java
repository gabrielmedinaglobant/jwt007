package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeChangedDayOverDayEtlResultDTO {
	
	private String date;
	
	private BigDecimal high;
	private BigDecimal low;
	@JsonProperty("absolute_high")
	private BigDecimal absolutehigh;
	@JsonProperty("absolute_low")
	private BigDecimal absolutelow;
	@JsonProperty("relative_high")
	private BigDecimal relativehigh;
	@JsonProperty("relative_low")
	private BigDecimal relativelow;
	private String trend;
	private BigDecimal value;
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	
	@Deprecated
	@JsonProperty("posts_word_normalized_prom")
	private BigDecimal postsWordNormalizedProm;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getAbsolutehigh() {
		return absolutehigh;
	}

	public void setAbsolutehigh(BigDecimal absolutehigh) {
		this.absolutehigh = absolutehigh;
	}

	public BigDecimal getAbsolutelow() {
		return absolutelow;
	}

	public void setAbsolutelow(BigDecimal absolutelow) {
		this.absolutelow = absolutelow;
	}

	public BigDecimal getRelativehigh() {
		return relativehigh;
	}

	public void setRelativehigh(BigDecimal relativehigh) {
		this.relativehigh = relativehigh;
	}

	public BigDecimal getRelativelow() {
		return relativelow;
	}

	public void setRelativelow(BigDecimal relativelow) {
		this.relativelow = relativelow;
	}

	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Deprecated
	public BigDecimal getPostsWordNormalizedProm() {
		return postsWordNormalizedProm;
	}

	@Deprecated
	public void setPostsWordNormalizedProm(BigDecimal postsWordNormalizedProm) {
		this.postsWordNormalizedProm = postsWordNormalizedProm;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	
	
}
