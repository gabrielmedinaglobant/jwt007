package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.charts.dto.SplineChartDTO;

public class ImmigrationGenderTrendDTO {
	private SplineChartDTO genderSplineChart;
	private PersonFillChartDTO femalePersonFillChart;
	private List<ImmigrationGenderIndicatorDTO> femaleIndicators;
	private PersonFillChartDTO malePersonFillChart;
	private List<ImmigrationGenderIndicatorDTO> maleIndicators;
	private List<ImmigrationGenderIndicatorDTO> indicators;
	
	public SplineChartDTO getGenderSplineChart() {
		return genderSplineChart;
	}
	public void setGenderSplineChart(SplineChartDTO genderSplineChart) {
		this.genderSplineChart = genderSplineChart;
	}
	public PersonFillChartDTO getFemalePersonFillChart() {
		return femalePersonFillChart;
	}
	public void setFemalePersonFillChart(PersonFillChartDTO femalePersonFillChart) {
		this.femalePersonFillChart = femalePersonFillChart;
	}
	public List<ImmigrationGenderIndicatorDTO> getFemaleIndicators() {
		return femaleIndicators;
	}
	public void setFemaleIndicators(List<ImmigrationGenderIndicatorDTO> femaleIndicators) {
		this.femaleIndicators = femaleIndicators;
	}
	public PersonFillChartDTO getMalePersonFillChart() {
		return malePersonFillChart;
	}
	public void setMalePersonFillChart(PersonFillChartDTO malePersonFillChart) {
		this.malePersonFillChart = malePersonFillChart;
	}
	public List<ImmigrationGenderIndicatorDTO> getMaleIndicators() {
		return maleIndicators;
	}
	public void setMaleIndicators(List<ImmigrationGenderIndicatorDTO> maleIndicators) {
		this.maleIndicators = maleIndicators;
	}
	public List<ImmigrationGenderIndicatorDTO> getIndicators() {
		return indicators;
	}
	public void setIndicators(List<ImmigrationGenderIndicatorDTO> indicators) {
		this.indicators = indicators;
	}
	
	
}
