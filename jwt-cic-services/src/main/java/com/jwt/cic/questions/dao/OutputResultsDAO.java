package com.jwt.cic.questions.dao;

import com.jwt.cic.questions.dto.OutputResultsDTO;

public interface OutputResultsDAO {
	OutputResultsDTO get();
}
