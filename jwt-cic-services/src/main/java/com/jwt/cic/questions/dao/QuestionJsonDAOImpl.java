package com.jwt.cic.questions.dao;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import com.jwt.cic.utils.JsonFetcher;

@Deprecated
@Repository
public class QuestionJsonDAOImpl implements QuestionJsonDAO {
	
	private JsonFetcher jsonFromHttpFetcher;
	
	@Value("${question.brands-people-talking-about.documents.json.url}")
	private String brandsPeopleTalkingAboutDocumentsUrl;
	@Value("${question.brands-people-talking-about.sentences.json.url}")
	private String brandsPeopleTalkingAboutSentencesUrl;
	@Value("${question.brands-people-talking-about.subject-sentences.json.url}")
	private String brandsPeopleTalkingAboutSubjectSentencesUrl;
	@Value("${question.key-themes-talk-about.json.url}")
	private String keyThemesTalkAbout;
	@Value("${question.sentiment-change-day-over-day.json.url}")
	private String sentimentChangeDayOverDay;
	@Value("${question.talking-about-me.json.url}")
	private String talkingAboutMe;
	@Value("${question.volume-change-day-over-day.json.url}")
	private String volumeChangedDayOverDay;
	
	@Autowired
	public QuestionJsonDAOImpl(JsonFetcher jsonFromHttpFetcher){
		this.jsonFromHttpFetcher = jsonFromHttpFetcher;
	}
	
	@Override
	public JSONObject getBrandsPeopleTalkingAboutDocuments() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.brandsPeopleTalkingAboutDocumentsUrl);
	}

	@Override
	public JSONObject getBrandsPeopleTalkingAboutSentences() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.brandsPeopleTalkingAboutSentencesUrl);
	}

	@Override
	public JSONObject getBrandsPeopleTalkingAboutSubjectSentences() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.brandsPeopleTalkingAboutSubjectSentencesUrl);
	}

	@Override
	public JSONObject getKeyThemesTalkAbout() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.keyThemesTalkAbout);
	}

	@Override
	public JSONObject getSentimentChangeDayOverDay() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.sentimentChangeDayOverDay);
	}

	@Override
	public JSONObject getTalkingAboutMe() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.talkingAboutMe);
	}

	@Override
	public JSONObject getVolumeChangedDayOverDay() {
		return this.jsonFromHttpFetcher.fetchFromUrl(this.volumeChangedDayOverDay);
	}

	@Override
	public JSONObject getSentimentChangeDayOverDay(String projectUuid) {
		// TODO Auto-generated method stub
		return getSentimentChangeDayOverDay();
	}

	
	
	
}
