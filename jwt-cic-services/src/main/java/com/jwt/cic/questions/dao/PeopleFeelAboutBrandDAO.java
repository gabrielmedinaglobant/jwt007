package com.jwt.cic.questions.dao;

import com.jwt.cic.questions.dto.PeopleFeelAboutBrandEtlResultDTO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandOptionDTO;

import java.util.List;

/**
 * Created by vishal.domale on 10-04-2017.
 */
public interface PeopleFeelAboutBrandDAO {
    List<PeopleFeelAboutBrandEtlResultDTO> get();
    List<PeopleFeelAboutBrandEtlResultDTO> get(String projectUuid);
}
