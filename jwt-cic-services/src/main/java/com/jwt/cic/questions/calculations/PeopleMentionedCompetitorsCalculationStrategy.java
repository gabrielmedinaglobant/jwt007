package com.jwt.cic.questions.calculations;

import java.util.Map;

import com.jwt.cic.queries.domain.QueryTypePerProjectViewElement;
import com.jwt.cic.questions.infegy.dto.VolumeDTO;

public interface PeopleMentionedCompetitorsCalculationStrategy {
	PeopleMentionedCompetitorsCalculationResult calculate(Map<String, QueryTypePerProjectViewElement> queryTypePerProjectElementsMap, Map<String, VolumeDTO> volumeResultsPerQueryType);
}
