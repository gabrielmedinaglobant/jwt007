package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.common.collect.Streams;
import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.TalkingAboutMeDAO;
import com.jwt.cic.questions.dto.AgeDistributionDTO;
import com.jwt.cic.questions.dto.DemographicIndicatorDTO;
import com.jwt.cic.questions.dto.ImmigrationGenderIndicatorDTO;
import com.jwt.cic.questions.dto.ImmigrationGenderTrendDTO;
import com.jwt.cic.questions.dto.IndicatorTrend;
import com.jwt.cic.questions.dto.LanguageDistributionDTO;
import com.jwt.cic.questions.dto.LanguageIndicatorDTO;
import com.jwt.cic.questions.dto.PersonFillChartDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.TalkingAboutMeDTO;
import com.jwt.cic.questions.dto.TalkingAboutMeEtlResultDTO;
import com.jwt.cic.questions.dto.UsDemographicsDTO;
import com.jwt.cic.questions.util.DemographicIndicatorType;
import com.jwt.cic.questions.util.Gender;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;

// TODO: Language implementation doesn't seem to be correct
// TalkingAboutMeDAO would have to include an additional method
// that gets JSON as Map, so that we could get languages dynamically

@Primary
@Service
public class TalkingAboutMeServiceImpl implements TalkingAboutMeService {

    private final Logger log = LoggerFactory.getLogger(TalkingAboutMeServiceImpl.class);

    private TalkingAboutMeDAO talkingAboutMeDAO;
    private ProjectDAO projectDAO;
    private QuestionResultContainerBuilder questionResultContainerBuilder;

    @Autowired
    public TalkingAboutMeServiceImpl(TalkingAboutMeDAO talkingAboutMeDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
        this.talkingAboutMeDAO = talkingAboutMeDAO;
        this.projectDAO = projectDAO;
        this.questionResultContainerBuilder = questionResultContainerBuilder;
    }

    @Override
    @Cacheable("question.talking-about-me.0")
    public QuestionSingleResultContainerDTO get() {
        List<TalkingAboutMeEtlResultDTO> etlResultDTOList = talkingAboutMeDAO.get();
        
        return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(etlResultDTOList), buildDTO(etlResultDTOList));
    }

    @Override
    @Cacheable("question.talking-about-me.1")
    public QuestionSingleResultContainerDTO get(long projectId) {
        Project project = projectDAO.findOne(projectId);
        if (project == null) {
            throw new ProjectNotFoundException();
        }

        List<TalkingAboutMeEtlResultDTO> etlResultDTOList = talkingAboutMeDAO.get(project.getUuid());

        TalkingAboutMeDTO responseDTO = buildDTO(etlResultDTOList);
        Long lastUpdatedDate = getLastUpdatedDate(etlResultDTOList);
        
        responseDTO.setLastUpdatedDate(lastUpdatedDate); 
        return questionResultContainerBuilder.buildDTO(lastUpdatedDate, responseDTO);
    }
    
    protected final Long getLastUpdatedDate( List<TalkingAboutMeEtlResultDTO> etlResultDTOList ){
    	String dateStr = etlResultDTOList
						    			.stream()
						    			.filter( (item) -> item.getLastUpdatedDate() != null )
						    			.map( (item) -> item.getLastUpdatedDate() )
						    			.findFirst().get();
    	return DateConverter.fromISODateToEpochSeconds( dateStr );
    }
    
    protected final Long getDateFrom( List<TalkingAboutMeEtlResultDTO> etlResultDTOList ){
    	String dateStr = etlResultDTOList
    			.stream()
    			.filter( (item) -> item.getDateFrom() != null )
    			.map( (item) -> item.getDateFrom() )
    			.findFirst().get();
		return DateConverter.fromISODateToEpochSecondsTrimmed( dateStr );
    }
    
    protected final Long getDateTo( List<TalkingAboutMeEtlResultDTO> etlResultDTOList ){
    	String dateStr = etlResultDTOList
    			.stream()
    			.filter( (item) -> item.getDateTo() != null )
    			.map( (item) -> item.getDateTo() )
    			.findFirst().get();
		return DateConverter.fromISODateToEpochSecondsTrimmed( dateStr );
    }

    protected final TalkingAboutMeDTO buildDTO(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        TalkingAboutMeDTO talkingAboutMeDTO = new TalkingAboutMeDTO();

        // talkingAboutMeDTO.setImmigrationGenderTrend( buildImmigrationGenderTrend(talkingAboutMeEtlResultDTOList) );
        talkingAboutMeDTO.setImmigrationGenderTrend(
                buildImmigrationGenderTrend(
                        getImmigrationGenderTrendOvertimeData(etlResultDTOList),
                        getImmigrationGenderTrendIndicatorsData(etlResultDTOList)
                )
        );
        talkingAboutMeDTO.setLanguageDistribution(
                buildLanguageDistribution(
                        getLanguageDistributionIndicatorsData(etlResultDTOList)
                )
        );
        talkingAboutMeDTO.setAgeDistribution(
                buildAgeDistribution(
                        getAgeDistributionData(etlResultDTOList)
                )
        );
        talkingAboutMeDTO.setUsDemographics(buildUsDemographics(getUsDemographicsData(etlResultDTOList)));

        return talkingAboutMeDTO;
    }

    private TalkingAboutMeEtlResultDTO getImmigrationGenderTrendIndicatorsData(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        return etlResultDTOList.stream().filter((etlResultDTO) -> {
            return etlResultDTO.getDaysLeadingFemale() != null;
        }).findFirst().get();
    }

    private List<TalkingAboutMeEtlResultDTO> getImmigrationGenderTrendOvertimeData(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        return etlResultDTOList.stream().filter((etlResultDTO) -> {
            return etlResultDTO.getDate() != null &&
                    etlResultDTO.getFemaleByWeek() != null &&
                    etlResultDTO.getMaleByWeek() != null;
        }).collect(Collectors.toList());
    }

    //TODO: This is not a good implementation for this JSON mapping
    private TalkingAboutMeEtlResultDTO getLanguageDistributionIndicatorsData(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        return etlResultDTOList.stream().filter((etlResultDTO) -> {
            return etlResultDTO.getLanguageOther() != null;
        }).findFirst().get();
    }

    private List<TalkingAboutMeEtlResultDTO> getAgeDistributionData(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        return etlResultDTOList.stream().filter((etlResultDTO) -> {
            return etlResultDTO.getDisplayName() != null &&
                    etlResultDTO.getProbability() != null;
        }).collect(Collectors.toList());
    }

    private List<TalkingAboutMeEtlResultDTO> getUsDemographicsData(List<TalkingAboutMeEtlResultDTO> etlResultDTOList) {
        return etlResultDTOList.stream().filter((etlResultDTO) -> {
            return etlResultDTO.getDemographicsType() != null;
        }).collect(Collectors.toList());
    }

    @Deprecated
    private ImmigrationGenderTrendDTO buildImmigrationGenderTrend(List<TalkingAboutMeEtlResultDTO> talkingAboutMeEtlResultDTOList) {
        ImmigrationGenderTrendDTO immigrationGenderTrendDTO = new ImmigrationGenderTrendDTO();
        TalkingAboutMeEtlResultDTO indicatorsData = getImmigrationGenderTrendIndicatorsData(talkingAboutMeEtlResultDTOList);

        immigrationGenderTrendDTO.setGenderSplineChart(buildGenderSplineChart(
                talkingAboutMeEtlResultDTOList.stream().filter((etlResultDTO) -> {
                    return etlResultDTO.getDate() != null &&
                            etlResultDTO.getFemaleByWeek() != null &&
                            etlResultDTO.getMaleByWeek() != null;
                }).collect(Collectors.toList())
        ));
        immigrationGenderTrendDTO.setFemalePersonFillChart(buildFemalePersonFillChart(indicatorsData));
        immigrationGenderTrendDTO.setFemaleIndicators(buildFemaleIndicators(indicatorsData));
        immigrationGenderTrendDTO.setMalePersonFillChart(buildMalePersonFillChart(indicatorsData));
        immigrationGenderTrendDTO.setMaleIndicators(buildMaleIndicators(indicatorsData));
        immigrationGenderTrendDTO.setIndicators(
                Arrays.asList(
                        new ImmigrationGenderIndicatorDTO[]{
                                new ImmigrationGenderIndicatorDTO("gendered-universe", BigDecimal.valueOf(83), "%", "Gendered Universe")
                        }
                )
        );

        return immigrationGenderTrendDTO;
    }

    private ImmigrationGenderTrendDTO buildImmigrationGenderTrend(List<TalkingAboutMeEtlResultDTO> overtimeData, TalkingAboutMeEtlResultDTO indicatorsData) {
        ImmigrationGenderTrendDTO immigrationGenderTrendDTO = new ImmigrationGenderTrendDTO();

        immigrationGenderTrendDTO.setGenderSplineChart(buildGenderSplineChart(overtimeData));
        immigrationGenderTrendDTO.setFemalePersonFillChart(buildFemalePersonFillChart(indicatorsData));
        immigrationGenderTrendDTO.setFemaleIndicators(buildFemaleIndicators(indicatorsData));
        immigrationGenderTrendDTO.setMalePersonFillChart(buildMalePersonFillChart(indicatorsData));
        immigrationGenderTrendDTO.setMaleIndicators(buildMaleIndicators(indicatorsData));
        immigrationGenderTrendDTO.setIndicators(
                Arrays.asList(
                        new ImmigrationGenderIndicatorDTO[]{
                                new ImmigrationGenderIndicatorDTO("gendered-universe", indicatorsData.getGenderedUniverseAvg().setScale(0, RoundingMode.HALF_UP), "%", "Gendered Universe")
                        }
                )
        );

        return immigrationGenderTrendDTO;
    }

    private SplineChartDTO buildGenderSplineChart(List<TalkingAboutMeEtlResultDTO> overtimeData) {
        SplineChartDTO splineChartDTO = new SplineChartDTO();
        List<SplineChartLineDTO> lines = new ArrayList<>();

        lines.add(
                buildGenderSplineChartFemaleLine(overtimeData)
        );
        lines.add(
                buildGenderSplineChartMaleLine(overtimeData)
        );

        splineChartDTO.setLines(lines);

        return splineChartDTO;
    }

    private SplineChartLineDTO buildGenderSplineChartFemaleLine(List<TalkingAboutMeEtlResultDTO> overtimeData) {
        return new SplineChartLineDTO(
                Gender.FEMALE.getValue(),
                overtimeData.stream().map((etlDTO) -> {
                    return new ArrayValueDTO(
                            // "Date: " + etlDTO.getDate() + ", Sum: " + etlDTO.getFemaleSumByWeek(),
                            etlDTO.getDate(),
                            new BigDecimal[]{
                                    DateConverter.fromISODateToEpochSecondsAsBigDecimal(etlDTO.getDate()),
                                    etlDTO.getFemaleSumByWeek()
                                    // trimValueForGenderChart(etlDTO.getFemaleSumByWeek())
                            }
                    );
                })
                        .sorted(new GenderSplineChartComparator())
                        .collect(Collectors.toList())
        );
    }

    private SplineChartLineDTO buildGenderSplineChartMaleLine(List<TalkingAboutMeEtlResultDTO> overtimeData) {
        return new SplineChartLineDTO(
                Gender.MALE.getValue(),
                overtimeData.stream().map((etlDTO) -> {
                    return new ArrayValueDTO(
                            // "Date: " + etlDTO.getDate() + ", Sum: " + etlDTO.getMaleSumByWeek(),
                            etlDTO.getDate(),
                            new BigDecimal[]{
                                    DateConverter.fromISODateToEpochSecondsAsBigDecimal(etlDTO.getDate()),
                                    etlDTO.getMaleSumByWeek().negate()
                                    // trimValueForGenderChart(etlDTO.getMaleSumByWeek()).negate()
                            }
                    );
                })
                        .sorted(new GenderSplineChartComparator())
                        .collect(Collectors.toList())
        );
    }

    private PersonFillChartDTO buildFemalePersonFillChart(TalkingAboutMeEtlResultDTO indicatorsData) {
        return new PersonFillChartDTO(indicatorsData.getFemaleUniverseAvg().divide(BigDecimal.valueOf(100L).setScale(1, RoundingMode.HALF_UP)));
    }

    private List<ImmigrationGenderIndicatorDTO> buildFemaleIndicators(TalkingAboutMeEtlResultDTO indicatorsData) {
        List<ImmigrationGenderIndicatorDTO> immigrationGenderIndicatorDTO = new ArrayList<ImmigrationGenderIndicatorDTO>();

        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("percentage", indicatorsData.getFemaleUniverseAvg().setScale(1, RoundingMode.HALF_UP), "%", null));
        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("universe-count", indicatorsData.getFemaleUniverse().setScale(1, RoundingMode.HALF_UP), null, "Universe Count"));
        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("days-leading", indicatorsData.getDaysLeadingFemale().setScale(1, RoundingMode.HALF_UP), null, "Days Leading"));

        return immigrationGenderIndicatorDTO;
    }

    private PersonFillChartDTO buildMalePersonFillChart(TalkingAboutMeEtlResultDTO indicatorsData) {
        return new PersonFillChartDTO(indicatorsData.getMaleUniverseAvg().divide(BigDecimal.valueOf(100L).setScale(1, RoundingMode.HALF_UP)));
    }

    private List<ImmigrationGenderIndicatorDTO> buildMaleIndicators(TalkingAboutMeEtlResultDTO indicatorsData) {
        List<ImmigrationGenderIndicatorDTO> immigrationGenderIndicatorDTO = new ArrayList<ImmigrationGenderIndicatorDTO>();

        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("days-leading", indicatorsData.getDaysLeadingMale().setScale(1, RoundingMode.HALF_UP), null, "Days Leading"));
        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("universe-count", indicatorsData.getMaleUniverse().setScale(1, RoundingMode.HALF_UP), null, "Universe Count"));
        immigrationGenderIndicatorDTO.add(new ImmigrationGenderIndicatorDTO("percentage", indicatorsData.getMaleUniverseAvg().setScale(1, RoundingMode.HALF_UP), "%", null));

        return immigrationGenderIndicatorDTO;
    }

    private LanguageDistributionDTO buildLanguageDistribution(TalkingAboutMeEtlResultDTO indicatorsData) {
        LanguageDistributionDTO languageDistributionDTO = new LanguageDistributionDTO();
        List<LanguageIndicatorDTO> indicators = new ArrayList<>();

        if (indicatorsData.getLanguageEn() != null)
            indicators.add(new LanguageIndicatorDTO("eng", "en", "English", indicatorsData.getLanguageEn().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageEs() != null)
            indicators.add(new LanguageIndicatorDTO("esp", "es", "Spanish", indicatorsData.getLanguageEs().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageFr() != null)
            indicators.add(new LanguageIndicatorDTO("fra", "fr", "French", indicatorsData.getLanguageFr().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageDe() != null)
            indicators.add(new LanguageIndicatorDTO("deu", "de", "German", indicatorsData.getLanguageDe().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageRu() != null)
            indicators.add(new LanguageIndicatorDTO("rus", "ru", "Russian", indicatorsData.getLanguageRu().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageIt() != null)
            indicators.add(new LanguageIndicatorDTO("ita", "it", "Italian", indicatorsData.getLanguageIt().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguagePt() != null)
            indicators.add(new LanguageIndicatorDTO("por", "pt", "Portuguese", indicatorsData.getLanguagePt().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageId() != null)
            indicators.add(new LanguageIndicatorDTO("ind", "id", "Indonesian", indicatorsData.getLanguageId().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageJa() != null)
            indicators.add(new LanguageIndicatorDTO("jpn", "ja", "Japanese", indicatorsData.getLanguageJa().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageVo() != null)
            indicators.add(new LanguageIndicatorDTO("vol", "vo", "Volapük", indicatorsData.getLanguageVo().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageTl() != null)
            indicators.add(new LanguageIndicatorDTO("tgl", "tl", "Tagalog", indicatorsData.getLanguageTl().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageMl() != null)
            indicators.add(new LanguageIndicatorDTO("mal", "ml", "Malayalam", indicatorsData.getLanguageMl().setScale(1, RoundingMode.HALF_UP), "%"));
        if (indicatorsData.getLanguageHi() != null)
            indicators.add(new LanguageIndicatorDTO("hin", "hi", "Hindi", indicatorsData.getLanguageHi().setScale(1, RoundingMode.HALF_UP), "%"));
        
        if (indicatorsData.getLanguageOther() != null)
            indicators.add(new LanguageIndicatorDTO(null, null, "Others", indicatorsData.getLanguageOther().setScale(1, RoundingMode.HALF_UP), "%"));

        languageDistributionDTO.setLanguageIndicator(indicators);
        return languageDistributionDTO;
    }

    private LanguageDistributionDTO buildLanguageDistribution(Map<String, BigDecimal> indicatorsMap) {
        //TODO: Implement this method so that it receives a map like this
        // "en"[string iso2 code],98.0[bigdecimal]
        // "es"[string iso2 code],...[bigdecimal]
        // "other"[string iso2 code],...[bigdecimal]
        //and returns LanguageDistributionDTO
        throw new RuntimeException("Not yet implemented");
    }

    private AgeDistributionDTO buildAgeDistribution(List<TalkingAboutMeEtlResultDTO> data) {
        AgeDistributionDTO ageDistributionDTO = new AgeDistributionDTO();
        ageDistributionDTO.setSplineChart(buildAgeDistributionSplineChart(data));
        return ageDistributionDTO;
    }

    private SplineChartDTO buildAgeDistributionSplineChart(List<TalkingAboutMeEtlResultDTO> data) {
        SplineChartDTO splineChartDTO = new SplineChartDTO();
        List<SplineChartLineDTO> lines = new ArrayList<>();

        lines.add(buildAgeDistributionSplineChartLine(data));
        splineChartDTO.setLines(lines);

        return splineChartDTO;
    }

    private SplineChartLineDTO buildAgeDistributionSplineChartLine(List<TalkingAboutMeEtlResultDTO> data) {
        List<TalkingAboutMeEtlResultDTO> sortedData = ImmutableList.sortedCopyOf(new AgeGroupComparator(), data);
        LinkedList<ArrayValueDTO> dataReadyForPlotting = new LinkedList<>();

        dataReadyForPlotting = Streams.mapWithIndex(sortedData.stream(), (etlDTO, idx) -> {
            return new ArrayValueDTO(
                    etlDTO.getDisplayName(),
                    new BigDecimal[]{
                            BigDecimal.valueOf(idx + 1L),
                            etlDTO.getProbability()
                    }
            );
        }).collect(Collectors.toCollection(LinkedList::new));

        dataReadyForPlotting.push(new ArrayValueDTO(
                "",
                new BigDecimal[]{
                        BigDecimal.ZERO,
                        BigDecimal.ZERO
                }
        ));
        dataReadyForPlotting.add(new ArrayValueDTO(
                "",
                new BigDecimal[]{
                        BigDecimal.valueOf(dataReadyForPlotting.size()),
                        BigDecimal.ZERO
                }
        ));

        return new SplineChartLineDTO("age-distribution", dataReadyForPlotting);
    }

    private UsDemographicsDTO buildUsDemographics(List<TalkingAboutMeEtlResultDTO> data) {
        //TODO: data... should be mapped

        UsDemographicsDTO usDemographicsDTO = new UsDemographicsDTO();
        List<DemographicIndicatorDTO> indicators = new ArrayList<DemographicIndicatorDTO>();

        indicators = data.stream().map((etlDTO) -> {
            DemographicIndicatorType t = DemographicIndicatorType.getByValue(etlDTO.getDemographicsType());
            return new DemographicIndicatorDTO(
                    t.getValue(),
                    IndicatorTrend.getFromSignum(etlDTO.getDemographicsValue().signum()),
                    etlDTO.getDemographicsValue().setScale(2, RoundingMode.HALF_UP).abs(),
                    t.getUnit(),
                    t.getDisplayName(),
                    etlDTO.getDemographicsMeanValue().setScale(2, RoundingMode.HALF_UP),
                    t.getUnit()
            );
        }).collect(Collectors.toList());

		/*
		indicators.add(new DemographicIndicatorDTO( "household-income",IndicatorTrend.PLUS,BigDecimal.valueOf(5322),"$","Household Income", BigDecimal.valueOf(58588), "$" ) );
		indicators.add(new DemographicIndicatorDTO( "disposable-income",IndicatorTrend.PLUS,BigDecimal.valueOf(90),"$","Disposable Income", BigDecimal.valueOf(21327), "$" ) );
		indicators.add(new DemographicIndicatorDTO( "household-size",IndicatorTrend.PLUS,BigDecimal.valueOf(15,2),"","Household Size", BigDecimal.valueOf(24,2), "" ) );
		indicators.add(new DemographicIndicatorDTO( "household-value",IndicatorTrend.PLUS,BigDecimal.valueOf(41047),"$","Household Value", BigDecimal.valueOf(59843), "$" ) );
		indicators.add(new DemographicIndicatorDTO( "higher-education",IndicatorTrend.PLUS,BigDecimal.valueOf(52,1),"$","Higher Education", BigDecimal.valueOf(55), "%" ) );
		*/

        usDemographicsDTO.setIndicators(indicators);
        return usDemographicsDTO;
    }


    private static class GenderSplineChartComparator implements Comparator<ArrayValueDTO> {

        @Override
        public int compare(ArrayValueDTO arg0, ArrayValueDTO arg1) {
            return arg0.getValue()[0].compareTo(arg1.getValue()[1]);
        }

    }

    private static class AgeGroupComparator implements Comparator<TalkingAboutMeEtlResultDTO> {

        private final List<String> ageGroup = ImmutableList.of("13-18", "19-24", "25-34", "35-44", "45-54", "55-64", "Over 65");
        private final Ordering<String> ageGroupOrdering = Ordering.explicit(ageGroup);

        @Override
        public int compare(TalkingAboutMeEtlResultDTO arg0, TalkingAboutMeEtlResultDTO arg1) {
            return ageGroupOrdering.compare(arg0.getDisplayName(), arg1.getDisplayName());
        }

    }

    private final BigDecimal TOP_VALUE_GENDER_CHART = BigDecimal.valueOf(1000L);

    private BigDecimal trimValueForGenderChart(BigDecimal value) {

        if (value.compareTo(TOP_VALUE_GENDER_CHART) > 0) {
            return TOP_VALUE_GENDER_CHART;
        } else {
            return value;
        }

    }

}
