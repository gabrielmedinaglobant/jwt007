package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class PostInterestDTO implements QuestionResultDTO {
	private String topic;
	private String subtopic;
	private BigDecimal value;
	private String unit;

	public PostInterestDTO() {
	}
	
	public PostInterestDTO(String topic, String subtopic, BigDecimal value, String unit) {
		this.topic = topic;
		this.subtopic = subtopic;
		this.value = value;
		this.unit = unit;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getSubtopic() {
		return subtopic;
	}

	public void setSubtopic(String subtopic) {
		this.subtopic = subtopic;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
}
