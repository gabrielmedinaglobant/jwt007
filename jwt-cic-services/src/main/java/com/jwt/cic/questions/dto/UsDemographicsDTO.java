package com.jwt.cic.questions.dto;

import java.util.List;

public class UsDemographicsDTO {

	private List<DemographicIndicatorDTO> indicators;

	public List<DemographicIndicatorDTO> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<DemographicIndicatorDTO> indicators) {
		this.indicators = indicators;
	}
	
	
}
