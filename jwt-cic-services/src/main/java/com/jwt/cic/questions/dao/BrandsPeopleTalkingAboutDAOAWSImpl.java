package com.jwt.cic.questions.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Primary
@Repository
public class BrandsPeopleTalkingAboutDAOAWSImpl implements BrandsPeopleTalkingAboutDAO{

	private final Logger log = LoggerFactory.getLogger(BrandsPeopleTalkingAboutDAOAWSImpl.class);
	
	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.brands-people-talking-about.json}")
	private String resourceLocation;
	
	@Autowired
	public BrandsPeopleTalkingAboutDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}
	
	@Override
	public BrandsPeopleTalkingAboutResultDTO get() {
		return resourceFetcher.fetchObject(bucketLocation+resourceLocation, BrandsPeopleTalkingAboutResultDTO.class);
	}

	@Override
	public BrandsPeopleTalkingAboutResultDTO get(String projectUuid) {
		return resourceFetcher.fetchObject(bucketLocation + projectUuid + "/output/" + resourceLocation, BrandsPeopleTalkingAboutResultDTO.class);
	}

}
