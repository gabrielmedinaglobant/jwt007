package com.jwt.cic.questions.service;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.CandleStickChartDTO;
import com.jwt.cic.charts.dto.CandleStickTrend;
import com.jwt.cic.charts.dto.LowHighCandleStickValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.VolumeChangedDayOverDayDAO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayDTO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayEtlResultDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;

@Service
public class VolumeChangedDayOverDayServiceImpl implements VolumeChangedDayOverDayService {

    private VolumeChangedDayOverDayDAO volumeChangedDayOverDayDAO;
    private ProjectDAO projectDAO;
    private QuestionResultContainerBuilder questionResultContainerBuilder;

    @Autowired
    public VolumeChangedDayOverDayServiceImpl(VolumeChangedDayOverDayDAO volumeChangedDayOverDayDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
        this.volumeChangedDayOverDayDAO = volumeChangedDayOverDayDAO;
        this.projectDAO = projectDAO;
        this.questionResultContainerBuilder = questionResultContainerBuilder;
    }

    @Override
    @Cacheable("question.volume-changed-day-over-day.0")
    public QuestionSingleResultContainerDTO get() {
        List<VolumeChangedDayOverDayEtlResultDTO> originDTOList = volumeChangedDayOverDayDAO.get();
        return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(originDTOList), buildDTO(originDTOList));
    }

    @Override
    @Cacheable("question.volume-changed-day-over-day.1")
    public QuestionSingleResultContainerDTO get(long projectId) {
        Project project = projectDAO.findOne(projectId);
        if (project == null) {
            throw new ProjectNotFoundException();
        }

        List<VolumeChangedDayOverDayEtlResultDTO> originDTOList = volumeChangedDayOverDayDAO.get(project.getUuid());
        VolumeChangedDayOverDayDTO responseDTO = buildDTO(originDTOList);
        return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(originDTOList), responseDTO);
    }

    protected final long getLastUpdatedDate(List<VolumeChangedDayOverDayEtlResultDTO> originDTOList){
    	return DateConverter.fromISODateToEpochSeconds(originDTOList.stream().findFirst().get().getLastUpdatedDate());
    }
    
    protected final VolumeChangedDayOverDayDTO buildDTO(List<VolumeChangedDayOverDayEtlResultDTO> originDTOList) {
        VolumeChangedDayOverDayDTO resultDTO = new VolumeChangedDayOverDayDTO();

        resultDTO.setSplineChart(buildSplineChart(originDTOList));
        resultDTO.setCandleStickChart(buildCandleStickChart(originDTOList));

        return resultDTO;
    }

    private CandleStickChartDTO buildCandleStickChart(List<VolumeChangedDayOverDayEtlResultDTO> originDTOList) {
        CandleStickChartDTO candleStickChartDTO = new CandleStickChartDTO();

        List<LowHighCandleStickValueDTO> line = originDTOList.stream()
                .map((item) -> {
                    LowHighCandleStickValueDTO candleStickValueDTO = new LowHighCandleStickValueDTO();

                    candleStickValueDTO.setLow(item.getAbsolutelow());
                    candleStickValueDTO.setRelativeLow(item.getRelativehigh());
                    candleStickValueDTO.setHigh(item.getAbsolutehigh());
                    candleStickValueDTO.setRelativeHigh(item.getRelativehigh());
                    candleStickValueDTO.setTrend(CandleStickTrend.getByValue(item.getTrend()));
                    candleStickValueDTO.setDateAsStringValue(item.getDate());
                    candleStickValueDTO.setDateValue(DateConverter.fromISODateToEpochSecondsAsBigDecimal(item.getDate()));
                    candleStickValueDTO.setValue(item.getValue());

                    return candleStickValueDTO;
                })
                .collect(Collectors.toList());

        candleStickChartDTO.setLine(line);

        return candleStickChartDTO;
    }

    @Deprecated
    private SplineChartDTO buildSplineChart(List<VolumeChangedDayOverDayEtlResultDTO> originDTOList) {
        SplineChartDTO splineChartDTO = new SplineChartDTO();
        List<SplineChartLineDTO> lines = new LinkedList<SplineChartLineDTO>();
        SplineChartLineDTO line = new SplineChartLineDTO();

        final AtomicInteger count = new AtomicInteger();
        line.setValues(
                originDTOList.stream()
                        .map(VolumeChangedDayOverDayServiceImpl::mapToBigDecimalArray)
                        .map((value) -> {
                            return new ArrayValueDTO("week " + count.incrementAndGet(), value);
                        })
                        .collect(Collectors.toList())
        );

        lines.add(line);
        splineChartDTO.setLines(lines);

        return splineChartDTO;
    }

    @Deprecated
    private static BigDecimal[] mapToBigDecimalArray(VolumeChangedDayOverDayEtlResultDTO jsonDTO) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime dateTime = LocalDateTime.parse(jsonDTO.getDate(), formatter);
        BigDecimal value = jsonDTO.getPostsWordNormalizedProm();
        BigDecimal[] arrayValue = new BigDecimal[]{BigDecimal.valueOf(dateTime.toEpochSecond(ZoneOffset.UTC)), value};

        return arrayValue;
    }


}
