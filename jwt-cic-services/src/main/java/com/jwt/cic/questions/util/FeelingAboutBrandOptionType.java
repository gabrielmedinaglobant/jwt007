package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FeelingAboutBrandOptionType {
	LIKE("like"), FEEL("feel"), DO("do");
	
	private String value;
	
	private static final Map<String, FeelingAboutBrandOptionType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private FeelingAboutBrandOptionType(String value){
		this.value = value;
	}
	
	private static Map<String, FeelingAboutBrandOptionType> initializeValueMap(){
		Map<String, FeelingAboutBrandOptionType> valueMap = new HashMap<>();
		for (FeelingAboutBrandOptionType ct : FeelingAboutBrandOptionType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static FeelingAboutBrandOptionType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, FeelingAboutBrandOptionType> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}
}
