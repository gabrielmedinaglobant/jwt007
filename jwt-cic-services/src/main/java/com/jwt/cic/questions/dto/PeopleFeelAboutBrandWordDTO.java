package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class PeopleFeelAboutBrandWordDTO {
	public String value;
	public BigDecimal feelingRank;
	public BigDecimal size;
	
	public PeopleFeelAboutBrandWordDTO(String value, BigDecimal feelingRank, BigDecimal size) {
		this.value = value;
		this.feelingRank = feelingRank;
		this.size = size;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public BigDecimal getFeelingRank() {
		return feelingRank;
	}
	public void setFeelingRank(BigDecimal feelingRank) {
		this.feelingRank = feelingRank;
	}
	public BigDecimal getSize() {
		return size;
	}
	public void setSize(BigDecimal size) {
		this.size = size;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((feelingRank == null) ? 0 : feelingRank.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeopleFeelAboutBrandWordDTO other = (PeopleFeelAboutBrandWordDTO) obj;
		if (feelingRank == null) {
			if (other.feelingRank != null)
				return false;
		} else if (!feelingRank.equals(other.feelingRank))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PeopleFeelAboutBrandWordDTO [" + (value != null ? "value=" + value + ", " : "")
				+ (feelingRank != null ? "feelingRank=" + feelingRank + ", " : "")
				+ (size != null ? "size=" + size : "") + "]";
	}
	
	
	
}
