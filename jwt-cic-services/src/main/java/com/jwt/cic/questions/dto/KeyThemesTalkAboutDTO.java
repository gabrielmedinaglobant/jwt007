package com.jwt.cic.questions.dto;

import java.util.List;

public class KeyThemesTalkAboutDTO {
	
	private List<ThemeChartsDTO> themes;

	public List<ThemeChartsDTO> getThemes() {
		return themes;
	}

	public void setThemes(List<ThemeChartsDTO> themes) {
		this.themes = themes;
	}
	
	
}
