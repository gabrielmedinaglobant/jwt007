package com.jwt.cic.questions.service;

import java.util.List;

import com.jwt.cic.questions.dto.KeyThemeTalkAboutDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.util.BlockType;

public interface KeyThemesTalkAboutService {
	QuestionResultListContainerDTO get();
	QuestionResultListContainerDTO get(long projectId);
}
