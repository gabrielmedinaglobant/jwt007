package com.jwt.cic.questions.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayDTO;
import com.jwt.cic.questions.service.VolumeChangedDayOverDayService;

@RestController
@RequestMapping("api/v1/questions/volume-changed-day-over-day")
public class VolumeChangedDayOverDayController {
	
	private VolumeChangedDayOverDayService volumeChangedDayOverDayService;
	
	@Autowired
	public VolumeChangedDayOverDayController(VolumeChangedDayOverDayService volumeChangedDayOverDayService) {
		this.volumeChangedDayOverDayService = volumeChangedDayOverDayService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public QuestionSingleResultContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){		
		if(projectId > 0){
			return volumeChangedDayOverDayService.get(projectId);
		}
		else{
			return volumeChangedDayOverDayService.get();
		}
	}
	
}
