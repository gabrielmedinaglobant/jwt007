package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;

public interface OtherTopicsCareAboutService {
	QuestionResultListContainerDTO getTopPostInterests();
	QuestionResultListContainerDTO getTopPostInterests(long projectId);
	QuestionResultListContainerDTO getTopSourceInterests();
	QuestionResultListContainerDTO getTopSourceInterests(long projectId);
}
