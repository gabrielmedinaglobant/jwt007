package com.jwt.cic.questions.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.questions.dto.InfoEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class PeopleMentionedCompetitorsDAOAWSImpl implements PeopleMentionedCompetitorsDAO {

	private ObjectMapper mapper = new ObjectMapper();

	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.people-mentioned-competitors.json}")
	private String resourceLocation;

	@Autowired
	public PeopleMentionedCompetitorsDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	@Deprecated
	public List<Map<String, String>> get() {
		String jsonString = resourceFetcher.fetch(bucketLocation + resourceLocation);
		List<Map<String, String>> nodeMaps = new ArrayList<>();

		try {
			JsonNode jsonNodeArray = mapper.readTree(jsonString);
			
			for(JsonNode jsonNodeObj : jsonNodeArray){
				Map<String, String> nodeMap = new HashMap<>();
				Streams.stream(jsonNodeObj.fields()).forEach((entry) -> {
					nodeMap.put(entry.getKey(), entry.getValue().asText());
				});
				nodeMaps.add(nodeMap);

			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidJsonFromDataSourceException();
		}

		return nodeMaps;

	}
	
	@Deprecated
	@Override
	public List<Map<String, String>> getDaily() {		
		return fetchData("daily", bucketLocation + resourceLocation);
	}
	
	@Deprecated
	@Override
	public List<Map<String, String>> getSummary() {
		return fetchData("summary", bucketLocation + resourceLocation);
	}

	@Deprecated
	@Override
	public InfoEtlResultDTO getInfo() {
		return resourceFetcher.fetchObject(bucketLocation + resourceLocation, PeopleMentionedCompetitorsEtlResult.class).getInfo();
	}
	
	@Override
	public List<Map<String, String>> getDaily(String projectUuid) {
		return fetchData("daily", bucketLocation + projectUuid + "/output/" + resourceLocation);
	}

	@Override
	public List<Map<String, String>> getSummary(String projectUuid) {
		return fetchData("summary", bucketLocation + projectUuid + "/output/" + resourceLocation);
	}
	
	@Override
	public InfoEtlResultDTO getInfo(String projectUuid) {
		return resourceFetcher.fetchObject(bucketLocation + projectUuid + "/output/" + resourceLocation, PeopleMentionedCompetitorsEtlResult.class).getInfo();
	}

	protected final List<Map<String, String>> fetchData(String attr, String location){
		String jsonString = resourceFetcher.fetch(location);
		List<Map<String, String>> nodeMaps = new ArrayList<>();
		
		try {
			JsonNode jsonNodeArray = mapper.readTree(jsonString).get(attr);
			
			for(JsonNode jsonNodeObj : jsonNodeArray){
				Map<String, String> nodeMap = new HashMap<>();
				Streams.stream(jsonNodeObj.fields()).forEach((entry) -> {
					nodeMap.put(entry.getKey(), entry.getValue().asText());
				});
				nodeMaps.add(nodeMap);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidJsonFromDataSourceException();
		}
		return nodeMaps;
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class PeopleMentionedCompetitorsEtlResult {
		private InfoEtlResultDTO info;

		public InfoEtlResultDTO getInfo() {
			return info;
		}

		public void setInfo(InfoEtlResultDTO info) {
			this.info = info;
		}
	}
}
