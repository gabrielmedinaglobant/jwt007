package com.jwt.cic.questions.gtrends.dto;

import java.util.List;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class CsvRequestDto {

    private String time;
    private String resolution = "DAY";
    private String locale = "en-US";
    private List<CsvComparisonItem> comparisonItem;
    private RequestOptions requestOptions;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<CsvComparisonItem> getComparisonItem() {
        return comparisonItem;
    }

    public void setComparisonItem(List<CsvComparisonItem> comparisonItem) {
        this.comparisonItem = comparisonItem;
    }

    public RequestOptions getRequestOptions() {
        return requestOptions;
    }

    public void setRequestOptions(RequestOptions requestOptions) {
        this.requestOptions = requestOptions;
    }
}
