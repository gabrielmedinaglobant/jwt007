package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum EmotionType {
	TRUST("trust"), ANTICIPATION("anticipation"), JOY("joy"), SURPRISE("surprise"), 
	FEAR("fear"), SADNESS("sadness"), ANGER("anger"), DISGUST("disgust");
	
	private String value;
	private static final Map<String, EmotionType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private EmotionType(String value) {
		this.value = value;
	}
	
	private static Map<String, EmotionType> initializeValueMap(){
		Map<String, EmotionType> valueMap = new HashMap<>();
		for (EmotionType ct : EmotionType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static EmotionType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, EmotionType> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}
	
}
