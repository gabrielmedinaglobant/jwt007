
package com.jwt.cic.questions.gtrends;

import org.springframework.stereotype.Component;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class GoogleTrendsResponseParser {

    private final String  lineSeparator ="\n";
    private final String csvSepartor=",";

    private static String cleanup(String content) {
        content = content.replace("Category: All categories", "");
        return content;
    }
    public  List<Map<String, String>> parse(String content) {
        content = cleanup(content);
        String[] csvLines = content.split(lineSeparator);
        List<String> header = null;
        List<Map<String, String>> nodeList = new ArrayList<Map<String, String>>();
        for (String line : csvLines) {
            if (line.trim().isEmpty()) {
                continue;
            }
            if (header == null) {
                header = Arrays.asList(line.split(csvSepartor)).stream().map(
                        token -> {
                            return token.replace(": (Worldwide)", "");
                        }
                ).collect(Collectors.toList());
                continue;
            }
            Map<String, String> nodeMap = new LinkedHashMap<>();
            String tokens[] = line.split(csvSepartor);
            for (int i = 0; i < header.size(); i++) {
                nodeMap.put(header.get(i), tokens[i]);
            }
            nodeList.add(nodeMap);
        }
        return nodeList;
    }
}
