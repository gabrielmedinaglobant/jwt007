package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.TalkingAboutMeDTO;
import com.jwt.cic.questions.service.TalkingAboutMeService;

@RestController
@RequestMapping("api/v1/questions/talking-about-me")
public class TalkingAboutMeController {
	
	private TalkingAboutMeService talkingAboutMeService;
	
	@Autowired
	private TalkingAboutMeController(TalkingAboutMeService talkingAboutMeService){
		this.talkingAboutMeService = talkingAboutMeService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public QuestionSingleResultContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0){
			return talkingAboutMeService.get(projectId);
		}
		else{
			return talkingAboutMeService.get();
		}
	}
	
}
