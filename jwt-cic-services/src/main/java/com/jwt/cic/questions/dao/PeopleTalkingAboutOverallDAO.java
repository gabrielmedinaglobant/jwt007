package com.jwt.cic.questions.dao;

import java.util.Map;

public interface PeopleTalkingAboutOverallDAO {
	Map<String, String> getAll();

	Map<String, String> getAll(String projectUuid);
}
