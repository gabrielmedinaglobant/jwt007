package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.jwt.cic.utils.DateConverter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.QuestionJsonDAO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.SentimentChangeDayOverDayDTO;
import com.jwt.cic.questions.dto.SentimentDateRangeDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.SentimentType;

@Service
public class SentimentChangeDayOverDayServiceImpl implements SentimentChangeDayOverDayService {

	private QuestionJsonDAO questionDAO;
	private ProjectDAO projectDAO;
	private QuestionResultContainerBuilder questionResultContainerBuilder;
	
	@Autowired
	public SentimentChangeDayOverDayServiceImpl(QuestionJsonDAO questionDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
		this.questionDAO = questionDAO;
		this.projectDAO = projectDAO;
		this.questionResultContainerBuilder = questionResultContainerBuilder;
	}

	@Override
	@Cacheable("question.sentiment-change-day-over-day.0")
	public QuestionSingleResultContainerDTO get(){
		JSONObject fetchedJSONObject = this.questionDAO.getSentimentChangeDayOverDay();
		return questionResultContainerBuilder.buildDTO(getLastUpdateDate(fetchedJSONObject), buildDTO(fetchedJSONObject));
	}
	
	@Override
	@Cacheable("question.sentiment-change-day-over-day.1")
	public QuestionSingleResultContainerDTO get(long projectId){
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		
		JSONObject fetchedJSONObject = this.questionDAO.getSentimentChangeDayOverDay(project.getUuid());

		SentimentChangeDayOverDayDTO responseDTO = buildDTO(fetchedJSONObject);
		responseDTO.setLastUpdatedDate( getLastUpdateDate(fetchedJSONObject) ); //TODO: This field is to be deprecated, needs to be cleaned up
		
		return questionResultContainerBuilder.buildDTO(getLastUpdateDate(fetchedJSONObject), responseDTO);
	}
	
	protected final long getLastUpdateDate(JSONObject fetchedJSONObject){
		JSONObject info = (JSONObject)fetchedJSONObject.get("info");
		return DateConverter.fromISODateToEpochSeconds( (String)info.get("last_updated_date") );
	}
	
	protected final SentimentChangeDayOverDayDTO buildDTO(JSONObject fetchedJSONObject){
		SentimentChangeDayOverDayDTO sentimentChangeDayOverDayDTO = new SentimentChangeDayOverDayDTO();
		SplineChartDTO splineChart = new SplineChartDTO();
		List<SplineChartLineDTO> lines = new LinkedList<SplineChartLineDTO>();
		
		lines.add(buildSplineChartLine("positive",fetchedJSONObject));
		lines.add(buildSplineChartLine("negative",fetchedJSONObject));
		lines.add(buildSplineChartLine("mixed",fetchedJSONObject));
		
		splineChart.setLines(lines);
		sentimentChangeDayOverDayDTO.setSplineChart( splineChart );
		
		sentimentChangeDayOverDayDTO.setSentimentDateRanges( buildSentimentDataRanges() );
		
		return sentimentChangeDayOverDayDTO;
	}
	
	@SuppressWarnings("unchecked")
	private SplineChartLineDTO buildSplineChartLine(String sentiment, JSONObject fetchedJSONObject){
		SplineChartLineDTO splineChartLineDTO = new SplineChartLineDTO();
		List<ArrayValueDTO> values = new LinkedList<ArrayValueDTO>();
		JSONArray jsonArray = (JSONArray)((JSONObject)fetchedJSONObject).get("values");
		
		jsonArray.forEach( (v) -> {
			
			JSONObject jsonObject = (JSONObject)v;
			String dateStrValue = (String)jsonObject.get("date");
			double doubleValue = (Double)jsonObject.get(sentiment + "_subject_sentences_prom");
			ArrayValueDTO value = new ArrayValueDTO();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
			LocalDateTime dateTime = LocalDateTime.parse(dateStrValue, formatter);

			value.setKey(dateStrValue);
			value.setValue(new BigDecimal[]{ BigDecimal.valueOf( dateTime.toEpochSecond(ZoneOffset.UTC) ),BigDecimal.valueOf(doubleValue) });
			
			values.add(value);
		} );
		
		splineChartLineDTO.setKey(sentiment);
		splineChartLineDTO.setValues(values);
		
		return splineChartLineDTO;
	}
	
	@Deprecated
	private List<SentimentDateRangeDTO> buildSentimentDataRanges(){
		List<SentimentDateRangeDTO> sentimentDateRangeDTOList = new ArrayList<>();
		
		//mocked implementations, this is actually being on FE
		sentimentDateRangeDTOList.add( new SentimentDateRangeDTO(SentimentType.POSITIVE, 1481500800, 1513382400) );
		sentimentDateRangeDTOList.add( new SentimentDateRangeDTO(SentimentType.NEGATIVE, 1513382400, 1483228800) );
		sentimentDateRangeDTOList.add( new SentimentDateRangeDTO(SentimentType.POSITIVE, 1483228800, 1513382400) );
		sentimentDateRangeDTOList.add( new SentimentDateRangeDTO(SentimentType.NEGATIVE, 1481500800, 1513382400) );
		sentimentDateRangeDTOList.add( new SentimentDateRangeDTO(SentimentType.POSITIVE, 1481500800, 1513382400) );
		
		return sentimentDateRangeDTOList;
	};
	
	public JSONObject getRaw() {
		// Should any further processing is needed, we can do it here
		return this.questionDAO.getSentimentChangeDayOverDay();
	}

}
