package com.jwt.cic.questions.dto;

public class TalkingAboutMeDTO implements QuestionResultDTO {
	private ImmigrationGenderTrendDTO immigrationGenderTrend;
	private LanguageDistributionDTO languageDistribution;
	private AgeDistributionDTO ageDistribution;
	private UsDemographicsDTO usDemographics;
	private long lastUpdatedDate;


	public ImmigrationGenderTrendDTO getImmigrationGenderTrend() {
		return immigrationGenderTrend;
	}
	public void setImmigrationGenderTrend(ImmigrationGenderTrendDTO immigrationGenderTrend) {
		this.immigrationGenderTrend = immigrationGenderTrend;
	}
	public LanguageDistributionDTO getLanguageDistribution() {
		return languageDistribution;
	}
	public void setLanguageDistribution(LanguageDistributionDTO languageDistribution) {
		this.languageDistribution = languageDistribution;
	}
	public AgeDistributionDTO getAgeDistribution() {
		return ageDistribution;
	}
	public void setAgeDistribution(AgeDistributionDTO ageDistribution) {
		this.ageDistribution = ageDistribution;
	}
	public UsDemographicsDTO getUsDemographics() {
		return usDemographics;
	}
	public void setUsDemographics(UsDemographicsDTO usDemographics) {
		this.usDemographics = usDemographics;
	}


	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
