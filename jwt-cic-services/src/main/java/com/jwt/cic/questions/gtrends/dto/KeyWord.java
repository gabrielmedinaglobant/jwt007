package com.jwt.cic.questions.gtrends.dto;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class KeyWord {
    String type = "BROAD";
    String value;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
