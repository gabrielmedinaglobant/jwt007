package com.jwt.cic.questions.util;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jwt.cic.questions.dto.QuestionResultContainerDTO;
import com.jwt.cic.questions.dto.QuestionResultDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

/***
 * 
 * Builder of QuestionResultContainerDTO
 * 
 * @author gabriel.medina
 *
 */
@Component
public class QuestionResultContainerBuilder {

	public QuestionResultListContainerDTO buildDTO(long dateFromEpoch, long dateToEpoch, long lastUpdatedDateEpoch,
			List<? extends QuestionResultDTO> questionResultDTOList) {
		
		QuestionResultListContainerDTO dto = new QuestionResultListContainerDTO();

		dto.setDateFrom(dateFromEpoch);
		dto.setDateTo(dateToEpoch);
		dto.setLastUpdatedDate(lastUpdatedDateEpoch);
		dto.setValue(questionResultDTOList);

		return dto;
	}

	public QuestionResultListContainerDTO buildDTO(long lastUpdatedDateEpoch,
			List<? extends QuestionResultDTO> questionResultDTOList) {
		
		QuestionResultListContainerDTO dto = new QuestionResultListContainerDTO();

		dto.setLastUpdatedDate(lastUpdatedDateEpoch);
		dto.setValue(questionResultDTOList);

		return dto;
	}

	public QuestionSingleResultContainerDTO buildDTO(long dateFromEpoch, long dateToEpoch, long lastUpdatedDateEpoch,
			QuestionResultDTO questionResultDTO) {
		
		QuestionSingleResultContainerDTO dto = new QuestionSingleResultContainerDTO();

		dto.setDateFrom(dateFromEpoch);
		dto.setDateTo(dateToEpoch);
		dto.setLastUpdatedDate(lastUpdatedDateEpoch);
		dto.setValue(questionResultDTO);

		return dto;
	}

	public QuestionSingleResultContainerDTO buildDTO(long lastUpdatedDateEpoch,
			QuestionResultDTO questionResultDTO) {
		
		QuestionSingleResultContainerDTO dto = new QuestionSingleResultContainerDTO();

		dto.setLastUpdatedDate(lastUpdatedDateEpoch);
		dto.setValue(questionResultDTO);

		return dto;
	}
}
