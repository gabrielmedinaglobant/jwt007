package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.charts.dto.SplineChartDTO;

public class BrandsPeopleTalkingAboutDTO implements QuestionResultDTO {
	
	private List<BrandIndicatorDTO> brands;
	private SplineChartDTO sentimentOvertimeSplineChart;
	private long lastUpdatedDate;
	
	public List<BrandIndicatorDTO> getBrands() {
		return brands;
	}
	public void setBrands(List<BrandIndicatorDTO> brands) {
		this.brands = brands;
	}
	
	public SplineChartDTO getSentimentOvertimeSplineChart() {
		return sentimentOvertimeSplineChart;
	}
	public void setSentimentOvertimeSplineChart(SplineChartDTO sentimentOvertimeSplineChart) {
		this.sentimentOvertimeSplineChart = sentimentOvertimeSplineChart;
	}

	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
