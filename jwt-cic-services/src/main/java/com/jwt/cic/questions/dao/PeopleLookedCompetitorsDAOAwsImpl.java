package com.jwt.cic.questions.dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.utils.ResourceFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;

/**
 * Created by vishal.domale on 24-04-2017.
 */

@Repository
public class PeopleLookedCompetitorsDAOAwsImpl implements PeopleLookedCompetitorsDAO {


    private ObjectMapper mapper = new ObjectMapper();

    private ResourceFetcher resourceFetcher;
    @Value("${question.s3.bucket}")
    private String bucketLocation;
    @Value("${question.s3.bucket.people-looked-competitors.json}")
    private String resourceLocation;

    @Autowired
    public PeopleLookedCompetitorsDAOAwsImpl(ResourceFetcher resourceFetcher) {
        this.resourceFetcher = resourceFetcher;
    }

    @Override
    public List<Map<String, String>> get(String[] brands) {
        String jsonString = resourceFetcher.fetch(bucketLocation + resourceLocation);
        List<Map<String, String>> nodeList = new ArrayList<Map<String, String>>();

        try {
            JsonNode jsonNode = mapper.readTree(jsonString);
            for (JsonNode jsonObj : jsonNode) {
                Map<String, String> nodeMap = new LinkedHashMap<>();
                Streams.stream(jsonObj.fields()).forEach((brandEntry) -> {
                    nodeMap.put(brandEntry.getKey(), brandEntry.getValue().asText());

                });
                nodeList.add(nodeMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new InvalidJsonFromDataSourceException();
        }
        return nodeList;
    }


}
