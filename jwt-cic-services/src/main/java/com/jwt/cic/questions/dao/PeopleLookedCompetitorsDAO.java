package com.jwt.cic.questions.dao;

import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Map;

/**
 * Created by vishal.domale on 24-04-2017.
 */
public interface PeopleLookedCompetitorsDAO {
    public   List<Map<String,String>>  get(String[] brands);

}
