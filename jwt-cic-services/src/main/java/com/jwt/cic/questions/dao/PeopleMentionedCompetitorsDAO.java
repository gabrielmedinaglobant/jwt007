package com.jwt.cic.questions.dao;

import java.util.List;
import java.util.Map;

import com.jwt.cic.questions.dto.InfoEtlResultDTO;

public interface PeopleMentionedCompetitorsDAO {
	@Deprecated
	List<Map<String, String>> get();
	List<Map<String, String>> getDaily();
	List<Map<String, String>> getDaily(String projectUuid);
	List<Map<String, String>> getSummary();
	List<Map<String, String>> getSummary(String projectUuid);
	InfoEtlResultDTO getInfo();
	InfoEtlResultDTO getInfo(String projectUuid);
}
