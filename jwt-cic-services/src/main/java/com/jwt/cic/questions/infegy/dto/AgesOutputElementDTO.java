package com.jwt.cic.questions.infegy.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgesOutputElementDTO implements OutputElementDTO {
	private BigDecimal mean;
	private BigDecimal median;
	private List<AgeRangeDataElementDTO> ranges;

	public BigDecimal getMean() {
		return mean;
	}

	public void setMean(BigDecimal mean) {
		this.mean = mean;
	}

	public BigDecimal getMedian() {
		return median;
	}

	public void setMedian(BigDecimal median) {
		this.median = median;
	}

	public List<AgeRangeDataElementDTO> getRanges() {
		return ranges;
	}

	public void setRanges(List<AgeRangeDataElementDTO> ranges) {
		this.ranges = ranges;
	}

	public static class AgeRangeDataElementDTO {
		private BigDecimal count;
		@JsonProperty("db_distribution")
		private BigDecimal dbDistribution;
		@JsonProperty("display_name")
		private String displayName;
		private String id;
		private BigDecimal probability;
		private RangeDTO range;
		@JsonProperty("vs_expected")
		private String vsExpected;

		public BigDecimal getCount() {
			return count;
		}

		public void setCount(BigDecimal count) {
			this.count = count;
		}

		public BigDecimal getDbDistribution() {
			return dbDistribution;
		}

		public void setDbDistribution(BigDecimal dbDistribution) {
			this.dbDistribution = dbDistribution;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public BigDecimal getProbability() {
			return probability;
		}

		public void setProbability(BigDecimal probability) {
			this.probability = probability;
		}

		public RangeDTO getRange() {
			return range;
		}

		public void setRange(RangeDTO range) {
			this.range = range;
		}

		public String getVsExpected() {
			return vsExpected;
		}

		public void setVsExpected(String vsExpected) {
			this.vsExpected = vsExpected;
		}

	}

	private static class RangeDTO {
		private BigDecimal start;
		private BigDecimal end;

		public BigDecimal getStart() {
			return start;
		}

		public void setStart(BigDecimal start) {
			this.start = start;
		}

		public BigDecimal getEnd() {
			return end;
		}

		public void setEnd(BigDecimal end) {
			this.end = end;
		}
	}
}
