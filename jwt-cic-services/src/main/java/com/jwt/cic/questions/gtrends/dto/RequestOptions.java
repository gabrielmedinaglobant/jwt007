package com.jwt.cic.questions.gtrends.dto;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class RequestOptions {

   private String property = "";
   private  String backend = "IZG";
   private  int category = 0;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getBackend() {
        return backend;
    }

    public void setBackend(String backend) {
        this.backend = backend;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
