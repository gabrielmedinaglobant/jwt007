package com.jwt.cic.questions.calculations;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.jwt.cic.queries.domain.QueryTypePerProjectViewElement;

public class PeopleMentionedCompetitorsCalculationResult {
	private Collection<QueryTypePerProjectViewElement> queryTypePerProjectElements;
	// get a map of query-type-keys and brands
	private LinkedHashMap<String, String> queryTypeKeysBrandsMap;
	// get posts universe and date for each brand
	private Map<String, List<Pair<BigDecimal, String>>> postsUniversesAndDates;
	//averages, date_max, date_min, highest, lowest, total for each brand/project
	private Map<String, BigDecimal> averages;
	private Map<String, String> datesMax;
	private Map<String, String> datesMin;
	private Map<String, BigDecimal> highests;
	private Map<String, BigDecimal> lowests;
	private Map<String, BigDecimal> totals;
	
	public Collection<QueryTypePerProjectViewElement> getQueryTypePerProjectElements() {
		return queryTypePerProjectElements;
	}
	public void setQueryTypePerProjectElements(Collection<QueryTypePerProjectViewElement> queryTypePerProjectElements) {
		this.queryTypePerProjectElements = queryTypePerProjectElements;
	}
	public LinkedHashMap<String, String> getQueryTypeKeysBrandsMap() {
		return queryTypeKeysBrandsMap;
	}
	public void setQueryTypeKeysBrandsMap(LinkedHashMap<String, String> queryTypeKeysBrandsMap) {
		this.queryTypeKeysBrandsMap = queryTypeKeysBrandsMap;
	}
	public Map<String, List<Pair<BigDecimal, String>>> getPostsUniversesAndDates() {
		return postsUniversesAndDates;
	}
	public void setPostsUniversesAndDates(Map<String, List<Pair<BigDecimal, String>>> postsUniversesAndDates) {
		this.postsUniversesAndDates = postsUniversesAndDates;
	}
	public Map<String, BigDecimal> getAverages() {
		return averages;
	}
	public void setAverages(Map<String, BigDecimal> averages) {
		this.averages = averages;
	}
	public Map<String, String> getDatesMax() {
		return datesMax;
	}
	public void setDatesMax(Map<String, String> datesMax) {
		this.datesMax = datesMax;
	}
	public Map<String, String> getDatesMin() {
		return datesMin;
	}
	public void setDatesMin(Map<String, String> datesMin) {
		this.datesMin = datesMin;
	}
	public Map<String, BigDecimal> getHighests() {
		return highests;
	}
	public void setHighests(Map<String, BigDecimal> highests) {
		this.highests = highests;
	}
	public Map<String, BigDecimal> getLowests() {
		return lowests;
	}
	public void setLowests(Map<String, BigDecimal> lowests) {
		this.lowests = lowests;
	}
	public Map<String, BigDecimal> getTotals() {
		return totals;
	}
	public void setTotals(Map<String, BigDecimal> totals) {
		this.totals = totals;
	}
	
	
}
