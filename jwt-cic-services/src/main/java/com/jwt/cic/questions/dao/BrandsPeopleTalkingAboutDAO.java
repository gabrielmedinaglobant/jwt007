package com.jwt.cic.questions.dao;

import java.util.List;

import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO;

public interface BrandsPeopleTalkingAboutDAO {
	public BrandsPeopleTalkingAboutResultDTO get();
	public BrandsPeopleTalkingAboutResultDTO get(String projectUuid);
}
