package com.jwt.cic.questions.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.VolumeChangedDayOverDayEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Primary
@Repository
public class VolumeChangedDayOverDayDAOAWSImpl implements VolumeChangedDayOverDayDAO {

	private final Logger log = LoggerFactory.getLogger(VolumeChangedDayOverDayDAOAWSImpl.class);
	
	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.volume-change-day-over-day.json}")
	private String resourceLocation;
	
	@Autowired
	public VolumeChangedDayOverDayDAOAWSImpl(ResourceFetcher resourceFetcher) {
		super();
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	public List<VolumeChangedDayOverDayEtlResultDTO> get() {
		return resourceFetcher.fetchObjects(bucketLocation+resourceLocation, VolumeChangedDayOverDayEtlResultDTO.class);
	}

	@Override
	public List<VolumeChangedDayOverDayEtlResultDTO> get(String projectUuid) {
		return resourceFetcher.fetchObjects(bucketLocation + projectUuid + "/output/" + resourceLocation, VolumeChangedDayOverDayEtlResultDTO.class);
	}

}
