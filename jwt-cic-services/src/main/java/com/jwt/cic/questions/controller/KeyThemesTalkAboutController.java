package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.service.KeyThemesTalkAboutService;

@RestController
@RequestMapping("api/v1/questions/key-themes-talk-about")
public class KeyThemesTalkAboutController {

	private KeyThemesTalkAboutService keyThemesTalkAboutService;
	
	@Autowired
	public KeyThemesTalkAboutController(KeyThemesTalkAboutService keyThemesTalkAboutService) {
		this.keyThemesTalkAboutService = keyThemesTalkAboutService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	private QuestionResultListContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){
		if(projectId > 0){
			return keyThemesTalkAboutService.get(projectId);
		}
		else{
			return keyThemesTalkAboutService.get();
		}
	} 
	
}
