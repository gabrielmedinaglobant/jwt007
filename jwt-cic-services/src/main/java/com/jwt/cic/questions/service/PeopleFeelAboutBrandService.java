package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;

/**
 * Created by vishal.domale on 10-04-2017.
 */
public interface PeopleFeelAboutBrandService {
	QuestionResultListContainerDTO get();
	QuestionResultListContainerDTO get(long projectId);
}
