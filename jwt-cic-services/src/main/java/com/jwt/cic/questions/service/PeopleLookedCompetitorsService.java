package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

/**
 * Created by vishal.domale on 24-04-2017.
 */
public interface PeopleLookedCompetitorsService {

    public QuestionSingleResultContainerDTO get(String[] brands);

}

