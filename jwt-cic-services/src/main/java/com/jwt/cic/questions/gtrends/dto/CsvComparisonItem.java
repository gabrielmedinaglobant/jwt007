package com.jwt.cic.questions.gtrends.dto;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class CsvComparisonItem {

    String geo="{}";
    ComplexKeywordsRestriction complexKeywordsRestriction;

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public ComplexKeywordsRestriction getComplexKeywordsRestriction() {
        return complexKeywordsRestriction;
    }

    public void setComplexKeywordsRestriction(ComplexKeywordsRestriction complexKeywordsRestriction) {
        this.complexKeywordsRestriction = complexKeywordsRestriction;
    }
}
