package com.jwt.cic.questions.infegy.dao;

import com.jwt.cic.questions.infegy.dto.SentimentDTO;

public interface SentimentDAO {
	SentimentDTO get(String projectUuid, String queryTypeValue);
}
