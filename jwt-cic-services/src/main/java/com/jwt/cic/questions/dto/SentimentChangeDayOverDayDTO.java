package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.charts.dto.SplineChartDTO;

public class SentimentChangeDayOverDayDTO implements QuestionResultDTO {
	private SplineChartDTO splineChart;
	private List<SentimentDateRangeDTO> sentimentDateRanges;
	private long lastUpdatedDate;


	public SplineChartDTO getSplineChart() {
		return splineChart;
	}

	public void setSplineChart(SplineChartDTO splineChart) {
		this.splineChart = splineChart;
	}

	public List<SentimentDateRangeDTO> getSentimentDateRanges() {
		return sentimentDateRanges;
	}

	public void setSentimentDateRanges(List<SentimentDateRangeDTO> sentimentDateRanges) {
		this.sentimentDateRanges = sentimentDateRanges;
	}

	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
