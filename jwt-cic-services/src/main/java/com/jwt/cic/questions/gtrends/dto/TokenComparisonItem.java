package com.jwt.cic.questions.gtrends.dto;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class TokenComparisonItem {
  private  String keyword;
  private   String geo = "";
  private   String time;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }
}
