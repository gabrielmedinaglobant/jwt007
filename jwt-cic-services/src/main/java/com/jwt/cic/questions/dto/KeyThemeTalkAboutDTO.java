package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.jwt.cic.charts.dto.PieChartDTO;

public class KeyThemeTalkAboutDTO implements QuestionResultDTO {
	private String name;
	private BigDecimal distributionIndicatorValue;
	private BigDecimal positivityIndicatorValue;
	private BigDecimal negativityIndicatorValue;
	private String distributionIndicatorUnit;
	private String positivityIndicatorUnit;
	private String negativityIndicatorUnit;
	private PieChartDTO pieChart;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getDistributionIndicatorValue() {
		return distributionIndicatorValue;
	}
	public void setDistributionIndicatorValue(BigDecimal distributionIndicatorValue) {
		this.distributionIndicatorValue = distributionIndicatorValue;
	}
	public BigDecimal getPositivityIndicatorValue() {
		return positivityIndicatorValue;
	}
	public void setPositivityIndicatorValue(BigDecimal positivityIndicatorValue) {
		this.positivityIndicatorValue = positivityIndicatorValue;
	}
	public BigDecimal getNegativityIndicatorValue() {
		return negativityIndicatorValue;
	}
	public void setNegativityIndicatorValue(BigDecimal negativityIndicatorValue) {
		this.negativityIndicatorValue = negativityIndicatorValue;
	}
	public String getDistributionIndicatorUnit() {
		return distributionIndicatorUnit;
	}
	public void setDistributionIndicatorUnit(String distributionIndicatorUnit) {
		this.distributionIndicatorUnit = distributionIndicatorUnit;
	}
	public String getPositivityIndicatorUnit() {
		return positivityIndicatorUnit;
	}
	public void setPositivityIndicatorUnit(String positivityIndicatorUnit) {
		this.positivityIndicatorUnit = positivityIndicatorUnit;
	}
	public String getNegativityIndicatorUnit() {
		return negativityIndicatorUnit;
	}
	public void setNegativityIndicatorUnit(String negativityIndicatorUnit) {
		this.negativityIndicatorUnit = negativityIndicatorUnit;
	}
	public PieChartDTO getPieChart() {
		return pieChart;
	}
	public void setPieChart(PieChartDTO pieChart) {
		this.pieChart = pieChart;
	}	

}
