package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vishal.domale on 11-04-2017.
 */
public enum ConversationHappeningType {
    FORUMS("forums", "Forums"), MICROBLOGS("microblogs", "Microblogs"), IMAGES("images", "Images"), BLOGS("blogs", "Blogs"),UNCATEGORIZED("uncategorized","Uncategorized");

    private String displayName;
    private String value;

    private static Map<String, ConversationHappeningType> valueMap = Collections.unmodifiableMap(initializeValueMap());

    private ConversationHappeningType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }


    private static Map<String, ConversationHappeningType> initializeValueMap() {
        Map<String, ConversationHappeningType> valueMap = new HashMap<>();

        for (ConversationHappeningType cht : ConversationHappeningType.values()) {
            valueMap.put(cht.value, cht);
        }
        return valueMap;
    }

    public static ConversationHappeningType getByValue(String value) {
         if(!valueMap.containsKey(value)){
             throw new IllegalArgumentException();
         }
         return valueMap.get(value);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
