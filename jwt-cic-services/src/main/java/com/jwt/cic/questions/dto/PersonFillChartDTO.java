package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class PersonFillChartDTO {
	private BigDecimal value;

	public PersonFillChartDTO() {
	}
	
	public PersonFillChartDTO(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	
}
