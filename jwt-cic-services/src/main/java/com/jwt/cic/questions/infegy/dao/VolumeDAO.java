package com.jwt.cic.questions.infegy.dao;

import com.jwt.cic.questions.infegy.dto.VolumeDTO;

public interface VolumeDAO {
	VolumeDTO get(String projectUuid, String queryTypeValue);
}
