package com.jwt.cic.questions.dao;

import java.util.List;

import com.jwt.cic.questions.dto.VolumeChangedDayOverDayDTO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayEtlResultDTO;

public interface VolumeChangedDayOverDayDAO {
	List<VolumeChangedDayOverDayEtlResultDTO> get();
	List<VolumeChangedDayOverDayEtlResultDTO> get(String projectUuid);
}
