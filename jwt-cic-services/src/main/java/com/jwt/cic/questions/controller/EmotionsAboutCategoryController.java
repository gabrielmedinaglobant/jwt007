package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.service.EmotionsAboutCategoryService;

/**
 * 
 * Controller for Question "What emotions do people have when talking about the category"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/emotions-about-category")
public class EmotionsAboutCategoryController {
	
	private EmotionsAboutCategoryService emotionsAboutCategoryService;
	
	@Autowired
	public EmotionsAboutCategoryController(EmotionsAboutCategoryService emotionsAboutCategoryService) {
		this.emotionsAboutCategoryService = emotionsAboutCategoryService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	private QuestionResultListContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId){		
		if(projectId > 0){
			return emotionsAboutCategoryService.get(projectId);
		}
		else{
			return emotionsAboutCategoryService.get();
		}
	}
}
