package com.jwt.cic.questions.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/***
 * 
 * The purpose of this data object is to transport list results with fetching data details
 * 
 * @author gabriel.medina
 *
 */
public class QuestionResultListContainerDTO implements QuestionResultContainerDTO< List<? extends QuestionResultDTO> > {
	
	private List<? extends QuestionResultDTO> value;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long lastUpdatedDate;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long dateFrom;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long dateTo;
	
	public List<? extends QuestionResultDTO> getValue() {
		return value;
	}
	public void setValue(List<? extends QuestionResultDTO> value) {
		this.value = value;
	}
	public Long getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public Long getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Long dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Long getDateTo() {
		return dateTo;
	}
	public void setDateTo(Long dateTo) {
		this.dateTo = dateTo;
	}
	
}
