package com.jwt.cic.questions.service;

import com.jwt.cic.charts.dto.PieChartDTO;
import com.jwt.cic.charts.dto.SingleValueDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.ConversationHappeningDAO;
import com.jwt.cic.questions.dto.ConversationHappeningDTO;
import com.jwt.cic.questions.dto.ConversationHappeningEtlResultDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.util.ConversationHappeningType;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service implementation of ConversationHappeningService
 * 
 * @author vishal.domale 
 * 11-04-2017
 * 
 */

@Service
public class ConversationHappeningServiceImpl implements ConversationHappeningService {

    private ConversationHappeningDAO conversationHappeningDAO;
    private ProjectDAO projectDAO;
    private QuestionResultContainerBuilder questionResultContainerBuilder;
    
    @Autowired
    public ConversationHappeningServiceImpl(ConversationHappeningDAO conversationHappeningDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
        this.conversationHappeningDAO = conversationHappeningDAO;
        this.projectDAO = projectDAO;
        this.questionResultContainerBuilder = questionResultContainerBuilder;
    }

    @Override
    @Cacheable("question.conversation-happening.0")
    public QuestionSingleResultContainerDTO get() {
    	ConversationHappeningEtlResultDTO eltResult = conversationHappeningDAO.get();
        return questionResultContainerBuilder.buildDTO(
        		DateConverter.fromISODateToEpochSecondsTrimmed(eltResult.getInfo().getDateFrom()), 
        		DateConverter.fromISODateToEpochSecondsTrimmed(eltResult.getInfo().getDateTo()), 
        		DateConverter.fromISODateToEpochSeconds(eltResult.getInfo().getLastUpdatedDate()),
        		buildResponseDTO(eltResult));
    }

    @Override
    @Cacheable("question.conversation-happening.1")
	public QuestionSingleResultContainerDTO get(long projectId) {
    	Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		ConversationHappeningEtlResultDTO eltResult = conversationHappeningDAO.get(project.getUuid());

        ConversationHappeningDTO responseDTO = buildResponseDTO(eltResult);
        
        return questionResultContainerBuilder.buildDTO(
        		DateConverter.fromISODateToEpochSecondsTrimmed(eltResult.getInfo().getDateFrom()), 
        		DateConverter.fromISODateToEpochSecondsTrimmed(eltResult.getInfo().getDateTo()), 
        		DateConverter.fromISODateToEpochSeconds(eltResult.getInfo().getLastUpdatedDate()), 
        		responseDTO);
    }

    protected final ConversationHappeningDTO buildResponseDTO(ConversationHappeningEtlResultDTO eltResult){
    	ConversationHappeningDTO conversationHappeningDTO = new ConversationHappeningDTO();
    	conversationHappeningDTO.setPieChart(buildPieChartDTO(eltResult));
        return conversationHappeningDTO;
    }
    
    private PieChartDTO buildPieChartDTO(ConversationHappeningEtlResultDTO eltResult) {
        PieChartDTO pieChartDTO = new PieChartDTO();
        pieChartDTO.setValues(getPieChartValues(eltResult));
        return pieChartDTO;
    }


    private List<SingleValueDTO> getPieChartValues(ConversationHappeningEtlResultDTO eltResult) {

        BigDecimal sum = calculateSum(eltResult);
        
        List<SingleValueDTO> values = eltResult.getValues()
        		.stream()
        		.filter( 
    				(value) -> (value.getType().compareToIgnoreCase(ConversationHappeningType.UNCATEGORIZED.getValue()) != 0)
				)
        		.map( value -> {
        			SingleValueDTO singleValueDTO = new SingleValueDTO();
        			singleValueDTO.setKey( value.getType() );
        			singleValueDTO.setValue(sum.equals(BigDecimal.ZERO) ? BigDecimal.ZERO : value.getValue().divide(sum,10,RoundingMode.HALF_UP));
        			singleValueDTO.setDisplayName( StringUtils.capitalize(value.getType()) );
        			singleValueDTO.setIndicatorValue( value.getValue() );
                    singleValueDTO.setIndicatorUnit( UnitType.NONE.getValue() );
                    return singleValueDTO;
        		} )
        		.collect(Collectors.toList());
        		
        return values;
    }

    private BigDecimal calculateSum(ConversationHappeningEtlResultDTO eltResult) {
    	
    	return eltResult.getValues()
    			.stream()
    			.map( (value) -> value.getValue() )
    			.reduce( BigDecimal.ZERO , (a,b) -> a.add(b));

    }

	

}

