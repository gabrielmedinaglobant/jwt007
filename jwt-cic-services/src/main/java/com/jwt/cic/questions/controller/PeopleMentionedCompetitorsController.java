package com.jwt.cic.questions.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.jwt.cic.clients.dao.ProjectDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.questions.dto.BrandIndicatorDTO;
import com.jwt.cic.questions.dto.DisplayableDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsSummaryDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsSummaryRowDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.service.PeopleMentionedCompetitorsService;

/**
 * 
 * Controller for Q1/23 - Discover - Question "How much have people mentioned me
 * in comparison to competitors?"
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/questions/people-mentioned-competitors")
public class PeopleMentionedCompetitorsController {

	private PeopleMentionedCompetitorsService peopleMentionedCompetitorsService;


	@Autowired
	public PeopleMentionedCompetitorsController(PeopleMentionedCompetitorsService peopleMentionedCompetitorsService) {
		this.peopleMentionedCompetitorsService = peopleMentionedCompetitorsService;

	}

	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public QuestionSingleResultContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId) {
		// return buildDTO();
		if(projectId > 0) {
			return peopleMentionedCompetitorsService.get(projectId);
		}else{
			return peopleMentionedCompetitorsService.get();
		}
	}

	private PeopleMentionedCompetitorsDTO buildDTO() {
		PeopleMentionedCompetitorsDTO dto = new PeopleMentionedCompetitorsDTO();

		dto.setBrands(buildBrands());
		dto.setSplineChart(buildSplineChart());
		dto.setSummary(buildSummary());

		return dto;
	}

	private PeopleMentionedCompetitorsSummaryDTO buildSummary() {
		PeopleMentionedCompetitorsSummaryDTO dto = new PeopleMentionedCompetitorsSummaryDTO();

		dto.setRows(buildSummaryRows());

		return dto;
	}

	private List<PeopleMentionedCompetitorsSummaryRowDTO> buildSummaryRows() {
		List<PeopleMentionedCompetitorsSummaryRowDTO> listDto = new ArrayList<>();

		listDto.add(buildMeSummaryRow());
		listDto.add(buildMarsSummaryRow());
		listDto.add(buildHersheysSummaryRow());
		listDto.add(buildCadburySummaryRow());
		listDto.add(buildTurinSummaryRow());

		return listDto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildMeSummaryRow() {
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();

		dto.setBrand("My brand");
		dto.setBrandKey("me");
		
		dto.setTotal(BigDecimal.valueOf(2898057L));
		dto.setLowest(BigDecimal.valueOf(55908L));

		dto.setAverage(BigDecimal.valueOf(90594L));

		dto.setHighest(BigDecimal.valueOf(147436L));

		dto.setChangeUnit("%");
		dto.setChangeValue(BigDecimal.valueOf(-8.2));

		dto.setTotalPercentage(BigDecimal.valueOf(90L));

		return dto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildTurinSummaryRow() {
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();

		dto.setBrand("Mars");
		dto.setBrandKey("mars");
		
		dto.setTotal(BigDecimal.valueOf(2898057L));
		dto.setLowest(BigDecimal.valueOf(55908L));

		dto.setAverage(BigDecimal.valueOf(90594L));

		dto.setHighest(BigDecimal.valueOf(147436L));

		dto.setChangeUnit("%");
		dto.setChangeValue(BigDecimal.valueOf(-8.2));

		dto.setTotalPercentage(BigDecimal.valueOf(80L));

		return dto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildCadburySummaryRow() {
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();

		dto.setBrand("Hershey's");
		dto.setBrandKey("hersheys");
		
		dto.setTotal(BigDecimal.valueOf(2898057L));
		dto.setLowest(BigDecimal.valueOf(55908L));

		dto.setAverage(BigDecimal.valueOf(90594L));

		dto.setHighest(BigDecimal.valueOf(147436L));

		dto.setChangeUnit("%");
		dto.setChangeValue(BigDecimal.valueOf(-8.2));

		dto.setTotalPercentage(BigDecimal.valueOf(70L));

		return dto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildHersheysSummaryRow() {
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();

		dto.setBrand("Cadbury");
		dto.setBrandKey("cadbury");
		
		dto.setTotal(BigDecimal.valueOf(2898057L));
		dto.setLowest(BigDecimal.valueOf(55908L));

		dto.setAverage(BigDecimal.valueOf(90594L));

		dto.setHighest(BigDecimal.valueOf(147436L));

		dto.setChangeUnit("%");
		dto.setChangeValue(BigDecimal.valueOf(-8.2));

		dto.setTotalPercentage(BigDecimal.valueOf(60L));

		return dto;
	}

	private PeopleMentionedCompetitorsSummaryRowDTO buildMarsSummaryRow() {
		PeopleMentionedCompetitorsSummaryRowDTO dto = new PeopleMentionedCompetitorsSummaryRowDTO();

		dto.setBrand("Turin");
		dto.setBrandKey("turin");
		
		dto.setTotal(BigDecimal.valueOf(2898057L));
		dto.setLowest(BigDecimal.valueOf(55908L));

		dto.setAverage(BigDecimal.valueOf(90594L));

		dto.setHighest(BigDecimal.valueOf(147436L));

		dto.setChangeUnit("%");
		dto.setChangeValue(BigDecimal.valueOf(-8.2));

		dto.setTotalPercentage(BigDecimal.valueOf(50L));

		return dto;
	}

	private SplineChartDTO buildSplineChart() {
		SplineChartDTO dto = new SplineChartDTO();

		dto.setLines(buildSplineChartLines());

		return dto;
	}

	private List<SplineChartLineDTO> buildSplineChartLines() {
		List<SplineChartLineDTO> dtoList = new ArrayList<>();

		dtoList.add(new SplineChartLineDTO("me", buildMeValues()));
		dtoList.add(new SplineChartLineDTO("mars", buildMarsValues()));
		dtoList.add(new SplineChartLineDTO("hersheys", buildHersheysValues()));
		dtoList.add(new SplineChartLineDTO("cadbury", buildCadburyValues()));
		dtoList.add(new SplineChartLineDTO("turin", buildTurinValues()));

		return dtoList;
	}

	private List<ArrayValueDTO> buildMeValues() {
		List<ArrayValueDTO> dtoList = new ArrayList<>();

		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491004800), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 01
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(26111) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491091200), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 02
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(36111) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491177600), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 03
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(46111) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491264000), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 04
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(56111) }));

		return dtoList;
	}

	private List<ArrayValueDTO> buildMarsValues() {
		List<ArrayValueDTO> dtoList = new ArrayList<>();

		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491004800), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 01
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(20000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491091200), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 02
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(30000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491177600), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 03
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(40000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491264000), // unix
																							// epoch
																							// time
																							// that
																							// correponds
																							// to
																							// 04
																							// Apr
																							// 2017
																							// 00:00:00
																							// GMT
				BigDecimal.valueOf(50000) }));

		return dtoList;
	}

	private List<ArrayValueDTO> buildHersheysValues() {
		List<ArrayValueDTO> dtoList = new ArrayList<>();

		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491004800), BigDecimal.valueOf(20000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491091200), BigDecimal.valueOf(30000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491177600), BigDecimal.valueOf(40000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491264000), BigDecimal.valueOf(50000) }));

		return dtoList;
	}

	private List<ArrayValueDTO> buildCadburyValues() {
		List<ArrayValueDTO> dtoList = new ArrayList<>();

		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491004800), BigDecimal.valueOf(10000) // these
																													// are
																													// random
																													// values,
																													// consider
																													// a
																													// possible
																													// change
																													// on
																													// scale,
		}));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491091200), BigDecimal.valueOf(20000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491177600), BigDecimal.valueOf(30000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491264000), BigDecimal.valueOf(40000) }));

		return dtoList;
	}

	private List<ArrayValueDTO> buildTurinValues() {
		List<ArrayValueDTO> dtoList = new ArrayList<>();

		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491004800), BigDecimal.valueOf(15000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491091200), BigDecimal.valueOf(25000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491177600), BigDecimal.valueOf(35000) }));
		dtoList.add(new ArrayValueDTO(new BigDecimal[] { BigDecimal.valueOf(1491264000), BigDecimal.valueOf(65000) }));

		return dtoList;
	}

	private List<DisplayableDTO> buildBrands() {
		List<DisplayableDTO> list = new ArrayList<>();

		list.add(new BrandIndicatorDTO("me", "My brand"));
		list.add(new BrandIndicatorDTO("mars", "Mars"));
		list.add(new BrandIndicatorDTO("hersheys", "Hershey's"));
		list.add(new BrandIndicatorDTO("cadbury", "Cadbury"));
		list.add(new BrandIndicatorDTO("turin", "Turin"));

		return list;
	}
}
