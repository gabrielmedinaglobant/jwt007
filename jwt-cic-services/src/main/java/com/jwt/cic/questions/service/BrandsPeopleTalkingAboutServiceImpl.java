package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SingleValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.calculations.BrandsPeopleTalkingAboutCalculatorService;
import com.jwt.cic.questions.dao.BrandsPeopleTalkingAboutDAO;
import com.jwt.cic.questions.dto.BrandIndicatorDTO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutDTO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutResultDTO.Sentiment;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.util.BlockType;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;

@Primary
@Service
public class BrandsPeopleTalkingAboutServiceImpl implements BrandsPeopleTalkingAboutService {

	private QuestionResultContainerBuilder questionResultContainerBuilder;
	private BrandsPeopleTalkingAboutDAO brandsPeopleTalkingAboutDAO;
	private ProjectDAO projectDAO;
	
	@Autowired
	public BrandsPeopleTalkingAboutServiceImpl(
			QuestionResultContainerBuilder questionResultContainerBuilder,
			BrandsPeopleTalkingAboutDAO brandsPeopleTalkingAboutDAO,
			ProjectDAO projectDAO) {
		this.questionResultContainerBuilder = questionResultContainerBuilder;
		this.brandsPeopleTalkingAboutDAO = brandsPeopleTalkingAboutDAO;
		this.projectDAO = projectDAO;
	}

	@Override
	@Deprecated
	public QuestionSingleResultContainerDTO get(BlockType blockType, long projectId) {
		return null;
	}

	@Override
	@Deprecated
	public QuestionSingleResultContainerDTO get(BlockType blockType) {
		return null;
	}

	@Override
	public QuestionSingleResultContainerDTO get(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		BrandsPeopleTalkingAboutResultDTO result = brandsPeopleTalkingAboutDAO.get(project.getUuid());
		BrandsPeopleTalkingAboutDTO serviceResult = buildDTO(result);
		
		return questionResultContainerBuilder.buildDTO( serviceResult.getLastUpdatedDate() , serviceResult );
	}
	
	protected BrandsPeopleTalkingAboutDTO buildDTO(BrandsPeopleTalkingAboutResultDTO result){
		BrandsPeopleTalkingAboutDTO dto = new BrandsPeopleTalkingAboutDTO();
		dto.setBrands(buildBrandIndicators(result));
		dto.setSentimentOvertimeSplineChart(buildChartDTO(result));
		dto.setLastUpdatedDate(getLastUpdatedDate(result));
		return dto;
	}
	
	protected long getLastUpdatedDate(BrandsPeopleTalkingAboutResultDTO result) {
		return DateConverter.fromISODateToEpochSeconds(result.getLastUpdatedDate());
	}

	protected List<BrandIndicatorDTO> buildBrandIndicators(BrandsPeopleTalkingAboutResultDTO result){
		return result.getBrandResults().stream().map( (res) -> {
			BrandIndicatorDTO brandIndicatorDTO = new BrandIndicatorDTO();
			brandIndicatorDTO.setDisplayName(res.getBrand());
			brandIndicatorDTO.setKey(res.getBrand());
			brandIndicatorDTO.setRatio(res.getRatio() + ":1");
			return brandIndicatorDTO;
		} ).collect( Collectors.toList() );
	}
	
	protected SplineChartDTO buildChartDTO(BrandsPeopleTalkingAboutResultDTO result){
		
		SplineChartDTO dto = new SplineChartDTO();
		dto.setHorizontalMarkers(buildHorizontalMarkers(result));
		dto.setLines( buildSplineLines(result) );
		
		return dto;
	}

	

	protected List<SingleValueDTO> buildHorizontalMarkers(BrandsPeopleTalkingAboutResultDTO result){		
		
		return result.getBrandResults().stream().map( (res) -> {
			SingleValueDTO dto = new SingleValueDTO();
			
			dto.setKey( res.getBrand() + "-average" );
			dto.setValue( res.getAverage() );
			dto.setDisplayName("Average");
			dto.setTag( res.getBrand() );
			
			return dto;
		} ).collect( Collectors.toList() );
		
		
		//List<SingleValueDTO> list = new ArrayList<>();
		
		//list.add( new SingleValueDTO( "average", result.getMyBrandAverage(), "Average",result.get ) );
		
		//return list;
	}
	
	protected List<SplineChartLineDTO> buildSplineLines(BrandsPeopleTalkingAboutResultDTO result) {
		// for each brand generate a spline line
		
		return result.getBrandResults().stream().map( (res) -> {
			SplineChartLineDTO dto = new SplineChartLineDTO();
			
			dto.setKey(res.getBrand());
			dto.setValues( buildSplineLineValues(res.getSentimentOverTime()) );
			
			return dto;
		} ).collect( Collectors.toList() );
	}

	private List<ArrayValueDTO> buildSplineLineValues(List<Sentiment> sentimentOverTime) {

		return sentimentOverTime.stream().map( (res) -> {
			ArrayValueDTO dto = new ArrayValueDTO();
			dto.setKey( res.getDate() );
			dto.setValue( new BigDecimal[]{
				DateConverter.fromISODateToEpochSecondsAsBigDecimal(res.getDate()),
				res.getValue()
			});
			return dto;
		} ).collect( Collectors.toList() );
		
	}
	
}
