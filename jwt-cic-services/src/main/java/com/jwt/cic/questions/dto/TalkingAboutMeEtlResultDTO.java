package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TalkingAboutMeEtlResultDTO {

	private String date;
	@JsonProperty("femalebyWeek")
	private BigDecimal femaleByWeek;
	@JsonProperty("malebyWeek")
	private BigDecimal maleByWeek;
	private BigDecimal femaleSumByWeek;
	private BigDecimal maleSumByWeek;

	@JsonProperty("daysleadingfemale")
	private BigDecimal daysLeadingFemale;
	@JsonProperty("daysleadingmale")
	private BigDecimal daysLeadingMale;
	@JsonProperty("famaleUniverse")
	private BigDecimal femaleUniverse;
	@JsonProperty("femaleUniverseavg")
	private BigDecimal femaleUniverseAvg;
	@JsonProperty("genderedUniverseavg")
	private BigDecimal genderedUniverseAvg;
	@JsonProperty("maleUniverse")
	private BigDecimal maleUniverse;
	@JsonProperty("maleUniverseavg")
	private BigDecimal maleUniverseAvg;
	
	@JsonProperty("display_name")
	private String displayName;
	@JsonProperty("probability")
	private BigDecimal probability;
	
	@JsonProperty("income_range")
	private String incomeRange;
	private String rent;
	@JsonProperty("house_name")
	private String houseName;
	private String education;
	private String value;
	
	@JsonProperty("es")
	private BigDecimal languageEs;
	@JsonProperty("de")
	private BigDecimal languageDe;
	@JsonProperty("en")
	private BigDecimal languageEn;
	@JsonProperty("fr")
	private BigDecimal languageFr;
	@JsonProperty("it")
	private BigDecimal languageIt;
	@JsonProperty("pt")
	private BigDecimal languagePt;
	@JsonProperty("ru")
	private BigDecimal languageRu;
	
	@JsonProperty("id")
	private BigDecimal languageId;
	@JsonProperty("ja")
	private BigDecimal languageJa;
	@JsonProperty("vo")
	private BigDecimal languageVo;
	@JsonProperty("tl")
	private BigDecimal languageTl;
	@JsonProperty("ml")
	private BigDecimal languageMl;
	@JsonProperty("hi")
	private BigDecimal languageHi;
	
	@JsonProperty("other")
	private BigDecimal languageOther;
	
	@JsonProperty("demographics_mean_value")
	private BigDecimal demographicsMeanValue;
	@JsonProperty("demographics_type")
	private String demographicsType;
	@JsonProperty("demographics_value")
	private BigDecimal demographicsValue;
	@JsonProperty("demographics_sample_size")
	private BigDecimal demographicsSampleSize;
	
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	@JsonProperty("date_from")
	private String dateFrom;
	@JsonProperty("date_to")
	private String dateTo;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigDecimal getFemaleByWeek() {
		return femaleByWeek;
	}
	public void setFemaleByWeek(BigDecimal femalebyWeek) {
		this.femaleByWeek = femalebyWeek;
	}
	public BigDecimal getMaleByWeek() {
		return maleByWeek;
	}
	public void setMaleByWeek(BigDecimal malebyWeek) {
		this.maleByWeek = malebyWeek;
	}
	public BigDecimal getDaysLeadingFemale() {
		return daysLeadingFemale;
	}
	public void setDaysLeadingFemale(BigDecimal daysLeadingFemale) {
		this.daysLeadingFemale = daysLeadingFemale;
	}
	public BigDecimal getDaysLeadingMale() {
		return daysLeadingMale;
	}
	public void setDaysLeadingMale(BigDecimal daysLeadingMale) {
		this.daysLeadingMale = daysLeadingMale;
	}
	public BigDecimal getFemaleUniverse() {
		return femaleUniverse;
	}
	public void setFemaleUniverse(BigDecimal femaleUniverse) {
		this.femaleUniverse = femaleUniverse;
	}
	public BigDecimal getFemaleUniverseAvg() {
		return femaleUniverseAvg;
	}
	public void setFemaleUniverseAvg(BigDecimal femaleUniverseAvg) {
		this.femaleUniverseAvg = femaleUniverseAvg;
	}
	public BigDecimal getGenderedUniverseAvg() {
		return genderedUniverseAvg;
	}
	public void setGenderedUniverseAvg(BigDecimal genderedUniverseAvg) {
		this.genderedUniverseAvg = genderedUniverseAvg;
	}
	public BigDecimal getMaleUniverse() {
		return maleUniverse;
	}
	public void setMaleUniverse(BigDecimal maleUniverse) {
		this.maleUniverse = maleUniverse;
	}
	public BigDecimal getMaleUniverseAvg() {
		return maleUniverseAvg;
	}
	public void setMaleUniverseAvg(BigDecimal maleUniverseAvg) {
		this.maleUniverseAvg = maleUniverseAvg;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public BigDecimal getProbability() {
		return probability;
	}
	public void setProbability(BigDecimal probability) {
		this.probability = probability;
	}
	public String getIncomeRange() {
		return incomeRange;
	}
	public void setIncomeRange(String incomeRange) {
		this.incomeRange = incomeRange;
	}
	public String getRent() {
		return rent;
	}
	public void setRent(String rent) {
		this.rent = rent;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public BigDecimal getLanguageDe() {
		return languageDe;
	}
	public void setLanguageDe(BigDecimal languageDe) {
		this.languageDe = languageDe;
	}
	public BigDecimal getLanguageEn() {
		return languageEn;
	}
	public void setLanguageEn(BigDecimal languageEn) {
		this.languageEn = languageEn;
	}
	public BigDecimal getLanguageFr() {
		return languageFr;
	}
	public void setLanguageFr(BigDecimal languageFr) {
		this.languageFr = languageFr;
	}
	
	public BigDecimal getLanguageRu() {
		return languageRu;
	}
	public void setLanguageRu(BigDecimal languageRu) {
		this.languageRu = languageRu;
	}
	public BigDecimal getLanguageEs() {
		return languageEs;
	}
	public void setLanguageEs(BigDecimal languageEs) {
		this.languageEs = languageEs;
	}
	public BigDecimal getLanguageOther() {
		return languageOther;
	}
	public void setLanguageOther(BigDecimal languageOther) {
		this.languageOther = languageOther;
	}
	public BigDecimal getDemographicsMeanValue() {
		return demographicsMeanValue;
	}
	public void setDemographicsMeanValue(BigDecimal demographicsMeanValue) {
		this.demographicsMeanValue = demographicsMeanValue;
	}
	public String getDemographicsType() {
		return demographicsType;
	}
	public void setDemographicsType(String demographicsType) {
		this.demographicsType = demographicsType;
	}
	public BigDecimal getDemographicsValue() {
		return demographicsValue;
	}
	public void setDemographicsValue(BigDecimal demographicsValue) {
		this.demographicsValue = demographicsValue;
	}
	public BigDecimal getDemographicsSampleSize() {
		return demographicsSampleSize;
	}
	public void setDemographicsSampleSize(BigDecimal demographicsSampleSize) {
		this.demographicsSampleSize = demographicsSampleSize;
	}
	public BigDecimal getFemaleSumByWeek() {
		return femaleSumByWeek;
	}
	public void setFemaleSumByWeek(BigDecimal femaleSumByWeek) {
		this.femaleSumByWeek = femaleSumByWeek;
	}
	public BigDecimal getMaleSumByWeek() {
		return maleSumByWeek;
	}
	public void setMaleSumByWeek(BigDecimal maleSumByWeek) {
		this.maleSumByWeek = maleSumByWeek;
	}
	
	@Override
	public String toString() {
		return "TalkingAboutMeEtlResultDTO [" + (date != null ? "date=" + date + ", " : "")
				+ (femaleByWeek != null ? "femaleByWeek=" + femaleByWeek + ", " : "")
				+ (maleByWeek != null ? "maleByWeek=" + maleByWeek + ", " : "")
				+ (daysLeadingFemale != null ? "daysLeadingFemale=" + daysLeadingFemale + ", " : "")
				+ (daysLeadingMale != null ? "daysLeadingMale=" + daysLeadingMale + ", " : "")
				+ (femaleUniverse != null ? "femaleUniverse=" + femaleUniverse + ", " : "")
				+ (femaleUniverseAvg != null ? "femaleUniverseAvg=" + femaleUniverseAvg + ", " : "")
				+ (genderedUniverseAvg != null ? "genderedUniverseAvg=" + genderedUniverseAvg + ", " : "")
				+ (maleUniverse != null ? "maleUniverse=" + maleUniverse + ", " : "")
				+ (maleUniverseAvg != null ? "maleUniverseAvg=" + maleUniverseAvg + ", " : "")
				+ (displayName != null ? "displayName=" + displayName + ", " : "")
				+ (probability != null ? "probability=" + probability + ", " : "")
				+ (incomeRange != null ? "incomeRange=" + incomeRange + ", " : "")
				+ (rent != null ? "rent=" + rent + ", " : "")
				+ (houseName != null ? "houseName=" + houseName + ", " : "")
				+ (education != null ? "education=" + education + ", " : "")
				+ (value != null ? "value=" + value + ", " : "")
				+ (languageDe != null ? "languageDe=" + languageDe + ", " : "")
				+ (languageEn != null ? "languageEn=" + languageEn + ", " : "")
				+ (languageFr != null ? "languageFr=" + languageFr + ", " : "")
				+ (languageOther != null ? "languageOther=" + languageOther : "") + "]";
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public BigDecimal getLanguageIt() {
		return languageIt;
	}
	public void setLanguageIt(BigDecimal languageIt) {
		this.languageIt = languageIt;
	}
	public BigDecimal getLanguagePt() {
		return languagePt;
	}
	public void setLanguagePt(BigDecimal languagePt) {
		this.languagePt = languagePt;
	}
	public BigDecimal getLanguageId() {
		return languageId;
	}
	public void setLanguageId(BigDecimal languageId) {
		this.languageId = languageId;
	}
	public BigDecimal getLanguageJa() {
		return languageJa;
	}
	public void setLanguageJa(BigDecimal languageJa) {
		this.languageJa = languageJa;
	}
	public BigDecimal getLanguageVo() {
		return languageVo;
	}
	public void setLanguageVo(BigDecimal languageVo) {
		this.languageVo = languageVo;
	}
	public BigDecimal getLanguageTl() {
		return languageTl;
	}
	public void setLanguageTl(BigDecimal languageTl) {
		this.languageTl = languageTl;
	}
	public BigDecimal getLanguageMl() {
		return languageMl;
	}
	public void setLanguageMl(BigDecimal languageMl) {
		this.languageMl = languageMl;
	}
	public BigDecimal getLanguageHi() {
		return languageHi;
	}
	public void setLanguageHi(BigDecimal languageHi) {
		this.languageHi = languageHi;
	}
	
	

}
