package com.jwt.cic.questions.gtrends.dto;

import java.util.List;

/**
 * Created by vishal.domale on 09-05-2017.
 */
public class TokenRequestDto {

    private List<TokenComparisonItem> comparisonItem;
    private int category = 0;
    private String property = "";

    public List<TokenComparisonItem> getComparisonItem() {
        return comparisonItem;
    }

    public void setComparisonItem(List<TokenComparisonItem> comparisonItem) {
        this.comparisonItem = comparisonItem;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
