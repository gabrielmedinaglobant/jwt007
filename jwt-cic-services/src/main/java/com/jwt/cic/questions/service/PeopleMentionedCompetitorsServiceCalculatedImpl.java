package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.jwt.cic.charts.dto.ArrayValueDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.charts.dto.SplineChartLineDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.queries.dao.QueryTypePerProjectViewElementDAO;
import com.jwt.cic.queries.domain.QueryTypePerProjectViewElement;
import com.jwt.cic.questions.calculations.PeopleMentionedCompetitorsCalculationResult;
import com.jwt.cic.questions.calculations.PeopleMentionedCompetitorsCalculationStrategy;
import com.jwt.cic.questions.dao.OutputResultsDAO;
import com.jwt.cic.questions.dto.BrandIndicatorDTO;
import com.jwt.cic.questions.dto.DisplayableDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsSummaryDTO;
import com.jwt.cic.questions.dto.PeopleMentionedCompetitorsSummaryRowDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.infegy.dao.VolumeDAO;
import com.jwt.cic.questions.infegy.dto.VolumeDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;

@Primary
@Service
public class PeopleMentionedCompetitorsServiceCalculatedImpl implements PeopleMentionedCompetitorsService {

	private final Logger log = LoggerFactory.getLogger(PeopleMentionedCompetitorsServiceCalculatedImpl.class);

	private static final String INFEGY_MY_BRAND = "infegy-my-brand";
	private static final String INFEGY_DO_KEY = "infegy-my-brand-do";
	private static final String INFEGY_FEEL_KEY = "infegy-my-brand-feel";
	private static final String INFEGY_LIKE_KEY = "infegy-my-brand-like";
	
	private VolumeDAO volumeDAO;
	private ProjectDAO projectDAO;
	private QueryTypePerProjectViewElementDAO queryTypePerProjectViewElementDAO;
	private QuestionResultContainerBuilder questionResultContainerBuilder;
	private OutputResultsDAO outputResultsDAO;
	private PeopleMentionedCompetitorsCalculationStrategy peopleMentionedCompetitorsCalculationStrategy;
	
	@Autowired
	public PeopleMentionedCompetitorsServiceCalculatedImpl(VolumeDAO volumeDAO, 
			ProjectDAO projectDAO,
			QueryTypePerProjectViewElementDAO queryTypePerProjectViewElementDAO,
			QuestionResultContainerBuilder questionResultContainerBuilder,
			OutputResultsDAO outputResultsDAO,
			PeopleMentionedCompetitorsCalculationStrategy peopleMentionedCompetitorsCalculationStrategy) {
		this.volumeDAO = volumeDAO;
		this.projectDAO = projectDAO;
		this.queryTypePerProjectViewElementDAO = queryTypePerProjectViewElementDAO;
		this.questionResultContainerBuilder = questionResultContainerBuilder;
		this.outputResultsDAO = outputResultsDAO;
		this.peopleMentionedCompetitorsCalculationStrategy = peopleMentionedCompetitorsCalculationStrategy;
	}

	@Override
	public QuestionSingleResultContainerDTO get() {
		throw new RuntimeException("A project-id is required");
	}

	@Override
	@Cacheable("question.people-mentioned-competitors")
	public QuestionSingleResultContainerDTO get(Long projectId) {
		
		String projectUuid = getProject(projectId).getUuid();
		//Get all possible brand names and its query types
		List<QueryTypePerProjectViewElement> queryTypePerProjectElements = queryTypePerProjectViewElementDAO.findAllByProjectUuid(projectUuid);
		Map<String, QueryTypePerProjectViewElement> queryTypePerProjectElementsMap = queryTypePerProjectViewElementDAO.findAllByProjectUuid(projectUuid)
			.stream()
			.filter( (e) -> {
				return (e.getQueryTypeKey().compareTo(INFEGY_DO_KEY) != 0 && e.getQueryTypeKey().compareTo(INFEGY_FEEL_KEY) != 0 && e.getQueryTypeKey().compareTo(INFEGY_LIKE_KEY) != 0 );
			} )
			.map( 
				(qtpve) -> {
					return Pair.of(qtpve.getQueryTypeKey(), qtpve);
				}
			)
			.collect( Collectors.toMap( Pair<String, QueryTypePerProjectViewElement>::getKey, Pair<String, QueryTypePerProjectViewElement>::getValue) );
		Map<String, VolumeDTO> volumeResultsPerQueryType = queryTypePerProjectElements
			.stream()
			.filter( (e) -> {
				return (e.getQueryTypeKey().compareTo(INFEGY_DO_KEY) != 0 && e.getQueryTypeKey().compareTo(INFEGY_FEEL_KEY) != 0 && e.getQueryTypeKey().compareTo(INFEGY_LIKE_KEY) != 0 );
			} )
			.map( (e) -> {
				VolumeDTO v = volumeDAO.get(projectUuid, e.getQueryTypeKey());
				return Pair.of(e.getQueryTypeKey(), v);
			} )
			.collect( Collectors.toMap( Pair<String, VolumeDTO>::getKey, Pair<String, VolumeDTO>::getValue) );
		

		return questionResultContainerBuilder.buildDTO(getLastUpdatedDate(), buildDTO(queryTypePerProjectElementsMap, volumeResultsPerQueryType));
	}

	protected final long getLastUpdatedDate(){
		return outputResultsDAO.get().getLastUpdatedTimestamp().longValue();
	}
	
	protected final Project getProject(long projectId){
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		return project;
	}
	
	protected final PeopleMentionedCompetitorsDTO buildDTO(Map<String, QueryTypePerProjectViewElement> queryTypePerProjectElementsMap, Map<String, VolumeDTO> volumeResultsPerQueryType){
		
		PeopleMentionedCompetitorsCalculationResult res = peopleMentionedCompetitorsCalculationStrategy.calculate(queryTypePerProjectElementsMap, volumeResultsPerQueryType);
		
		PeopleMentionedCompetitorsDTO dto = new PeopleMentionedCompetitorsDTO();
		dto.setBrands( buildBrandsList(res.getQueryTypeKeysBrandsMap()) );
		dto.setSplineChart( buildSplineChart(res.getQueryTypeKeysBrandsMap(), res.getPostsUniversesAndDates()) );
		dto.setSummary( buildSummary(res.getQueryTypeKeysBrandsMap(), res.getAverages(), res.getDatesMax(), res.getDatesMin(), res.getHighests(), res.getLowests(), res.getTotals()) );
		dto.setLastUpdatedDate( getLastUpdatedDate() );
		
		return dto;
	}

	

	private PeopleMentionedCompetitorsSummaryDTO buildSummary(
			Map<String, String> queryTypeKeysBrandsMap,
			Map<String, BigDecimal> averages, Map<String, String> datesMax, Map<String, String> datesMin,
			Map<String, BigDecimal> highests, Map<String, BigDecimal> lowests, Map<String, BigDecimal> totals) {
		
		PeopleMentionedCompetitorsSummaryDTO dto = new PeopleMentionedCompetitorsSummaryDTO();
		BigDecimal sumOfAllTotals = totals.values().stream().reduce( BigDecimal.ZERO , BigDecimal::add );
		
		dto.setRows(
			queryTypeKeysBrandsMap.entrySet().stream().map( (entry) -> {
				
				String queryTypeKey = entry.getKey();
				String brandName = entry.getValue();
				PeopleMentionedCompetitorsSummaryRowDTO rowDTO = new PeopleMentionedCompetitorsSummaryRowDTO();
				
				rowDTO.setBrand( StringUtils.capitalize(brandName) );
				rowDTO.setBrandKey( brandName.toLowerCase() );
				rowDTO.setTotal( totals.get(queryTypeKey) ); 
				rowDTO.setLowest( lowests.get(queryTypeKey) );
				rowDTO.setLowestDate( DateConverter.fromISODateToEpochSeconds( datesMin.get(queryTypeKey) ) );
				rowDTO.setAverage( averages.get(queryTypeKey) );
				rowDTO.setHighest( highests.get(queryTypeKey) );
				rowDTO.setHighestDate( DateConverter.fromISODateToEpochSeconds( datesMax.get(queryTypeKey) ) );;
				rowDTO.setChangeUnit( UnitType.PERCENTAGE.getValue() );
				rowDTO.setChangeValue( BigDecimal.ZERO );
				rowDTO.setTotalPercentage( totals.get(queryTypeKey).divide(sumOfAllTotals, 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100L)));
				
				return rowDTO;
			} ).collect( Collectors.toList() )
		);
		// This should be ordered by brand name first, then by total
		
		return dto;
	}

	private SplineChartDTO buildSplineChart(Map<String, String> queryTypeKeysBrandsMap,
			Map<String, List<Pair<BigDecimal, String>>> postsUniversesAndDates) {
		
		SplineChartDTO dto = new SplineChartDTO();
		
		dto.setLines( buildSplineLineChartLines( queryTypeKeysBrandsMap, postsUniversesAndDates  ) );
		
		return dto;
	}

	private List<SplineChartLineDTO> buildSplineLineChartLines(
			Map<String, String> queryTypeKeysBrandsMap,
			Map<String, List<Pair<BigDecimal, String>>> postsUniversesAndDates) {
		
		List<SplineChartLineDTO> list = new ArrayList<>();
		
		queryTypeKeysBrandsMap.keySet().forEach( (key) -> {
			List<Pair<BigDecimal, String>> postsUniversesAndDatesList = postsUniversesAndDates.get(key);
			List<ArrayValueDTO> arrayValuesDTOList = 
				(List<ArrayValueDTO>) postsUniversesAndDatesList
				.stream()
				.map( (postsUniverseAndDate) -> {
					return new ArrayValueDTO(
						postsUniverseAndDate.getRight(), 
						new BigDecimal[]{ DateConverter.fromISODateToEpochSecondsAsBigDecimal(postsUniverseAndDate.getRight()), postsUniverseAndDate.getLeft() }
					);
				} )
				.collect( Collectors.toList() );
			list.add(new SplineChartLineDTO(StringUtils.capitalize( queryTypeKeysBrandsMap.get(key) ), arrayValuesDTOList));
		} );
		
		return list;
	}

	private List<DisplayableDTO> buildBrandsList(Map<String, String> queryTypeKeysBrandsMap) {
		
		log.info(queryTypeKeysBrandsMap.toString());
		
		return queryTypeKeysBrandsMap
				.entrySet()
				.stream()
				.map( (entry) -> {
			return new BrandIndicatorDTO( entry.getValue() , StringUtils.capitalize(entry.getValue()));
		} ).collect( Collectors.toList() );
	}
	
}
