package com.jwt.cic.questions.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Splitter;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.OtherTopicsCareAboutDAO;
import com.jwt.cic.questions.dto.OtherTopicsCareAboutEtlResultDTO;
import com.jwt.cic.questions.dto.PostInterestDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.dto.SourceInterestDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;

@Service
public class OtherTopicsCareAboutServiceImpl implements OtherTopicsCareAboutService {

	private OtherTopicsCareAboutDAO otherTopicsCareAboutDAO;
	private ProjectDAO projectDAO;
    private QuestionResultContainerBuilder questionResultListContainerBuilder;
	
	
	@Autowired
	public OtherTopicsCareAboutServiceImpl(OtherTopicsCareAboutDAO otherTopicsCareAboutDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultListContainerBuilder) {
		this.otherTopicsCareAboutDAO = otherTopicsCareAboutDAO;
		this.projectDAO = projectDAO;
		this.questionResultListContainerBuilder = questionResultListContainerBuilder;
	}

	@Override
	@Cacheable("question.other-topics-care-about.top-post-interests.0")
	public QuestionResultListContainerDTO getTopPostInterests() {
		List<OtherTopicsCareAboutEtlResultDTO> data = otherTopicsCareAboutDAO.getTopPostInterestsData();
		return questionResultListContainerBuilder.buildDTO( getDateFrom(data), getDateTo(data), getLastUpdatedDate(data), buildTopPostInterests( data ));
	}
	
	@Override
	@Cacheable("question.other-topics-care-about.top-post-interests.1")
	public QuestionResultListContainerDTO getTopPostInterests(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		List<OtherTopicsCareAboutEtlResultDTO> data = otherTopicsCareAboutDAO.getTopPostInterestsData(project.getUuid());
		return questionResultListContainerBuilder.buildDTO( getDateFrom(data), getDateTo(data), getLastUpdatedDate(data), buildTopPostInterests( data ));
	}

	@Override
	@Cacheable("question.other-topics-care-about.top-source-interests.0")
	public QuestionResultListContainerDTO getTopSourceInterests() {
		List<OtherTopicsCareAboutEtlResultDTO> data = otherTopicsCareAboutDAO.getTopSourceInterestsData();
		return questionResultListContainerBuilder.buildDTO( getDateFrom(data), getDateTo(data), getLastUpdatedDate(data), buildTopSourceInterests( data ));
	}
	
	@Override
	@Cacheable("question.other-topics-care-about.top-source-interests.1")
	public QuestionResultListContainerDTO getTopSourceInterests(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		List<OtherTopicsCareAboutEtlResultDTO> data = otherTopicsCareAboutDAO.getTopSourceInterestsData(project.getUuid());
		return questionResultListContainerBuilder.buildDTO( getDateFrom(data), getDateTo(data), getLastUpdatedDate(data), buildTopSourceInterests(data) );
	}
	
	protected final long getLastUpdatedDate( List<OtherTopicsCareAboutEtlResultDTO> data ){
		return DateConverter.fromISODateToEpochSeconds( data.stream().findFirst().get().getLastUpdatedDate() );
	}
	
	protected final long getDateFrom( List<OtherTopicsCareAboutEtlResultDTO> data ){
		return DateConverter.fromISODateToEpochSecondsTrimmed( data.stream().findFirst().get().getDateFrom() );
	}
	
	protected final long getDateTo( List<OtherTopicsCareAboutEtlResultDTO> data ){
		return DateConverter.fromISODateToEpochSecondsTrimmed( data.stream().findFirst().get().getDateTo() );
	}
	
	private List<PostInterestDTO> buildTopPostInterests( List<OtherTopicsCareAboutEtlResultDTO> data ){
		return data.stream().map( (etlDTO) -> {
			PostInterestDTO postInterestDTO = new PostInterestDTO();
			List<String> topics =  Splitter.on("|").splitToList(etlDTO.getName());
			
			postInterestDTO.setUnit( UnitType.PERCENTAGE.getValue() );
			postInterestDTO.setValue( etlDTO.getDistribution() );
			postInterestDTO.setTopic( topics.get(0).toUpperCase() );
			postInterestDTO.setSubtopic( topics.get(1) );
			//TODO: Throw exception if data form name doesn't come as xxx|yyy
			
			return postInterestDTO;
		} ).collect( Collectors.toList() );
	}

	private List<SourceInterestDTO> buildTopSourceInterests( List<OtherTopicsCareAboutEtlResultDTO> data ){
		return data.stream().map( (etlDTO) -> {			
			SourceInterestDTO sourceInterestDTO = new SourceInterestDTO();
			List<String> topics =  Splitter.on("|").splitToList(etlDTO.getName());
			
			sourceInterestDTO.setDistributionUnit( UnitType.PERCENTAGE.getValue() );
			sourceInterestDTO.setDistributionValue( etlDTO.getDistribution() );
			sourceInterestDTO.setRatioUnit( UnitType.RATIO.getValue() );
			sourceInterestDTO.setRatioValue( etlDTO.getRatio() );
			sourceInterestDTO.setRatioPercentageUnit( UnitType.PERCENTAGE.getValue() );
			sourceInterestDTO.setRatioPercentageValue( etlDTO.getRatioPercentage() );
			sourceInterestDTO.setTopic( topics.get(1).toUpperCase() );
			sourceInterestDTO.setSubtopic( topics.get(0) );
			//TODO: Throw exception if data form name doesn't come as xxx|yyy
			
			return sourceInterestDTO;
		} ).collect( Collectors.toList() );
	}

	
}
