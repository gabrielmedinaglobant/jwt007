package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;

public interface EmotionsAboutCategoryService {
	QuestionResultListContainerDTO get();
	QuestionResultListContainerDTO get(long projectId);
}
