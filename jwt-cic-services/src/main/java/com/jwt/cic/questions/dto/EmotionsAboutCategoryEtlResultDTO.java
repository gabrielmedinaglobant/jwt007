package com.jwt.cic.questions.dto;

import java.math.BigDecimal;
import java.util.List;

public class EmotionsAboutCategoryEtlResultDTO {
	private List<ValueDTO> values;
	private InfoEtlResultDTO info;
	
	public List<ValueDTO> getValues() {
		return values;
	}
	public void setValues(List<ValueDTO> values) {
		this.values = values;
	}

	public InfoEtlResultDTO getInfo() {
		return info;
	}
	public void setInfo(InfoEtlResultDTO info) {
		this.info = info;
	}

	public static class ValueDTO {
		private String type;
		private BigDecimal value;
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public BigDecimal getValue() {
			return value;
		}
		public void setValue(BigDecimal value) {
			this.value = value;
		}
		
	}
}
