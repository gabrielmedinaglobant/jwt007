package com.jwt.cic.questions.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.OutputResultsDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class OutputResultsDAOAWSImpl implements OutputResultsDAO {

    private ResourceFetcher resourceFetcher;
    @Value("${question.s3.bucket}")
    private String bucketLocation;
    @Value("${question.s3.bucket.output-results.json}")
    private String resourceLocation;

    @Autowired
    public OutputResultsDAOAWSImpl(ResourceFetcher resourceFetcher) {
        this.resourceFetcher = resourceFetcher;
    }
    
    public OutputResultsDTO get() {        
        return fetchData(bucketLocation + resourceLocation);
    }
    
    protected final OutputResultsDTO fetchData(String location){
    	return resourceFetcher.fetchObject(location, OutputResultsDTO.class);
    }

}
