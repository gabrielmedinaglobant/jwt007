package com.jwt.cic.questions.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dto.TalkingAboutMeEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class TalkingAboutMeDAOAWSImpl implements TalkingAboutMeDAO {

	private final Logger log = LoggerFactory.getLogger(TalkingAboutMeDAOAWSImpl.class);
	
	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.talking-about-me.json}")
	private String resourceLocation;
	
	@Autowired
	public TalkingAboutMeDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	public List<TalkingAboutMeEtlResultDTO> get() {
		return resourceFetcher.fetchObjects(bucketLocation+resourceLocation, TalkingAboutMeEtlResultDTO.class);
	}
	
	@Override
	public List<TalkingAboutMeEtlResultDTO> get(String projectUuid) {
		return resourceFetcher.fetchObjects(bucketLocation + projectUuid + "/output/" + resourceLocation, TalkingAboutMeEtlResultDTO.class);
	}
	
}
