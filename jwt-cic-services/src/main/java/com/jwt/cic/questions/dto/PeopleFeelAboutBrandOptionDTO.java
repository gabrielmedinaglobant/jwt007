package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.questions.util.FeelingAboutBrandOptionType;

public class PeopleFeelAboutBrandOptionDTO implements QuestionResultDTO {
	private FeelingAboutBrandOptionType option;
	private List<PeopleFeelAboutBrandWordDTO> words;
	
	public PeopleFeelAboutBrandOptionDTO(FeelingAboutBrandOptionType option, List<PeopleFeelAboutBrandWordDTO> words) {
		this.option = option;
		this.words = words;
	}

	public FeelingAboutBrandOptionType getOption() {
		return option;
	}
	public void setOption(FeelingAboutBrandOptionType option) {
		this.option = option;
	}
	public List<PeopleFeelAboutBrandWordDTO> getWords() {
		return words;
	}
	public void setWords(List<PeopleFeelAboutBrandWordDTO> words) {
		this.words = words;
	}
	
}
