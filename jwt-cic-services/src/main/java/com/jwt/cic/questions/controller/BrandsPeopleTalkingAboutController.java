package com.jwt.cic.questions.controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutDTO;
import com.jwt.cic.questions.dto.QuestionResultContainerDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.service.BrandsPeopleTalkingAboutService;
import com.jwt.cic.questions.service.QuestionService;
import com.jwt.cic.questions.util.BlockType;

@RestController
@RequestMapping("api/v1/questions/brands-people-talking-about")
public class BrandsPeopleTalkingAboutController {

	private QuestionService questionService;
	private BrandsPeopleTalkingAboutService brandsPeopleTalkingAboutService;

	@Autowired
	public BrandsPeopleTalkingAboutController(QuestionService questionService,
			BrandsPeopleTalkingAboutService brandsPeopleTalkingAboutService) {
		this.questionService = questionService;
		this.brandsPeopleTalkingAboutService = brandsPeopleTalkingAboutService;
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public QuestionSingleResultContainerDTO get(
			@RequestParam(value = "block-type", defaultValue = "documents") String blockType,
			@RequestParam(value = "project-id", defaultValue = "-1") long projectId) {
		
		if(projectId > 0){
			return brandsPeopleTalkingAboutService.get(projectId);
		}
		else{
			return brandsPeopleTalkingAboutService.get(BlockType.getByQueryParamValue(blockType));
		}
		
	}

	@Deprecated
	@RequestMapping(method = RequestMethod.GET, value = { "/raw" })
	public JSONObject getBrandsPeopleTalkingAboutRaw(
			@RequestParam(value = "block-type", defaultValue = "documents") String blockType,
			@RequestParam(value = "project-id") long projectId) {
		return questionService.getBrandsPeopleTalkingAboutRaw(BlockType.getByQueryParamValue(blockType));
	}
}
