package com.jwt.cic.questions.dao;

import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.questions.gtrends.GoogleTrendsClient;
import com.jwt.cic.questions.gtrends.GoogleTrendsRequest;
import com.jwt.cic.questions.gtrends.GoogleTrendsResponseParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by vishal.domale on 09-05-2017.
 */
@Primary
@Repository
public class PeopleLookedCompetitorsDAOGtrendImpl implements PeopleLookedCompetitorsDAO {


    private GoogleTrendsClient googleTrendsClient;

    private GoogleTrendsResponseParser googleTrendsResponceParser;

    @Value("${google.trends.general.url}")
    private String generalUrlString;
    @Value("${google.trends.csv.url}")
    private String csvUrlString;

    @Autowired
    public PeopleLookedCompetitorsDAOGtrendImpl(GoogleTrendsClient googleTrendsClient, GoogleTrendsResponseParser googleTrendsResponceParser) {
        this.googleTrendsClient = googleTrendsClient;
        this.googleTrendsResponceParser = googleTrendsResponceParser;
    }

    @Override
    public List<Map<String, String>> get(String[] brands) {
        List<Map<String, String>> etlResult = null;
        try {
            GoogleTrendsRequest googleTrendsRequest = new GoogleTrendsRequest(brands, csvUrlString, generalUrlString);
            String content = googleTrendsClient.execute(googleTrendsRequest);
            etlResult = googleTrendsResponceParser.parse(content);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvalidJsonFromDataSourceException();
        }
        return etlResult;
    }
}
