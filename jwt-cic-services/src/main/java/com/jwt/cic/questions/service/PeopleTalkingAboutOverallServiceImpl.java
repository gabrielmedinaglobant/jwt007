package com.jwt.cic.questions.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jwt.cic.questions.dao.PeopleTalkingAboutOverallDAO;
import com.jwt.cic.questions.dto.PeopleTalkingAboutOverallWordDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.gtrends.GoogleTrendsHotTrends;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;

/**
 * 
 * Service Implementation for Q6/23 - Discover - Question "What are people
 * talking about overall?"
 * 
 * @author gabriel.medina
 *
 */
@Service
public class PeopleTalkingAboutOverallServiceImpl implements PeopleTalkingAboutOverallService {

	private GoogleTrendsHotTrends googleTrendsHotTrends;
	private ProjectDAO projectDAO;
	private QuestionResultContainerBuilder questionResultListContainerBuilder;

	private String UNITED_STATES_REGION = "united_states";
	
	private final int US_RESULTS = 10;
	private final int WORLD_RESULTS = 15;
	
	@Autowired
	public PeopleTalkingAboutOverallServiceImpl(GoogleTrendsHotTrends googleTrendsHotTrends,
			ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultListContainerBuilder) {
		this.googleTrendsHotTrends = googleTrendsHotTrends;
		this.projectDAO = projectDAO;
		this.questionResultListContainerBuilder = questionResultListContainerBuilder;
	}

	@Deprecated
	@Override
	public QuestionResultListContainerDTO getAll() {
		long epoch = System.currentTimeMillis()/1000;
		List<PeopleTalkingAboutOverallWordDTO> results = googleTrendsHotTrends
				.getTrendingSearchesPerRegion().get(UNITED_STATES_REGION)
				.stream()
				.map((trend) -> {
					return new PeopleTalkingAboutOverallWordDTO(trend);
				}).collect(Collectors.toList());
		Collections.shuffle(results);
		return questionResultListContainerBuilder.buildDTO( epoch , epoch, epoch, buildDTO( googleTrendsHotTrends.getTrendingSearchesPerRegion() ) );
	}

	@Override
	public QuestionResultListContainerDTO getAll(long projectId) {
		long epoch = System.currentTimeMillis()/1000;
		Project project = projectDAO.findOne(projectId);
		if (project == null) {
			throw new ProjectNotFoundException();
		}
		
		return questionResultListContainerBuilder.buildDTO( epoch , epoch, epoch, buildDTO( googleTrendsHotTrends.getTrendingSearchesPerRegion() ) );
			
	}
	
	private List<PeopleTalkingAboutOverallWordDTO> buildDTO(Map<String, List<String>> googleTrendsResults){
		List<String> usResults = googleTrendsResults.get(UNITED_STATES_REGION);
		Collections.shuffle(usResults);
		
		List<String> worldwideResults = new ArrayList<>();
		googleTrendsResults
			.entrySet()
			.stream()
			.filter((x) -> x.getKey().compareTo(UNITED_STATES_REGION) != 0)
			.forEach((regionTrendsEntry) -> {
				worldwideResults.addAll(regionTrendsEntry.getValue());
			});
		Collections.shuffle(worldwideResults);
		
		List<String> finalResults = new ArrayList<>();
		finalResults.addAll( usResults.subList(0, US_RESULTS - 1) );
		finalResults.addAll( worldwideResults.subList(0, WORLD_RESULTS - 1) );
		
		List<PeopleTalkingAboutOverallWordDTO> results = finalResults
			.stream()
			.map((trend) -> {
				return new PeopleTalkingAboutOverallWordDTO(trend);
			}).collect(Collectors.toList());
		
		return results;
	}
	
}
