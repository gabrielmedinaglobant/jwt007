package com.jwt.cic.questions.infegy.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jwt.cic.questions.dao.VolumeChangedDayOverDayDAOAWSImpl;
import com.jwt.cic.questions.infegy.dto.VolumeDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Repository
public class VolumeDAOAWSImpl implements VolumeDAO {

private final Logger log = LoggerFactory.getLogger(VolumeChangedDayOverDayDAOAWSImpl.class);
	
	private ResourceFetcher resourceFetcher;
	@Value("${question.s3.bucket}")
	private String bucketLocation;
	@Value("${question.s3.bucket.input.volume.json}")
	private String resourceLocation;
	
	@Autowired
	public VolumeDAOAWSImpl(ResourceFetcher resourceFetcher) {
		this.resourceFetcher = resourceFetcher;
	}

	@Override
	public VolumeDTO get(String projectUuid, String queryTypeValue) {
		return resourceFetcher.fetchObject(bucketLocation + projectUuid + "/input/" + queryTypeValue + "/" + resourceLocation, VolumeDTO.class);
	}

}
