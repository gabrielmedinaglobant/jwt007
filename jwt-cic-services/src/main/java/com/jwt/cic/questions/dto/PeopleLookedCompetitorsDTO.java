package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.charts.dto.SingleBarChartDTO;
import com.jwt.cic.charts.dto.SplineChartDTO;

public class PeopleLookedCompetitorsDTO implements QuestionResultDTO {
	private List<DisplayableDTO> brands;
	private SplineChartDTO splineChart;
	private SingleBarChartDTO barChart;
	private List<PeopleLookedCompetitorsIndicatorDTO> indicators;
	
	public List<DisplayableDTO> getBrands() {
		return brands;
	}
	public void setBrands(List<DisplayableDTO> brands) {
		this.brands = brands;
	}
	public SplineChartDTO getSplineChart() {
		return splineChart;
	}
	public void setSplineChart(SplineChartDTO splineChart) {
		this.splineChart = splineChart;
	}
	public SingleBarChartDTO getBarChart() {
		return barChart;
	}
	public void setBarChart(SingleBarChartDTO barChart) {
		this.barChart = barChart;
	}
	public List<PeopleLookedCompetitorsIndicatorDTO> getIndicators() {
		return indicators;
	}
	public void setIndicators(List<PeopleLookedCompetitorsIndicatorDTO> indicators) {
		this.indicators = indicators;
	}
	
	
}