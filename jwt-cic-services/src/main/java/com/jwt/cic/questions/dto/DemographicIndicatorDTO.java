package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class DemographicIndicatorDTO {
	private String key;
	private IndicatorTrend trend;
	private BigDecimal value;
	private String unit;
	private String title;
	private BigDecimal meanValue;
	private String meanUnit;
	
	
	public DemographicIndicatorDTO(String key, IndicatorTrend trend, BigDecimal value, String unit, String title,
			BigDecimal meanValue, String meanUnit) {
		this.key = key;
		this.trend = trend;
		this.value = value;
		this.unit = unit;
		this.title = title;
		this.meanValue = meanValue;
		this.meanUnit = meanUnit;
	}
	public DemographicIndicatorDTO() {
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public IndicatorTrend getTrend() {
		return trend;
	}
	public void setTrend(IndicatorTrend trend) {
		this.trend = trend;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getMeanValue() {
		return meanValue;
	}
	public void setMeanValue(BigDecimal meanValue) {
		this.meanValue = meanValue;
	}
	public String getMeanUnit() {
		return meanUnit;
	}
	public void setMeanUnit(String meanUnit) {
		this.meanUnit = meanUnit;
	}
	
}
