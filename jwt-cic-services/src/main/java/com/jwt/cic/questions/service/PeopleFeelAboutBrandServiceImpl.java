package com.jwt.cic.questions.service;

import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.PeopleFeelAboutBrandDAO;
import com.jwt.cic.questions.dao.PeopleFeelAboutBrandDAOAWSImpl;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandEtlResultDTO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandOptionDTO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandWordDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.util.FeelingAboutBrandOptionType;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by vishal.domale on 2017-04-10. Last update by gabriel.medina on
 * 2017-05-10
 * 
 */

@Service
public class PeopleFeelAboutBrandServiceImpl implements PeopleFeelAboutBrandService {

	private final Logger log = LoggerFactory.getLogger(PeopleFeelAboutBrandServiceImpl.class);
	
	private PeopleFeelAboutBrandDAO peopleFeelAboutBrandDAO;
	private ProjectDAO projectDAO;
	private QuestionResultContainerBuilder questionResultListContainerBuilder;
	
	@Autowired
	PeopleFeelAboutBrandServiceImpl(PeopleFeelAboutBrandDAO peopleFeelAboutBrandDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultListContainerBuilder) {
		this.peopleFeelAboutBrandDAO = peopleFeelAboutBrandDAO;
		this.projectDAO = projectDAO;
		this.questionResultListContainerBuilder = questionResultListContainerBuilder;
	}

	@Override
	@Cacheable("question.people-feel-about-brand.0")
	public QuestionResultListContainerDTO get() {
		List<PeopleFeelAboutBrandEtlResultDTO> etlResults = peopleFeelAboutBrandDAO.get();
		return questionResultListContainerBuilder.buildDTO( getDateFrom(etlResults), getDateTo(etlResults), getLastUpdatedDate(etlResults), buildDTOList(etlResults) );
	}

	@Override
	@Cacheable("question.people-feel-about-brand.1")
	public QuestionResultListContainerDTO get(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		
		List<PeopleFeelAboutBrandEtlResultDTO> etlResults = peopleFeelAboutBrandDAO.get(project.getUuid());
		return questionResultListContainerBuilder.buildDTO( getDateFrom(etlResults), getDateTo(etlResults), getLastUpdatedDate(etlResults), buildDTOList(etlResults));
	}
	
	protected final long getLastUpdatedDate(List<PeopleFeelAboutBrandEtlResultDTO> etlResults){
		return DateConverter.fromISODateToEpochSeconds( etlResults.stream().findFirst().get().getLastUpdatedDate() );
	}
	
	protected final long getDateFrom(List<PeopleFeelAboutBrandEtlResultDTO> etlResults){
		return DateConverter.fromISODateToEpochSecondsTrimmed( etlResults.stream().findFirst().get().getDateFrom() );
	}
	
	protected final long getDateTo(List<PeopleFeelAboutBrandEtlResultDTO> etlResults){
		return DateConverter.fromISODateToEpochSecondsTrimmed( etlResults.stream().findFirst().get().getDateTo() );
	}
	
	private List<PeopleFeelAboutBrandOptionDTO> buildDTOList(List<PeopleFeelAboutBrandEtlResultDTO> etlResults) {
		
		Map<FeelingAboutBrandOptionType, List<PeopleFeelAboutBrandEtlResultDTO>> resultsGroupedByOption = buildResultsGroupedByOption(etlResults);
		
		//now that results are grouped, we can create a list with each option.
		return resultsGroupedByOption
				.entrySet()
				.stream()
				.map( (entry) -> {
					
					log.info("Entry KEY: " + entry.getKey());
					log.info("Entry VALUE: " + entry.getValue());
					
					List<PeopleFeelAboutBrandWordDTO> words = new ArrayList<>();
					
					words = entry
								.getValue()
								.stream()
								.map( (obj) -> {
									return new PeopleFeelAboutBrandWordDTO(obj.getTopic(), obj.getColor(), obj.getSize());
								} ).collect( Collectors.toList() );
					
					log.info("WORDS DTO: " + words);
					
					return new PeopleFeelAboutBrandOptionDTO( entry.getKey(), words );
				} ).collect( Collectors.toList() );
	}

	private Map<FeelingAboutBrandOptionType, List<PeopleFeelAboutBrandEtlResultDTO>> buildResultsGroupedByOption(
			List<PeopleFeelAboutBrandEtlResultDTO> etlResults) {
		
		Map<FeelingAboutBrandOptionType, List<PeopleFeelAboutBrandEtlResultDTO>> resultsGroupedByOption = new HashMap<>();
		
		for( PeopleFeelAboutBrandEtlResultDTO etlResult : etlResults ){
			//get list from map by option
			//if element is not cointaind yet, creates a new list
			List<PeopleFeelAboutBrandEtlResultDTO> optionEtlResults;
			FeelingAboutBrandOptionType option = etlResult.getOption();
			
			if(!resultsGroupedByOption.containsKey(option)){
				optionEtlResults = new ArrayList<>();
				resultsGroupedByOption.put(option, optionEtlResults);
			} else {
				optionEtlResults = resultsGroupedByOption.get(option);
			}
			optionEtlResults.add(etlResult);
		}
		
		return resultsGroupedByOption;
	}

	
}
