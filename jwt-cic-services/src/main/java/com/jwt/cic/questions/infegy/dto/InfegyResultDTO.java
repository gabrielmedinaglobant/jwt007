package com.jwt.cic.questions.infegy.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class InfegyResultDTO {
	@JsonProperty("query_info")
	private QueryInfoDTO queryInfo;
	@JsonProperty("query_meta")
	private QueryMetaDTO queryMeta;
	
	public abstract List<? extends OutputElementDTO> getOutput();
	
	public QueryInfoDTO getQueryInfo() {
		return queryInfo;
	}
	public void setQueryInfo(QueryInfoDTO queryInfo) {
		this.queryInfo = queryInfo;
	}
	public QueryMetaDTO getQueryMeta() {
		return queryMeta;
	}
	public void setQueryMeta(QueryMetaDTO queryMeta) {
		this.queryMeta = queryMeta;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InfegyResultDTO [output=").append(getOutput()).append(", queryInfo=").append(queryInfo)
				.append(", queryMeta=").append(queryMeta).append("]");
		return builder.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getOutput() == null) ? 0 : getOutput().hashCode());
		result = prime * result + ((queryInfo == null) ? 0 : queryInfo.hashCode());
		result = prime * result + ((queryMeta == null) ? 0 : queryMeta.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfegyResultDTO other = (InfegyResultDTO) obj;
		if (getOutput() == null) {
			if (other.getOutput() != null)
				return false;
		} else if (!getOutput().equals(other.getOutput()))
			return false;
		if (queryInfo == null) {
			if (other.queryInfo != null)
				return false;
		} else if (!queryInfo.equals(other.queryInfo))
			return false;
		if (queryMeta == null) {
			if (other.queryMeta != null)
				return false;
		} else if (!queryMeta.equals(other.queryMeta))
			return false;
		return true;
	}
	
}
