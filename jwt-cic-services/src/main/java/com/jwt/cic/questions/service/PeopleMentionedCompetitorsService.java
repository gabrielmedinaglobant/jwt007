package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

public interface PeopleMentionedCompetitorsService {
	QuestionSingleResultContainerDTO get();
	QuestionSingleResultContainerDTO get(Long projectId);
}
