package com.jwt.cic.questions.dto;

import com.jwt.cic.charts.dto.SplineChartDTO;

public class AgeDistributionDTO {
	private SplineChartDTO splineChart;

	public SplineChartDTO getSplineChart() {
		return splineChart;
	}

	public void setSplineChart(SplineChartDTO splineChart) {
		this.splineChart = splineChart;
	}
	
	
}
