package com.jwt.cic.questions.infegy.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeOutputElementDTO implements OutputElementDTO {

	private String date;
	@JsonProperty("posts_count")
	private BigDecimal postsCount;
	@JsonProperty("posts_normalized")
	private BigDecimal postsNormalized;
	@JsonProperty("posts_normalized_div")
	private BigDecimal postsNormalizedDiv;
	@JsonProperty("posts_universe")
	private BigDecimal postsUniverse;
	@JsonProperty("posts_word_div")
	private BigDecimal postsWordDiv;
	@JsonProperty("posts_word_normalized")
	private BigDecimal postsWordNormalized;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getPostsCount() {
		return postsCount;
	}

	public void setPostsCount(BigDecimal postsCount) {
		this.postsCount = postsCount;
	}

	public BigDecimal getPostsNormalized() {
		return postsNormalized;
	}

	public void setPostsNormalized(BigDecimal postsNormalized) {
		this.postsNormalized = postsNormalized;
	}

	public BigDecimal getPostsNormalizedDiv() {
		return postsNormalizedDiv;
	}

	public void setPostsNormalizedDiv(BigDecimal postsNormalizedDiv) {
		this.postsNormalizedDiv = postsNormalizedDiv;
	}

	public BigDecimal getPostsUniverse() {
		return postsUniverse;
	}

	public void setPostsUniverse(BigDecimal postsUniverse) {
		this.postsUniverse = postsUniverse;
	}

	public BigDecimal getPostsWordDiv() {
		return postsWordDiv;
	}

	public void setPostsWordDiv(BigDecimal postsWordDiv) {
		this.postsWordDiv = postsWordDiv;
	}

	public BigDecimal getPostsWordNormalized() {
		return postsWordNormalized;
	}

	public void setPostsWordNormalized(BigDecimal postsWordNormalized) {
		this.postsWordNormalized = postsWordNormalized;
	}
}
