package com.jwt.cic.questions.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by vishal.domale on 31-05-2017.
 */
public class QuestionSingleResultContainerDTO implements QuestionResultContainerDTO<QuestionResultDTO> {

    private QuestionResultDTO value;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long lastUpdatedDate;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long dateFrom;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long dateTo;

	public QuestionResultDTO getValue() {
        return value;
    }

    public void setValue(QuestionResultDTO value) {
        this.value = value;
    }

    public Long getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Long lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Long getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Long dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Long getDateTo() {
        return dateTo;
    }

    public void setDateTo(Long dateTo) {
        this.dateTo = dateTo;
    }
}
