package com.jwt.cic.questions.dto;

import com.jwt.cic.charts.dto.PieChartDTO;
import com.jwt.cic.charts.dto.SingleStackedBarDTO;

public class ThemeChartsDTO {
	private String name;
	private PieChartDTO pieChart;
	private SingleStackedBarDTO singleStackedBar;
	
	public ThemeChartsDTO() {
	}
	
	public ThemeChartsDTO(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PieChartDTO getPieChart() {
		return pieChart;
	}
	public void setPieChart(PieChartDTO pieChart) {
		this.pieChart = pieChart;
	}
	public SingleStackedBarDTO getSingleStackedBar() {
		return singleStackedBar;
	}
	public void setSingleStackedBar(SingleStackedBarDTO singleStackedBar) {
		this.singleStackedBar = singleStackedBar;
	}
	
}
