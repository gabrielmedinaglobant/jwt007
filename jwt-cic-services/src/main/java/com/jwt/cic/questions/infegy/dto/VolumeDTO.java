package com.jwt.cic.questions.infegy.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeDTO extends InfegyResultDTO {
	private List<VolumeOutputElementDTO> output;
	
	public List<VolumeOutputElementDTO> getOutput() {
		return output;
	}

	public void setOutput(List<VolumeOutputElementDTO> output) {
		this.output = output;
	}

}
