package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Deprecated
@JsonIgnoreProperties(ignoreUnknown = true)
public class BrandsPeopleTalkingAboutEtlResultDTO {
	
	private String date;
	
	@JsonProperty("documents_negativity")
	private BigDecimal documentsNegativity;
	@JsonProperty("documents_positivity")
	private BigDecimal documentsPositivity;
	@JsonProperty("documents_weekly_diff")
	private BigDecimal documentsWeeklyDiff;
	@JsonProperty("sentences_negativity")
	private BigDecimal sentencesNegativity;
	@JsonProperty("sentences_positivity")
	private BigDecimal sentencesPositivity;
	@JsonProperty("sentences_weekly_diff")
	private BigDecimal sentencesWeeklyDiff;
	@JsonProperty("subject_sentences_negativity")
	private BigDecimal subjectSentencesNegativity;
	@JsonProperty("subject_sentences_positivity")
	private BigDecimal subjectSentencesPositivity;
	@JsonProperty("subject_sentences_weekly_diff")
	private BigDecimal subjectSentencesWeeklyDiff;
	
	@JsonProperty("mixed_documents")
	private int mixedDocuments;
	@JsonProperty("mixed_documents_prom")
	private BigDecimal mixedDocumentsProm;
	@JsonProperty("mixed_subject_sentences")
	private int mixedSubjectSentences;
	@JsonProperty("mixed_subject_sentences_prom")
	private BigDecimal mixedSubjectSentencesProm;
	@JsonProperty("mixed_sentences")
	private int mixedSentences;
	@JsonProperty("mixed_sentences_prom")
	private BigDecimal mixedSentencesProm;
	
	@JsonProperty("negative_documents")
	private int negativeDocuments;
	@JsonProperty("negative_documents_prom")
	private BigDecimal negativeDocumentsProm;
	@JsonProperty("negative_subject_sentences")
	private int negativeSubjectSentences;
	@JsonProperty("negative_subject_sentences_prom")
	private BigDecimal negativeSubjectSentencesProm;
	@JsonProperty("negative_sentences")
	private int negativeSentences;
	@JsonProperty("negative_sentences_prom")
	private BigDecimal negativeSentencesProm;
	
	@JsonProperty("positive_documents")
	private int positiveDocuments;
	@JsonProperty("positive_documents_prom")
	private BigDecimal positiveDocumentsProm;
	@JsonProperty("positive_subject_documents") //TODO: Change this on python ETL scripts
	private int positiveSubjectSentences;
	@JsonProperty("positive_subject_sentences_prom")
	private BigDecimal positiveSubjectSentencesProm;
	@JsonProperty("positive_sentences")
	private int positiveSentences;
	@JsonProperty("positive_sentences_prom")
	private BigDecimal positiveSentencesProm;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigDecimal getDocumentsNegativity() {
		return documentsNegativity;
	}
	public void setDocumentsNegativity(BigDecimal documentsNegativity) {
		this.documentsNegativity = documentsNegativity;
	}
	public BigDecimal getDocumentsPositivity() {
		return documentsPositivity;
	}
	public void setDocumentsPositivity(BigDecimal documentsPositivity) {
		this.documentsPositivity = documentsPositivity;
	}
	public BigDecimal getDocumentsWeeklyDiff() {
		return documentsWeeklyDiff;
	}
	public void setDocumentsWeeklyDiff(BigDecimal documentsWeeklyDiff) {
		this.documentsWeeklyDiff = documentsWeeklyDiff;
	}
	public BigDecimal getSentencesNegativity() {
		return sentencesNegativity;
	}
	public void setSentencesNegativity(BigDecimal sentencesNegativity) {
		this.sentencesNegativity = sentencesNegativity;
	}
	public BigDecimal getSentencesPositivity() {
		return sentencesPositivity;
	}
	public void setSentencesPositivity(BigDecimal sentencesPositivity) {
		this.sentencesPositivity = sentencesPositivity;
	}
	public BigDecimal getSentencesWeeklyDiff() {
		return sentencesWeeklyDiff;
	}
	public void setSentencesWeeklyDiff(BigDecimal sentencesWeeklyDiff) {
		this.sentencesWeeklyDiff = sentencesWeeklyDiff;
	}
	public BigDecimal getSubjectSentencesNegativity() {
		return subjectSentencesNegativity;
	}
	public void setSubjectSentencesNegativity(BigDecimal subjectSentencesNegativity) {
		this.subjectSentencesNegativity = subjectSentencesNegativity;
	}
	public BigDecimal getSubjectSentencesPositivity() {
		return subjectSentencesPositivity;
	}
	public void setSubjectSentencesPositivity(BigDecimal subjectSentencesPositivity) {
		this.subjectSentencesPositivity = subjectSentencesPositivity;
	}
	public BigDecimal getSubjectSentencesWeeklyDiff() {
		return subjectSentencesWeeklyDiff;
	}
	public void setSubjectSentencesWeeklyDiff(BigDecimal subjectSentencesWeeklyDiff) {
		this.subjectSentencesWeeklyDiff = subjectSentencesWeeklyDiff;
	}
	public int getMixedDocuments() {
		return mixedDocuments;
	}
	public void setMixedDocuments(int mixedDocuments) {
		this.mixedDocuments = mixedDocuments;
	}
	public BigDecimal getMixedDocumentsProm() {
		return mixedDocumentsProm;
	}
	public void setMixedDocumentsProm(BigDecimal mixedDocumentsProm) {
		this.mixedDocumentsProm = mixedDocumentsProm;
	}
	public int getMixedSubjectSentences() {
		return mixedSubjectSentences;
	}
	public void setMixedSubjectSentences(int mixedSubjectSentences) {
		this.mixedSubjectSentences = mixedSubjectSentences;
	}
	public BigDecimal getMixedSubjectSentencesProm() {
		return mixedSubjectSentencesProm;
	}
	public void setMixedSubjectSentencesProm(BigDecimal mixedSubjectSentencesProm) {
		this.mixedSubjectSentencesProm = mixedSubjectSentencesProm;
	}
	public int getMixedSentences() {
		return mixedSentences;
	}
	public void setMixedSentences(int mixedSentences) {
		this.mixedSentences = mixedSentences;
	}
	public BigDecimal getMixedSentencesProm() {
		return mixedSentencesProm;
	}
	public void setMixedSentencesProm(BigDecimal mixedSentencesProm) {
		this.mixedSentencesProm = mixedSentencesProm;
	}
	public int getNegativeDocuments() {
		return negativeDocuments;
	}
	public void setNegativeDocuments(int negativeDocuments) {
		this.negativeDocuments = negativeDocuments;
	}
	public BigDecimal getNegativeDocumentsProm() {
		return negativeDocumentsProm;
	}
	public void setNegativeDocumentsProm(BigDecimal negativeDocumentsProm) {
		this.negativeDocumentsProm = negativeDocumentsProm;
	}
	public int getNegativeSubjectSentences() {
		return negativeSubjectSentences;
	}
	public void setNegativeSubjectSentences(int negativeSubjectSentences) {
		this.negativeSubjectSentences = negativeSubjectSentences;
	}
	public BigDecimal getNegativeSubjectSentencesProm() {
		return negativeSubjectSentencesProm;
	}
	public void setNegativeSubjectSentencesProm(BigDecimal negativeSubjectSentencesProm) {
		this.negativeSubjectSentencesProm = negativeSubjectSentencesProm;
	}
	public int getNegativeSentences() {
		return negativeSentences;
	}
	public void setNegativeSentences(int negativeSentences) {
		this.negativeSentences = negativeSentences;
	}
	public BigDecimal getNegativeSentencesProm() {
		return negativeSentencesProm;
	}
	public void setNegativeSentencesProm(BigDecimal negativeSentencesProm) {
		this.negativeSentencesProm = negativeSentencesProm;
	}
	public int getPositiveDocuments() {
		return positiveDocuments;
	}
	public void setPositiveDocuments(int positiveDocuments) {
		this.positiveDocuments = positiveDocuments;
	}
	public BigDecimal getPositiveDocumentsProm() {
		return positiveDocumentsProm;
	}
	public void setPositiveDocumentsProm(BigDecimal positiveDocumentsProm) {
		this.positiveDocumentsProm = positiveDocumentsProm;
	}
	public int getPositiveSubjectSentences() {
		return positiveSubjectSentences;
	}
	public void setPositiveSubjectSentences(int positiveSubjectSentences) {
		this.positiveSubjectSentences = positiveSubjectSentences;
	}
	public BigDecimal getPositiveSubjectSentencesProm() {
		return positiveSubjectSentencesProm;
	}
	public void setPositiveSubjectSentencesProm(BigDecimal positiveSubjectSentencesProm) {
		this.positiveSubjectSentencesProm = positiveSubjectSentencesProm;
	}
	public int getPositiveSentences() {
		return positiveSentences;
	}
	public void setPositiveSentences(int positiveSentences) {
		this.positiveSentences = positiveSentences;
	}
	public BigDecimal getPositiveSentencesProm() {
		return positiveSentencesProm;
	}
	public void setPositiveSentencesProm(BigDecimal positiveSentencesProm) {
		this.positiveSentencesProm = positiveSentencesProm;
	}

	
	
}
