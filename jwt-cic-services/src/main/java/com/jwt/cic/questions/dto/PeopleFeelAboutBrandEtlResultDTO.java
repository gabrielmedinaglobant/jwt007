package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jwt.cic.questions.util.FeelingAboutBrandOptionType;

/*
 * This class implements the following JSON result structure:
 * {
        "Size": 10,
        "__ColorAux": 0,
        "color": 1,
        "negative_documents": 674,
        "option": "do",
        "positive_documents": 6284,
        "score": 24652.85546875,
        "topics": "year"
    }
 */

/**
 * Created by vishal.domale on 13-04-2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PeopleFeelAboutBrandEtlResultDTO {

	@JsonProperty("size")
	private BigDecimal size;
	@JsonProperty("__ColorAux")
	private BigDecimal colorAux;
	private BigDecimal color;
	@JsonProperty("negative_documents")
	private BigDecimal negativeDocuments;
	private FeelingAboutBrandOptionType option;
	@JsonProperty("positive_documents")
	private BigDecimal positiveDocuments;
	private BigDecimal score;
	private String topic;
	
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	@JsonProperty("date_from")
	private String dateFrom;
	@JsonProperty("date_to")
	private String dateTo;
	
	public BigDecimal getSize() {
		return size;
	}
	public void setSize(BigDecimal size) {
		this.size = size;
	}
	public BigDecimal getColorAux() {
		return colorAux;
	}
	public void setColorAux(BigDecimal colorAux) {
		this.colorAux = colorAux;
	}
	public BigDecimal getColor() {
		return color;
	}
	public void setColor(BigDecimal color) {
		this.color = color;
	}
	public BigDecimal getNegativeDocuments() {
		return negativeDocuments;
	}
	public void setNegativeDocuments(BigDecimal negativeDocuments) {
		this.negativeDocuments = negativeDocuments;
	}
	public FeelingAboutBrandOptionType getOption() {
		return option;
	}
	public void setOption(FeelingAboutBrandOptionType option) {
		this.option = option;
	}
	public BigDecimal getPositiveDocuments() {
		return positiveDocuments;
	}
	public void setPositiveDocuments(BigDecimal positiveDocuments) {
		this.positiveDocuments = positiveDocuments;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((colorAux == null) ? 0 : colorAux.hashCode());
		result = prime * result + ((negativeDocuments == null) ? 0 : negativeDocuments.hashCode());
		result = prime * result + ((option == null) ? 0 : option.hashCode());
		result = prime * result + ((positiveDocuments == null) ? 0 : positiveDocuments.hashCode());
		result = prime * result + ((score == null) ? 0 : score.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		result = prime * result + ((topic == null) ? 0 : topic.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeopleFeelAboutBrandEtlResultDTO other = (PeopleFeelAboutBrandEtlResultDTO) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (colorAux == null) {
			if (other.colorAux != null)
				return false;
		} else if (!colorAux.equals(other.colorAux))
			return false;
		if (negativeDocuments == null) {
			if (other.negativeDocuments != null)
				return false;
		} else if (!negativeDocuments.equals(other.negativeDocuments))
			return false;
		if (option != other.option)
			return false;
		if (positiveDocuments == null) {
			if (other.positiveDocuments != null)
				return false;
		} else if (!positiveDocuments.equals(other.positiveDocuments))
			return false;
		if (score == null) {
			if (other.score != null)
				return false;
		} else if (!score.equals(other.score))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		if (topic == null) {
			if (other.topic != null)
				return false;
		} else if (!topic.equals(other.topic))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PeopleFeelAboutBrandEtlResultDTO [" + (size != null ? "size=" + size + ", " : "")
				+ (colorAux != null ? "colorAux=" + colorAux + ", " : "")
				+ (color != null ? "color=" + color + ", " : "")
				+ (negativeDocuments != null ? "negativeDocuments=" + negativeDocuments + ", " : "")
				+ (option != null ? "option=" + option + ", " : "")
				+ (positiveDocuments != null ? "positiveDocuments=" + positiveDocuments + ", " : "")
				+ (score != null ? "score=" + score + ", " : "") + (topic != null ? "topics=" + topic : "") + "]";
	}
	
	
}
