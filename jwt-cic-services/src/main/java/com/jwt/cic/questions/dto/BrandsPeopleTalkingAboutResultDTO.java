package com.jwt.cic.questions.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BrandsPeopleTalkingAboutResultDTO {
	
	@JsonProperty("brand_results")
	private List<Brand> brandResults;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	
	private InfoEtlResultDTO info;
	
	public BrandsPeopleTalkingAboutResultDTO() {
		this.brandResults = new ArrayList<>(180);
	}
	
	public List<Brand> getBrandResults() {
		return brandResults;
	}
	public void setBrandResults(List<Brand> results) {
		this.brandResults = results;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public InfoEtlResultDTO getInfo() {
		return info;
	}


	public void setInfo(InfoEtlResultDTO info) {
		this.info = info;
	}


	@Override
	public String toString() {
		return "{\"brandResults\":\"" + brandResults 
				+ "\",\"lastUpdatedDate\":\"" + lastUpdatedDate + "\"}";
	}

	public static class Brand {
		private String brand;
		private BigDecimal ratio;
		private BigDecimal average;
		@JsonProperty("sentiment_over_time")
		private List<Sentiment> sentimentOverTime;
		
		public Brand() {
			this.sentimentOverTime = new ArrayList<>(10);
		}
		
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand = brand;
		}
		public BigDecimal getRatio() {
			return ratio;
		}
		public void setRatio(BigDecimal ratio) {
			this.ratio = ratio;
		}
		public List<Sentiment> getSentimentOverTime() {
			return sentimentOverTime;
		}
		public void setSentimentOverTime(List<Sentiment> sentimentOverTime) {
			this.sentimentOverTime = sentimentOverTime;
		}
		public BigDecimal getAverage() {
			return average;
		}

		public void setAverage(BigDecimal average) {
			this.average = average;
		}

		@Override
		public String toString() {
			return "{\"brand\":\"" + brand + "\",\"ratio\":\"" + ratio + "\",\"average\":\"" + average
					+ "\",\"sentimentOverTime\":\"" + sentimentOverTime + "\"}";
		}
		
		
		
	}
	public static class Sentiment {
		private String date;
		private BigDecimal value;
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public BigDecimal getValue() {
			return value;
		}
		public void setValue(BigDecimal value) {
			this.value = value;
		}
		
		@Override
		public String toString() {
			return "{\"date\":\"" + date + "\",\"value\":\"" + value + "\"}";
		}
		
	}
	
}
