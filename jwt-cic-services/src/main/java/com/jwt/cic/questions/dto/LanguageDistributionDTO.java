package com.jwt.cic.questions.dto;

import java.util.List;

public class LanguageDistributionDTO {
	private List<LanguageIndicatorDTO> languageIndicator;

	public List<LanguageIndicatorDTO> getLanguageIndicator() {
		return languageIndicator;
	}

	public void setLanguageIndicator(List<LanguageIndicatorDTO> languageIndicator) {
		this.languageIndicator = languageIndicator;
	}
	
	
}
