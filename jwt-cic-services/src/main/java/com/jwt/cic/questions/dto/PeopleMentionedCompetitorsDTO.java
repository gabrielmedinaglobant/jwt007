package com.jwt.cic.questions.dto;

import java.util.List;

import com.jwt.cic.charts.dto.SplineChartDTO;

public class PeopleMentionedCompetitorsDTO implements QuestionResultDTO {
	private List<DisplayableDTO> brands;
	private SplineChartDTO splineChart;
	private PeopleMentionedCompetitorsSummaryDTO summary;
	private long lastUpdatedDate;

	public List<DisplayableDTO> getBrands() {
		return brands;
	}
	public void setBrands(List<DisplayableDTO> brands) {
		this.brands = brands;
	}
	public SplineChartDTO getSplineChart() {
		return splineChart;
	}
	public void setSplineChart(SplineChartDTO splineChart) {
		this.splineChart = splineChart;
	}
	public PeopleMentionedCompetitorsSummaryDTO getSummary() {
		return summary;
	}
	public void setSummary(PeopleMentionedCompetitorsSummaryDTO summary) {
		this.summary = summary;
	}

	public long getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(long lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
}
