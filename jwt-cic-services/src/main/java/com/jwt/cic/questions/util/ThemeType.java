package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ThemeType {
	ACQUISITION("Acquisition","acquisition"),ATTRACTION("Attraction","attraction"),CONFIDENCE("Confidence","confidence"),
	COST("Cost","cost"),CREATIVITY("Creativity","creativity"),EXPECTATION("Expectation","expectation"),
	HEALTH("Health","health"),HUMOR("Humor","humor"),PURCHASE_INTENT("Purchase Intent","purchase_intent"),
	QUALITY("Quality","quality"),SERVICE("Service","service"),TASTE("Taste","taste");
	
	private String value;
	private String eltJsonPropertyName;
	
	private static final Map<String, ThemeType> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private ThemeType(String value, String eltJsonPropertyName) {
		this.value = value;
		this.eltJsonPropertyName = eltJsonPropertyName;
	}
	
	private static Map<String, ThemeType> initializeValueMap(){
		Map<String, ThemeType> valueMap = new HashMap<>();
		for (ThemeType ct : ThemeType.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static ThemeType getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, ThemeType> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }

	public String getValue() {
		return value;
	}

	public String getEltJsonPropertyName() {
		return eltJsonPropertyName;
	}
	
	
}
