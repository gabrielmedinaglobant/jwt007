package com.jwt.cic.questions.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum BlockType {
	DOCUMENTS("documents","documents"), SENTENCES("sentences","sentences"), SUBJECT_SENTENCES("subject_sentences","subject-sentences");
	
	private String queryParamValue;
	private String value;
	private static final Map<String, BlockType> qpMap = Collections.unmodifiableMap(initializeQueryParamValueMap());
	
	BlockType(String value, String queryParamValue){
		this.value = value;
		this.queryParamValue = queryParamValue;
	}

	public String getValue() {
		return value;
	}

	public String getQueryParamValue() {
		return queryParamValue;
	}
	
	public static BlockType getByQueryParamValue(String queryParamValue){
		if(!qpMap.containsKey(queryParamValue)){
			throw new IllegalArgumentException();
		}
		return qpMap.get(queryParamValue);
	}
	
	private static Map<String, BlockType> initializeQueryParamValueMap(){
		Map<String, BlockType> qpMap = new HashMap<>();
	    for (BlockType s : BlockType.values()) {
	    	qpMap.put(s.queryParamValue, s);
	    }
	    return qpMap;
	}
}
