package com.jwt.cic.questions.dao;

import java.util.List;
	
import com.jwt.cic.questions.dto.TalkingAboutMeEtlResultDTO;

public interface TalkingAboutMeDAO {
	public List<TalkingAboutMeEtlResultDTO> get();

	List<TalkingAboutMeEtlResultDTO> get(String projectUuid);
}
