package com.jwt.cic.questions.dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandEtlResultDTO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandOptionDTO;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vishal.domale on 10-04-2017.
 */

@Deprecated
@Repository
public class PeopleFeelAboutBrandHardcodedJsonDAOImpl implements PeopleFeelAboutBrandDAO {

    @Override
    public List<PeopleFeelAboutBrandEtlResultDTO> get() {

        String jsonString = "[{\"option\":\"like\",\"value\":\"company\",\"feelingRank\":1,\"size\":10},{\"option\":\"like\",\"value\":\"factory\",\"feelingRank\":5,\"size\":1},{\"option\":\"like\",\"value\":\"produced\",\"feelingRank\":1,\"size\":5},{\"option\":\"like\",\"value\":\"chocolate\",\"feelingRank\":2,\"size\":4},{\"option\":\"like\",\"value\":\"younger\",\"feelingRank\":3,\"size\":3},{\"option\":\"like\",\"value\":\"integrated\",\"feelingRank\":4,\"size\":5},{\"option\":\"like\",\"value\":\"weaker\",\"feelingRank\":5,\"size\":6},{\"option\":\"like\",\"value\":\"brand\",\"feelingRank\":3,\"size\":2},{\"option\":\"like\",\"value\":\"Ghirardelli's\",\"feelingRank\":4,\"size\":5},{\"option\":\"like\",\"value\":\"chocolate\",\"feelingRank\":1,\"size\":1},{\"option\":\"like\",\"value\":\"wrapper\",\"feelingRank\":5,\"size\":4},{\"option\":\"like\",\"value\":\"colour\",\"feelingRank\":1,\"size\":8},{\"option\":\"feel\",\"value\":\"orange\",\"feelingRank\":4,\"size\":10},{\"option\":\"feel\",\"value\":\"new\",\"feelingRank\":2,\"size\":1},{\"option\":\"feel\",\"value\":\"black\",\"feelingRank\":3,\"size\":5},{\"option\":\"feel\",\"value\":\"Netflix\",\"feelingRank\":3,\"size\":4},{\"option\":\"feel\",\"value\":\"who\",\"feelingRank\":3,\"size\":3},{\"option\":\"feel\",\"value\":\"doctor\",\"feelingRank\":4,\"size\":5},{\"option\":\"feel\",\"value\":\"beauty\",\"feelingRank\":5,\"size\":6},{\"option\":\"feel\",\"value\":\"beast\",\"feelingRank\":3,\"size\":4},{\"option\":\"feel\",\"value\":\"Ghost\",\"feelingRank\":1,\"size\":2},{\"option\":\"feel\",\"value\":\"Shell\",\"feelingRank\":5,\"size\":3},{\"option\":\"feel\",\"value\":\"Power\",\"feelingRank\":5,\"size\":10},{\"option\":\"feel\",\"value\":\"Rangers\",\"feelingRank\":1,\"size\":8},{\"option\":\"do\",\"value\":\"Lorem\",\"feelingRank\":5,\"size\":1},{\"option\":\"do\",\"value\":\"ipsum\",\"feelingRank\":1,\"size\":10},{\"option\":\"do\",\"value\":\"dolor\",\"feelingRank\":4,\"size\":5},{\"option\":\"do\",\"value\":\"sit\",\"feelingRank\":2,\"size\":4},{\"option\":\"do\",\"value\":\"amet\",\"feelingRank\":4,\"size\":3},{\"option\":\"do\",\"value\":\"consectetur\",\"feelingRank\":3,\"size\":5},{\"option\":\"do\",\"value\":\"adipiscing\",\"feelingRank\":4,\"size\":6},{\"option\":\"do\",\"value\":\"elit\",\"feelingRank\":5,\"size\":4},{\"option\":\"do\",\"value\":\"sed\",\"feelingRank\":1,\"size\":2},{\"option\":\"do\",\"value\":\"eiusmod\",\"feelingRank\":2,\"size\":3},{\"option\":\"do\",\"value\":\"tempor\",\"feelingRank\":2,\"size\":10},{\"option\":\"do\",\"value\":\"incididunt\",\"feelingRank\":1,\"size\":8}]";
        ObjectMapper mapper = new ObjectMapper();
        List<PeopleFeelAboutBrandEtlResultDTO> result = null;
        try {

           result = mapper.readValue(jsonString,
                   TypeFactory.defaultInstance().constructCollectionLikeType(List.class, PeopleFeelAboutBrandEtlResultDTO.class));
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvalidJsonFromDataSourceException();
        }
        return result;
    }

	@Override
	public List<PeopleFeelAboutBrandEtlResultDTO> get(String projectUuid) {
		// TODO Auto-generated method stub
		return get();
	}
}
