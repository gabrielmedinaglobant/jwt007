package com.jwt.cic.questions.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jwt.cic.charts.dto.PieChartDTO;
import com.jwt.cic.charts.dto.SingleValueDTO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.questions.dao.KeyThemesTalkAboutDAO;
import com.jwt.cic.questions.dto.KeyThemeTalkAboutDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.dto.ThemeChartsDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.questions.util.ThemeType;
import com.jwt.cic.questions.util.UnitType;
import com.jwt.cic.utils.DateConverter;

@Service
public class KeyThemesTalkAboutServiceImpl implements KeyThemesTalkAboutService {

	private KeyThemesTalkAboutDAO keyThemesTalkAboutDAO;
	private ProjectDAO projectDAO;
	private QuestionResultContainerBuilder questionResultListContainerBuilder;
	
	private final String POSITIVE_SUFFIX = "_positive";
	private final String NEGATIVE_SUFFIX = "_negative";
	
	private final Logger log = LoggerFactory.getLogger(KeyThemesTalkAboutServiceImpl.class);
	
	@Autowired
	public KeyThemesTalkAboutServiceImpl(KeyThemesTalkAboutDAO keyThemesTalkAboutDAO, ProjectDAO projectDAO, QuestionResultContainerBuilder questionResultListContainerBuilder){
		this.keyThemesTalkAboutDAO = keyThemesTalkAboutDAO;
		this.projectDAO = projectDAO;
		this.questionResultListContainerBuilder = questionResultListContainerBuilder;
	}
	
	@Override
	@Cacheable("question.key-themes-talk-about.0")
	public QuestionResultListContainerDTO get() {
		Map<String,Object> eltResult = keyThemesTalkAboutDAO.get();
		
		log.info("ELT RESULTS");
		log.info(eltResult.toString());
		
		return questionResultListContainerBuilder.buildDTO( 
				DateConverter.fromISODateToEpochSecondsTrimmed( (String)eltResult.get("date_from") ), 
				DateConverter.fromISODateToEpochSecondsTrimmed( (String)eltResult.get("date_to") ), 
				DateConverter.fromISODateToEpochSeconds( (String)eltResult.get("last_updated_date") ), 
				buildDTOList(eltResult) );
	}
	
	@Override
	@Cacheable("question.key-themes-talk-about.1")
	public QuestionResultListContainerDTO get(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		
		Map<String,Object> eltResult = keyThemesTalkAboutDAO.get(project.getUuid());
		
		log.info("ELT RESULTS WITH PROJECT");
		log.info(eltResult.toString());
		
		return questionResultListContainerBuilder.buildDTO( 
				DateConverter.fromISODateToEpochSecondsTrimmed( (String)eltResult.get("date_from") ), 
				DateConverter.fromISODateToEpochSecondsTrimmed( (String)eltResult.get("date_to") ), 
				DateConverter.fromISODateToEpochSeconds( (String)eltResult.get("last_updated_date") ), 
				buildDTOList(eltResult) );
	}
	
	private List<KeyThemeTalkAboutDTO> buildDTOList(Map<String,Object> eltResult){
		List<KeyThemeTalkAboutDTO> result = 
			Arrays.asList(ThemeType.values())
					.stream()
					.filter( (tt) -> { return tt != ThemeType.EXPECTATION; } )
					.map( getMapper(eltResult) )
					.collect( Collectors.toList() );
		Collections.sort( result, new KeyThemesTalkAboutComparator() );
		return result;
	}

	private Function<ThemeType, KeyThemeTalkAboutDTO> getMapper(Map<String,Object> eltResult){
		return (tt) -> {
			
			log.info("ENTER MAPPER");
			
			KeyThemeTalkAboutDTO dto = new KeyThemeTalkAboutDTO();
			
			log.info("NAME: " + tt.getValue());
			log.info("DISTRIBUTION NAME: " + eltResult.get( tt.getEltJsonPropertyName() ) );
			log.info("POSITIVE INDICATOR NAME: " + tt.getEltJsonPropertyName() + POSITIVE_SUFFIX);
			log.info("POSITIVE INDICATOR VALUE: " + eltResult.get( tt.getEltJsonPropertyName() + POSITIVE_SUFFIX ) );
			log.info("NEGATIVE INDICATOR NAME: " + tt.getEltJsonPropertyName() + POSITIVE_SUFFIX);
			log.info("NEGATIVE INDICATOR VALUE: " + eltResult.get( tt.getEltJsonPropertyName() + NEGATIVE_SUFFIX ) );
			
			dto.setName( tt.getValue() );
			dto.setDistributionIndicatorValue( ((BigDecimal)eltResult.get( tt.getEltJsonPropertyName() )).setScale(0,RoundingMode.HALF_UP) );
			dto.setNegativityIndicatorValue( ((BigDecimal)eltResult.get( tt.getEltJsonPropertyName() + NEGATIVE_SUFFIX )).setScale(0,RoundingMode.HALF_UP) );
			dto.setPositivityIndicatorValue( ((BigDecimal)eltResult.get( tt.getEltJsonPropertyName() + POSITIVE_SUFFIX )).setScale(0,RoundingMode.HALF_UP) );
			dto.setDistributionIndicatorUnit( UnitType.PERCENTAGE.getValue() );
			dto.setNegativityIndicatorUnit( UnitType.PERCENTAGE.getValue() );
			dto.setPositivityIndicatorUnit( UnitType.PERCENTAGE.getValue() );
			dto.setPieChart( buildPieChartDTO( ((BigDecimal)eltResult.get( tt.getEltJsonPropertyName() ) ) ) );
			
			return dto;
		};
	}
	
	private PieChartDTO buildPieChartDTO( BigDecimal distributionValue ){
		PieChartDTO pieChartDTO = new PieChartDTO();
		
		pieChartDTO.setValues( buildPieChartDTOValues(distributionValue) );
		pieChartDTO.setIndicator( distributionValue.setScale(0,RoundingMode.HALF_UP) );
		pieChartDTO.setIndicatorUnit( UnitType.PERCENTAGE.getValue() );
		
		return pieChartDTO;
	}
	
	private List<SingleValueDTO> buildPieChartDTOValues( BigDecimal distributionValue ){
		List<SingleValueDTO> svList = new ArrayList<>();
		
		SingleValueDTO sv = new SingleValueDTO();
		sv.setKey("distribution");
		sv.setValue(distributionValue);
		svList.add(sv);
		
		sv = new SingleValueDTO();
		sv.setKey("leftover");
		sv.setValue(BigDecimal.valueOf(100).subtract(distributionValue));
		svList.add(sv);
		
		return svList;
	}
	
	private static class KeyThemesTalkAboutComparator implements Comparator<KeyThemeTalkAboutDTO> {

		@Override
		public int compare(KeyThemeTalkAboutDTO o1, KeyThemeTalkAboutDTO o2) {
			return -o1.getDistributionIndicatorValue().compareTo(o2.getDistributionIndicatorValue());
		}
		
	}

	
}
