package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class PeopleMentionedCompetitorsSummaryRowDTO {
	private String brand;
	private String brandKey;
	private BigDecimal total;
	private BigDecimal totalPercentage;
	private BigDecimal lowest;
	private long lowestDate;
	private BigDecimal average;
	private BigDecimal highest;
	private long highestDate;
	private BigDecimal changeValue;
	private String changeUnit;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getBrandKey() {
		return brandKey;
	}
	public void setBrandKey(String brandKey) {
		this.brandKey = brandKey;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotalPercentage() {
		return totalPercentage;
	}
	public void setTotalPercentage(BigDecimal totalPercentage) {
		this.totalPercentage = totalPercentage;
	}
	public BigDecimal getLowest() {
		return lowest;
	}
	public void setLowest(BigDecimal lowest) {
		this.lowest = lowest;
	}
	public long getLowestDate() {
		return lowestDate;
	}
	public void setLowestDate(long lowestDate) {
		this.lowestDate = lowestDate;
	}
	public BigDecimal getAverage() {
		return average;
	}
	public void setAverage(BigDecimal average) {
		this.average = average;
	}
	public BigDecimal getHighest() {
		return highest;
	}
	public void setHighest(BigDecimal highest) {
		this.highest = highest;
	}
	public long getHighestDate() {
		return highestDate;
	}
	public void setHighestDate(long highestDate) {
		this.highestDate = highestDate;
	}
	public BigDecimal getChangeValue() {
		return changeValue;
	}
	public void setChangeValue(BigDecimal changeValue) {
		this.changeValue = changeValue;
	}
	public String getChangeUnit() {
		return changeUnit;
	}
	public void setChangeUnit(String changeUnit) {
		this.changeUnit = changeUnit;
	}
	
}
