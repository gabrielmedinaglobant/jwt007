package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OtherTopicsCareAboutEtlResultDTO {
	private BigDecimal distribution;
	private String name;
	private BigDecimal ratio;
	private BigDecimal ratioPercentage;
	private BigDecimal score;
	
	@JsonProperty("last_updated_date")
	private String lastUpdatedDate;
	@JsonProperty("date_from")
	private String dateFrom;
	@JsonProperty("date_to")
	private String dateTo;
	
	public BigDecimal getDistribution() {
		return distribution;
	}
	public void setDistribution(BigDecimal distribution) {
		this.distribution = distribution;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getRatio() {
		return ratio;
	}
	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}
	public BigDecimal getRatioPercentage() {
		return ratioPercentage;
	}
	public void setRatioPercentage(BigDecimal ratioPercentage) {
		this.ratioPercentage = ratioPercentage;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	
}
