package com.jwt.cic.questions.dto;

public interface DisplayableDTO {
	public String getKey();
	public String getDisplayName();
}
