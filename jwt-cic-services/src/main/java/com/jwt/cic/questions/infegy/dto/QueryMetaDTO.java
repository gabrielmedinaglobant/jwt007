package com.jwt.cic.questions.infegy.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMetaDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("index_result_age")
	private BigDecimal indexResultAge;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("linguistics_count")
	private BigDecimal linguisticsCount;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal reach;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("sources_from_index")
	private BigDecimal sourcesFromIndex;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("total_from_index")
	private BigDecimal totalFromIndex;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("total_results")
	private BigDecimal totalResults;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("total_sources_est")
	private BigDecimal totalSourcesEst;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("total_spammed")
	private BigDecimal totalSpammed;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("total_universe")
	private BigDecimal totalUniverse;
	
	public BigDecimal getIndexResultAge() {
		return indexResultAge;
	}
	public void setIndexResultAge(BigDecimal indexResultAge) {
		this.indexResultAge = indexResultAge;
	}
	public BigDecimal getLinguisticsCount() {
		return linguisticsCount;
	}
	public void setLinguisticsCount(BigDecimal linguisticsCount) {
		this.linguisticsCount = linguisticsCount;
	}
	public BigDecimal getReach() {
		return reach;
	}
	public void setReach(BigDecimal reach) {
		this.reach = reach;
	}
	public BigDecimal getSourcesFromIndex() {
		return sourcesFromIndex;
	}
	public void setSourcesFromIndex(BigDecimal sourcesFromIndex) {
		this.sourcesFromIndex = sourcesFromIndex;
	}
	public BigDecimal getTotalFromIndex() {
		return totalFromIndex;
	}
	public void setTotalFromIndex(BigDecimal totalFromIndex) {
		this.totalFromIndex = totalFromIndex;
	}
	public BigDecimal getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(BigDecimal totalResults) {
		this.totalResults = totalResults;
	}
	public BigDecimal getTotalSourcesEst() {
		return totalSourcesEst;
	}
	public void setTotalSourcesEst(BigDecimal totalSourcesEst) {
		this.totalSourcesEst = totalSourcesEst;
	}
	public BigDecimal getTotalSpammed() {
		return totalSpammed;
	}
	public void setTotalSpammed(BigDecimal totalSpammed) {
		this.totalSpammed = totalSpammed;
	}
	public BigDecimal getTotalUniverse() {
		return totalUniverse;
	}
	public void setTotalUniverse(BigDecimal totalUniverse) {
		this.totalUniverse = totalUniverse;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QueryMetaDTO [indexResultAge=").append(indexResultAge).append(", linguisticsCount=")
				.append(linguisticsCount).append(", reach=").append(reach).append(", sourcesFromIndex=")
				.append(sourcesFromIndex).append(", totalFromIndex=").append(totalFromIndex).append(", totalResults=")
				.append(totalResults).append(", totalSourcesEst=").append(totalSourcesEst).append(", totalSpammed=")
				.append(totalSpammed).append(", totalUniverse=").append(totalUniverse).append("]");
		return builder.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indexResultAge == null) ? 0 : indexResultAge.hashCode());
		result = prime * result + ((linguisticsCount == null) ? 0 : linguisticsCount.hashCode());
		result = prime * result + ((reach == null) ? 0 : reach.hashCode());
		result = prime * result + ((sourcesFromIndex == null) ? 0 : sourcesFromIndex.hashCode());
		result = prime * result + ((totalFromIndex == null) ? 0 : totalFromIndex.hashCode());
		result = prime * result + ((totalResults == null) ? 0 : totalResults.hashCode());
		result = prime * result + ((totalSourcesEst == null) ? 0 : totalSourcesEst.hashCode());
		result = prime * result + ((totalSpammed == null) ? 0 : totalSpammed.hashCode());
		result = prime * result + ((totalUniverse == null) ? 0 : totalUniverse.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryMetaDTO other = (QueryMetaDTO) obj;
		if (indexResultAge == null) {
			if (other.indexResultAge != null)
				return false;
		} else if (!indexResultAge.equals(other.indexResultAge))
			return false;
		if (linguisticsCount == null) {
			if (other.linguisticsCount != null)
				return false;
		} else if (!linguisticsCount.equals(other.linguisticsCount))
			return false;
		if (reach == null) {
			if (other.reach != null)
				return false;
		} else if (!reach.equals(other.reach))
			return false;
		if (sourcesFromIndex == null) {
			if (other.sourcesFromIndex != null)
				return false;
		} else if (!sourcesFromIndex.equals(other.sourcesFromIndex))
			return false;
		if (totalFromIndex == null) {
			if (other.totalFromIndex != null)
				return false;
		} else if (!totalFromIndex.equals(other.totalFromIndex))
			return false;
		if (totalResults == null) {
			if (other.totalResults != null)
				return false;
		} else if (!totalResults.equals(other.totalResults))
			return false;
		if (totalSourcesEst == null) {
			if (other.totalSourcesEst != null)
				return false;
		} else if (!totalSourcesEst.equals(other.totalSourcesEst))
			return false;
		if (totalSpammed == null) {
			if (other.totalSpammed != null)
				return false;
		} else if (!totalSpammed.equals(other.totalSpammed))
			return false;
		if (totalUniverse == null) {
			if (other.totalUniverse != null)
				return false;
		} else if (!totalUniverse.equals(other.totalUniverse))
			return false;
		return true;
	}
	
	
}
