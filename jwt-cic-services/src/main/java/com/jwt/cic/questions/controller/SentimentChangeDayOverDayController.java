package com.jwt.cic.questions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.questions.dto.QuestionResultDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.dto.SentimentChangeDayOverDayDTO;
import com.jwt.cic.questions.service.QuestionService;
import com.jwt.cic.questions.service.SentimentChangeDayOverDayService;

import org.json.simple.JSONObject;

@RestController
@RequestMapping("api/v1/questions/sentiment-changed-day-over-day")
public class SentimentChangeDayOverDayController {
	
	private SentimentChangeDayOverDayService sentimentChangeDayOverDayService;
	
	@Autowired
	public SentimentChangeDayOverDayController(SentimentChangeDayOverDayService sentimentChangeDayOverDayService) {
		this.sentimentChangeDayOverDayService = sentimentChangeDayOverDayService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public QuestionSingleResultContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId) {
		if(projectId > 0){
			return sentimentChangeDayOverDayService.get(projectId);
		}
		else{
			return sentimentChangeDayOverDayService.get();
		}
	}
	
}
