package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;

/**
 * Created by vishal.domale on 11-04-2017.
 */
public interface ConversationHappeningService {
	QuestionSingleResultContainerDTO get();
	QuestionSingleResultContainerDTO get(long projectId);
}
