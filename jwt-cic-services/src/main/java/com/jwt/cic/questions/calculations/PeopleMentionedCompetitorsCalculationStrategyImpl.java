package com.jwt.cic.questions.calculations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jwt.cic.queries.domain.QueryTypePerProjectViewElement;
import com.jwt.cic.questions.infegy.dto.VolumeDTO;
import com.jwt.cic.questions.infegy.dto.VolumeOutputElementDTO;
import com.jwt.cic.questions.service.PeopleMentionedCompetitorsServiceCalculatedImpl;

@Component
public class PeopleMentionedCompetitorsCalculationStrategyImpl implements PeopleMentionedCompetitorsCalculationStrategy {

	private final Logger log = LoggerFactory.getLogger(PeopleMentionedCompetitorsServiceCalculatedImpl.class);
	
	private static final String INFEGY_MY_BRAND = "infegy-my-brand";
	private static final String INFEGY_DO_KEY = "infegy-my-brand-do";
	private static final String INFEGY_FEEL_KEY = "infegy-my-brand-feel";
	private static final String INFEGY_LIKE_KEY = "infegy-my-brand-like";
	
	@Override
	public PeopleMentionedCompetitorsCalculationResult calculate(
			Map<String, QueryTypePerProjectViewElement> queryTypePerProjectElementsMap,
			Map<String, VolumeDTO> volumeResultsPerQueryType) {
		
		//at this point I have info of my brands and its available competitors
		Collection<QueryTypePerProjectViewElement> queryTypePerProjectElements = queryTypePerProjectElementsMap.values();
		// get a map of query-type-keys and brands
		LinkedHashMap<String, String> queryTypeKeysBrandsMap = buildQueryTypeKeysBrandsMap(queryTypePerProjectElementsMap);
		// get posts universe and date for each brand
		Map<String, List<Pair<BigDecimal, String>>> postsUniversesAndDates = new HashMap<>();
		//averages, date_max, date_min, highest, lowest, total for each brand/project
		Map<String, BigDecimal> averages = new HashMap<>();
		Map<String, String> datesMax = new HashMap<>();
		Map<String, String> datesMin = new HashMap<>();
		Map<String, BigDecimal> highests = new HashMap<>();
		Map<String, BigDecimal> lowests = new HashMap<>();
		Map<String, BigDecimal> totals = new HashMap<>();
		//String mainBrandName = volumeResultsPerQueryType.get(INFEGY_MY_BRAND).
		
		// For each project
		queryTypePerProjectElements.forEach( (e) -> {
			
			BigDecimal sumOfPostsUniverse = volumeResultsPerQueryType.get(e.getQueryTypeKey())
												.getOutput()
												.stream()
												.map( VolumeOutputElementDTO::getPostsUniverse )
												.reduce( BigDecimal.ZERO, BigDecimal::add );
			BigDecimal countOfDays = BigDecimal.valueOf( volumeResultsPerQueryType.get(e.getQueryTypeKey()).getOutput().size() );
			BigDecimal avgOfPostsUniverse = sumOfPostsUniverse.divide(countOfDays, 2, RoundingMode.HALF_UP);
			
			VolumeOutputElementDTO max = volumeResultsPerQueryType.get(e.getQueryTypeKey())
												.getOutput()
												.stream()
												.max((o1,o2) -> o1.getPostsUniverse().compareTo(o2.getPostsUniverse()) )
												.get();
			VolumeOutputElementDTO min = volumeResultsPerQueryType.get(e.getQueryTypeKey())
					.getOutput()
					.stream()
					.max((o1,o2) -> -o1.getPostsUniverse().compareTo(o2.getPostsUniverse()) )
					.get();
			BigDecimal maxValue = max.getPostsUniverse();
			BigDecimal minValue = min.getPostsUniverse();
			String dateStrMax = max.getDate();
			String dateStrMin = min.getDate();
			
			averages.put(e.getQueryTypeKey(), avgOfPostsUniverse);
			datesMax.put(e.getQueryTypeKey(), dateStrMax);
			datesMin.put(e.getQueryTypeKey(), dateStrMin);
			highests.put(e.getQueryTypeKey(), maxValue);
			lowests.put(e.getQueryTypeKey(), minValue);
			totals.put(e.getQueryTypeKey(), sumOfPostsUniverse);
			
			volumeResultsPerQueryType.get(e.getQueryTypeKey()).getOutput().forEach((output) -> {
				List<Pair<BigDecimal, String>> list;
				if(postsUniversesAndDates.containsKey(e.getQueryTypeKey())){
					list = postsUniversesAndDates.get(e.getQueryTypeKey());
				} else {
					list = new ArrayList<Pair<BigDecimal, String>>();
					postsUniversesAndDates.put(e.getQueryTypeKey(),list);
				}
				list.add(Pair.of(output.getPostsUniverse(), output.getDate()));
			});
			
			log.info("QUERY-TYPE: " + e.getQueryTypeKey());
			log.info("BRAND: " + e.getBrandName());
			log.info("TOTAL POSTS: " + sumOfPostsUniverse.toString());
			
		} );
		
		PeopleMentionedCompetitorsCalculationResult result = new PeopleMentionedCompetitorsCalculationResult();
		
		result.setQueryTypePerProjectElements(queryTypePerProjectElements);
		result.setQueryTypeKeysBrandsMap(queryTypeKeysBrandsMap);
		
		result.setAverages(averages);
		result.setDatesMax(datesMax);
		result.setDatesMin(datesMin);
		result.setHighests(highests);
		result.setLowests(lowests);
		result.setTotals(totals);
		result.setPostsUniversesAndDates(postsUniversesAndDates);
		
		return result;
	}
	
	private LinkedHashMap<String, String> buildQueryTypeKeysBrandsMap(
			Map<String, QueryTypePerProjectViewElement> queryTypePerProjectElementsMap) {
		
		List<Pair<String,String>> queryTypeBrands = new ArrayList<>();
		String myBrandName = queryTypePerProjectElementsMap.get(INFEGY_MY_BRAND).getBrandName();
		
		queryTypeBrands.add( Pair.of( INFEGY_MY_BRAND, myBrandName ) );
		queryTypeBrands.addAll(
			queryTypePerProjectElementsMap.values()
				.stream()
				.filter(  (e) -> e.getQueryTypeKey().compareTo(INFEGY_MY_BRAND) != 0 )
				.map( (e) -> {
					return Pair.of( e.getQueryTypeKey() , e.getBrandName() );
				} )
				.sorted( (e1,e2) -> e1.getRight().compareTo(e2.getRight()) )
				.collect( Collectors.toList() )
		);
		
		LinkedHashMap<String, String> queryTypeKeysBrandsMap = new LinkedHashMap<>();
		queryTypeBrands.forEach( (p) -> {
			queryTypeKeysBrandsMap.put(p.getKey(), p.getValue());
		} );
		
		return queryTypeKeysBrandsMap;
	}

}
