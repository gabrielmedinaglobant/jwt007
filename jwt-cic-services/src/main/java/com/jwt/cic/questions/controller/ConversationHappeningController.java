package com.jwt.cic.questions.controller;

import com.jwt.cic.questions.dto.ConversationHappeningDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.service.ConversationHappeningService;
import com.jwt.cic.questions.util.BlockType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for Question "Where is the conversation happening?"
 *
 * @author gabriel.medina
 */
@RestController
@RequestMapping("api/v1/questions/conversation-happening")
public class ConversationHappeningController {


    private ConversationHappeningService conversationHappeningService;

    @Autowired
    public ConversationHappeningController(ConversationHappeningService conversationHappeningService) {
        this.conversationHappeningService = conversationHappeningService;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/", ""})
    public QuestionSingleResultContainerDTO get(@RequestParam(value = "project-id", defaultValue = "-1") long projectId) {        
        if(projectId > 0){
			return conversationHappeningService.get(projectId);
		}
		else{
			return conversationHappeningService.get();
		}
    }


}
