package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class ImmigrationGenderIndicatorDTO {
	private String key;
	private BigDecimal value;
	private String unit;
	private String title;
	
	
	public ImmigrationGenderIndicatorDTO() {
	}
	public ImmigrationGenderIndicatorDTO(String key, BigDecimal value, String unit, String title) {
		this.key = key;
		this.value = value;
		this.unit = unit;
		this.title = title;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
