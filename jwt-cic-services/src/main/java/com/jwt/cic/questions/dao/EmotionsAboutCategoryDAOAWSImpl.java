package com.jwt.cic.questions.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.questions.dto.EmotionsAboutCategoryEtlResultDTO;
import com.jwt.cic.utils.ResourceFetcher;

@Primary
@Repository
public class EmotionsAboutCategoryDAOAWSImpl implements EmotionsAboutCategoryDAO {

    private ObjectMapper mapper = new ObjectMapper();

    private ResourceFetcher resourceFetcher;
    @Value("${question.s3.bucket}")
    private String bucketLocation;
    @Value("${question.s3.bucket.emotions-about-category.json}")
    private String resourceLocation;

    @Autowired
    public EmotionsAboutCategoryDAOAWSImpl(ResourceFetcher resourceFetcher) {
        this.resourceFetcher = resourceFetcher;
    }
    
    public EmotionsAboutCategoryEtlResultDTO get(String projectUuid) {        
        return fetchData(bucketLocation + projectUuid + "/output/" + resourceLocation);
    }
    
    protected final EmotionsAboutCategoryEtlResultDTO fetchData(String location){
    	return resourceFetcher.fetchObject(location, EmotionsAboutCategoryEtlResultDTO.class);
    }

}
