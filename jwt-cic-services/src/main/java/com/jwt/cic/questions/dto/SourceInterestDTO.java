package com.jwt.cic.questions.dto;

import java.math.BigDecimal;

public class SourceInterestDTO implements QuestionResultDTO {
	private String topic;
	private String subtopic;
	private BigDecimal distributionValue;
	private String distributionUnit;
	private BigDecimal ratioValue;
	private String ratioUnit;
	private BigDecimal ratioPercentageValue;
	private String ratioPercentageUnit;
	
	public SourceInterestDTO() {

	}
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getSubtopic() {
		return subtopic;
	}
	public void setSubtopic(String subtopic) {
		this.subtopic = subtopic;
	}
	public BigDecimal getDistributionValue() {
		return distributionValue;
	}
	public void setDistributionValue(BigDecimal distributionValue) {
		this.distributionValue = distributionValue;
	}
	public String getDistributionUnit() {
		return distributionUnit;
	}
	public void setDistributionUnit(String distributionUnit) {
		this.distributionUnit = distributionUnit;
	}
	public BigDecimal getRatioValue() {
		return ratioValue;
	}
	public void setRatioValue(BigDecimal ratioValue) {
		this.ratioValue = ratioValue;
	}
	public String getRatioUnit() {
		return ratioUnit;
	}
	public void setRatioUnit(String ratioUnit) {
		this.ratioUnit = ratioUnit;
	}

	public BigDecimal getRatioPercentageValue() {
		return ratioPercentageValue;
	}

	public void setRatioPercentageValue(BigDecimal ratioPercentageValue) {
		this.ratioPercentageValue = ratioPercentageValue;
	}

	public String getRatioPercentageUnit() {
		return ratioPercentageUnit;
	}

	public void setRatioPercentageUnit(String ratioPercentageUnit) {
		this.ratioPercentageUnit = ratioPercentageUnit;
	}

	
}
