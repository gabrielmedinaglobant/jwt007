package com.jwt.cic.questions.service;

import com.jwt.cic.charts.dto.*;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.questions.dao.PeopleLookedCompetitorsDAO;
import com.jwt.cic.questions.dto.BrandIndicatorDTO;
import com.jwt.cic.questions.dto.DisplayableDTO;
import com.jwt.cic.questions.dto.PeopleLookedCompetitorsDTO;
import com.jwt.cic.questions.dto.PeopleLookedCompetitorsIndicatorDTO;
import com.jwt.cic.questions.dto.QuestionSingleResultContainerDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import com.jwt.cic.utils.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by vishal.domale on 24-04-2017.
 */

@Service
public class PeopleLookedCompetitorsServiceImpl implements PeopleLookedCompetitorsService {


    private PeopleLookedCompetitorsDAO peopleLookedCompetitorsDAO;
    private QuestionResultContainerBuilder questionResultContainerBuilder;
    
    private final String DATE_KEY = "Day";


    @Autowired
    public PeopleLookedCompetitorsServiceImpl(PeopleLookedCompetitorsDAO peopleLookedCompetitorsDAO, QuestionResultContainerBuilder questionResultContainerBuilder) {
        this.peopleLookedCompetitorsDAO = peopleLookedCompetitorsDAO;
        this.questionResultContainerBuilder = questionResultContainerBuilder;
    }

    @Override
    public QuestionSingleResultContainerDTO get(String[] brands)  {
        if(brands==null || brands.length==0){
            return  null;
        }
        List<Map<String, String>> etlResult = peopleLookedCompetitorsDAO.get(brands);

        if(etlResult.isEmpty()){
         throw new InvalidJsonFromDataSourceException();
        }
        return questionResultContainerBuilder.buildDTO( getLastUpdatedDate() , buildDTO(etlResult,brands));
    }

    protected final long getLastUpdatedDate(){
    	return System.currentTimeMillis()/1000;
    }
    
    private PeopleLookedCompetitorsDTO buildDTO(List<Map<String, String>> etlResult,String[]  brands) {
        PeopleLookedCompetitorsDTO dto = new PeopleLookedCompetitorsDTO();
        String firstBrand = brands[0];
        dto.setBrands(buildBrands(brands));
        dto.setSplineChart(buildSplineChart(etlResult, brands));
        dto.setBarChart(buildBarChart(etlResult, brands));
        dto.setIndicators(buildIndicators(etlResult, firstBrand));
        return dto;
    }

    private List<DisplayableDTO> buildBrands(String[]  brands) {
        return Arrays.asList(brands).stream().map(
                (brand) -> {
                    return new BrandIndicatorDTO(brand, brand);
                }
        ).collect(Collectors.toList());
    }

    private SplineChartDTO buildSplineChart(List<Map<String, String>> etlResult, String[] brands) {
        SplineChartDTO dto = new SplineChartDTO();
        dto.setLines(Arrays.asList(brands).stream().map((value) -> {
            return new SplineChartLineDTO(value, buildSplineChartLines(etlResult, value));
        }).collect(Collectors.toList()));
        return dto;
    }

    private List<ArrayValueDTO> buildSplineChartLines(List<Map<String, String>> etlResult, String brand) {
        List<ArrayValueDTO> valueList = new ArrayList<ArrayValueDTO>();
        for (Map<String, String> brandMap : etlResult) {
            if (brandMap.containsKey(brand)) {
                String dateString=brandMap.get(DATE_KEY);
                String valueString = brandMap.get(brand);
                valueList.add(new ArrayValueDTO(new BigDecimal[]{
                        DateConverter.fromUSHyphenDateToEpochSecondsAsBigDecimal(dateString), new BigDecimal(valueString).setScale(0, RoundingMode.HALF_UP)
                }));
            }
        }
        return valueList;
    }

    private SingleBarChartDTO buildBarChart(List<Map<String, String>> etlResult, String[] brands) {
        SingleBarChartDTO dto = new SingleBarChartDTO();
        dto.setValues(Arrays.asList(brands).stream().map((value) -> {
            return new SingleValueDTO(value, getBrandAvgValue(etlResult, value));
        }).collect(Collectors.toList()));
        return dto;


    }

    private BigDecimal getBrandAvgValue(List<Map<String, String>> etlResult, String brand) {
        BigDecimal avgDecimal=etlResult.stream().map(
                map->{
                    if(!map.containsKey(brand)){
                        return   BigDecimal.ZERO;
                    }
                    return new BigDecimal(map.get(brand));
                }
        ).reduce( BigDecimal.ZERO , BigDecimal::add).divide(new BigDecimal(etlResult.size()),0,RoundingMode.HALF_UP);
        return avgDecimal;
    }

    private List<PeopleLookedCompetitorsIndicatorDTO> buildIndicators(List<Map<String, String>> etlResult, String firstBrand) {
        List<PeopleLookedCompetitorsIndicatorDTO> dtoList = new ArrayList<>();
       TreeSet<BigDecimal> valueSet=new TreeSet<>( etlResult.stream().map(
                map->{
                    if(!map.containsKey(firstBrand)){
                        return   BigDecimal.ZERO;
                    }
                    return new BigDecimal(map.get(firstBrand));
                }
        ).collect(Collectors.toList()));
        BigDecimal minDecimal = valueSet.first();
        BigDecimal maxDecimal = valueSet.last();
        dtoList.add(new PeopleLookedCompetitorsIndicatorDTO("maximum", maxDecimal, "%", "Maximum"));
        dtoList.add(new PeopleLookedCompetitorsIndicatorDTO("minimum", minDecimal, "%", "Minimum"));
        return dtoList;
    }
}
