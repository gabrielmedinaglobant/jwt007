/*
package com.jwt.cic;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.jwt.cic.exceptions.InvalidQueryParameter;
import com.jwt.cic.questions.util.BlockType;

@Configuration
public class CustomWebMvcConfigurationSupport extends WebMvcConfigurationSupport {
	
	@Override
	public FormattingConversionService mvcConversionService() {
		FormattingConversionService f = super.mvcConversionService();
		
		f.addConverter(new Converter<String, BlockType>(){
			@Override
			public BlockType convert(String arg0) {
				BlockType value;
				try{
					value = BlockType.getByQueryParamValue(arg0);
				} catch(IllegalArgumentException | NullPointerException e){
					throw new InvalidQueryParameter();
				}
				return value;
			}
		});
		
		return f;
	}
}

TODO: CHECK HOW NOT TO OVERRIDE SWAGGER!!!

*/