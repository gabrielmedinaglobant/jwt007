package com.jwt.cic.thirdparty.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="SourceCrenditial entity not found") 
public class SourceCredentialNotFoundException extends RuntimeException{
	
}
