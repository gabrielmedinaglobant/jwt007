package com.jwt.cic.thirdparty.dao;

import org.springframework.data.repository.CrudRepository;

import com.jwt.cic.thirdparty.domain.Source;

public interface SourceDAO extends CrudRepository<Source, Long>{

}
