package com.jwt.cic.thirdparty.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.thirdparty.dao.SourceCredentialDAO;
import com.jwt.cic.thirdparty.domain.SourceCredential;
import com.jwt.cic.thirdparty.dto.SourceCredentialDTO;
import com.jwt.cic.thirdparty.exceptions.SourceCredentialNotFoundException;

@Service
public class SourceCredentialServiceImpl implements SourceCredentialService{

	private SourceCredentialDAO sourceCredentialDAO;
	
	@Autowired
	public SourceCredentialServiceImpl(SourceCredentialDAO sourceCredentialDAO) {
		this.sourceCredentialDAO = sourceCredentialDAO;
	}

	@Override
	public List<SourceCredentialDTO> getAllByUserNameId(String userNameId) {
		return assembleDTOList( sourceCredentialDAO.findAllByUserNameId(userNameId) );
	}

	@Override
	public SourceCredentialDTO getByUserNameIdAndSourceKeyname(String userNameId, String sourceKeyname) {
		SourceCredential sourceCredential = sourceCredentialDAO.findOneByUserNameIdAndSourceKeyname(userNameId, sourceKeyname);
		if(sourceCredential == null)
			throw new SourceCredentialNotFoundException();
		
		return mapToDTO(sourceCredential);
	}

	@Override
	public SourceCredentialDTO update(SourceCredentialDTO sourceCredentialDTO, String userNameId, String sourceKeyname) {
		SourceCredential sourceCredential = sourceCredentialDAO.findOneByUserNameIdAndSourceKeyname(userNameId, sourceKeyname);
		if(sourceCredential == null)
			throw new SourceCredentialNotFoundException();
		
		if( sourceCredentialDTO.getPassword() != null ){
			sourceCredential.setPassword(sourceCredentialDTO.getPassword());
		}
		
		sourceCredential = sourceCredentialDAO.save( sourceCredential );
		
		return mapToDTO( sourceCredential );
	}

	private List<SourceCredentialDTO> assembleDTOList(List<SourceCredential> domainObjList){
		return domainObjList
				.stream()
				.map( this::mapToDTO )
				.collect( Collectors.toList() );
	}
	
	private SourceCredentialDTO mapToDTO(SourceCredential domainObj){
		SourceCredentialDTO dto = new SourceCredentialDTO();
		
		dto.setId( domainObj.getId() );
		dto.setSourceId( domainObj.getSource().getId() );
		dto.setSourceKeyname( domainObj.getSource().getKeyname() );
		dto.setUserNameId( domainObj.getUser().getNameId() );
		dto.setUsername( domainObj.getUsername() );
		dto.setPassword( domainObj.getPassword() );
		
		return dto;
	}
	
	
	
}
