package com.jwt.cic.thirdparty.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Source {

	@Id
	private long id;
	private String keyname;
	private String name;
	private boolean active;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "source")
	private List<SourceCredential> sourceCredentials;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<SourceCredential> getSourceCredentials() {
		return sourceCredentials;
	}
	public void setSourceCredentials(List<SourceCredential> sourceCredentials) {
		this.sourceCredentials = sourceCredentials;
	}
	
}
