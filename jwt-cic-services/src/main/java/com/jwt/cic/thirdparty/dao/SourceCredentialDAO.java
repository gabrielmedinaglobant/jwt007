package com.jwt.cic.thirdparty.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.thirdparty.domain.SourceCredential;

public interface SourceCredentialDAO extends CrudRepository<SourceCredential, Long>{
	@Query("select sc from SourceCredential sc join fetch sc.user u where u.nameId = :userNameId and sc.active = true")
	List<SourceCredential> findAllByUserNameId(@Param("userNameId") String userNameId);
	@Query("select sc from SourceCredential sc join fetch sc.user u join fetch sc.source s where u.nameId = :userNameId and s.keyname = :sourceKeyname and sc.active = true")
	SourceCredential findOneByUserNameIdAndSourceKeyname(@Param("userNameId") String userNameId, @Param("sourceKeyname") String sourceKeyname);
}
