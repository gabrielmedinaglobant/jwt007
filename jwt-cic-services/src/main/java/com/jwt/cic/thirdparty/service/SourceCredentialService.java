package com.jwt.cic.thirdparty.service;

import java.util.List;

import com.jwt.cic.thirdparty.dto.SourceCredentialDTO;

public interface SourceCredentialService {
	List<SourceCredentialDTO> getAllByUserNameId(String userNameId);
	SourceCredentialDTO getByUserNameIdAndSourceKeyname(String userNameId, String sourceKeyname);
	SourceCredentialDTO update(SourceCredentialDTO sourceCredentialDTO, String userNameId, String sourceKeyname);
}
