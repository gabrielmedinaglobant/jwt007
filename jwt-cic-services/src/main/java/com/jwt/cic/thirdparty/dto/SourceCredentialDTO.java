package com.jwt.cic.thirdparty.dto;

public class SourceCredentialDTO {
	private long id;
	private String username;
	private String password;
	
	private String userNameId;
	private long sourceId;
	private String sourceKeyname;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(String userNameId) {
		this.userNameId = userNameId;
	}
	public long getSourceId() {
		return sourceId;
	}
	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}
	public String getSourceKeyname() {
		return sourceKeyname;
	}
	public void setSourceKeyname(String sourceKeyname) {
		this.sourceKeyname = sourceKeyname;
	}
	
	
}
