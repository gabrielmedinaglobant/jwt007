package com.jwt.cic.thirdparty.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.thirdparty.dto.SourceCredentialDTO;
import com.jwt.cic.thirdparty.service.SourceCredentialService;

@RestController
@RequestMapping("api/v1/users/{user-name-id}/source-credentials")
public class SourceCredentialController {

	private SourceCredentialService sourceCredentialService;

	@Autowired
	public SourceCredentialController(SourceCredentialService sourceCredentialService) {
		this.sourceCredentialService = sourceCredentialService;
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public List<SourceCredentialDTO> getAllByUserNameId(@PathVariable("user-name-id") String userNameId) {
		return sourceCredentialService.getAllByUserNameId(userNameId);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/{source-keyname}" })
	public SourceCredentialDTO getByUserNameIdAndSourceKeyname(
			@PathVariable("user-name-id") String userNameId,
			@PathVariable("source-keyname") String sourceKeyname) {
		return sourceCredentialService.getByUserNameIdAndSourceKeyname(userNameId, sourceKeyname);

	}

	@RequestMapping(method = RequestMethod.POST, value = { "/{source-keyname}" })
	public SourceCredentialDTO update(
			@RequestBody SourceCredentialDTO sourceCredentialDTO,
			@PathVariable("user-name-id") String userNameId, 
			@PathVariable("source-keyname") String sourceKeyname) {
		return sourceCredentialService.update(sourceCredentialDTO, userNameId, sourceKeyname);

	}
}
