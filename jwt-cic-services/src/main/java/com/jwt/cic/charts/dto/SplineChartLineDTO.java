package com.jwt.cic.charts.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SplineChartLineDTO {
	private String key;
	private List<ArrayValueDTO> values;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String unit;
	
	public SplineChartLineDTO() {
	}
	public SplineChartLineDTO(String key, List<ArrayValueDTO> values) {
		this.key = key;
		this.values = values;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<ArrayValueDTO> getValues() {
		return values;
	}

	public void setValues(List<ArrayValueDTO> values) {
		this.values = values;
	}
	
}
