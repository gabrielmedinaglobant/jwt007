package com.jwt.cic.charts.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SingleBarChartDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String key;
	private List<SingleValueDTO> values;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String unit;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<SingleValueDTO> getValues() {
		return values;
	}
	public void setValues(List<SingleValueDTO> values) {
		this.values = values;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
}
