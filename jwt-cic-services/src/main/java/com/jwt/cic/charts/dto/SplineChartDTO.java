package com.jwt.cic.charts.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SplineChartDTO {
	private List<SplineChartLineDTO> lines;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SingleValueDTO> horizontalMarkers;
	
	public List<SplineChartLineDTO> getLines() {
		return lines;
	}

	public void setLines(List<SplineChartLineDTO> lines) {
		this.lines = lines;
	}

	public List<SingleValueDTO> getHorizontalMarkers() {
		return horizontalMarkers;
	}

	public void setHorizontalMarkers(List<SingleValueDTO> horizontalMarkers) {
		this.horizontalMarkers = horizontalMarkers;
	}
	
	
}
