package com.jwt.cic.charts.dto;

import java.util.List;

public class SplineChartSingleValuesDTO {
	private List<ArrayValueDTO> values;

	public List<ArrayValueDTO> getValues() {
		return values;
	}

	public void setValues(List<ArrayValueDTO> values) {
		this.values = values;
	}
	
}
