package com.jwt.cic.charts.dto;

import java.math.BigDecimal;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ArrayValueDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String key;
	private BigDecimal[] value;
	
	public ArrayValueDTO() {
	}
	
	public ArrayValueDTO(BigDecimal[] value) {
		this.value = value;
	}
	
	public ArrayValueDTO(String key, BigDecimal[] value) {
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public BigDecimal[] getValue() {
		return value;
	}
	public void setValue(BigDecimal[] value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + Arrays.hashCode(value);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArrayValueDTO other = (ArrayValueDTO) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (!Arrays.equals(value, other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArrayValueDTO [" + (key != null ? "key=" + key + ", " : "")
				+ (value != null ? "value=" + Arrays.toString(value) : "") + "]";
	}
	
}
