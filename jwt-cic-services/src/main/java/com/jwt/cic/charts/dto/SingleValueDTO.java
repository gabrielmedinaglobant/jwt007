package com.jwt.cic.charts.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SingleValueDTO {
	private String key;
	private BigDecimal value;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String displayName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal indicatorValue;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String indicatorUnit;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String tag;
	
	public SingleValueDTO() {
	}
	public SingleValueDTO(String key, BigDecimal value) {
		this.key = key;
		this.value = value;
	}
	public SingleValueDTO(String key, BigDecimal value, String displayName) {
		this.key = key;
		this.value = value;
		this.displayName = displayName;
	}
	public SingleValueDTO(String key, BigDecimal value, String displayName, BigDecimal indicatorValue,
			String indicatorUnit) {
		this.key = key;
		this.value = value;
		this.displayName = displayName;
		this.indicatorValue = indicatorValue;
		this.indicatorUnit = indicatorUnit;
	}
	public SingleValueDTO(String key, BigDecimal value, String displayName, BigDecimal indicatorValue,
			String indicatorUnit, String tag) {
		this.key = key;
		this.value = value;
		this.displayName = displayName;
		this.indicatorValue = indicatorValue;
		this.indicatorUnit = indicatorUnit;
		this.tag = tag;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getIndicatorValue() {
		return indicatorValue;
	}

	public void setIndicatorValue(BigDecimal indicatorValue) {
		this.indicatorValue = indicatorValue;
	}

	public String getIndicatorUnit() {
		return indicatorUnit;
	}

	public void setIndicatorUnit(String indicatorUnit) {
		this.indicatorUnit = indicatorUnit;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
}
