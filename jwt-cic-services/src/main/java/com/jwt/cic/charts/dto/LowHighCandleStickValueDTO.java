package com.jwt.cic.charts.dto;

import java.math.BigDecimal;

public class LowHighCandleStickValueDTO implements CandleStickValueDTO {
	private BigDecimal low;
	private BigDecimal relativeLow;
	private BigDecimal high;
	private BigDecimal relativeHigh;
	private CandleStickTrend trend;
	private BigDecimal dateValue;
	private BigDecimal value;
	private String dateAsStringValue;
	
	public LowHighCandleStickValueDTO(BigDecimal low, BigDecimal relativeLow, BigDecimal high,
			BigDecimal relativeHigh, CandleStickTrend trend, BigDecimal dateValue, 
			BigDecimal value) {
		this.low = low;
		this.relativeLow = relativeLow;
		this.high = high;
		this.relativeHigh = relativeHigh;
		this.trend = trend;
		this.dateValue = dateValue;
		this.value = value;
	}

	public LowHighCandleStickValueDTO() {
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getRelativeLow() {
		return relativeLow;
	}

	public void setRelativeLow(BigDecimal relativeLow) {
		this.relativeLow = relativeLow;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getRelativeHigh() {
		return relativeHigh;
	}

	public void setRelativeHigh(BigDecimal relativeHigh) {
		this.relativeHigh = relativeHigh;
	}

	public CandleStickTrend getTrend() {
		return trend;
	}

	public void setTrend(CandleStickTrend trend) {
		this.trend = trend;
	}

	public BigDecimal getDateValue() {
		return dateValue;
	}

	public void setDateValue(BigDecimal dateValue) {
		this.dateValue = dateValue;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String getDateAsStringValue() {
		// TODO Auto-generated method stub
		return dateAsStringValue;
	}

	@Override
	public void setDateAsStringValue(String dateAsStringValue) {
		this.dateAsStringValue = dateAsStringValue;
		
	}

	
	
}

