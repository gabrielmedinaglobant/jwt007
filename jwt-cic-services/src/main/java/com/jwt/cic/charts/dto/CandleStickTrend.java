package com.jwt.cic.charts.dto;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.jwt.cic.questions.util.BlockType;


public enum CandleStickTrend {
	UP("up"), DOWN("down");
	
	private String value;
	private static final Map<String, CandleStickTrend> valueMap = Collections.unmodifiableMap(initializeValueMap());
	
	private CandleStickTrend(String value){
		this.value = value;
	}
	
	private static Map<String, CandleStickTrend> initializeValueMap(){
		Map<String, CandleStickTrend> valueMap = new HashMap<>();
		for (CandleStickTrend ct : CandleStickTrend.values()) {
			valueMap.put(ct.value, ct);
	    }
		return valueMap;
	}
	
	@JsonCreator
	public static CandleStickTrend getByValue(String value){
		if(!valueMap.containsKey(value)){
			throw new IllegalArgumentException();
		}
		return valueMap.get(value);
	}

    @JsonValue
    public String toValue() {
        for (Entry<String, CandleStickTrend> entry : valueMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null; // fail
    }
	
}
