package com.jwt.cic.charts.dto;

import java.util.List;

public class CandleStickChartDTO {
	private List<? extends CandleStickValueDTO> line;

	public List<? extends CandleStickValueDTO> getLine() {
		return line;
	}
	public void setLine(List<? extends CandleStickValueDTO> line) {
		this.line = line;
	}
	
}
