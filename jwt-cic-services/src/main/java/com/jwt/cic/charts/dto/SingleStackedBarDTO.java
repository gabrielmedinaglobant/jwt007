package com.jwt.cic.charts.dto;

import java.math.BigDecimal;
import java.util.List;

public class SingleStackedBarDTO {
	private List<SingleValueDTO> values;
	private BigDecimal indicator;
	private String indicatorUnit;
	
	public List<SingleValueDTO> getValues() {
		return values;
	}
	public void setValues(List<SingleValueDTO> values) {
		this.values = values;
	}
	public BigDecimal getIndicator() {
		return indicator;
	}
	public void setIndicator(BigDecimal indicator) {
		this.indicator = indicator;
	}
	public String getIndicatorUnit() {
		return indicatorUnit;
	}
	public void setIndicatorUnit(String indicatorUnit) {
		this.indicatorUnit = indicatorUnit;
	}
	
	
}
