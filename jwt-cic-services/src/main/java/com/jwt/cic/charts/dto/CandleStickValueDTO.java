package com.jwt.cic.charts.dto;

import java.math.BigDecimal;

public interface CandleStickValueDTO {
	BigDecimal getLow();
	void setLow(BigDecimal low);
	BigDecimal getHigh();
	void setHigh(BigDecimal high);
	CandleStickTrend getTrend();
	void setTrend(CandleStickTrend trend);
	String getDateAsStringValue();
	void setDateAsStringValue(String dateAsStringValue);
	BigDecimal getDateValue();
	void setDateValue(BigDecimal dateValue);
	BigDecimal getValue();
	void setValue(BigDecimal value);
}