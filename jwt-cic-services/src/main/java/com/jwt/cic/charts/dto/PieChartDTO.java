package com.jwt.cic.charts.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class PieChartDTO {
	private List<SingleValueDTO> values;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal indicator;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String indicatorUnit;
	
	public List<SingleValueDTO> getValues() {
		return values;
	}
	public void setValues(List<SingleValueDTO> values) {
		this.values = values;
	}
	public BigDecimal getIndicator() {
		return indicator;
	}
	public void setIndicator(BigDecimal indicator) {
		this.indicator = indicator;
	}
	public String getIndicatorUnit() {
		return indicatorUnit;
	}
	public void setIndicatorUnit(String indicatorUnit) {
		this.indicatorUnit = indicatorUnit;
	}
	
	
}
