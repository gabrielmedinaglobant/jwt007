package com.jwt.cic.clients.service;

import java.util.List;
import java.util.Set;

import com.jwt.cic.clients.dto.ProjectDTO;
import com.jwt.cic.clients.dto.ProjectExistsResponseDTO;
import com.jwt.cic.clients.dto.ProjectSaveDTO;
import com.jwt.cic.clients.dto.ProjectSetAssignedUserDTO;
import com.jwt.cic.clients.dto.ProjectUpdateDTO;
import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;

public interface ProjectService {
	List<ProjectDTO> getAll(String clientName, String brandName, String projectName);
	List<UserProjectDTO> getAssignedUsers(long projectId);
	ProjectSaveDTO save(ProjectSaveDTO saveDTO);
	ProjectUpdateDTO update(long projectId, ProjectUpdateDTO saveDTO);
	void delete(long projectId);
	ProjectSetAssignedUserDTO setAssignedUser(long projectId, ProjectSetAssignedUserDTO projectSetAssignUsersDTO);
	void unassignUser(long projectId, String userNameId);
	void unassignManyUsers(long projectId, Set<String> userNameId);
	List<UserDTO> getUnassignedUsers(long projectId, String userNameId,String firstName,String lastName,String email);
	ProjectExistsResponseDTO exists(long brandId);
}
