package com.jwt.cic.clients.service;

import java.util.List;

import com.jwt.cic.clients.dto.BrandDTO;

public interface BrandService {
	List<BrandDTO> getAll();
	List<BrandDTO> findAllByNameAndClientName(String name, String clientName);
	List<BrandDTO> findAllByNameAndClientId(String name, long clientId);
	BrandDTO save(BrandDTO brandDTO);
	BrandDTO update(long id, BrandDTO brandDTO);
	void delete(long id);
	boolean nameExists(String name, long clientId);
	BrandDTO get(long id);
}
