package com.jwt.cic.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class BrandDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	private String uuid;
	private String name;
	private long clientId;
	private String clientName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getClientId() {
		return clientId;
	}
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
}
