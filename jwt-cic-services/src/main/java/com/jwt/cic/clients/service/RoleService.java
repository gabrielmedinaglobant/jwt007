package com.jwt.cic.clients.service;

import java.util.List;

import com.jwt.cic.clients.dto.RoleDTO;

public interface RoleService {
	public List<RoleDTO> getAll();
}
