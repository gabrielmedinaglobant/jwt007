package com.jwt.cic.clients.dao;

import com.jwt.cic.clients.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface UserDAO extends CrudRepository<User, Long> {
    public List<User> findAll();

    public User findOneByNameId(String nameId);

    @Query("select u from User u where u.id not in (:ids) and lower(u.nameId) like lower(concat(:nameId,'%')) and lower(u.name) like lower(concat(:name,'%')) and lower(u.lastName) like lower(concat(:lastName,'%')) and lower(u.email) like lower(concat(:email,'%')) and u.active = true")
    public List<User> findAllByIdNotIn(@Param("ids") Set<Long> ids, @Param("nameId") String nameId, @Param("name") String name, @Param("lastName") String lastName, @Param("email") String email);

    @Query("select u from User u where lower(u.name) like lower(concat(:name,'%')) and lower(u.lastName) like lower(concat(:lastName,'%')) and lower(u.email) like lower(concat(:email,'%')) and u.active = true")
    public List<User> findAllByNameAndLastNameAndEmail(@Param("name") String name, @Param("lastName") String lastName, @Param("email") String email);

    @Query("select u from User u left outer join fetch u.userProjects up where u.nameId = :nameId and u.active = true")
    public User findOneWithProjectsByNameId(@Param("nameId") String nameId);

    @Query("select u from User u where lower(u.name) like lower(concat(:query,'%')) or lower(u.lastName) like lower(concat(:query,'%')) or lower(u.email) like lower(concat(:query,'%')) and u.active = true")
    public List<User> findAllByNameOrLastNameOrEmail(@Param("query")String query);

}
