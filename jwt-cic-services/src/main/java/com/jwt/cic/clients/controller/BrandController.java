package com.jwt.cic.clients.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.clients.dto.BrandDTO;
import com.jwt.cic.clients.service.BrandService;

/***
 * 
 * Controller for Brand (Client Mapping)
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/brands")
public class BrandController {

	private BrandService brandService;
	
	@Autowired
	public BrandController(BrandService brandService) {
		this.brandService = brandService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/{id}"})
	public BrandDTO get(@PathVariable long id){
		return brandService.get(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public List<BrandDTO> getAll(
			@RequestParam(value = "name", defaultValue = "", required = false) String name,
			@RequestParam(value = "client-id", required = false) Long clientId,
			@RequestParam(value = "client-name", defaultValue = "", required = false) String clientName) {
		
		List<BrandDTO> result = new ArrayList<>();
		
		if(clientId != null) {
			result = brandService.findAllByNameAndClientId(name, clientId);
		}
		else if(clientName.compareToIgnoreCase("") != 0) {
			result = brandService.findAllByNameAndClientName(name, clientName);
		}
		else {
			result = brandService.getAll();
		}
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/",""})
	public BrandDTO save(@RequestBody BrandDTO brandDTO){
		return brandService.save(brandDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/{id}"})
	public BrandDTO update(@PathVariable long id, @RequestBody BrandDTO brandDTO){
		return brandService.update(id, brandDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{id}"})
	public void delete(@PathVariable long id){
		brandService.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/exists"})
	public boolean nameExists(@RequestParam(value = "client-id", required = true) long clientId,
							@RequestParam(value = "name", required=true) String name){
		return brandService.nameExists(name, clientId);
	}
	
	
}
