package com.jwt.cic.clients.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.clients.domain.Client;

public interface ClientDAO extends CrudRepository<Client, Long> {
	public List<Client> findAll();
	@Query("select c from Client c where lower(c.name) like lower(concat(:name,'%'))")
	public List<Client> findAllByNameLike(@Param("name") String name);
	@Query("select c from Client c where lower(c.name) like lower(:name)")
	public List<Client> findAllByNameIgnoreCase(@Param("name") String name);
}
