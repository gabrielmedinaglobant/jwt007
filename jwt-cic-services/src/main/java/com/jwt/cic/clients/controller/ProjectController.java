package com.jwt.cic.clients.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.clients.dto.*;
import com.jwt.cic.clients.service.ProjectService;

@RestController
@RequestMapping("api/v1/projects")
public class ProjectController {
	
	private ProjectService projectService;
	
	@Autowired
	public ProjectController(ProjectService projectService){
		this.projectService = projectService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public List<ProjectDTO> getAll(
			@RequestParam(value = "client-name", defaultValue = "", required=false) String clientName,
			@RequestParam(value = "brand-name", defaultValue = "", required=false) String brandName,
			@RequestParam(value = "name", defaultValue = "", required=false) String name){
		return projectService.getAll(clientName, brandName, name);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/",""})
	public ProjectSaveDTO save(@RequestBody ProjectSaveDTO saveDTO){
		return projectService.save(saveDTO);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/exists"})
	public ProjectExistsResponseDTO exists( @RequestParam(value = "brand-id", required=true) long brandId ){
		return projectService.exists(brandId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/{projectId}"})
	public ProjectUpdateDTO update(@PathVariable long projectId, @RequestBody ProjectUpdateDTO updateDTO){
		return projectService.update(projectId, updateDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{projectId}"})
	public void delete(@PathVariable long projectId){
		projectService.delete(projectId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/{projectId}/users"})
	public List<UserProjectDTO> getAssignedUsers(@PathVariable long projectId){
		return projectService.getAssignedUsers(projectId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/{projectId}/users"})
	public ProjectSetAssignedUserDTO setAssignedUser(@PathVariable long projectId, @RequestBody ProjectSetAssignedUserDTO projectSetAssignedUserDTO){
		return projectService.setAssignedUser(projectId, projectSetAssignedUserDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{projectId}/users/{userNameId:.+}"})
	@ResponseStatus(value = HttpStatus.OK)
	public void unassignUser(@PathVariable long projectId, @PathVariable String userNameId){
		projectService.unassignUser(projectId, userNameId);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{projectId}/users/unassign-many"})
	@ResponseStatus(value = HttpStatus.OK)
	public void unassignManyUsers(@PathVariable long projectId, @RequestBody Set<String> userNameIds){
		projectService.unassignManyUsers(projectId, userNameIds);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/{projectId}/users/unassigned"})
	public List<UserDTO> getUnassignedUsers(
			@PathVariable long projectId,
			@RequestParam(value = "user-name-id", defaultValue = "", required = false) String userNameId,
			@RequestParam(value = "first-name", defaultValue = "", required = false) String firstName,
			@RequestParam(value = "last-name", defaultValue = "", required = false) String lastName,
			@RequestParam(value = "email", defaultValue = "", required = false) String email){
		return projectService.getUnassignedUsers(projectId, userNameId,firstName,lastName,email);
	}
		
}
