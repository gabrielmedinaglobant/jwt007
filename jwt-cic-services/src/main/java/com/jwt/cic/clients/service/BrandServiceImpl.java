package com.jwt.cic.clients.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.clients.dao.BrandDAO;
import com.jwt.cic.clients.dao.ClientDAO;
import com.jwt.cic.clients.domain.Brand;
import com.jwt.cic.clients.domain.Client;
import com.jwt.cic.clients.dto.BrandDTO;
import com.jwt.cic.exceptions.BrandContainsProjectsExceptions;
import com.jwt.cic.exceptions.BrandNameAlreadyExistsException;
import com.jwt.cic.exceptions.BrandNotFoundException;
import com.jwt.cic.exceptions.ClientNotFoundException;
import com.jwt.cic.utils.RecordUtils;

@Service
public class BrandServiceImpl implements BrandService {

	private BrandDAO brandDAO;
	private ClientDAO clientDAO;
	
	@Autowired
	public BrandServiceImpl(BrandDAO brandDAO, ClientDAO clientDAO) {
		this.brandDAO = brandDAO;
		this.clientDAO = clientDAO;
	}

	@Override
	public BrandDTO get(long id) {
		Brand brand = brandDAO.findOne(id);
		if( brand == null || !brand.isActive() ){
			throw new BrandNotFoundException();
		}
		
		return mapToDTO(brand);
	}
	
	@Override
	public List<BrandDTO> getAll() {
		return buildClientDTOList( brandDAO.findAll().parallelStream().filter( (b) -> b.isActive() == true ).collect( Collectors.toList() ) );
	}

	@Override
	public List<BrandDTO> findAllByNameAndClientName(String name, String clientName){
		return buildClientDTOList( brandDAO.findAllByNameAndClientName(name, clientName).parallelStream().filter( (b) -> b.isActive() == true ).collect( Collectors.toList() ) );
	}
	
	@Override
	public List<BrandDTO> findAllByNameAndClientId(String name, long clientId) {
		return buildClientDTOList( brandDAO.findAllByNameILikeAndClientId(name, clientId).parallelStream().filter( (b) -> b.isActive() == true ).collect( Collectors.toList() ) );
	}
	
	private List<BrandDTO> buildClientDTOList( List<Brand> brands ){
		return brands.stream()
				.map( BrandServiceImpl::mapToDTO )
				.collect( Collectors.toList() );
	}
	
	@Override
	public BrandDTO save(BrandDTO brandDTO) {
		// We need to find if there is an active client
		long clientId = brandDTO.getClientId();
		Client client = clientDAO.findOne(clientId);
		if(client == null || !client.isActive()){
			throw new ClientNotFoundException();
		}
		//don't allow brands with the same name
		if(nameExists(brandDTO.getName(), brandDTO.getClientId())){
			throw new BrandNameAlreadyExistsException();
		}
		
		//proceed to create Brand
		Brand brand = new Brand();
		brand.setUuid(UUID.randomUUID().toString());
		brand.setClient(client);
		brand.setName(brandDTO.getName());
		RecordUtils.setCreateFlags(brand);
		brand = brandDAO.save(brand);
		
		return mapToDTO(brand);
	}

	@Override
	public BrandDTO update(long id, BrandDTO brandDTO) {
		Brand brand = brandDAO.findOne(id);
		if( brand == null || !brand.isActive() ){
			throw new BrandNotFoundException();
		}
		
		// We need to find if there is an active client
		long clientId = brandDTO.getClientId();
		Client client = clientDAO.findOne(clientId);
		if(client == null || !client.isActive()){
			throw new ClientNotFoundException();
		}
		//don't allow brands with the same name
		if(nameExists(brandDTO.getName(), brandDTO.getClientId())){
			throw new BrandNameAlreadyExistsException();
		}
		
		brand.setClient(client);
		brand.setName( brandDTO.getName() );
		RecordUtils.setUpdateFlags(brand);
		brand = brandDAO.save(brand);
		
		return mapToDTO(brand);
	}

	@Override
	public void delete(long id) {
		Brand brand = brandDAO.findOne(id);
		
		if( brand == null || !brand.isActive() ){
			throw new BrandNotFoundException();
		}
		if(containsProjects(brand)){
			throw new BrandContainsProjectsExceptions();
		}
		
		RecordUtils.setDeleteFlags(brand);
		brand = brandDAO.save(brand);
	}
	
	private static BrandDTO mapToDTO(Brand objToMap){
		BrandDTO dto = new BrandDTO();
		
		dto.setId( objToMap.getId() );
		dto.setUuid( objToMap.getUuid() );
		dto.setName( objToMap.getName() );
		dto.setClientId( objToMap.getClient().getId() );
		dto.setClientName( objToMap.getClient().getName() );
		
		return dto;
	}

	@Override
	public boolean nameExists(String name, long clientId) {
		long count = brandDAO.findAllByNameAndClientId(name, clientId).parallelStream().filter( (b) -> b.isActive() == true ).count();
		return count > 0;
	}
	
	private boolean containsProjects(Brand brand){
		return brand.getProjects().parallelStream().filter( (b) -> b.isActive() == true ).count() > 0;
	}

	
	
}
