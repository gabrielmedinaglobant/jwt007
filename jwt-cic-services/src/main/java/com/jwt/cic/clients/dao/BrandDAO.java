package com.jwt.cic.clients.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.clients.domain.Brand;

public interface BrandDAO extends CrudRepository<Brand, Long> {
	
	public List<Brand> findAll();
	@Query("select b from Brand b join fetch b.client c where lower(b.name) like lower(concat(:name,'%')) and lower(c.name) like lower(concat(:clientName,'%'))")
	public List<Brand> findAllByNameAndClientName(
			@Param("name") String name,
			@Param("clientName") String clientName);
	@Query("select b from Brand b join fetch b.client c where lower(b.name) = lower(:name) and c.id = :clientId")
	public List<Brand> findAllByNameAndClientId(
			@Param("name") String name,
			@Param("clientId") long clientId);
	@Query("select b from Brand b join fetch b.client c where lower(b.name) like lower(concat(:name,'%')) and c.id = :clientId")
	public List<Brand> findAllByNameILikeAndClientId(
			@Param("name") String name,
			@Param("clientId") long clientId);
	
}
