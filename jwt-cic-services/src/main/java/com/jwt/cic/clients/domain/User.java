package com.jwt.cic.clients.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.jwt.cic.domain.Record;
import com.jwt.cic.thirdparty.domain.SourceCredential;

@Table(name="jwt_user")
@Entity
public class User implements Record {
	
	@Id
	private long id;
	@Column(name="name_id")
	private String nameId;
	private String name;
	@Column(name="last_name")
	private String lastName;
	private String email;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "user")
	private List<UserProject> userProjects;
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "user")
	private List<SourceCredential> sourceCredentials;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_updated")
	private Date dateUpdated;
	private boolean active;
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<UserProject> getUserProjects() {
		return userProjects;
	}
	public void setUserProjects(List<UserProject> userProject) {
		this.userProjects = userProject;
	}
	public List<SourceCredential> getSourceCredentials() {
		return sourceCredentials;
	}
	public void setSourceCredentials(List<SourceCredential> sourceCredentials) {
		this.sourceCredentials = sourceCredentials;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", nameId=" + nameId + ", dateCreated=" + dateCreated + ", dateUpdated=" + dateUpdated
				+ ", active=" + active + "]";
	}
	
	
}
