package com.jwt.cic.clients.dto;

public class ProjectSetAssignedUserDTO {
	private String userNameId;
	private String role;
	
	public String getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(String userNameId) {
		this.userNameId = userNameId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}
