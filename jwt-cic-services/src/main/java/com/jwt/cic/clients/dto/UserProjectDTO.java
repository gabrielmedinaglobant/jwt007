package com.jwt.cic.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class UserProjectDTO {
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	public long userId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String userNameId;
	public String email;
	public long roleId;
	public String roleDisplayName;
	public String role;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	public long clientId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String clientName;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	public long brandId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String brandName;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	public long projectId;
	@JsonInclude(JsonInclude.Include.NON_NULL) 
	public String projectName;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(String userNameId) {
		this.userNameId = userNameId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getRoleId() {
		return roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public long getClientId() {
		return clientId;
	}
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public long getBrandId() {
		return brandId;
	}
	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getRoleDisplayName() {
		return roleDisplayName;
	}
	public void setRoleDisplayName(String roleDisplayName) {
		this.roleDisplayName = roleDisplayName;
	}
	
}
