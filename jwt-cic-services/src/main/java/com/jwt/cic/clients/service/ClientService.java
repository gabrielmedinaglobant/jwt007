package com.jwt.cic.clients.service;

import java.util.List;

import com.jwt.cic.clients.dto.ClientDTO;

public interface ClientService {
	List<ClientDTO> getAll();
	List<ClientDTO> getAllByName(String name);
	ClientDTO save(ClientDTO clientDTO);
	void delete(long id);
	ClientDTO update(long id, ClientDTO clientDTO);
	boolean nameExists(String name);
	ClientDTO get(long id);
}
