package com.jwt.cic.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class UserDTO {
	private long id;
	private String nameId;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String lastName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String email;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<UserProjectDTO> projects;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<UserProjectDTO> getProjects() {
		return projects;
	}
	public void setProjects(List<UserProjectDTO> projects) {
		this.projects = projects;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", nameId=" + nameId + "]";
	}

}
