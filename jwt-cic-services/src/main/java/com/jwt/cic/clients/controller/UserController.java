package com.jwt.cic.clients.controller;

import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;
import com.jwt.cic.clients.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController {


    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/", ""})
    public List<UserDTO> getAll(
            @RequestParam(value = "first-name", defaultValue = "", required = false) String firstName,
            @RequestParam(value = "last-name", defaultValue = "", required = false) String lastName,
            @RequestParam(value = "email", defaultValue = "", required = false) String email) {
        return userService.getAll(firstName, lastName, email);
    }


    @RequestMapping(method = RequestMethod.GET, value = {"/{user-name-id:.+}"})
    public UserDTO get(@PathVariable("user-name-id") String userNameId) {
        return userService.get(userNameId);
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/search"})
    public List<UserDTO> searchUsers(@RequestParam(value = "query", defaultValue = "", required = true) String query) {
        return userService.getAllByNameOrLastNameOrEmail(query);
    }
}
