package com.jwt.cic.clients.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.clients.dto.ClientDTO;
import com.jwt.cic.clients.service.ClientService;

/***
 * 
 * Controller for Client (Client Mapping)
 * 
 * @author gabriel.medina
 *
 */
@RestController
@RequestMapping("api/v1/clients")
public class ClientController {

	private ClientService clientService;
	
	@Autowired
	public ClientController(ClientService clientService) {
		this.clientService = clientService;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/{id}"})
	public ClientDTO get(@PathVariable long id){
		return clientService.get(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public List<ClientDTO> getAll(@RequestParam(value = "name", defaultValue = "", required=false) String name){
		return clientService.getAllByName(name);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/",""})
	public ClientDTO save(@RequestBody ClientDTO clientDTO){
		return clientService.save(clientDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {"/{id}"})
	public ClientDTO update(@PathVariable long id, @RequestBody ClientDTO clientDTO){
		return clientService.update(id, clientDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{id}"})
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable long id){
		clientService.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/exists"})
	public ClientExistsResponseDTO nameExists(@RequestParam(value = "name", required=true) String name){
		return new ClientExistsResponseDTO(clientService.nameExists(name));
	}
	
	static public class ClientExistsResponseDTO {
		private boolean exists;

		
		public ClientExistsResponseDTO(boolean exists) {
			super();
			this.exists = exists;
		}

		public boolean isExists() {
			return exists;
		}

		public void setExists(boolean exists) {
			this.exists = exists;
		}
		
	}
	
}
