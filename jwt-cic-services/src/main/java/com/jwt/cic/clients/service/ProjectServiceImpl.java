package com.jwt.cic.clients.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.cic.clients.dao.BrandDAO;
import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.dao.RoleDAO;
import com.jwt.cic.clients.dao.UserDAO;
import com.jwt.cic.clients.dao.UserProjectDAO;
import com.jwt.cic.clients.domain.Brand;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.clients.domain.Role;
import com.jwt.cic.clients.domain.User;
import com.jwt.cic.clients.domain.UserProject;
import com.jwt.cic.clients.dto.ProjectDTO;
import com.jwt.cic.clients.dto.ProjectExistsResponseDTO;
import com.jwt.cic.clients.dto.ProjectSaveDTO;
import com.jwt.cic.clients.dto.ProjectSetAssignedUserDTO;
import com.jwt.cic.clients.dto.ProjectUpdateDTO;
import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.exceptions.RoleNotFoundException;
import com.jwt.cic.exceptions.UserNotFoundException;
import com.jwt.cic.exceptions.UserProjectAssociationNotFoundException;
import com.jwt.cic.queries.dao.QueryDAO;
import com.jwt.cic.queries.dao.QueryTypeDAO;
import com.jwt.cic.queries.domain.Query;
import com.jwt.cic.queries.domain.QueryType;
import com.jwt.cic.queries.exceptions.QueryTypeNotFoundException;
import com.jwt.cic.utils.QueryUtils;
import com.jwt.cic.utils.RecordUtils;

@Service
public class ProjectServiceImpl implements ProjectService {

	private final Logger log = LoggerFactory.getLogger(ProjectServiceImpl.class);
	
	private ProjectDAO projectDAO;
	private UserProjectDAO userProjectDAO;
	private BrandDAO brandDAO;
	private RoleDAO roleDAO;
	private UserDAO userDAO;
	private QueryDAO queryDAO;
	private QueryTypeDAO queryTypeDAO;
	
	@Autowired
	public ProjectServiceImpl(ProjectDAO projectDAO, UserProjectDAO userProjectDAO, BrandDAO brandDAO, RoleDAO roleDAO,
			UserDAO userDAO, QueryDAO queryDAO, QueryTypeDAO queryTypeDAO) {
		this.projectDAO = projectDAO;
		this.userProjectDAO = userProjectDAO;
		this.brandDAO = brandDAO;
		this.roleDAO = roleDAO;
		this.userDAO = userDAO;
		this.queryDAO = queryDAO;
		this.queryTypeDAO = queryTypeDAO;
	}

	@Override
	public List<ProjectDTO> getAll(String clientName, String brandName, String projectName) {
		List<Project> projects = this.projectDAO.findAllByClientNameOrBrandNameOrName(clientName, brandName, projectName);
		List<ProjectDTO> results = new LinkedList<>();
		
		projects.forEach((p) -> {
			ProjectDTO projectDTO = new ProjectDTO();
			projectDTO.setId(p.getId());
			projectDTO.setClientId(p.getBrand().getClient().getId());
			projectDTO.setClientName(p.getBrand().getClient().getName());
			projectDTO.setBrandId(p.getBrand().getId());
			projectDTO.setBrandName(p.getBrand().getName());
			projectDTO.setName(p.getName());
			results.add(projectDTO);
		});
		
		return results;
	}

	@Override
	public List<UserProjectDTO> getAssignedUsers(long projectId) {
		
		List<UserProject> userProjects = userProjectDAO.findAllByProjectId(projectId);
		List<UserProjectDTO> results = new LinkedList<>();
		
		userProjects.forEach((up) -> {
			UserProjectDTO dto = new UserProjectDTO();
			dto.setUserId( up.getUser().getId() );
			dto.setUserNameId(up.getUser().getNameId());
			dto.setEmail(up.getUser().getEmail());
			dto.setRoleId(up.getRole().getId());
			dto.setRoleDisplayName(up.getRole().getDisplayName());
			dto.setRole(up.getRole().getValue());
			dto.setClientId(up.getProject().getBrand().getClient().getId());
			dto.setClientName(up.getProject().getBrand().getClient().getName());
			dto.setBrandId(up.getProject().getBrand().getId());
			dto.setBrandName(up.getProject().getBrand().getName());
			dto.setProjectId(up.getProject().getId());
			dto.setProjectName(up.getProject().getName());
			results.add(dto);
		});
		
		return results;
	}

	@Override
	@Transactional
	public ProjectSaveDTO save( ProjectSaveDTO saveDTO ) {
		log.info("Saving a project");
		log.info(saveDTO.toString());
		
		Project project = new Project();
		List<UserProject> userProjectsList = buildUserListProjects(project, saveDTO);
		List<Query> queriesList = buildQueryListProjects(project, saveDTO);
		Iterable<UserProject> savedUserProjects;
		Iterable<Query> savedQueries;
		
		// search for an available brand
		Brand brand = brandDAO.findOne( saveDTO.getBrandId() );
		project.setName( saveDTO.getProjectName() );
		project.setBrand( brand );
		
		//Creates assocations between users and projects
		project.setUserProjects( userProjectsList );
		project.setUuid( UUID.randomUUID().toString() );
		
		//Create assocation with queries
		project.setQueries( queriesList );
		
		project.setActive( true );
		project.setDateCreated( DateTime.now().toDate() );
		project.setDateUpdated( DateTime.now().toDate() );
		
		
		//Saving...
		project = projectDAO.save( project );
		savedUserProjects = userProjectDAO.save( userProjectsList );
		savedQueries = queryDAO.save( queriesList );
		
		log.info("The following project has been saved " + project);
		
		return buildResponseSaveDTO( saveDTO, project, savedUserProjects );
	}
	
	public ProjectSaveDTO buildResponseSaveDTO( ProjectSaveDTO saveDTO , Project project, Iterable<UserProject> userProjectsIterable ){
		
		saveDTO.setProjectId( project.getId() );
		saveDTO.setUsers(
			StreamSupport.stream( userProjectsIterable.spliterator() , false )
				.map( (userProject) -> {
					UserProjectDTO userProjectDTO = new UserProjectDTO();
					userProjectDTO.setUserId( userProject.getUser().getId() );
					userProjectDTO.setUserNameId( userProject.getUser().getNameId() );
					userProjectDTO.setRoleId( userProject.getRole().getId() );
					userProjectDTO.setRole( userProject.getRole().getValue() );
					return userProjectDTO;
				} )
				.collect( Collectors.toList() )
		);
		
		return saveDTO;
	}
	
	private List<UserProject> buildUserListProjects(Project project, ProjectSaveDTO saveDTO){
		log.info("Build UserProject list to save");
		return saveDTO.getUsers()
					.stream()
					.map( (userProjectDTO) -> {
						log.info("Getting user for project " + userProjectDTO.toString());
						UserProject up = new UserProject();
						Role role = roleDAO.findOneByValue(userProjectDTO.getRole());
						User user = userDAO.findOneByNameId(userProjectDTO.getUserNameId());
						
						if( role==null ) {
							throw new RoleNotFoundException();
						}
						if( user==null ) {
							throw new UserNotFoundException();
						}
							
						log.info("Getting role for user in project " + role.getId() + ":" + role.getValue());
					//	up.setId(userProjectDTO.getUserId());
						//TODO: this will found out a user, right now we don't have any
						
						//up.setNameId(userDTO.getNameId());	
						
						up.setProject(project);
						up.setRole(role);
						up.setUser(user);
						
						up.setDateCreated( DateTime.now().toDate() );
						up.setDateUpdated( DateTime.now().toDate() );
						up.setActive( true );
						
						return up;
					} )
					.collect( Collectors.toList() );
	}

	private List<Query> buildQueryListProjects(Project project, ProjectSaveDTO saveDTO){
		log.info("Build Query list to save");
		return saveDTO.getQueries()
						.stream()
						.map( (queryDTO) -> {
							Query q = new Query();
							QueryType qt = queryTypeDAO.findOne( queryDTO.getQueryTypeId() );
							
							if(qt == null){
								throw new QueryTypeNotFoundException();
							}
							
							q.setQueryType(qt);
							q.setQueryString(QueryUtils.clean(queryDTO.getQueryString()));
							q.setDirty(true);
							q.setProject(project);
							RecordUtils.setCreateFlags(q);
							
							return q;
						} )
						.collect( Collectors.toList() );
	}
	
	@Override
	@Transactional
	public void delete(long projectId) {
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		List<UserProject> userProjects = project.getUserProjects();
		
		userProjects.forEach(
			(userProject)->{
				userProject.setActive( false );
			}
		);
		project.setActive( false );
		
		userProjectDAO.save( userProjects );
		projectDAO.save( project );
	}
	
	@Override
	@Transactional
	public ProjectSetAssignedUserDTO setAssignedUser(long projectId, ProjectSetAssignedUserDTO projectSetAssignUserDTO) {		
		log.info("unassignUser: projectId -> " + projectId + ", nameId -> " + projectSetAssignUserDTO.getUserNameId());
		// Checks if there is user is already assigned to project.
		// If not assigned, create new instance
		// else, update its role and save
		
		UserProject up = userProjectDAO.findOneByProjectIdAndNameId(projectId, projectSetAssignUserDTO.getUserNameId());
		
		//association not found, create one
		if(up == null){
			log.info("Creating new UserProject instance. No association was found for: projectId -> " + projectId + ", nameId -> " + projectSetAssignUserDTO.getUserNameId());
			Project project = projectDAO.findOne(projectId);
			User user = userDAO.findOneByNameId(projectSetAssignUserDTO.getUserNameId());
			
			if(project == null){
				throw new ProjectNotFoundException();
			}
			
			up = new UserProject();
			up.setProject( project );
			up.setUser(user);
			
			up.setDateCreated( DateTime.now().toDate() );
			up.setActive(true);
		}
		
		Role role = roleDAO.findOneByValue(projectSetAssignUserDTO.getRole());
		
		if( role ==null ) {
			throw new RoleNotFoundException();
		}
		
		up.setRole(role);
		
		up.setDateUpdated( DateTime.now().toDate() );
		
		up = userProjectDAO.save(up);
		
		return buildProjectSetAssignedUserDTO(up);
	}
	
	private ProjectSetAssignedUserDTO buildProjectSetAssignedUserDTO(UserProject up){
		ProjectSetAssignedUserDTO dto = new ProjectSetAssignedUserDTO();
		dto.setUserNameId( up.getUser().getNameId() );
		dto.setRole( up.getRole().getValue() );
		return dto;
	}

	@Override
	@Transactional
	public void unassignUser(long projectId, String nameId) {
		log.info("unassignUser: projectId -> " + projectId + ", nameId -> " + nameId);
		UserProject up = userProjectDAO.findOneByProjectIdAndNameId(projectId, nameId);
		
		if(up == null){
			log.info("No association was found for: projectId -> " + projectId + ", nameId -> " + nameId);
			throw new UserProjectAssociationNotFoundException();
		}
		
		up.setActive( false );
		up.setDateUpdated( DateTime.now().toDate() );
		
		userProjectDAO.save( up );
	}

	@Override
	@Transactional
	public void unassignManyUsers(long projectId, Set<String> userNameIds) {
		log.info("unassignManyUsers: projectId -> " + projectId + ", nameId -> " + userNameIds.toString());
		
		List<UserProject> ups = userProjectDAO.findAllByProjectIdAndNameIds(projectId, userNameIds);
		
		ups.forEach( (up) -> {
			up.setActive( false );
			up.setDateUpdated( DateTime.now().toDate() );
		});
		
		userProjectDAO.save( ups );
	}

	@Override
	public List<UserDTO> getUnassignedUsers(long projectId, String userNameId,String firstName,String lastName,String email) {

		log.info("getUnassignedUsers: projectId -> " + projectId + ", userNameId -> " + userNameId);
		
		List<UserProject> userProjects = userProjectDAO.findAllByProjectId(projectId);
		Set<Long> userProjectsIds = userProjects.stream()
												.map((userProject) -> { return userProject.getUser().getId(); })
												.collect( Collectors.toSet() );
		List<User> users = userDAO.findAllByIdNotIn(userProjectsIds,userNameId,firstName,lastName,email);
		
		return buildUnassignedUsersUserDTOList( users );
	}
	
	private List<UserDTO> buildUnassignedUsersUserDTOList(List<User> users){
		return users.stream().map( (user) -> {
			UserDTO userDTO = new UserDTO();
			
			userDTO.setId( user.getId() );
			userDTO.setNameId( user.getNameId() );
			userDTO.setLastName(user.getLastName());
			userDTO.setName(user.getName());
			userDTO.setEmail(user.getEmail());

			return userDTO;
		} ).collect( Collectors.toList() );
	}

	@Override
	public ProjectUpdateDTO update(long projectId, ProjectUpdateDTO updateDTO) {
		log.info("Updating project");
		log.info(updateDTO.toString());
		
		Project project = projectDAO.findOne(projectId);
		if(project == null){
			throw new ProjectNotFoundException();
		}
		
		if(updateDTO.getName() != null){
			project.setName( updateDTO.getName() );
			log.info("Updating project name");
			log.info(project.getName());
		}
		
		// search for an available brand if brandId was requested to be saved
		if(updateDTO.getBrandId() != null){
			// TODO: change it so that it actually check for an active one
			Brand brand = brandDAO.findOne( updateDTO.getBrandId() );
			if(brand == null){
				throw new RuntimeException(); // TODO: change it to brand not found
			}
			project.setBrand( brand );
		}
		
		project.setDateUpdated( DateTime.now().toDate() );
		
		project = projectDAO.save( project );
		
		
		log.info("The following project has been saved " + project);
		
		return buildResponseUpdateDTO( updateDTO, project );
	}

	public ProjectUpdateDTO buildResponseUpdateDTO( ProjectUpdateDTO updateDTO , Project project ){
		
		updateDTO.setId( project.getId() );
		updateDTO.setName( project.getName() );
		updateDTO.setBrandId( project.getBrand().getId() );
		
		return updateDTO;
	}

	@Override
	public ProjectExistsResponseDTO exists(long brandId) {
		ProjectExistsResponseDTO projectExistsResponseDTO = new ProjectExistsResponseDTO();
		Project project = projectDAO.findOneByBrandId(brandId);
		
		if(project == null){
			projectExistsResponseDTO.setExists(false);
		} else{
			projectExistsResponseDTO.setExists(true);
			projectExistsResponseDTO.setProjectDTO(new ProjectDTO(
				project.getId(), 
				project.getBrand().getClient().getId(), project.getBrand().getClient().getName(), 
				project.getBrand().getId(), project.getBrand().getName(), 
				project.getName()
			));
		}
		
		return projectExistsResponseDTO;
	}
	
}
