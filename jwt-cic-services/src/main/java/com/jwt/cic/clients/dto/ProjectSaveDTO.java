package com.jwt.cic.clients.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jwt.cic.queries.dto.QueryDTO;

public class ProjectSaveDTO {
	private long brandId;
	@JsonInclude(JsonInclude.Include.NON_NULL) 
	private long projectId;
	private String projectName;
	private List<UserProjectDTO> users;
	private List<QueryDTO> queries;
	
	public long getBrandId() {
		return brandId;
	}
	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<UserProjectDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserProjectDTO> users) {
		this.users = users;
	}
	public List<QueryDTO> getQueries() {
		return queries;
	}
	public void setQueries(List<QueryDTO> queries) {
		this.queries = queries;
	}
	
	@Override
	public String toString() {
		return "ProjectSaveDTO [brandId=" + brandId + ", projectId=" + projectId + ", projectName=" + projectName
				+ ", users=" + users + "]";
	}

}
