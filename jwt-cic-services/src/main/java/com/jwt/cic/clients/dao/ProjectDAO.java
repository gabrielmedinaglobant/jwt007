package com.jwt.cic.clients.dao;

import com.jwt.cic.clients.domain.Project;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProjectDAO extends CrudRepository<Project, Long> {
	static final String FIND_ALL_BY_CLIENT_NAME_OR_BRAND_NAME_OR_NAME = "select p from Project p join fetch p.brand b join fetch b.client c where lower(p.name) like lower(concat(:projectName,'%')) and b.name like concat(:brandName,'%') and c.name like concat(:clientName,'%') and p.active = true";

	public List<Project> findAll();

	@Query(FIND_ALL_BY_CLIENT_NAME_OR_BRAND_NAME_OR_NAME)
	public List<Project> findAllByClientNameOrBrandNameOrName(@Param("clientName") String clientName,
			@Param("brandName") String brandName, @Param("projectName") String projectName);
	
	@Query("select p from Project p join fetch p.brand b where b.id = :brandId")
	public Project findOneByBrandId(@Param("brandId") long brandId);

}
