package com.jwt.cic.clients.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.clients.domain.UserProject;

public interface UserProjectDAO extends CrudRepository<UserProject, Long> {
	@Query("select up from UserProject up join fetch up.project p where p.id = :projectId and up.active = true")
	public List<UserProject> findAllByProjectId(@Param("projectId") long projectId);

	@Query("select up from UserProject up join fetch up.project p join fetch up.user u where p.id = :projectId and u.nameId = :nameId and up.active = true")
	public UserProject findOneByProjectIdAndNameId(@Param("projectId") long projectId, @Param("nameId") String nameId);

	@Query("select up from UserProject up join fetch up.project p join fetch up.user u where p.id = :projectId and u.nameId in (:nameIds) and up.active = true")
	public List<UserProject> findAllByProjectIdAndNameIds(
			@Param("projectId") long projectId,
			@Param("nameIds") Set<String> nameIds);

	// @Query("select up from UserProject up left join up.user u join fetch up.project p where p.id = :projectId and lower(u.nameId) like lower(concat(:userNameId,'%')) and up is null")
	// public List<UserProject> findAllUnassignedUsers(@Param("projectId") long
	// projectId, @Param("userNameId") String userNameId);

	//@Query("select u from User u left join u.userProjects up join fetch up.project p where p.id = :projectId and lower(u.nameId) like lower(concat(:userNameId,'%')) and up is null")
	//@Query("select up from UserProject up left join up.user u join fetch up.project p where p.id = :projectId and lower(u.nameId) like lower(concat(:userNameId,'%')) and up is null")
	//public List<UserProject> findAllUnassignedUsers(
	//		@Param("projectId") long projectId,
	//		@Param("userNameId") String userNameId);

}
