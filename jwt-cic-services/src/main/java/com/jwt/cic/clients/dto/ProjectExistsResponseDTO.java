package com.jwt.cic.clients.dto;

public class ProjectExistsResponseDTO {
	private boolean exists;
	private ProjectDTO projectDTO;
	
	public ProjectExistsResponseDTO() {
	}
	
	public ProjectExistsResponseDTO(boolean exists, ProjectDTO projectDTO) {
		this.exists = exists;
		this.projectDTO = projectDTO;
	}
	
	public boolean isExists() {
		return exists;
	}
	public void setExists(boolean exists) {
		this.exists = exists;
	}
	public ProjectDTO getProjectDTO() {
		return projectDTO;
	}
	public void setProjectDTO(ProjectDTO projectDTO) {
		this.projectDTO = projectDTO;
	}
	
	
}
