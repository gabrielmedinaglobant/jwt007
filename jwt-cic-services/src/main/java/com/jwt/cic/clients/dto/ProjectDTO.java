package com.jwt.cic.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ProjectDTO {
	public long id;
	public long clientId;
	public String clientName;
	public long brandId;
	public String brandName;
	public String name;



	public ProjectDTO() {
	}

	public ProjectDTO(long id, long clientId, String clientName, long brandId, String brandName, String name) {
		this.id = id;
		this.clientId = clientId;
		this.clientName = clientName;
		this.brandId = brandId;
		this.brandName = brandName;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
