package com.jwt.cic.clients.service;

import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;

import java.util.List;

/**
 * Created by vishal.domale on 09-04-2017.
 */
public interface UserService {
    List<UserDTO> getAll(String firstName, String lastName, String email);
    UserDTO get(String  userId);
    List<UserDTO> getAllByNameOrLastNameOrEmail(String query);
}
