package com.jwt.cic.clients.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jwt.cic.clients.domain.Role;

public interface RoleDAO extends CrudRepository<Role, Long> {
	public List<Role> findAll();
	public Role findOneByValue(String value);
}
