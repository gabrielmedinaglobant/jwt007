package com.jwt.cic.clients.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.clients.dao.ClientDAO;
import com.jwt.cic.clients.domain.Client;
import com.jwt.cic.clients.dto.ClientDTO;
import com.jwt.cic.exceptions.ClientContainsBrandsException;
import com.jwt.cic.exceptions.ClientNameAlreadyExistsException;
import com.jwt.cic.exceptions.ClientNotFoundException;
import com.jwt.cic.utils.RecordUtils;

@Service
public class ClientServiceImpl implements ClientService {

	private ClientDAO clientDAO;
	
	@Autowired
	public ClientServiceImpl(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

	@Override
	public ClientDTO get(long id){
		Client client = clientDAO.findOne(id);
		
		if(client == null || !client.isActive() ){
			throw new ClientNotFoundException();
		}
		
		return mapToDTO(client);
	}
	
	@Override
	public List<ClientDTO> getAll() {
		return buildClientDTOList( clientDAO.findAll().parallelStream().filter( (b) -> b.isActive() == true ).collect( Collectors.toList() ) );
	}
	
	public List<ClientDTO> getAllByName(String name){
		return buildClientDTOList( clientDAO.findAllByNameLike(name).parallelStream().filter( (b) -> b.isActive() == true ).collect( Collectors.toList() ) );
	}
	
	private List<ClientDTO> buildClientDTOList( List<Client> clients ){
		return clients.stream()
				.map( ClientServiceImpl::mapToDTO )
				.collect( Collectors.toList() );
	}
	
	@Override
	public ClientDTO save(ClientDTO clientDTO) {
		Client client = new Client();
		
		if(nameExists(clientDTO.getName())){
			throw new ClientNameAlreadyExistsException();
		}
		
		client.setUuid(UUID.randomUUID().toString());
		client.setName(clientDTO.getName());
		RecordUtils.setCreateFlags(client);
		
		client = clientDAO.save(client);
		
		return mapToDTO(client);
	}

	@Override
	public ClientDTO update(long id, ClientDTO clientDTO) {
		Client client = clientDAO.findOne(id);
		
		if(client == null || !client.isActive() ){
			throw new ClientNotFoundException();
		}
		
		if(nameExists(clientDTO.getName())){
			throw new ClientNameAlreadyExistsException();
		}
		
		client.setName(clientDTO.getName());
		RecordUtils.setUpdateFlags(client);
		client = clientDAO.save(client);
		
		return mapToDTO(client);
	}

	@Override
	public void delete(long id) {
		Client client = clientDAO.findOne(id);
		long activeBrandsCount = 0;
		
		if(client == null || !client.isActive() ){
			throw new ClientNotFoundException();
		}
		
		//check if client doesn't contain any active brands.
		activeBrandsCount = client.getBrands().parallelStream().filter( (b) -> b.isActive() == true ).count();
		if(activeBrandsCount > 0){
			throw new ClientContainsBrandsException();
		}
		
		RecordUtils.setDeleteFlags(client);
		client = clientDAO.save(client);
	}
	
	private static ClientDTO mapToDTO(Client client){
		ClientDTO clientDTO = new ClientDTO();
		
		clientDTO.setId( client.getId() );
		clientDTO.setUuid( client.getUuid() );
		clientDTO.setName( client.getName() );
		
		return clientDTO;
	}

	@Override
	public boolean nameExists(String name) {
		long countExists = clientDAO.findAllByNameIgnoreCase(name).parallelStream().filter( (b) -> b.isActive() == true ).count();
		return countExists > 0;
	}
}
