package com.jwt.cic.clients.service;

import com.jwt.cic.clients.dao.UserDAO;
import com.jwt.cic.clients.domain.User;
import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;
import com.jwt.cic.exceptions.UserNotFoundException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by vishal.domale on 09-04-2017.
 */

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	
    private UserDAO userDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }


    @Override
    public List<UserDTO> getAll(String firstName, String lastName, String email) {
        List<User> users = userDAO.findAllByNameAndLastNameAndEmail(firstName, lastName, email);
        return buildUsersUserDTOList(users);
    }

    @Override
    public UserDTO get(String userNameId) {
        User user=userDAO.findOneWithProjectsByNameId(userNameId);
        if(user==null){
            throw new UserNotFoundException();
        }
        return buildUserDto(user) ;
    }

    @Override
    public List<UserDTO> getAllByNameOrLastNameOrEmail(String query) {
        List<User> users= userDAO.findAllByNameOrLastNameOrEmail(query);
        return buildUsersUserDTOList(users);
    }

    private UserDTO buildUserDto(User user){
        UserDTO userDTO=new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setNameId(user.getNameId());
        userDTO.setProjects(buildUserProjectsDTOList(user));
        return userDTO ;
    }
    private List<UserProjectDTO> buildUserProjectsDTOList(User user){
        return  user.getUserProjects().stream()
        		.filter( (userProject) -> { return userProject.isActive(); } )
        		.map((userProject) ->{
            UserProjectDTO userProjectDTO=new UserProjectDTO();

            userProjectDTO.setClientId(userProject.getProject().getBrand().getClient().getId());
            userProjectDTO.setClientName(userProject.getProject().getBrand().getClient().getName());

            userProjectDTO.setBrandName(userProject.getProject().getBrand().getName());
            userProjectDTO.setBrandId(userProject.getProject().getBrand().getId());

            userProjectDTO.setProjectId(userProject.getProject().getId());
            userProjectDTO.setProjectName(userProject.getProject().getName());

            userProjectDTO.setRole(userProject.getRole().getValue());
            userProjectDTO.setRoleDisplayName(userProject.getRole().getDisplayName());
            userProjectDTO.setRoleId(userProject.getRole().getId());

            return userProjectDTO;
        }).collect(Collectors.toList());
    }

    private List<UserDTO> buildUsersUserDTOList(List<User> users) {
        return users.stream().map((user) -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setNameId(user.getNameId());
            userDTO.setLastName(user.getLastName());
            userDTO.setName(user.getName());
            userDTO.setEmail(user.getEmail());
            return userDTO;
        }).collect(Collectors.toList());
    }

}
