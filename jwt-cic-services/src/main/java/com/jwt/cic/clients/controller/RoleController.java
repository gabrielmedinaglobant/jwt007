package com.jwt.cic.clients.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.clients.dto.RoleDTO;
import com.jwt.cic.clients.service.RoleService;

@RestController
@RequestMapping("api/v1/roles")
public class RoleController {
	
	private RoleService roleService;

	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public List<RoleDTO> getAll(){
		return roleService.getAll();
	}
	
}
