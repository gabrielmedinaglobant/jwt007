package com.jwt.cic.clients.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.clients.dao.RoleDAO;
import com.jwt.cic.clients.domain.Role;
import com.jwt.cic.clients.dto.RoleDTO;

@Service
public class RoleServiceImpl implements RoleService {

	private RoleDAO roleDAO;
	
	@Autowired
	public RoleServiceImpl(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	@Override
	public List<RoleDTO> getAll() {
		return buildRoleDTOList( roleDAO.findAll() );
	}
	
	private List<RoleDTO> buildRoleDTOList( List<Role> roles ){
		return roles.stream()
				.map( RoleServiceImpl::mapper )
				.collect( Collectors.toList() );
	}
	
	private static RoleDTO mapper(Role objToMap){
		RoleDTO dto = new RoleDTO();
		
		dto.setId( objToMap.getId() );
		dto.setValue( objToMap.getValue() );
		dto.setDisplayName( objToMap.getDisplayName() );
		
		return dto;
	}

}
