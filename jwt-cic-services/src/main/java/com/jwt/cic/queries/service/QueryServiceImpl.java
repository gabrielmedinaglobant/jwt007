package com.jwt.cic.queries.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.clients.dao.ProjectDAO;
import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.exceptions.ProjectNotFoundException;
import com.jwt.cic.queries.dao.QueryDAO;
import com.jwt.cic.queries.dao.QueryTypeDAO;
import com.jwt.cic.queries.domain.Query;
import com.jwt.cic.queries.domain.QueryType;
import com.jwt.cic.queries.dto.QueryDTO;
import com.jwt.cic.queries.exceptions.QueryNotFoundException;
import com.jwt.cic.queries.exceptions.QueryTypeNotFoundException;
import com.jwt.cic.utils.QueryUtils;
import com.jwt.cic.utils.RecordUtils;

/***
 * 
 * @author gabriel.medina
 *
 */
@Service
public class QueryServiceImpl implements QueryService {

	private QueryDAO queryDAO;
	private QueryTypeDAO queryTypeDAO;
	private ProjectDAO projectDAO;
	
	@Autowired
	public QueryServiceImpl(QueryDAO queryDAO, QueryTypeDAO queryTypeDAO, ProjectDAO projectDAO) {
		this.queryDAO = queryDAO;
		this.queryTypeDAO = queryTypeDAO;
		this.projectDAO = projectDAO;
	}

	@Override
	public List<QueryDTO> getAllByProjectId(long projectId) {
		return queryDAO.findAllByProjectId(projectId)
						.stream().map( QueryServiceImpl::mapEntityToDto )
						.collect( Collectors.toList() );
	}

	@Override
	public QueryDTO save(QueryDTO queryDTO) {
		Query query = new Query();
		Project project = projectDAO.findOne( queryDTO.getProjectId() );
		QueryType queryType = queryTypeDAO.findOne( queryDTO.getQueryTypeId() );
		
		if(project == null){
			throw new ProjectNotFoundException();
		}
		if (queryType == null){
			throw new QueryTypeNotFoundException();
		}
		
		query.setProject(project);
		query.setQueryType(queryType);
		query.setQueryString( QueryUtils.clean(queryDTO.getQueryString()) );
		query.setDirty(true);
		
		RecordUtils.setCreateFlags(query);
		query = queryDAO.save(query);
		
		return mapEntityToDto(query);
	}

	@Override
	public QueryDTO update(long queryId, QueryDTO queryDTO) {
		Query query = queryDAO.findOne(queryId);
		Project project = projectDAO.findOne( queryDTO.getProjectId() );
		QueryType queryType = queryTypeDAO.findOne( queryDTO.getQueryTypeId() );
		
		if(query == null){
			throw new QueryNotFoundException();
		}
		if(project == null){
			throw new ProjectNotFoundException();
		}
		if (queryType == null){
			throw new QueryTypeNotFoundException();
		}
		
		query.setProject(project);
		query.setQueryType(queryType);
		query.setQueryString( QueryUtils.clean(queryDTO.getQueryString()) );
		query.setDirty(true);
		
		RecordUtils.setUpdateFlags(query);
		query = queryDAO.save(query);
		
		return mapEntityToDto(query);
	}

	@Override
	public void delete(long queryId) {
		Query query = queryDAO.findOne(queryId);
		
		if(query == null){
			throw new QueryNotFoundException();
		}
		
		RecordUtils.setDeleteFlags(query);
		queryDAO.save(query);
	}
	
	protected static QueryDTO mapEntityToDto(Query query){
		QueryDTO dto = new QueryDTO();
		dto.setId( query.getId() );
		dto.setProjectId( query.getProject().getId() );
		dto.setQueryString( query.getQueryString() );
		dto.setQueryTypeKeyname( query.getQueryType().getKeyname() );
		dto.setQueryTypeDescription( query.getQueryType().getDescription() );
		dto.setQueryTypeId( query.getQueryType().getId() );
		return dto;
	}

}
