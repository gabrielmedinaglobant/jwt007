package com.jwt.cic.queries.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jwt.cic.clients.domain.Project;
import com.jwt.cic.domain.Record;

/***
 * 
 * @author gabriel.medina
 *
 */
@Table(name="jwt_query")
@Entity
public class Query implements Record {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Column(name="query_string")
	private String queryString;
	private boolean dirty;
	@Column(name="last_execution")
	private Date lastExecution;
	@Column(name="last_execution_successful")
	private Boolean lastExecutionSuccessful;
	@Column(name="last_execution_message")
	private String lastExecutionMessage;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="query_type_id")
	private QueryType queryType;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="project_id")
	private Project project;
	
	private boolean active;
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_updated")
	private Date dateUpdated;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public boolean isDirty() {
		return dirty;
	}
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}
	public Date getLastExecution() {
		return lastExecution;
	}
	public void setLastExecution(Date lastExecution) {
		this.lastExecution = lastExecution;
	}
	public Boolean isLastExecutionSuccessful() {
		return lastExecutionSuccessful;
	}
	public void setLastExecutionSuccessful(Boolean lastExecutionSuccessful) {
		this.lastExecutionSuccessful = lastExecutionSuccessful;
	}
	public String getLastExecutionMessage() {
		return lastExecutionMessage;
	}
	public void setLastExecutionMessage(String lastExecutionMessage) {
		this.lastExecutionMessage = lastExecutionMessage;
	}
	public QueryType getQueryType() {
		return queryType;
	}
	public void setQueryType(QueryType queryType) {
		this.queryType = queryType;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	
}
