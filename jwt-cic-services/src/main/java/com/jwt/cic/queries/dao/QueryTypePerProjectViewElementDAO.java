package com.jwt.cic.queries.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.queries.domain.QueryTypePerProjectViewElement;
import com.jwt.cic.queries.domain.QueryTypePerProjectViewElementPK;

public interface QueryTypePerProjectViewElementDAO extends CrudRepository<QueryTypePerProjectViewElement, QueryTypePerProjectViewElementPK>{
	@Query("select qtpe from QueryTypePerProjectViewElement qtpe where qtpe.projectUuid = :projectUuid")
	List<QueryTypePerProjectViewElement> findAllByProjectUuid(@Param("projectUuid") String projectUuid);
}
