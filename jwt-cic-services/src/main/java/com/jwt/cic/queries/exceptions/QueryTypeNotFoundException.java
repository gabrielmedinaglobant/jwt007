package com.jwt.cic.queries.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="QueryType entity not found") 
public class QueryTypeNotFoundException extends RuntimeException {

}
