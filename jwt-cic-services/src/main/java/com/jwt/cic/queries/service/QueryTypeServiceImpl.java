package com.jwt.cic.queries.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.queries.dao.QueryTypeDAO;
import com.jwt.cic.queries.dto.QueryTypeDTO;

@Service
public class QueryTypeServiceImpl implements QueryTypeService {

	private QueryTypeDAO queryTypeDAO;
	
	@Autowired
	public QueryTypeServiceImpl(QueryTypeDAO queryTypeDAO) {
		this.queryTypeDAO = queryTypeDAO;
	}

	@Override
	public List<QueryTypeDTO> getAll() {
		return queryTypeDAO.findAll().stream().map( (queryType) -> {
			QueryTypeDTO dto = new QueryTypeDTO();
			
			dto.setId( queryType.getId() );
			dto.setKeyname( queryType.getKeyname() );
			dto.setDescription( queryType.getDescription() );
			
			return dto;
		} ).collect( Collectors.toList() );
	}

}
