package com.jwt.cic.queries.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class QueryTypePerProjectViewElementPK implements Serializable {

	private static final long serialVersionUID = 7473333562720634872L;
	
	@Column(name="project_uuid")
	private String projectUuid;
	@Column(name="query_type")
	private String queryTypeKey;
	
	public String getProjectUuid() {
		return projectUuid;
	}
	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	public String getQueryTypeKey() {
		return queryTypeKey;
	}
	public void setQueryTypeKey(String queryTypeKey) {
		this.queryTypeKey = queryTypeKey;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((projectUuid == null) ? 0 : projectUuid.hashCode());
		result = prime * result + ((queryTypeKey == null) ? 0 : queryTypeKey.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryTypePerProjectViewElementPK other = (QueryTypePerProjectViewElementPK) obj;
		if (projectUuid == null) {
			if (other.projectUuid != null)
				return false;
		} else if (!projectUuid.equals(other.projectUuid))
			return false;
		if (queryTypeKey == null) {
			if (other.queryTypeKey != null)
				return false;
		} else if (!queryTypeKey.equals(other.queryTypeKey))
			return false;
		return true;
	}
	
	
}
