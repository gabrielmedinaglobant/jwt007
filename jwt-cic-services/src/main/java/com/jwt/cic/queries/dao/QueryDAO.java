package com.jwt.cic.queries.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/***
 * 
 * @author gabriel.medina
 *
 */
public interface QueryDAO extends CrudRepository<com.jwt.cic.queries.domain.Query, Long>{
	@Query("select q from Query q join fetch q.project p where q.active = true and p.id = :projectId")
	List<com.jwt.cic.queries.domain.Query> findAllByProjectId(@Param("projectId") long projectId);
}
