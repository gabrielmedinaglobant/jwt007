package com.jwt.cic.queries.service;

import java.util.List;

import com.jwt.cic.queries.dto.QueryTypeDTO;

public interface QueryTypeService {
	List<QueryTypeDTO> getAll();
}
