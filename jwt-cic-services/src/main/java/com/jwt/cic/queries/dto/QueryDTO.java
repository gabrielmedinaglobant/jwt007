package com.jwt.cic.queries.dto;

public class QueryDTO {
	private long id;
	private String queryString;
	private long queryTypeId;
	private String queryTypeKeyname;
	private String queryTypeDescription;
	private long projectId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public long getQueryTypeId() {
		return queryTypeId;
	}
	public void setQueryTypeId(long queryTypeId) {
		this.queryTypeId = queryTypeId;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public String getQueryTypeKeyname() {
		return queryTypeKeyname;
	}
	public void setQueryTypeKeyname(String queryTypeKeyname) {
		this.queryTypeKeyname = queryTypeKeyname;
	}
	public String getQueryTypeDescription() {
		return queryTypeDescription;
	}
	public void setQueryTypeDescription(String queryTypeDescription) {
		this.queryTypeDescription = queryTypeDescription;
	}
	
	
}
