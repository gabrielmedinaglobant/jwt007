package com.jwt.cic.queries.service;

import java.util.List;

import com.jwt.cic.queries.dto.QueryDTO;

public interface QueryService {
	List<QueryDTO> getAllByProjectId(long projectId);
	QueryDTO save(QueryDTO queryDTO);
	QueryDTO update(long projectId, QueryDTO queryDTO);
	void delete(long queryId);
}
