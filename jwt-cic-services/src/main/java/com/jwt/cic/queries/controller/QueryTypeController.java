package com.jwt.cic.queries.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.queries.dto.QueryTypeDTO;
import com.jwt.cic.queries.service.QueryTypeService;

@RestController
@RequestMapping("api/v1/query-types")
public class QueryTypeController {
	
	private QueryTypeService queryTypeService;
	
	@Autowired
	public QueryTypeController(QueryTypeService queryTypeService) {
		this.queryTypeService = queryTypeService;
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/", "" })
	public List<QueryTypeDTO> getAll(){
		return queryTypeService.getAll();
	}
	
}
