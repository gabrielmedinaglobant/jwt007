package com.jwt.cic.queries.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.queries.dto.QueryDTO;
import com.jwt.cic.queries.service.QueryService;

@RestController
@RequestMapping("api/v1/queries")
public class QueryController {
	
	private QueryService queryService;
	
	@Autowired
	public QueryController(QueryService queryService) {
		this.queryService = queryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public List<QueryDTO> getAllByProjectId(@RequestParam(value = "project-id", defaultValue = "", required=false) long projectId){
		return queryService.getAllByProjectId(projectId);
	}
	@RequestMapping(method = RequestMethod.POST, value = {"/",""})
	public QueryDTO save(@RequestBody QueryDTO queryDTO){
		return queryService.save(queryDTO);
		
	}
	@RequestMapping(method = RequestMethod.POST, value = {"/{queryId}"})
	public QueryDTO update(@PathVariable long queryId, @RequestBody QueryDTO queryDTO){
		return queryService.update(queryId, queryDTO);
	}
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{queryId}"})
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable long queryId){
		queryService.delete(queryId);
	}
}
