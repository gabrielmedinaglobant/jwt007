package com.jwt.cic.queries.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="vw_query_types_per_project")
@Entity
public class QueryTypePerProjectViewElement {
	
	@EmbeddedId
    private QueryTypePerProjectViewElementPK id;
	
	@Column(name="project_uuid",insertable=false, updatable=false)
	private String projectUuid;
	@Column(name="query_type",insertable=false, updatable=false)
	private String queryTypeKey;
	@Column(name="brand_name", length = 65535, columnDefinition="TEXT")
	private String brandName;
	
	
	public QueryTypePerProjectViewElementPK getId() {
		return id;
	}
	public void setId(QueryTypePerProjectViewElementPK id) {
		this.id = id;
	}
	
	public String getProjectUuid() {
		return projectUuid;
	}
	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	public String getQueryTypeKey() {
		return queryTypeKey;
	}
	public void setQueryTypeKey(String queryTypeKey) {
		this.queryTypeKey = queryTypeKey;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"projectUuid\":\"" + projectUuid + "\",\"queryTypeKey\":\"" + queryTypeKey
				+ "\",\"brandName\":\"" + brandName + "\"}";
	}
	
	
	
}
