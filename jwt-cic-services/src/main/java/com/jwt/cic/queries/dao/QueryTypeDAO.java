package com.jwt.cic.queries.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jwt.cic.queries.domain.QueryType;

public interface QueryTypeDAO extends CrudRepository<QueryType, Long>{
	
	@Query("select qt from QueryType qt where qt.active = true")
	public List<QueryType> findAll();
}
