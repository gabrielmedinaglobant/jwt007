package com.jwt.cic.domain;

import java.util.Date;

public interface Record {
	Date getDateCreated();
	void setDateCreated(Date dateCreated);
	Date getDateUpdated();
	void setDateUpdated(Date dateUpdated);
	boolean isActive();
	void setActive(boolean active);
}
