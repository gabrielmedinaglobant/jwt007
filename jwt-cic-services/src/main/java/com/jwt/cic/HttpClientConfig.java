package com.jwt.cic;

import org.apache.http.HttpHost;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by vishal.domale on 09-05-2017.
 */

@Configuration
public class HttpClientConfig {

    @Bean
    public HttpClient buildClient() throws MalformedURLException {
        CookieStore cookieStore = new BasicCookieStore();
        int timeout = 5;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create()
                .setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(config);
        if(System.getenv("http_proxy") != null && System.getenv("http_proxy").compareTo("") != 0 ){
            URL proxyURL = new URL(System.getenv("http_proxy"));
             HttpHost proxy = new HttpHost(proxyURL.getHost(), proxyURL.getPort());
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            httpClientBuilder.setRoutePlanner(routePlanner);
        }
        BasicClientCookie cookie = new BasicClientCookie("I4SUserLocale", "it");
        cookie.setVersion(0);
        cookie.setDomain("www.google.com");
        cookie.setPath("/trends");
        cookie.setSecure(false);
        cookieStore.addCookie(cookie);
        cookie = new BasicClientCookie("PREF", "");
        cookie.setVersion(0);
        cookie.setDomain("www.google.com");
        cookie.setPath("/trends");
        cookie.setSecure(false);
        cookieStore.addCookie(cookie);
        HttpClient httpClient = httpClientBuilder.build();

        return httpClient;
    }
}
