package com.jwt.cic;

import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.aws.autoconfigure.cache.ElastiCacheAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.context.config.annotation.EnableContextInstanceData;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.AmazonS3Client;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(exclude = {
        ContextStackAutoConfiguration.class,
        ElastiCacheAutoConfiguration.class
})
@EnableSwagger2
@EnableContextInstanceData
@EnableCaching
public class App extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public AmazonS3Client amazonS3Client() throws MalformedURLException {
        return new AmazonS3Client(clientConfiguration());
    }

    @Bean
    public ClientConfiguration clientConfiguration() throws MalformedURLException {
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        if (System.getenv("http_proxy") != null && System.getenv("http_proxy").compareTo("") != 0) {
            URL proxyURL = new URL(System.getenv("http_proxy"));
            clientConfiguration.setProxyHost(proxyURL.getHost());
            clientConfiguration.setProxyPort(proxyURL.getPort());
        }
        // clientConfiguration.setConnectionTimeout(60 * 1000);
        // clientConfiguration.setMaxConnections(500);
        // clientConfiguration.setSocketTimeout(60 * 1000);
        // clientConfiguration.setMaxErrorRetry(10);

        return clientConfiguration.withConnectionTimeout(60 * 1000).withSocketTimeout(60 * 1000).withMaxErrorRetry(0);
    }

    @Bean
    public RestTemplate restTemplate() throws MalformedURLException {
        //TODO: Create a proper configuration so that handles proxy settings
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        if (System.getenv("http_proxy") != null && System.getenv("http_proxy").compareTo("") != 0) {
            URL proxyURL = new URL(System.getenv("http_proxy"));
            Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(proxyURL.getHost(), proxyURL.getPort()));
            requestFactory.setProxy(proxy);
        }


        return new RestTemplate(requestFactory);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {

            @Override
            public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
                configurer.favorPathExtension(false);
            }

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**").allowedOrigins("*").allowedHeaders("*").allowedMethods("*");
            }
        };
    }
}
