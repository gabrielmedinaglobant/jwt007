package com.jwt.cic.app.service;

public interface CacheService {
	void evictAllInfegyQuestionsCache();
	void updateInfegyQuestionsCacheIfNecessary();
	void evictPeopleTalkingAboutOverallCache();
}
