package com.jwt.cic.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.app.service.CacheService;

@RestController
@RequestMapping("api/v1/cache")
public class CacheController {
	
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheController.class);
	
	private CacheService cacheService;
	
	@Autowired
	private CacheController(CacheService cacheService){
		this.cacheService = cacheService;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = { "/","" })
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(){
		LOG.info("Requesting cache deletion");
		cacheService.evictAllInfegyQuestionsCache();
	}
}
