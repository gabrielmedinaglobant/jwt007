package com.jwt.cic.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.jwt.cic.app.controller.CacheController;
import com.jwt.cic.questions.dao.OutputResultsDAO;

@Service
public class CacheServiceImpl implements CacheService {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheController.class);
	
	private CacheManager cacheManager;
	private OutputResultsDAO outputResultsDAO;
	private long lastCacheUpdatedTimestamp;
	
	private final static String GOOGLE_TRENDS_HOT_TRENDS = "google-trends-hot-trends-trending-searches-per-region";
	
	@Autowired
	public CacheServiceImpl(CacheManager cacheManager, OutputResultsDAO outputResultsDAO){
		this.cacheManager = cacheManager;
		this.outputResultsDAO = outputResultsDAO;
		updateLastCacheUpdatedTimestamp();
	}
	
	@Override
	public void evictAllInfegyQuestionsCache() {
		cacheManager
		.getCacheNames()
		.parallelStream()
		.filter( (name) -> name.compareTo(GOOGLE_TRENDS_HOT_TRENDS) != 0 )
		.forEach(name -> cacheManager.getCache(name).clear());		
		LOG.info("All cache from Infegy questions was cleared");
	}
	
	@Override
	@Scheduled(cron="${clear.all.cache.google.hottrends.cron}")
	public void evictPeopleTalkingAboutOverallCache(){
		cacheManager.getCache(GOOGLE_TRENDS_HOT_TRENDS).clear();
		LOG.info("All cache from GOOGLE_TRENDS_HOT_TRENDS was cleared");
	}
	
	@Scheduled(fixedRateString = "${clear.all.cache.if.s3.updated.fixed.rate}", initialDelayString = "${clear.all.cache.if.s3.updated.init.delay}")
	@Override
	public void updateInfegyQuestionsCacheIfNecessary(){
		LOG.info("Checking last updated time on S3, [lastCacheUpdatedTimestamp=" + lastCacheUpdatedTimestamp + "]");
		long lastUpdatedTimestamp = outputResultsDAO.get().lastUpdatedTimestamp.longValue();
		LOG.info("Last updated time on S3 was [lastUpdatedTimestamp=" + lastUpdatedTimestamp + "]");
		if(lastUpdatedTimestamp >= lastCacheUpdatedTimestamp){
			LOG.info("There were updates on S3, cache will be evicted");
			evictAllInfegyQuestionsCache();
		}
		updateLastCacheUpdatedTimestamp();
	}

	private void updateLastCacheUpdatedTimestamp(){
		lastCacheUpdatedTimestamp = System.currentTimeMillis()/1000;
	}
	
}
