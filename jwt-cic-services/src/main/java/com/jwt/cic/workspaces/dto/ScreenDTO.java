package com.jwt.cic.workspaces.dto;

import java.util.List;

public class ScreenDTO {
	private long id;
	private String name;

	private long workspaceId;
	private List<ScreenElementDTO> screenElements;
	
	public ScreenDTO(long id, String name, long workspaceId, List<ScreenElementDTO> screenElements) {
		this.id = id;
		this.name = name;
		this.screenElements = screenElements;
	}

	public ScreenDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(long workspaceId) {
		this.workspaceId = workspaceId;
	}

	public List<ScreenElementDTO> getScreenElements() {
		return screenElements;
	}

	public void setScreenElements(List<ScreenElementDTO> screenElements) {
		this.screenElements = screenElements;
	}
	

}
