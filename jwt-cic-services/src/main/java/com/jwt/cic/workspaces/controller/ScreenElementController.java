package com.jwt.cic.workspaces.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.workspaces.dto.ScreenElementDTO;
import com.jwt.cic.workspaces.service.ScreenElementService;

@RestController
@RequestMapping("api/v1/screen-elements")
public class ScreenElementController {
	private ScreenElementService screenElementService;

	@Autowired
	public ScreenElementController(ScreenElementService screenElementService) {
		this.screenElementService = screenElementService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	private List<ScreenElementDTO> getAll(){
		return screenElementService.getAll();
	}
	
}
