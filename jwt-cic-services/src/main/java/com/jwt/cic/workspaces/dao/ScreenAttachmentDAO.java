package com.jwt.cic.workspaces.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.workspaces.domain.ScreenAttachment;

public interface ScreenAttachmentDAO extends CrudRepository<ScreenAttachment, Long> {
	@Query("select sa from ScreenAttachment sa join fetch sa.screen s join fetch sa.screenElement se where s.id = :screenId and se.keyname = :screenElementKeyname")
	ScreenAttachment findOneByScreenIdAndScreenElementKeyname(@Param("screenId") long screenId, @Param("screenElementKeyname") String screenElementKeyname);
}
