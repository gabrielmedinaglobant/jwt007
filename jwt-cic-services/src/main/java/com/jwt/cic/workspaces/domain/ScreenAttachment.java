package com.jwt.cic.workspaces.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jwt.cic.domain.Record;

@Table(name="screen_attachment")
@Entity
public class ScreenAttachment implements Record {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_updated")
	private Date dateUpdated;
	private boolean active;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="screen_id")
	private Screen screen;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="screen_element_id")
	private ScreenElement screenElement;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Screen getScreen() {
		return screen;
	}
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
	public ScreenElement getScreenElement() {
		return screenElement;
	}
	public void setScreenElement(ScreenElement screenElement) {
		this.screenElement = screenElement;
	}
	
	
	
}
