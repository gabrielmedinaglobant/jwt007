package com.jwt.cic.workspaces.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.workspaces.dto.ScreenDTO;
import com.jwt.cic.workspaces.service.ScreenService;

@RestController
@RequestMapping("api/v1/screens")
public class ScreenController {
	
	private ScreenService screenService;
	
	@Autowired
	public ScreenController(ScreenService screenService) {
		this.screenService = screenService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/",""})
	public List<ScreenDTO> getAll(@RequestParam(value = "workspace-id", defaultValue = "-1", required=false) long workspaceId){
		List<ScreenDTO> result = new LinkedList<>();
		
		if(workspaceId > 0){
			result = screenService.getAllByWorkspaceId(workspaceId);
		} else {
			result = screenService.getAll();
		}
		
		return result;
	}
	@RequestMapping(method = RequestMethod.POST, value = {"/",""})
	public ScreenDTO save(@RequestBody ScreenDTO screenDTO){
		return screenService.save( screenDTO );
	}
	@RequestMapping(method = RequestMethod.DELETE, value = {"/{screen-id}"})
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable(value = "screen-id", required=true) long screenId){
		screenService.delete(screenId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/screen-elements/{screen-element-keyname}"})
	public List<ScreenDTO> getAllByScreenElement(
			@RequestParam(value = "workspace-id", required=true) long workspaceId,
			@PathVariable(value = "screen-element-keyname", required=true) String screenElementKeyname){
		return screenService.getAllByWorkspaceIdAndScreenElementKeyname(workspaceId, screenElementKeyname);
	}

	@RequestMapping(method = RequestMethod.POST, value = { "/{screen-id}/screen-elements/{screen-element-keyname}" })
	@ResponseStatus(value = HttpStatus.OK)
	public void attachScreenElement(
			@PathVariable(value = "screen-id", required = true) long screenId,
			@PathVariable(value = "screen-element-keyname", required = true) String screenElementKeyname) {
		screenService.attachScreenElement(screenId, screenElementKeyname);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = { "/{screen-id}/screen-elements/{screen-element-keyname}" })
	@ResponseStatus(value = HttpStatus.OK)
	public void detachScreenElement(
			@PathVariable(value = "screen-id", required = true) long screenId,
			@PathVariable(value = "screen-element-keyname", required = true) String screenElementKeyname) {
		screenService.detachScreenElement(screenId, screenElementKeyname);
	}
}
