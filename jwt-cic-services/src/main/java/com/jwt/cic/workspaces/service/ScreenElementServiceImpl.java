package com.jwt.cic.workspaces.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.workspaces.dao.ScreenElementDAO;
import com.jwt.cic.workspaces.dto.ScreenElementDTO;

@Service
public class ScreenElementServiceImpl implements ScreenElementService {

	private ScreenElementDAO screenElementDAO;
	
	@Autowired
	public ScreenElementServiceImpl(ScreenElementDAO screenElementDAO) {
		this.screenElementDAO = screenElementDAO;
	}

	@Override
	public List<ScreenElementDTO> getAll() {
		return screenElementDAO.findAll().stream().map( (screenElement) -> {
			return new ScreenElementDTO(
					screenElement.getId(), 
					screenElement.getKeyname(), 
					screenElement.getScreenElementType().getId(), 
					screenElement.getScreenElementType().getKeyname()
			); 
		} ).collect( Collectors.toList() );
	}

}
