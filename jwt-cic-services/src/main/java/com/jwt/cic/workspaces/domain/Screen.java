package com.jwt.cic.workspaces.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.jwt.cic.domain.Record;

@Entity
public class Screen implements Record {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_updated")
	private Date dateUpdated;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="workspace_id")
	private Workspace workspace;
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "screen")
	private List<ScreenAttachment> screenAttachments;
	
	private boolean active;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Workspace getWorkspace() {
		return workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	public List<ScreenAttachment> getScreenAttachments() {
		return screenAttachments;
	}

	public void setScreenAttachments(List<ScreenAttachment> screenAttachments) {
		this.screenAttachments = screenAttachments;
	}

	
		
	
}
