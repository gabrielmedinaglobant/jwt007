package com.jwt.cic.workspaces.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="ScreenElement entity not found") 
public class ScreenElementNotFoundException extends RuntimeException{
	
}
