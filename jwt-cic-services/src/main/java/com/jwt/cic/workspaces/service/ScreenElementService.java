package com.jwt.cic.workspaces.service;

import java.util.List;

import com.jwt.cic.workspaces.dto.ScreenElementDTO;

public interface ScreenElementService {

	List<ScreenElementDTO> getAll();

}
