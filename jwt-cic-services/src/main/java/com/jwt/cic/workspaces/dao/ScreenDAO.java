package com.jwt.cic.workspaces.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jwt.cic.workspaces.domain.Screen;

public interface ScreenDAO extends CrudRepository<Screen, Long> {
	// TODO: Check a way to filter inactive screen charts (sc.active == true)
	List<Screen> findAll();
	List<Screen> findAllByWorkspaceId(@Param("workspaceId") long workspaceId);
	@Query("select s from Screen s left join fetch s.screenAttachments sa join fetch sa.screenElement se join fetch s.workspace w where w.id = :workspaceId and se.keyname = :screenElementKeyname")
	List<Screen> findAllByWorkspaceIdAndScreenElementKeyname(@Param("workspaceId") long workspaceId, @Param("screenElementKeyname") String screenElementKeyname);
	@Query("select count(w.screens) from Workspace w where w.id = :workspaceId")
	long countByWorkspaceId(@Param("workspaceId") long workspaceId);
}
