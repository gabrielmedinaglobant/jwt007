package com.jwt.cic.workspaces.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jwt.cic.workspaces.domain.ScreenElement;

public interface ScreenElementDAO extends CrudRepository<ScreenElement, Long>{
	@Query("select se from ScreenElement se where se.active = true")
	List<ScreenElement> findAll();
	ScreenElement findOneByKeyname(String keyname);
}
