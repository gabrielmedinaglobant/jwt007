package com.jwt.cic.workspaces.controller.dto;

public class ScreenElementDTO {
	private long id;
	private String keyname;
	private long screenElementTypeId;
	private String screenElementTypeKeyname;
	
	public ScreenElementDTO() {
	}
	public ScreenElementDTO(long id, String keyname) {
		this.id = id;
		this.keyname = keyname;
	}
	
	public ScreenElementDTO(long id, String keyname, long screenElementTypeId, String screenElementTypeKeyname) {
		this.id = id;
		this.keyname = keyname;
		this.screenElementTypeId = screenElementTypeId;
		this.screenElementTypeKeyname = screenElementTypeKeyname;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public long getScreenElementTypeId() {
		return screenElementTypeId;
	}
	public void setScreenElementTypeId(long screenElementTypeId) {
		this.screenElementTypeId = screenElementTypeId;
	}
	public String getScreenElementTypeKeyname() {
		return screenElementTypeKeyname;
	}
	public void setScreenElementTypeKeyname(String screenElementTypeKeyname) {
		this.screenElementTypeKeyname = screenElementTypeKeyname;
	}
	
	
}
