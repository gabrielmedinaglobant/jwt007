package com.jwt.cic.workspaces.dao;

import org.springframework.data.repository.CrudRepository;

import com.jwt.cic.workspaces.domain.Workspace;

public interface WorkspaceDAO extends CrudRepository<Workspace, Long>{

}
