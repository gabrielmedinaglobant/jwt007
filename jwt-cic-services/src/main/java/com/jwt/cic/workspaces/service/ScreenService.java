package com.jwt.cic.workspaces.service;

import java.util.List;

import com.jwt.cic.workspaces.dto.ScreenDTO;

public interface ScreenService {
	List<ScreenDTO> getAll();
	List<ScreenDTO> getAllByWorkspaceId( long workspaceId );
	ScreenDTO save( ScreenDTO screenDTO );
	void delete( long screenId );
	List<ScreenDTO> getAllByWorkspaceIdAndScreenElementKeyname( long workspaceId, String screenElementKeyname );
	void attachScreenElement( long screenId, String screenElementKeyname );
	void detachScreenElement( long screenId, String screenElementKeyname );
	
}
