package com.jwt.cic.workspaces.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwt.cic.utils.RecordUtils;
import com.jwt.cic.workspaces.dao.ScreenAttachmentDAO;
import com.jwt.cic.workspaces.dao.ScreenDAO;
import com.jwt.cic.workspaces.dao.ScreenElementDAO;
import com.jwt.cic.workspaces.dao.WorkspaceDAO;
import com.jwt.cic.workspaces.domain.Screen;
import com.jwt.cic.workspaces.domain.ScreenAttachment;
import com.jwt.cic.workspaces.domain.ScreenElement;
import com.jwt.cic.workspaces.domain.Workspace;
import com.jwt.cic.workspaces.dto.ScreenDTO;
import com.jwt.cic.workspaces.dto.ScreenElementDTO;
import com.jwt.cic.workspaces.exceptions.ScreenAttachmentNotFoundException;
import com.jwt.cic.workspaces.exceptions.ScreenElementNotFoundException;
import com.jwt.cic.workspaces.exceptions.ScreenNotFoundException;
import com.jwt.cic.workspaces.exceptions.WorkspaceNotFoundException;

@Service
public class ScreenServiceImpl implements ScreenService {

	private final String SCREEN_NAME_PREFIX = "Screen ";
	
	private ScreenDAO screenDAO;
	private ScreenAttachmentDAO screenAttachmentDAO;
	private ScreenElementDAO screenElementDAO;
	private WorkspaceDAO workspaceDAO;
	
	@Autowired
	public ScreenServiceImpl(ScreenDAO screenDAO, ScreenAttachmentDAO screenAttachmentDAO,
			ScreenElementDAO screenElementDAO, WorkspaceDAO workspaceDAO) {
		this.screenDAO = screenDAO;
		this.screenAttachmentDAO = screenAttachmentDAO;
		this.screenElementDAO = screenElementDAO;
		this.workspaceDAO = workspaceDAO;
	}

	@Override
	public List<ScreenDTO> getAll() {
		return assembleScreenDTOList( screenDAO.findAll() );
	}


	@Override
	public List<ScreenDTO> getAllByWorkspaceId(long workspaceId) {
		return assembleScreenDTOList( screenDAO.findAllByWorkspaceId(workspaceId) );
	}
	
	@Override
	public ScreenDTO save( ScreenDTO screenDTO ) {
		Screen savedEntity = screenDAO.save( buildScreenEntity(screenDTO) );
		ScreenDTO responseDTO = new ScreenDTO();
		
		responseDTO.setId( savedEntity.getId() );
		responseDTO.setName( savedEntity.getName() );
		responseDTO.setWorkspaceId( savedEntity.getWorkspace().getId() );
		
		return responseDTO;
	}

	@Override
	public void delete(long screenId) {
		Screen screen = null;
		
		screen = screenDAO.findOne(screenId);
		if(screen == null){
			throw new ScreenNotFoundException();
		}
		RecordUtils.setDeleteFlags( screen );
		screen.getScreenAttachments().forEach( (screenAttachment) -> {
			RecordUtils.setDeleteFlags( screenAttachment );
		});
		
		
		screenAttachmentDAO.save( screen.getScreenAttachments() );
		screenDAO.save( screen );
		
	}
	private List<ScreenDTO> assembleScreenDTOList(List<Screen> screens){
		return screens
				.stream()
				.filter( (screen) -> { 
					return screen.isActive() == true; // TODO: Move this to JPQL Query
				} )
				.map( (screen) -> {
					ScreenDTO screenDTO = new ScreenDTO();
					screenDTO.setId( screen.getId() );
					screenDTO.setName( screen.getName() );
					screenDTO.setWorkspaceId( screen.getWorkspace().getId() );
					screenDTO.setScreenElements( assembleScreenElementsDTOList( screen.getScreenAttachments() ) );
					return screenDTO;
				} )
				.collect( Collectors.toList() );
	}
	
	private List<ScreenElementDTO> assembleScreenElementsDTOList(List<ScreenAttachment> screenAttachments) {
		return screenAttachments
			.stream()
			.filter( (screenAttachment) -> { 
				return screenAttachment.isActive() == true; // TODO: Move this to JPQL Query
			} )
				.map((screenAttachment) -> {
					return new ScreenElementDTO(screenAttachment.getScreenElement().getId(),
							screenAttachment.getScreenElement().getKeyname(),
							screenAttachment.getScreenElement().getScreenElementType().getId(),
							screenAttachment.getScreenElement().getScreenElementType().getKeyname());
			} ).collect( Collectors.toList() );
	}


	
	private Screen buildScreenEntity(ScreenDTO screenDTO){
		long workspaceId = screenDTO.getWorkspaceId() ;
		Screen screen = new Screen();
		Workspace workspace = workspaceDAO.findOne( workspaceId );
		String workspaceName = "";
		
		if(workspace == null){
			throw new WorkspaceNotFoundException();
		}
		
		//if no name specified on workspace, a name is generated
		workspaceName = screenDTO.getName();
		if(workspaceName == null || workspaceName.trim().compareTo("") == 0){
			screen.setName( SCREEN_NAME_PREFIX + (count(workspaceId)+1) );
		}
		
		screen.setName( screenDTO.getName() );
		screen.setWorkspace( workspace );
		RecordUtils.setCreateFlags( screen );
		
		return screen;
	}
	
	private long count(long workspaceId){
		return screenDAO.countByWorkspaceId(workspaceId);
	}

	@Override
	public List<ScreenDTO> getAllByWorkspaceIdAndScreenElementKeyname(long workspaceId, String screenElementKeyname) {
		return assembleScreenDTOList( screenDAO.findAllByWorkspaceIdAndScreenElementKeyname(workspaceId, screenElementKeyname));
	}

	@Override
	public void attachScreenElement(long screenId, String screenElementKeyname) {
        //We need to find available Screen and an available chartKeyname
		Screen screen = null;
		ScreenElement screenElement = null;
		
		screen = screenDAO.findOne(screenId);
		if (screen == null) {
			throw new ScreenNotFoundException();
		}
		screenElement = screenElementDAO.findOneByKeyname(screenElementKeyname);
		if(screenElement == null) {
			throw new ScreenElementNotFoundException();
		}
		
		ScreenAttachment screenAttachment = new ScreenAttachment();
		
		screenAttachment.setScreen(screen);
		screenAttachment.setScreenElement(screenElement);
		RecordUtils.setCreateFlags( screenAttachment );
		
		screenAttachmentDAO.save( screenAttachment );
	}

	@Override
	public void detachScreenElement(long screenId, String screenElementKeyname) {
		ScreenAttachment screenAttachment = screenAttachmentDAO.findOneByScreenIdAndScreenElementKeyname(screenId, screenElementKeyname);
		
		if(screenAttachment == null){
			throw new ScreenAttachmentNotFoundException();
		}
		RecordUtils.setDeleteFlags(screenAttachment);
		screenAttachmentDAO.save(screenAttachment);		
	}
	
}
