package com.jwt.cic.workspaces.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.jwt.cic.domain.Record;

@Table(name="screen_element")
@Entity
public class ScreenElement implements Record {

	@Id
	private long id;
	private String keyname;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_updated")
	private Date dateUpdated;
	private boolean active;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="screen_element_type_id")
	private ScreenElementType screenElementType;
	@OneToMany(fetch=FetchType.LAZY, mappedBy="screenElement")
	private List<ScreenAttachment> screenAttachments;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public ScreenElementType getScreenElementType() {
		return screenElementType;
	}
	public void setScreenElementType(ScreenElementType screenElementType) {
		this.screenElementType = screenElementType;
	}
	public List<ScreenAttachment> getScreenAttachments() {
		return screenAttachments;
	}
	public void setScreenAttachments(List<ScreenAttachment> screenAttachments) {
		this.screenAttachments = screenAttachments;
	}
	
	

}
