package com.jwt.cic.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.io.CharStreams;
import com.jwt.cic.exceptions.FetchingResourceException;
import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;
import com.jwt.cic.exceptions.InvalidJsonUrlException;

@Component
public class ResourceFetcher {
	
	private final Logger log = LoggerFactory.getLogger(ResourceFetcher.class);
	
	private ResourceLoader resourceLoader;
	
	@Autowired
	public ResourceFetcher(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	public String fetch(String resourceLocation) {

		log.info("Data is being pulled from the following AWS S3 bucket: " + resourceLocation);
		Resource resource = this.resourceLoader.getResource(resourceLocation);
		
		try {
			log.info("Enter try after resource loader");
			InputStream inputStream = new BufferedInputStream(resource.getInputStream());
			log.info("InputStream nstance has been obtained");
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			log.info("InputStreamReader instance has been obtained");
			String stringResult = CharStreams.toString(inputStreamReader);
			log.info("stringResult has been obtained");
			
			return stringResult;
						
		} catch (IOException e) {
			e.printStackTrace();
			throw new FetchingResourceException();
		}
	}
	
	public <T> List<T> fetchObjects(String resourceLocation, Class<T> clazz){
		ObjectMapper om = new ObjectMapper();
		List<T> result = null;
		try {
			log.info(resourceLocation);
			result = om.readValue( fetch(resourceLocation),
					TypeFactory.defaultInstance().constructCollectionLikeType(List.class, clazz));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonUrlException();
		} catch (JsonParseException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		}
		return result;
	}
	
	public <T> T fetchObject(String resourceLocation, Class<T> clazz){
		ObjectMapper om = new ObjectMapper();
		T result = null;
		try {
			log.info(resourceLocation);
			result = om.readValue( fetch(resourceLocation) , clazz);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonUrlException();
		} catch (JsonParseException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidJsonFromDataSourceException();
		}
		return result;
	}
}
