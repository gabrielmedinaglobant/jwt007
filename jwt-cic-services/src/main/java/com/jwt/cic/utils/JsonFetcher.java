package com.jwt.cic.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.jwt.cic.exceptions.InvalidJsonFromDataSourceException;

@Component
public class JsonFetcher {
	
	private RestTemplate restTemplate;
	private ResourceFetcher resourceFetcher;
	
	@Autowired
	public JsonFetcher(RestTemplate restTemplate, ResourceFetcher resourceFetcher){
		this.restTemplate = restTemplate;
		this.resourceFetcher = resourceFetcher;
	}
	
	public JSONObject fetchFromResource(String resourceLocation){
		JSONParser parser = new JSONParser();
		String jsonString = this.resourceFetcher.fetch(resourceLocation);
		JSONObject result;
		
		try {
			result = (JSONObject)parser.parse(jsonString);
		} catch (ParseException e) {
			throw new InvalidJsonFromDataSourceException();
		}
		
		return result;
	}
	
	public JSONObject fetchFromUrl(String url){
		JSONParser parser = new JSONParser();
		String jsonString = restTemplate.getForObject(url, String.class);
		JSONObject result;
		
		try {
			result = (JSONObject)parser.parse(jsonString);
		} catch (ParseException e) {
			throw new InvalidJsonFromDataSourceException();
		}
		
		return result;
	}
		
}
