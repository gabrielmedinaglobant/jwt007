package com.jwt.cic.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component("doubleDigitsPercentageRoundingStrategy")
public class DoubleDigitsPercentageRoundingStrategy implements RoundingStrategy {
	/* (non-Javadoc)
	 * @see com.jwt.cic.utils.RoundingStrategy#round(java.math.BigDecimal)
	 */
	 private final Logger log = LoggerFactory.getLogger(DoubleDigitsPercentageRoundingStrategy.class);
	@Override
	public BigDecimal round(BigDecimal number){
		log.info("Number: " + number.toString());
		BigDecimal result;
		if(number.compareTo(BigDecimal.TEN) > 0){
			result = number.setScale(0, RoundingMode.HALF_UP);
			log.info("Result: " + result.toString());
		} else {
			result = number.setScale(1, RoundingMode.HALF_UP);
			log.info("Result: : " + result.toString());
		}
		return result;
	}
}
