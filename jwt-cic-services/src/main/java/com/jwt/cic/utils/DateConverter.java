package com.jwt.cic.utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public final class DateConverter {
	
	public static BigDecimal fromISODateToEpochSecondsAsBigDecimal(String isoDate){
		return BigDecimal.valueOf( fromISODateToEpochSeconds(isoDate) );
	}
	
	public static BigDecimal fromISODateToEpochSecondsTrimmedAsBigDecimal(String isoDate){
		return BigDecimal.valueOf( fromISODateToEpochSecondsTrimmed(isoDate) );
	}
	
	public static BigDecimal fromUSHyphenDateToEpochSecondsAsBigDecimal(String strDate){
		return BigDecimal.valueOf( fromUSHyphenDateToEpochSeconds(strDate) );
	}
	
	public static long fromISODateToEpochSeconds(String isoDate){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime dateTime = LocalDateTime.parse(isoDate, formatter);
		return dateTime.toEpochSecond( ZoneOffset.UTC );
	}
	
	public static long fromISODateToEpochSecondsTrimmed(String isoDate){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime dateTime = LocalDateTime.parse(isoDate, formatter);
		return dateTime.truncatedTo(ChronoUnit.DAYS).toEpochSecond( ZoneOffset.UTC );
	}
	
	public static long fromUSHyphenDateToEpochSeconds(String strDate){
		strDate = strDate + "T00:00:00Z";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime dateTime = LocalDateTime.parse(strDate, formatter);
		return dateTime.toEpochSecond( ZoneOffset.UTC );
	}

	public static long fromDateTOSecond(Date date){
		LocalDateTime dateTime = 	LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
		return dateTime.toEpochSecond( ZoneOffset.UTC );
	}
}
