package com.jwt.cic.utils;

import java.math.BigDecimal;

public interface RoundingStrategy {

	BigDecimal round(BigDecimal number);

}