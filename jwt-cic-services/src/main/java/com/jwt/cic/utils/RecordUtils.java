package com.jwt.cic.utils;

import org.joda.time.DateTime;

import com.jwt.cic.domain.Record;

public class RecordUtils {
	/***
	 * Sets timestamps and active flags to a Record entity 
	 * correspoding to its creation
	 * Consider this method mutates "t" object
	 * 
	 * @param t
	 * @return
	 */
	public static <T extends Record> T setCreateFlags(T t){
		t.setActive(true);
		t.setDateCreated( DateTime.now().toDate() );
		t.setDateUpdated( DateTime.now().toDate() );
		return t;
	}
	
	public static <T extends Record> T setUpdateFlags(T t){
		t.setDateUpdated( DateTime.now().toDate() );
		return t;
	}
	
	public static <T extends Record> T setDeleteFlags(T t){
		t.setActive(false);
		t.setDateUpdated( DateTime.now().toDate() );
		return t;
	}
}
