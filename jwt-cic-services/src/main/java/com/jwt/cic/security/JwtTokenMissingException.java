package com.jwt.cic.security;

public class JwtTokenMissingException extends RuntimeException {

	public JwtTokenMissingException(String string) {
		super(string);
	}

}
