package com.jwt.cic.security;

import java.security.Principal;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtUserDetails implements UserDetails {

	private String nameId;
	private Collection<? extends GrantedAuthority> authorities;
	private String token;
	
	public JwtUserDetails(String nameId, Collection<? extends GrantedAuthority> authorities, String token) {
		this.nameId = nameId;
		this.authorities = authorities;
		this.token = token;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return "";
	}

	@Override
	public String getUsername() {
		return nameId;
	}

	@Override
	public boolean isAccountNonExpired() {
		// Not needed, handled by external authentication
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// Not needed, handled by external authentication
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// Not needed, handled by external authentication
		return true;
	}

	@Override
	public boolean isEnabled() {
		// Not needed, handled by external authentication
		return true;
	}

}
