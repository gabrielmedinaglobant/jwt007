package com.jwt.cic.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.cic.security.dto.JwtUserDetailsDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String secret;
    private static ObjectMapper mapper = new ObjectMapper();
    
    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     * 
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public JwtUserDetailsDTO parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            JwtUserDetailsDTO u = new JwtUserDetailsDTO();
            u.setNameId( body.get("nameId", String.class) );
            //u.setRoles( buildRoles(body.get("roles", String.class)) );
            u.setRole( body.get("role", String.class) );

            return u;

        } catch (JwtException | ClassCastException e) {
        	e.printStackTrace();
            throw new JwtParsingException();
        }
    }

//    private List<String> buildRoles(String string) throws JsonParseException, JsonMappingException, IOException {
//    	List<String> list;
//			list = mapper.readValue(string, new TypeReference<ArrayList<String>>(){});
//		return list;
//	}

	/**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     * 
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(JwtUserDetailsDTO u) {
        Claims claims = Jwts.claims().setSubject(u.getNameId());
        claims.put("nameId", u.getNameId() + "");
        claims.put("role", u.getRole());
//        try {
//        	claims.put("role", mapper.writeValueAsString(u.getRoles()));
//		} catch (JsonProcessingException e) {
//			throw new JwtGeneratingTokenException();
//		}

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
}
