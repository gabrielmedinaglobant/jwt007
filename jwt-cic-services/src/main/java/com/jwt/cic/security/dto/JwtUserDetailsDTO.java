package com.jwt.cic.security.dto;

import java.util.Arrays;
import java.util.List;

public class JwtUserDetailsDTO {
	private String nameId;
//	private List<String> roles;
	private String role;
	
	public JwtUserDetailsDTO(){
		
	}
//	public JwtUserDetailsDTO(String nameId, String role) {
//		this.nameId = nameId;
//		this.roles = Arrays.asList( new String[]{ role } );
//	}
//	public JwtUserDetailsDTO(String nameId, List<String> roles) {
//		this.nameId = nameId;
//		this.roles = roles;
//	}
	public JwtUserDetailsDTO(String nameId, String role) {
		this.nameId = nameId;
		this.role = role;
	}
	
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
//	public List<String> getRoles() {
//		return roles;
//	}
//	public void setRoles(List<String> roles) {
//		this.roles = roles;
//	}
	
	
}
