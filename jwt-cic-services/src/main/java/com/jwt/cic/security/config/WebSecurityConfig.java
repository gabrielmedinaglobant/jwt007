package com.jwt.cic.security.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.context.annotation.Bean;

import com.jwt.cic.security.JwtAuthenticationFilter;
import com.jwt.cic.security.JwtAuthenticationProvider;
import com.jwt.cic.security.RestAuthenticationEntryPoint;
import com.jwt.cic.security.SkipPathRequestMatcher;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	public static final String SWAGGER_ENTRY_POINT = "/api/v1/authenticate";
	public static final String H2_ENTRY_POINT_0 = "/h2-console";
	public static final String H2_ENTRY_POINT_1 = "/h2-console/**";
	public static final String AUTHENTICATE_ENTRY_POINT = "/api/v1/authenticate";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/api/**";
    public static final String TOKEN_REFRESH_ENTRY_POINT = "/api/v1/authenticate/refresh";
	
	@Autowired
	private RestAuthenticationEntryPoint authenticationEntryPoint;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	@Autowired
	private JwtAuthenticationProvider jwtAuthenticationProvider;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf().disable()
        	.exceptionHandling()
            .authenticationEntryPoint(this.authenticationEntryPoint)
            .and()
    			.headers().frameOptions().sameOrigin()
            .and()
	            .sessionManagement()
	            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            	.authorizeRequests()
            		.antMatchers("/**").permitAll();
            		// .antMatchers("/api/**").authenticated()
	                // .antMatchers("/api/v1/screens", "/api/v1/screens/**").hasRole("ANALYST")
            // .and()
            	// .addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
	
    
	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter(){
		JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter();
		
		jwtAuthenticationFilter.setAuthenticationManager(authenticationManager);
		jwtAuthenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
		jwtAuthenticationFilter.setRequiresAuthenticationRequestMatcher(requestMatcher());
		
		return jwtAuthenticationFilter;
	}
	
	@Bean RequestMatcher requestMatcher(){
		List<String> pathsToSkip = Arrays.asList(SWAGGER_ENTRY_POINT, H2_ENTRY_POINT_0, H2_ENTRY_POINT_1, AUTHENTICATE_ENTRY_POINT, TOKEN_REFRESH_ENTRY_POINT);
		RequestMatcher rm = new SkipPathRequestMatcher(pathsToSkip, TOKEN_BASED_AUTH_ENTRY_POINT);
		return rm;
	}
}
