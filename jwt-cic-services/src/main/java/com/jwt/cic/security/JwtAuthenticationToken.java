package com.jwt.cic.security;

import java.security.Principal;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1L;
	
	private String token;
	
	public JwtAuthenticationToken() {
		super(new Principal(){
			@Override
			public String getName() {
				return "";
			}}, "");
	}
	
	public JwtAuthenticationToken(String token) {
		super(new Principal(){
			@Override
			public String getName() {
				return token;
			}}, token);
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	@Override
	public String getName() {
		return token;
	}
	
}
