package com.jwt.cic.security;

public class JwtTokenMalformedException extends RuntimeException {

	public JwtTokenMalformedException(String string) {
		super(string);
	}

}
