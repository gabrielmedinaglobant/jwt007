package com.jwt.cic.membership.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.cic.security.JwtUtil;
import com.jwt.cic.security.dto.JwtUserDetailsDTO;

@RestController
@RequestMapping("api/v1/authenticate")
public class AuthenticateController {
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@RequestMapping(method = RequestMethod.POST, value = { "/","" })
	public AuthenticateDTO authenticate(@RequestBody AuthenticateRequestDTO authenticateRequestDTO){
		if( authenticateRequestDTO.userNameId.compareTo("john.doe") == 0 )
			return new AuthenticateDTO( jwtUtil.generateToken( new JwtUserDetailsDTO("john.doe","ROLE_ANALYST") ) );
			// return new AuthenticateDTO("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lSWQiOiJqb2huLmRvZSIsInJvbGVzIjpbIlJPTEVfQU5BTFlTVCJdfQ.c7smTe0Xt17acD9J2MS1klxzpx-JRHge_wYGBglTITk");
		else if( authenticateRequestDTO.userNameId.compareTo("sarah.jones") == 0 ){
			return new AuthenticateDTO( jwtUtil.generateToken( new JwtUserDetailsDTO("sarah.jones","ROLE_STRATEGIC") ) );
			// return new AuthenticateDTO("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lSWQiOiJzYXJhaC5qb25lcyIsInJvbGVzIjpbIlJPTEVfRFVNTVkiXX0.lQljviTHj1o10mEMn1xzHScJZanGVCKbo82EI_YI0D4");
		}
		else {
			throw new BadCrentialsException();
		}
	}
	
	public static class AuthenticateDTO {
		private String token;

		public AuthenticateDTO(String token) {
			this.token = token;
		}

		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		
	}
	
	public static class AuthenticateRequestDTO {
		private String userNameId;
		private String password;
		
		public AuthenticateRequestDTO() {

		}
		public AuthenticateRequestDTO(String userNameId, String password) {
			this.userNameId = userNameId;
			this.password = password;
		}
		public String getUserNameId() {
			return userNameId;
		}
		public void setUserNameId(String userNameId) {
			this.userNameId = userNameId;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
	}
	
	@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Bad Credentials") 
	public static class BadCrentialsException extends RuntimeException {
		
	}
}
