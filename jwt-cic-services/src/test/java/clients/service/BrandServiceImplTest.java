package clients.service;

import com.jwt.cic.clients.dao.BrandDAO;
import com.jwt.cic.clients.domain.Brand;
import com.jwt.cic.clients.domain.Client;
import com.jwt.cic.clients.dto.BrandDTO;
import com.jwt.cic.clients.service.BrandServiceImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@Ignore
public class BrandServiceImplTest {

    @Mock
    private BrandDAO brandDAO;

    @InjectMocks
    private BrandServiceImpl brandService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(brandDAO.findAll()).thenReturn(buildMockBrandList());
        Mockito.when(brandDAO.findAllByNameAndClientName("M&M","Mars")).thenReturn(buildMockBrandList());
    }

    private List<Brand> buildMockBrandList() {
        List<Brand> brandList = new ArrayList<>();
        Client client = new Client();
        client.setId(3);
        client.setName("Mars");
        Brand brand = new Brand();
        brand.setClient(client);
        brand.setId(1);
        brand.setName("M&M");
        brandList.add(brand);
        return brandList;
    }

    @Test
    public void testGetAll() {
        List<BrandDTO> brandDTOS = brandService.getAll();
        assertNotNull(brandDTOS);
        assertEquals(1,brandDTOS.size());
        assertEquals("Mars",brandDTOS.get(0).getClientName());
        assertEquals(3,brandDTOS.get(0).getClientId());
        assertEquals("M&M",brandDTOS.get(0).getName());
        assertEquals(1,brandDTOS.get(0).getId().longValue());

    }

    @Test
    public void testFindAllByNameAndClientName() {
        List<BrandDTO> brandDTOS = brandService.findAllByNameAndClientName("M&M","Mars");
        assertNotNull(brandDTOS);
        assertEquals(1,brandDTOS.size());
        assertEquals("Mars",brandDTOS.get(0).getClientName());
        assertEquals(3,brandDTOS.get(0).getClientId());
        assertEquals("M&M",brandDTOS.get(0).getName());
        assertEquals(1,brandDTOS.get(0).getId().longValue());
    }
}
