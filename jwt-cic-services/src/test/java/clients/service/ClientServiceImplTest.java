package clients.service;

import com.jwt.cic.clients.dao.ClientDAO;
import com.jwt.cic.clients.domain.Client;
import com.jwt.cic.clients.dto.ClientDTO;
import com.jwt.cic.clients.service.ClientServiceImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@Ignore
public class ClientServiceImplTest {

    @Mock
    private ClientDAO clientDAO;

    @InjectMocks
    private ClientServiceImpl clientService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(clientDAO.findAll()).thenReturn(buildMockClientList());
        Mockito.when(clientDAO.findAllByNameIgnoreCase("Mars")).thenReturn(buildMockClientList());

    }

    private List<Client> buildMockClientList() {
        List<Client> clientList = new ArrayList<>();
        clientList.add(buildMockFirstClient());
        return clientList;
    }

    private Client buildMockFirstClient() {
        Client client = new Client();
        client.setId(3);
        client.setName("Mars");
        return client;
    }

    @Test
    public void testGetAll() {

        List<ClientDTO> clientDTOList = clientService.getAll();
        assertNotNull(clientDTOList);
        assertEquals(1, clientDTOList.size());
        assertEquals("Mars", clientDTOList.get(0).getName());
        assertEquals(3, clientDTOList.get(0).getId().longValue());


    }

    @Test
    public void testGetAllByName() {
        List<ClientDTO> clientDTOList = clientService.getAllByName("Mars");
        assertNotNull(clientDTOList);
        assertEquals(1, clientDTOList.size());
        assertEquals("Mars", clientDTOList.get(0).getName());
        assertEquals(3, clientDTOList.get(0).getId().longValue());

    }


}
