package clients.service;

import com.jwt.cic.clients.dao.RoleDAO;
import com.jwt.cic.clients.domain.Role;
import com.jwt.cic.clients.dto.RoleDTO;
import com.jwt.cic.clients.service.RoleServiceImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@Ignore
public class RoleServiceImplTest {

    @Mock
    private RoleDAO roleDAO;

    @InjectMocks
    private RoleServiceImpl roleService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(roleDAO.findAll()).thenReturn(buildMockRoleList());
    }

    @Test
    public void testGetAll() {
        List<RoleDTO>  roleDTOList = roleService.getAll();
        assertNotNull(roleDTOList);
        assertEquals(2,roleDTOList.size());
        assertEquals(1,roleDTOList.get(0).getId());
        assertEquals("ROLE_ANALYST",roleDTOList.get(0).getDisplayName());
        assertEquals("Analyst",roleDTOList.get(0).getValue());
        assertEquals(2,roleDTOList.get(1).getId());
        assertEquals("ROLE_STRATEGIC",roleDTOList.get(1).getDisplayName());
        assertEquals("Strategic",roleDTOList.get(1).getValue());


    }

    private List<Role> buildMockRoleList() {
        List<Role> roles = new ArrayList<>();
        roles.add(buildMockfirstRole());
        roles.add(buildMockSecondRole());
        return roles;
    }

    private Role buildMockfirstRole() {
        Role role = new Role();
        role.setId(1);
        role.setDisplayName("ROLE_ANALYST");
        role.setValue("Analyst");
        role.setActive(true);
        return role;
    }

    private Role buildMockSecondRole() {
        Role role = new Role();
        role.setId(2);
        role.setDisplayName("ROLE_STRATEGIC");
        role.setValue("Strategic");
        role.setActive(true);
        return role;
    }
}
