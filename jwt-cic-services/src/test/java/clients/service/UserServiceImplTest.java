package clients.service;

import com.jwt.cic.clients.dao.UserDAO;
import com.jwt.cic.clients.domain.*;
import com.jwt.cic.clients.dto.UserDTO;
import com.jwt.cic.clients.dto.UserProjectDTO;
import com.jwt.cic.clients.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@Ignore
public class UserServiceImplTest {

    @Mock
    private UserDAO userDAO;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(userDAO.findAllByNameAndLastNameAndEmail("gabriel", "medina", "gabriel")).thenReturn(buildMockUserList());
        Mockito.when(userDAO.findOneWithProjectsByNameId("gabriel.medina")).thenReturn(buildMockFirstUser());
        Mockito.when(userDAO.findAllByNameOrLastNameOrEmail("gabriel")).thenReturn(buildMockUserList());

    }

    @Test
    public void testGetAll() {

        List<UserDTO>  userDTOList = userService.getAll("gabriel", "medina", "gabriel");
        assertNotNull(userDTOList);
        assertEquals(1,userDTOList.size());
        assertEquals("gabriel.medina@globant.com",userDTOList.get(0).getEmail());
        assertEquals("medina",userDTOList.get(0).getLastName());
        assertEquals("gabriel",userDTOList.get(0).getName());
        assertEquals(1,userDTOList.get(0).getId());
        assertEquals("gabriel.medina",userDTOList.get(0).getNameId());
    }

    @Test
    public void testGet() {
        UserDTO  userDTO = userService.get("gabriel.medina");
        assertNotNull(userDTO);
        assertEquals("gabriel.medina@globant.com",userDTO.getEmail());
        assertEquals("medina",userDTO.getLastName());
        assertEquals("gabriel",userDTO.getName());
        assertEquals(1,userDTO.getId());
        assertEquals("gabriel.medina",userDTO.getNameId());

        List<UserProjectDTO> projects = userDTO.getProjects();
        assertNotNull(projects);
        assertEquals(2,projects.size());

        assertEquals("M&M",projects.get(0).getBrandName());
        assertEquals("Mars",projects.get(0).getClientName());
        assertEquals("Kryspy",projects.get(0).getProjectName());
        assertEquals("Analyst",projects.get(0).getRole());

        assertEquals("Galaxy",projects.get(1).getBrandName());
        assertEquals("Mars",projects.get(1).getClientName());
        assertEquals("Peanut",projects.get(1).getProjectName());
        assertEquals("Analyst",projects.get(1).getRole());
    }

    @Test
    public void testGetAllByNameOrLastNameOrEmail() {

        List<UserDTO>  userDTOList = userService.getAllByNameOrLastNameOrEmail("gabriel");
        assertNotNull(userDTOList);
        assertEquals(1,userDTOList.size());
        assertEquals("gabriel.medina@globant.com",userDTOList.get(0).getEmail());
        assertEquals("medina",userDTOList.get(0).getLastName());
        assertEquals("gabriel",userDTOList.get(0).getName());
        assertEquals(1,userDTOList.get(0).getId());
        assertEquals("gabriel.medina",userDTOList.get(0).getNameId());

    }


    private List<User> buildMockUserList() {
        List<User> users = new ArrayList<>();
        users.add(buildMockFirstUser());
        return users;
    }

    private User buildMockFirstUser() {
        User user = new User();
        user.setId(1);
        user.setName("gabriel");
        user.setEmail("gabriel.medina@globant.com");
        user.setLastName("medina");
        user.setNameId("gabriel.medina");
        user.setActive(true);
        user.setUserProjects(buildMockUserProjectList());
        return user;
    }

    private List<UserProject> buildMockUserProjectList() {
        List<UserProject> userProjects = new ArrayList<>();
        userProjects.add(buildMockFirstUserProject());
        userProjects.add(buildMockSecondUserProject());
        return userProjects;
    }

    private UserProject buildMockFirstUserProject() {
        UserProject userProject = new UserProject();
        userProject.setActive(true);
        userProject.setId(1);
        userProject.setProject(buildMockFirstProject());
        userProject.setRole(buildMockfirstRole());
        return userProject;
    }

    private UserProject buildMockSecondUserProject() {
        UserProject userProject = new UserProject();
        userProject.setActive(true);
        userProject.setId(2);
        userProject.setProject(buildMockSecondProject());
        userProject.setRole(buildMockfirstRole());
        return userProject;
    }

    private Project buildMockFirstProject() {
        Project project = new Project();
        project.setActive(true);
        project.setId(1);
        project.setName("Kryspy");
        project.setBrand(buildMockFirstBrand());
        return project;
    }
    private Project buildMockSecondProject() {
        Project project = new Project();
        project.setActive(true);
        project.setId(2);
        project.setName("Peanut");
        project.setBrand(buildMockSecondBrand());
        return project;
    }

    private Brand buildMockFirstBrand() {
        Client client = new Client();
        client.setId(3);
        client.setName("Mars");
        Brand brand = new Brand();
        brand.setClient(client);
        brand.setId(1);
        brand.setName("M&M");
        return brand;
    }

    private Brand buildMockSecondBrand() {
        Client client = new Client();
        client.setId(3);
        client.setName("Mars");
        Brand brand = new Brand();
        brand.setClient(client);
        brand.setId(2);
        brand.setName("Galaxy");
        return brand;
    }

    private Role buildMockfirstRole() {
        Role role = new Role();
        role.setId(1);
        role.setDisplayName("ROLE_ANALYST");
        role.setValue("Analyst");
        role.setActive(true);
        return role;
    }

}



