package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dao.QuestionJsonDAO;
import com.jwt.cic.questions.dto.SentimentChangeDayOverDayDTO;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by vishal.domale on 28-04-2017.
 */
@RunWith(SpringRunner.class)
@Ignore
public class SentimentChangeDayOverDayServiceImplTest {

    @Mock
    private QuestionJsonDAO questionDAO;

    @InjectMocks
    private SentimentChangeDayOverDayServiceImpl sentimentChangeDayOverDayService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(questionDAO.getSentimentChangeDayOverDay()).thenReturn(buildMockData());
    }


    @Test
    public void testGet() {

        SentimentChangeDayOverDayDTO actualResult=(SentimentChangeDayOverDayDTO)sentimentChangeDayOverDayService.get().getValue();
        assertNotNull(actualResult);
        assertEquals(BigDecimal.valueOf(1481500800),actualResult.getSplineChart().getLines().get(0).getValues().get(0).getValue()[0]);
        assertEquals(BigDecimal.valueOf(59.64912280701754),actualResult.getSplineChart().getLines().get(0).getValues().get(0).getValue()[1]);
        assertEquals(BigDecimal.valueOf(1481500800),actualResult.getSplineChart().getLines().get(1).getValues().get(0).getValue()[0]);
        assertEquals(BigDecimal.valueOf(28.07017543859649),actualResult.getSplineChart().getLines().get(1).getValues().get(0).getValue()[1]);
        assertEquals(BigDecimal.valueOf(1481500800),actualResult.getSplineChart().getLines().get(2).getValues().get(0).getValue()[0]);
        assertEquals(BigDecimal.valueOf(12.280701754385964),actualResult.getSplineChart().getLines().get(2).getValues().get(0).getValue()[1]);

    }

    private static JSONObject buildMockData() {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject =null;
        String jsonString ="{\"values\": [{\"date\": \"2016-12-12T00:00:00Z\",   \"positive_subject_sentences_prom\": 59.64912280701754,\"mixed_subject_sentences_prom\": 12.280701754385964,\"negative_subject_sentences_prom\": 28.07017543859649}]}";
        try {
            jsonObject=(JSONObject)parser.parse(jsonString);

        } catch (ParseException pe) {
            pe.getStackTrace();
        }
        return jsonObject;
    }
}
