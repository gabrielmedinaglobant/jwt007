package com.jwt.cic.questions.service;

import com.jwt.cic.charts.dto.CandleStickValueDTO;
import com.jwt.cic.questions.dao.VolumeChangedDayOverDayDAO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayDTO;
import com.jwt.cic.questions.dto.VolumeChangedDayOverDayEtlResultDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishal.domale on 25-04-2017.
 */

@RunWith(SpringRunner.class)
@Ignore
public class VolumeChangedDayOverDayServiceImplTest {

    @Mock
    private VolumeChangedDayOverDayDAO volumeChangedDayOverDayDAO;

    @InjectMocks
    private VolumeChangedDayOverDayServiceImpl volumeChangedDayOverDayService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        List<VolumeChangedDayOverDayEtlResultDTO> mockResult = buildMockData();
        Mockito.when(volumeChangedDayOverDayDAO.get()).thenReturn(mockResult);

    }

    @Test
    public void testGet() {
        VolumeChangedDayOverDayDTO actualResult= (VolumeChangedDayOverDayDTO)volumeChangedDayOverDayService.get().getValue();
        assertEquals(2,actualResult.getCandleStickChart().getLine().size());
        CandleStickValueDTO firstCandleStickValueDTO=actualResult.getCandleStickChart().getLine().get(0);
        assertEquals(BigDecimal.valueOf(2.059532243008934e-05),firstCandleStickValueDTO.getHigh());
        assertEquals(BigDecimal.valueOf(2.059532243008934e-05),firstCandleStickValueDTO.getLow());
        assertEquals("2016-12-12T00:00:00Z",firstCandleStickValueDTO.getDateAsStringValue());
        assertEquals(BigDecimal.valueOf(2.059532243008934e-05),firstCandleStickValueDTO.getValue());

    }


    private static List<VolumeChangedDayOverDayEtlResultDTO> buildMockData() {
        List<VolumeChangedDayOverDayEtlResultDTO> mockData = new ArrayList<VolumeChangedDayOverDayEtlResultDTO>();
        mockData.add(buildMockDataFirstBlock());
        mockData.add(buildMockDataFirstBlock());
        return mockData;
    }

    private static VolumeChangedDayOverDayEtlResultDTO buildMockDataFirstBlock() { VolumeChangedDayOverDayEtlResultDTO volumeChangedDayOverDayEtlResultDTO = new VolumeChangedDayOverDayEtlResultDTO();
        volumeChangedDayOverDayEtlResultDTO.setAbsolutehigh(BigDecimal.valueOf(2.059532243008934e-05));
        volumeChangedDayOverDayEtlResultDTO.setAbsolutelow(BigDecimal.valueOf(2.059532243008934e-05));
        volumeChangedDayOverDayEtlResultDTO.setDate("2016-12-12T00:00:00Z");
        volumeChangedDayOverDayEtlResultDTO.setRelativehigh(BigDecimal.valueOf(0));
        volumeChangedDayOverDayEtlResultDTO.setRelativelow(BigDecimal.valueOf(0));
        volumeChangedDayOverDayEtlResultDTO.setTrend("up");
        volumeChangedDayOverDayEtlResultDTO.setValue(BigDecimal.valueOf(2.059532243008934e-05));
        return volumeChangedDayOverDayEtlResultDTO;
    }

    private static VolumeChangedDayOverDayEtlResultDTO buildMockDataSeconDBlock() {
        VolumeChangedDayOverDayEtlResultDTO volumeChangedDayOverDayEtlResultDTO = new VolumeChangedDayOverDayEtlResultDTO();
        volumeChangedDayOverDayEtlResultDTO.setAbsolutehigh(BigDecimal.valueOf(2.059532243008934e-05));
        volumeChangedDayOverDayEtlResultDTO.setAbsolutelow(BigDecimal.valueOf(2.059532243008934e-05));
        volumeChangedDayOverDayEtlResultDTO.setDate("2016-12-12T00:00:00Z");
        volumeChangedDayOverDayEtlResultDTO.setRelativehigh(BigDecimal.valueOf(0));
        volumeChangedDayOverDayEtlResultDTO.setRelativelow(BigDecimal.valueOf(0));
        volumeChangedDayOverDayEtlResultDTO.setTrend("up");
        volumeChangedDayOverDayEtlResultDTO.setValue(BigDecimal.valueOf(2.059532243008934e-05));
        return volumeChangedDayOverDayEtlResultDTO;
    }


}
