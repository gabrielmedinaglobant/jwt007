package com.jwt.cic.questions.service;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jwt.cic.clients.dao.ProjectDAO;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jwt.cic.questions.dao.PeopleTalkingAboutOverallDAO;
import com.jwt.cic.questions.dto.PeopleTalkingAboutOverallWordDTO;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;

@RunWith(MockitoJUnitRunner.class)
@Ignore public class PeopleTalkingAboutOverallServiceImplTest {
	/*
	private static PeopleTalkingAboutOverallServiceImpl peopleTalkingAboutOverallServiceImpl;
	private static PeopleTalkingAboutOverallDAO peopleTalkingAboutOverallDAO;
	private static ProjectDAO projectDAO;
	
	@BeforeClass
	public static void setUp(){
		peopleTalkingAboutOverallDAO = Mockito.mock(PeopleTalkingAboutOverallDAO.class);
		Mockito.when( peopleTalkingAboutOverallDAO.getAll() ).thenReturn( buildMockData() );
		projectDAO=Mockito.mock(ProjectDAO.class );
		peopleTalkingAboutOverallServiceImpl = new PeopleTalkingAboutOverallServiceImpl(peopleTalkingAboutOverallDAO,projectDAO, new QuestionResultContainerBuilder());
	}
	
	private static Map<String,String> buildMockData() {
		Map<String,String> mockMap = new HashMap<>();
		
		mockMap.put("0", "Lorem");
		mockMap.put("1", "Ipsum");
		mockMap.put("2", "Donor");
		
		return mockMap;
	}

	@Test
	public void testGetAll(){
		List<PeopleTalkingAboutOverallWordDTO> resultDTO = (List<PeopleTalkingAboutOverallWordDTO>) peopleTalkingAboutOverallServiceImpl.getAll().getValue();
		
		assertEquals(resultDTO.get(0).getValue(), "Lorem");
		assertEquals(resultDTO.get(1).getValue(), "Ipsum");
		assertEquals(resultDTO.get(2).getValue(), "Donor");
	}*/
}
