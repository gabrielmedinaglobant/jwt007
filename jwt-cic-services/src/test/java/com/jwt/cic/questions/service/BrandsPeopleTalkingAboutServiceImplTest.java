package com.jwt.cic.questions.service;

import com.jwt.cic.charts.dto.SplineChartDTO;
import com.jwt.cic.questions.dao.BrandsPeopleTalkingAboutDAO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutDTO;
import com.jwt.cic.questions.dto.BrandsPeopleTalkingAboutEtlResultDTO;
import com.jwt.cic.questions.util.BlockType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by vishal.domale on 25-04-2017.
 */
@Deprecated
@RunWith(SpringRunner.class)
@Ignore
public class BrandsPeopleTalkingAboutServiceImplTest {
/*
    @Mock
    private BrandsPeopleTalkingAboutDAO brandsPeopleTalkingAboutDAO;


    @InjectMocks
    private BrandsPeopleTalkingAboutServiceImpl brandsPeopleTalkingAboutService;


    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(brandsPeopleTalkingAboutDAO.get()).thenReturn(buildMockData());
    }


    @Test
    public void testGetDocuments() {
        BrandsPeopleTalkingAboutDTO actualResult = (BrandsPeopleTalkingAboutDTO)brandsPeopleTalkingAboutService.get(BlockType.DOCUMENTS).getValue();
        assertNotNull(actualResult);

        //NegativityPieChart Test
//        assertEquals(BlockType.DOCUMENTS.getValue(), actualResult.getBlockType());
//        assertEquals("positivity", actualResult.getNegativityPieChart().getValues().get(0).getKey());
//        assertEquals(BigDecimal.valueOf(44.81030780243378), actualResult.getNegativityPieChart().getValues().get(0).getValue());
//        assertEquals("negativity", actualResult.getNegativityPieChart().getValues().get(1).getKey());
//        assertEquals(BigDecimal.valueOf(46.12264376043903), actualResult.getNegativityPieChart().getValues().get(1).getValue());
//        assertEquals("mix", actualResult.getNegativityPieChart().getValues().get(2).getKey());
//        assertEquals(BigDecimal.valueOf(9.067048437127177), actualResult.getNegativityPieChart().getValues().get(2).getValue());
//        assertEquals(BigDecimal.valueOf(46.12264376043903), actualResult.getNegativityPieChart().getIndicator());


        //PositivityPieChart Test
//        assertEquals("positivity", actualResult.getPositivityPieChart().getValues().get(0).getKey());
//        assertEquals(BigDecimal.valueOf(44.81030780243378), actualResult.getPositivityPieChart().getValues().get(0).getValue());
//        assertEquals("negativity", actualResult.getPositivityPieChart().getValues().get(1).getKey());
//        assertEquals(BigDecimal.valueOf(46.12264376043903), actualResult.getPositivityPieChart().getValues().get(1).getValue());
//        assertEquals("mix", actualResult.getPositivityPieChart().getValues().get(2).getKey());
//        assertEquals(BigDecimal.valueOf(9.067048437127177), actualResult.getPositivityPieChart().getValues().get(2).getValue());
//        assertEquals(BigDecimal.valueOf(44.81030780243378), actualResult.getPositivityPieChart().getIndicator());

        // Test SentimentOvertimeSplineChart

        SplineChartDTO splineChartDTO = actualResult.getSentimentOvertimeSplineChart();
        assertEquals(BigDecimal.valueOf(6.25), splineChartDTO.getLines().get(0).getValues().get(0).getValue()[1]);

        //Test SingleStackedBarChart
//        assertEquals(BigDecimal.valueOf(45), actualResult.getSingleStackedBarChart().getValues().get(0).getValue());
//        assertEquals(BigDecimal.valueOf(9), actualResult.getSingleStackedBarChart().getValues().get(1).getValue());
//        assertEquals(BigDecimal.valueOf(46), actualResult.getSingleStackedBarChart().getValues().get(2).getValue());


    }


    @Test
    public void testGetSentences() {
        BrandsPeopleTalkingAboutDTO actualResult = (BrandsPeopleTalkingAboutDTO)brandsPeopleTalkingAboutService.get(BlockType.SENTENCES).getValue();
        assertNotNull(actualResult);
        assertEquals(BlockType.SENTENCES.getValue(), actualResult.getBlockType());
        //NegativityPieChart Test
//        assertEquals("positivity", actualResult.getNegativityPieChart().getValues().get(0).getKey());
//        assertEquals(BigDecimal.valueOf(47.90352843070829), actualResult.getNegativityPieChart().getValues().get(0).getValue());
//        assertEquals("negativity", actualResult.getNegativityPieChart().getValues().get(1).getKey());
//        assertEquals(BigDecimal.valueOf(44.48584979413145), actualResult.getNegativityPieChart().getValues().get(1).getValue());
//        assertEquals("mix", actualResult.getNegativityPieChart().getValues().get(2).getKey());
//        assertEquals(BigDecimal.valueOf(7.610621775160265), actualResult.getNegativityPieChart().getValues().get(2).getValue());
//        assertEquals(BigDecimal.valueOf(44.48584979413145), actualResult.getNegativityPieChart().getIndicator());
//
//        //PositivityPieChart Test
//        assertEquals("positivity", actualResult.getPositivityPieChart().getValues().get(0).getKey());
//        assertEquals(BigDecimal.valueOf(47.90352843070829), actualResult.getPositivityPieChart().getValues().get(0).getValue());
//        assertEquals("negativity", actualResult.getPositivityPieChart().getValues().get(1).getKey());
//        assertEquals(BigDecimal.valueOf(44.48584979413145), actualResult.getPositivityPieChart().getValues().get(1).getValue());
//        assertEquals("mix", actualResult.getPositivityPieChart().getValues().get(2).getKey());
//        assertEquals(BigDecimal.valueOf(7.610621775160265), actualResult.getPositivityPieChart().getValues().get(2).getValue());
//        assertEquals(BigDecimal.valueOf(47.90352843070829), actualResult.getPositivityPieChart().getIndicator());

        // Test SentimentOvertimeSplineChart

        SplineChartDTO splineChartDTO = actualResult.getSentimentOvertimeSplineChart();
        assertEquals(BigDecimal.valueOf(6.535947712418299), splineChartDTO.getLines().get(0).getValues().get(0).getValue()[1]);

        //Test SingleStackedBarChart
//        assertEquals(BigDecimal.valueOf(48), actualResult.getSingleStackedBarChart().getValues().get(0).getValue());
//        assertEquals(BigDecimal.valueOf(8), actualResult.getSingleStackedBarChart().getValues().get(1).getValue());
//        assertEquals(BigDecimal.valueOf(44), actualResult.getSingleStackedBarChart().getValues().get(2).getValue());

    }


    @Test
    public void testGetSubjectSentences() {
        BrandsPeopleTalkingAboutDTO actualResult = (BrandsPeopleTalkingAboutDTO)brandsPeopleTalkingAboutService.get(BlockType.SUBJECT_SENTENCES).getValue();
        assertNotNull(actualResult);
        assertEquals("subject-sentences", actualResult.getBlockType());
        assertEquals("positivity", actualResult.getNegativityPieChart().getValues().get(0).getKey());
        assertEquals(BigDecimal.valueOf(47.33295445319477), actualResult.getNegativityPieChart().getValues().get(0).getValue());
        assertEquals("negativity", actualResult.getNegativityPieChart().getValues().get(1).getKey());
        assertEquals(BigDecimal.valueOf(44.25049389732349), actualResult.getNegativityPieChart().getValues().get(1).getValue());
        assertEquals("mix", actualResult.getNegativityPieChart().getValues().get(2).getKey());
        assertEquals(BigDecimal.valueOf(8.416551649481747), actualResult.getNegativityPieChart().getValues().get(2).getValue());
        // assertEquals(BigDecimal.valueOf(44.25049389732349), actualResult.getNegativityPieChart().getIndicator());

        //PositivityPieChart Test
        assertEquals("positivity", actualResult.getPositivityPieChart().getValues().get(0).getKey());
        assertEquals(BigDecimal.valueOf(47.33295445319477), actualResult.getPositivityPieChart().getValues().get(0).getValue());
        assertEquals("negativity", actualResult.getPositivityPieChart().getValues().get(1).getKey());
        assertEquals(BigDecimal.valueOf(44.25049389732349), actualResult.getPositivityPieChart().getValues().get(1).getValue());
        assertEquals("mix", actualResult.getPositivityPieChart().getValues().get(2).getKey());
        assertEquals(BigDecimal.valueOf(8.416551649481747), actualResult.getPositivityPieChart().getValues().get(2).getValue());
        // assertEquals(BigDecimal.valueOf(47.33295445319477), actualResult.getPositivityPieChart().getIndicator());

        // Test SentimentOvertimeSplineChart

        SplineChartDTO splineChartDTO = actualResult.getSentimentOvertimeSplineChart();
        assertEquals(BigDecimal.valueOf(31.57894736842105), splineChartDTO.getLines().get(0).getValues().get(0).getValue()[1]);


        //Test SingleStackedBarChart
        assertEquals(BigDecimal.valueOf(47), actualResult.getSingleStackedBarChart().getValues().get(0).getValue());
        assertEquals(BigDecimal.valueOf(8), actualResult.getSingleStackedBarChart().getValues().get(1).getValue());
        assertEquals(BigDecimal.valueOf(44), actualResult.getSingleStackedBarChart().getValues().get(2).getValue());

    }


    private static List<BrandsPeopleTalkingAboutEtlResultDTO> buildMockData() {
        List<BrandsPeopleTalkingAboutEtlResultDTO> mockData = new ArrayList<BrandsPeopleTalkingAboutEtlResultDTO>();
        mockData.add(buildMockDataFirstBlock());
        mockData.add(buildMockDataSeconDBlock());
        return mockData;
    }

    private static BrandsPeopleTalkingAboutEtlResultDTO buildMockDataFirstBlock() {
        BrandsPeopleTalkingAboutEtlResultDTO brandsPeopleTalkingAboutEtlResultDTO = new BrandsPeopleTalkingAboutEtlResultDTO();
        brandsPeopleTalkingAboutEtlResultDTO.setDate("2017-03-06T00:00:00Z");
        brandsPeopleTalkingAboutEtlResultDTO.setDocumentsNegativity(BigDecimal.valueOf(37.5));
        brandsPeopleTalkingAboutEtlResultDTO.setDocumentsPositivity(BigDecimal.valueOf(43.75));
        brandsPeopleTalkingAboutEtlResultDTO.setDocumentsWeeklyDiff(BigDecimal.valueOf(6.25));
        brandsPeopleTalkingAboutEtlResultDTO.setMixedDocuments(18);
        brandsPeopleTalkingAboutEtlResultDTO.setMixedSentences(6);
        brandsPeopleTalkingAboutEtlResultDTO.setMixedSubjectSentences(12);
        brandsPeopleTalkingAboutEtlResultDTO.setSentencesNegativity(BigDecimal.valueOf(43.4640522875817));
        brandsPeopleTalkingAboutEtlResultDTO.setSentencesPositivity(BigDecimal.valueOf(50));
        brandsPeopleTalkingAboutEtlResultDTO.setSentencesWeeklyDiff(BigDecimal.valueOf(6.535947712418299));
        brandsPeopleTalkingAboutEtlResultDTO.setSubjectSentencesNegativity(BigDecimal.valueOf(28.07017543859649));
        brandsPeopleTalkingAboutEtlResultDTO.setSubjectSentencesPositivity(BigDecimal.valueOf(59.64912280701754));
        brandsPeopleTalkingAboutEtlResultDTO.setSubjectSentencesWeeklyDiff(BigDecimal.valueOf(31.57894736842105));
        return brandsPeopleTalkingAboutEtlResultDTO;
    }

    private static BrandsPeopleTalkingAboutEtlResultDTO buildMockDataSeconDBlock() {
        BrandsPeopleTalkingAboutEtlResultDTO brandsPeopleTalkingAboutEtlResultDTO = new BrandsPeopleTalkingAboutEtlResultDTO();
        brandsPeopleTalkingAboutEtlResultDTO.setMixedDocumentsProm(BigDecimal.valueOf(9.067048437127177));
        brandsPeopleTalkingAboutEtlResultDTO.setMixedSentencesProm(BigDecimal.valueOf(7.610621775160265));
        brandsPeopleTalkingAboutEtlResultDTO.setMixedSubjectSentencesProm(BigDecimal.valueOf(8.416551649481747));
        brandsPeopleTalkingAboutEtlResultDTO.setNegativeDocumentsProm(BigDecimal.valueOf(46.12264376043903));
        brandsPeopleTalkingAboutEtlResultDTO.setNegativeSentencesProm(BigDecimal.valueOf(44.48584979413145));
        brandsPeopleTalkingAboutEtlResultDTO.setNegativeSubjectSentencesProm(BigDecimal.valueOf(44.25049389732349));
        brandsPeopleTalkingAboutEtlResultDTO.setPositiveDocumentsProm(BigDecimal.valueOf(44.81030780243378));
        brandsPeopleTalkingAboutEtlResultDTO.setPositiveSentencesProm(BigDecimal.valueOf(47.90352843070829));
        brandsPeopleTalkingAboutEtlResultDTO.setPositiveSubjectSentencesProm(BigDecimal.valueOf(47.33295445319477));
        return brandsPeopleTalkingAboutEtlResultDTO;
    }
    */
}
