package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dao.EmotionsAboutCategoryDAO;
import com.jwt.cic.questions.dto.EmotionsIndicatorDTO;
import com.jwt.cic.utils.DoubleDigitsPercentageRoundingStrategy;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by vishal.domale on 25-04-2017.
 */

@RunWith(SpringRunner.class)
@Ignore public class EmotionsAboutCategoryServiceImplTest {

/*
    @Mock
    private EmotionsAboutCategoryDAO emotionsAboutCategoryDAO;

    @Spy
    private DoubleDigitsPercentageRoundingStrategy roundingStrategy;

    @InjectMocks
    private EmotionsAboutCategoryServiceImpl emotionsAboutCategoryService;


    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Map<String, BigDecimal> mockResult=prepareMockData();
        Mockito.when(emotionsAboutCategoryDAO.get()).thenReturn(mockResult);

    }

    @Test
    public void testGet() {
        List<EmotionsIndicatorDTO> actualResult = (List<EmotionsIndicatorDTO>) emotionsAboutCategoryService.get().getValue();
        assertNotNull(actualResult);
        assertEquals(8,actualResult.size());
    }


   private  Map<String, BigDecimal> prepareMockData(){
        Map<String, BigDecimal> mockResult = new HashMap<String, BigDecimal>();
        mockResult.put("anger", BigDecimal.valueOf(0.08941684665226782));
        mockResult.put("anticipation", BigDecimal.valueOf(0.272829373650108));
        mockResult.put("disgust", BigDecimal.valueOf(0.051317494600431966));
        mockResult.put("fear", BigDecimal.valueOf(0.14393088552915767));
        mockResult.put("joy", BigDecimal.valueOf(0.09105831533477322));
        mockResult.put("sadness", BigDecimal.valueOf(0.09062634989200864));
        mockResult.put("surprise", BigDecimal.valueOf(0.07395248380129589));
        mockResult.put("trust", BigDecimal.valueOf(0.18686825053995682));
        return mockResult;
    }
*/    
}

