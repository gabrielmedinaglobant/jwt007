package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dao.PeopleLookedCompetitorsDAO;
import com.jwt.cic.questions.dto.PeopleLookedCompetitorsDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by vishal.domale on 26-04-2017.
 */
@RunWith(SpringRunner.class)
@Ignore
public class PeopleLookedCompetitorsServiceImplTest {


    @MockBean
    private PeopleLookedCompetitorsDAO peopleLookedCompetitorsDAO;


    @InjectMocks
    private PeopleLookedCompetitorsServiceImpl peopleLookedCompetitorsService;


    private  String brands[]={"adidas","nike","puma"};


    @Before
    public void setupMock() {

        MockitoAnnotations.initMocks(this);
        List<Map<String, String>> mockResult = buildMockData();
        Mockito.when(peopleLookedCompetitorsDAO.get(brands)).thenReturn(mockResult);

    }

    @Test
    public void testGet() {
        PeopleLookedCompetitorsDTO actualResult = (PeopleLookedCompetitorsDTO)peopleLookedCompetitorsService.get(brands).getValue();
        assertEquals("adidas", actualResult.getBrands().get(0).getKey());
        assertEquals("nike", actualResult.getBrands().get(1).getKey());
        assertEquals("puma", actualResult.getBrands().get(2).getKey());

    }

    private static List<Map<String, String>> buildMockData() {
        List<Map<String, String>> mockData = new ArrayList<>();
        mockData.add(buildMockDataFirstBlock());
        mockData.add(buildMockDataSecondBlock());
        mockData.add(buildMockDataThirdBlock());
        return mockData;
    }


    private static Map<String, String> buildMockDataThirdBlock() {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("puma", "101110.0");
        jsonMap.put("Day", "2017-01-31");
        jsonMap.put("adidas", "59411.307692307695");
        jsonMap.put("nike", "34657.0");
        return jsonMap;
    }

    private static Map<String, String> buildMockDataSecondBlock() {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("puma", "101110.0");
        jsonMap.put("Day", "2017-01-31");
        jsonMap.put("adidas", "59411.307692307695");
        jsonMap.put("nike", "34657.0");
        return jsonMap;
    }

    private static Map<String, String> buildMockDataFirstBlock() {
        Map<String, String> jsonMap = new LinkedHashMap<>();

        jsonMap.put("puma", "101110.0");
        jsonMap.put("Day", "2017-01-31");
        jsonMap.put("adidas", "59411.307692307695");
        jsonMap.put("nike", "34657.0");
        return jsonMap;
    }
}
