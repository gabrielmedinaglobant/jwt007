package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dao.KeyThemesTalkAboutDAO;
import com.jwt.cic.questions.dto.KeyThemeTalkAboutDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by vishal.domale on 25-04-2017.
 */

@RunWith(SpringRunner.class)
@Ignore public class KeyThemesTalkAboutServiceImplTest {

/*
    @MockBean
    private KeyThemesTalkAboutDAO keyThemesTalkAboutDAO;


    @InjectMocks
    private KeyThemesTalkAboutServiceImpl keyThemesTalkAboutService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Map<String, BigDecimal> mockResult = prepareMockData();
        Mockito.when(keyThemesTalkAboutDAO.get()).thenReturn(mockResult);

    }

    @Test
    public void testGet() {


       List<KeyThemeTalkAboutDTO> actualResult = (List<KeyThemeTalkAboutDTO>) keyThemesTalkAboutService.get().getValue();
        assertNotNull(actualResult);
        assertEquals(11,actualResult.size());
    }


    private Map<String, BigDecimal> prepareMockData() {
        Map<String, BigDecimal> mockResult = new HashMap<String, BigDecimal>();
        mockResult.put("acquisition", BigDecimal.valueOf(50.5175983436853));
        mockResult.put("acquisition_negative", BigDecimal.valueOf(48.114754098360656));
        mockResult.put("acquisition_positive", BigDecimal.valueOf(40.28688524590164));

        mockResult.put("attraction", BigDecimal.valueOf(23.043478260869566));
        mockResult.put("attraction_negative", BigDecimal.valueOf(37.01707097933513));
        mockResult.put("attraction_positive", BigDecimal.valueOf(57.053009883198555));

        mockResult.put("confidence", BigDecimal.valueOf(22.380952380952383));
        mockResult.put("confidence_negative", BigDecimal.valueOf(54.11655874190564));
        mockResult.put("confidence_positive", BigDecimal.valueOf(39.77798334875116));

        mockResult.put("cost", BigDecimal.valueOf(19.026915113871638));
        mockResult.put("cost_negative", BigDecimal.valueOf(44.83133841131664));
        mockResult.put("cost_positive", BigDecimal.valueOf(48.74863982589772));

        mockResult.put("creativity", BigDecimal.valueOf(27.97101449275362));
        mockResult.put("creativity_negative", BigDecimal.valueOf(27.831236121391562));
        mockResult.put("creativity_positive", BigDecimal.valueOf(65.21095484826056));

        mockResult.put("expectation", BigDecimal.valueOf(74.45134575569358));
        mockResult.put("expectation_negative", BigDecimal.valueOf(42.241379310344826));
        mockResult.put("expectation_positive", BigDecimal.valueOf(45.49499443826474));

        mockResult.put("health", BigDecimal.valueOf(26.438923395445137));
        mockResult.put("health_negative", BigDecimal.valueOf(38.84103367267032));
        mockResult.put("health_positive", BigDecimal.valueOf(54.424432263116685));

        mockResult.put("humor", BigDecimal.valueOf(4.22360248447205));
        mockResult.put("humor_negative", BigDecimal.valueOf(31.372549019607842));
        mockResult.put("humor_positive", BigDecimal.valueOf(60.29411764705882));

        mockResult.put("purchase_intent", BigDecimal.valueOf(27.84679089026915));
        mockResult.put("purchase_intent_negative", BigDecimal.valueOf(50.55762081784386));
        mockResult.put("purchase_intent_positive", BigDecimal.valueOf(38.81040892193309));

        mockResult.put("quality", BigDecimal.valueOf(22.17391304347826));
        mockResult.put("quality_negative", BigDecimal.valueOf(43.13725490196079));
        mockResult.put("quality_positive", BigDecimal.valueOf(48.64612511671335));

        mockResult.put("services", BigDecimal.valueOf(33.18840579710145));
        mockResult.put("services_negative", BigDecimal.valueOf(38.365564566437925));
        mockResult.put("services_positive", BigDecimal.valueOf(54.64753587024329));

        mockResult.put("taste", BigDecimal.valueOf(15.590062111801242));
        mockResult.put("taste_negative", BigDecimal.valueOf(46.34794156706507));
        mockResult.put("taste_positive", BigDecimal.valueOf(47.410358565737056));
        return mockResult;
    }
*/
}
