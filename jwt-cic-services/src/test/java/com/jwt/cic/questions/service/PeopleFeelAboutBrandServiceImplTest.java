package com.jwt.cic.questions.service;

import com.jwt.cic.questions.dao.PeopleFeelAboutBrandDAO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandEtlResultDTO;
import com.jwt.cic.questions.dto.PeopleFeelAboutBrandOptionDTO;
import com.jwt.cic.questions.dto.QuestionResultListContainerDTO;
import com.jwt.cic.questions.util.FeelingAboutBrandOptionType;
import com.jwt.cic.questions.util.QuestionResultContainerBuilder;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by vishal.domale on 28-04-2017.
 */

@RunWith(SpringRunner.class)
@Ignore
public class PeopleFeelAboutBrandServiceImplTest {

    @Mock
    private PeopleFeelAboutBrandDAO peopleFeelAboutBrandDAO;

    @InjectMocks
    private PeopleFeelAboutBrandServiceImpl peopleFeelAboutBrandService;

    @Spy
    private QuestionResultContainerBuilder questionResultListContainerBuilder;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(peopleFeelAboutBrandDAO.get()).thenReturn(buildMockData());
    }


    @Test
    public void testGet() {
        QuestionResultListContainerDTO  result = peopleFeelAboutBrandService.get();
        assertNotNull(result);

        List<PeopleFeelAboutBrandOptionDTO> actualResult=(List<PeopleFeelAboutBrandOptionDTO>)  result.getValue();
        //assertEquals(FeelingAboutBrandOptionType.DO,actualResult.get(0).getOption());
        assertEquals(BigDecimal.valueOf(4),actualResult.get(0).getWords().get(0).getSize());
        assertEquals(BigDecimal.valueOf(5),actualResult.get(0).getWords().get(0).getFeelingRank());
        //assertEquals("director",actualResult.get(0).getWords().get(0).getValue());

        //assertEquals(FeelingAboutBrandOptionType.FEEL,actualResult.get(1).getOption());
        assertEquals(BigDecimal.valueOf(4),actualResult.get(1).getWords().get(0).getSize());
        assertEquals(BigDecimal.valueOf(5),actualResult.get(1).getWords().get(0).getFeelingRank());
        //assertEquals("name",actualResult.get(1).getWords().get(0).getValue());


        //assertEquals(FeelingAboutBrandOptionType.LIKE,actualResult.get(2).getOption());
        assertEquals(BigDecimal.valueOf(4),actualResult.get(2).getWords().get(0).getSize());
        assertEquals(BigDecimal.valueOf(5),actualResult.get(2).getWords().get(0).getFeelingRank());
        //assertEquals("Land",actualResult.get(2).getWords().get(0).getValue());

    }

    private static List<PeopleFeelAboutBrandEtlResultDTO> buildMockData() {
        List<PeopleFeelAboutBrandEtlResultDTO> mockData = new ArrayList<PeopleFeelAboutBrandEtlResultDTO>();
        mockData.add(buildMockDataFirstBlock());
        mockData.add(buildMockDataSecondBlock());
        mockData.add(buildMockDataThirdBlock());
        return mockData;
    }

    private static PeopleFeelAboutBrandEtlResultDTO buildMockDataFirstBlock() {
        PeopleFeelAboutBrandEtlResultDTO peopleFeelAboutBrandEtlResultDTO = new PeopleFeelAboutBrandEtlResultDTO();
        peopleFeelAboutBrandEtlResultDTO.setColor(BigDecimal.valueOf(5));
        peopleFeelAboutBrandEtlResultDTO.setNegativeDocuments(BigDecimal.valueOf(514.0));
        peopleFeelAboutBrandEtlResultDTO.setOption(FeelingAboutBrandOptionType.FEEL);
        peopleFeelAboutBrandEtlResultDTO.setPositiveDocuments(BigDecimal.valueOf(7297.0));
        peopleFeelAboutBrandEtlResultDTO.setScore(BigDecimal.valueOf(16159.9931640625));
        peopleFeelAboutBrandEtlResultDTO.setTopic("name");
        peopleFeelAboutBrandEtlResultDTO.setSize(BigDecimal.valueOf(4));

        return peopleFeelAboutBrandEtlResultDTO;
    }

    private static PeopleFeelAboutBrandEtlResultDTO buildMockDataSecondBlock() {
        PeopleFeelAboutBrandEtlResultDTO peopleFeelAboutBrandEtlResultDTO = new PeopleFeelAboutBrandEtlResultDTO();
        peopleFeelAboutBrandEtlResultDTO.setColor(BigDecimal.valueOf(5));
        peopleFeelAboutBrandEtlResultDTO.setNegativeDocuments(BigDecimal.valueOf(345.0));
        peopleFeelAboutBrandEtlResultDTO.setOption(FeelingAboutBrandOptionType.LIKE);
        peopleFeelAboutBrandEtlResultDTO.setPositiveDocuments(BigDecimal.valueOf(4819.0));
        peopleFeelAboutBrandEtlResultDTO.setScore(BigDecimal.valueOf(12809.96875));
        peopleFeelAboutBrandEtlResultDTO.setTopic("Land");
        peopleFeelAboutBrandEtlResultDTO.setSize(BigDecimal.valueOf(4));

        return peopleFeelAboutBrandEtlResultDTO;
    }


    private static PeopleFeelAboutBrandEtlResultDTO buildMockDataThirdBlock() {
        PeopleFeelAboutBrandEtlResultDTO peopleFeelAboutBrandEtlResultDTO = new PeopleFeelAboutBrandEtlResultDTO();
        peopleFeelAboutBrandEtlResultDTO.setColor(BigDecimal.valueOf(5));
        peopleFeelAboutBrandEtlResultDTO.setNegativeDocuments(BigDecimal.valueOf(884.0));
        peopleFeelAboutBrandEtlResultDTO.setOption(FeelingAboutBrandOptionType.DO);
        peopleFeelAboutBrandEtlResultDTO.setPositiveDocuments(BigDecimal.valueOf(6262.0));
        peopleFeelAboutBrandEtlResultDTO.setScore(BigDecimal.valueOf(16171.115234375));
        peopleFeelAboutBrandEtlResultDTO.setTopic("director");
        peopleFeelAboutBrandEtlResultDTO.setSize(BigDecimal.valueOf(4));

        return peopleFeelAboutBrandEtlResultDTO;
    }

}
