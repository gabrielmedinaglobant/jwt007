package com.jwt.cic.questions.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jwt.cic.questions.dao.PeopleMentionedCompetitorsDAO;
import com.jwt.cic.questions.dto.DisplayableDTO;

@RunWith(MockitoJUnitRunner.class)
@Ignore public class PeopleMentionedCompetitorsServiceImplTest {
/*	
	private static PeopleMentionedCompetitorsServiceImpl peopleMentionedCompetitorsServiceImpl;
	private static PeopleMentionedCompetitorsDAO peopleMentionedCompetitorsDAO;
	
	@BeforeClass
	public static void setUp(){
		peopleMentionedCompetitorsDAO = Mockito.mock(PeopleMentionedCompetitorsDAO.class);
		peopleMentionedCompetitorsServiceImpl = new PeopleMentionedCompetitorsServiceImpl( peopleMentionedCompetitorsDAO, null, null );
		
		Mockito.when(peopleMentionedCompetitorsDAO.get()).thenReturn( buildMockData() );
	}
	
	private static List<Map<String, String>> buildMockData() {
		List<Map<String, String>> mockData = new ArrayList<>();
		mockData.add( buildMockDataFirstBlock() );
		mockData.add( buildMockDataSecondBlock() );
		mockData.add( buildMockDataThirdBlock() );
		return mockData;
	}

	private static Map<String, String> buildMockDataThirdBlock() {
		Map<String, String> jsonMap = new HashMap<>();
		jsonMap.put("date", "2017-01-31");
		jsonMap.put("nike_max", "101110.0");
		jsonMap.put("nike_mean", "59411.307692307695");
		jsonMap.put("nike_min", "34657.0");
		jsonMap.put("nike_sum", "772347.0");
		return jsonMap;
	}

	private static Map<String, String> buildMockDataSecondBlock() {
		Map<String, String> jsonMap = new HashMap<>();
		jsonMap.put("date", "2017-01-31");
		jsonMap.put("adidas_max", "101110.0");
		jsonMap.put("adidas_mean", "59411.307692307695");
		jsonMap.put("adidas_min", "34657.0");
		jsonMap.put("adidas_sum", "772347.0");
		return jsonMap;
	}

	private static Map<String, String> buildMockDataFirstBlock() {
		Map<String, String> jsonMap = new HashMap<>();
		jsonMap.put("date", "2017-01-31");
		jsonMap.put("puma_max", "101110.0");
		jsonMap.put("puma_mean", "59411.307692307695");
		jsonMap.put("puma_min", "34657.0");
		jsonMap.put("puma_sum", "772347.0");
		return jsonMap;
	}

	@Test
	public void testExtractBrandPrefix(){
		assertEquals( peopleMentionedCompetitorsServiceImpl.extractBrandName("brandy_sadasd"),"brandy" );
	}
	
	@Test
	public void testBuildBrandsList(){
		Set<String> brands = peopleMentionedCompetitorsServiceImpl.buildBrandsList( buildMockData() );
		List<DisplayableDTO> brandsDTOList = peopleMentionedCompetitorsServiceImpl.buildBrandsDTOList( brands );
		
		assertEquals( "puma", brandsDTOList.get(0).getKey() );
		assertEquals( "adidas", brandsDTOList.get(1).getKey() );
		assertEquals( "nike", brandsDTOList.get(2).getKey() );
	}
*/
}
