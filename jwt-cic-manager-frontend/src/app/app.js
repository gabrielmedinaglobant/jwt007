export const AppComponent = {
  template: require('./app.html'),
  controller: class AppController {
    constructor($log, $state, $window, $location, PropertiesService, AuthenticationService, MESSAGES) {
      'ngInject';
      this.$log = $log;
      this.$state = $state;
      this.$window = $window;
      this.$location = $location;
      this.PropertiesService = PropertiesService;
      this.AuthenticationService = AuthenticationService;
      this.MESSAGES = MESSAGES;

    }

    getCurrentUser() {
      let currentUser = '';

      try {
        currentUser = this.AuthenticationService.getCurrentUser().email;
      } catch (e) {
        this.$log.error('AppComponent -> $onInit -> ', e);
        currentUser = this.MESSAGES.ERROR.NOT_AUTHORIZED;
      }

      return currentUser;
    }

    isClientActive() {
      return this.$state.is('app.client-index'); // or client-create, or client-edit
    }
    isBrandActive() {
      return this.$state.is('app.brand-index'); // or brand-create, or brand-edit
    }
    isLogOffActive() {
      return this.$state.is('app.log-off');
    }

    logout() {
      this.AuthenticationService.cleanAuthentication();
      this.$window.location.href = this.PropertiesService.get('AUTHENTICATION_APP_LOG_OUT_URL');
    }

    $onInit() {

      

    }

  }
};
