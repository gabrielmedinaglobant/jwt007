import angular from 'angular';
import 'angular-mocks';
import {AppComponent} from './app';

describe('AppComponent component', () => {
  beforeEach(() => {
    angular
      .module('fountainHello', ['app/app.html'])
      .component('fountainHello', AppComponent);
    angular.mock.module('fountainHello');
  });
  it('should render hello world', angular.mock.inject(($rootScope, $compile) => {
    const element = $compile('<fountain-hello>Loading...</fountain-hello>')($rootScope);
    $rootScope.$digest();
    const h1 = element.find('h1');
    expect(h1.html()).toEqual('Hello World!');
  }));
});
