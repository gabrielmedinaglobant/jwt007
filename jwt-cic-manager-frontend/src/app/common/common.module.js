import angular from 'angular';

import {EnterKeyPressModule} from './enter-key-press/enter-key-press.module';

import {KeywordBrowserModule} from './keyword-browser/keyword-browser.module';

import {ProcessingCounterFactoryService} from './processing-counter/processing-counter.service';

export const CommonModule = angular
  .module('app.common', [
    EnterKeyPressModule,
    KeywordBrowserModule
  ])
  .service('ProcessingCounterFactoryService', ProcessingCounterFactoryService)
  .name;
