import _ from 'lodash';

export const KeywordBrowserComponent = {
  template: require('./keyword-browser.html'),
  bindings: {
    onBrowseAction: '&',
    onRefreshAction: '&'
  },
  controller: class KeywordBrowserController {
    constructor($log, MESSAGES) {
      'ngInject';
      this.$log = $log;
      this.keyword = '';
      this.errorMessages = {};
      this.errorMessages.required = MESSAGES.ERROR.NON_BLANK_SPACE;
      this.errorMessages.pattern = MESSAGES.ERROR.ONLY_ALPHANUMERIC_ALLOWED;
      this.$log.debug("init errorMessages", this.errorMessages);
    }
    $onInit() {
      this.message = 'Here it goes keyword-browser';
      this.$log.debug('$onInit run');
    }
    browse() {
      if(this.form.$valid) { 
        this.$log.debug('there has been an action ' + this.keyword);
        this.onBrowseAction({
          $event: {
            text: this.keyword
          }
        });
      }
    }
    refresh() {
      this.keyword = '';
      this.onRefreshAction({ $event: {} });
    }
    getErrorMessage(errType) {
      return this.errorMessages[errType];
    }
    getFormErrors() {
      return _.filter( Object.keys(this.form.$error), errKey => {
        return errKey !== 'required';
      } );
    }
    isBrowseDisabled() {
      return Object.keys(this.form.$error).length > 0;
    }
  }

};
