import angular from 'angular';

import {KeywordBrowserComponent} from './keyword-browser.component';

export const KeywordBrowserModule = angular
  .module('common.keyword-browser', [
  ])
  .component('jwtKeywordBrowser', KeywordBrowserComponent)
  .name;
