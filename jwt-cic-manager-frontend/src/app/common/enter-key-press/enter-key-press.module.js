import angular from 'angular';

import {EnterKeyPressDirective} from './enter-key-press.directive';

export const EnterKeyPressModule = angular
  .module('common.enter-key-press', [
  ])
  .directive('jwtEnterKeyPress', EnterKeyPressDirective)
  .name;
