import {ProcessingCounter} from './processing-counter';

export class ProcessingCounterFactoryService {

  createInstance() {
    return new ProcessingCounter();
  }
  
}
