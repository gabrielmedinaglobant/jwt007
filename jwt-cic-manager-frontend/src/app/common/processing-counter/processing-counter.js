export class ProcessingCounter {

  constructor(){
    this.processingCount = 0;
    this.errors = [];
  }

  addError(error) {
    this.errors.push(error);
  }

  hasError() {
    return (this.errors.length > 0);
  }

  incrementProcessing() {
    this.processingCount++;
  }

  isProcessing() {
    if(this.processingCount > 0){
      return true;
    }
    return false;
  }

  decrementProcessing() {
    this.processingCount--;
  }

  clear() {
    this.processingCount = 0;
    this.errors = [];
  }

  clearErrors() {
    this.errors = [];
  }

}
