import angular from 'angular';

import {ProjectIndexComponent} from './project-index.component';

export const ProjectIndexModule = angular
  .module('project.index', [
  ])
  .component('jwtProjectIndex', ProjectIndexComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.project-index', {
          url: '/project/index',
          component: 'jwtProjectIndex'
        });
    }
  )
  .name;
