import angular from 'angular';

import {ProjectIndexModule} from './project-index/project-index.module';

export const ProjectModule = angular
  .module('components.project', [
    ProjectIndexModule
  ])
  .name;