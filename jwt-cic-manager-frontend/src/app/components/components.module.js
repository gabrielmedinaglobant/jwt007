import angular from 'angular';

import {AuthenticationModule} from './authentication/authentication.module';
import {BrandModule} from './brand/brand.module';
import {ClientModule} from './client/client.module';
import {ProjectModule} from './project/project.module';

import {MessagesModule} from './messages/messages.module';
import {PropertiesModule} from './properties/properties.module';

export const ComponentsModule = angular
  .module('app.components', [
    AuthenticationModule,
    BrandModule,
    ClientModule,
    ProjectModule,
    MessagesModule,
    PropertiesModule
  ])
  .name;
