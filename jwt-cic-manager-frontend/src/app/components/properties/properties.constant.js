export const PROPERTIES = {
  LOCAL: {
    SERVICES_BACKEND_URL: 'http://localhost:8080/api/v1/',
    AUTHENTICATION_APP_LOG_IN_URL: 'http://jwt-cic-auth-local.ngrok.io/jwt-cic-auth/auth/manager-app',
    AUTHENTICATION_APP_LOG_OUT_URL: 'http://jwt-cic-auth-local.ngrok.io/jwt-cic-auth/auth/manager-app/logout'
  },
  DEVELOPMENT: {
    SERVICES_BACKEND_URL: 'http://jwt-dcc-a-elasticl-umtrlt5fkngp-1533616561.us-west-2.elb.amazonaws.com/services/api/v1/',
    AUTHENTICATION_APP_LOG_IN_URL: 'http://jwt-dcc-a-elasticl-umtrlt5fkngp-1533616561.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app',
    AUTHENTICATION_APP_LOG_OUT_URL: 'http://jwt-dcc-a-elasticl-umtrlt5fkngp-1533616561.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app/logout'
  },
  QA: {
    SERVICES_BACKEND_URL: 'http://jwt-dcc-a-elasticl-wtmbubstalj1-1109151366.us-west-2.elb.amazonaws.com/services/api/v1/',
    AUTHENTICATION_APP_LOG_IN_URL: 'http://jwt-dcc-a-elasticl-wtmbubstalj1-1109151366.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app',
    AUTHENTICATION_APP_LOG_OUT_URL: 'http://jwt-dcc-a-elasticl-wtmbubstalj1-1109151366.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app/logout'
  },
  UAT: {
    SERVICES_BACKEND_URL: 'http://jwt-dcc-a-elasticl-12qwwy2j6urmg-1406172478.us-west-2.elb.amazonaws.com/services/api/v1/',
    AUTHENTICATION_APP_LOG_IN_URL: 'http://jwt-dcc-a-elasticl-12qwwy2j6urmg-1406172478.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app',
    AUTHENTICATION_APP_LOG_OUT_URL: 'http://jwt-dcc-a-elasticl-12qwwy2j6urmg-1406172478.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app/logout'
  },
  PRODUCTION: {
    SERVICES_BACKEND_URL: 'http://jwt-dcc-a-ElasticL-UQK02M2APRMY-1511142900.us-west-2.elb.amazonaws.com/services/api/v1/',
    AUTHENTICATION_APP_LOG_IN_URL: 'http://jwt-dcc-a-ElasticL-UQK02M2APRMY-1511142900.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app',
    AUTHENTICATION_APP_LOG_OUT_URL: 'http://jwt-dcc-a-ElasticL-UQK02M2APRMY-1511142900.us-west-2.elb.amazonaws.com/jwt-cic-auth/auth/manager-app/logout'
  },
  /* This property is changed via --env=ENVIROMENT argument */
  CURRENT_ENVIRONMENT: 'QA'
};
