import angular from 'angular';

import {PROPERTIES} from './properties.constant';
import {PropertiesService} from './properties.service';

export const PropertiesModule = angular
  .module('components.properties', [
  ])
  .constant('PROPERTIES', PROPERTIES)
  .service('PropertiesService', PropertiesService)
  .name;
