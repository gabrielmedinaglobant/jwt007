import angular from 'angular';

export class PropertiesService {
  constructor(PROPERTIES) {
    'ngInject';
    this.PROPERTIES = PROPERTIES;
  }

  getAll() {
    return angular.copy(this.PROPERTIES);
  }

  get(propertyName) {
    return angular.copy(this.PROPERTIES[this.PROPERTIES.CURRENT_ENVIRONMENT][propertyName]);
  }
}
