import _ from 'lodash';
import angular from 'angular';

export const BrandFormComponent = {
  template: require('./brand-form.html'),
  bindings: {
    create: '<',
    brandToUpdate: '<',
    apiError: '<',
    apiErrorDetails: '<',
    apiErrorMessage: '<',
    communicationError: '<',
    communicationErrorMessage: '<',
    onCancel: '&',
    onSave: '&'
  },
  controller: class BrandFormController {
    constructor($log, MESSAGES, ClientService, ProcessingCounterFactoryService) {
      'ngInject';

      this.$log = $log;
      this.MESSAGES = MESSAGES;
      this.ClientService = ClientService;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();
    }

    clientChange() {
      this.name = '';
    }

    // client loading

    findAllClientsByNameLike(clientName) {
      this.$log.debug("BrandFormComponent -> getAllClients -> ", clientName);

      return this.ClientService
        .findAllClientsByNameLike(clientName)
        .then( (response) => {
          this.$log.debug("BrandFormComponent -> findAllClientsByNameLike -> ", response.data);
          return response.data;
        } );
    }

    // data fetching

    isLoadingTable() {

    }

    loadData() {
      this.$log.debug('BrandFormComponent -> loadData', this.brandToUpdate);
      this.id = this.brandToUpdate.id;
      this.clientId = this.brandToUpdate.clientId;
      this.clientName = this.brandToUpdate.clientName;
      this.client = {
        id: this.clientId,
        name: this.clientName
      };
      this.uuid = this.brandToUpdate.uuid;
      this.name = this.brandToUpdate.name;
    }

    // view data retrieving

    hasApiError() {
      return this.apiError;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    isCreateMode() {
      return this.create;
    }

    isUpdateMode() {
      return !this.create;
    }

    isFormValid() {
      return this.form.$valid;
    }

    // form actions

    cancel() {
      this.onCancel( { $event: {} } );
    }

    save() {
      if(this.isFormValid()) {
        this.onSave( { $event: {
          create: this.create,
          id: this.id,
          uuid: this.uuid,
          name: this.name,
          clientId: this.client.id,
          clientName: this.client.name
        } } );
      }
    }

    // lifecycle hooks

    $onInit() {
      
    }

    $onChanges(changesObj) {
      if(changesObj.brandToUpdate && this.create === false){
        this.brandToUpdate = angular.copy(this.brandToUpdate);
        this.loadData();
      }
    }

  }
};
