import angular from 'angular';

import {BrandFormComponent} from './brand-form.component';

export const BrandFormModule = angular
  .module('brand.form', [
  ])
  .component('jwtBrandForm', BrandFormComponent)
  .name;
