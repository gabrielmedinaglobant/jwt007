export class BrandService {
  constructor($log, $http, $q, PropertiesService) {
    'ngInject';
    this.$log = $log;
    this.$http = $http;
    this.$q = $q;

    this.url = PropertiesService.get('SERVICES_BACKEND_URL');
    this.resourceUrl = this.url + 'brands';
    this.resourceByIdBaseUrl = this.resourceUrl + '/{id}';
    this.headers = {Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwcmFzYWQuc2FsYXNrYXJAZ2xvYmFudC5jb20iLCJuYW1lSWQiOiJwcmFzYWQuc2FsYXNrYXJAZ2xvYmFudC5jb20iLCJyb2xlIjoiUk9MRV9BTkFMWVNUIiwicHJvamVjdElkIjoxLCJwcm9qZWN0QXNzaWdubWVudHMiOlt7InByb2plY3RJZCI6MSwicm9sZSI6IlJPTEVfQU5BTFlTVCJ9XX0.nA111M1v3F6D38pyIeWarHfy3COvMYqLWD5AkQPHP_k'};
  }

  delete(brandId) {
    this.url = this.resourceByIdBaseUrl.replace('{id}', brandId);

    return this.$http.delete(this.url, {
      headers: this.headers
    });
  }

  get(brandId) {
    this.url = this.resourceByIdBaseUrl.replace('{id}', brandId);

    this.$log.debug("Calling the following URL");
    this.$log.debug(this.url);

    return this.$http.get(this.url, {
      headers: this.headers
    });
  }

  getAll() {
    this.$log.debug("Calling the following URL");
    this.$log.debug(this.resourceUrl);
    return this.$http.get(this.resourceUrl, {
      headers: this.headers
    });
  }

  update(brand){
    this.url = this.resourceByIdBaseUrl.replace('{id}', brand.id);

    this.$log.debug("update -> URL");
    this.$log.debug(this.url);

    return this.$http.post(this.url, {
      clientId: brand.clientId,
      name: brand.name
    }, {
      headers: this.headers
    });
  }

  save(brand) {
    this.$log.debug("Calling the following URL -> ", this.resourceUrl, brand);

    return this.$http.post(this.resourceUrl, {
      clientId: brand.clientId,
      name: brand.name
    }, {
      headers: this.headers
    });
  }

  nameExists(clientId, name) {
    let url = this.resourceUrl + '/exists';

    this.$log.debug("Calling the following URL");
    this.$log.debug(url);

    return this.$http.get(url, {
      params: {
        "client-id": clientId,
        name: name
      },
      headers: this.headers
    });
  }
}
