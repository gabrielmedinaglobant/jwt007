import angular from 'angular';

import {BrandEditComponent} from './brand-edit.component';

export const BrandEditModule = angular
  .module('brand.edit', [
  ])
  .component('jwtBrandEdit', BrandEditComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.brand-edit', {
          url: '/brand/{id}/edit',
          component: 'jwtBrandEdit',
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
