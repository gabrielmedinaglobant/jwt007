import _ from 'lodash';
import angular from 'angular';

export const BrandEditComponent = {
  template: require('./brand-edit.html'),
  controller: class BrandEditController {
    constructor($log, $state, $stateParams, MESSAGES, BrandService, ProcessingCounterFactoryService) {
      'ngInject';
      this.$log = $log;
      this.$state = $state;
      this.$stateParams = $stateParams;
      this.MESSAGES = MESSAGES;
      this.BrandService = BrandService;

      this.id = $stateParams.id;
      this.brand = {};

      this.apiError = false;
      this.apiErrorDetails = '';
      this.apiErrorMessage = '';
      this.communicationError = false;
      this.communicationErrorMessage = MESSAGES.ERROR.COMMUNICATION_PROBLEM;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();

      this.save = _.once( this._save );
    }

    getBrand() {
      return this.brand;
    }

    hasApiError() {
      return this.apiError;
    }

    getApiErrorMessage() {
      return this.apiErrorMessage;
    }

    getApiErrorDetails() {
      return this.apiErrorDetails;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    getCommunicationErrorMessage() {
      return this.communicationErrorMessage;
    }

    cancel($event) {
      this.$state.go('app.brand-index');
    }

    _save($event) {
      // here it goes save for brand service
      this.$log.debug("Save of new brand was requested");
      this.$log.debug("$event obj received: ", $event);

      this.processingCounter.incrementProcessing();
      this.BrandService.update({
        id: $event.id,
        clientId: $event.clientId,
        name: $event.name
      })
      .then( (response) => {
        this.processingCounter.decrementProcessing();
        this.$log.debug(response);
        this.$state.go('app.brand-index', {
          flashSuccess: true,
          successAction: 'edit',
          item: {
            name: $event.name
          }
        });
      } )
      .catch( (response) => {
        this.processingCounter.decrementProcessing();

        if(response.status === -1){
          this.communicationError = true;
        }
        
        this.$log.debug(response);
      } );

      
    }

    // data fetching

    isProcessing() {
      return this.processingCounter.isProcessing();
    }

    loadData() {
      this.processingCounter.incrementProcessing();
      this.BrandService
        .get(this.id)
        .then( (response) => {
          this.$log.debug("Load brand data", response);

          this.brand = response.data;
          this.processingCounter.decrementProcessing();
        } )
        .catch( (response) => {
          this.$log.error("Couldn't load brand data", response);
        } );
    }

    // lifecycle hooks

    $onInit() {
      this.loadData();
    }

  }
};
