import angular from 'angular';

import {BrandCreateComponent} from './brand-create.component';

export const BrandCreateModule = angular
  .module('brand.create', [
  ])
  .component('jwtBrandCreate', BrandCreateComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.brand-create', {
          url: '/brand/create',
          component: 'jwtBrandCreate',
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
