import _ from 'lodash';
import angular from 'angular';

export const BrandCreateComponent = {
  template: require('./brand-create.html'),
  controller: class BrandCreateController {
    constructor($log, $state, MESSAGES, ClientService, BrandService, ProcessingCounterFactoryService) {
      'ngInject';
      this.$log = $log;
      this.$state = $state;
      this.MESSAGES = MESSAGES;
      this.ClientService = ClientService;
      this.BrandService = BrandService;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();

      this.save = _.once( this._save );
    }

    hasApiError() {
      return this.apiError;
    }

    getApiErrorMessage() {
      return this.apiErrorMessage;
    }

    getApiErrorDetails() {
      return this.apiErrorDetails;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    getCommunicationErrorMessage() {
      return this.communicationErrorMessage;
    }

    cancel($event) {
      this.$state.go('app.brand-index');
    }

    _save($event) {
      // here it goes save for client service
      this.$log.debug("Save of new brand was requested");
      this.$log.debug("$event obj received: ", $event);

      this.processingCounter.incrementProcessing();
      this.BrandService
        .save({
          clientId: $event.clientId,
          name: $event.name
        })
        .then( (response) => {
          this.processingCounter.decrementProcessing();
          this.$log.debug('brand create save -> ', response);

          this.$state.go('app.brand-index', {
            flashSuccess: true,
            successAction: 'create',
            item: {
              name: $event.name,
              clientName: $event.clientName
            }
          });
        } )
        .catch( (response) => {
          this.processingCounter.decrementProcessing();

          if(response.status === -1){
            this.communicationError = true;
          }
          
          this.$log.debug(response);
        } );
      
    }

    // general processing

    isProcessing() {
      return this.processingCounter.isProcessing();
    }

    // lifecycle hooks

    $onInit() {

    }

  }
};
