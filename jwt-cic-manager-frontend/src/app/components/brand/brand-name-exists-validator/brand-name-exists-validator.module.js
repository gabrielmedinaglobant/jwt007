import angular from 'angular';

import {BrandNameExistsValidatorDirective} from './brand-name-exists-validator.directive';

export const BrandNameExistsValidatorModule = angular
  .module('brand.name-exists-validator', [
  ])
  .directive('jwtBrandNameExistsValidator', BrandNameExistsValidatorDirective)
  .name;
