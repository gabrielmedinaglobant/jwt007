export const BrandNameExistsValidatorDirective = function (BrandService, $q, $log, $parse){

  'ngInject';

  let directive = {
    require: 'ngModel',
    link: link,
    restrict: 'A'
  };

  return directive;

  function link(scope, element, attrs, ngModel) {
    ngModel.$asyncValidators.brandNameExists = function (modelValue, viewValue) {

      $log.debug('I need to get an attribute for client-id ', attrs);
      $log.debug( $parse(attrs.clientId)(scope) );

      let clientId = $parse(attrs.clientId)(scope);

      return BrandService.nameExists(clientId, viewValue).then(
        function (brandNameExists){
          $log.debug("BrandService.nameExists(viewValue).promise");
          if(brandNameExists.data === true){
            $log.debug(brandNameExists, " -> rejected");
            return $q.reject('brandNameExists');
          }
          return true;
        },
        function (error){
          return $q.reject('error');
        }
      );

    };
  }

};
