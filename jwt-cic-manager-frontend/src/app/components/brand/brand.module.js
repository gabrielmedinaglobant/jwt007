import angular from 'angular';

import {BrandCreateModule} from './brand-create/brand-create.module';
import {BrandDeleteModalModule} from './brand-delete-modal/brand-delete-modal.module';
import {BrandEditModule} from './brand-edit/brand-edit.module';
import {BrandFormModule} from './brand-form/brand-form.module';
import {BrandIndexModule} from './brand-index/brand-index.module';
import {BrandNameExistsValidatorModule} from './brand-name-exists-validator/brand-name-exists-validator.module';
import {BrandService} from './brand.service';

export const BrandModule = angular
  .module('components.brand', [
    BrandCreateModule,
    BrandDeleteModalModule,
    BrandEditModule,
    BrandFormModule,
    BrandIndexModule,
    BrandNameExistsValidatorModule
  ])
  .service('BrandService', BrandService)
  .name;