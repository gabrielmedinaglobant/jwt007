import angular from 'angular';

import {BrandDeleteModalComponent} from './brand-delete-modal.component';

export const BrandDeleteModalModule = angular
  .module('brand.delete-modal', [
  ])
  .component('jwtBrandDeleteModal', BrandDeleteModalComponent)
  .name;
