import angular from 'angular';

export const BrandDeleteModalComponent = {
  template: require('./brand-delete-modal.html'),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: class BrandDeleteModalController {
    
    constructor($log){
      'ngInject';
      this.$log = $log;
    }

    confirm() {
      this.$log.debug('BrandDeleteModalController -> User has clicked on confirm');
      this.close( {
        $value: {
          action: 'confirm',
          item: this.item
        }
      } );
    }

    cancel() {
      this.$log.debug('BrandDeleteModalController -> User has clicked on cancel');
      this.close( {
        $value: {
          action: 'cancel'
        }
      } );
    }
    
    $onInit() {
      this.$log.debug('BrandDeleteModalController -> $onInit');

      this.item = this.resolve.item;
    }
  }
};