import angular from 'angular';

import {BrandIndexComponent} from './brand-index.component';

export const BrandIndexModule = angular
  .module('brand.index', [
  ])
  .component('jwtBrandIndex', BrandIndexComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.brand-index', {
          url: '/brand/index',
          component: 'jwtBrandIndex',
          params: {
            flashSuccess: null,
            successAction: null,
            item: null
          },
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
