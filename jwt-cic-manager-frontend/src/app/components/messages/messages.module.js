import angular from 'angular';
import {MESSAGES} from './messages.constant';

export const MessagesModule = angular
  .module('components.messages', [
  ])
  .constant('MESSAGES', MESSAGES)
  .name;
