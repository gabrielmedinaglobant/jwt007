export const MESSAGES = {
  ERROR: {
    API: 'API operation could not be completed for <strong>{{itemName}}</strong>.',
    COMMUNICATION_PROBLEM: 'There is a connection error. Please refresh and try again later.',
    NON_BLANK_SPACE: 'Do not leave blanks',
    ONLY_ALPHANUMERIC_ALLOWED: 'You are only allowed to use alphanumeric characters',
    NOT_AUTHORIZED: 'Not authorized'
  }, 
  INFO: {
    WAITING: 'Please wait...'
  }, 
  SUCCESS: {
    CLIENT_CREATED: '<strong>{{name}}</strong> client has been successfully created',
    CLIENT_UPDATED: '<strong>{{name}}</strong> client has been successfully updated',
    CLIENT_DELETED: '<strong>{{name}}</strong> client has been successfully deleted',
    BRAND_CREATED: '<strong>{{name}}</strong> brand of <strong>{{clientName}}</strong> client has been successfully created',
    BRAND_UPDATED: '<strong>{{name}}</strong> brand of <strong>{{clientName}}</strong> client has been successfully updated',
    BRAND_DELETED: '<strong>{{name}}</strong> brand of <strong>{{clientName}}</strong> client has been successfully deleted'
  }
};
