import angular from 'angular';

import {AuthenticationService} from './authentication.service';
import {AuthenticationRepositoryService} from './authentication-repository.service';
import {AuthorizationService} from './authorization.service';

import {AuthenticationErrorComponent} from './authentication-error.component';
import {TokenReceiverComponent} from './token-receiver.component';

export const AuthenticationModule = angular
  .module('common.authentication', [
  ])
  .component('jwtAuthenticationError', AuthenticationErrorComponent)
  .component('jwtTokenReceiver', TokenReceiverComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.authentication-error', {
          url: '/authentication/error',
          component: 'jwtAuthenticationError'
        })
        .state('app.authentication-token-receiver', {
          url: '/receive-token?token',
          component: 'jwtTokenReceiver'
        });
    }
  )
  .service('AuthenticationService', AuthenticationService)
  .service('AuthenticationRepositoryService', AuthenticationRepositoryService)
  .service('AuthorizationService', AuthorizationService)
  .name;
