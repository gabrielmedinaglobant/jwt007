import angular from 'angular';
import jwtDecode from 'jwt-decode';

export class AuthenticationRepositoryService {
  constructor($log, $http, $q, $timeout, $window) {
    'ngInject';

    this.$log = $log;
    this.$window = $window;
  }

  deleteJwtToken() {
    this.$window.localStorage.removeItem('jwt.cic.manager.jwtToken');
  }

  setJwtToken(jwtToken) {
    this.$window.localStorage.setItem('jwt.cic.manager.jwtToken', jwtToken);
  }

  getJwtToken() {
    return this.$window.localStorage.getItem('jwt.cic.manager.jwtToken');
  }

  getCurrentUser() {
    let jwtToken = this.getJwtToken();
    let jwtPayloadObj = angular.fromJson(jwtDecode(jwtToken));

    this.$log.debug("AuthenticationRepositoryService -> getCurrentUser -> jwtPayloadObj", jwtPayloadObj);

    return jwtPayloadObj;
  }

}
