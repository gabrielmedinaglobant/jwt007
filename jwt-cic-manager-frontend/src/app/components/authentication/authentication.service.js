import _ from 'lodash';
import jwtDecode from 'jwt-decode';

export class AuthenticationService {
  constructor($log, $http, $q, $timeout, $window, AuthenticationRepositoryService) {
    'ngInject';
    this.AuthenticationRepositoryService = AuthenticationRepositoryService;
  }

  getJwtToken() {
    return this.AuthenticationRepositoryService.getJwtToken();
  }

  getCurrentUser() {
    return this.AuthenticationRepositoryService.getCurrentUser();
  }

  isAuthenticated() {
    // TODO: Improve error handling of this. (What happens if a bad JWT token is introduced on localStorage)
    let jwtToken = this.AuthenticationRepositoryService.getJwtToken();
    if(!jwtToken){
      return false;
    }

    return true;
  }

  authenticate(jwtToken) {
    this.AuthenticationRepositoryService.setJwtToken(jwtToken);
  }

  cleanAuthentication() {
    this.AuthenticationRepositoryService.deleteJwtToken();
  }

}
