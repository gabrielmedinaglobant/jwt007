import _ from 'lodash';
import jwtDecode from 'jwt-decode';

export class AuthorizationService {
  constructor($log, $http, $q, $timeout, $window, AuthenticationService) {
    'ngInject';
    this.$log = $log;
    this.AuthenticationService = AuthenticationService;
  }

  _isRoleAllowed(roleToCheck, roles) {
    let allowed = false;

    _.forEach(roles, (role) => {
      if(role === roleToCheck){
        allowed = true;
        return false;
      }
    });

    return allowed;
  }

  isAllowed(state, roles) {
    
    let user = this.AuthenticationService.getCurrentUser();
    let noUser = (angular.isUndefined(user) || user === null);
    let noRoles = (angular.isUndefined(roles) || roles.length === 0);
    let allowed = false;

    if( noRoles ) {
      // if this state is allowed as ANY, return true
      allowed = true;
    } else if( noUser && this._isRoleAllowed('ROLE_ANONYMOUS', roles) ) {
      // if this state is only allowed for anonymous and there is no user, return true
      allowed = true;
    } else if( !noUser && this._isRoleAllowed(user.role, roles) ) {
      // check if this state is allowed for any of the contained roles of the authenticated user, return true
      allowed = true;
    }

    return allowed;
  }

}
