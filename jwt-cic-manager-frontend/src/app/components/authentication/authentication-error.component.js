import _ from 'lodash';
import angular from 'angular';

export const AuthenticationErrorComponent = {
  template: `
    <div class="container">
      <div class="alert alert-danger element__top-padding">
        Not authorized
      </div>
    </div>
  `,
  controller: class AuthenticationErrorController {
    constructor($log, $state, $stateParams, MESSAGES) {
      'ngInject';

      this.$log = $log;
      this.$state = $state;
      this.$stateParams = $stateParams;
      this.MESSAGES = MESSAGES;

    }

    $onInit(){

    }

  }
};
