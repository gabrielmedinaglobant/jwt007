import _ from 'lodash';
import angular from 'angular';

export const TokenReceiverComponent = {
  template: `
    <div class="container">
      <div class="alert alert-info element__top-padding">
        <span class="glyphicon glyphicon-cog gly-spin" aria-hidden="true"></span> Please wait... 
      </div>
    </div>
  `,
  controller: class TokenReceiverController {
    constructor($log, $state, $stateParams, MESSAGES, AuthenticationService) {
      'ngInject';

      this.$log = $log;
      this.$state = $state;
      this.$stateParams = $stateParams;
      this.MESSAGES = MESSAGES;
      this.AuthenticationService = AuthenticationService;

    }

    $onInit(){

      // Here a jwtToken will be received and stored if SAML authentication was successfull
      // If role is not analyst, there will be a message that indicates your role is not analyst
      // every state except this should be intercepted so as to detect that only analysts role are valid
      // to do any operation

      // Bear in mind that if token is stored directly on local storage, it will bypass analyst verification
      // Under normal circumstances, auth app should not send you this token if you are not an 
      // analyst user. If token is bypassed, UI will be loaded, but any API operation should be denied.
        

      this.$log.debug('Receiving JWT Token ...', this.$stateParams.token);

      this.$log.debug('Storing JWT token ...');
      this.AuthenticationService.authenticate(this.$stateParams.token);
      
      this.$log.debug('Redirects to app view');
      this.$state.go('app');
    }

  }
};
