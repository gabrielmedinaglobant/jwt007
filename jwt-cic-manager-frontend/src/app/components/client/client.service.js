export class ClientService {
  constructor($log, $http, $q, $timeout, PropertiesService) {
    'ngInject';
    this.$log = $log;
    this.$http = $http;
    this.$q = $q;
    this.$timeout = $timeout;

    this.url = PropertiesService.get('SERVICES_BACKEND_URL');
    this.resourceUrl = this.url + 'clients';
    this.resourceByIdBaseUrl = this.url + 'clients/{id}';

    this.headers = {Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwcmFzYWQuc2FsYXNrYXJAZ2xvYmFudC5jb20iLCJuYW1lSWQiOiJwcmFzYWQuc2FsYXNrYXJAZ2xvYmFudC5jb20iLCJyb2xlIjoiUk9MRV9BTkFMWVNUIiwicHJvamVjdElkIjoxLCJwcm9qZWN0QXNzaWdubWVudHMiOlt7InByb2plY3RJZCI6MSwicm9sZSI6IlJPTEVfQU5BTFlTVCJ9XX0.nA111M1v3F6D38pyIeWarHfy3COvMYqLWD5AkQPHP_k'};
  }

  nameExists(name) {
    /*
    return this.$q( (resolve, rejects) => {
      this.$timeout( () => {
        resolve(true);
      }, 1000 );
    } );
    */

    let url = this.resourceUrl + '/exists';

    this.$log.debug("nameExists -> URL");
    this.$log.debug(url);

    return this.$http.get(url, {
      headers: this.headers,
      params: {
        name: name
      }
    });
  }

  get(clientId) {
    let url = this.resourceByIdBaseUrl.replace('{id}', clientId);

    this.$log.debug("get -> URL");
    this.$log.debug(url);

    return this.$http.get(url, {
      headers: this.headers
    });
  }

  getAll() {
    this.$log.debug("getAll -> URL");
    this.$log.debug(this.resourceUrl);

    return this.$http.get(this.resourceUrl, {
      headers: this.headers
    });
  }

  findAllClientsByNameLike(name) {
    this.$log.debug("findAllClientsByNameLike -> URL");
    this.$log.debug(this.resourceUrl);

    return this.$http.get(this.resourceUrl, {
      params: {
        name: name
      },
      headers: this.headers
    });
  }

  save(client) {
    this.$log.debug("save -> URL");
    this.$log.debug(this.resourceUrl);
    this.$log.debug("client -> ", client);

    return this.$http.post(this.resourceUrl, {
      name: client.name
    }, {
      headers: this.headers
    });
  }

  update(client) {
    
    this.url = this.resourceByIdBaseUrl.replace('{id}', client.id);

    this.$log.debug("update -> URL");
    this.$log.debug(this.url);

    return this.$http.post(this.url, {
      name: client.name
    }, {
      headers: this.headers
    });
  }

  delete(clientId) {
    this.url = this.resourceByIdBaseUrl.replace('{id}', clientId);

    this.$log.debug("ClientService -> delete -> ", clientId);
    this.$log.debug("delete -> URL ", this.url);

    return this.$http.delete(this.url, {
      headers: this.headers
    });
  }
  
}
