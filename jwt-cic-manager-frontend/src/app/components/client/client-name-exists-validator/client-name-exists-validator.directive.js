export const ClientNameExistsValidatorDirective = function (ClientService, $q, $log){

  'ngInject';

  let directive = {
    require: 'ngModel',
    link: link,
    restrict: 'A'
  };

  return directive;

  function link(scope, element, attrs, ngModel) {
    ngModel.$asyncValidators.clientNameExists = function (modelValue, viewValue) {

      return ClientService.nameExists(viewValue).then(
        function (clientNameExists){
          $log.debug("ClientService.nameExists(viewValue).promise");
          if(clientNameExists.data.exists === true){
            $log.debug(clientNameExists, " -> rejected");
            return $q.reject('clientNameExists');
          }
          return true;
        },
        function (error){
          return $q.reject('error');
        }
      );

    };
  }

};
