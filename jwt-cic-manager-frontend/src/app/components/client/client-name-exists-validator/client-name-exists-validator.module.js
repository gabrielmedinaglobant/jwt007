import angular from 'angular';

import {ClientNameExistsValidatorDirective} from './client-name-exists-validator.directive';

export const ClientNameExistsValidatorModule = angular
  .module('client.name-exists-validator', [
  ])
  .directive('jwtClientNameExistsValidator', ClientNameExistsValidatorDirective)
  .name;
