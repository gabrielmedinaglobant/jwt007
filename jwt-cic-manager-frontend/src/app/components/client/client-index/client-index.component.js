import _ from 'lodash';
import angular from 'angular';

export const ClientIndexComponent = {
  template: require('./client-index.html'),
  controller: class ClientIndexController {
    constructor($log, $stateParams, $sce, $uibModal, MESSAGES, ClientService, ProcessingCounterFactoryService) {
      'ngInject';
      this.$log = $log;
      this.$stateParams = $stateParams;
      this.$sce = $sce;
      this.$uibModal = $uibModal;
      this.MESSAGES = MESSAGES;
      this.ClientService = ClientService;

      this.apiError = false;
      this.apiErrorDetails = '';
      this.apiErrorMessage = '';
      this.communicationError = false;
      this.communicationErrorMessage = MESSAGES.ERROR.COMMUNICATION_PROBLEM;
      this.waitingMessage = MESSAGES.INFO.WAITING;
      this.success = false;
      this.successMessage = '';

      this.loadingTable = false;
      this.processingCounter = ProcessingCounterFactoryService.createInstance();

      this.data = [];
      this.filteredData = [];
      // data for view
      this.viewData = [];
      this.dataHasBeenSorted = false;
      // uib-pagination from data
      this.dataTotalItems = 0;
      // uib-pagination "inmutable" (not exactly, should not change)
      this.dataMaxItems = 10;
      this.dataMaxSize = 10;
      // uib-pagination mutable
      this.dataNumPages = 0;
      this.dataCurrentPage = 1;
      this.dataPreviousCurrentPage = this.dataCurrentPage;

      this.$log.debug('$stateParams -> ', $stateParams);
    }

    // success

    hasSuccess() {
      return this.success;
    }

    getSuccessMessage() {
      return this.successMessage;
    }

    // error

    hasApiError() {
      return this.apiError;
    }

    getApiErrorMessage() {
      return this.apiErrorMessage;
    }

    getApiErrorDetails() {
      return this.apiErrorDetails;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    getCommunicationErrorMessage() {
      return this.communicationErrorMessage;
    }

    clearAllMessages() {
      this.success = false;
      this.successMessage = '';
      this.communicationError = false;
      this.apiError = false;
      this.apiErrorMessage = '';
      this.apiErrorDetails = '';
    }

    fetchFlashMessages() {
      // if there is a success message
      // check action and item and show accordingly
      this.$log.debug('fetchFlashMessages -> $stateParams -> ', this.$stateParams);

      if(this.$stateParams.flashSuccess){
        this.success = true;
        if(this.$stateParams.successAction === 'create'){
          this.successMessage = this.$sce.trustAsHtml(this.MESSAGES.SUCCESS.CLIENT_CREATED.replace('{{name}}', this.$stateParams.item.name));
        } else if(this.$stateParams.successAction === 'edit'){
          this.successMessage = this.$sce.trustAsHtml(this.MESSAGES.SUCCESS.CLIENT_UPDATED.replace('{{name}}', this.$stateParams.item.name));
        }
        
      }
    }

    // data fetching

    isProcessing() {
      return this.processingCounter.isProcessing();
    }

    isLoadingTable() {
      return this.loadingTable;
    }

    loadData() {
      this.loadingTable = true;
      this.processingCounter.incrementProcessing();
      this.ClientService
        .getAll()
        .then( response => {
          this.processingCounter.decrementProcessing();

          this.data = response.data;
          this.filteredData = this.data;
          this.viewData = _.take(this.filteredData, this.dataMaxItems);
          this.dataTotalItems = this.data.length;
          this.dataCurrentPage = 1;
          this.dataHasBeenSorted = false;

          this.loadingTable = false;
        } )
        .catch( response => {
          this.processingCounter.decrementProcessing();

          if(response.status === -1){
            this.communicationError = true;
          }
          
          this.$log.debug(response);
        } );
    }

    // view data retrieving

    getAll() {
      return this.viewData;
    }

    // pagination methods

    sortByNameAsc() {
      this.filteredData = _.orderBy(this.filteredData, ['name'], ['asc']);
      this.goToPage();
    }

    sortByNameDesc() {
      this.filteredData = _.orderBy(this.filteredData, ['name'], ['desc']);
      this.goToPage();
    }

    goToPage() {
      let currentOffset = this.dataMaxItems*(this.dataCurrentPage-1);
      let calculatedLastElementIndex = currentOffset + this.dataMaxItems;
      let filteredDataItemsCount = this.filteredData.length;
      let lastElementIndex = (calculatedLastElementIndex >= filteredDataItemsCount) ? filteredDataItemsCount : calculatedLastElementIndex;
      this.viewData = _.slice(this.filteredData, currentOffset, currentOffset+this.dataMaxItems);
      this.dataHasBeenSorted = false;
    }

    filterByName($event) {
      let name = $event.text;
      this.filteredData = _.filter( this.data, d => {
        return _.startsWith(_.toLower(d.name), _.toLower(name));
      } );
      this.viewData = _.take(this.filteredData, this.dataMaxItems);
      this.dataCurrentPage = 1;
      this.dataTotalItems = this.filteredData.length;
      this.dataHasBeenSorted = false;
    }

    // actions per items

    deleteItem(item) {
      this.$log.debug('ClientIndexComponent => deleteItem =>', item);
      //item.id and item.name needed
      let modalInstance = this.$uibModal.open({
        animation: true,
        component: 'jwtClientDeleteModal',
        resolve: {
          item: () => {
            return item;
          }
        }
      });

      modalInstance.result
      .then( ($value) => {
        this.$log.info('$value => ', $value);

        if($value.action === 'confirm') {

          this.processingCounter.incrementProcessing();

          this.clearAllMessages();

          this.ClientService.delete($value.item.id)
            .then( (response) => {
              this.$log.debug('delete -> then -> ', response);

              this.loadData();

              this.success = true;
              this.successMessage = this.$sce.trustAsHtml(this.MESSAGES.SUCCESS.CLIENT_DELETED.replace('{{name}}', $value.item.name));

              this.processingCounter.decrementProcessing();
            } )
            .catch( (response) => {
              this.$log.debug('delete -> catch -> ', response);

              if(response.status === -1) {
                this.communicationError = true;
              } else if(response.data.error !== angular.isUndefined) {
                this.apiError = true;
                this.apiErrorMessage = this.$sce.trustAsHtml(this.MESSAGES.ERROR.API.replace('{{itemName}}', $value.item.name));
                this.apiErrorDetails = response.data.message;
              }

              this.success = false;
              this.processingCounter.decrementProcessing();
            } );
        }
      } );

    }

    // lifecycle hooks

    $onInit() {
      this.loadData();
      this.fetchFlashMessages();
    }

    $doCheck() {
      if( (this.dataPreviousCurrentPage !== this.dataCurrentPage) ){
        this.dataPreviousCurrentPage = this.dataCurrentPage;
        this.$log.debug('page has changed to ' + this.dataCurrentPage);
        this.goToPage();
      }
    }
  }
};
