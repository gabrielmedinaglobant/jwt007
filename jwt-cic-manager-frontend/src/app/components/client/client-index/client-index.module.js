import angular from 'angular';

import {ClientIndexComponent} from './client-index.component';

export const ClientIndexModule = angular
  .module('client.index', [
  ])
  .component('jwtClientIndex', ClientIndexComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.client-index', {
          url: '/client/index',
          component: 'jwtClientIndex',
          params: {
            flashSuccess: null,
            successAction: null,
            item: null
          },
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
