import angular from 'angular';

export const ClientDeleteModalComponent = {
  template: require('./client-delete-modal.html'),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: class ClientDeleteModalController {
    
    constructor($log){
      'ngInject';
      this.$log = $log;
    }

    confirm() {
      this.$log.debug('ClientDeleteModalController -> User has clicked on confirm');
      this.close( {
        $value: {
          action: 'confirm',
          item: this.item
        }
      } );
    }

    cancel() {
      this.$log.debug('ClientDeleteModalController -> User has clicked on cancel');
      this.close( {
        $value: {
          action: 'cancel'
        }
      } );
    }
    
    $onInit() {
      this.$log.debug('ClientDeleteModalController -> $onInit');

      this.item = this.resolve.item;
    }
  }
};