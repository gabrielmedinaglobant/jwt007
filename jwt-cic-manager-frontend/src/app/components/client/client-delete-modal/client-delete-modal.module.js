import angular from 'angular';

import {ClientDeleteModalComponent} from './client-delete-modal.component';

export const ClientDeleteModalModule = angular
  .module('client.delete-modal', [
  ])
  .component('jwtClientDeleteModal', ClientDeleteModalComponent)
  .name;
