import angular from 'angular';

import {ClientFormComponent} from './client-form.component';

export const ClientFormModule = angular
  .module('client.form', [
  ])
  .component('jwtClientForm', ClientFormComponent)
  .name;
