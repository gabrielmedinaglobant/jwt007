import _ from 'lodash';
import angular from 'angular';

export const ClientFormComponent = {
  template: require('./client-form.html'),
  bindings: {
    create: '<',
    clientToUpdate: '<',
    apiError: '<',
    apiErrorDetails: '<',
    apiErrorMessage: '<',
    communicationError: '<',
    communicationErrorMessage: '<',
    onCancel: '&',
    onSave: '&'
  },
  controller: class ClientFormController {
    constructor($log, MESSAGES, ProcessingCounterFactoryService) {
      'ngInject';

      this.$log = $log;
      this.MESSAGES = MESSAGES;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();
    }

    // data fetching

    isLoadingTable() {

    }

    loadData() {
      this.$log.debug('ClientFormComponent -> loadData', this.clientToUpdate);
      this.id = this.clientToUpdate.id;
      this.uuid = this.clientToUpdate.uuid;
      this.name = this.clientToUpdate.name;
    }

    // view data retrieving

    hasApiError() {
      return this.apiError;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    isCreateMode() {
      return this.create;
    }

    isUpdateMode() {
      return !this.create;
    }

    isFormValid() {
      return this.form.$valid;
    }

    // form actions

    cancel() {
      this.onCancel( { $event: {} } );
    }

    save() {
      if(this.isFormValid()) {
        this.onSave( { $event: {
          create: this.create,
          id: this.id,
          uuid: this.uuid,
          name: this.name
        } } );
      }
    }

    // lifecycle hooks

    $onInit() {
      
    }

    $onChanges(changesObj) {
      if(changesObj.clientToUpdate && this.create === false){
        this.clientToUpdate = angular.copy(this.clientToUpdate);
        this.loadData();
      }
    }

  }
};
