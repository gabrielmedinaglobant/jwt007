import _ from 'lodash';
import angular from 'angular';

export const ClientEditComponent = {
  template: require('./client-edit.html'),
  controller: class ClientEditController {
    constructor($log, $state, $stateParams, MESSAGES, ClientService, ProcessingCounterFactoryService) {
      'ngInject';
      this.$log = $log;
      this.$state = $state;
      this.$stateParams = $stateParams;
      this.MESSAGES = MESSAGES;
      this.ClientService = ClientService;

      this.id = $stateParams.id;
      this.client = {};

      this.apiError = false;
      this.apiErrorDetails = '';
      this.apiErrorMessage = '';
      this.communicationError = false;
      this.communicationErrorMessage = MESSAGES.ERROR.COMMUNICATION_PROBLEM;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();

      this.save = _.once( this._save );
    }

    getClient() {
      return this.client;
    }

    hasApiError() {
      return this.apiError;
    }

    getApiErrorMessage() {
      return this.apiErrorMessage;
    }

    getApiErrorDetails() {
      return this.apiErrorDetails;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    getCommunicationErrorMessage() {
      return this.communicationErrorMessage;
    }

    cancel($event) {
      this.$state.go('app.client-index');
    }

    _save($event) {
      // here it goes save for client service
      this.$log.debug("Save of new client was requested");
      this.$log.debug("$event obj received: ", $event);

      this.processingCounter.incrementProcessing();
      this.ClientService.update({
        id: $event.id,
        name: $event.name
      })
      .then( (response) => {
        this.processingCounter.decrementProcessing();
        this.$log.debug(response);
        this.$state.go('app.client-index', {
          flashSuccess: true,
          successAction: 'edit',
          item: {
            name: $event.name
          }
        });
      } )
      .catch( (response) => {
        this.processingCounter.decrementProcessing();

        if(response.status === -1){
          this.communicationError = true;
        }
        
        this.$log.debug(response);
      } );

      
    }

    // data fetching

    isProcessing() {
      return this.processingCounter.isProcessing();
    }

    loadData() {
      this.processingCounter.incrementProcessing();
      this.ClientService
        .get(this.id)
        .then( (response) => {
          this.$log.debug("Load client data", response);

          this.client = response.data;
          this.processingCounter.decrementProcessing();
        } )
        .catch( (response) => {
          this.$log.error("Couldn't load client data", response);
        } );
    }

    // lifecycle hooks

    $onInit() {
      this.loadData();
    }

  }
};
