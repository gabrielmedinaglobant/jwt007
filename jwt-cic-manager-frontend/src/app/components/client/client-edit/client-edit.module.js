import angular from 'angular';

import {ClientEditComponent} from './client-edit.component';

export const ClientEditModule = angular
  .module('client.edit', [
  ])
  .component('jwtClientEdit', ClientEditComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.client-edit', {
          url: '/client/{id}/edit',
          component: 'jwtClientEdit',
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
