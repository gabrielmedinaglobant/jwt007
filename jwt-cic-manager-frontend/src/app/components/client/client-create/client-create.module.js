import angular from 'angular';

import {ClientCreateComponent} from './client-create.component';

export const ClientCreateModule = angular
  .module('client.create', [
  ])
  .component('jwtClientCreate', ClientCreateComponent)
  .config(
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      'ngInject';
      $stateProvider
        .state('app.client-create', {
          url: '/client/create',
          component: 'jwtClientCreate',
          authorize: ['ROLE_ANALYST']
        });
    }
  )
  .name;
