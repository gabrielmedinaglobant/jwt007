import _ from 'lodash';
import angular from 'angular';

export const ClientCreateComponent = {
  template: require('./client-create.html'),
  controller: class ClientCreateController {
    constructor($log, $state, MESSAGES, ClientService, ProcessingCounterFactoryService) {
      'ngInject';
      this.$log = $log;
      this.$state = $state;
      this.MESSAGES = MESSAGES;
      this.ClientService = ClientService;

      this.apiError = false;
      this.apiErrorDetails = '';
      this.apiErrorMessage = '';
      this.communicationError = false;
      this.communicationErrorMessage = MESSAGES.ERROR.COMMUNICATION_PROBLEM;

      this.processingCounter = ProcessingCounterFactoryService.createInstance();

      this.save = _.once( this._save );
    }

    hasApiError() {
      return this.apiError;
    }

    getApiErrorMessage() {
      return this.apiErrorMessage;
    }

    getApiErrorDetails() {
      return this.apiErrorDetails;
    }

    hasCommunicationError() {
      return this.communicationError;
    }

    getCommunicationErrorMessage() {
      return this.communicationErrorMessage;
    }

    cancel($event) {
      this.$state.go('app.client-index');
    }

    _save($event) {
      // here it goes save for client service
      this.$log.debug("Save of new client was requested");
      this.$log.debug("$event obj received: ", $event);

      this.processingCounter.incrementProcessing();
      this.ClientService.save({
        name: $event.name
      })
      .then( (response) => {
        this.processingCounter.decrementProcessing();
        this.$log.debug(response);
        this.$state.go('app.client-index', {
          flashSuccess: true,
          successAction: 'create',
          item: {
            name: $event.name
          }
        });
      } )
      .catch( (response) => {
        this.processingCounter.decrementProcessing();

        if(response.status === -1){
          this.communicationError = true;
        }
        
        this.$log.debug(response);
      } );

      
    }

    // general processing

    isProcessing() {
      return this.processingCounter.isProcessing();
    }

    // lifecycle hooks

    $onInit() {

    }

  }
};
