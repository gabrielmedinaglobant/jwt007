import angular from 'angular';

import {ClientService} from './client.service';

import {ClientCreateModule} from './client-create/client-create.module';
import {ClientDeleteModalModule} from './client-delete-modal/client-delete-modal.module';
import {ClientEditModule} from './client-edit/client-edit.module';
import {ClientFormModule} from './client-form/client-form.module';
import {ClientIndexModule} from './client-index/client-index.module';
import {ClientNameExistsValidatorModule} from './client-name-exists-validator/client-name-exists-validator.module';

export const ClientModule = angular
  .module('components.client', [
    ClientCreateModule,
    ClientDeleteModalModule,
    ClientEditModule,
    ClientFormModule,
    ClientIndexModule,
    ClientNameExistsValidatorModule
  ])
  .service('ClientService', ClientService)
  .config(($stateProvider, $urlRouterProvider) => {
    'ngInject';
    // states for client go here
  })
  .name;
