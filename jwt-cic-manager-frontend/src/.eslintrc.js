module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'angular/log': 'warn',
    'angular/on-watch': 'warn',
    'semi': 'error',
    'linebreak-style': 0,
    'eol-last': 'warn',
    'no-unused-vars': 'warn',
    'space-in-parens': 'warn',
    'object-curly-spacing': 'warn',
    'space-before-blocks': 'warn',
    'spaced-comment': 'warn',
    'padded-blocks': 'warn',
    'prefer-arrow-callback': 'warn',
    'quotes': 'warn',
    'keyword-spacing': 'warn',
    'no-trailing-spaces': 'warn',
    'dot-notation': 'warn',
    'no-multiple-empty-lines': 'warn',
    'prefer-const': 'warn',
    'space-infix-ops': 'warn',
    'arrow-parens': 'warn',
    'object-shorthand': 'warn'
  }
}
