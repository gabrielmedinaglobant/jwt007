import angular from 'angular';

import {AppComponent} from './app/app';
import {ComponentsModule} from './app/components/components.module';
import {CommonModule} from './app/common/common.module';

import 'angular-ui-router';
import 'angular-ui-bootstrap';
import routesConfig from './routes';

import './index.scss';
import bootstrapLoader from 'bootstrap-loader';

export const app = 'app';

angular
  .module(app, ['ui.router', 'ui.bootstrap', ComponentsModule, CommonModule])
  .config(routesConfig)
  .component('app', AppComponent)
  .run(
    ($log, $transitions, $state, AuthorizationService) => {
      'ngInject';

      $log.debug('**************** angular run -> ');

      //Authorization goes everytime a $stateChangeStart
      $transitions.onStart(
        {
          to: (state) => {
            return state.authorize;
          }
        },
        (trans) => {
          
          let $state = trans.router.stateService;
          let stateData = trans.$to();
          let result = true;

          $log.debug('$transitions.onStart -> state details', stateData.name, stateData.authorize);

          try{
            if( !AuthorizationService.isAllowed(stateData.name, stateData.authorize) ){
              result = $state.target('app.authentication-error');
            }
          } catch(err){
            // InvalidTokenError
            result = $state.target('app.authentication-error');
          }

          return result;

        });
    }
  );
  
