export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider, $logProvider) {
  $locationProvider.html5Mode(false).hashPrefix('!');
  $urlRouterProvider.otherwise('/app');

  $stateProvider
    .state('app', {
      url: '/app',
      component: 'app',
      authorize: ['ROLE_ANALYST']
    });
    
  $logProvider.debugEnabled(true);
}
